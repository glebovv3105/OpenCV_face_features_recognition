#pragma once
#include "opencv2/opencv.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>
#include <Windows.h>

using namespace std;
using namespace cv;

struct RecData // ��������� ��� ������ �������� ���������� ������ � � ����
{
public:
	vector<Rect> eyes;
	vector<Rect> faces;
	int maxnum; // ��������� ����������� ���� � ������� faces
};

class FaceRec
{
public:
	FaceRec();
	RecData recognize(Mat &frame_gray);
	~FaceRec();
};