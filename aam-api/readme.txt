

INTRODUCTION

The AAM-API is a free, open-source, C++ implementation of the Active 
Appearance Models (AAM) framework targeted for education, further AAM 
research and for pure analysis and segmentation purposes.

Please don't redistribute, but refer to updates of the AAM-API at 
http://www.imm.dtu.dk/~aam/ Good luck with the AAM-API!

Best Regards

Mikkel B. Stegmann



HOW TO GET STARTED?

First, read 'license.txt'. Besides all the standard legal jumbo, it kindly 
asks you to cite a specific reference if you publish work based on the API.

The AAM-API is distributed in a form where all precompiled binaries can be 
run as-is. This means that the demos in /test can be executed and that you 
can toy around with the AAMLab. Just for fun or for educational purposes. 
Notice, that the AAMLab is not open source.

The best way to start is to go to the /test/colour/ directory and:

    1) run __demo__.cmd

    2) have a look at the output files

    3) have a look at __demo__.cmd and __conf__.acf in notepad

    4) start the AAMLab (colour) 

         - open the image from the unseen directory
         - load the model.amf from /test/colour/
         - view the model modes
         - insert the mean shape
         - play around with the search function
           (a shape is moved by dragging the red COG)

    5) that's it

PLEASE NOTICE that the intention of this software is to demonstrate how AAMs 
work. Thus, don't except it to solve the general problem of e.g. face 
recognition for you in seconds. It is not a piece of software that can be 
readily used for your special purpose, without knowing the basics of AAMs. 
I highly recommend reading some of the original AAM papers, which are 
available at Tim Cootes' homepage: http://www.isbe.man.ac.uk/~bim/


If you would like to tamper with the code yourself or use the API in another 
software project, you will have to set-up Visual Studio and install the 
VisSDK. However, I have tried to describe the steps as carefully as possible.

Go to the doc-directory and carry out the howto's in this order:

    1) howto install the aam-api.txt

    2) howto add lapack support to vissdk.txt

    3) howto test the aam-api.txt    

You could also read some of the optional howto's inside /doc.
Source code documentation can be reached from /doc/codedoc.html



DIRECTORY OVERVIEW

/bin          Pre-built executables:

              aamc.exe     Single-band build of the aamc project (see below)
              aamcm.exe    RGB build of the aamc project (see below)

              AAMLab.exe   A graphical front-end to some of the API (grey)
              AAMLabm.exe  A graphical front-end to some of the API (RGB)

              Tip: You can use the AAMLab in conjunction with 
                   matlab/annotate.m to build your own models.

/diva         Root directory of the DIVA library.

/doc          HOWTOs and source code documentation.

/inc          AAM-API header files.

/lib          Static library versions of the AAM-API:

              aam-api.lib       Grey-scale library, release version.
              aam-apim.lib      RGB library, release version.

              Debug versions (*not* included, must be built by /aam-api-lib):

              aam-apiDB.lib     Grey-scale library, debug version.
              aam-apimDB.lib    RGB library, debug version.
	
/matlab       MatLab functions for shape annotation, plotting, asf i/o etc.

              Use annotate.m to produce asf files, when building your own
              models.

/test         Demo files for testing the AAM-API in single-band and RGB mode.

              /test/greyscale contains a medical example
              /test/colour contains a face segmentation example

/aam-api      The AAM-API source code.

/aam-api-lib  MS VC++ project for building the AAM-API libs inside /lib. 
              Use this project to build debug versions of the library.

/aamc         Console application demonstrating AAM-API usage.
              Just write 'aamc' in a prompt for the various options.



REQUIREMENTS FOR EDITING AND RECOMPILATION

- MS Visual C++ 6.0 (or higher) 
- MS VisionSDK
- LAPACK



BRIEF HISTORY OF THE AAM-API

This project was started as my Master's thesis project in 2000. Since then, I
have used some time during my Ph.D. study to improve and extend the API. 

Changes and extensions are as a rule initiated by the topics that I deal with
in my Ph.D. study. As a consequence of that, you may find strange code bits,
undocumented features and more changes in format files etc. than in a normal
open source project.

Anyway, I believe that this "release often, release early" strategy will thrill
more people than it will annoy. I hope that you agree on this point :-) 



ACKNOWLEDGEMENTS

Thanks go out to the following people for giving me feedback, ideas, bug 
reports etc.:

Rune Fisker, Allan Reinhold Kildeby, Martin Egholm Nielsen, Tue Lehn-Schi�ler,
Mark Wrobel, Michael Moesby Nordstr�m, Mads Larsen, Janusz Sierakowski, 
Morten Rufus Blas, Mads Fogtmann Hansen, Kasper Olesen, Kristian Dahl Hansen, 
Peter Stanley J�rgensen, Kristina Hoffmann Larsen, Bjarke Sandvei Jensen, 
Jacob Overgaard Hansen, Steffen Holmslykke, Steffen Andersen and 
S�ren Riisgaard. 

Dmytriyev Anatoliy, Henrik Gehrmann and Christian Stender Simonsen are 
acknowledged for implementing and testing the multi-band extension.

Further, I would like to thank all the people I only have been in contact
with via email. I am very grateful for all your comments, questions, bug 
reports, test images, etc.

This would also be the right place to thank Nullsoft for the awesome free
installation system, NSIS.

Last, but definitely not least, I would like to thank Tim Cootes for making 
my visit to ISBE during Dec.'02 and Jan.'03 a great stay and for discussing 
the various subtleties and non-subtleties of AAMs.


___________________________________
$Revision: 1.10 $ 
$Date: 2003/04/23 14:49:15 $ 
