/* $Id: AAMLinearReg.cpp,v 1.2 2003/01/14 17:28:51 aam Exp $ */
#include "AAMLinearReg.h"


/**

  @author   Mikkel B. Stegmann
  @version  3-16-2001

  @memo     Constructor.

  @doc      Constructor.
  
  @return   Nothning.
  
*/
CAAMLinearReg::CAAMLinearReg() {
	
}


/**

  @author   Mikkel B. Stegmann
  @version  3-16-2001

  @memo     Destructor.

  @doc      Destructor.
  
  @return   Nothning.
  
*/
CAAMLinearReg::~CAAMLinearReg() {
	
	
}


/**

  @author   Mikkel B. Stegmann
  @version  3-15-2000

  @memo		Performs k-estimation.  

  @doc      Performs k-estimation.  
   
  @param    C			The parameter matrix.

  @param    EigenVec_k	Output k eigenvectors.

  @param	EigenVal_k  Output k eigenvalues.
  
  @return   k
  
*/
int CAAMLinearReg::EstimateK( const CDMatrix &C, CDMatrix &EigenVec_k, CDMatrix &EigenVal_k ) {

	// estimate optimal k value
	int k_min = m_nParams;
	double Ek, Ek_min = 1.0e308;
	CDMatrix delta_C( C.NRows(), C.NCols() );    
    CDVector c_j( C.NRows() );
    int k_st, k_end;	

    if (1) {

        // estimate k
	    k_st	= m_nParams;
	    k_end	= m_nExperiments;
    } else {

        // use hard-wired k
	    k_st	= m_nExperiments/2;
	    k_end	= m_nExperiments/2;
    }

	const double eps = 1e-10;	

	// naive linear search for the best k
	for(int k=k_st;k<=k_end;k++) {

		EigenVec_k.Resize( m_nExperiments, k );
        CDVector b_kj( k );

		// extract the k largest eigen vectors.
		m_mEigenVec.Submatrix( m_nExperiments, k, EigenVec_k, 0, m_mEigenVec.NCols()-k );        

        // calc Ek
		delta_C = C*EigenVec_k*EigenVec_k.Transposed();
        delta_C -= C;
		
		Ek = .0;
		for(int j=0;j<delta_C.NCols();j++) {

			delta_C.Col( j, c_j );

			// calc a_kj			
			EigenVec_k.Row(j, b_kj); // equal to: b_kj = EigenVec_k.Transposed().Column( j );
			double a_kj = b_kj.Norm2();
			a_kj*=a_kj;

			// calc f_kj
			if ( fabs(1.-a_kj) > eps ) {

                delta_C.Col( j, c_j );				
                double f_kj = 1./(1.-a_kj);
                double v=c_j.Norm2();
				Ek += f_kj*f_kj*v*v;				
			} else{

                C.Col( j, c_j );
				double v=c_j.Norm2();	
				Ek += v*v;				
			}			
		}		
		if (Ek<Ek_min) {
			            
			Ek_min = Ek;
			k_min = k;
		}
	}
	k=k_min;   

	// extract the k largest eigen vectors.
    EigenVec_k.Resize( m_nExperiments, k );		
	EigenVec_k = m_mEigenVec.Submatrix( m_nExperiments, k, 0, m_mEigenVec.NCols()-k );
    // swap cols
	CDVector tmp( EigenVec_k.NRows() );
	for(int r=0;r<EigenVec_k.NCols()/2;r++) {

		tmp = EigenVec_k.Column( r );
		int end = EigenVec_k.NCols()-r-1;

		EigenVec_k.SetColumn( r, EigenVec_k.Column( end ) );
		EigenVec_k.SetColumn( end, tmp );
	}

    
	// extract the k largest eigen values
	CDVector vEigenVal_k( k );
	for(int i=0;i<k;i++) vEigenVal_k[i] = m_vEigenVal[m_vEigenVal.Length()-i-1];

	// insert them into a diagonal matrix
	EigenVal_k.Resize( k, k );
	EigenVal_k.Diag( vEigenVal_k );

	return k;
}



/**

  @author   Mikkel B. Stegmann
  @version  3-15-2000

  @memo     Calculates the regression matrix, R.

  @doc      Calculates the regression matrix, R from the set of experiments
			in C and X, obtaining the relationship: C = RX.

            NOTE: Destroys X!!!
  
  @param    C	In the AAM case: the input parameters displacements.
  @param    X	In the AAM case: the input normalized pixel differences.
  @param	R	The output regression matrix.
  
  @return   k
  
*/
int CAAMLinearReg::DoRegression( const CDMatrix &C, const CDMatrix &X, CDMatrix &R ) {

	assert( C.NCols()==C.NCols() );
	
	m_nExperiments = X.NCols();
	m_nPixelsDiffs = X.NRows();
	m_nParams      = C.NRows();

	assert( m_nExperiments>m_nParams );

	// setup matrices
	CDMatrix EigenVec_k, EigenVal_k;	

    // debug
    printf("sizeof(X) = %.1f MB\n", m_nExperiments*m_nPixelsDiffs*sizeof(double)/(1024*1024.0) );
  
    
    // calc eigen vectors and values
    printf("Doing eigenvalue decomposition...\n");
    m_vEigenVal.Resize( m_nExperiments );
	m_mEigenVec.Resize( m_nExperiments, m_nExperiments );        
    bool useSVD = false;
    if (useSVD) {

        /////////////////////////////////////////////////////
	    // use SVD: extract the right singular vectors of X
        //          (notice the sorting of singular vectors
        //           is reversed to that of eigen values)
        /////////////////////////////////////////////////////
        CDMatrix dummy;
        VisDMatrixSVD(X, m_vEigenVal, dummy, m_mEigenVec, 0, 1 ); 
        m_vEigenVal.Sqr();
        m_vEigenVal.Reverse();
        m_mEigenVec.FlipLR();

    } else {

        /////////////////////////////////////
        // use ordinary eigen analysis
        /////////////////////////////////////                  
       
        // calc X^t by hand to avoid large VisSDK temporary objects
        int i, j, n = X.NRows(), m = X.NCols();
        CDMatrix X_t(m, n);    
        for (i = 0; i < m; i++)
            for (j = 0; j < n; j++)
                X_t[i][j] = X[j][i];  // not very cache-coherent on X :-(                      
        
        // do eigen analysis
        VisDMatrixSymmetricEigen( X_t*X, m_vEigenVal, m_mEigenVec );    
    }    
		
	// estimate optimal k value and calc 'EigenVec_k' and 'EigenVal_k'
    printf( "Estimating k... " );    
	int k = EstimateK( C, EigenVec_k, EigenVal_k );
    printf("k = %i\n", k );

	// calc R_k 
	R.Resize( m_nParams, m_nPixelsDiffs );

	// the line below is simlar to:
    //
    // R = C*EigenVec_k*(EigenVal_k.Inverted()*EigenVec_k.Transposed()*X.Transposed());
    //
    R = C*EigenVec_k*(EigenVal_k.Inverted()*(X*EigenVec_k).Transposed());
    
	return k;
}
