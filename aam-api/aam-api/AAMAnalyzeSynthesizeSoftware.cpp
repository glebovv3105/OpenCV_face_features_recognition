/* $Id: AAMAnalyzeSynthesizeSoftware.cpp,v 1.3 2003/01/17 12:02:08 aam Exp $ */
#include "AAMAnalyzeSynthesize.h"
#include "AAMShape.h"
#include "AAMUtil.h"
#include "DMultiBand.h"
#include "AAMReferenceFrame.h"
#include "HTimer.h"
#include "AAMWarp.h"



/**

  @author   Mikkel B. Stegmann
  @version  6-12-2002

  @memo     Constructor.

  @doc      Constructor.
  
  @param    rf		The reference frame to analyze and synthesize through.
  
  @return   Nothing.
  
*/
CAAMAnalyzeSynthesizeSoftware::CAAMAnalyzeSynthesizeSoftware( const CAAMReferenceFrame &rf ) :
    CAAMAnalyzeSynthesize(rf) {

    m_Id = asSoftware;

    BuildWarpTable();
}


/**

  @author   Mikkel B. Stegmann
  @version  6-12-2002

  @memo     Destructor.

  @doc      Destructor.
  
  @return   Nothing.
  
*/
CAAMAnalyzeSynthesizeSoftware::~CAAMAnalyzeSynthesizeSoftware() {



}



/**

  @author   Mikkel B. Stegmann
  @version  6-12-2002

  @memo     Cache method, that caches triangle info.

  @doc      This method cache triangle information that does not change.
  
  @return   Nothing.
  
*/
void CAAMAnalyzeSynthesizeSoftware::BuildWarpTable() {

    const CDMultiBand<TAAMPixel> &mask = m_pReferenceFrame->MaskImage();
    int width = mask.Width();
    int height = mask.Height();
    sWarpEntry e;
    bool found;

    // scan mask image 
    // NOTE: this is potentially unsafe, since we relies
    //       on CAAMReferenceFrame doing the exact same
    //       scanning
    for(int y=0;y<height;y++) {
        for(int x=0;x<width;x++) {

            if (mask.Pixel(x,y)==255) {

                // we're inside mask
                found = m_pReferenceFrame->FindTriangle( CAAMPoint(x,y), e.triangle, 
                                                         e.alpha, e.beta, e.gamma );
                assert( found );                               
                if ( found ) { m_WarpTable.push_back( e ); }
            }
        }
    }

    assert( m_WarpTable.size()==m_pReferenceFrame->NTextureSamples()/BANDS );
}



/**

  @author   Mikkel B. Stegmann
  @version  6-12-2002

  @memo     Sets the image to be analyzed.

  @doc      Sets the image to be analyzed. That reason for this, at first sight,
			somewhat clumpsy design is due to the OpenGL implementation.

  @param    img		Image to be analyzed.
  
  @return   Nothing.
  
*/
void CAAMAnalyzeSynthesizeSoftware::SetAnalyzeImage( const CDMultiBand<TAAMPixel> &img ) {

    m_pAnalyzeImage = &img;
}



/**

  @author   Mikkel B. Stegmann
  @version  6-12-2002

  @memo     Samples the texture under a given shape.

  @doc      This method samples the image intensities under a user-
			supplied shape into a texture vector.

			The image is set using the SetAnalyzeImage call.

			If a reference image of the shape is needed instead 
			call the alternative form of Analyze.
			
  @see      SetAnalyzeImage
  
  @param    shape				A shape in �mage coordinates.

  @param    texture				Output texture vector.

  @param	useInterpolation	If true bilinear interpolation is used (default).
								Otherwise the faster nearest neighbor interpolation
								is used.
								
  @return   True is the shape is inside the image.
  
*/
bool CAAMAnalyzeSynthesizeSoftware::Analyze( const CAAMShape &shape,
                                             CDVector &texture,
                                             const bool useInterpolation ) const {

    // setup
    double p[BANDS];
    TAAMPixel iP[BANDS];
    double x,y,x1,x2,x3,y1,y2,y3;
    const sWarpEntry *e;
    const CAAMTriangle *tri;    
    const std::vector<CAAMTriangle> &vTriangles = m_pReferenceFrame->RefMesh().Triangles();    
    int np = m_pReferenceFrame->NTextureSamples();
    int c = 0, n = m_WarpTable.size();
    if ( texture.Length()!=np ) { texture.Resize( np ); }
    assert( np/BANDS==n );    

    // warp    
    for(int i=0;i<n;i++) {

        // for each entry in the warp table
        e = &(m_WarpTable[i]);

        // get triangle
        tri = &(vTriangles[e->triangle]);

        // calculate position in the analyze image
        shape.GetPointUS( tri->V1(), x1, y1 );
        shape.GetPointUS( tri->V2(), x2, y2 );
        shape.GetPointUS( tri->V3(), x3, y3 );
        x = e->alpha*x1 + e->beta*x2 + e->gamma*x3;
	    y = e->alpha*y1 + e->beta*y2 + e->gamma*y3;			
        
        if (useInterpolation) {
        
            // do bilinear sampling
            m_pAnalyzeImage->Pixel1( (float)x, (float)y, p );                
            for(int k=0;k<BANDS;k++) { texture[c++] = p[k]; }        
        } else {

            // nearest neighbor
            m_pAnalyzeImage->get_pixel( (int)(x+.5), (int)(y+.5), iP );
            for(int k=0;k<BANDS;k++) { texture[c++] = iP[k]; }
        }
    }

    return true;
}



/**

  @author   Mikkel B. Stegmann
  @version  6-12-2002

  @memo     Samples the texture under a given shape.

  @doc      This method samples the image intensities under a user-
			supplied shape into a texture vector.

			The image is set using the SetAnalyzeImage call.

			If a texture vector of the shape is needed instead 
			call the alternative form of Analyze.
			
  @see      SetAnalyzeImage
  
  @param    shape				A shape in �mage coordinates.

  @param    refImg				Output reference image.

  @param	useInterpolation	If true bilinear interpolation is used (default).
								Otherwise the faster nearest neighbor interpolation
								is used.

								NOTE: This flag is actually ignored currently in
									  this OpenGL implementation.
  
  @return   True is the shape is inside the image.
  
*/
bool CAAMAnalyzeSynthesizeSoftware::Analyze( const CAAMShape &shape, 
                                             CDMultiBand<TAAMPixel> &refImg,
                                             const bool useInterpolation ) const {

    // setup
    CDVector texture;
    
    Analyze( shape, texture );

    // convert texture vector to reference image
    m_pReferenceFrame->Vector2Image( texture, refImg );    


    return true;
}



/**

  @author   Mikkel B. Stegmann
  @version  6-12-2002

  @memo     Renders a texture vector into a shape.

  @doc      This method renders a texture vector into a shape 
			defined in image coordinates.
  
  @param    shape				The shape to synthesize into.

  @param    texture				The input texture vector in byte range [0;255].

  @param	destImage			Destination image

  @param	renderOntoImage		If true the synthesization is done on top of the 
								existing image.
  
  @return   True on success.
  
*/
bool CAAMAnalyzeSynthesizeSoftware::Synthesize( const CAAMShape &shape, 
                                                const CDVector &texture,
                                                CDMultiBand<TAAMPixel> &destImage,
                                                bool renderOntoImage ) const {

    CDMultiBand<TAAMPixel> refImage;       

    // make shape free image
    m_pReferenceFrame->Vector2Image( texture, refImage );
    
    
    // mirror the edge to avoid warped points that falls 
    // just outside the reference image mask
    CAAMUtil::MirrorEdge( refImage, m_pReferenceFrame->MaskImage(), 1 );        
     
    
    double shapeWidth  = shape.Width();
    double shapeHeight = shape.Height();
    if (!renderOntoImage) {

        // init destination image
        destImage = CDMultiBand<TAAMPixel>( (int)shapeWidth+1, (int)(shapeHeight+1), 
                                            BANDS, evisimoptDontAlignMemory );        
    }

    // config
    //bool add_noise = renderOntoImage;
	bool add_noise = false;
    double noise_amplitude;
    if (add_noise) { 
        
        noise_amplitude = .1*texture.Mean();    // add 10% of the texture mean
    }

    // setup warp
    CAAMWarpLinear warp(false);

    warp.UseConvexHull( m_pReferenceFrame->UseConvexHull() );
    warp.SetSrcShape( shape );
    warp.SetDestShape( m_pReferenceFrame->RefShape() );

    // misc setup
    CAAMPoint out;
    double p[BANDS];
    double xmin = shape.MinX(), ymin = shape.MinY();    
    int refWidth   = refImage.Width();
    int refHeight  = refImage.Height();
    int offsetX    = renderOntoImage ? AAMRound(xmin) : 0;
    int offsetY    = renderOntoImage ? AAMRound(ymin) : 0;

    // correct for subpixel placement of shape in the destination image
    int srcOffsetX = (int)(renderOntoImage ? xmin-(xmin-offsetX) : xmin); 
    int srcOffsetY = (int)(renderOntoImage ? ymin-(ymin-offsetY) : ymin);

    // warp pixels
    for(int y=0;y<=shapeHeight;y++) {

        for(int x=0;x<=shapeWidth;x++) {

            bool ok = warp.Warp( CAAMPoint( x+srcOffsetX, y+srcOffsetY ), out );
            if (ok) {

                // pixel is inside shape

                // skip pixels at the edge to save
                // the bilinear interpolation from a slow
                // and painful death
                if ( (int)(out.x)>=refWidth-1  || 
                     (int)(out.y)>=refHeight-1 ||
                     (int)(out.x)==0           ||
                     (int)(out.y)==0    ) {

                    // skip
                    continue;
                }


                // do bilinear sampling
                refImage.Pixel1( (float)out.x, (float)out.y, p );

                if (add_noise && renderOntoImage) {

                    // add monochromatic noise                    
                    double r = noise_amplitude*(rand()/(double)RAND_MAX-.5);
                    for(int k=0;k<BANDS;k++) {
                        
                        double val = p[k]+r;
                        p[k] = val>255. ? 255. : (val<0. ? 0. : val);
                    }
                }

                // write the result back to the destination image
                for(int k=0;k<BANDS;k++) { 
                    destImage.Pixel( x+offsetX, y+offsetY, k ) = (unsigned char)p[k]; 
                }                        
            }
        }
    }

    return true;
}



/**

  @author   Mikkel B. Stegmann
  @version  6-7-2002

  @memo     Clones ifself (smart way to convey type info).

  @doc      Clones ifself (smart way to convey type info).
  
  @param    rf		The reference frame of the cloned object.
  
  @return   A cloned object created on the heap.
  
*/
CAAMAnalyzeSynthesize *CAAMAnalyzeSynthesizeSoftware::Clone( const CAAMReferenceFrame &rf ) const { 

    CAAMAnalyzeSynthesizeSoftware *obj;
    
    obj = new CAAMAnalyzeSynthesizeSoftware( rf );      

    return obj;
}