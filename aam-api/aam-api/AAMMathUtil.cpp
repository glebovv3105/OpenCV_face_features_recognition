/* $Id: AAMMathUtil.cpp,v 1.2 2003/01/17 12:02:08 aam Exp $ */
#include "AAMMathUtil.h"


// define the pi constant if needed
#ifndef M_PI
	#define M_PI 3.141592653589793				 
#endif /* M_PI */


/**

  @author   Mikkel B. Stegmann
  @version  1-25-2002

  @memo     Calculates the histogram.

  @doc      Calculates the histogram of a vector within [min;max]
            with optional nomalization. 
  
  @param    v           Input vector.  

  @param    min         Value for the first bin (values lower are clamped to min).

  @param    max         Value for the last bin (values lower are clamped to max).

  @param    hist        Output histogram vector.

  @param    nbins       The number of bins. Default 256.

  @param    normalize   Optional normalization, i.e. make the histogram entries sum to 1.
                        Default true.

  @param    transform   Optional transformation of v into 'nbins' integer intervals between [min;max].
                        Default false.
  
  @return   Nothing.
  
*/
void CAAMMathUtil::Hist( 	CDVector &v, const double min, const double max, 
           					CDVector &hist, const int nbins, bool normalize, 
           					bool transform  ) {

    hist.Resize(nbins);

    hist = .0;
    double val, mag = max-min; 
    unsigned int bin, len = v.Length();    
    for(unsigned int i=0;i<len;i++) {

        val = v[i];
        val = val > max ? max : (val<min ? min : val);                
        bin = (unsigned int)(.5+(nbins-1)*(val-min)/mag);
        ++hist[bin];
        if (transform) v[i] = bin;                
    }

    if (normalize) {

        hist /= len;
    }
}



/**

  @author   Mikkel B. Stegmann
  @version  1-25-2002

  @memo     Cumulative sum of elements.

  @doc      Cumulative sum of elements.  
  
  @param    v           Input vector.

  @param    cumsum      Output vector containing the cumulative sum of elements.

  @param    normalize   Optional normalization of output, i.e. cumsum[end] = 1.0.
                        Default true.
  
  @return   Nothing.   
  
*/
void CAAMMathUtil::CumSum( const CDVector &v, CDVector &cumsum, bool normalize ) {

    int len=v.Length();
    cumsum.Resize(len);
    cumsum[0] = v[0];
    for(int i=1;i<len;i++) {

        cumsum[i] = cumsum[i-1] + v[i];
    }

    if (normalize) {

        cumsum /= cumsum[len-1];
    }
}



/**

  @author   Mikkel B. Stegmann
  @version  1-22-2002

  @memo     Calulates the Mutual Information (MI) between two vectors.

  @doc      Calulates the Mutual Information (MI) between two vectors.
            See e.g. C. Studholme et al. or Viola et al.
    
  @param    v1      Signal 1.
  @param    v2      Signal 2.
  @param    nbins   The number of bins in the single and joint histograms.
                    Default 256.

  @return   The Mutual Information (MI).
  
*/
double CAAMMathUtil::MutualInformation( const CDVector &v1, const CDVector &v2, int nbins ) {

    assert( v1.Length()==v2.Length() );
    
    double v1min, v1max, v2min, v2max;
    v1min = v1.Min();
    v1max = v1.Max();
    v2min = v2.Min();
    v2max = v2.Max();
    int v1bin, v2bin, len = v1.Length();    

    // creat hists and reset them
    CDVector hist1( nbins);
    CDVector hist2( nbins);        
    CDMatrix hist12( nbins, nbins );
    hist1  = .0;
    hist2  = .0;
    hist12 = .0;

    // make and normalize hists
    double mag1 = v1max-v1min;
    double mag2 = v2max-v2min;
    for(int i=0;i<len;i++) {

        v1bin = (int)(.5+(nbins-1)*(v1[i]-v1min)/mag1);
        v2bin = (int)(.5+(nbins-1)*(v2[i]-v2min)/mag2);

        ++hist1[v1bin];
        ++hist2[v2bin];
        ++hist12[v1bin][v2bin];
    }
    hist1  /= len;
    hist2  /= len;
    hist12 /= len;

    // calculate entropy of the signals 
    double eps = 2.220446049250313e-016;
    double hi, H1=.0, H2=.0, log2 = log(2);    
    for(i=0;i<nbins;i++) {

        hi = hist1[i]; H1 -= hi*log(hi+eps)/log2;
        hi = hist2[i]; H2 -= hi*log(hi+eps)/log2;
    }
    // calculate joint entropy
    double H12=.0;
    for(i=0;i<nbins;i++) {
        for(int j=0;j<nbins;j++) {

            hi = hist12[i][j]; 
            H12 -= hi*log(hi+eps)/log2;
        }
    }
      
    // calculate the mutual information
    return H1+H2-H12;

}



/**

  @author   Mikkel B. Stegmann
  @version  1-25-2002

  @memo     Maps the distibution of v into an approximate Gaussian distribution.

  @doc      Maps the distibution of v into an approximate Gaussian distribution.
  
  @param    v           Input vector.
  
  @param    out         Output vector.

  @param    nbins       Number of bins used to approx the distribution. 
                        Default 256.

  @param    gaussLUT    If not null, the calulated LUT is returned here.

  
  @return   Nothing.
  
*/
void CAAMMathUtil::GaussianHistogramMatching( const CDVector &v, CDVector &out, 
                                              const int nbins, CDVector *gaussLUT ) {

    int v_len = v.Length();
    out.Resize(v_len);
    CDVector gauss(nbins);
    CDVector gauss_accum(nbins);
    CDVector x(nbins);

    // calc gauss frq
    double k = 1.0/sqrt(2*M_PI);
    double tail = 3.5;
    for(int i=0;i<nbins;i++) {

        x[i] = tail*( 2.*i/(nbins-1.) - 1.);
        gauss[i] = k*exp(-.5*x[i]*x[i]);
    }

    // calc accum gauss dist
    CumSum( gauss, gauss_accum, true );    

    // make hist of incoming vector
    CDVector hist, accum_hist;
    out = v;
    Hist( out, out.Min(), out.Max(), hist, nbins, true, true );
    CumSum( hist, accum_hist, true );

    // make a lookup table that maps 'hist' into 
    // a gaussian distribution
    CDVector gausslut(nbins);        
    int binhit = 0;
    for(i=0;i<nbins;i++) {
       
        // use monotonicity and find the closest class in
        // the accumulative gaussian distribution         
        while( fabs(accum_hist[i]-gauss_accum[binhit+1]) < 
               fabs(accum_hist[i]-gauss_accum[binhit])    ) {

            ++binhit;
        }

        // map the i-th class of v into x 
        gausslut[i] = x[binhit];        
    }       

    // transform 
    for(i=0;i<v_len;i++) {

        out[i] = gausslut[(int)(out[i])];
    }

    // copy gausslut (if specified)
    if (gaussLUT) {

        gaussLUT->Resize(gausslut.Length());
        *gaussLUT = gausslut;
    }
}


/**

  @author   Mikkel B. Stegmann
  @version  4-24-2002

  @memo     Expands a matrix to have dyadic size.

  @doc      This method expands a matrix to have dyadic size, i.e.
			nrows and ncols that are powers of two.

			Expansion are done using zero-padding.
  
  @param    m		Input matrix.

  @param    dyad	Output dyadic version.
  
  @return   Nothing.
  
*/
void CAAMMathUtil::ExpandMatrix2DyadicSize( const CDMatrix &m, CDMatrix &dyad ) {

    // calc output size   	
	int new_r, new_c;
	new_c = (int)pow(2, ceil( log(m.NCols())/log(2) ) );
	new_r = (int)pow(2, ceil( log(m.NRows())/log(2) ) );
	dyad.Resize( new_r, new_c );       	
    dyad = .0;

    // copy data    
	for(int r=0;r<m.NRows();r++) {

		for(int c=0;c<m.NCols();c++) {                        

            dyad[r][c] = m[r][c];
		}
	}		
}



/**

  @author   Mikkel B. Stegmann
  @version  3-22-2000

  @memo     Calculates the variance of each component in a set of vectors.
  
  @param    cVectors	Input set of vectors.

  @param	varVec		A vector containing the variance of each compoment
						in cVectors.

  @param    vpMean      Optional vector pointer to return the mean vector in.
 
  @return   Nothing.
  
*/
void CAAMMathUtil::CalcElementVar( const std::vector<CDVector> &vVectors, 
	 							   CDVector &varVec, CDVector *vpMean ) {
    
	assert( vVectors.size()>0 );	
    if (vpMean) {

        vpMean->Resize( vVectors[0].Length() );
    }

	CDVector elemVec( vVectors.size() );
	varVec.Resize( vVectors[0].Length() );

	for(int elem=0;elem<vVectors[0].Length();elem++) {

		for(unsigned int vecNb=0;vecNb<vVectors.size();vecNb++) {
			
			elemVec[vecNb] = vVectors[vecNb][elem];
		}
        double mean;
		varVec[elem] = elemVec.Var( &mean );
        if (vpMean) {

            (*vpMean)[elem] = mean;
        }
	}    
}


/**

  @author   Mikkel B. Stegmann
  @version  4-25-2002

  @memo     Maps a vector linearly from [min;max] to [new_min;new_max].

  @doc      Maps a vector linearly from [min;max] to [new_min;new_max].

  @see      LinearStretchClamp
  
  @param    v           Input vector.
  @param    new_min     Desired minimum.
  @param    new_max     Desired maximum.
  
  @return   Nothing.
  
*/
void CAAMMathUtil::LinearStretchMinMax( CDVector &v, 
                                        const double new_min,
                                        const double new_max ) {
       
    double min = v.Min();
    double max = v.Max();

    if (max==min) {

        // the vector is constant -> do nothing
        return;
    }
    
	// map [min;max] linear to [new_min;new_max]
    double mul = (new_max-new_min)/(max-min);    
	for(int i=0;i<v.Length();i++) {
		            
		v[i] = mul*(v[i]-min)+new_min;
	}
}



/**

  @author   Mikkel B. Stegmann
  @version  4-25-2002

  @memo     Maps a vector linearly from [x1;x2] to [new_min;new_max].

  @doc      Maps a vector linearly from [x1;x2] to [new_min;new_max]. 
            Values outside [x1;x2] are clamped to x1, x2 respectively.
  
  @see      LinearStretchMinMax
  
  @param    v           Input vector.
  @param    x1          Starting point of stretch.
  @param    x2          Ending point of stretch.
  @param    new_min     Desired minimum.
  @param    new_max     Desired maximum.
    
  @return   Nothing.
  
*/
void CAAMMathUtil::LinearStretchClamp( CDVector &v, 
                                       const double x1, const double x2,
                                       const double new_min, const double new_max ) {

    assert( x1!=x2 );

    if (x1==x1) {

        // constant vector is reqested -> do nothing
        return;
    }
    
	// map the vector linearly from [x1;x2] to [new_min;new_max]
    double mul = (new_max-new_min)/(x2-x1);
    double val;
	for(int i=0;i<v.Length();i++) {

		val  = v[i];
		v[i] = val>x2 ? new_max : val<x1 ? new_min : (val-x1)*mul+new_min;
	}
}



/**

  @author   Mikkel B. Stegmann
  @version  5-15-2002

  @memo     Performs a 3x3 mean filtering of a matrix.

  @doc      Performs a 3x3 mean filtering of a matrix.  
  
  @param    in      Input matrix.

  @param    out     Output mean filtered matrix.
  
  @return   Nothing.
  
*/
void CAAMMathUtil::MeanFilter( const CDMatrix &in, CDMatrix &out ) {

    out.Resize( in.NRows(), in.NCols() );
    out = .0;
    
    // top
    for(int c=1;c<out.NCols()-1;c++) {
        
        int r=0;                        
        out[r][c] += in[r][c-1];
        out[r][c] += in[r][c];
        out[r][c] += in[r][c+1];
        out[r][c] += in[r+1][c-1];
        out[r][c] += in[r+1][c];
        out[r][c] += in[r+1][c+1];
        out[r][c] /= 6;
    }
    // bottom
    for(c=1;c<out.NCols()-1;c++) {
        
        int r=out.NRows()-1; 
        out[r][c] += in[r-1][c-1];
        out[r][c] += in[r-1][c];
        out[r][c] += in[r-1][c+1];
        out[r][c] += in[r][c-1];
        out[r][c] += in[r][c];
        out[r][c] += in[r][c+1];        
        out[r][c] /= 6;
    }
    // left
    for(int r=1;r<out.NRows()-1;r++) {

        int c=0;        
        out[r][c] += in[r-1][c];
        out[r][c] += in[r-1][c+1];        
        out[r][c] += in[r][c];
        out[r][c] += in[r][c+1];        
        out[r][c] += in[r+1][c];
        out[r][c] += in[r+1][c+1];
        out[r][c] /= 6;
    }
    // right
    for(r=1;r<out.NRows()-1;r++) {

        int c=out.NCols()-1;        
        out[r][c] += in[r-1][c-1];
        out[r][c] += in[r-1][c];        
        out[r][c] += in[r][c-1];
        out[r][c] += in[r][c];        
        out[r][c] += in[r+1][c-1];
        out[r][c] += in[r+1][c];        
        out[r][c] /= 6;
    }
    // rest
    for(r=1;r<out.NRows()-1;r++) {
        for(int c=1;c<out.NCols()-1;c++) {
            
            out[r][c] += in[r-1][c-1];
            out[r][c] += in[r-1][c];
            out[r][c] += in[r-1][c+1];
            out[r][c] += in[r][c-1];
            out[r][c] += in[r][c];
            out[r][c] += in[r][c+1];
            out[r][c] += in[r+1][c-1];
            out[r][c] += in[r+1][c];
            out[r][c] += in[r+1][c+1];
            out[r][c] /= 9;
        }
    }        
}



/**

  @author   Mikkel B. Stegmann
  @version  6-12-2002

  @memo     Normalises a vector to zero mean and unit length.

  @doc      Normalises a vector to zero mean and unit length.
  
  @param    v	Input vector.
  
  @return   Nothing. The result is returned in v.
  
*/
void CAAMMathUtil::ZeroMeanUnitLength( CDVector &v ) {      
    
    double sqsum = .0, mean = v.Mean();            
    int n = v.Length();
    
    for(unsigned int i=0;i<n;i++) {

        v[i]  -= mean; 
        sqsum += v[i]*v[i];
    }
    double a = 1./sqrt(sqsum);

    for(i=0;i<n;i++) {

        v[i] *= a;
    }    
}



/**

  @author   Mikkel B. Stegmann
  @version  7-19-2002

  @memo     Calculates the pseudo inverse of a matrix.

  @doc      Calculates the pseudo inverse of a matrix.  
  
  @param    A   Input matrix.

  @param    P   Pseudo inverse of A.
  
  @return   Nothing.
  
*/
void CAAMMathUtil::PseudoInv( const CDMatrix &A, CDMatrix &P ) {
    
    int m = A.NRows();
    int n = A.NCols();
    CDMatrix U(m,n), V(n,n), SI;
    CDVector s(n);  
    
    VisDMatrixSVD( A, s, U, V, 1, 1 );

    // construct SI              
    SI.Resize( n, n );
    SI = .0;
    double si,eps = 1e-16;

    // a tolerance setting that fell down from matlab heaven
    double tolerance = max( m, n ) * s.Max() * eps;

    for(int i=0;i<n;i++) {

        si = s[i];       
        SI[i][i] = si>tolerance ? 1./si : .0;
    }

    // calc pinv(A)
    //
    // beware: potentially very large temporary objects ahead(!)
    // any zero-multiplications below could also be avoided
    //
    P = V*SI*U.Transposed();
}