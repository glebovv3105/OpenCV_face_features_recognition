/* $Id: AAMWarp.cpp,v 1.2 2003/01/14 17:28:51 aam Exp $ */
#include "AAMWarp.h"
#include "AAMDelaunay.h"


/**

  @author   Mikkel B. Stegmann
  @version  4-26-2000

  @memo     Constructor. 

  @doc      Constructor.
  
  @param    useSrcDelaunay	If true the Delaunay of the source shape is used.
    
  @return   Nothing.
  
*/
CAAMWarpLinear::CAAMWarpLinear( bool useSrcDelaunay ) : 

    m_PreviousTriangleHit(0),
    m_bUseSrcDelaunay(useSrcDelaunay) {
	    
}



/**

  @author   Mikkel B. Stegmann
  @version  4-26-2000

  @memo     Destructor.

  @doc      Destructor.    
  
  @return   Nothing.
  
*/
CAAMWarp::~CAAMWarp() { }



/**

  @author   Mikkel B. Stegmann
  @version  4-26-2000

  @memo     Sets the shape to warp from. 

  @doc      Sets the shape to warp from.  
  
  @return   Nothing.
  
*/
void CAAMWarp::SetSrcShape(const CAAMShape &s) {	

    m_SrcShape = s;

	// get src shape extents
	m_dSrcShapeMinX = s.MinX();
	m_dSrcShapeMaxX = s.MaxX();
	m_dSrcShapeMinY = s.MinY();
	m_dSrcShapeMaxY = s.MaxY();	
}


/**

  @author   Mikkel B. Stegmann
  @version  4-26-2000

  @memo     Sets the shape to warp from. 
  
  @doc      Sets the shape to warp from. 

  @param    s   Input shape.    
  
  @return   Nothing.
  
*/void CAAMWarpLinear::SetSrcShape(const CAAMShape &s) {

	// get src shape extents
	CAAMWarp::SetSrcShape( s );

	// delete any old content in the mesh member variable
	m_Mesh.Clear();

    if (m_bUseSrcDelaunay) {

	    // make a mesh using a Delaunay Triangulation.
	    CAAMDelaunay::MakeMesh( s, m_Mesh, !m_bUseConvexHull );                
    }
}



/**

  @author   Mikkel B. Stegmann
  @version  4-26-2000

  @memo     Sets the shape to warp to.
  
  @doc      Sets the shape to warp to.
  
  @param    s   Input shape.  
  
  @return   Nothing.
  
*/
void CAAMWarpLinear::SetDestShape(const CAAMShape &s) {

	int nbPoints = s.NPoints();		

	// ensure that the mesh and the point set 
	// has an equal amount of points
	assert(m_bUseSrcDelaunay==false || nbPoints==m_Mesh.NPoints());

	// delete any old destination points
	m_vDestPoints.clear();		

	// add the destination points
	for(int i=0;i<nbPoints;i++) {
					
		m_vDestPoints.push_back( s.GetPoint( i ) );
	}

    if (m_bUseSrcDelaunay==false) {

        // make a mesh using a Delaunay Triangulation.        	    
        CAAMDelaunay::MakeMesh( s, m_Mesh, !m_bUseConvexHull );                        
        m_Mesh.ReplacePoints( m_SrcShape );        
    }
}


/**

  @author   Mikkel B. Stegmann
  @version  4-26-2000

  @memo     Warps point 'in' to point 'out' (if possible).

  @doc      Warps point 'in' to point 'out' (if possible).  
  
  @param    in   Input point.
  @param    out  Output point.  
  
  @return   True if 'in' is inside the source mesh, false if not.
  
*/
bool CAAMWarpLinear::Warp(const CAAMPoint &in, CAAMPoint &out) const {
	
	double alpha, beta, gamma;
	int triangle;	

    bool bFound = m_Mesh.IsInside( in, triangle, alpha, beta, gamma );

    if (!bFound) { return false; } 

	// get triangle 
	const CAAMTriangle &tri = m_Mesh.Triangles()[triangle];

    // calculate warped point	
	out.x = alpha*m_vDestPoints[tri.V1()].x + 
			beta* m_vDestPoints[tri.V2()].x + 
			gamma*m_vDestPoints[tri.V3()].x;
	out.y = alpha*m_vDestPoints[tri.V1()].y + 
			beta* m_vDestPoints[tri.V2()].y + 
			gamma*m_vDestPoints[tri.V3()].y;		

    return true;
}
