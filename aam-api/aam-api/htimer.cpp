/* $Id: htimer.cpp,v 1.1.1.1 2003/01/03 19:17:12 aam Exp $ */
#include "htimer.h"
#include <math.h>

#if defined( _WIN32 ) || defined( _WINDOWS_ )
	#include <windows.h>
	//Intersante makroer defines
	//_WIN32 Defined for applications for Win32�. Always defined.  
	//_MSC_VER Defines the compiler version. Defined as 1200 for Microsoft Visual C++ 6.0. Always defined. 

	//Thanks prophecy. Real inline
	#pragma inline_depth(255)
	#pragma inline_recursion(on)
	#pragma auto_inline(on)

#endif //if _WIN32

	

typedef	signed char			int8;
typedef	signed short		int16;
typedef signed int			int32;
typedef signed __int64		int64;
typedef	unsigned char		uint8;
typedef	unsigned short		uint16;
typedef unsigned int		uint32;
typedef unsigned __int64	uint64;
typedef uint8				byte;
typedef	uint16				word;
typedef uint32				dword;
typedef uint64				qword;

static LARGE_INTEGER	largeInteger;



CHTimer::CHTimer(bool startNow)
{
	mIsRunning = false;
	mTime = 0;
	mHighPerformanceTimer = QueryPerformanceFrequency(&largeInteger)!=0;
	if(mHighPerformanceTimer)
	{
		mFrequency = (double)largeInteger.QuadPart;
	}else
	{
		mFrequency = 1000;
	}
	if(startNow) start();
}

void CHTimer::start()
{
	if(mIsRunning) return;
	mIsRunning = true;
	if(mHighPerformanceTimer)
	{
		QueryPerformanceCounter(&largeInteger);
		mHighPerformanceTimerStartCount = (double)largeInteger.QuadPart;
	}else
	{
		mStartTick = GetTickCount();
	}
}
void CHTimer::stop()
{
	if(!mIsRunning) return;
	if(mHighPerformanceTimer)
	{
		double currentCount;
		QueryPerformanceCounter(&largeInteger);
		currentCount = (double)largeInteger.QuadPart;
		mTime += (currentCount-mHighPerformanceTimerStartCount)/mFrequency;
	} else
	{
		mTime += ((double)(GetTickCount()-mStartTick))/mFrequency;
	}
	mIsRunning=false;
}

void CHTimer::reset()
{
	mTime = 0;
	if(mHighPerformanceTimer)
	{
		QueryPerformanceCounter(&largeInteger);
		mHighPerformanceTimerStartCount = (double)largeInteger.QuadPart;
	}else
	{
		mStartTick = GetTickCount();
	}
}

double CHTimer::getTime() const	// seconds
{
	if(!mIsRunning) return mTime;
	if(mHighPerformanceTimer)
	{
		double currentCount;
		QueryPerformanceCounter(&largeInteger); 
		currentCount = (double)largeInteger.QuadPart;
		return mTime + (currentCount-mHighPerformanceTimerStartCount)/mFrequency;
	} else
	return mTime + ((double)(GetTickCount()-mStartTick))/mFrequency;
}

