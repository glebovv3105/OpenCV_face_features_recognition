/* $Id: AAMVisualizer.cpp,v 1.2 2003/01/17 14:45:13 aam Exp $ */
#include "AAMVisualizer.h"
#include "AAMMovie.h"
#include "AAMDelaunay.h"
#include "AAMModel.h"
#include "AAMReferenceFrame.h"




/**

  @author   Mikkel B. Stegmann
  @version  5-8-2002

  @memo     Constructor.

  @doc      Constructor that sets the model pointer.
  
  @param    pModel	Pointer to the model that you want visualised.
  
  @return   Nothing.
  
*/
CAAMVisualizer::CAAMVisualizer( const CAAMModel *pModel ) : m_pModel(pModel) {
	  
}



/**

  @author   Mikkel B. Stegmann
  @version  5-8-2002

  @memo     Destructor.
  
  @doc      Destructor.
  
  @return   Nothing.
  
*/
CAAMVisualizer::~CAAMVisualizer() {
	    
}



/**

  @author   Mikkel B. Stegmann
  @version  5-7-2002

  @memo     Visualizes the iterations during a model search.

  @doc      This method visualizes the iterations during a model search.
			Output format is an AVI-file that you can put into e.g. 
			Powerpoint to impress your supervisor with.
  
  @param    optStates	An array of optimization states.

  @param    img			The image used for model searching.

  @param	filename	The output movie filename, e.g. 'search.avi'.

  @param	frameRate	The number of frame per second.
  
  @return   Nothing.
  
*/
void CAAMVisualizer::OptimizationMovie( const std::vector<CAAMModel::CAAMOptState> &optStates,
                                        const CDMultiBand<TAAMPixel> &img,
                                        const char *filename, const int frameRate ) const {

    CAAMMovieAVI avi;
    CDMultiBand<TAAMPixel> frame( img.Width(), img.Height(), BANDS, evisimoptDontAlignMemory );
    CVisImage<CVisRGBABytePixel> rgba( img.Width(), img.Height(), 1, evisimoptDontAlignMemory	);
    int n=optStates.size();

    // open avi    
	avi.Open( filename, OF_CREATE|OF_WRITE );

    // render frames
    for(int i=0;i<n;i++) {
    					
		img.CopyPixelsTo( frame );							        
		m_pModel->ModelImage( optStates[i].c, frame, &optStates[i].shape, true );
		frame.CopyPixelsToRGBA( rgba );		
		avi.WriteFrame( rgba );			
	}

    // close avi
	avi.Close();
}



/**

  @author   Mikkel B. Stegmann
  @version  5-7-2002

  @memo     Plots a shape into an image and save it to disk.
  
  @doc      Plots a shape into an image and save it to disk.
  
  @param    img		Image to plot shape onto.

  @param    shape	Shape in image coordinates.

  @param	fileman	Destination image file, e.g. 'result.bmp'.
  
  @return   Nothing.
  
*/
void CAAMVisualizer::ShapeStill( const CDMultiBand<TAAMPixel> &img,
                                 const CAAMShape &shape,
                                 const char *filename ) {

    CDMultiBand<TAAMPixel> resImage( img.Width(), img.Height(), BANDS, evisimoptDontAlignMemory );
	img.CopyPixelsTo( resImage );		
    TAAMPixel p255[BANDS];
    SPAWN( p255, 255)
	CAAMUtil::PlotShapeIntoImage( resImage, shape, p255, p255, false, false, false );		
	resImage.WriteBandedFile( filename, "bmp" );						
}



/**

  @author   Mikkel B. Stegmann
  @version  2-26-2002

  @memo     Writes the texture eigen modes as shape-free images.

  @doc      This method converts each column of the texture eigen
			vectors into corresponding shape-free images and save
			them to disk in the BMP format.
    
  @return   Nothing.
  
*/
void CAAMVisualizer::WriteEigenImages() const {       	

    int nmodes = m_pModel->TexturePCA().NParameters();
    CDMultiBand<TAAMPixel> img;   
    CDVector texture;
    for(int i=0;i<nmodes;i++) {

        m_pModel->TexturePCA().EigenVectors().Col(nmodes-i-1,texture);	// take largest first        
        m_pModel->ShapeFreeImage( texture, img );
        CString str;
        str.Format("eigen_image%02i.bmp", i );
        img.WriteBandedFile( str );       				
    }
}



/**

  @author   Mikkel B. Stegmann
  @version  2-26-2002

  @memo     Writes the parameter update matrix as shape-free images.
  
  @doc      This method converts each column of the parameter update
			matrices into corresponding shape-free images and save
			them to disk in the BMP format.
  
  @return   Nothing.
  
*/
void CAAMVisualizer::WritePredictionImages() const {    
    
    CString str; 
    
    CDMultiBand<TAAMPixel> img;   
    CDVector texture;
    int n = m_pModel->Rc().NRows();
    for(int i=0;i<n;i++) {

        m_pModel->Rc().Row(n-i-1, texture); // take largest first        
        m_pModel->ShapeFreeImage( texture, img );
        str.Format("Rc_image%02i.bmp", i );
        img.WriteBandedFile( str );       				
    }
    n = m_pModel->Rt().NRows();
    for(i=0;i<n;i++) {

        m_pModel->Rt().Row(i, texture);
        m_pModel->ShapeFreeImage( texture, img );
        str.Format("Rt_image%02i.bmp", i );
        img.WriteBandedFile( str );       				
    }
}



/**

  @author   Mikkel B. Stegmann
  @version  2-28-2000

  @memo     Generates movies files showing each mode of texture variation.
  @doc      Generates movies files showing each mode of texture variation by
			adjusting the each mode of variation to 
			
			  +/- 'range' times <the mode standard deviation>

			in 2*'step'+1 steps.

  @see      Shape, Combined
    
  @param	filename	The output filename excl. extension.

  @param	nbModes		The number of modes to generate movies from.

  @param	range       The rendered range in standard deviations.	

  @param	steps		The number of frames to generate to reach 'range'.
						Thus the total number of frames will be 2*step+1.
						(the +1 term is the mean texture).

  @param    whiteBackground If true the background is rendered in white.
                            Default is black.
  
  @return   true on success, false on file errors.
  
*/
bool CAAMVisualizer::TextureMovie( const CString &filename, const int nbModes, 
                                   const double range, const int steps,
                                   const bool whiteBackground ) const {

	bool success = true;
	CDVector texVec;
	double min, max, pVal;
	int nbAllModes = m_pModel->TexturePCA().NParameters();
	CDVector b_g(nbAllModes), c;
	int nFrames = steps*2+1;	

	for( int i=nbAllModes-1;i>=nbAllModes-min(nbModes,nbAllModes);i--) {

        double ev_sqr = sqrt( m_pModel->TexturePCA().EigenValues()[i] );
		min = -range*ev_sqr;
		max =  range*ev_sqr;

		CString fn;
		fn.Format("%s%02i.avi", filename, nbAllModes-i );
		CAAMMovieAVI avi;
        avi.SetFrameRate( 8 );

		try {

			avi.Open( fn, OF_CREATE|OF_WRITE );
			for(int x=0;x<nFrames;x++) {
				
				pVal = (double)x/(nFrames-1)*(max-min)+min;			                
				b_g = .0;
				b_g[i] = pVal;

				CDMultiBand<TAAMPixel> frame( m_pModel->ReferenceFrame().RefImageWidth(),
                                              m_pModel->ReferenceFrame().RefImageHeight(),
                                              BANDS,
                                              evisimoptDontAlignMemory);								
                if (whiteBackground) { frame.FillPixels( 255 ); }
				m_pModel->TexturePCA().Deform( b_g, texVec );							

				// do the sampling
				m_pModel->ShapeFreeImage( texVec, frame );				

				CVisImage<CVisRGBABytePixel> rgba( frame.Width(), frame.Height() );
				frame.CopyPixelsToRGBA( rgba );
				avi.WriteFrame( rgba );
			}
			avi.Close();
		}
		catch (CVisFileIOError e) {
				
			AfxMessageBox( e.FullMessage() ); 
			success = false;
		}
	}

	return success;
}



/**

  @author   Mikkel B. Stegmann
  @version  2-28-2000

  @memo     Generates movies files showing each mode of shape variation.

  @doc      Generates movies files showing each mode of shape variation by
			adjusting the each mode of variation to 
			
			  +/- 'range' times <the mode standard deviation>

			in 2*'step'+1 steps.

  @see      Texture, Combined
  
  @param	filename	The output filename excl. extension.

  @param	nbModes		The number of modes to generate movies from.

  @param	range       The rendered range in standard deviations.

  @param	steps		The number of frames to generate to reach 'range'.
						Thus the total number of frames will be 2*step+1.
						(the +1 term is the mean shape).

  @param    whiteBackground If true the background is rendered in white.
                            Default is black.
  
  @return   true on success, false on file errors.
  
*/
bool CAAMVisualizer::ShapeMovie( const CString &filename, const int nbModes, 
                                 const double range, const int steps,
                                 const bool whiteBackground ) const {

	bool success = true;
	double min, max, pVal;
	int nbAllModes = m_pModel->ShapePCA().NParameters();
	CDVector b_s(nbAllModes), c;
    CAAMShape shape, s1, s2;	
	int nFrames = steps*2+1;

	// denormalize mean texture
	CDVector meantex( m_pModel->MeanTexture().Length() );	
    meantex = m_pModel->MeanTexture();  
   
    // setup    
    int border = 0;
	
	for( int i=nbAllModes-1;i>=nbAllModes-min(nbModes,nbAllModes);i--) {

        double ev_sqr = sqrt( m_pModel->ShapePCA().EigenValues()[i] );
		min = -range*ev_sqr;
		max =  range*ev_sqr;

        // calc shape bounding box
        b_s = .0; 
        b_s[i] = min;					        		
		m_pModel->ShapePCAInstance( b_s, s1 );
        s1.Scale( m_pModel->MeanShapeSize() );
        b_s[i] = max;
        m_pModel->ShapePCAInstance( b_s, s2 );
        s2.Scale( m_pModel->MeanShapeSize() );
        double xMin = min( s1.MinX(), s2.MinX() );
        double xMax = max( s1.MaxX(), s2.MaxX() );
        double yMin = min( s1.MinY(), s2.MinY() );
        double yMax = max( s1.MaxY(), s2.MaxY() );
        int width  = (int)ceil(xMax-xMin) + 2*border;
        int height = (int)ceil(yMax-yMin) + 2*border;        

		try {

			CString fn;
			fn.Format("%s%02i.avi", filename, nbAllModes-i );
			CAAMMovieAVI avi;
            avi.SetFrameRate( 8 );
			avi.Open( fn, OF_CREATE|OF_WRITE );

			for(int x=0;x<nFrames;x++) {
				
				pVal = (double)x/(nFrames-1)*(max-min)+min;			                
				b_s = .0;
				b_s[i] = pVal;	
								
				m_pModel->ShapePCAInstance( b_s, shape );							
				shape.Scale( m_pModel->MeanShapeSize() );
                shape.Translate( -xMin+border, -yMin+border );

				CDMultiBand<TAAMPixel> frame( width, height, BANDS, evisimoptDontAlignMemory );						
                if (whiteBackground) { frame.FillPixels( 255 ); }

				m_pModel->ModelImageEx( shape, meantex, frame, true, false );              						
                           
				CVisImage<CVisRGBABytePixel> rgba( width, height );
				frame.CopyPixelsToRGBA( rgba );
				avi.WriteFrame( rgba );
			}
			avi.Close();
		}
		catch (CVisFileIOError e) {
					
				AfxMessageBox( e.FullMessage() ); 
				success = false;
		}
	}
	
	return success;
}



/**

  @author   Mikkel B. Stegmann
  @version  2-25-2000

  @memo     Generates movies files showing each mode of combined variation.
  @doc      Generates movies files showing each mode of combined shape and
			texture variation by adjusting the each mode of variation to:
			
			  +/- 'range' times <the mode standard deviation>

			in 2*'step'+1 steps.

  @see      Texture, Shape
  
  @param	filename	The output filename excl. extension.

  @param	nbModes		The number of modes to generate movies from.

  @param	range       The rendered range in standard deviations.

  @param	steps		The number of frames to generate to reach 'range'.
						Thus the total number of frames will be 2*step+1.
						(the +1 term is the mean shape).

  @param    whiteBackground If true the background is rendered in white.
                            Default is black.
  
  @return   true on success, false on file errors.
  
*/
bool CAAMVisualizer::CombinedMovie( const CString &filename, const int nbModes, 
                                    const double range, const int steps,
                                    const bool whiteBackground ) const {

	bool success = true;
	double min, max, pVal;
	int nbAllModes = m_pModel->CombinedPCA().NParameters();
	CDVector c(nbAllModes);
	int nFrames = steps*2+1;
    CAAMShape shape, s1, s2;	

    // setup
    int border = 0;

	for( int i=nbAllModes-1;i>=nbAllModes-min(nbModes,nbAllModes);i--) {

        double ev_sqr = sqrt( m_pModel->CombinedPCA().EigenValues()[i] );
		min = -range*ev_sqr;
		max =  range*ev_sqr;

        // calc shape bounding box
        c = .0; 
        c[i] = min;					        		
		m_pModel->ShapeInstance( c, s1 );
        s1.Scale( m_pModel->MeanShapeSize() );
        c[i] = max;
        m_pModel->ShapeInstance( c, s2 );
        s2.Scale( m_pModel->MeanShapeSize() );
        double xMin = min( s1.MinX(), s2.MinX() );
        double xMax = max( s1.MaxX(), s2.MaxX() );
        double yMin = min( s1.MinY(), s2.MinY() );
        double yMax = max( s1.MaxY(), s2.MaxY() );
        int width  = (int)ceil(xMax-xMin) + 2*border;
        int height = (int)ceil(yMax-yMin) + 2*border;               

		try {

			CString fn;
			fn.Format("%s%02i.avi", filename, nbAllModes-i );
			CAAMMovieAVI avi;
            avi.SetFrameRate( 8 ); 
			avi.Open( fn, OF_CREATE|OF_WRITE  );

			for(int x=0;x<nFrames;x++) {

				c = 0;
				pVal = (double)x/(nFrames-1)*(max-min)+min;			                
				c[i] = pVal;


				CDMultiBand<TAAMPixel> frame( width, height, BANDS, evisimoptDontAlignMemory );						
                if (whiteBackground) { frame.FillPixels( 255 ); }


                m_pModel->ShapeInstance( c, shape );                
                shape.Scale( m_pModel->MeanShapeSize() );
                shape.Translate( -xMin+border, -yMin+border );
				
                
                m_pModel->ModelImage( c, frame, &shape, false );				               				


				CVisImage<CVisRGBABytePixel> rgba( width, height );
				frame.CopyPixelsToRGBA( rgba );
				avi.WriteFrame( rgba );
			}
			avi.Close();
		}
		catch (CVisFileIOError e) {
			
			AfxMessageBox( e.FullMessage() ); 
			success = false;
		}
	}

	return success;
}



/**

  @author   Mikkel B. Stegmann
  @version  2-22-2000

  @memo     Generates a movie of the registered training set.

  @doc      Generates a movie of the registered training set by warping
			each texture to the mean shape and output this to an AVI-file.
  
  @see      CombinedMovie, TextureMovie, ShapeMovie
  
  @param    filename	The output filename.  

  @param    m_vTexture  The vector of textures. 
  
  @return   true on succes, false on file errors.
  
*/
bool CAAMVisualizer::RegistrationMovie( const CString &filename, 
                                        const std::vector<CDVector> &vTexture ) const {

	bool success = true;
	CAAMShape meanShape;

	try {
		// generate shapefree images and write to avi
		CAAMMovieAVI avi;
		avi.Open( filename, OF_CREATE|OF_WRITE  );
		for(unsigned int i=0;i<vTexture.size();i++) {

			// generate the i-th frame
			CDMultiBand<TAAMPixel> frame;
			CDVector tmp( vTexture[i] );			

		
			// do the sampling
			m_pModel->ShapeFreeImage( tmp, frame );


			// convert and write frame
			CVisImage<CVisRGBABytePixel> rgbImage( frame.MemoryRect() );		
			frame.CopyPixelsToRGBA( rgbImage );		
			avi.WriteFrame( rgbImage );
		}
	}
	catch (CVisFileIOError e) {
		
		AfxMessageBox( e.FullMessage() ); 
		success = false;
	}

	return success;
}
