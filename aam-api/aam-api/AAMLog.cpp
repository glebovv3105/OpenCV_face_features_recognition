/* $Id: AAMLog.cpp,v 1.2 2003/01/14 17:28:51 aam Exp $ */
#include "AAMLog.h"
#include <stdarg.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>


// static memeber initializations
FILE *CAAMLog::m_file_hndl = NULL;
char *CAAMLog::m_logfile = "";
int CAAMLog::m_init = 0;

// array of strings containing the severity levels. Used in the logfile.
static char INFOLEVEL[4][20] = {	{"INFO"}, 
									{"WARNING"}, 
									{"ERROR"}, 
									{"FATAL"}	};



/**

  @author   Hans P. Palb�l & Mikkel B. Stegmann
  @version  3-5-2001

  @memo     Default constructor.

  @doc      Default constructor.   
  
  @param    service     Name of the calling class or function.  

  @param    level       The minimum level that will be reported.
  
  @return   Nothing.
  
*/
CAAMLog::CAAMLog( const char *service, const eLogLevel level )
{
	// remember which service subscribed to the info service
	m_service = new char[strlen(service)+1];
	strcpy( m_service, service );

	// default is INFO which essentially means that all messages will show
	m_level = level;
}


/**

  @author   Hans P. Palb�l & Mikkel B. Stegmann
  @version  3-5-2001

  @memo     Basic initialization.

  @doc      Basic initialization. Sets up the optional log file.
  
  @param    filename    The log filename. If   
  
  @return   Nothing.
  
*/
void CAAMLog::InitLogFile( const char *filename )
{
	if( m_init ) {
		Printf( WARNING, "already initialized!" );	
		return;
	}

	// set logfile name
	CAAMLog::m_logfile = new char[strlen(filename)+1];
	strcpy( CAAMLog::m_logfile, filename );
	Printf( INFO, "Starting log service" );

	m_init = 1;
}


/**

  @author   Hans P. Palb�l & Mikkel B. Stegmann
  @version  3-5-2001

  @memo     Prints a message with the given severity level.

  @doc      Prints a message with the given severity level.
  
  @param    level   Severity level of the incoming message.
  @param    message Format string in printf() format.
  @param    ...     Optional arguments for the printf() format string.
  
  @return   Nothing.
  
*/
void CAAMLog::Printf( eLogLevel level, char *message, ... )
{
	va_list ap;	

	char str[128];
	sprintf( str, "[%s:%s] %s\n", m_service, INFOLEVEL[level], message );

	if( level >= m_level ) {

		va_start( ap, message );

		// send message to console
		vfprintf( stdout, str, ap );

		va_end( ap );
	
		if ( strlen(CAAMLog::m_logfile)>0 ) {

            // send message to logfile
			char timestr[127];
			char buf[128];
			GetTime( timestr );
			sprintf( buf, "%s%s", timestr, str );
		
		    CAAMLog::m_file_hndl = fopen( CAAMLog::m_logfile, "a+" );
		    fwrite( buf, sizeof(char), strlen(buf), CAAMLog::m_file_hndl );
		    fclose( CAAMLog::m_file_hndl );
		}
	}
}


/**

  @author   Hans P. Palb�l
  @version  3-5-2001

  @memo     Returns the current (local) time in the string 'str'.

  @doc      Returns the current (local) time in the string 'str'.
            Format: "hh:mm:ss".  
  
  @param    str     Output string. Must be pre-allocated.  
  
  @return   Nothing.
  
*/
void CAAMLog::GetTime( char *str )
{
	tm my_tm;
	time_t my_time;
	time( &my_time );
	my_tm = *localtime( &my_time );
	strftime( str, 127, "%H:%M:%S ", &my_tm );
}


/**

  @author   Hans P. Palb�l
  @version  3-5-2001

  @memo     Sets the minimum severity level.

  @doc      Sets the minimum severity level. All messages at this level and above
            are reported.
  
  @param    str     Output string. Must be pre-allocated.  
  
  @return   Nothing.
  
*/
void CAAMLog::SetLevel( eLogLevel level )
{
	m_level = level;
}
