/* $Id: AAMDeform.cpp,v 1.3 2003/04/23 14:50:00 aam Exp $ */
#include "AAMDeform.h"
#include "AAMUtil.h"
#include "AAMMathUtil.h"


/**

  @author   Mikkel B. Stegmann
  @version  10-17-2000

  @memo     Returns the eigenvalue of the i-th parameter.

  @doc      Returns the eigenvalue of the i-th parameter in absolute 
            numbers or as percentage.

  @see      ParameterWeightOrg
    
  @return   The eigenvalue.
  
*/
double CAAMDeformPCA::ParameterWeight( const int i, bool asPercentage ) const {

    double ret = m_vEigenValues[i];

    if (asPercentage) {
        
        ret /= m_vEigenValues.Sum();
    } 

    return ret;
}



/**

  @author   Mikkel B. Stegmann
  @version  10-17-2000

  @memo     Returns the eigenvalue of the i-th parameter.

  @doc      Returns the eigenvalue of the i-th parameter in absolute 
            numbers or as percentage.

  @see      ParameterWeightOrg
    
  @return   The eigenvalue.
  
*/
double CAAMDeformPCA::ParameterWeightOrg( const int i, bool asPercentage ) const {

    double ret = m_vEigenValuesOrg[i];

    if (asPercentage) {
        
        ret /= m_vEigenValuesOrg.Sum();
    } 

    return ret;
}



/**

  @author   Mikkel B. Stegmann
  @version  10-17-2000

  @memo     Inserts a data vector.

  @doc		Inserts a data vector.
  
  @param    v	A data vector.
  
  @return   Nothing.
  
*/
void CAAMDeformPCA::InsertDataItem( const CDVector &v ) {

    m_vvData.push_back( v );
}



/**

  @author   Mikkel B. Stegmann
  @version  10-30-2002

  @memo     Makes the object use an identity basis instead of PCA basis.

  @doc      Makes the object use an identity basis instead of PCA basis, i.e.
			essentially a by-pass of this object.

  @return   Nothing.
  
*/
void CAAMDeformPCA::UseIdentityTransformation() {


    // get nb samples in each data item
    int nbSamples   = m_vvData[0].Length();
    int nbDataItems = m_vvData.size();

    // we need at least one data item to do this
    assert(nbDataItems>0);   

    // calc the mean data item
    m_vDataMean.Resize(nbSamples);
    m_vDataMean = 0;
    for(int i=0;i<nbDataItems;i++) {
        
        m_vDataMean += m_vvData[i];
    }
    m_vDataMean /= nbDataItems;

    
    CAAMMathUtil::CalcElementVar( m_vvData, m_vEigenValues );
    m_vEigenValuesOrg = m_vEigenValues;       

    // set the eigenvectors to the identity matrix
    m_mEigenVectors.Resize( nbSamples, nbSamples );
    m_mEigenVectors.Eye();
       
    // everything is now ready
    m_bValid = true;

    // return without further ado
    return;       
}



/**

  @author   Mikkel B. Stegmann
  @version  10-17-2000

  @memo     Performs the principal component analysis.

  @doc      Performs the principal component analysis on the data
            items. Uses the Eckhart-Young theorem if necessary for
            reduced memory and computational requirements.
  
  @param    bWriteCoVarMarix    If true the covariance matrix is written
                                to the current dir.
                                NOTE: Only in the case of more data items
                                than dimensions (samples).
   
  @return   Nothing.
  
*/
void CAAMDeformPCA::DoPCA( bool bWriteCoVarMarix ) {    

	// get nb samples in each data item
    int nbSamples   = m_vvData[0].Length();
    int nbDataItems = m_vvData.size();

    // we need at least one data item to do this
    assert(nbDataItems>0);

    // handle the training set of size 1 situation
    //
    // in the AAM case this would lead to a model
    // containing pure rigid-body deformation
    // parameters (i.e. only pose parameters would
    // be present)
    //
    if (nbDataItems==1) {

        // the hacker solution: 
        // to add one 'null'-mode that don't deform the data
        //
        // this is due to the fact that all code expects
        // at least one deformation parameter
        //
        // sorry about this hack, but this seem as the
        // most clean solution anyway, otherwise all 
        // code would be bloated with checks on the number
        // of parameters in the deformation model        
        //
        m_vEigenValues.Resize(1);
        m_vEigenValuesOrg.Resize(1);        

        m_vEigenValues = 1;
        m_vEigenValuesOrg = 1;

        m_vDataMean = m_vvData[0];

        m_mEigenVectors.Resize( nbSamples, 1 );
        m_mEigenVectors = .0;        

        // everything is now ready
        m_bValid = true;

        // return without further ado
        return;
    }

    // calc the mean data item
    m_vDataMean.Resize(nbSamples);
    m_vDataMean = 0;
    for(int i=0;i<nbDataItems;i++) {
        
        m_vDataMean += m_vvData[i];
    }
    m_vDataMean /= nbDataItems;

    // my apologies for the difference in coding
    // in the two if-branches below, they were 
    // originally from two different classes
    // I'll rewrite it some day... :-)
    if (nbDataItems>nbSamples) {

        // more observations than data dimensions
        // -> do a normal PCA        

	    // calculate the covariance matrix
        CDVector x_i( nbSamples );
        CDMatrix dx_i( nbSamples, 1 );
        CDMatrix covarMatrix(  nbSamples,  nbSamples );
        covarMatrix = .0;
	    for(i=0;i<nbDataItems;i++) {

		    x_i  = m_vvData[i]-m_vDataMean;			
		    dx_i.SetColumn( 0, x_i );
		    covarMatrix +=  dx_i*dx_i.Transposed();
	    }
	    covarMatrix /= nbDataItems;	

        // write the covariance matrix to the current dir
        if (bWriteCoVarMarix) {

		    covarMatrix.ToMatlab(	"pca_covar.m", 
                                    "pca_cv",
			    					"The covariance matrix of a training set.", 
                                    false );
        }

	    // calculate eigenvalues and eigenvectors
	    m_vEigenValues.Resize( nbSamples );
	    m_mEigenVectors.Resize( covarMatrix.NRows(), covarMatrix.NCols() );
	    VisDMatrixSymmetricEigen( covarMatrix, m_vEigenValues, m_mEigenVectors);	    

    } else {

        // more observations than data dimensions
        // -> use the Eckhart-Youngh theorem in the PCA
        // see also the appendix of Cootes' AAM-report
       	CDMatrix D, T, e_i;
	    CDVector x_i;        
	    	    
	    // build D
	    D.Resize(  nbSamples, nbDataItems );	
	    x_i.Resize( nbSamples );
	    for(int i=0;i<nbDataItems;i++) {

		    x_i = m_vvData[i]-m_vDataMean;
		    D.SetColumn( i, x_i );
	    }

	    // build T
	    T.Resize( nbDataItems, nbDataItems );
	    T = D.Transposed()*D;
	    T /= nbDataItems;

	    // calculate eigenvalues and eigenvectors of the 'small' matrix T
	    m_vEigenValues.Resize( nbDataItems );
	    e_i.Resize( nbDataItems, nbDataItems );	
	    VisDMatrixSymmetricEigen( T, m_vEigenValues, e_i );

	    // transform into the eigen vectors of 'm_mEigenVectors'
	    m_mEigenVectors.Resize( nbSamples, nbDataItems );
	    m_mEigenVectors = D*e_i;

	    // normalize the pseudo eigenvectors in 'm_mEigenVectors'
	    CDVector tmp( nbSamples );
	    for(i=0;i<nbDataItems;i++) {

		    m_mEigenVectors.Col(i,tmp);		
		    tmp.Normalize2();
		    m_mEigenVectors.SetColumn(i,tmp);
	    }
    }

    // remove 'zero' eigen-values
	// that is eigenvalues that contribute 
	// less than 0.01% to the total variation
    //
    // this is *not* considered a truncation since 
    // eigenvalues are *very* close to zero
    //
	double limit =.0001*m_vEigenValues.Sum(); 
	for(i=0;i<m_vEigenValues.Length();i++) {

		if (m_vEigenValues[i]>limit)
			break;
	}
	int NZeroEV = i;
	int NNonZeroEV = m_vEigenValues.Length()-NZeroEV;	

	CDVector v(NNonZeroEV);
	for(i=0;i<NNonZeroEV;i++) {

		v[i] = m_vEigenValues[i+NZeroEV];
	}
	m_vEigenValues.Resize(NNonZeroEV);
	m_vEigenValues = v;	

	CDMatrix truncEVEC;        
    truncEVEC.Resize( m_mEigenVectors.NRows(), NNonZeroEV );
	truncEVEC = m_mEigenVectors.Submatrix( truncEVEC.NRows(), 
											truncEVEC.NCols(),
											0,
											NZeroEV );	
	m_mEigenVectors.Resize( truncEVEC.NRows(), truncEVEC.NCols() );
    m_mEigenVectors = truncEVEC;

    // copy to org eigenvalues -> no VEL truncation (yet)
    m_vEigenValuesOrg.Resize( m_vEigenValues.Length() );
    m_vEigenValuesOrg = m_vEigenValues;
    
    // everything is now ready
    m_bValid = true;
}


/**

  @author   Mikkel B. Stegmann
  @version  10-17-2000

  @memo     Returns the sample mean.

  @doc      Returns the mean of all sample vectors.
    
  @return   A mean vector.
  
*/
const CDVector &CAAMDeformPCA::MeanDataItem() const {

    return m_vDataMean;
}



/**

  @author   Mikkel B. Stegmann
  @version  10-18-2000

  @memo     Writes a PCA object to file.

  @doc      Writes a PCA object to a binary file.
  
  @see		FromFile
  
  @param    fh	File handle to binary file open for writing.
  
  @return   Nothing.
  
*/
void CAAMDeformPCA::ToFile( FILE *fh ) const {

    if (m_bValid==false) {

        throw CVisError( "Can't save data. Call DoPCA() first.",
                         eviserrorUnknown, "ToFile", __FILE__, __LINE__ );
    }

    if (fh==NULL) {

        throw CVisError( "fh==Null.",
                         eviserrorUnknown, "ToFile", __FILE__, __LINE__ );
    }

    m_mEigenVectors.ToFile( fh );
    m_vEigenValues.ToFile( fh );
    m_vEigenValuesOrg.ToFile( fh );
    m_vDataMean.ToFile( fh );
}



/**

  @author   Mikkel B. Stegmann
  @version  10-18-2000

  @memo     Reads a PCA object from file.

  @doc      Reads a PCA object from a binary file.
  
  @see		ToFile
  
  @param    fh	File handle to binary file open for reading.
  
  @return   Nothing.
  
*/
void CAAMDeformPCA::FromFile( FILE *fh ) {

    if (fh==NULL) {

        throw CVisError( "fh==Null.",
                         eviserrorUnknown, "FromFile", __FILE__, __LINE__ );
    }

    m_mEigenVectors.FromFile( fh );
    m_vEigenValues.FromFile( fh );    
    m_vEigenValuesOrg.FromFile( fh );    
    m_vDataMean.FromFile( fh );

    m_bValid = true;
}



/**

  @author   Mikkel B. Stegmann
  @version  10-18-2000

  @memo     Writes eigen vectors and eigen values
            to matlab text files: 
            
            pca_eigenvectors.m
            pca_eigenvalues.m
            pca_eigenvalues_org.m

  @doc      Debug method that provides a human readable
            file dump of the object data.

  @param    szPath  Path including terminating backslash   
                    where the data is dumped.
    
  @return   Nothing.  

  @throws   VisError
  
*/
void CAAMDeformPCA::Dump( const char *szPath ) const {

    CString str;

    str = CString(szPath)+ CString("pca_eigenvectors.m");
    m_mEigenVectors.ToMatlab( str, "pca_evec", "Column eigenvectors from CAAMDeformPCA.", false );
	
    str = CString(szPath)+ CString("pca_eigenvalues.m");
    m_vEigenValues.ToMatlab( str, "pca_ev", "Eigenvalues from CAAMDeformPCA.", false );

    str = CString(szPath)+ CString("pca_eigenvalues_org.m");
	m_vEigenValuesOrg.ToMatlab( str, "pca_ev_org", "Original eigenvalues from CAAMDeformPCA.", false );
}


/**

  @author   Mikkel B. Stegmann
  @version  10-18-2000

  @memo     Projects a set of PC scores to the original space.

  @doc      Projects a set of PC scores to the original space.
  
  @param    params	PC scores, i.e. the model paramerisation.

  @param    object	Resulting projection into the original space, e.g. a shape.
  
  @return   Nothing.
  
*/
void CAAMDeformPCA::Deform( const CDVector &params, CDVector &object ) const {

    object.Resize( m_mEigenVectors.NRows() );    
    object = m_vDataMean+m_mEigenVectors*params;
}



/**

  @author   Mikkel B. Stegmann

  @version  6-11-2000

  @memo     Returns the Mahalanobis distance of a set of PCA parameters.

  @doc      Returns the Mahalanobis distance of a set of PCA parameters.
  
  @param	params	A set of PCA parameters.  
 
  @return   The Mahalanobis distance.
  
*/
double CAAMDeformPCA::MahalanobisDistance( const CDVector &params ) const {

	double maha = .0;

    assert( params.Length()==m_vEigenValues.Length() );

	for(int i=0;i<params.Length();i++) {
		
		
		if ( !(m_vEigenValues[i]<1e-15) ) {	// exclude near-zero eigenvalues

			maha += params[i]*params[i]/m_vEigenValues[i];			
		}
		
	}

	return maha;
}



/**

  @author   Mikkel B. Stegmann
  @version  7-17-2002

  @memo     Shuffle each dimension in all data items.

  @doc      Shuffle each dimension in all data items over all observations.
  
  @return   Nothing.
  
*/
void CAAMDeformPCA::ShuffleData() {

    int p = m_vvData[0].Length();
    int n = (int)m_vvData.size();
    CDVector indicies;
    CDVector shuffledIndicies(n);
    indicies.Linspace( 0, n-1, n );
    double tmp;

    // for each dimension
    for(int i=0;i<p;i++) {

        // make shuffled indicies
        shuffledIndicies = indicies;
        shuffledIndicies.Shuffle();

        // apply shuffling over all observations
        for(int j=0;j<n;j++) {

            // suffled index
            int jj = (int)shuffledIndicies[j];

            // swap 'j' and 'jj'
            tmp = m_vvData[j][i];
            m_vvData[j][i] = m_vvData[jj][i];
            m_vvData[jj][i] = tmp;
        }
    }
}


/**

  @author   Mikkel B. Stegmann
  @version  7-15-2002

  @memo     Truncate eigenvectors and eigenvalues using parallel analysis.

  @doc      Truncate eigenvectors and eigenvalues using parallel analysis.            
            
            Also known as Horn's parallel analysis or Humphrey-Ilgen 
            parallel analysis. 

            Assumes the data items have not been cleared.

  @see      TruncateVar
    
  @return   The number of parameters in the truncated basis. 
  
*/
int CAAMDeformPCA::TruncateParallel() {

    //
    //  below: a poor mans version of PA, i.e. we don't keep
    //         keep track of the permutations, instead we are
    //         just doing random samples within each dimension
    //         across all data items
    //
    assert(m_vvData.size()>0);
    
    int p = m_vvData[0].Length();
    int n = (int)m_vvData.size();
    int nEV = m_vEigenValues.Length();
    CDVector v(p);
    CDVector simulatedEV(nEV);
    simulatedEV = .0;
    
    // set-up the number of Monte Carlo simulations
    int nSimulations = 10;

    for(int s=0;s<nSimulations;s++) {

        // create PCA object
        CAAMDeformPCA pca;    

        // copy data
        pca.m_vvData = this->m_vvData;

        // shuffle data
        pca.ShuffleData();

        // calc PCA
        pca.DoPCA();
        pca.ClearDataItems();
        
        // accumulate simulated eigenvalue into 'simulatedEV'
        const CDVector &simEV = pca.EigenValues();
        for(int e=0;e<min(simEV.Length(), nEV);e++) {
            
            simulatedEV[e] += simEV[e];
        }
    }

    // calc mean eigenvalues
    simulatedEV /= nSimulations;
           
    // dermine PA cut-off (largest eigenvalues are last)
    int paCutOff = 0;
    for(int i=nEV-1;i>=0;i--) {

        if (m_vEigenValues[i]>=simulatedEV[i]) {

            ++paCutOff;
        } else {

            break;
        }
    }

    // force at least one mode
    paCutOff = paCutOff==0 ? 1 : paCutOff;
   
    // debug    
    m_vEigenValues.ToMatlab( CAAMUtil::FindVacantFilename("data_ev.m"), "dev", "", false );
    simulatedEV.ToMatlab( CAAMUtil::FindVacantFilename("sim_ev.m"), "sev", "", false );

    // truncate eigenvectors/values
    this->Truncate( paCutOff );

    // return the retain number of modes   
    return paCutOff;
}



/**

  @author   Mikkel B. Stegmann
  @version  6-11-2000

  @memo    Truncate eigenvectors and eigenvalues to retain
		   'retained_variance'. 
          
  @doc     Truncate eigenvectors and eigenvalues to retain
		   'retained_variance'. 

           I.e. if  retained_variance = .95 this function calculates how
		   many eigenvectors we need to explain 95% of the total
		   variation in the PCA training set.     
  
  @param   retained_variance   Amount of variance to retain [0<x<1].  
  
  @return  The number of parameters in the truncated basis. 
  
*/
int CAAMDeformPCA::TruncateVar( const double retained_variance ) {

    // truncate eigenvectors and eigenvalues to
	// satisfy the variance explanation level constraint
	int n = CalcNParam(retained_variance);
   
    Truncate( n );

    return n;
}



/*
  @author   Mikkel B. Stegmann
  @version  6-11-2000

  @memo    Truncate eigenvectors and eigenvalues.
          
  @doc     Truncate eigenvectors and eigenvalues to retain
		   the 'n' largest.           
  
  @param   n   Amount of eigenvectors/values to retain.
  
  @return  Nothing.
  
*/
void CAAMDeformPCA::Truncate( const int n ) {

    // truncate eigenvectors and eigenvalues	
	int cufoff = NParameters()-n;

	CDMatrix truncEVEC( m_mEigenVectors.NRows(), n );
	truncEVEC = m_mEigenVectors.Submatrix( truncEVEC.NRows(), 
							   			   truncEVEC.NCols(),
										   0,
										   cufoff );	
	m_mEigenVectors.Resize( truncEVEC.NRows(), truncEVEC.NCols() );
	m_mEigenVectors = truncEVEC;
	
	CDVector truncEV( n );
	for(int i=0;i<n;i++) {

		truncEV[i] = m_vEigenValues[i+cufoff];
	}
	m_vEigenValues.Resize( truncEV.Length() );
	m_vEigenValues = truncEV;    
}

 

/**

  @author   Mikkel B. Stegmann
  @version  6-5-2000

  @memo     Calulates the needed number of parameters.

  @doc      Calulates the needed number of parameters to retain
			'retained_variance'. I.e. if 
			retained_variance = .95 then this function calculates how
			many eigenvectors we need to explain 95% of the total
			variation in the PCA training set.  

			Assumes that no eigenvalue cutoff has been done prior to the
			call.

 @param    retained_variance   Amount of variance to retain [0<x<1].
    
 @return   The number of model parameters.

*/
int CAAMDeformPCA::CalcNParam( const double retained_variance ) const {


	double sum = EigenValues().Sum();
	double ps = .0;
	int np =0;

	for(int i=NParameters()-1;i>=0;i--) {

		ps += EigenValues()[i];
		++np;
        if( ps/sum>=retained_variance) break;		
	}

	return np;
}



/**

  @author   Mikkel B. Stegmann
  @version  3-6-2003

  @memo     Projects an observation into the PCA space.

  @doc      Projects an observation into the PCA space.

			Notice: Costly, due to the non-cached transpose 
			of the eigenvectors.

  @see      BackProject, Filter
  
  @param    obs		Input observation.

  @param    param	Output model parameters of the (possibly truncated)
					basis.
  
  @return   Nothing.
  
*/
void CAAMDeformPCA::Project( const CDVector &obs, CDVector &param ) const {

	param.Resize( NParameters() );
	param = this->m_mEigenVectors.Transposed() * (obs-m_vDataMean);
}


/**

  @author   Mikkel B. Stegmann
  @version  3-6-2003

  @memo     Back projects a set of PCA model parameters into the original space.

  @doc		Back projects a set of PCA model parameters into the original space.
  
  @see		Project, Filter  
  
  @param    param		Input PCA model parameters.

  @param    synth_obs	Synthesized output observation.
  
  @return   Nothing.
  
*/
void CAAMDeformPCA::BackProject( const CDVector &param, CDVector &synth_obs ) const {


	const int n = m_vDataMean.Length();

	synth_obs.Resize( n );

	synth_obs  = m_vDataMean;
	synth_obs += m_mEigenVectors*param;
}


/**

  @author   Mikkel B. Stegmann
  @version  3-6-2003

  @memo     Projects an observation into the PCA space and back.

  @doc      Projects an observation into the PCA space and back.

			Notice: Costly, due to the non-cached transpose 
			of the eigenvectors in the project call.
  
  @see      Project, BackProject
  
  @param    obs		Input observation.
  
  @return   Nothing.
  
*/
void CAAMDeformPCA::Filter( CDVector &obs ) const {

	CDVector param;

	this->Project( obs, param );
	this->BackProject( param, obs );
}