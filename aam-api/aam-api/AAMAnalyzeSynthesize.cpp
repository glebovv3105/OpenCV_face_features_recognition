/* $Id: AAMAnalyzeSynthesize.cpp,v 1.3 2003/01/17 14:45:13 aam Exp $ */
#include "AAMAnalyzeSynthesize.h"


/**

  @author   Mikkel B. Stegmann
  @version  6-13-2002

  @memo     Reads an object from disk.

  @doc      Reads an object form disk. Don't use this function 
			directly. Use AAMLoadAnalyzerSynthesizer.

  @see		AAMLoadAnalyzerSynthesizer
  
  @param    fh	Open file handle.
  
  @return   Nothing.
  
*/
void CAAMAnalyzeSynthesize::FromFile( FILE *fh ) {

    unsigned int id;
    fread((void*)&id,sizeof(unsigned int),1,fh);
    m_Id = (eASid)id;	
}
 

/**

  @author   Mikkel B. Stegmann
  @version  6-13-2002

  @memo     Writes an object to disk.

  @doc      Writes an object to disk. 

  @see		AAMLoadAnalyzerSynthesizer
  
  @param    fh	Open file handle.
  
  @return   Nothing.
  
*/
void CAAMAnalyzeSynthesize::ToFile( FILE *fh ) const {

    unsigned int id;

    id = (unsigned int)m_Id;
    fwrite((void*)&id,sizeof(unsigned int),1,fh);   
}


/**

  @author   Mikkel B. Stegmann
  @version  6-13-2002

  @memo     Analyzer/Synthesizer loader.

  @doc      The function loads an Analyzer/Synthesizer from a stream,
			instantiates the correct concrete class and returns a
			base class pointer.
			  
  @param    fh		Open binary stream.

  @param    pModel	Model pointer (currently not used).
  
  @return   A pointer to an Analyzer/Synthesize object created on the heap.
  
*/
CAAMAnalyzeSynthesize *AAMLoadAnalyzerSynthesizer( FILE *fh, const CAAMReferenceFrame &rf ) {

    CAAMAnalyzeSynthesize *pAS;
    unsigned int id;
    fread((void*)&id,sizeof(unsigned int),1,fh);
    eASid as_id = (eASid)id;
    
    // unwind stream
    fseek( fh, -(int)(sizeof(unsigned int)), SEEK_CUR );
    
    switch (as_id) {

        case asSoftware:  pAS = new CAAMAnalyzeSynthesizeSoftware( rf ); 
                          break;

        case asOpenGL:    pAS = new CAAMAnalyzeSynthesizeOpenGL( rf ); 
                          break;        
        
        default:          printf("Error: AAMLoadAnalyzerSynthesizer(): Unknown class id, NULL returned(!)\n");
                          return NULL;
                          break;
    }

    pAS->FromFile( fh );

    return pAS; 
}