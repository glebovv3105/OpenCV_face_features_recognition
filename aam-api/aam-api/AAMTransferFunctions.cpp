/* $Id: AAMTransferFunctions.cpp,v 1.2 2003/01/17 12:02:09 aam Exp $ */
#include "AAMTransferFunctions.h"
#include "AAMModel.h"
#include "AAMMathUtil.h"


/**

  @author   Mikkel B. Stegmann
  @version  1-28-2002

  @memo     Loads the lookup table and generates the inverse.

  @doc      Loads the lookup table and generates the inverse,
			i.e. assumes that the LUT monotonic.

  @param    lut		A lookup table in vector form.
  
  @return   Nothing.
  
*/
void CAAMTFLookUp::LoadLUT( const CDVector &lut ) {

    int len = lut.Length();
    m_LUT.Resize( len );
    m_LUT = lut;       

    // approximate inverse lut
    double min = m_LUT.Min();
    double max = m_LUT.Max();
    double mag = max-min;

    double val, min_dist, dist;
    int min_pos;
    m_InvLUT.Resize( len );
    for(int i=0;i<len;i++) {

        val = mag*i/(len-1.0)+min;
        min_dist = 1e306;        
        for(int j=i;j<len;j++) {

            dist = fabs(m_LUT[j]-val );
            if (dist<min_dist) {
                min_pos  = j;
                min_dist = dist;
            }            
        }
        m_InvLUT[i] = min_pos;
    }

    m_InvLUT.ToMatlab( "invlut.m", "il", "", false );
}


/**

  @author   Mikkel B. Stegmann
  @version  1-28-2002

  @memo     Maps an input vector using the current lookup table.

  @doc      Maps an input vector using the current lookup table.
  
  @param    v	Input vector.
  
  @return   Nothing. The result is returned in v.
  
*/
void CAAMTFLookUp::Map( CDVector &v ) const {
      
    // transform v from [min;max] into (int)[0, 256], hist is not used
    // (actually *very* costly compared to the mapping...)
    CDVector hist;
    CAAMMathUtil::Hist( v, v.Min(), v.Max(), hist, 256, false, true );

    // do LUT mapping
    if (m_LUT.Length()>0) {        
        
        int v_len = v.Length();
        for(int i=0;i<v_len;i++) {

            v[i] = m_LUT[(int)v[i]];
        }    
    } else {

        printf("Warning: CAAMTFLookUp is not initialized. Using [min;max]->[0,256] map only.\n");
    }
}



/**

  @author   Mikkel B. Stegmann
  @version  1-28-2002

  @memo     The inverse of the lookup table transform.

  @doc      The inverse of the lookup table transform. The input vector
			is transformed into [0;255] and the the inverse LUT is applied.
  
  @see      Map
  
  @param    v	Input vector to de-map.
  
  @return   Nothing. The result is returned in v.
  
*/
void CAAMTFLookUp::DeMap( CDVector &v ) const {
       
    // transform v from [min;max] into (int)[0, 256], hist is not used
    CAAMMathUtil::LinearStretchMinMax( v, 0, 255 );

    // do inverse LUT mapping
    int v_len = v.Length();
    for(int i=0;i<v_len;i++) {

        v[i] = m_InvLUT[(int)v[i]];
    } 
}



/**

  @author   Mikkel B. Stegmann
  @version  1-28-2002

  @memo     Reads the class info from file.

  @doc		Reads the class info from file.
  
  @see      ToFile
  
  @param    fh	Open binary file handle.
  
  @return   Nothing.
  
*/
void CAAMTFLookUp::FromFile( FILE *fh ) {

    CAAMTransferFunction::FromFile(fh);

    m_LUT.FromFile(fh);
    m_InvLUT.FromFile(fh);
}
 


/**

  @author   Mikkel B. Stegmann
  @version  1-28-2002

  @memo     Writes the class info to file.

  @doc		Writes the class info to file.
  
  @see      FromFile
  
  @param    fh	Open binary file handle.
  
  @return   Nothing.
  
*/
void CAAMTFLookUp::ToFile( FILE *fh ) const {

    CAAMTransferFunction::ToFile(fh);

    m_LUT.ToFile(fh);
    m_InvLUT.ToFile(fh);
}


/**

  @author   Mikkel B. Stegmann
  @version  1-28-2002

  @memo     Does nothing.

  @doc      Does nothing.
  
  @param    v	Input vector.
  
  @return   Nothing.
  
*/
void CAAMTFUniformStretch::Map( CDVector &v ) const {
        
}



/**

  @author   Mikkel B. Stegmann
  @version  1-28-2002

  @memo     Maps the vector into [0;255].

  @doc      Maps the vector into [0;255].
  
  @param    v	Input vector.
  
  @return   Nothing. The result is returned in v.
  
*/
void CAAMTFUniformStretch::DeMap( CDVector &v ) const {

    CAAMMathUtil::LinearStretchMinMax( v, 0, 255 );    
}
