/* $Id: AAMUtil.cpp,v 1.6 2003/04/23 14:50:01 aam Exp $ */
#include "AAMUtil.h"
#include "AAMMovie.h"
#include "AAMAnalyzeSynthesize.h"
#include <direct.h>


/**

  @author   Mikkel B. Stegmann
  @version  5-3-2000

  @memo     Plots a mesh into an image.

  @doc      Plots a mesh into an image.

  @see      PlotShapeIntoImage
  
  @param    image		The image in which the shape should be plotted.
  @param    shape		The shape to be plotted.
  @param	point_color	The point color of the mesh.
  @param	line_color	The line color of the mesh.
  
  @return   Nothing.
  
*/
void CAAMUtil::PlotMeshIntoImage( CDMultiBand<TAAMPixel> image, 
									const CAAMMesh &mesh,
									TAAMPixel *point_color, 
									TAAMPixel *line_color ) {


	// plot triangles
	for(int i=0;i<mesh.NTriangles();i++) {
		
		const CAAMTriangle &tri = mesh.Triangles()[i];

		// get points
		const CAAMPoint &p1 = mesh.Points()[tri.V1()];
		const CAAMPoint &p2 = mesh.Points()[tri.V2()];
		const CAAMPoint &p3 = mesh.Points()[tri.V3()];

		// plot lines
		image.DrawLine( (int)(.5+p1.x), 
						(int)(.5+p1.y), 
						(int)(.5+p2.x), 
						(int)(.5+p2.y), line_color, false );

		image.DrawLine( (int)(.5+p2.x), 
						(int)(.5+p2.y), 
						(int)(.5+p3.x), 
						(int)(.5+p3.y), line_color, false );

		image.DrawLine( (int)(.5+p3.x), 
						(int)(.5+p3.y), 
						(int)(.5+p1.x), 
						(int)(.5+p1.y), line_color, false );

		// plot points
		image.set_pixel( (int)(p1.x+.5), (int)(p1.y+.5), point_color);
		image.set_pixel( (int)(p2.x+.5), (int)(p2.y+.5), point_color);
		image.set_pixel( (int)(p3.x+.5), (int)(p3.y+.5), point_color);
	}		
}


/**

  @author   Mikkel B. Stegmann
  @version  3-27-2000

  @memo     Draws the shape into an image with a given color.

  @doc		Draws the shape into an image with a given color.

  @see      PlotMeshIntoImage
  
  @param    image		The image in which the shape should be plotted.
  @param    shape		The shape to be plotted.
  @param	point_color	The point color of the shape.
  @param	line_color	The line color of the shape.
  @param	drawNormals	If true the point normals are drawn.
  @param	drawArea    If true the inside area of the shape is drawn.
  @param	drawPoints  If true the points of the shape is drawn.
  @param	drawLines   If true the lines of the shape is drawn.
  
  @return   Nothing.
  
*/
void CAAMUtil::PlotShapeIntoImage( CDMultiBand<TAAMPixel> image, 
										 const CAAMShape &shape,
										 TAAMPixel *point_color, 
										 TAAMPixel *line_color,
										 bool drawNormals,
										 bool drawArea,
										 bool drawPoints,
                                         bool drawLines ) {

	int w, h;
	int path_id=-1;
	double x, y;
	w = image.Width();
	h = image.Height();

    bool drawOriginalPointsOnly = false;    

	// make inside test
	if ( shape.MinX() < 3    || shape.MinY() < 3 ||
	 	 shape.MaxX() >= w-4 || shape.MaxY() >= h-4 ) {

		return;
	}

        int k;
        TAAMPixel invpcolor[BANDS], invlcolor[BANDS];
        for ( k = 0; k < BANDS; k++) {
           invpcolor[k] = 255 - point_color[k]; 
           invlcolor[k] = 255 - line_color[k]; 
        }

    if (drawLines) {

	    // draw lines
	    for(int i=0;i<shape.NPoints();i++) {

		    CAAMPoint p1 = shape.GetPoint( i );
            const CAAMShape::CAAMPointInfo &pi = shape.PointAux()[i];
		    CAAMPoint p2 = shape.GetPoint( pi.m_iConnectTo );

		    if (path_id != pi.m_iPathID) {

			    // new path reached
			    path_id = pi.m_iPathID;
		    }

		    if ( pi.m_iConnectTo!=pi.m_iConnectFrom && 
                ( drawOriginalPointsOnly==false || pi.IsOriginal() )    ) {
		    
                // draw line
			    image.DrawLine( (int)(.5+p1.x), 
							    (int)(.5+p1.y), 
							    (int)(.5+p2.x), 
							    (int)(.5+p2.y), line_color, false );
            } 
                
            if (i==pi.m_iConnectTo && pi.m_iConnectTo==pi.m_iConnectFrom) {            

                // force drawing of isolated points
                int xi = (int)(.5+p1.x);
			    int yi = (int)(.5+p1.y);

                // draw isolated point as a cross 
			    image.set_pixel( xi, yi, invpcolor);
			    image.set_pixel( xi+1, yi, point_color);
			    image.set_pixel( xi-1, yi, point_color);
			    image.set_pixel( xi+2, yi, point_color);
			    image.set_pixel( xi-2, yi, point_color);			
			    image.set_pixel( xi, yi-1, point_color);
			    image.set_pixel( xi, yi+1, point_color);
			    image.set_pixel( xi, yi-2, point_color);
			    image.set_pixel( xi, yi+2, point_color);
            }
	    }
    }
	
	// draw points 	
	if (drawPoints) {

		for(int i=0;i<shape.NPoints();i++) {

			shape.GetPoint( i, x, y );
			
			if (x>=0 && x<w && y>=0 && y<h ) {				

				int xi = (int)(.5+x);
				int yi = (int)(.5+y);

				// draw cross 
				image.set_pixel( xi, yi, invpcolor);
				image.set_pixel( xi+1, yi, point_color);
				image.set_pixel( xi-1, yi, point_color);
				image.set_pixel( xi+2, yi, point_color);
				image.set_pixel( xi-2, yi, point_color);			
				image.set_pixel( xi, yi-1, point_color);
				image.set_pixel( xi, yi+1, point_color);
				image.set_pixel( xi, yi-2, point_color);
				image.set_pixel( xi, yi+2, point_color);
			}		
		}
	}

	// draw normals 	
	if (drawNormals) {

		for(int i=0;i<shape.NPoints();i++) {

			shape.GetPoint( i, x, y );
			
			// draw normal
			CAAMPoint p1, p2;
			shape.Normal( i, p1, p2, 10 );

			if ( p1.x>=0 && p1.x<=w-1 && p1.y>=0 && p1.y<=h-1 && 
				 p2.x>=0 && p2.x<=w-1 && p2.y>=0 && p2.y<=h-1		) {
			
				image.DrawLine( (int)(.5+x), 
								(int)(.5+y), 
								(int)(.5+p2.x), 
								(int)(.5+p2.y), invlcolor, false );

				image.DrawLine( (int)(.5+p1.x), 
								(int)(.5+p1.y), 
								(int)(.5+x), 
								(int)(.5+y), line_color, false );					
			}
		}
	}

	// draw the inside area of the shape
	// signaled by white and black dots
	if ( drawArea ) {

		
		int maxX = (int)(.5+shape.MaxX());
		int maxY = (int)(.5+shape.MaxY());
		int minX = (int)(.5+shape.MinX());
		int minY = (int)(.5+shape.MinY());	
                TAAMPixel p255[BANDS], p0[BANDS];
                SPAWN( p0, 0)
                SPAWN( p255, 255)
	

		for(int y=minY;y<maxY;y+=5) {

			for(int x=minX;x<maxX;x+=5) {

				if ( shape.IsInside( CAAMPoint(x, y) ) ) {
					image.set_pixel( x, y, p255);
					image.set_pixel( x+1, y, p0);
				}
			}
		}
	}

}


/**

  @author   Mikkel B. Stegmann
  @version  3-14-2000

  @memo     Concatenates three vectors

  @doc		Concatenates three vectors.

  @param	dest	Output vector.

  @param	v1		First input vector.

  @param	v2		Second input vector.

  @param	v3		Third input vector.
  
  @return   Nothing.
  
*/
void CAAMUtil::VecCat3( CDVector &dest, const CDVector &v1, 
					   const CDVector &v2, const CDVector &v3 ) {

	assert( dest.Length() == (v1.Length()+v2.Length()+v3.Length()) );

	int v1_end = v1.Length();
	int v2_end = v1_end+v2.Length();
	int v3_end = dest.Length();
	for(int j=0;j<v1_end;j++)  dest[j] = v1[j];
	for(j=v1_end;j<v2_end;j++) dest[j] = v2[j-v1_end];
	for(j=v2_end;j<v3_end;j++) dest[j] = v3[j-v2_end];
}



// local (static) compare function for the qsort() call below
static int str_compare( const void *arg1, const void *arg2 ) {   

   return _stricmp( LPCTSTR ( *(CString*)arg1), LPCTSTR ( *(CString*)arg2) );
}


/**

  @author   Mikkel B. Stegmann
  @version  3-27-2000

  @memo     Scans and sorts a directory for files. 

  @doc      Scans and sorts a directory for files with a specfied extension.
  
  @param    path		Path to read from.
  @param    extension	The file extension to search for. ex. "hips".
  
  @return  The filenames found without any path. 
  
*/
std::vector<CString> CAAMUtil::ScanSortDir( const CString &path, 
										    const CString &extension ) {


	WIN32_FIND_DATA fd;
	HANDLE h;
	CString searchPath;
	int nbFiles;
	std::vector<CString> vFilenames;

    // add terminatin backslash (if needed)
    CString pathBS = CAAMUtil::AddBackSlash(path);
	
	// build and sort list of filenames
    searchPath = pathBS + CString("*.") + extension;
	h = FindFirstFile( searchPath, &fd );
	if (h==INVALID_HANDLE_VALUE) return vFilenames;	// path does not exist
	vFilenames.push_back( pathBS+CString(fd.cFileName) );
	nbFiles=1;
	while( FindNextFile( h, &fd ) ) {

		vFilenames.push_back( pathBS+CString(fd.cFileName) );
		nbFiles++;
	}	

	// sort the filenames
	qsort(	(void *)&(vFilenames[0]), (size_t)nbFiles,
			sizeof( CString ), str_compare );

	// return
	return vFilenames;
}


/**

  @author   Mikkel B. Stegmann
  @version  3-10-2000

  @memo     Reads an image and a shape.
  @doc      Reads an image and a shape (in the corrosponding .m-file).
  @see      
  
  @param    filename	The filename of an annotation. Ex. "horse.asf".
  @param    img			Output image.
  @param	shape		Output shape.
  @param	rfactor		The reduction factor. Performs a scaling of the
						input of 1/rfactor.
  
  @return   Nothing (but throws CVisError)

  @throws   CVisError
  
*/
void CAAMUtil::ReadExample( const CString &filename, CDMultiBand<TAAMPixel> &img, 
						    CAAMShape &shape, int rfactor ) {	
			
	// load annotation	
	bool ok = shape.ReadASF( filename );		

    // check for any errors
	if (!ok) throw CVisError( CString("The requested .asf file '")+filename+CString("' does not exist."),eviserrorUnknown,"ReadExample","AAMUtil.cpp", __LINE__);;

    // load image
    if (shape.HostImage()!="") {
			
        img.ReadBandedFile( CAAMUtil::GetPath( filename ) + shape.HostImage() );								
        img.SetName( shape.HostImage() );

    } else {

        CVisError("No host image for shape.",eviserrorUnknown,"ReadExample",__FILE__, __LINE__);;
    }		

	// convert to absolute coordinates
	shape.Rel2Abs( img.Width(), img.Height() );
	
    // scale if requested
	if (rfactor!=1) {

		shape.Scale( 1.0/(double)rfactor );
		img.ReducePyr( rfactor );
	}
}


/**

  @author   Rune Fisker
  @version  4-6-2000

  @memo     Calculates the point to point error.

  @doc      Calculates the point to point error between two shapes.
  
  @see		DistEuclidianAssBorder 
  
  @param	s1		Shape 1.

  @param	s2		Shape 2.
  
  @return   The average point to point error.
  
*/
double CAAMUtil::DistEuclidianPoints(const CAAMShape &s1,
									 const CAAMShape &s2 ) {

	double x, y;

	assert( s1.NPoints()==s2.NPoints() );

	// convert point format
	CDVector vX1( s1.NPoints() ) , vY1( s1.NPoints() );
	CDVector vX2( s2.NPoints() ) , vY2( s2.NPoints() );
	for(int i=0;i<s1.NPoints();i++) {

		s1.GetPoint( i, x, y );	
		vX1[i] = x;
		vY1[i] = y;

		s2.GetPoint( i, x, y );	
		vX2[i] = x;
		vY2[i] = y;
	}

	return DistEuclidianPoints( vX1, vY1, vX2, vY2 );
}


/**

  @author   Rune Fisker
  @version  4-6-2000

  @memo     Calculates the point to curve error.

  @doc      Calculates the point to curve error between two shapes.
  
  @see		DistEuclidianPoints 
  
  @param	s1		Shape 1.

  @param	s2		Shape 2.

  @param	pvDist	Optional pointer to an output vector containing 
					all point to curve distances (one for each landmark).
  
  @return   The average point to curve error.
  
*/
double CAAMUtil::DistEuclidianAssBorder(const CAAMShape &s1,
										const CAAMShape &s2, 
										CDVector *pvDist	 ) {

	double x, y;    

	assert( s1.NPoints()==s2.NPoints() );

	// convert point format
	CDVector vX1( s1.NPoints() ) , vY1( s1.NPoints() );
	CDVector vX2( s2.NPoints() ) , vY2( s2.NPoints() );
	for(int i=0;i<s1.NPoints();i++) {

		s1.GetPoint( i, x, y );	
		vX1[i] = x;
		vY1[i] = y;

		s2.GetPoint( i, x, y );	
		vX2[i] = x;
		vY2[i] = y;
	}

	return DistEuclidianAssBorder( vX1, vY1, vX2, vY2, pvDist );
}


/**

  @author   Rune Fisker
  @version  4-6-2000

  @memo     Calculates the point to point error.

  @doc      Calculates the point to point error between two shapes.
  
  @see		DistEuclidianAssBorder 
  
  @param	vX1		X-positions of shape 1.

  @param	vY1		Y-positions of shape 1.

  @param	vX2		X-positions of shape 2.

  @param	vY2		Y-positions of shape 2.
  
  @return   The average point to point error.
  
*/
double CAAMUtil::DistEuclidianPoints(const CDVector& vX1, 
									 const CDVector& vY1, 
									 const CDVector& vX2, 
									 const CDVector& vY2 ) {

	assert(vX1.Length() == vY1.Length());
	assert(vX2.Length() == vY2.Length());
	assert(vX1.Length() == vX2.Length());
	
	double dDist = 0;
	
	// euclidian distance with offset as reparameterization
	// Staib & Duncan, PAMI, pp. 1061-1069, 1992
	for (int i=0; i<vX1.Length(); i++)
	{
		dDist += sqrt( (vX1[i]-vX2[i])*(vX1[i]-vX2[i]) + (vY1[i]-vY2[i])*(vY1[i]-vY2[i]) );
	}
	
	dDist /= vX1.Length();	
	
	return dDist;
}


/**

  @author   Rune Fisker
  @version  4-6-2000

  @memo     Calculates the point to curve error.

  @doc      Calculates the point to curve error between two shapes.
  
  @see		DistEuclidianPoints 
  
  @param	vX1		X-positions of shape 1.

  @param	vY1		Y-positions of shape 1.

  @param	vX2		X-positions of shape 2.

  @param	vY2		Y-positions of shape 2.

  @param	pvDist	Optional pointer to an output vector containing 
					all point to curve distances (one for each landmark).
  
  @return   The average point to curve error.
  
*/
double CAAMUtil::DistEuclidianAssBorder(const CDVector& vX1, 
										const CDVector& vY1, 
										const CDVector& vX2,
										const CDVector& vY2,
										CDVector *pvDist ) {

	CDVector vXBorder(vX1.Length()), vYBorder(vX1.Length()), vDist(vX1.Length());
	
	PointOnAssBorder(vX1, vY1, vX2, vY2, vXBorder, vYBorder, vDist);

	if (pvDist) {

		pvDist->Resize( vDist.Length() );
		*pvDist = vDist;
	}
	
	return vDist.Mean();
}


/**

  @author   Rune Fisker
  @version  4-6-2000

  @memo     Calculates the point to curve error.

  @doc      Calculates the point to curve error between two shapes.
  
  @see		DistEuclidianPoints 
  
  @param	vX1			X-positions of shape 1.

  @param	vY1			Y-positions of shape 1.

  @param	vX2			X-positions of shape 2.

  @param	vY2			Y-positions of shape 2.

  @param	vXBorder	Output x border.

  @param	vYBorder	Output y border.
  
  @param	vDist		Point to curve distance for each landmark.
  
  @return   Nothing.
  
*/
void CAAMUtil::PointOnAssBorder(const CDVector& vX1, 
								const CDVector& vY1, 
								const CDVector& vX2, 
								const CDVector& vY2, 
								CDVector& vXBorder, 
								CDVector& vYBorder, 
								CDVector& vDist		) {

	int nStart = 0;
	int nEnd = vX2.Length();
	
	// find associated point on border for all coordinates
	for (int iFrom = 0; iFrom < vX1.Length(); iFrom++)
	{
		double dDist;
		int iDistMin;
		
		vDist[iFrom] = DBL_MAX;
		
		// find closest existing point on associated border
		for (int iTo = nStart; iTo < nEnd; iTo++)
		{
			dDist = sqrt( pow(vX1[iFrom]-vX2[iTo],2) + pow(vY1[iFrom]-vY2[iTo],2) );
			
			if (dDist < vDist[iFrom])
			{
				vDist[iFrom] = dDist;
				iDistMin = iTo;
			}
		}
		
		// set min distance
		vXBorder[iFrom] = vX2[iDistMin];
		vYBorder[iFrom] = vY2[iDistMin];		
		
		// find project on line before
		int iBefore = (iDistMin - 1 + vX2.Length()) % vX2.Length(); // cyclic
		double dDistBefore,dXProjBefore,dYProjBefore;
		
		// dDistBefore = ProjPointOnLine(3, 3, 5, 1, 5, 3, dXProjBefore, dYProjBefore);
		dDistBefore = ProjPointOnLine(  vX2[iBefore], vY2[iBefore], 
                                        vX2[iDistMin], vY2[iDistMin], 
                                        vX1[iFrom], vY1[iFrom], dXProjBefore, dYProjBefore);
		
		// update min distance
		if (dDistBefore < vDist[iFrom])
		{
			vDist[iFrom] = dDistBefore;
			vXBorder[iFrom] = dXProjBefore;
			vYBorder[iFrom] = dYProjBefore;
		}
		
		// find project on line after
		int iAfter = (iDistMin + 1 ) % vX2.Length(); // cyclic
		double dDistAfter,dXProjAfter,dYProjAfter;
		
		dDistAfter = ProjPointOnLine(   vX2[iAfter], vY2[iAfter], 
                                        vX2[iDistMin], vY2[iDistMin], 
                                        vX1[iFrom], vY1[iFrom], 
                                        dXProjAfter, dYProjAfter);
		
		// update min distance
		if (dDistAfter < vDist[iFrom])
		{
			vDist[iFrom] = dDistAfter;
			vXBorder[iFrom] = dXProjAfter;
			vYBorder[iFrom] = dYProjAfter;
		}		
	}
}



/**

  @author   Rune Fisker

  @version  11-22-1999

  @memo     Find the projection dProj of the point dP on the line through dL1 and dL2.

  @doc      Find the projection dProj of the point dP on the line 
			through dL1 and dL2. Returns the distance between dProj 
			and dP, if the dProj lays between dL1 and dL2 otherwise DBL_MAX.  

			dXL1,dYL1,dXL2,dYL2: points on line

			dXP,dYP: points to project

			dXProj,dYProj: projected point on the line
    
  @return   The distance.
  
*/
double CAAMUtil::ProjPointOnLine(const double dXL1, const double dYL1, 
								 const double dXL2, const double dYL2, 
								 const double dXP, const double dYP, 
								 double& dXProj, double& dYProj)
{
	// handle special cases
	if (dXL2 == dXL1)
	{
		dXProj = dXL1;
		dYProj = dYP;
	}
	else if (dYL2 == dYL1)
	{
		dXProj = dXP;
		dYProj = dYL1;
	}
	else
	{
		// get line from point dL1 to dL2
		assert(dXL2 != dXL1);
		double dAL = (dYL2 - dYL1)/(dXL2 - dXL1);
		double dBL = dYL1 - dAL*dXL1;
		
		// find perpendicular line through dP
		assert(dAL != 0);
		double dAP = -1/dAL;
		double dBP = dYP - dAP*dXP;
		
		// find line crossing (projection)
		assert((dAP - dAL) != 0);
		dXProj = (dBP - dBL)/(dAL - dAP);
		dYProj = dAL * dXProj + dBL;
	}
	
	double dDist;
	// to be legal the projected point should lay on the line between dL1 and dL2
	if (( ((dXL1 <= dXProj) && (dXL2 >= dXProj)) || ((dXL1 >= dXProj) && (dXL2 <= dXProj)) ) &&
		( ((dYL1 <= dYProj) && (dYL2 >= dYProj)) || ((dYL1 >= dYProj) && (dYL2 <= dYProj)) ))
	{
		dDist = sqrt( pow(dXP-dXProj,2) + pow(dYP-dYProj,2) );
	}
	else
	{
		dDist = DBL_MAX;
	}
	
	return dDist;
}



/**

  @author   Mikkel B. Stegmann
  @version  1-17-2003

  @memo     Removes the extension of a file name.

  @doc      Removes the extension of a file name.
  
  @param    s	Input file name.
  
  @return   File name without extension.
  
*/
CString CAAMUtil::RemoveExt( const CString &s ) {
	
	return s.Left( s.ReverseFind('.') );
}


/**

  @author   Mikkel B. Stegmann
  @version  1-17-2003

  @memo     Returns the extension of a file name.

  @doc      Returns the extension of a file name.
  
  @param    s	Input filename
  
  @return   The extension.
  
*/
CString CAAMUtil::GetExt( const CString &s ) {
	
	return s.Right( s.GetLength()-s.ReverseFind('.')-1 );
}


/**

  @author   Mikkel B. Stegmann
  @version  1-17-2003

  @memo     Tests if a file can be created for writing.

  @doc      Tests if a file can be created for writing.
  
  @param    file	Input file name.
  
  @return   True, if the file could be created (and deleted again).
  
*/
bool CAAMUtil::CreateTest( const CString &file ) {

	FILE *fh;

	fh=fopen( file, "wb" );

	if ( fh ) {

		// close
		fclose( fh );

		// delete it again
		if ( 0==remove( file ) ) {

			// could be created and deleted
			return true;
		}
	}

	// could not be created
	return false;
}


/**

  @author   Mikkel B. Stegmann
  @version  1-17-2003

  @memo     Ensures that a string is terminated with a backslash

  @doc      Ensures that a string is terminated with a backslash.
			If the already has a terminating backslash, nothing
			is done.
  
  @param    path	Input string.
  
  @return   Backslash-terminated output string.
  
*/
CString CAAMUtil::AddBackSlash( const CString &path ) {

	int len = path.GetLength();

	if (len>0) {
		
		if (path[len-1]=='\\') {

			return path;
		}
	} else {

		return path;
	}

	return path+"\\";
}


/**

  @author   Mikkel B. Stegmann
  @version  1-17-2003

  @memo     Returns the path of a filename.

  @doc      Returns the path of a filename.
  
  @param    fullfilename	Filename including any path.
  
  @return   The path to the filename.
  
*/
CString CAAMUtil::GetPath( const CString &fullfilename ) {	

	int slashpos = fullfilename.ReverseFind('\\');

	if (slashpos==-1) {
		
		// no path
		return "";
	}

	return fullfilename.Left( slashpos+1 );
}


/**

  @author   Mikkel B. Stegmann
  @version  1-17-2003

  @memo     Returns the file name of a path+file name string.

  @doc      Returns the file name of a path+file name string.
  
  @param    filename	Full qualified filename including path.
  
  @return   The file name including any extension, but without any path.
  
*/
CString CAAMUtil::GetFilename( const CString &filename ) {

	int slashpos = filename.ReverseFind('\\');

	if (slashpos==-1) {
		
		// no path
		return filename;
	}

	return filename.Right( filename.GetLength()-slashpos-1 );
}


/**

  @author   Mikkel B. Stegmann
  @version  1-17-2003

  @memo     Forces an extension onto a file name.

  @doc      The method tests the extension of a filename.
			If the wanted extension is presented nothing is done.
			If not, the extension is appended.
  
  @param    filename	Input file name.

  @param    ext			Wanted extension.
  
  @return   The file name including the wanted extension.
  
*/
CString CAAMUtil::ForceExt( const CString &filename, const CString &ext ) {

	return ext!=CAAMUtil::GetExt(filename) ? filename + "." + ext : filename;
}


/**

  @author   Mikkel B. Stegmann
  @version  1-17-2003

  @memo     Tests if a file exists.

  @doc      Tests if a file exists.
  
  @param    filename	File name to test.
  
  @return   True if the file exists.
  
*/
bool CAAMUtil::FileExists( const CString &filename ) {

    FILE *fh = fopen(filename, "r");
    if(fh) {
        
        fclose(fh);
        return true;
    }
    return false;
}


/**

  @author   Mikkel B. Stegmann
  @version  4-17-2000

  @memo     Constructor.

  @doc      Constructor. Opens the file.
  
  @param    filename	The file to open.
    
  @return   Nothing.
  
*/
CAAMPropsReader::CAAMPropsReader( const CString &filename ) : CR(0x0a), LF(0x0d), COMMENT_CHAR('#') {
	
	fh = fopen( filename, "rb" );
}


/**

  @author   Mikkel B. Stegmann
  @version  4-17-2000

  @memo     Destructor.

  @doc      Destructor. Closes the file.
    
  @return   Nothing.
  
*/
CAAMPropsReader::~CAAMPropsReader() {

	if (fh) fclose(fh);
}


/**

  @author   Mikkel B. Stegmann
  @version  4-17-2000

  @memo     Increments the file pointer beyond any white space.

  @doc      Increments the file pointer beyond any white space.
    
  @return   Nothing.
  
*/
void CAAMPropsReader::SkipWhiteSpace() {

    int ch; 
	do { 
        ch=fgetc( fh ); 
    } while( ch==' ' || ch=='\t' || ch==CR || ch==LF );
    ungetc( ch, fh );
}


/**

  @author   Mikkel B. Stegmann
  @version  4-17-2000

  @memo     Increments the file pointer to the start of the next line.

  @doc      Increments the file pointer to the start of the next line.
    
  @return   Nothing.
  
*/
void CAAMPropsReader::SkipRestOfLine() { 
	
	int ch; 
	do { 
        ch=fgetc( fh ); 
    } while( ch!=EOF && ch!=CR && ch!=LF );
    ungetc( ch, fh );

	SkipWhiteSpace();
}


/**

  @author   Mikkel B. Stegmann
  @version  4-17-2000

  @memo     Returns true if more white space is present.

  @doc      Returns true if more white space is present on the current line.
    
  @return   Nothing.
  
*/
bool CAAMPropsReader::MoreNonWhiteSpaceOnLine() {

    char buf[256];
	int ch, n_gets = 0; 
    bool non_white = false;

	  do {
      ch=fgetc( fh ); 
      buf[n_gets++] = ch;
      if ( ch!='\t' && ch!=' ' && ch!=CR && ch!=LF && ch!=EOF) {  non_white = true; break; }
    
    } while( ch!=EOF && ch!=CR && ch!=LF );

    for(int i=0;i<n_gets;i++) ungetc( buf[--n_gets], fh );	

    return non_white;
}


/**

  @author   Mikkel B. Stegmann
  @version  4-17-2000

  @memo     Increments the file pointer beyond any comments.

  @doc      Increments the file pointer beyond any comments.
    
  @return   Nothing.
  
*/
void CAAMPropsReader::SkipComments() { 

	int ch;
	
    ch = getc( fh );        
	if ( ch==COMMENT_CHAR ) {
			
		SkipRestOfLine();                        
        SkipComments();
	} else {
		ungetc( ch, fh );
	}
}


/**

  @author   Mikkel B. Stegmann
  @version  4-22-2002

  @memo     Expands an image to have dyadic size.

  @doc      The method expands an image by using zero-padding
			so that the resulting image �has width and height
			that are powers of two.
  
  @param    img		Input image.

  @param    out		Output dyadic image.
  
  @return   Nothing.
  
*/
void CAAMUtil::ExpandImg2DyadicSize( const CDMultiBand<TAAMPixel> &img, 
                                     CDMultiBand<TAAMPixel> &out ) {

    // calc output size   	
	int new_w, new_h;
	new_w = (int)pow(2, ceil( log(img.Width())/log(2) ) );
	new_h = (int)pow(2, ceil( log(img.Height())/log(2) ) );
	out = CDMultiBand<TAAMPixel>(	new_w,
									new_h,
                                    BANDS, 
									evisimoptDontAlignMemory	);       	

    // copy pixels
    TAAMPixel pixel[BANDS];
	for(int y=0;y<img.Height();y++) {

		for(int x=0;x<img.Width();x++) {
            
            ((CDMultiBand<TAAMPixel>*)&img)->get_pixel( x, y, pixel );                        
			out.set_pixel( x, y, pixel);
		}
	}		
}



/**

  @author   Mikkel B. Stegmann
  @version  4-25-2002

  @memo     Mirrors the edge of an arbitrary shape.

  @doc      Mirrors the edge of an arbitrary shape mask in a matrix
			using poor-mans mirroring.
  
  @param    img			Input matrix. Overwritten by mirrored version.

  @param    mask		Image mask defining the shape.

  @param	edgeWidth	The width of the edge.
  
  @return   Nothing.
  
*/
void CAAMUtil::MirrorEdge( CDMatrix &m, const CDMultiBand<TAAMPixel> &mask, int edgeWidth ) {
        
    int width  = m.NCols();
    int height = m.NRows();
    
    // left horisontal edge
    for(int y=0;y<height;y++) {

        int x=0;
        while( x<width && mask.Pixel(x,y)==0 ) x++;
        if (x==width) continue;

        for(int i=1;i<=edgeWidth;i++) {

            if(x-i<0) break;
            if(x+i>=width) break;

            m[y][x-i] = m[y][x+i];    
        }
    }

    // right horisontal edge
    for(y=0;y<height;y++) {

        int x=width-1;
        while( x>0 && mask.Pixel(x,y)==0 ) x--;
        if (x==0) continue;

        for(int i=1;i<=edgeWidth;i++) {

            if(x-i<0) break;
            if(x+i>=width) break;

            m[y][x+i] = m[y][x-i];    
        }
    }


    // left vertical edge
    for(int x=0;x<width;x++) {

        int y=0;
        while( y<height && mask.Pixel(x,y)==0) y++;
        if (y==height) continue;

        for(int i=1;i<=edgeWidth;i++) {

            if(y-i<0) break;
            if(y+i>=height) break;

            m[y-i][x] = m[y+i][x];    
        }
    }

    // right vertical edge
    for(x=0;x<width;x++) {

        int y=height-1;
        while( y>0 && mask.Pixel(x,y)==0 ) y--;
        if (x==0) continue;

        for(int i=1;i<=edgeWidth;i++) {

            if(y-i<0) break;
            if(y+i>=height) break;

            m[y+i][x] = m[y-i][x];    
        }
    }
}


/**

  @author   Mikkel B. Stegmann
  @version  4-25-2002

  @memo     Mirrors the edge of an arbitrary shape.

  @doc      Mirrors the edge of an arbitrary shape mask in an image
			using poor-mans mirroring.
  
  @param    img			Input image. Overwritten by mirrored version.

  @param    mask		Image mask defining the shape.

  @param	edgeWidth	The width of the edge.
  
  @return   Nothing.
  
*/
void CAAMUtil::MirrorEdge( CDMultiBand<TAAMPixel> &img, 
                           const CDMultiBand<TAAMPixel> &mask, int edgeWidth ) {
        
    int width  = img.Width();
    int height = img.Height();
    TAAMPixel pixel[BANDS];    
    
    // left horisontal edge
    for(int y=0;y<height;y++) {

        int x=0;
        while( x<width && mask.Pixel(x,y)==0 ) x++;
        if (x==width) continue;

        for(int i=1;i<=edgeWidth;i++) {

            if(x-i<0) break;
            if(x+i>=width) break;
            
            img.get_pixel( x+i, y, pixel );                        
			img.set_pixel( x-i, y, pixel );
        }
    }

    // right horisontal edge
    for(y=0;y<height;y++) {

        int x=width-1;
        while( x>0 && mask.Pixel(x,y)==0 ) x--;
        if (x==0) continue;

        for(int i=1;i<=edgeWidth;i++) {

            if(x-i<0) break;
            if(x+i>=width) break;
            
            img.get_pixel( x-i, y, pixel );                        
			img.set_pixel( x+i, y, pixel );
        }
    }


    // left vertical edge
    for(int x=0;x<width;x++) {

        int y=0;
        while( y<height && mask.Pixel(x,y)==0) y++;
        if (y==height) continue;

        for(int i=1;i<=edgeWidth;i++) {

            if(y-i<0) break;
            if(y+i>=height) break;
            
            img.get_pixel( x, y+i, pixel );                        
			img.set_pixel( x, y-i, pixel );
        }
    }

    // right vertical edge
    for(x=0;x<width;x++) {

        int y=height-1;
        while( y>0 && mask.Pixel(x,y)==0 ) y--;
        if (x==0) continue;

        for(int i=1;i<=edgeWidth;i++) {

            if(y-i<0) break;
            if(y+i>=height) break;
            
            img.get_pixel( x, y-i, pixel );                        
			img.set_pixel( x, y+i, pixel );        
        }
    }
}


/**

  @author   Mikkel B. Stegmann
  @version  5-7-2002

  @memo     Tests if a shape is fully inside an image.

  @doc      Tests if a shape is fully inside an image.
  
  @param    s		Input shape.

  @param    img		Input image.
  
  @return   True if the shape is fully inside the image.
  
*/
bool CAAMUtil::ShapeInsideImage( const CAAMShape &s, const CDMultiBand<TAAMPixel> &img ) {

    bool outside =  s.MinX() <  0				||
				    s.MaxX() > img.Width()-1	||
				    s.MinY() <  0				||
				    s.MaxY() > img.Height()-1;

    return !outside;
}


/**

  @author   Mikkel B. Stegmann
  @version  4-10-2000

  @memo     Calculates optimization results.

  @doc      Calculates optimization results.

  @param	optimized	Model shape.

  @param	groundTruth	Ground truth shape.

  @param	ptpt		Average point to point landmark error.

  @param	ptcrv		Average point to curve landmark error.
						NOTICE: This is not a symmetric measure!

  @return	Nothing.

*/
void CAAMUtil::CalcShapeDistances(  const CAAMShape &optimized, 
			            		    const CAAMShape &groundTruth,
				                    double &ptpt,
				                    double &ptcrv,
					                CDVector *pvDists ) {

    ptpt    = DistEuclidianPoints( optimized, groundTruth );
    ptcrv = DistEuclidianAssBorder( optimized, groundTruth, pvDists );
}


/**

  @author   Mikkel B. Stegmann
  @version  8-2-2002

  @memo     Converts seconds to a MM:SS string.

  @doc      Converts seconds to a MM:SS string.  
  
  @param    secs    Time in seconds.  
  
  @return   The time in MM:SS.
  
*/
CString CAAMUtil::Secs2Mins( double secs ) {

    CString ret;
    ret.Format( "%02i:%02i", (int)(secs/60), (int)(.5+secs)%60 );
    return ret;
}


/**

  @author   Mikkel B. Stegmann
  @version  9-3-2002

  @memo     Finds a file name that is not 'occupied'.

  @doc      This method finds a file name that is not 'occupied' by
			adding at number to the base part of the suggested
			file name.
  
  @param    filename_suggestion		Suggestion including extension.
  
  @return   An unused filename resembling the suggstion.
  
*/
CString CAAMUtil::FindVacantFilename( const CString &filename_suggestion ) {

    if ( FileExists(filename_suggestion)==false ) {

        return filename_suggestion;
    }

    CString base = RemoveExt( filename_suggestion ); 
    CString ext  = GetExt( filename_suggestion );
    CString fn;
    
    for(int i=0;i<1000;i++) {

        
        fn.Format("%s%03i.%s", base, i, ext );
        bool exists = CAAMUtil::FileExists( fn );

        if ( !exists ) {

            return fn;
        }
    }
    
    return "";
}


/**

  @author   Mikkel B. Stegmann
  @version  10-29-2002

  @memo     Samples a set of texture vectors given a set of
            shape in absolute (i.e. image) coordinates.

  @doc      Samples a set of texture vectors given a set of
            shape in absolute (i.e. image) coordinates.  
  
  @param    unalignedShapes     Shapes in absolute coordinates.

  @param    vTextures           The set of textures.

  @param    outputRF            The output reference frame 
                                generated for sampling the textures.

  @param    removeMean          If true the mean from each 
                                texture vector (i.e. the DC) is
                                removed.

  @param    useTSP              Use tangent space projection to
                                align the shapes.

  @param    useConvexHull       If true the convex hull is used
                                to determine the extent of a shape.
  
  @return   
  
*/
void CAAMUtil::SampleTextures( const CAAMShapeCollection &unalignedShapes,                                
                               std::vector< CDVector > &vTextures,
                               CAAMReferenceFrame &outputRF,
                               const int imageReduction,
                               const bool removeMean,
                               const bool useTSP,
                               const bool useConvexHull ) {
   

    ///////////////////////////////////////
    // align shapes
    ///////////////////////////////////////

    // copy the unaligned shapes
	CAAMShapeCollection alignedShapes = unalignedShapes;     

	// align shape with respect to position, scale and rotation
	alignedShapes.AlignShapes( useTSP ); 
	
    // calculate the cached reference shape of the unaligned shapes
    CAAMShape refShape;
    alignedShapes.ReferenceShape( refShape );



    ///////////////////////////////////////
    // make reference frame and analyzer
    ///////////////////////////////////////	    
    outputRF.Setup( refShape, useConvexHull );    
    CAAMAnalyzeSynthesizeSoftware as( outputRF ); 



    ///////////////////////////////////////
    // sample texture vectors
    ///////////////////////////////////////	    		
    vTextures.clear();
	for(int i=0;i<unalignedShapes.NShapes();i++) {       

        // get the shape image
        CDMultiBand<TAAMPixel> image; 
        unalignedShapes[i].GetHostImage( image, 
                                         unalignedShapes.Path(), 
                                         imageReduction );                
	
		// sample the texture of the i-th shape into 'vTexture'        	
        CDVector tex;	        
        as.SetAnalyzeImage( image );
        as.Analyze( unalignedShapes[i], tex, true );

        if (removeMean) {

            // remove mean
            double mean = tex.Mean();
            for(int i=0;i<tex.Length();i++) {

                tex[i] -= mean;        
            }
        }

        // store texture
        vTextures.push_back( tex );        
    }
}



/**

  @author   Mikkel B. Stegmann
  @version  10-29-2002

  @memo     Writes a movie file containing a set of textures 
            warped to their mean shape.

  @doc      Writes a movie file containing a set of textures 
            warped to their mean shape.

  @param    filename    Output movie filename.  
  
  @param    vTexture    The set of textures.
  
  @param    rf          The reference frame used for sampling
                        the textures.
  
  @return   Nothing.
  
*/
void CAAMUtil::RegistrationMovie( const CString &filename,
                                  const std::vector<CDVector> &vTexture,
                                  const CAAMReferenceFrame &rf ) {
    

    try {
		// generate shapefree images and write to avi
		CAAMMovieAVI avi;
		avi.Open( filename, OF_CREATE|OF_WRITE  );
		for(unsigned int i=0;i<vTexture.size();i++) {

			// generate the i-th frame
			CDMultiBand<TAAMPixel> frame;			 			
		
			// do the sampling            
            rf.Vector2Image(  vTexture[i], frame );	
                
			// convert and write frame
			CVisImage<CVisRGBABytePixel> rgbImage( frame.MemoryRect() );		
			frame.CopyPixelsToRGBA( rgbImage );		
			avi.WriteFrame( rgbImage );
		}
	}
	catch (CVisFileIOError e) {
		
		AfxMessageBox( e.FullMessage() ); 		
	}
}


/**

  @author   Mikkel B. Stegmann
  @version  10-29-2002

  @memo     Writes a movie file containing all shapes from a
            directory warped to their mean shape.

  @doc      Writes a movie file containing all shapes from a
            directory warped to their mean shape.  
  
  @param    filename        Output movie filename.

  @param    asfPath         Path to annotation files.

  @param    useConvexHull   If true the convex hull is used
                            to determine the extent of a shape.

  @param    writeRefShape   If true the reference shape corresponding
                            to the movie file is written as 
                            "regshape.asf".
  
  @return   Nothing.
  
*/
void CAAMUtil::RegistrationMovie( const CString &filename,
                                  const CString asfPath,
                                  const bool useConvexHull,
                                  const bool writeRefShape ) {

    std::vector<CString> asfFiles;    
    CAAMShapeCollection shapes;  
    asfFiles = CAAMUtil::ScanSortDir( CAAMUtil::AddBackSlash( asfPath ), "asf" );
    shapes.ReadShapes( asfFiles, true );    
    shapes.Rel2Abs( 1 );


    CAAMReferenceFrame rf;
    std::vector<CDVector> vTextures;    
    SampleTextures( shapes,
                    vTextures,
                    rf,
                    1,
                    false,
                    true,
                    useConvexHull );

    RegistrationMovie( filename, vTextures, rf );

    if (writeRefShape) {

        CAAMShape shape(rf.RefShape());
        shape.SetHostImage( filename );
        shape.WriteASF( "regshape.asf", rf.RefImageWidth(), rf.RefImageHeight() );
    }
}


/**

  @author   Mikkel B. Stegmann
  @version  12-3-2002

  @memo     Converts AAM-API shape files (.asf) to the ISBE .pts format.

  @doc      Converts AAM-API shape files (.asf) to the ISBE .pts format.
			Output is written in the directory 'pts'.
  
  @param    path	Path to .asf files.
  
  @return   Nothing.
  
*/
void CAAMUtil::ASF2PTS( const CString &path ) {


	// find .asfs
	std::vector<CString> asf_files = CAAMUtil::ScanSortDir( path, "asf" );

	_mkdir( CAAMUtil::AddBackSlash(path)+"pts" );
	for(int i=0;i<asf_files.size();i++) {

		CAAMShape shape;
		CDMultiBand<TAAMPixel> img;
		CAAMUtil::ReadExample( asf_files[i], img, shape );
		shape.Rel2Abs( img.Width(), img.Height() );

		CString fn( CAAMUtil::RemoveExt( CAAMUtil::GetFilename(asf_files[i]) )+".pts" );

		FILE *fh = fopen( CAAMUtil::AddBackSlash(path)+"pts/"+fn, "wt" );

		fprintf( fh, "version: 1\n" );
		fprintf( fh, "// Originally for image %s\n", shape.HostImage() );
		fprintf( fh, "image_size_x: %i\n", img.Width() );
		fprintf( fh, "image_size_y: %i\n", img.Height() );
		fprintf( fh, "n_points: %i\n", shape.NPoints() );
		fprintf( fh, "{\n" );
		for(int j=0;j<shape.NPoints();j++) {

			CAAMPoint p = shape.GetPoint(j);
			fprintf( fh, "%f %f\n", p.x, p.y );
		}
		fprintf( fh, "}\n" );
		printf( "%s : %s\n", fn, shape.HostImage() );
		fclose(fh);
	}
}



/**

  @author   Mikkel B. Stegmann
  @version  2-9-2003

  @memo     A symmetric point-to-curve measure.

  @doc      A symmetric poin- to-curve measure, i.e.

			SymmetricPtCrv(a,b) == SymmetricPtCrv( b,a );

  @see      DistEuclidianAssBorder
  
  @param    s1		First shape

  @param    s2		Second shape
  
  @return   The average symmetric point-to-curve error over all landmarks.
  
*/
double CAAMUtil::SymmetricPtCrv( const CAAMShape &s1, const CAAMShape &s2 ) {

	double s12 = CAAMUtil::DistEuclidianAssBorder( s1, s2 );
	double s21 = CAAMUtil::DistEuclidianAssBorder( s2, s1 );

	// return the mean
	return .5*(s12+s21);
}



/**

  @author   Mikkel B. Stegmann
  @version  2-9-2003

  @memo     Calculates the overlap between two shapes.

  @doc      Calculates the overlap between two shapes as specified in
			"Active Shape Model Segmentation With Optimal Features"
			Bram van Ginneken et al., IEEE TMI 21(8) Aug. 2002.

			Notice that this only makes sense for one-path closed shapes.
  
  @param    model	Model shape

  @param    gt		Ground truth shape

  @return   The shape overlap (1 = perfect match, 0 = no overlap).
  
*/
double CAAMUtil::ShapeOverlap( const CAAMShape &model, const CAAMShape &gt ) {

	double overlap, TP, FP, FN;

	double minX = min( model.MinX(), gt.MinX() );
	double maxX = max( model.MaxX(), gt.MaxX() );
	double minY = min( model.MinY(), gt.MinY() );
	double maxY = max( model.MaxY(), gt.MaxY() );

	
	//CDMultiBand<TAAMPixel> img( int(maxX+2), int(maxY+1) );		

	// calculate overlap in a one-unit spaced grid
	TP = .0;
	FP = .0;
	FN = .0;
	for(double x=minX;x<maxX;x=x+1.) {

		for(double y=minY;y<maxY;y=y+1.) {

			bool insideModel = model.IsInside( CAAMPoint(x,y) );
			bool insideGT    = gt.IsInside( CAAMPoint(x,y) );

			if ( insideModel || insideGT ) {

				TP = (insideGT && insideModel)  ? TP+1. : TP;
				FP = (insideModel && !insideGT) ? FP+1. : FP;
				FN = (!insideModel && insideGT) ? FN+1. : FN;
			}

			/*
			TAAMPixel pixel = 0;
			if (insideGT) pixel = 180;
			if (insideModel) pixel = 100;
			if (insideModel&&insideGT) pixel = 255;
			img.set_pixel( int(.5+x), int(.5+y), &pixel );
			*/
		}
	}

	overlap = TP/(TP+FP+FN);

	/*
	img.WriteBandedFile( CAAMUtil::FindVacantFilename("overlap.bmp") );
	printf("TP = %.0f\n", TP );
	printf("FP = %.0f\n", FP );
	printf("FN = %.0f\n", FN );
	printf("OL = %.2f\n\n", overlap );
	getchar();
	*/

	return overlap;
}



/**

  @author   Mikkel B. Stegmann
  @version  2-22-2003

  @memo     Reads a file into an array of lines.

  @doc      Reads a file into an array of lines.
  
  @param    filename	Name of input file.
  
  @return   A vector of text lines.
  
*/
std::vector<CString> CAAMUtil::ReadLines( const CString &filename ) {

	std::vector<CString> lines;

	// I know that this is *really* not very 2003-like,
	// but I just can't help reinventing the wheel...
	// (could be interpreted as ph.d.students have too 
	//  much time on their hands, but don't tell anyone)
	FILE *fh = fopen( filename, "rt" );

	if (!fh) return lines;

	const char CR(0x0a);
    const char LF(0x0d);    
	const int maxLineLen = 4096;
	char linebuf[maxLineLen];
	int ch;
	do {
	
		int i = 0;
		do { 			
			ch=fgetc( fh ); 
			linebuf[i] = (char)ch;
			++i;
		} while( ch!=EOF && ch!=CR && ch!=LF && i<maxLineLen-1 );

		if (i<maxLineLen-1 || ch==CR || ch==LF) {

			linebuf[i-1] = 0x0;
		} else {

			linebuf[i] = 0x0;
		}
		if (i>1) { lines.push_back( linebuf ); }
	} while (ch!=EOF);

	fclose(fh);

	return lines;
}



/**

  @author   Rune Fisker
  @version  2-22-1999

  @memo     Enlarge the image. 

  @doc      Enlarge the image.

  @see      DIVAReduceImage 
  
  @param    img			Input image.

  @param  	nFactor		The factor to enlarge the width and height of the image.
  
  @return   
  
*/
template <class TPixel>
void DIVAEnlargeImage( CVisImage<TPixel> &img, int nFactor)
{
	assert(nFactor > 1);
	
	CVisImage<TPixel> imageDest(img.Width()*nFactor, img.Height()*nFactor);
	int yDest = 0;

	for (int ySrc=img.Top();ySrc<img.Bottom();ySrc++,yDest += nFactor)
	{
		TPixel *ppixelSrc = &( img.Pixel(img.Left(),ySrc) );

		for (int xSrc=img.Left();xSrc<img.Right();xSrc++)
		{				
			// insert pixel value in nFactor x nFactor window
			for (int i=0;i<nFactor;i++)
			{
				TPixel *ppixelDest = &( imageDest.Pixel(nFactor*(xSrc-img.Left()),yDest+i) );

				for (int j=0;j<nFactor;j++)
				{
					*ppixelDest++ = *ppixelSrc;
				}
			}

			ppixelSrc++;
		}
	}

	img.Deallocate();
	img.Allocate(imageDest.Width(), imageDest.Height());

	imageDest.CopyPixelsTo(img,evisnormalizeCopyBytesSameType);
}


/**

  @author   Rune Fisker
  @version  2-22-1999

  @memo     Reduce the image.

  @doc      Reduce the image.

  @see      DIVAEnlargeImage 
  
  @param    img			Input image.

  @param  	nFactor		The factor to reduce the width and height of the image.
  
  @return   
  
*/
template <class TPixel>
void DIVAReduceImage( CVisImage<TPixel> &img, int nFactor)
{
	assert(nFactor > 1);
	
	CVisImage<TPixel> imageDest(img.Width()/nFactor, img.Height()/nFactor);

	TPixel dSum;
	int nDiv = nFactor*nFactor;
	int yDest = 0;
	int nNewRight = nFactor*imageDest.Width()+img.Left();
	int nNewBottom = nFactor*imageDest.Height()+img.Top();


	for (int ySrc=img.Top();ySrc<nNewBottom;yDest++,ySrc+= nFactor)
	{
		TPixel *ppixelDest = &( imageDest.Pixel(0,yDest) );

		for (int xSrc=img.Left();xSrc<nNewRight;xSrc += nFactor)
		{				
			dSum = 0;

			// clac. mean in nFactor x nFactor window
			for (int i=0;i<nFactor;i++)
			{
				TPixel *ppixelSrc = &( img.Pixel(xSrc,ySrc+i) );

				for (int j=0;j<nFactor;j++)
				{
					dSum += *ppixelSrc++/(double)nDiv;
							// bad, bad way to sum, I know..
							// its done to avoid overflow in crappy 
					        // byte pixel types
				}
			}
			
			*ppixelDest++ = dSum;
		}
	}


	img.Deallocate();
	img.Allocate(imageDest.Width(), imageDest.Height());

	imageDest.CopyPixelsTo(img,evisnormalizeCopyBytesSameType);
}

// explicit template instiantation
template void DIVAEnlargeImage( CVisImage<CVisUCharPixel> &img, int nFactor);
template void DIVAReduceImage( CVisImage<CVisUCharPixel> &img, int nFactor);
template void DIVAEnlargeImage( CVisImage<CVisRGBABytePixel> &img, int nFactor);
template void DIVAReduceImage( CVisImage<CVisRGBABytePixel> &img, int nFactor);