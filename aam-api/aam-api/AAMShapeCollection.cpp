/* $Id: AAMShapeCollection.cpp,v 1.3 2003/04/23 14:50:01 aam Exp $ */
#include "AAMShapeCollection.h"
#include "AAMUtil.h"

/**

  @author   Mikkel B. Stegmann
  @version  7-27-2000

  @memo     Constructor.

  @doc      Constructor.
  
  
*/
CAAMShapeCollection::CAAMShapeCollection() { }



/**

  @author   Mikkel B. Stegmann
  @version  7-27-2000

  @memo     Destructor.

  @doc      Destructor.
  
  
*/
CAAMShapeCollection::~CAAMShapeCollection() { }



/**

  @author   Mikkel B. Stegmann
  @version  7-27-2000

  @memo     Inserts a shape into the collection.
  
  @param    s	The input shape.
  
*/
void CAAMShapeCollection::Insert(const CAAMShape &s) {

	push_back( s );
}



/**

  @author	Mikkel B. Stegmann
  @version	02-09-2000
  
  @doc		Writes the shapes in a (NShapes x 2*NPoints) matrix in Matlab
			(*.m) format.
			The i-th row thus contains the i-th shape in xxxyy format.   

  @param	sFilename	Filename including path to be written.
  @param	sName		The matlab variable name of the matrix.
  @param	sComment	An optional comment.
  @param	fAppend		Addend to an existing file or overwrite.

  @return	Zero on sucess, non-zero if no shapes are stored in the 
			collection.
	
*/
int CAAMShapeCollection::ToMatlab(const CString& sFilename, const CString& sName, const CString& sComment, bool fAppend) const
{
	assert( NShapes()>0 );

	if ( NShapes()==0 ) return -1;

	CDMatrix m( NShapes(), 2*NPoints() );

	for(int i=0;i<NShapes();i++) {

		m.SetRow( i, (*this)[i] );	
	}
	m.ToMatlab(sFilename, sName, sComment, fAppend);

	return 0;
}

/**

  @author	Mikkel B. Stegmann
  @version	02-09-2000
  
  @doc		Normalizes all shapes with respect to position, scale 
			and orientation. 
			Position normalization are done by a translation of the
			center of gravity to origo.
			Scale normalization are done by a scaling of 1/<norm2>.
			Rotation normalization are done by minimizing the sum of
			squared point distances, as described by Goodall using
			Singular Value Decomposition (SVD).

  @return	Zero on succes.
	
*/
int CAAMShapeCollection::AlignShapes( bool use_tangentspace ) {
	
    // check if we have any shapes at all
	if (NShapes()<1) return 0;    

    // calculate the average shape size according to the 2-norm
    m_dAvgSize = .0;
    for(int i=0;i<NShapes();i++) {

        m_dAvgSize += (*this)[i].Normalize(); // move to origo and scale to unit size
    }
    m_dAvgSize /= NShapes();  

    // the initial estimate of the mean shape is 
    // set to the first shape
    CAAMShape mean_est;    
    mean_est = (*this)[0]; 


    // setup
    bool verbose = false;
	bool forceMeanOrientation = true;


    // do a number number of alignment iterations 
    // until the mean shape estimate is stable
    double diff, diff_max = 0.001;  // diff must be less than 0.1%  
    int max_iter = 30;
    CAAMShape mean_est_old;
    int iter = 1;
	double theta;
    do {

	    // normalize and align all other shapes to the mean shape estimate
		CDVector rot(NShapes());
	    for(int i=0;i<NShapes();i++) {

            // align the i-th shape to the estimate of the mean shape
		    (*this)[i].AlignTo( mean_est, &theta );				
			rot[i] = theta; // record the rotation
            
            // re-scale to unit size to avoid the so-called 'shrinking effect'
            // i.e. that that the alignment error goes towards zero, when
            // the shapes are downscaled
            (*this)[i].Scale( 1.0/(*this)[i].Norm2() );
	    }       
        mean_est_old = mean_est;

        // estimate the new mean shape
        MeanShape( mean_est );
		
		// if this is the first iteration, correct the
		// orientation of the mean shape, so that
		// rotation of the training set to fit the mean
		// shape is -- on average -- zero
		//
		// or put more clearly: 
		//
		// "make the meanshape have a mean orientation"
		//
		if (forceMeanOrientation && iter==1) {
			
			mean_est.Rotate( -rot.Mean() );
		}

        diff = (mean_est_old-mean_est).Norm2();

        if (verbose) {
            printf("Alignment iteration #%i, mean shape est. diff. = %f\n", iter, diff );
        }

        ++iter;

    } while( fabs(diff)/mean_est.Norm2() > diff_max && iter<max_iter );

    // save the mean shape before tangent space projection
	m_MeanShapeBeforeTS = mean_est;

	// project into tangent space to avoid non-linearity in point movements
	if (use_tangentspace) {
			
		CDVector ts(1);
		double scale;
			
		CDMatrix m_MeanShapeMatTransposed( 1, m_MeanShapeBeforeTS.Length() );	
		m_MeanShapeMatTransposed.SetRow( 0, m_MeanShapeBeforeTS );

		for(i=0;i<NShapes();i++) {
			
			ts = m_MeanShapeMatTransposed*(*this)[i];
			scale = 1./ts[0];
			(*this)[i].Scale( scale );			
		}			
	} 
     
	// Success
 	return 0;
}
 


/**

  @author   Mikkel B. Stegmann
  @version  7-27-2000

  @memo     Calcs the mean shape of all shapes.

  @doc      Calcs the mean shape of all shapes.
  @see      
  
  @param    meanShape	The output mean shape.
 
*/
void CAAMShapeCollection::MeanShape(CAAMShape &meanShape) const {

	if ( meanShape.NPoints()!=NPoints() ) {
				
		meanShape.Resize( 2*NPoints() );
	}
	
	// copy point aux data
	meanShape = (*this)[0];

	meanShape = 0.0;
	for(int i=0;i<NShapes();i++) {

		meanShape += (*this)[i];
	}
	meanShape /= NShapes();	
}


/**

  @author   Mikkel B. Stegmann
  @version  7-27-2000

  @memo     Calcs the mean shape of all aligned shapes and size it to mean size.

  @doc      Calcs the mean shape of all aligned shapes and size it to mean size.
  @see      
  
  @param    refShape	The output reference shape.
 
*/
void CAAMShapeCollection::ReferenceShape(CAAMShape &refShape) const {
    
    MeanShape( refShape );
    refShape.Scale( this->MeanSize(), true );
}


/**

  @author   Mikkel B. Stegmann
  @version  5-15-2000

  @memo     Find the minimum x component of all shapes.
  
  @return   The x-minimum.
  
*/
double CAAMShapeCollection::MinX() const {
	
	double val, min = 1.7E+308;

	for(int i=0;i<NShapes();i++) {	

		val = (*this)[i].MinX();
		min = val<min ? val : min;
	}
	return min;
}

/**

  @author   Mikkel B. Stegmann
  @version  5-15-2000

  @memo     Find the maximum x component of all shapes.
  
  @return   The x-maximum.
  
*/
double CAAMShapeCollection::MaxX() const {
	
	double val, max = -1.7E+308;

	for(int i=0;i<NShapes();i++) {	

		val = (*this)[i].MaxX();
		max = val>max ? val : max;
	}
	return max;
}

/**

  @author   Mikkel B. Stegmann
  @version  5-15-2000

  @memo     Find the minimum x component of all shapes.
  
  @return   The x-minimum.
  
*/
double CAAMShapeCollection::MinY() const {
	
	double val, min = 1.7E+308;

	for(int i=0;i<NShapes();i++) {	

		val = (*this)[i].MinY();
		min = val<min ? val : min;
	}
	return min;
}

/**

  @author   Mikkel B. Stegmann
  @version  5-15-2000

  @memo     Find the maximum y component of all shapes.
  
  @return   The y-maximum.
  
*/
double CAAMShapeCollection::MaxY() const {
	
	double val, max = -1.7E+308;

	for(int i=0;i<NShapes();i++) {	

		val = (*this)[i].MaxY();
		max = val>max ? val : max;
	}
	return max;
}


/**

  @author   Mikkel B. Stegmann
  @version  3-15-2000

  @memo     Scale all shapes.

  @doc		Scale the shapes.
  
  @param    s			Scale factor.
  @param    aroundCOG	If true the scale is being done around the 
						cog of the shape instead of around the global
						center.
  
  @return   Nothing.
  
*/
void CAAMShapeCollection::Scale( const double s, const bool aroundCOG ) {

	for(int i=0;i<NShapes();i++) {	

		(*this)[i].Scale( s, aroundCOG );
	}
}


/**

  @author   Mikkel B. Stegmann
  @version  4-4-2000

  @memo		Expands all shapes (contraction can be done by using a negative nPixels).

  @doc      Expands all shapes by moving each model point 'nPixels' perperdicular
			to the shape contour (that is: along the model point normal).

			This function will expand each outer (closed) path of the shape.
			
			No tests for crossing contours are being made as of now.

  @param    nPixels		The number of pixel to expand the shape with.
    
  @return   Nothing.
  
*/
void CAAMShapeCollection::Expand( int nPixels ) {

	for(int i=0;i<NShapes();i++) {	

		(*this)[i].Expand( nPixels );
	}
}


/**

  @author   Mikkel B. Stegmann
  @version  5-4-2000

  @memo     Write the set of shapes to a binary file.
 
  @doc		Write the set of shapes to a binary file.
  
  @see      FromFile
  
  @param    sFilename	Destination filename.
    
  @return   Nothing.
  
*/
void CAAMShapeCollection::ToFile(const CString& sFilename) const {

	FILE *fh;					

    // open file
	fh=fopen(sFilename,"wb");	
	if(fh==NULL)
	{
		throw CVisError( "Can not open file.",
                         eviserrorOpen, "ToFile", __FILE__, __LINE__ );
	}

    // write data
	ToFile( fh );

    // close file
    int ret = fclose( fh );											
    if ( ret!=0 ) {

        throw CVisError( "Can not close file.",
                         eviserrorUnknown, "ToFile", __FILE__, __LINE__ );
    }
}


/**

  @author   Mikkel B. Stegmann
  @version  3-15-2000

  @memo     Reads a set of shapes from a file.  			
  
  @param    sFilename	The filename.
  
  @return   Nothing.
  
*/
void CAAMShapeCollection::FromFile(const CString& sFilename) {

	FILE *Stream;			

	Stream=fopen(sFilename,"rb");
	if(Stream==NULL)
	{
		throw CVisError("Can not open file.",eviserrorOpen,"FromFile","AAMShapeCollection.cpp", __LINE__);
	}

	FromFile( Stream );

	fclose( Stream );
}


/**

  @author   Mikkel B. Stegmann
  @version  5-4-2000

  @memo     Write the set of shapes to a binary file.
 
  @doc		Write the set of shapes to a binary file.
  
  @see      FromFile
  
  @param    fh		Destination file handle.
    
  @return   Nothing.
  
*/
void CAAMShapeCollection::ToFile( FILE *fh ) const {

	int nbShapes = NShapes();

	// write number of shapes as an int
	if(fwrite( (void*)(&nbShapes),sizeof(int),1,fh) < 1) 
	{
		throw CVisError("Problem writing to file.",eviserrorWrite,"ToFile","AAMShapeCollection.cpp", __LINE__);
	}

	for(int i=0;i<nbShapes;i++) {

		(*this)[i].ToFile( fh );
	}
	
	if ( m_MeanShapeBeforeTS.Length()==0 ) {

		// insert zero vector if no alignment has been done
		((CAAMShapeCollection*)this)->m_MeanShapeBeforeTS.Resize( (*this)[0].Length() );

		// copy point aux
		((CAAMShapeCollection*)this)->m_MeanShapeBeforeTS = (*this)[0];

		((CAAMShapeCollection*)this)->m_MeanShapeBeforeTS = .0;
		
	}

	m_MeanShapeBeforeTS.ToFile( fh );

	CDVector v(1);
	v[0] = m_dAvgSize;
	v.ToFile( fh );

    char buf[256];
    strcpy( buf, m_szPath );
    for(int p=0;p<256;p++) fputc( buf[p], fh );       
}


/**

  @author   Mikkel B. Stegmann
  @version  3-15-2000

  @memo     Reads a set of shapes from a file.  			
  
  @param    fh	An open file handle.
  
  @return   Nothing.
  
*/
void CAAMShapeCollection::FromFile( FILE *fh ) {

	int nbShapes;

	// read number of shapes
	if(fread((void*)(&nbShapes),sizeof(int),1,fh) <1 )
	{
		throw CVisError("Problem reading from file.",eviserrorWrite,"FromFile","AAMShapeCollection.cpp", __LINE__);
	}

	for(int i=0;i<nbShapes;i++) {

		CAAMShape s;
		s.FromFile( fh );
		this->Insert( s );
	}

	m_MeanShapeBeforeTS.FromFile( fh );

	CDVector v(1);
	v.FromFile( fh );
	m_dAvgSize = v[0];    
    
    char buf[256];
    for(int p=0;p<256;p++) buf[p] = fgetc( fh );       
    m_szPath = buf;
}


/**

  @author   Mikkel B. Stegmann
  @version  10-24-2000

  @memo     Reads a set of shapes.

  @doc      Reads a set of shapes in the order given in the 
            vector of strings.              
  
  @param    asfFiles    Vector of asf filenames.

  @param    validate    Validates that all shapes have the
                        same number of points.

  @return   True on a valid training set - otherwise false.
  
*/ 
bool CAAMShapeCollection::ReadShapes( const std::vector<CString> &asfFiles, 
                                      bool validate ) {

    int npoints;
    bool valid = true;

    for(unsigned int i=0;i<asfFiles.size();i++) {

        CAAMShape shape;
        CString fn=asfFiles[i];
        shape.ReadASF( fn ); 

        if (i==0) {
            
            npoints = shape.NPoints();
        }

        if (valid) {        

            // check nb points
            if ( shape.NPoints()!=npoints ) {

                valid = false;
                printf( "Error: Shape number %i has %i points. Training point size: %i.\n",
                        i, shape.NPoints(), npoints );
            }
        }

        Insert( shape );
    }   

    // set the path member
    if(asfFiles.size()>0) {
        
        m_szPath = CAAMUtil::GetPath(asfFiles[0]);                
    }

    return valid;
}


/**

  @author   Mikkel B. Stegmann
  @version  10-24-2000

  @memo     Scans a dir for annotations (.asf) and read shapes.

  @doc      Scans a dir for annotations (.asf) and read shapes
            in alphabetical order.
  
  @param    path        Full path to annotations.

  @param    validate    Validates that all shapes have the
                        same number of points.

  @return   True on a valid training set - otherwise false.
  
*/ 
bool CAAMShapeCollection::ReadDir( const CString &path, bool validate ) {	    
      
    // scan for .asf files
    std::vector<CString> filenames;
    CString inputDir = CAAMUtil::AddBackSlash( path );    
    filenames = CAAMUtil::ScanSortDir( inputDir, "asf" );
       
    // read shapes
    return this->ReadShapes( filenames, validate );
}


/**

  @author   Mikkel B. Stegmann
  @version  10-25-2000

  @memo     Converts all shapes with relative coordinates to
            absolute. 

  @doc      Converts all shapes with relative coordinates to
            absolute. Unfortunately this requires to read the
            headers of all host images. VisSDK does not
            provide any operation for this. Thus, all images
            are one by one read into memory and discarded again
            to obtain height and width. Very costly :-(
  
  @param	rfactor	Optional reduction factor. Performs a scaling of the
					the shape by 1/rfactor. Default 1 i.e. no scaling.
  
  @return   Nothing.
  
*/
void CAAMShapeCollection::Rel2Abs( int rfactor ) {    

    for(unsigned int i=0;i<this->size();i++) {

        if ( (*this)[i].IsAbs()==false ) {
          
            // convert to absolute coordinates
		    (*this)[i].Rel2Abs( m_szPath );

            if (rfactor!=1) {

                (*this)[i].Scale( 1.0/(double)rfactor );
            }
        }
    }
}