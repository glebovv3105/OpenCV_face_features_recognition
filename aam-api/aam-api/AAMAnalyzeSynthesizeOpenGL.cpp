/* $Id: AAMAnalyzeSynthesizeOpenGL.cpp,v 1.2 2003/01/17 12:02:07 aam Exp $ */
#include "AAMAnalyzeSynthesize.h"
#include "AAMShape.h"
#include "AAMUtil.h"
#include "DMultiBand.h"
#include "AAMReferenceFrame.h"
#include "HTimer.h"

// Pbuffer stuff
#define GLH_EXT_SINGLE_FILE
#include "pbuffer.h"
#include "glh_extensions.h"


// import library (thus avoiding to tamper with the configuration settings)
#pragma comment( lib, "opengl32.lib" )
 
   
/**

  @author   Mikkel B. Stegmann
  @version  6-7-2002

  @memo     Constructor.

  @doc      Constructor.
  
  @param    rf		The reference frame to analyze and synthesize through.
  
  @return   Nothing.
  
*/
CAAMAnalyzeSynthesizeOpenGL::CAAMAnalyzeSynthesizeOpenGL( const CAAMReferenceFrame &rf ) :      
      CAAMAnalyzeSynthesize(rf),
      m_pAnalyzeImage(NULL),
      m_pSynthesizeImage(NULL),
      m_pAnalyzePBuffer(NULL),
      m_pSynthesizePBuffer(NULL),
      m_DC(NULL),
      m_RC(NULL) {
   
    int w = (int)ceil( m_pReferenceFrame->RefImageWidth() );
    int h = (int)ceil( m_pReferenceFrame->RefImageHeight() );

    // set id
    m_Id = asOpenGL;

    // setup ordinary OpenGL window (needed by the pbuffer)
    m_pWnd = new CFrameWnd;
    m_pWnd->Create(	NULL,
			        "AAM-API OpenGL rendering window",
			        WS_OVERLAPPEDWINDOW | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | CS_OWNDC,
			        CRect(0,0,w,h),
			        AfxGetMainWnd() );                        
    

    m_DC = GetDC( m_pWnd->m_hWnd );

    PIXELFORMATDESCRIPTOR pfd;
    ZeroMemory( &pfd, sizeof( pfd ) );
    pfd.nSize = sizeof( pfd );
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 24;
    pfd.cDepthBits = 16;
    pfd.iLayerType = PFD_MAIN_PLANE;
    int iFormat = ChoosePixelFormat( m_DC, &pfd );
    SetPixelFormat( m_DC, iFormat, &pfd );

    m_RC = wglCreateContext( m_DC );    

    // usage of the nvidia rectangle texture extension
    m_bUseRectangleExt = true;
    m_TextureModeID = m_bUseRectangleExt ? GL_TEXTURE_RECTANGLE_NV : GL_TEXTURE_2D;
        
    // setup analyze pbuffer by default
    SetupPBuffer( w, h, &m_pAnalyzePBuffer );
}



/**

  @author   Mikkel B. Stegmann
  @version  6-7-2002

  @memo     Destructor.

  @doc      Destructor.
  
  @return   Nothing.
  
*/
CAAMAnalyzeSynthesizeOpenGL::~CAAMAnalyzeSynthesizeOpenGL() {    

    // delete textures
    if ( glIsTexture(m_iAnalyzeTextureID) ) {
        
        glDeleteTextures( 1, &m_iAnalyzeTextureID );    
    }
    if ( glIsTexture(m_iSynthesizeTextureID) ) {
        
        glDeleteTextures( 1, &m_iSynthesizeTextureID );    
    }
   
    // delete analyze image (if it is a copy)
    if (m_bUseRectangleExt==false && m_pAnalyzeImage) {
       
       delete m_pAnalyzeImage;
    }

    // delete synthesize image (if it is a copy)
    if (m_bUseRectangleExt==false && m_pSynthesizeImage) {
       
       delete m_pSynthesizeImage;
    }    
   
    // delete the pbuffers
    if (m_pAnalyzePBuffer) {
       
       delete m_pAnalyzePBuffer;
    }
    if (m_pSynthesizePBuffer) {
       
       delete m_pSynthesizePBuffer;
    }

    // delete the device context
    if (m_DC) {
       
       ReleaseDC( m_pWnd->m_hWnd, m_DC );
    }
   
    // delete the render context
    if (m_RC) {
        wglMakeCurrent( NULL, NULL );
        wglDeleteContext( m_RC );
    }
}



/**

  @author   Mikkel B. Stegmann
  @version  6-7-2002

  @memo     Samples the texture under a given shape.

  @doc      This method samples the image intensities under a user-
			supplied shape into a texture vector.

			The image is set using the SetAnalyzeImage call.

			If a texture vector of the shape is needed instead 
			call the alternative form of Analyze.
			
  @see      SetAnalyzeImage
  
  @param    shape				A shape in �mage coordinates.

  @param    refImg				Output reference image.

  @param	useInterpolation	If true bilinear interpolation is used (default).
								Otherwise the faster nearest neighbor interpolation
								is used.

								NOTE: This flag is actually ignored currently in
									  this OpenGL implementation.
  
  @return   True is the shape is inside the image.
  
*/
bool CAAMAnalyzeSynthesizeOpenGL::Analyze( const CAAMShape &shape, 
                                           CDMultiBand<TAAMPixel> &refImg,
                                           const bool useInterpolation ) const {
	
	// non-const pointer for caching
    CAAMAnalyzeSynthesizeOpenGL *pNCthis = (CAAMAnalyzeSynthesizeOpenGL*)this;	

    // warp input shape to the reference frame   

    // setup texture    
    double xs = m_pAnalyzeImage->Width()-1.;
    double ys = m_pAnalyzeImage->Height()-1.;
    const std::vector<CAAMTriangle> &triangles = m_pReferenceFrame->RefMesh().Triangles();
    const std::vector<CAAMPoint> &points = m_pReferenceFrame->RefMesh().Points();      
    int np;
    double dx,dy;     
    
    // make the analyze pbuffer the current rendering context
	m_pAnalyzePBuffer->MakeCurrent(); 
		

    // clear color buffer    
    glClear( GL_COLOR_BUFFER_BIT ); 
    
    glEnable(m_TextureModeID); 
    glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );    
    glBindTexture( m_TextureModeID, m_iAnalyzeTextureID );     
                
    
    glMatrixMode(GL_TEXTURE);
    glLoadIdentity();
    if (m_bUseRectangleExt) {

        // do not normalize texture coordinates                
        glTranslated( .5, .5, 0 );        
    } else {

        // use standard normalized texture coordinates        
        glScaled( 1./xs, 1./ys, 1 );                
    }
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity(); 
    
    //
    //  we could use 50% vertex arrays and compiled 50% vertex arrays here
    //
    //  but this is really not the bottle neck, glReadPixels() is...
    //

    glBegin(GL_TRIANGLES);          
        for(unsigned int i=0;i<triangles.size();i++) {

            np = triangles[i].V1(); // point #                  
            shape.GetPointUS( np, dx, dy );
            glTexCoord2d( dx, dy );            
            glVertex2d( points[ np ].x, points[ np ].y );

            np = triangles[i].V2(); // point #                  
            shape.GetPointUS( np, dx, dy );
            glTexCoord2d( dx, dy );            
            glVertex2d( points[ np ].x, points[ np ].y );

            np = triangles[i].V3(); // point #                  
            shape.GetPointUS( np, dx, dy );
            glTexCoord2d( dx, dy );            
            glVertex2d( points[ np ].x, points[ np ].y );
        }
    glEnd();

    glDisable(m_TextureModeID); 
    glFinish();
    
    // make reference image
    int refWidth  = m_pReferenceFrame->RefImageWidth();
    int refHeight = m_pReferenceFrame->RefImageHeight();
    if ( refImg.Width()!=refWidth || refImg.Height()!=refHeight ) {
        
        refImg = CDMultiBand<TAAMPixel>( refWidth, refHeight, BANDS,
                                         evisimoptDontAlignMemory );
    }        

    // read pbuffer
    glReadPixels( 0,0, 
                  refImg.Width(), refImg.Height(), 
                  BANDS==1 ? GL_RED : GL_RGB, 
                  GL_UNSIGNED_BYTE, &(refImg.Pixel(0,0))  );

    return true;
}



/**

  @author   Mikkel B. Stegmann
  @version  6-7-2002

  @memo     Samples the texture under a given shape.

  @doc      This method samples the image intensities under a user-
			supplied shape into a texture vector.

			The image is set using the SetAnalyzeImage call.

			If a reference image of the shape is needed instead 
			call the alternative form of Analyze.
			
  @see      SetAnalyzeImage
  
  @param    shape				A shape in �mage coordinates.

  @param    texture				Output texture vector.

  @param	useInterpolation	If true bilinear interpolation is used (default).
								Otherwise the faster nearest neighbor interpolation
								is used.

								NOTE: This flag is actually ignored currently in
									  this OpenGL implementation.
  
  @return   True is the shape is inside the image.
  
*/
bool CAAMAnalyzeSynthesizeOpenGL::Analyze( const CAAMShape &shape,
                                           CDVector &texture,
                                           const bool useInterpolation ) const {

    // setup
    CDMultiBand<TAAMPixel> refImage;
    bool ret;

    ret = Analyze( shape, refImage, useInterpolation );

    if (ret) {

        // convert reference image to texture vector
        m_pReferenceFrame->Image2Vector( refImage, texture );    
    }
   
    // we're done
    return ret;
}



/**

  @author   Mikkel B. Stegmann
  @version  6-7-2002

  @memo     Constructs a p-buffer and sets up various OpenGL stuff.

  @doc      This method constructs a p-buffer and sets up various OpenGL stuff.
			Credit goes to NVidea for the p-buffer code.
  
  @param    w			P-buffer width.

  @param    h			P-buffer height.

  @param	pPBuffer	Pointer to a pointer of the newly created p-buffer.
  
  @return   Nothing.
  
*/
void CAAMAnalyzeSynthesizeOpenGL::SetupPBuffer( const int w, const int h,
                                                PBuffer **pPBuffer ) {

	if (w==0 || h==0) {

		printf("Error: Trying to create a pbuffer with zero height or width.\n");
		exit( -1 );
	}   

    // make the dummy opengl render contex current
    // (needed to initialize the pbufffer)
    wglMakeCurrent( m_DC, m_RC );


    // make PBuffer
    if (*pPBuffer) delete *pPBuffer;
    *pPBuffer = new PBuffer( w, h, MODE_SINGLE );
  
    // Get the entry points for the extension.
	if( !glh_init_extensions( " WGL_ARB_pbuffer "
                              " WGL_ARB_pixel_format " ) ) {

        printf("Error: Necessary OpenGL extensions were not supported.\n");
		exit( -1 );
	}

    // Initialize the PBuffer now that we have a valid context
    // that we can use during the p-buffer creation process.
    (*pPBuffer)->Initialize();

    // Initialize some graphics state for the PBuffer's rendering context.
    (*pPBuffer)->MakeCurrent();

    // clear color buffer
    glClearColor( .0, .0, .0, .0 );
    glClear( GL_COLOR_BUFFER_BIT );      
    
    // setup projection    
    glMatrixMode(GL_PROJECTION); 
    glLoadIdentity();
    glOrtho(-.5, w-.5, -.5, h-.5, -1, 1 );
    glViewport(0, 0, w, h);     

    // setup misc flags           
    glPixelStorei(GL_PACK_ALIGNMENT, 1);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);     

     // set texture filtering mode
    bool bUseBilinearFiltering = true;
    glTexParameteri( m_TextureModeID, GL_TEXTURE_MIN_FILTER, 
				     bUseBilinearFiltering ? GL_LINEAR : GL_NEAREST );
    glTexParameteri( m_TextureModeID, GL_TEXTURE_MAG_FILTER, 
				     bUseBilinearFiltering ? GL_LINEAR : GL_NEAREST );
}


/**

  @author   Mikkel B. Stegmann
  @version  6-11-2002

  @memo     Benchmarks the glReadPixels call.

  @doc      This function tries to find the fastest form of the 
			glReadPixels(). This is typically *very* driver/card-specific.

			Results are written to stdout.
  
  @return   Nothing.
  
*/
void CAAMAnalyzeSynthesizeOpenGL::ClockReadPixels() {

    int w = m_pReferenceFrame->RefImageWidth();
    int h = m_pReferenceFrame->RefImageHeight();
    unsigned char *pPixels = new unsigned char[w*h*4];
    CHTimer timer;
    double RGB2R_Time = .0, RGBA2R_Time = .0;
    double R_Time = .0, RGB_Time = .0, RGBA_Time = .0;
    CDMultiBand<TAAMPixel> dest = CDMultiBand<TAAMPixel>( w, h, BANDS, 
                                                          evisimoptDontAlignMemory );


    // dummy call since the first call to glReadPixels can be slower
    // than any subsequent calls
    glReadPixels( 0, 0, w, h, GL_RGB, GL_UNSIGNED_BYTE, pPixels  ); 

    int n = 100;
    for(int i=0;i<n;i++) {

        timer.reset();
        timer.start();    
        glReadPixels( 0, 0, w, h, GL_RGB, GL_UNSIGNED_BYTE, pPixels  ); 
        ExtractChannel( 3, 0, pPixels, dest );
        timer.stop();
        RGB2R_Time += timer.getTime();

        timer.reset();
        timer.start();    
        glReadPixels( 0, 0, w, h, GL_RGBA, GL_UNSIGNED_BYTE, pPixels  ); 
        ExtractChannel( 4, 0, pPixels, dest );
        timer.stop();
        RGBA2R_Time += timer.getTime();

        timer.reset();
        timer.start();    
        glReadPixels( 0, 0, w, h, GL_RED, GL_UNSIGNED_BYTE, pPixels  );         
        timer.stop();
        R_Time += timer.getTime();

        timer.reset();
        timer.start();    
        glReadPixels( 0, 0, w, h, GL_RGB, GL_UNSIGNED_BYTE, pPixels  );         
        timer.stop();
        RGB_Time += timer.getTime();

        timer.reset();
        timer.start();    
        glReadPixels( 0, 0, w, h, GL_RGBA, GL_UNSIGNED_BYTE, pPixels  );         
        timer.stop();
        RGBA_Time += timer.getTime();    
    }

    printf("\nReadPixels timings:\n");
    printf("  RGB2R_Time  = %.2f ms\n", RGB2R_Time*1000./n );
    printf("  RGBA2R_Time = %.2f ms\n", RGBA2R_Time*1000./n );
    printf("  R_Time      = %.2f ms\n", R_Time*1000./n );
    printf("  RGB_Time    = %.2f ms\n", RGB_Time*1000./n );
    printf("  RGBA_Time   = %.2f ms\n\n", RGBA_Time*1000./n );   

    delete pPixels;
}



/**

  @author   Mikkel B. Stegmann
  @version  6-11-2002

  @memo     Extracts a channel (band) from an RGB/RGBA image.

  @doc      Extracts a channel (band) from an RGB/RGBA image.
  
  @param    pixelSize	The size of a pixel in bytes.

  @param    channelNo	The channel to extract (zero is the first).

  @param    pPixels		Pointer to a continuous pixel-array
						(i.e. *no* row-padding etc.) of a 
						reference image.
  
  @param    dest		Pre-allocated destination image.
  
  @return   Nothing.
  
*/
void CAAMAnalyzeSynthesizeOpenGL::ExtractChannel(	const int pixelSize,
													const int channelNo,
													unsigned char *pPixels, 
													CDMultiBand<TAAMPixel> &dest ) {

    int w = m_pReferenceFrame->RefImageWidth();
    int h = m_pReferenceFrame->RefImageHeight();
    unsigned char *oP = pPixels+channelNo;

    unsigned char *destPixels = &(dest.Pixel(0,0)); // UNSAFE

    for(int y=0;y<h;y++) {

        for(int x=0;x<w;x++) {

            //dest.Pixel(x,y) = *oP;    // safe version
            *destPixels++ = *oP;        // UNSAFE (but fast)
            oP += pixelSize;
        }
    }
}



/**

  @author   Mikkel B. Stegmann
  @version  6-7-2002

  @memo     Renders a texture vector into a shape.

  @doc      This method renders a texture vector into a shape 
			defined in image coordinates.
  
  @param    shape				The shape to synthesize into.

  @param    texture				The input texture vector in byte range [0;255].

  @param	destImage			Destination image

  @param	renderOntoImage		If true the synthesization is done on top of the 
								existing image.
  
  @return   True on success.
  
*/
bool CAAMAnalyzeSynthesizeOpenGL::Synthesize( const CAAMShape &shape, 
                                              const CDVector &texture,
                                              CDMultiBand<TAAMPixel> &destImage,
                                              bool renderOntoImage ) const {
    
    // non-const pointer for caching
    CAAMAnalyzeSynthesizeOpenGL *pNCthis = (CAAMAnalyzeSynthesizeOpenGL*)this;	

    int w, h;
    bool sizeChanged;
    CAAMShape synthShape(shape);
    if (renderOntoImage) {

        w = destImage.Width();
        h = destImage.Height();
    } else {

        w = (int)ceil( synthShape.Width() );
        h = (int)ceil( synthShape.Height() );

        // move shape to fourth quadrant
        double xMin = synthShape.MinX();
        double yMin = synthShape.MinY();
        synthShape.Translate( -xMin, -yMin );
    }

    int pbW=w, pbH=h;
    if (m_pSynthesizePBuffer==NULL) {

        // double the needed size the first time we need
        // a synthesize pbuffer (gives us some air...)
        pbW *= 2;
        pbH *= 2;
    } 

    // ensure that the pbuffer is large enough to hold the shape            
    sizeChanged = pNCthis->EnsureSize( w, h, &(pNCthis->m_pSynthesizePBuffer) );
    if (!sizeChanged) { m_pSynthesizePBuffer->MakeCurrent(); }

    // make shape-free texture image
    CDMultiBand<TAAMPixel> shapeFreeTexture;
    m_pReferenceFrame->Vector2Image( texture, shapeFreeTexture ); 
    
    // mirror the edge to avoid warped points that falls 
    // just outside the reference image mask
    CAAMUtil::MirrorEdge( shapeFreeTexture, m_pReferenceFrame->MaskImage(), 1 );             

    // upload texture   
    pNCthis->SetImage(  shapeFreeTexture, 
                        &(pNCthis->m_pSynthesizeImage), 
                        &(pNCthis->m_iSynthesizeTextureID) );    

    double xs = m_pSynthesizeImage->Width()-1.;
    double ys = m_pSynthesizeImage->Height()-1.;
    const std::vector<CAAMTriangle> &triangles = m_pReferenceFrame->RefMesh().Triangles();
    const std::vector<CAAMPoint> &points = m_pReferenceFrame->RefMesh().Points();      
    int np;
    double dx,dy;  
    bool wireframe = false;

    // clear color buffer    
    glClearColor( 1.0, .0, .0, .0 );    
    glClear( GL_COLOR_BUFFER_BIT );  
      
    // draw image
    if (renderOntoImage) {        

        glDrawPixels( destImage.Width(), destImage.Height(),
                      BANDS==1 ? GL_RED : GL_RGB, 
                      GL_UNSIGNED_BYTE, &(destImage.Pixel(0,0)) );
    }    
  
    glEnable(m_TextureModeID); 
    glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );    
    glBindTexture( m_TextureModeID, m_iSynthesizeTextureID );     


    glMatrixMode(GL_TEXTURE);
    glLoadIdentity();
    if (m_bUseRectangleExt) {

        // do not normalize texture coordinates                
        glTranslated( .5, .5, 0 );        
    } else {

        // use standard normalized texture coordinates        
        glScaled( 1./xs, 1./ys, 1 );                
    }
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    //
    //  we could use 50% vertex arrays and compiled 50% vertex arrays here
    //
    //  but this is really not the bottle neck, glReadPixels() is...
    //
    if (wireframe) { glPolygonMode(GL_FRONT_AND_BACK, GL_LINE ); }
    glBegin(GL_TRIANGLES);
        for(unsigned int i=0;i<triangles.size();i++) {

            np = triangles[i].V1(); // point #                                  
            glTexCoord2d( points[ np ].x, points[ np ].y );
            shape.GetPointUS( np, dx, dy );
            glVertex2d( dx, dy );            

            np = triangles[i].V2(); // point #                  
            glTexCoord2d( points[ np ].x, points[ np ].y );
            shape.GetPointUS( np, dx, dy );
            glVertex2d( dx, dy );

            np = triangles[i].V3(); // point #                  
            glTexCoord2d( points[ np ].x, points[ np ].y );
            shape.GetPointUS( np, dx, dy );
            glVertex2d( dx, dy );
        }
    glEnd();

    glDisable(m_TextureModeID); 
    glFinish();
        
    // make dest image
    if ( destImage.Width()!=w || destImage.Height()!=h ) {

        destImage = CDMultiBand<TAAMPixel>( w, h, BANDS,
                                            evisimoptDontAlignMemory );
    }    
  

    // read pbuffer
    glReadPixels( 0, 0, 
                  destImage.Width(), destImage.Height(), 
                  BANDS==1 ? GL_RED : GL_RGB, 
                  GL_UNSIGNED_BYTE, &(destImage.Pixel(0,0))  );        

    return true;
}



/**

  @author   Mikkel B. Stegmann
  @version  6-7-2002

  @memo     Loads an image into the texture memory.

  @doc      Loads an image into the texture memory. Expensive call.

  
  @param    textureImage	Image to upload to the GPU.

  @param    pTextureId		Destination texture id.
  
  @return   Nothing.
  
*/
void CAAMAnalyzeSynthesizeOpenGL::LoadTexture( const CDMultiBand<TAAMPixel> &textureImage,
                                               unsigned int *pTextureId ) {    

    // make open gl texture    
    if ( !glIsTexture(*pTextureId) ) {
        
        glGenTextures( 1, pTextureId );        
    } 
    
    // UNSAFE: will only work for TAAMPixel = unsigned char
    unsigned char *pTexPixels = (unsigned char *)&(textureImage.Pixel( 0, 0 ));
    int width  = textureImage.Width();
    int height = textureImage.Height();    


    //
    //  LATER: use only glTexImage2D() once then glSubTexImage2D()
    //
    glBindTexture( m_TextureModeID, *pTextureId );               
    switch(BANDS) {

        case 1:     glTexImage2D(   m_TextureModeID, 0, 1, 
                                    width, height, 0, GL_RED,
				                    GL_UNSIGNED_BYTE, pTexPixels );	
                    break;

        case 3:     glTexImage2D(   m_TextureModeID, 0, 3, 
                                    width, height, 0, GL_RGB,
				                    GL_UNSIGNED_BYTE, pTexPixels );
                    break;

        default:    printf("Unknown number of bands.\n");
                    break;
    } 
}



/**

  @author   Mikkel B. Stegmann
  @version  6-7-2002

  @memo     Sets the image to be analyzed.

  @doc      Sets the image to be analyzed. Recognize that this means that
			the image must be uploaded to the GPU.

			Hence, during multiple calls to Analyze on the same image *avoid*
			calling SetAnalyze image every time!

  @param    img		Image to be analyzed.
  
  @return   Nothing.
  
*/
void CAAMAnalyzeSynthesizeOpenGL::SetAnalyzeImage( const CDMultiBand<TAAMPixel> &img ) {

    SetImage(img, &m_pAnalyzeImage, &m_iAnalyzeTextureID );
}


/**

  @author   Mikkel B. Stegmann
  @version  6-7-2002

  @memo     Clones ifself (smart way to convey type info).

  @doc      Clones ifself (smart way to convey type info).
  
  @param    rf		The reference frame of the cloned object.
  
  @return   A cloned object created on the heap.
  
*/
CAAMAnalyzeSynthesize *CAAMAnalyzeSynthesizeOpenGL::Clone( const CAAMReferenceFrame &rf ) const {

    CAAMAnalyzeSynthesizeOpenGL *obj;
    
    obj = new CAAMAnalyzeSynthesizeOpenGL( rf );  

    return obj;
}


/**

  @author   Mikkel B. Stegmann
  @version  7-4-2002

  @memo     Call this to ensure that the p-buffer is large enough.

  @doc      The method ensures that the p-buffer is large enough to hold 
			and image of w * h.
  
  @param    w			Desired minimum p-buffer width.

  @param    h			Desired minimum p-buffer height.

  @param    pPBuffer	Pointer to a p-buffer pointer.
  
  @return   True the the size of 'pPBuffer' has changed.
  
*/
bool CAAMAnalyzeSynthesizeOpenGL::EnsureSize( const int w, const int h, PBuffer **pPBuffer ) {
    
    bool sizeChanged = false;

    if (*pPBuffer==NULL) {
        
        SetupPBuffer( w, h, pPBuffer );
        return true;
    }

    int bufW  = (*pPBuffer)->width;
    int bufH  = (*pPBuffer)->height;
    
    if (bufW<w || bufH<h) {
        
        SetupPBuffer( w, h, pPBuffer );
        return true;
    }

    return sizeChanged;
}



/**

  @author   Mikkel B. Stegmann
  @version  7-4-2002

  @memo     Uploads an image to the GPU.

  @doc      Uploads an image to the GPU. Don't call this directly. 
  
			This method is called by SetAnalyzeImage.

  @see      SetAnalyzeImage
  
  @param	img			Input image.
  
  @param    destImage	Pointer to an image pointer of the image actually being uploaded.

  @param    pTextureId	Input texture id.
  
  @return   Nothing.
  
*/
void CAAMAnalyzeSynthesizeOpenGL::SetImage( const CDMultiBand<TAAMPixel> &img, 
                                            CDMultiBand<TAAMPixel> **destImage,
                                            unsigned int *pTextureId ) {

    if (m_bUseRectangleExt==false && *destImage) delete *destImage;
     
    if(m_bUseRectangleExt) {

        *destImage = &(CDMultiBand<TAAMPixel>&)img;        
    } else {

        *destImage = new CDMultiBand<TAAMPixel>;
        CAAMUtil::ExpandImg2DyadicSize( img, **destImage );         
    }
   
    LoadTexture( **destImage, pTextureId );
}