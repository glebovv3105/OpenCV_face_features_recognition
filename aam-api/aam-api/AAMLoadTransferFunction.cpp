/* $Id: AAMLoadTransferFunction.cpp,v 1.2 2003/01/17 12:02:08 aam Exp $ */
#include "AAMTransferFunctions.h"


/**

  @author   Mikkel B. Stegmann
  @version  1-29-2002

  @memo     Transfer function loader.

  @doc      The function loads a transfer function from a stream,
			instantiates the correct concrete class and returns a
			base class pointer.
			  
  @param    fh		Open binary stream.

  @param    pModel	Model pointer (currently not used).
  
  @return   A pointer to a transfer function created on the heap.
  
*/
CAAMTransferFunction *AAMLoadTransferFunction( FILE *fh, CAAMModel *pModel ) {   
    
    CAAMTransferFunction *pTF;
    unsigned int id;
    fread((void*)&id,sizeof(unsigned int),1,fh);
    eTFid tf_id = (eTFid)id;
    
    // unwind stream
    fseek( fh, -(int)(sizeof(unsigned int)), SEEK_CUR );
    
    switch (tf_id) {


        case tfIdentity:        pTF = new CAAMTFIdentity; 
                                break;

        case tfLookUp:          pTF = new CAAMTFLookUp; 
                                break;

        case tfUniformStretch:  pTF = new CAAMTFUniformStretch; 
                                break;        
        
        default:                printf("Error: LoadTF(): Unknown transfer function.\n");
                                exit(-1);
                                break;
    }

    pTF->FromFile( fh );

    return pTF; 
}