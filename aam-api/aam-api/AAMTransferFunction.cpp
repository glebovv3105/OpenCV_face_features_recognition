/* $Id: AAMTransferFunction.cpp,v 1.1.1.1 2003/01/03 19:17:12 aam Exp $ */
#include "AAMTransferFunction.h"


void CAAMTransferFunction::FromFile( FILE *fh ) {

    unsigned int id;
    fread((void*)&id,sizeof(unsigned int),1,fh);
    m_Id = (eTFid)id;	
}
 

void CAAMTransferFunction::ToFile( FILE *fh ) const {

    unsigned int id;

    id = (unsigned int)m_Id;
    fwrite((void*)&id,sizeof(unsigned int),1,fh);   
}