/* $Id: AAMOptimize.cpp,v 1.2 2003/02/14 21:49:23 aam Exp $ */
#include "AAMOptimize.h"
#include "AAMModel.h"


// import library (thus avoiding to tamper with the configuration settings)
#ifdef _DEBUG
#pragma comment( lib, "DivaOptDB.lib" )
#else
#pragma comment( lib, "DivaOpt.lib" )
#endif




/**

  @author   Mikkel B. Stegmann
  @version  7-7-2000

  @memo     Constructor for the AAM optimizer.

  @doc      Constructor for the AAM optimizer. Sets uo initial info
			about AAM, shape and the image the model should optimized
			on.

  @see      
  
  @param    aammodel	The AAM.
  @param    s			The initial shape pose being optimized.
  @param    image 		The image on which the optimization shall
						be done.
  @param	similaritym The simlarity measure to be used
  
  @return   Nothing.
  
*/
CAAMOptimize::CAAMOptimize(const CAAMModel &aammodel, 
						   const CAAMShape &s,
						   const CDMultiBand<TAAMPixel> &image,
						   const int similaritym	) {

    m_dFit   = .0;
	m_pModel = &aammodel;	
	m_pShape = &s;	
	m_pImage = &image;
	m_iSimilarityMeasure = similaritym;

	m_dMinFit = 1e306;
}



/**

  @author   Mikkel B. Stegmann
  @version  7-7-2000

  @memo		Fuction to be optimized.

  @doc		Function providing a scalar interpretation of the 
			AAM fit based on a set of paramters.
  @see      
  
  @param    vX	The independent parameters that will be optimized.
				The first n-4 elements constitues the normal model
				parameters of the AAM. The last four are the pose 
				parameters.
 
  @return	The scalar fit of the AAM.
  
*/
inline double CAAMOptimize::EvalFunction(CDVector& vX) {
	
	int n = m_pModel->CombinedPCA().NParameters();	

	// extract the model parameters
	CDVector c(n); 
	for(int i=0;i<n;i++) c[i] = vX[i];
	
	// extract pose parameters
	CDVector pose(4), tmpPose(4);
	for(i=0;i<4;i++) pose[i] = vX[i+n]; 


	// we use the initial shape as reference, i.e. pose = [0 0 0 0]
	CAAMShape s;
	s = *m_pShape;
	s.Displace( pose );


	m_dFit = m_pModel->ModelEstimateTexDiff( *m_pImage, c, 
											 s, m_vDiff, 
											 m_iSimilarityMeasure );

	if (m_dFit<0) {
	
		// we're outside the image
		m_dFit = 1e306;
	}    

	if (m_dFit<m_dMinFit) {

		m_dMinFit = m_dFit;
		m_sMinShape = s;
		m_vMinC = c;
	}

	return m_dFit;		
}


/**

  @author   Mikkel B. Stegmann
  @version  7-7-2000

  @memo		Returns the optimisation results as c, shape and error.

  @doc		Returns the optimisation results as c, shape and error.
			This is to avoid and extra conversion after ended 
			optimisation, and worse, an extra image sampling to get
			the error.
			
  @param    c	The optimal model parameters.
				
  @param    s	The optimal shape.

  @param    s	The optimal error.
 
  @return	Nothing.
  
*/
void CAAMOptimize::OptResults( CDVector &c, CAAMShape &s, double &fit ) {

	c.Resize( m_vMinC.Length() );
	
	c = m_vMinC;
	s = m_sMinShape;
	fit = m_dMinFit;
}


