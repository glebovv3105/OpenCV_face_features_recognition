/* $Id: AAMDelaunay.cpp,v 1.1.1.1 2003/01/03 19:17:09 aam Exp $ */
#include "AAMDelaunay.h"
#include "delaunay.h"
#include <stdlib.h>
#include <stdio.h>


// import library (thus avoiding to tamper with the configuration settings)
#pragma comment( lib, "delaunay.lib" )


/**

  @author   Mikkel B. Stegmann
  @version  7-27-2000

  @memo     Constructor.
  
  @return   Nothing.
  
*/
CAAMDelaunay::CAAMDelaunay() {
	
}

/**

  @author   Mikkel B. Stegmann
  @version  7-27-2000

  @memo     Destructor.
  
  @return   Nothing.
  
*/
CAAMDelaunay::~CAAMDelaunay() {

}


/**

  @author   Mikkel B. Stegmann
  @version  7-27-2000

  @memo     Generates a triangular mesh.

  @doc      Generates a triangular mesh, where all triangles satisfies
			the Delaunay property.  

  @param    s					The input shape one wants to triangulate.
  @param    m					The output Delaunay mesh.
  @param	bConcaveCleanUp		If true convex triangles of concave shapes
								are removed (default false).
  @return   Nothing.
  
*/
void CAAMDelaunay::MakeMesh( const CAAMShape &s, CAAMMesh &m,
							 bool bConcaveCleanUp ) {

	// setup input structures for the c routine
	int nbPoints = s.NPoints();
	int nbTriangles = nbPoints*2;
	int *triangles;
	double *xVec = (double*)malloc( nbPoints*sizeof(double) );
	double *yVec = (double*)malloc( nbPoints*sizeof(double) );
	triangles = (int*)malloc( 3*nbTriangles*sizeof(int) );
	double x,y;

	assert(nbPoints>0);

	for(int i=0;i<nbPoints;i++) {
	
		s.GetPoint( i, x, y );
		xVec[i] = x;
		yVec[i] = y;
		m.Insert( CAAMPoint(x, y) );
	}
	
	int outTriangles = ATTDelaunay(xVec,yVec,nbPoints,triangles);

	assert(outTriangles<=nbTriangles);

	for(i=0;i<3*outTriangles;i+=3) {

		m.Insert( CAAMTriangle( triangles[i], 
								triangles[i+1], 
								triangles[i+2],
								&(m.Points())		) );
	}

	// deallocate
	free(xVec);
	free(yVec);
	free(triangles);


	// do clean up
	if (bConcaveCleanUp) {

		// clean up the mesh for concave triangles
		// produced by the Delaunay Triangulisation
		for(int i=0;i<m.NTriangles();i++) {
			
			CAAMTriangle &t = m.Triangles()[i];
			
			// check if the triangle belongs to the shape
			if ( !s.IsInside( t.CenterPoint() ) ) {

				// delete triangle
				m.Triangles().erase( &t );

				// correct the counter
				--i;
			}
		}
	}
}