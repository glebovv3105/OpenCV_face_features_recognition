/* $Id: AAMInitialize.cpp,v 1.5 2003/04/23 14:50:00 aam Exp $ */
#include "AAMInitialize.h"
#include "AAMOptimize.h"
#include "AAMModel.h"
#include "AAMVisualizer.h"


/**

  @author   Mikkel B. Stegmann
  @version  7-22-2000

  @memo     Constructor.

  @doc      Constructor.

  @param    model	Reference to a model.
  
  @return   Nothing.
  
*/
CAAMInitialize::CAAMInitialize( const CAAMModel &aammodel ) {

	m_pModel = &aammodel;
}



/**

  @author   Mikkel B. Stegmann
  @version  7-22-2000

  @memo     Constructor.

  @doc      Constructor.

  @param    aammodel    The model to initialize.
  
  @return   Nothing.
  
*/
CAAMInitializeStegmann::CAAMInitializeStegmann( const CAAMModel &aammodel ) {

    // setup model
    m_pModel = &aammodel;  
    m_sCentredRefshape = m_pModel->ReferenceShape();

    // center the reference shape
    double cogx, cogy; 
    m_sCentredRefshape.COG( cogx, cogy );  
	m_sCentredRefshape.Translate( -cogx, -cogy );  
    
    // setup number of iterations
    m_nIterations1stPass = 3; 
    m_nIterations2ndPass = 10;

    // setup number of refiment candidates
    m_nCandidates = 10;


    /////////////////////////////////
    // setup default search ranges
    /////////////////////////////////

    // scale
    m_vScaleSteps.Resize(3);
    m_vScaleSteps[0] = 1.0;
    m_vScaleSteps[1] = 1.1;
    m_vScaleSteps[2] = 0.9;
    
    // rotation
    m_vRotationSteps.Resize(1);
	m_vRotationSteps[0] = .0;			// radians
	//m_vRotationSteps[1] = CAAMUtil::Deg2Rad(20);
	//m_vRotationSteps[2] = CAAMUtil::Deg2Rad(-20);


    // model parameters (mean)
    CDVector cparam(1);
    cparam[0] = .0; // in standard deviations of that parameter
    m_vModelParameterSteps.push_back( cparam );

    // default grid size
    m_dXspacing = m_sCentredRefshape.Width()/5.; 
	m_dYspacing = m_sCentredRefshape.Height()/5.; 

    // calc border size
    m_dXmin = m_sCentredRefshape.MinX();
    m_dXmax = m_sCentredRefshape.MaxX();
    m_dYmin = m_sCentredRefshape.MinY();
    m_dYmax = m_sCentredRefshape.MaxY();
}



/**

  @author   Mikkel B. Stegmann
  @version  7-22-2000

  @memo     Performs a somewhat brute force initialization of an AAM.

  @doc      Performs a somewhat brute force initialization of an AAM.        
			
            Assumes one object per image as of now.			
  
  @param    image	The image beeing searched in.

  @param    s		The output shape after initialization.

  @param    c		The output model parameters after initialization.
  
  @return   0 on succes, non-zero on errors.
  
*/
int CAAMInitializeStegmann::Initialize( const CDMultiBand<TAAMPixel> &image,
										CAAMShape &s, CDVector &c ) {
    
    // seach rectangle
    double xst, xend, yst, yend;
    

    // normal search over the complete image        
    // (excluding some border due to possible scaling)
    // using the reference shape        
    double sb = 1.05;    // some border

    int iw = image.Width();
	int ih = image.Height();
    
    xst  = -sb*m_dXmin;
	xend =  iw-sb*m_dXmax; 
	yst  = -sb*m_dYmin;
    yend =  ih-sb*m_dYmax;   
    
    // first pass: find candidates
	int cfgs = 0, np = m_pModel->CombinedPCA().NParameters();
	c.Resize( np );      
    CAAMModel::CAAMOptRes res;
    CAAMInitCandidates candidates(m_nCandidates);

    // for each model parameter
	for(unsigned int mode=0;mode<m_vModelParameterSteps.size();mode++) {
        
        // for each model parameter displacement
        for(int modedisp=0;modedisp<m_vModelParameterSteps[mode].Length();modedisp++) {

            c = .0;
            // get mode standard deviation
            double stddev = sqrt( m_pModel->CombinedPCA().EigenValues()[np-mode-1] );

            // displace
            c[np-mode-1] = m_vModelParameterSteps[mode][modedisp]*stddev;

            // for each scale
		    for(int scale=0;scale<m_vScaleSteps.Length();scale++) {

                // for each rotation
                for(int rot=0;rot<m_vRotationSteps.Length();rot++) {
                
                    // for each y position
			        for(double y=yst;y<yend;y+=m_dYspacing) {

                        // for each x position
				        for(double x=xst;x<xend;x+=m_dXspacing) {

                            // apply pose
					        s = m_sCentredRefshape;
                            s.Scale( m_vScaleSteps[scale], false );   // false since COG==(0,0) for
                            s.Rotate( m_vRotationSteps[rot], false ); // centred reference shape
					    					    					       
					        // apply translation to (x,y)
					        s.Translate( x, y );                        

					        // optimize					
					        res = m_pModel->OptimizeModel( image, s, c, 
                                                           m_nIterations1stPass, 
                                                           NULL, true );                   
                            ++cfgs;

					        // record experiment
					        CAAMInitEntry e( res.SimilarityMeasure(), s, c );                                            
					        candidates.ApplyForAcceptance( e );				                        
                        }
				    }
                }
			}
		}
	}	

    // 2nd pass: refine candidates
	int nc = candidates.NCandidates();
    assert( nc>0 );    
	double min_e_fit = 1e306;
	int minPos = 0;
    CDVector minC( c.Length() );
	CAAMShape minShape;                    
	for(int cand=0;cand<nc;cand++) {		

        // get shape and model parameters for this candidate
		CDVector  c_tmp = candidates.Candidate( cand ).C();
		CAAMShape s_tmp = candidates.Candidate( cand ).Shape();
		
		// optimize to convergence        
		res = m_pModel->OptimizeModel(	image,
								        s_tmp,
								        c_tmp,
								        m_nIterations2ndPass,
                                        NULL, false );
		
        // test results
		if (min_e_fit>res.SimilarityMeasure() ) { 
            
            // best configuration obtained -> save
			min_e_fit = res.SimilarityMeasure(); 			
			minShape  = s_tmp;
			minC      = c_tmp;						
		} 
	}

    // set output shape to the best configuration
	s = minShape;

    // get the corresponding model parameters
	c = minC;

    // debug
    bool debug = true;
    if (debug) {

	    double cx, cy;
	    s.COG( cx, cy);
	    printf(	"Final COG=( %3.0f, %3.0f ), E_min=%10.2f, %i configurations searched \n", 
                cx, cy, min_e_fit, cfgs );
    }

	bool dumpCandidates = false;
	for(cand=0;cand<nc && dumpCandidates;cand++) {		

		CString fn; fn.Format( "init_cand%02i.bmp", cand );
		CAAMVisualizer::ShapeStill( image, candidates.Candidate( cand ).Shape() , fn );
	}

    return 0;
}


/**

  @author   Mikkel B. Stegmann
  @version  7-19-2000

  @memo     Allocates a pool with room for 'n' candidates.

  @doc      Allocates a pool with room for 'n' candidates.
	  
  @param    n		The number of candidates.
  
  @return   Nothing.
  
*/
CAAMInitializeStegmann::CAAMInitCandidates::CAAMInitCandidates( int n ) {

	m_iNMaxCand = n;

	m_vInitEntries.reserve( m_iNMaxCand );

}


/**

  @author   Mikkel B. Stegmann
  @version  7-19-2000

  @memo     Apply for acceptance of a new initialization hypothesis.

  @doc      Apply for acceptance of a new initialization hypothesis.
			The entry are accepted if it is better than the worst 
			candiate, or if there is less than 'n' candidates in the set.
  
  @param    e	The seach result, which is applieing for acceptance.
  
  @return   True if it is accepted, false if not.
  
*/
bool CAAMInitializeStegmann::CAAMInitCandidates::ApplyForAcceptance( const CAAMInitEntry e ) {

	// find max
	int len = m_vInitEntries.size();
	double max = -1e306;
	int maxPos = -1;

    if (e.E_fit()<.0) {
        
        // something's wrong
        // the model is probably outside the image
        return false;
    }


	if (len < m_iNMaxCand) {

		m_vInitEntries.push_back( e );
		return true;
	}

	for(int i=0;i<len;i++) {

		if ( m_vInitEntries[i].E_fit() > max ) {

			max = m_vInitEntries[i].E_fit();
			maxPos = i;
		}
	}
	
	if ( e.E_fit() < max ) {
		
		if (maxPos!=-1) {

			m_vInitEntries.erase( &(m_vInitEntries[maxPos]) );
		}

		m_vInitEntries.push_back( e );

		return true;
	} 

	return false;
}