/* $Id: AAMMesh.cpp,v 1.3 2003/01/14 17:28:51 aam Exp $ */
#include "AAMMesh.h"


/**

  @author   Mikkel B. Stegmann
  @version  11-15-2000

  @memo     Constructor.

  @doc      Constructor.
  
  @return   Nothing.
  
*/
CAAMTriangle::CAAMTriangle(	int _v1, int _v2, int _v3,
						    std::vector<CAAMPoint> *pPoints ) {	

	m_v1=_v1; 
	m_v2=_v2; 
	m_v3=_v3; 
	m_pPoints = pPoints;

    Calc_dD();
}


/**

  @author   Mikkel B. Stegmann
  @version  11-15-2000

  @memo     Cache function.

  @doc      Cache function.
  
  @return   Nothing.
  
*/
void CAAMTriangle::Calc_dD() {

    double x1, x2, x3, y1, y2, y3;

	x1 = (*m_pPoints)[m_v1].x;
	x2 = (*m_pPoints)[m_v2].x;
	x3 = (*m_pPoints)[m_v3].x;	
	y1 = (*m_pPoints)[m_v1].y;
	y2 = (*m_pPoints)[m_v2].y;
	y3 = (*m_pPoints)[m_v3].y;
	
	m_dD = -x2*y3+x2*y1+x1*y3+x3*y2-x3*y1-x1*y2;
}


/**

  @author   Mikkel B. Stegmann
  @version  11-15-2000

  @memo     Constructor.

  @doc      Constructor.
  
  @return   Nothing.
  
*/
CAAMMesh::CAAMMesh() : m_iPrevTriangle(0) {



}


/**

  @author   Mikkel B. Stegmann
  @version  11-15-2000

  @memo     Destructor.

  @doc      Destructor.
  
  @return   Nothing.
  
*/
CAAMMesh::~CAAMMesh() {


}


/**

  @author   Mikkel B. Stegmann
  @version  11-15-2000

  @memo     Adds a point to the end.

  @doc      Adds a point to the end.

  @param	p	Point to add.

  @return   Nothing.
  
*/
void CAAMMesh::Insert( const CAAMPoint &p ) {

	m_vPoints.push_back( p );
}


/**

  @author   Mikkel B. Stegmann
  @version  1-14-2003

  @memo     Replaces the points in the mesh.

  @doc      Replaces the points in the mesh with the one given by a
			shape. Preserves the triangles.

  @param    s	Shape to fecth points from.
  
  @return   Nothing.
  
*/
void CAAMMesh::ReplacePoints(const CAAMShape &s) {	

	// delete the current points
	m_vPoints.clear();

	for(int i=0;i<s.NPoints();i++) {
		Insert( s.GetPoint(i) );
	}


    for(unsigned int j=0;j<m_vTriangles.size();j++) {

        m_vTriangles[j].Calc_dD();
    }
}


/**

  @author   Mikkel B. Stegmann
  @version  11-15-2000

  @memo     Adds a triangle to the end.

  @doc      Adds a triangle to the end.

  @param	t	Triangle to add.

  @return   Nothing.
  
*/
void CAAMMesh::Insert( const CAAMTriangle &t ) {

	m_vTriangles.push_back( t );
}


/**

  @author	Mikkel B. Stegmann
  @version	02-11-2000
  
  @doc		Writes mesh structure to a matlab file containing three
			vectors:

			xTri	X-points.
			yTri	Y-points.
			Tri		Triangles defined as an (ntriangles x 3) matrix.
					Thus each row defines a triangle using three
					indices pointing to the point-vectors.

  @param	sFilename	The filename to be written. The file is
			overwritten if it already exists.  

  @return	Zero on succes, non-zero if the mesh is empty.
	
*/
int CAAMMesh::ToMatlab(const CString& sFilename) const {

	assert( NPoints()>0 );

	if ( NPoints()<1 ) return -1;

	CDVector xVec( NPoints() ), yVec( NPoints() );
	CDMatrix mTriangles( NTriangles(), 3 );

	for(int i=0;i<NPoints();i++) {
	
		xVec[i] = m_vPoints[i].x;
		yVec[i] = m_vPoints[i].y;
	}

	for(i=0;i<NTriangles();i++) {

		mTriangles[i][0] = m_vTriangles[i].V1();
		mTriangles[i][1] = m_vTriangles[i].V2();
		mTriangles[i][2] = m_vTriangles[i].V3();
	}

	xVec.ToMatlab( sFilename, "xTri", "x-positions", false );
	yVec.ToMatlab( sFilename, "yTri", "y-positions", true );
	mTriangles.ToMatlab( sFilename, "Tri", "the triangles", true );
	
	return 0;
}


/**

  @author   Mikkel B. Stegmann
  @version  11-15-2000

  @memo     Returns the total area of all triangles in the mesh.

  @doc      Returns the total area of all triangles in the mesh.
  
  @return   The area.
  
*/
double CAAMMesh::Area() const {

    double sum = .0;

    for(unsigned int i=0;i<m_vTriangles.size();i++) {

        sum += m_vTriangles[i].Area();
    }

    return sum;
}



/**

  @author   Mikkel B. Stegmann
  @version  2-14-2000

  @memo     Assignment operator.

  @doc      Assignment operator.
  
  @return   Nothing.
  
*/
CAAMMesh& CAAMMesh::operator=(const CAAMMesh &m) {
    
    m_vPoints    = m.m_vPoints;
    m_vTriangles = m.m_vTriangles;
	m_iPrevTriangle = 0;

    for(unsigned int i=0;i<m_vTriangles.size();i++) {

        // update pointer member
        m_vTriangles[i].m_pPoints = &m_vPoints;
    }

    return *this;
}


/**

  @author   Mikkel B. Stegmann
  @version  2-14-2000

  @memo     Copy constructor.

  @doc      Copy constructor.

  @param    m	Mesh to copy.
  
  @return   Nothing.
  
*/
CAAMMesh::CAAMMesh( const CAAMMesh &m ) : m_iPrevTriangle(0) {

    *this = m;    
}


/**

  @author   Mikkel B. Stegmann
  @version  2-14-2000
  
  @doc		Returns true if the point 'p' is inside the triangle.  
  
  @param    p	The point to test  
  
  @return   True if the point is inside, otherwise false.
  
*/
bool CAAMTriangle::IsInside(const CAAMPoint &p) const {

	double alpha, beta, gamma;

	return IsInside( p, alpha, beta, gamma );
}


/**

  @author   Mikkel B. Stegmann
  @version  2-14-2000
  
  @doc		Performs a hit test on the point p. If p is inside -- the
			position of p relative to the triangle is returned.

			The relative position is:

				p = alpha*p1 + beta*p2 + gamma*p3

			Where p1-3 is the three points of the triangle.  
  
  @param    p		The point to test  
			alpha	Relative x1 position.
			beta	Relative x2 position.
			gamma	Relative x3 position.
  
  @return   True if the point is inside, otherwise false.
  
*/
bool CAAMTriangle::IsInside(const CAAMPoint &p, 
							double &alpha, 
							double &beta, 
							double &gamma) const {

	double x, y, x1, x2, x3, y1, y2, y3;
	bool inSide;

	x = p.x;
	y = p.y;	

	x1 = (*m_pPoints)[m_v1].x;
	x2 = (*m_pPoints)[m_v2].x;
	x3 = (*m_pPoints)[m_v3].x;

	// perform bounding box test on x
	if ( x<AAM_MIN3(x1,x2,x3) || x>AAM_MAX3(x1,x2,x3) ) return false;

	y1 = (*m_pPoints)[m_v1].y;
	y2 = (*m_pPoints)[m_v2].y;
	y3 = (*m_pPoints)[m_v3].y;

	// perform bounding box test on y
	if ( y<AAM_MIN3(y1,y2,y3) || y>AAM_MAX3(y1,y2,y3) ) return false;

	alpha = -y*x3+y3*x-x*y2+x2*y-x2*y3+x3*y2;
	beta  = y*x3-x1*y-x3*y1-y3*x+x1*y3+x*y1;
	gamma = x*y2-x*y1-x1*y2-x2*y+x2*y1+x1*y;	

	inSide = alpha>=0.0 && alpha <=m_dD &&
			 beta >=0.0 && beta <=m_dD  &&
			 gamma>=0.0 && gamma<=m_dD;
    
	if (inSide) {

		alpha /= m_dD;
		beta  /= m_dD;		
		gamma /= m_dD;
	}

	return inSide;
}



/**

  @author   Mikkel B. Stegmann
  @version  4-4-2000
  
  @doc		Returns the center point of the triangle.		  

  @return   Nothing.
  
*/
CAAMPoint CAAMTriangle::CenterPoint( ) const {

	CAAMPoint out;

	out.x = ((*m_pPoints)[m_v1].x+(*m_pPoints)[m_v2].x+(*m_pPoints)[m_v3].x)/3.0;
	out.y = ((*m_pPoints)[m_v1].y+(*m_pPoints)[m_v2].y+(*m_pPoints)[m_v3].y)/3.0;

	return out;
}


/**

  @author   Mikkel B. Stegmann
  @version  11-15-2000

  @memo     Returns the area.

  @doc      Returns the area.
  
  @return   The area.
  
*/
double CAAMTriangle::Area() const {

    // get points
    double x1, x2, x3, y1, y2, y3;
    x1 = (*m_pPoints)[m_v1].x;
	x2 = (*m_pPoints)[m_v2].x;
	x3 = (*m_pPoints)[m_v3].x;
    y1 = (*m_pPoints)[m_v1].y;
	y2 = (*m_pPoints)[m_v2].y;
	y3 = (*m_pPoints)[m_v3].y;

    // sidelengths
    double a = sqrt( (x1-x2)*(x1-x2)+(y1-y2)*(y1-y2) );
    double b = sqrt( (x2-x3)*(x2-x3)+(y2-y3)*(y2-y3) );
    double c = sqrt( (x3-x1)*(x3-x1)+(y3-y1)*(y3-y1) );
    
    // perimeter
    double s = (a+b+c)/2;

    // Return the area.
    //
    // This formula is attributed to Heron of Alexandria 
    // but can be traced back to Archimedes :-)
    return sqrt( s*(s-a)*(s-b)*(s-c) );
}