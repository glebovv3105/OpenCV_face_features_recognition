/* $Id: AAMShape.cpp,v 1.3 2003/04/23 14:50:01 aam Exp $ */
#include "AAMShape.h"
#include "AAMUtil.h"
#include "AAMDelaunay.h"


// macros
#define DEC_TO_ZERO(a) (a==0 ? 0 : a-1)

// import library (thus avoiding to tamper with the configuration settings)
#ifdef _DEBUG
#pragma comment( lib, "DivaMatrixDB.lib" )
#else
#pragma comment( lib, "DivaMatrix.lib" )
#endif 



/**

  @author   Mikkel B. Stegmann
  @version  5-8-2000

  @memo     Default constructor.

  @doc      Defaults to absolute shape coordinates 
			and sets the number of points to zero.

            Notice that the user defined fields is *not* allocated by default.

            In order to preserved memory the user must to this explicitly by
            using AllocateUserFields();
                
  @return   Nothing.
  
*/
CAAMShape::CAAMShape() :

    m_iNbPoints( 0 ),
    m_szHostImage( "" ),        
    m_szASFVer( "1.0" ),
    m_bAbsPointCoordinates( true )  
{
    m_pData = &((*this)[0]);
}


/**

  @author   Mikkel B. Stegmann
  @version  5-8-2000

  @memo     Constructs a shape with 'nbPoints' points. 

  @doc      Constructs a shape with 'nbPoints' points. 
			Defaults to absolute point coordinates and single closed path connectivity.

  @see      SetClosedPathConnectivity
  
  @param    nbPoints	The number of points the shape should contain.  
  
  @return   Nothing.
  
*/
CAAMShape::CAAMShape(int nbPoints) {

    m_szHostImage = "";
	m_bAbsPointCoordinates = true;
	m_iNbPoints = nbPoints;
	this->Resize(2*m_iNbPoints);

	SetClosedPathConnectivity();
}


/**

  @author   Mikkel B. Stegmann
  @version  5-8-2000

  @memo     Constructs a shape from a vector. Assumes closed path connectivity.
  
  @doc      Constructs a shape from a vector.
            Defaults to absolute point coordinates and single closed path connectivity.

  @see      SetClosedPathConnectivity
  
  @param    v	The input vector.
    
  @return   Nothing.
  
*/
CAAMShape::CAAMShape( const CDVector &v ) { 

    m_szHostImage = "";

	m_bAbsPointCoordinates = true;	
	assert( v.Length()%2==0 );

	Resize( v.Length() ); 
	(*(CVisDVector*)(this)) = v; 

    m_pData = &((*this)[0]);
	
	SetClosedPathConnectivity();
}


/**

  @author   Mikkel B. Stegmann
  @version  5-8-2000

  @memo     Copy contructor.
  
  @doc      Copy contructor.  
  
  @param    s	The input shape.
    
  @return   Nothing.
  
*/
CAAMShape::CAAMShape( const CAAMShape &s ) {     

    this->CopyData( s );
}


/**

  @author   Mikkel B. Stegmann
  @version  4-26-2000

  @memo     Sets all point to be connected in one closed path.

  @doc		Manipulates the aux point info such that the shape points 
			is interpreted as one closed outer path defined clock-wise
			with repect to it's normals.
			
    
  @return   Nothing.
  
*/
void CAAMShape::SetClosedPathConnectivity() {

	// set closed path connectivity
	m_vPointAux.resize( m_iNbPoints );
	for(unsigned int i=0;i<m_vPointAux.size();i++) {

		// the first and only path
		m_vPointAux[i].m_iPathID		= 0;	
		
		// closed path, outer point etc.
		m_vPointAux[i].m_iTypeFlags		= 0;	

		// make closed path
		m_vPointAux[i].m_iConnectFrom	= i==0 ? m_iNbPoints-1 : i-1;
		
		// make closed path
		m_vPointAux[i].m_iConnectTo		= i==m_iNbPoints-1 ? 0 : i+1;
	}			
}


/**

  @author   Mikkel B. Stegmann
  @version  4-26-2000

  @memo     Assignment operator (double).

  @doc		Sets all x and y compoments of a shape equal to a double.
			[Actually only used when calculating a mean shape -- 
			since we want to accumulate in an empty shape].
  
  @param	value	The value to set all shape points to. 
  
  @return   Nothing.
  
*/
CDVector& CAAMShape::operator=(double value) {
	
	CVisDVector& vTmp = *this; 
	vTmp = value; 	
	return *this; 
}


/**

  @author   Mikkel B. Stegmann
  @version  5-9-2000

  @memo     Assignment operator (CAAMShape).

  @doc      Set one shape equal another.
    
  @param    s	The shape to copy.
  
  @return   Nothing.
  
*/
CAAMShape& CAAMShape::operator=(const CAAMShape &s) {
		
   	// copy all data from s
    CopyData( s );    
	
	return *this; 
}


/**

  @author   Mikkel B. Stegmann
  @version  10-20-2000

  @memo     Copies all data from a shape to this.

  @doc      Copies all data from a shape to this.
            Called from the assignment operator.
  
  @param    s   Shape to copy data from.  
  
  @return   Nothing,
  
*/
void CAAMShape::CopyData( const CAAMShape &s ) {

    // copy vector data (i.e. the point coordinates)
	this->Resize( s.Length() );
	(*(CVisDVector*)(this)) = (CVisDVector)s; 

    m_pData = &((*this)[0]);

    // copy class data members
    this->m_iNbPoints            = s.m_iNbPoints;
	this->m_bAbsPointCoordinates = s.m_bAbsPointCoordinates;
	this->m_vPointAux            = s.m_vPointAux;
    this->m_szHostImage          = s.m_szHostImage;
    this->m_szASFVer             = s.m_szASFVer;
    this->m_vUser1               = s.m_vUser1;
    this->m_vUser2               = s.m_vUser2;
    this->m_vUser3               = s.m_vUser3;        
}


/**

  @author   Mikkel B. Stegmann
  @version  4-26-2000

  @memo     Assignment operator (CAAMShape).

  @doc      Set the shape to be equal an xxx-yyy formatted vector.
			NOTE: this method does not manipulate any connectivity
			(i.e. point aux) info. So, if the shape beforehand was 
			empty one should call SetClosedPathConnectivity() afterwards
			this call to obtain sensible point aux info.
   
  @param    vIn	xxx-yyy formatted vector.
    
  @return   Nothing.
  
*/
CDVector& CAAMShape::operator=(const CVisDVector &vIn) {
	
	CVisDVector& vTmp = *this; 
	vTmp = vIn; 	

    m_pData = &((*this)[0]);

	return *this; 
}


/**

  @author   Mikkel B. Stegmann
  @version  5-15-2000

  @memo     Shape destructor.
    
*/
CAAMShape::~CAAMShape() { 

}


/**

  @author   Mikkel B. Stegmann
  @version  3-15-2000

  @memo     Rotates the shape.

  @doc		Rotates the shape 'theta' radians.
  
  @param    theta		Rotation angle in radians.
  @param    aroundCOG	If true the rotation is being done around the 
						cog of the shape instead of around the global
						center.
  
  @return   Nothing.
  
*/
void CAAMShape::Rotate( const double theta, const bool aroundCOG ) {
	
	double x, y, cx, cy;
	
	// set up rotation matrix
	double c00 =  cos( theta );
	double c01 = -sin( theta );
	double c10 =  sin( theta );
	double c11 =  cos( theta );

	if (aroundCOG) {
	
		COG( cx, cy );
		Translate( -cx, -cy );
	}
	
	for(int i=0;i<m_iNbPoints;i++) {

		x = (*this)[i];
		y = (*this)[i+m_iNbPoints];
		(*this)[i]				= c00*x+c01*y;
		(*this)[i+m_iNbPoints]	= c10*x+c11*y;				
	}
	
	if (aroundCOG) {
		
		Translate( cx, cy );
	}
}


/**

  @author   Mikkel B. Stegmann
  @version  5-15-2000

  @memo     Translates the shape.
  
  @param    p	The offset to translate.
    
  @return   Nothing.
  
*/
void CAAMShape::Translate( const CAAMPoint &p ) {

	this->Translate( p.x, p.y );
}


/**

  @author   Mikkel B. Stegmann
  @version  5-15-2000

  @memo     Translates the shape.
  
  @param    x	X-translation.
  @param    y	Y-translation.
  
  @return   Nothing.
  
*/
void CAAMShape::Translate( const double x, const double y ) {

	for(int i=0;i<m_iNbPoints;i++) {
	
		(*this)[i]			   += x;
		(*this)[i+m_iNbPoints] += y;
	}
}


/**

  @author   Mikkel B. Stegmann
  @version  5-15-2000

  @memo     Returns the 2-norm of this shape centralized.
    
  @return   The 2-norm.
  
*/
double CAAMShape::ShapeSize() const { 
    
    CAAMShape tmp(*this);
    
    double x,y;	
	tmp.COG( x, y );
	tmp.Translate( -x, -y );
	
    return tmp.Norm2();
}



/**

  @author   Mikkel B. Stegmann
  @version  5-15-2000

  @memo     Find the maximum x component of the shape.
  
  @return   The x-maximum.
  
*/
double CAAMShape::MinX() const {
	
	double val, min = 1.7E+308;

	for(int i=0;i<m_iNbPoints;i++) {	

		val = (*this)[i];
		min = val<min ? val : min;
	}
	return min;
}


/**

  @author   Mikkel B. Stegmann
  @version  5-15-2000

  @memo     Find the maximum x component of the shape.
  
  @return   The x-maximum.
  
*/
double CAAMShape::MaxX() const {
	
	double val, max = -1.7E+308;

	for(int i=0;i<m_iNbPoints;i++) {	

		val = (*this)[i];
		max = val>max ? val : max;
	}
	return max;
}


/**

  @author   Mikkel B. Stegmann
  @version  5-15-2000

  @memo     Find the minimum y component of the shape.
  
  @return   The y-minimum.
  
*/
double CAAMShape::MinY() const {
	
	double val, min = 1.7E+308;

	for(int i=0;i<m_iNbPoints;i++) {	

		val = (*this)[i+m_iNbPoints];
		min = val<min ? val : min;
	}
	return min;
}

/**

  @author   Mikkel B. Stegmann
  @version  5-15-2000

  @memo     Find the maximum y component of the shape.
  
  @return   The y-maximum.
  
*/
double CAAMShape::MaxY() const {
	
	double val, max = -1.7E+308;

	for(int i=0;i<m_iNbPoints;i++) {	

		val = (*this)[i+m_iNbPoints];
		max = val>max ? val : max;
	}
	return max;
}


/**

  @author   Mikkel B. Stegmann
  @version  3-15-2000

  @memo     Scales the shape.

  @doc		Scales the shape.
  
  @param    s			Scale factor.
  @param    aroundCOG	If true the scale is being done around the 
						cog of the shape instead of around the global
						center.
  
  @return   Nothing.
  
*/
void CAAMShape::Scale( const double s, const bool aroundCOG ) {

	double cx, cy;

	if (aroundCOG) {
	
		COG( cx, cy );
		Translate( -cx, -cy );
	}
	
	for(int i=0;i<2*m_iNbPoints;i++) {
	
		(*this)[i] *= s;		
	}

	if (aroundCOG) {

		Translate( cx, cy );
	}
}


/**

  @author   Mikkel B. Stegmann
  @version  3-15-2000

  @memo     Calculates the center of gravity of the shape.

  @doc		Calculates the center of gravity of the shape
			(actually it's the center of the centroid).
  
  @param    p	cog output.
    
  @return   Nothing.
  
*/
CAAMPoint CAAMShape::COG() const {

	CAAMPoint p;

	COG( p.x, p.y );

	return p;
}


/**

  @author   Mikkel B. Stegmann
  @version  3-15-2000

  @memo     Calculates the center of gravity of the shape.

  @doc		Calculates the center of gravity of the shape
			(actually it's the center of the centroid).
  
  @param    x	X cog output.
  @param    y	X cog output.
  
  @return   Nothing.
  
*/
void CAAMShape::COG( double &x, double &y ) const {

	assert( m_iNbPoints>0 );

	double xSum, ySum;

	xSum = ySum = 0.0;
	for(int i=0;i<m_iNbPoints;i++) {

		xSum += (*this)[i];
		ySum += (*this)[i+m_iNbPoints];
	}
	
	x = xSum/m_iNbPoints;
	y = ySum/m_iNbPoints;	
}


/**

  @author	Mikkel B. Stegmann
  @version	02-08-2000

  @memo		Normalize to unit scale and translates COG to origo.

  @doc		Normalizes the shape by translating it's center of gravity
			to origo and scale by the reciprocal of the 2-norm.	

  @return	The 2-norm of the shape seen as a 2*nbPoint vector after
			the translation to origo.
*/
double CAAMShape::Normalize() {

	double x,y;
	
	COG( x, y );
	Translate( -x, -y );

	// Normalize the vector to unit length, using the 2-norm.
	double norm = Norm2();
	Scale( 1./norm );

	return norm;
}


/**

  @author	Mikkel B. Stegmann
  @version	02-10-2000

  @doc		Overwrites the i'th point. 

  @return	Zero.
*/
int CAAMShape::SetPoint( int i, const double &x, const double &y) {
	
	assert( i<m_iNbPoints );	

	(*this)[i]             = x;
	(*this)[i+m_iNbPoints] = y;

	return 0;
}


/**

  @author	Mikkel B. Stegmann
  @version	02-10-2000

  @doc		Overwrites the i'th point. If the point doesn't exists,
			non-zero is returned.

  @return	Zero on success, non-zero if the point doesn't exists.
*/
int CAAMShape::SetPoint( const int i, const CAAMPoint &p ) {

	return SetPoint( i, p.x, p.y );
}


/**

  @author	Mikkel B. Stegmann
  @version	02-10-2000

  @memo		Returns the i'th point. 

  @doc		Returns the i'th point. 

  @return	Zero.
*/
int CAAMShape::GetPoint( int i, double &x, double &y) const  {
 
	assert( i<m_iNbPoints );
    
	x = (*this)[i];
	y = (*this)[i+m_iNbPoints];

	return 0;
}


/**

  @author	Mikkel B. Stegmann
  @version	02-10-2000

  @memo		Returns the i'th point.

  @doc		Returns the i'th point.

  @return	The i'th point.
*/
CAAMPoint CAAMShape::GetPoint( int i ) const  {

	CAAMPoint out;

	GetPoint( i, out.x, out.y );

	return out;
}



/**

  @author   Mikkel B. Stegmann
  @version  4-26-2000

  @memo		Change number of shape points in the shape.
  @doc      Change number of shape points in the shape.
			Note: Destroys *all* current point data and point info data.
  @see      
  
  @param    length
  @param	storage
  
  @return   Nothing.
  
*/
void CAAMShape::Resize(int length, double *storage) { 
	
	assert( length%2==0 );
	CVisDVector::Resize(length,storage);
	m_iNbPoints = length/2;

    m_pData = &((*this)[0]);

	SetClosedPathConnectivity();
}



/**

  @author	Mikkel B. Stegmann
  @version	03-06-2000

  @memo		Returns the transformation that aligns this to 'ref' with respect to pose.
  	
  @doc		Returns the transformation that aligns this to 'ref' with respect to pose.

  @param	ref	The reference shape.

  @return	Nothing.
*/

void CAAMShape::AlignTransformation( const CAAMShape &ref, double &scale, 
                                     double &theta, CAAMPoint &t ) const {
    	
    CAAMShape refCpy( ref );
    CAAMShape thisCpy( *this );
	double x,y;
	
	// move thisCpy and refCpy to origo
	thisCpy.COG( t.x, t.y );
	thisCpy.Translate( -t.x, -t.y );
	refCpy.COG( x, y );
	refCpy.Translate( -x, -y );
    t.x = x-t.x;
    t.y = y-t.y;

	// normalize scale, using the 2-norm
    double this_size = thisCpy.Norm2();
	double ref_size = refCpy.Norm2();
    scale = refCpy.Norm2()/thisCpy.Norm2();
	thisCpy.Scale( scale );	

	// align rotation between thisCpy and refCpy		
	theta = -thisCpy.GetRotation( refCpy );	
}



/**

  @author	Mikkel B. Stegmann
  @version	03-06-2000

  @memo		Aligns this to 'ref' with respect to pose.
  	
  @doc		Aligns this to 'ref' with respect to pose.

  @param	ref			The reference shape.

  @param	pTheta		Optional pointer to return the rotation
						carried out on this.

  @return	The 2-norm of the this shape seen as a 2*nbPoint vector after
			the translation to origo but before the scale done to fit
			'ref'.
*/
double CAAMShape::AlignTo( const CAAMShape &ref, double *pTheta ) {

	// make a copy of 'ref'
	CAAMShape refCpy( ref );
	double x,y;
	
	// move this and refCpy to origo
	this->COG( x, y );
	this->Translate( -x, -y );
	refCpy.COG( x, y );
	refCpy.Translate( -x, -y );

	// normalize scale, using the 2-norm
	double this_size = this->Norm2();
	double ref_size = refCpy.Norm2();
	this->Scale( ref_size/this_size );	

	// align rotation between this and refCpy	
	double theta;
	theta = this->GetRotation( refCpy );	
	this->Rotate( -theta );	

	if (pTheta) { 

		*pTheta = -theta;
	}
	
	// translate this to ref origo
	this->Translate( x, y );	

	return this_size;
}


/**

  @author	Mikkel B. Stegmann
  @version	02-09-2000

  @memo		Returns the rotation between ref and this (in radians).
  
  @doc		Get the rotation between two shapes by minimizing the sum of
			squared point distances, as described by Goodall (and Bookstein)
			using Singular Value Decomposition (SVD).

			Note that both shapes must be normalized with respect to scale and
			position beforehand. This could be done by using
			CAAMSAhape::Normalize(). 

  @return	The estimated angle, theta, between the two shapes.
				
*/
double CAAMShape::GetRotation( const CAAMShape &ref ) const { 

	assert( ref.NPoints() == this->NPoints() );	

	int nbPoints = ref.NPoints();

	CVisDMatrix mRef( nbPoints, 2 ), mS( nbPoints, 2 );
	CVisDMatrix res( 2, 2 );
	int i;
	const int X = 0;
	const int Y = 1;

	// get data as matrices ( nbPoints x 2 columns )	
	for(i=0;i<nbPoints;i++) {

		mRef[i][X] = ref[i];
		mRef[i][Y] = ref[i+nbPoints];		

		mS[i][X] = (*this)[i];
		mS[i][Y] = (*this)[i+nbPoints];		
	}

	// calculate the rotation by minimizing the sum of squared
	// point distances
	res = mRef.Transposed()*mS;
	CVisDVector S( 2 );
	CVisDMatrix U( 2, 2 ), V( 2, 2 );
	VisDMatrixSVD( res, S, U, V );
	res = V*U.Transposed();

	// res holds now a normal 2x2 rotation matrix
	double angle;
	double cos_theta = res[0][0];
	double sin_theta = res[1][0];
	const double epsilon = 1e-12;

	if ( 1.0-cos_theta<epsilon ) {

		// cos_theta=1  =>  shapes are already aligned
		angle = 0;
		return angle;
	}     
    if ( fabs(cos_theta)<epsilon ) {

        // cos_theta=0  =>  90 degrees rotation
        return M_PI/2;
    }

	if ( 1.0+cos_theta<epsilon ) {

        // cos_theta=-1  =>  180 degrees rotation
		angle = M_PI;
	} else {

		// get the rotation in radians
		double a_cos = acos( cos_theta );
		double a_sin = asin( sin_theta );

		if (a_sin<0) {
		
			// lower half of the unit circle
			angle = -a_cos;
		} else {

			// upper half of the unit circle
			angle = a_cos;
		}
	}

	return angle;
}


/**

  @author   Mikkel B. Stegmann
  @version  3-15-2000

  @memo     Reads a shape from a file.  			
  
  @param    fh		An open file handle.  
  
  @return   Nothing.
  
*/
void CAAMShape::FromFile( FILE *fh ) {

	assert(fh!=NULL);

	// read data
	CDVector::FromFile( fh );

	// update 'm_iNbPoints'
	assert( Length()%2==0 );
	m_iNbPoints = Length()/2;	

	// read aux point data
	for(int i=0;i<m_iNbPoints;i++) {

		CAAMPointInfo pi;
		fread( &pi, sizeof(CAAMPointInfo), 1, fh );
		m_vPointAux.push_back( pi );
	}

    char buf[256];
    for(int p=0;p<256;p++) buf[p] = fgetc( fh );       
    m_szHostImage = buf;

    // read abs/rel status
    int isAbs;
    fread( &isAbs, sizeof(int), 1, fh );
    m_bAbsPointCoordinates = isAbs==1;

    m_bAbsPointCoordinates = true;
   
    // restore un-safe pointer to the first point data element
    m_pData = &((*this)[0]);
}


/**

  @author   Mikkel B. Stegmann
  @version  5-4-2000

  @memo     Write the shape to a binary file.
 
  @doc		Write the shape to a binary file.
  
  @see      FromFile
  
  @param    sFilename	Destination filename.
    
  @return   Nothing.
  
*/
void CAAMShape::ToFile( FILE *fh ) const {

	// write point data
	CDVector::ToFile( fh );

	assert( m_iNbPoints==m_vPointAux.size() );

	// write aux point data
	for(int i=0;i<m_iNbPoints;i++) {

		fwrite( &(m_vPointAux[i]), sizeof(CAAMPointInfo), 1, fh );
	}
    
    char buf[256];
    strcpy( buf, m_szHostImage );
    for(int p=0;p<256;p++) fputc( buf[p], fh ); 

    // abs/rel status
    int isAbs;
    isAbs = m_bAbsPointCoordinates ? 1 : 0;
    fwrite( &isAbs, sizeof(int), 1, fh );   
}


/**

  @author   Mikkel B. Stegmann
  @version  3-15-2000

  @memo     Converts pose parameters: scale, theta, tx, ty to a pose vector.

  @doc      Converts pose parameters: scale, theta, tx, ty to a pose vector.
			The pose vector will be in the format:
			
				[ s, theta, tx, ty ] 

			where s = scale-1				

  @see      PoseVec2Param, Displace   
  
  @param    scale	Scale input.
  @param    theta	Rotational input.
  @param    tx		X translation input.
  @param    ty		Y translation input.
  @param    poseVec	The output pose vector.  
  
  @return   Nothing.
  
*/
void CAAMShape::Param2PoseVec( const double scale, const double theta, 
							   const double tx, const double ty,
							   CDVector &poseVec ) {
       
	poseVec.Resize( 4 );

    if (1) {
        
        // MBS pose representation
        poseVec[0] = scale-1.;
        poseVec[1] = theta;
        poseVec[2] = tx;
        poseVec[3] = ty;

    } else {

        // traditional AAM pose representation
        // (don't work with the gradient learning method)
        /*

        The pose vector will be in the format:
			
				[ s_x, s_y, tx, ty ] 

			where

				s_x = scale*cos( theta )-1
				s_y = scale*sin( theta )
        */
	    poseVec[0] = scale*cos( theta )-1.;
	    poseVec[1] = scale*sin( theta );
	    poseVec[2] = tx;
	    poseVec[3] = ty;

    }
}


/**

  @author   Mikkel B. Stegmann
  @version  3-27-2000

  @memo     Converts a pose vector to pose parameters: scale, theta, tx, ty.

  @doc      Converts a pose vector to pose parameters: scale, theta, tx, ty.
			The pose vector are expected to be in the format:
			
				[ s, theta, tx, ty ] 

			where s = scale-1				

  @see      Param2PoseVec, Displace
  
  @param    poseVec	The input pose vector.  
  @param    scale	Scale output.
  @param    theta	Rotational output.
  @param    tx		X translation output.
  @param    ty		Y translation output.
  
  @return   
  
*/
void CAAMShape::PoseVec2Param(	const CDVector &poseVec, 
								double &scale, double &theta, 
								double &tx, double &ty			) {

	assert( poseVec.Length()==4 );

    if (1) {

        // MBS pose representation        
        scale = poseVec[0]+1.;
        theta = poseVec[1];
        tx    = poseVec[2];
        ty    = poseVec[3];

    } else {

        // traditional AAM pose representation
        // (don't work with the gradient learning method)
        /*

        The pose vector are expected to be in the format:
			
				[ sx, sy, tx, ty ] 

			where

				sx = scale*cos( theta )-1
				sy = scale*sin( theta )
        */

	    // find scale
	    scale = sqrt( (1.+poseVec[0])*(1.+poseVec[0])+poseVec[1]*poseVec[1] );

	    // find rotation	
	    theta = atan2( poseVec[1], (1.+poseVec[0]) );
	    
	    tx = poseVec[2];
	    ty = poseVec[3];
    }
}



/**

  @author   Mikkel B. Stegmann
  @version  3-15-2000

  @memo     Displaces the shape around it's center of gravity.

  @doc      Displaces the shape around it's center of gravity using a
			displacement vector.

  @see      PoseToParam
  
  @param    poseVec		The input pose vector.  
  
  @return   Nothing.
  
*/
void CAAMShape::Displace( const CDVector &poseVec ) {

	double scale, theta, tx, ty, cx, cy;

	// convert pose vector
	this->PoseVec2Param( poseVec, scale, theta, tx, ty );

	// translate to origin
	this->COG( cx, cy );
	this->Translate( -cx, -cy );
	
	// scale, rotate and translate
	this->Scale( scale );
	this->Rotate( theta );

	// translate the specified amount plus back to c.o.g.
	this->Translate( cx+tx, cy+ty );
}


/**

  @author   Mikkel B. Stegmann
  @version  5-4-2000

  @memo     Tests if the point 'p' is inside the path starting at position
			'path_start'. Rarely used. 

  @doc      Tests if the point 'p' is inside the path starting at position
			'path_start'. Rarely used. Primary a helper function to
			IsInside.

  @see      IsInside
  
  @param    p			The point to test for.
  @param    path_start	The point index where a path starts
  
  @return   True if 'p' is inside
  
*/
bool CAAMShape::IsInsidePath( const CAAMPoint &p, const int path_start ) const {


	double x1, x2, y1, y2, dX, dY, slope, b;
	int nEdgeHit, path_id;		

	// save the path id
	path_id = m_vPointAux[path_start].m_iPathID;

	// test if the path should be an open path
	if ( m_vPointAux[path_start].IsClosed() == false ) {

		// it's open -> we can't be inside
		return false;
	}


	if ( m_vPointAux[path_start].m_iConnectTo  ==
		 m_vPointAux[path_start].m_iConnectFrom		) {

		// the path consist of single points -> we 'inside'
		return true;		
	}


	// do the test
	nEdgeHit = 0;
	int pathlen = PathLen(path_start);
	for(int i=path_start;i<path_start+pathlen;i++) {

		// test the path id
		if ( path_id != m_vPointAux[i].m_iPathID ) {

			// we've reached the end of the path
			// -> stop
			break;
		}
		
		if (m_vPointAux[i].m_iConnectTo==i) {

			// there is not connection -> continue
			continue;
		}

		// get points
		GetPoint( i, x1, y1 );				
		GetPoint( m_vPointAux[i].m_iConnectTo , x2, y2 );
		
		if( AAM_MIN2(x1,x2)>=p.x || AAM_MAX2(x1,x2)<p.x) {

			// no intersection
			continue;
		}

		dX=x2-x1;
		dY=y2-y1;

		if ( dX==0.0 ) { 

			// edge is horisontal -> no intersection
			// since we test for intersection on the line
			// from 'p' to (p.x, infinity), that is upwards
			// to infinity
			continue;
		}

		if ( dY==0.0 ) {

			// edge is vertical
			if(p.y<=y1) {
				
				nEdgeHit++;				
			}
			continue;
		}

		slope = dX/dY;
		b = x1-slope*y1;

		if ( slope>.0 ) {

			// increasing line
			if(p.x>=slope*p.y+b) {

				nEdgeHit++;
				continue;
			}
		} else {

			// decreasing line
			if( p.x<=slope*p.y+b ) {

				nEdgeHit++;
				continue;
			}
		}
	}

	// heureka(!) is edgehit is an odd number?
	// if the line from 'p' to (p.y,infinity)
	// intersects an odd number of edges, 'p'
	// is inside in n-polygon
	return (nEdgeHit%2)!=0;		
}


/**

  @author   Mikkel B. Stegmann
  @version  4-4-2000

  @memo		Tests if the point 'p' belongs to the shape. 

  @doc      Tests if the point 'p' belongs to the shape. In the current 
			version all shape points are assumes to be ordered points in
			a n-point polygon with no holes.
			For example used to clean up the meshes (removing unwanted 
			triangles from the triangulisation).
  
  @param    'p'			The test point.

  @param	bBoundTest	Flags wheather a bounding box test should be
						performed prior to the exact test.
    
  @return   True if 'p' is inside.
  
*/
bool CAAMShape::IsInside( const CAAMPoint &p, bool bBoundTest ) const {
	
	bool isInside;

	if (bBoundTest) {

		// do bounding box test
		if ( p.x > MaxX() || p.x < MinX() ||
			 p.y > MaxY() || p.y < MinY() ) {

			return false;
		}
	}

	// get paths
	std::vector<int> paths = this->GetPaths();

	// test each path
	isInside = false;
	for(unsigned int i=0;i<paths.size();i++) {

		// for each path
		if(	this->IsInsidePath( p, paths[i] ) ) {

			// we were inside a path
			isInside = true;

			// investigate further
			bool isHole = m_vPointAux[ paths[i] ].IsHole();									

			if (isHole) {

				// oops we were inside a hole and thus
				// can never be inside the shape				
				return false;
			}
		}
	}

	return isInside;	
}


/**

  @author   Mikkel B. Stegmann
  @version  4-4-2000

  @memo		Expands the shape (contraction can be done by using a negative nPixels).

  @doc      Expands the shape by moving each model point 'nPixels' perperdicular
			to the shape contour (that is: along the model point normal).

			This function will expand each outer (closed) path of the shape.
			
			No tests for crossing contours are being made as of now.

  @param    nPixels		The number of pixel to expand the shape with.
    
  @return   Nothing.
  
*/
void CAAMShape::Expand( int nPixels ) {

	CAAMPoint p_out, p_in;	

	std::vector<int> paths;

	paths = this->GetPaths();

	for(unsigned int i=0;i<paths.size();i++) {

		// check type
		const CAAMPointInfo &pi = this->PointAux()[ paths[i] ];			

		if ( pi.IsOuterEdge() && pi.IsClosed() ) {

			for(int i=0;i<m_iNbPoints;i++) {

				this->Normal( i, p_out, p_in, abs(nPixels) );
				this->SetPoint( i, nPixels>0 ? p_out : p_in );			
			}
		}
	}
}


/**

  @author   Mikkel B. Stegmann
  @version  4-4-2000

  @memo		Finds the normal to the i'th point on the shape.

  @doc      Finds the normal to the i'th point on the shape.
			If the point should be a single point the normal points
			is defined to be equal to the point it self.
			
  @param    i		Index of point.
  @param	p1		Reference to the outside normal point.
  @param	p2		Reference to the inside normal point.
  @param    dist    The desired distance from p[1|2] to the i-th point.
    
  @return   Nothing.
  
*/
void CAAMShape::Normal( const int i, CAAMPoint& p1, CAAMPoint& p2, const double dist ) const {

	double theta, theta0, theta1;	
	CAAMPoint p;
	int from, to;	

	// get the point
	p = this->GetPoint( i );

	// if the point is an endpoint the point itself are used as neighbor
    from = m_vPointAux[i].m_iConnectFrom;
    to = m_vPointAux[i].m_iConnectTo;

    // check for connection
    assert( !(m_vPointAux[i].m_iConnectFrom==i && m_vPointAux[i].m_iConnectTo==i) );


	// get connected points	
	p1 = this->GetPoint( from );
	p2 = this->GetPoint( to );
	
	// find angles (why not use the dot product..should be faster...???)
	theta0 = atan2( p1.y-p.y, p1.x-p.x );
	theta1 = atan2( p2.y-p.y, p2.x-p.x );
	theta = (theta0+theta1)/2.;	

	// ensure that p1 is always the outside point
	if ( theta0>theta1 ) theta += M_PI;

	// calc output normal points
	p1.x = p.x + dist*cos(theta);
	p1.y = p.y + dist*sin(theta);	
	p2.x = p.x - dist*cos(theta);
	p2.y = p.y - dist*sin(theta);
}


/**

  @author   Mikkel B. Stegmann
  @version  12-6-2000

  @memo     Displaces the i-th point along the normal.

  @doc      Displaces the i-th point along the normal. 
  
  @see      Normal
  
  @param    i       Index of point.
  @param    dist    The distance to move the point 
                    (>0 move point outwards, <0 inwards).
  
  @return   Nothing.
  
*/
void CAAMShape::NormalDisplacement( const int i, const double dist ) {

    CAAMPoint p1, p2;	

    // get point on normal
    this->Normal( i, p1, p2, dist );

	// replace the i-th point
	this->SetPoint( i, p1 );
}

/**

  @author   Mikkel B. Stegmann
  @version  4-25-2000

  @memo     Writes the shape to a ASF file.
  @doc      Writes the shape to a ASF file. Remember asf's are always
            in relative coordinates. Se format description else where.

  @see      ReadASF
  
  @param    filename		Output filename.  
  @param    image_width		The image the coord. is relative to.
  @param    image_height	The image the coord. is relative to.
  
  @return   true on success, false on errors
  
*/
bool CAAMShape::WriteASF( const CString &filename,
						  const int image_width, 
						  const int image_height		) {

    return WriteASF0_90( filename, image_width, image_height );
}



/**

  @author   Mikkel B. Stegmann
  @version  7-2-2001

  @memo     ASF writer version 0.90.

  @doc      Writes the shape to a ver. 0.90 ASF file. Remember asf's are always
            in relative coordinates. Se format description else where.

  @see      ReadASF
  
  @param    filename		Output filename.  
  @param    image_width		The image the coord. is relative to.
  @param    image_height	The image the coord. is relative to.
  
  @return   true on success, false on errors
  
*/
bool CAAMShape::WriteASF0_90(   const CString &filename,
						        const int image_width, 
						        const int image_height		) {    

	FILE *fh;

	fh = fopen( filename, "wb" );

	if (!fh ) return false;

	// convert to relative coordinates
	if ( IsAbs() ) {
		
		Abs2Rel( image_width, image_height );		
	}

	// write header
	CTime t = CTime::GetCurrentTime();
	CString s = t.Format( "%A %B %d - %Y [%H:%M]" );
	fprintf( fh, "######################################################################\n" );
	fprintf( fh, "#\n#    AAM Shape File  -  written: %s\n#\n", s );
	fprintf( fh, "######################################################################\n\n" );

	// write nb points
	fprintf( fh, "#\n" );
	fprintf( fh, "# number of model points\n#\n%i\n\n", m_iNbPoints );	

    // write points
	fprintf( fh, "#\n" );
	fprintf( fh, "# model points\n" );
	fprintf( fh, "#\n" );
	fprintf( fh, "# format: <path#> <type> <x rel.> <y rel.> <point#> <connects from> <connects to> <user1> <user2> <user3>\n", m_iNbPoints );
	fprintf( fh, "#\n" );

    // check for user fields
    bool u1 = m_vUser1.size()==NPoints();
    bool u2 = m_vUser2.size()==NPoints();
    bool u3 = m_vUser3.size()==NPoints();
	
	for(int i=0;i<m_iNbPoints;i++) {
		
		CAAMPoint p = this->GetPoint( i );
		fprintf( fh, "%i \t%i \t%.8f \t%.8f \t%i \t%i \t%i",
					m_vPointAux[i].m_iPathID,
					m_vPointAux[i].m_iTypeFlags,
					p.x,
					p.y,
					i,
					m_vPointAux[i].m_iConnectFrom,
					m_vPointAux[i].m_iConnectTo			);

        // print user fields if present
        if ( u1 ) fprintf( fh, "\t%f", m_vUser1[i] ); else  fprintf( fh, "\t0.00" ); 
        if ( u2 ) fprintf( fh, "\t%f", m_vUser2[i] ); else  fprintf( fh, "\t0.00" ); 
        if ( u3 ) fprintf( fh, "\t%f", m_vUser3[i] ); else  fprintf( fh, "\t0.00" );         
        fprintf( fh, "\n");
	}

    // write host image
    fprintf( fh, "\n#\n" );
	fprintf( fh, "# host image\n" );
	fprintf( fh, "#\n" );
    fprintf( fh, "%s\n", m_szHostImage );

    // close file
	fclose( fh );

	if ( !IsAbs() ) {
				
		// convert back
		Rel2Abs( image_width, image_height );
	}
    
	// succes
	return true;
}



/**

  @author   Mikkel B. Stegmann
  @version  4-25-2000

  @memo     Reads an ver. 0.90 .asf into relative coordinates.
  @doc      Reads an ver. 0.90 .asf into relative coordinates.
            Se format description else where.

  @see      WriteASF
  
  @param    filename	Input filename.  
  
  @return   true on success, false on errors
  
*/
bool CAAMShape::ReadASF( const CString &filename ) {

    return ReadASF0_90( filename );
}


/**

  @author   Mikkel B. Stegmann
  @version  4-25-2000

  @memo     Reads an ver. 0.90 .asf into relative coordinates.
  @doc      Reads an ver. 0.90 .asf into relative coordinates.
            Se format description else where.

  @see      WriteASF
  
  @param    filename	Input filename.  
  
  @return   true on success, false on errors
  
*/
bool CAAMShape::ReadASF0_90( const CString &filename ) {
	
	CAAMPropsReader r( filename );

    if ( !r.IsValid() ) {
        
        return false;
    }

	int npoints, path_id, type, point_nb, from, to;
	float x_rel, y_rel;

	// read nb points
	r.Sync();
	fscanf( r.FH(), "%i", &npoints );

	// resize this shape
	this->Resize( npoints*2 );
	m_vPointAux.resize( npoints );

	// read all point data
	r.Sync();	
    int nelem = 0;
    float user1, user2, user3;
    m_vUser1.clear();
    m_vUser2.clear();
    m_vUser3.clear();
	for(int i=0;i<npoints;i++) {

		// read point data
		// format: <path#> <type> <x rel.> <y rel.> <point#> <connects from> <connects to>
		fscanf( r.FH(), "%i %i %f %f %i %i %i", &path_id, &type, &x_rel, &y_rel, &point_nb, &from, &to );		

        if ( r.MoreNonWhiteSpaceOnLine() ) {

            nelem = fscanf( r.FH(), "%f %f %f", &user1, &user2, &user3 );
        }

        r.Sync();

        if (nelem==3) {
            
            // user defined fields present
            m_vUser1.push_back( user1 );
            m_vUser2.push_back( user2 );
            m_vUser3.push_back( user3 );

        } else {

            // user defined fields not present            
        }
        
		// save point data
		SetPoint( i, x_rel, y_rel );

		// save point aux data
		m_vPointAux[i].m_iPathID		= path_id;		
		m_vPointAux[i].m_iTypeFlags		= type;
		m_vPointAux[i].m_iConnectFrom	= from;
		m_vPointAux[i].m_iConnectTo		= to;
	}

    // read host image	
    char buf[256];	
    r.Sync();    
	fscanf( r.FH(), "%s", buf ); 
    m_szHostImage = buf;

	// we're now hold relative coordinates
	m_bAbsPointCoordinates = false;

	return true;
}



/**

  @author   Mikkel B. Stegmann
  @version  11-15-2000

  @memo     Converts shape coordinates from absolute to relative by
            using the hostimage.

  @doc      Converts shape coordinates from absolute to relative by
            using the hostimage.
*/
void CAAMShape::Abs2Rel( const CString hostImagePath ) {

    if ( IsAbs()==true ) {

        CVisImage<TAAMPixel> img;
        img.ReadFile( CAAMUtil::AddBackSlash( hostImagePath ) + m_szHostImage );
   
        // convert to absolute coordinates
	    Abs2Rel( img.Width(), img.Height() );
    }
}


/**

  @author   Mikkel B. Stegmann
  @version  11-15-2000

  @memo     Converts shape coordinates from relative to absolute by
            using the hostimage.

  @doc      Converts shape coordinates from relative to absolute by
            using the hostimage.

*/
void CAAMShape::Rel2Abs( const CString hostImagePath ) {

    if ( IsAbs()==false ) {

        CVisImage<TAAMPixel> img;
        img.ReadFile( CAAMUtil::AddBackSlash( hostImagePath ) + m_szHostImage );
   
        // convert to absolute coordinates
	    Rel2Abs( img.Width(), img.Height() );
    }
}


/**

  @author   Mikkel B. Stegmann
  @version  4-25-2000

  @memo     Converts shape coordinates from absolute to relative.
  @doc      Converts shape coordinates from absolute to relative.
			Relative coordinates are specified as:

				x_relative = x_abs/image_width

				y_relative = y_abs/image_height

  @see      Rel2Abs
  
  @param    image_width		The image the coord. should be relative to.
  @param    image_height	The image the coord. should be relative to.
  
  @return   Nothing.
  
*/
void CAAMShape::Abs2Rel( const int image_width, const int image_height ) {

	if ( !IsAbs() ) {

		// coordinates are already in relative format
		return;
	}

	// scale x
	for(int i=0;i<m_iNbPoints;i++) {

		(*this)[i] /= image_width;
	}

	// scale y
	for(i=m_iNbPoints;i<2*m_iNbPoints;i++) {

		(*this)[i] /= image_height;
	}

	m_bAbsPointCoordinates = false;
}


/**

  @author   Mikkel B. Stegmann
  @version  4-25-2000

  @memo     Converts shape coordinates from relative to absolute.
  @doc      Converts shape coordinates from relative to absolute.
			Relative coordinates are specified as:

				x_relative = x_abs/image_width

				y_relative = y_abs/image_height

  @see      Abs2Rel
  
  @param    image_width		The image the coord. is relative to.
  @param    image_height	The image the coord. is relative to.
    
  @return   Nothing.
  
*/
void CAAMShape::Rel2Abs( const int image_width, const int image_height ) {

	if ( IsAbs() ) {

		// coordinates are already in absolute format
		return;
	}

	// scale x
	for(int i=0;i<m_iNbPoints;i++) {

		(*this)[i] *= image_width;
	}

	// scale y
	for(i=m_iNbPoints;i<2*m_iNbPoints;i++) {

		(*this)[i] *= image_height;
	}

	m_bAbsPointCoordinates = true;
}	


/**

  @author   Mikkel B. Stegmann
  @version  4-26-2000

  @memo     Adds a path to the shape.
  @doc      Adds a path to the shape. All added point inherits the 
			given pointtype.  
  
  @param    shape		A shape containing one path.
  @param    pointType	The pointtype of the added points.
  
  @return   Nothing.
  
*/
void CAAMShape::AddPath(	const CAAMShape &shape, 
							const CAAMPointInfo &pointType ) {

	int max_path_id = -1;
	int first_point;	// the first point the new path

	// backup current shape
	CAAMShape tmpOrg = *this;

	int newNPoints = tmpOrg.NPoints() + shape.NPoints();

	// resize this
	this->Resize( 2*newNPoints );

	first_point = tmpOrg.NPoints();

	// copy all points
	for(int i=0;i<newNPoints;i++) {

		if ( i<first_point ) {

			// copy old point
			this->SetPoint( i, tmpOrg.GetPoint( i ) );

			// copy old point info
			m_vPointAux[i] = tmpOrg.PointAux()[i]; 

			// find max path_id
			if ( m_vPointAux[i].m_iPathID > max_path_id ) {

				max_path_id = m_vPointAux[i].m_iPathID;
			}

		} else {

			// copy new point
			this->SetPoint( i, shape.GetPoint( i-first_point ) );

			// make new point info		
			
			// copy flags
			m_vPointAux[i] = pointType;

			// set the remaining fields
			m_vPointAux[i].m_iPathID = max_path_id+1;

			if ( pointType.IsClosed() ) {
			
				// make closed path
				m_vPointAux[i].m_iConnectFrom	= i==first_point ? newNPoints-1 : i-1;		
				m_vPointAux[i].m_iConnectTo		= i==newNPoints-1 ? first_point : i+1;

			} else {

				// make open path
				m_vPointAux[i].m_iConnectFrom	= i==first_point ? first_point   : i-1;
				m_vPointAux[i].m_iConnectTo		= i==newNPoints-1 ? newNPoints-1 : i+1;
			}
		}
	}
}


/**

  @author   Mikkel B. Stegmann
  @version  5-2-2000

  @memo     Reverses the point point ordering.
  @doc      Reverses the point point ordering. 
			Used when a clock-wise outer path shall converted to a 
			counter-clock wise hole.
   
  @return   Nothing.
  
*/
void CAAMShape::ReversePointOrder() {

	CDVector xx( m_iNbPoints ), yy( m_iNbPoints );

	// extract x and y part
	for(int i=0;i<m_iNbPoints;i++) {

		xx[i] = (*this)[i];
		yy[i] = (*this)[i+m_iNbPoints];
	}

	// reverse
	xx.Reverse();
	yy.Reverse();

	// write back
	for(i=0;i<m_iNbPoints;i++) {

		(*this)[i] = xx[i];
		(*this)[i+m_iNbPoints] = yy[i];
	}
}


/**

  @author   Mikkel B. Stegmann
  @version  5-3-2000

  @memo     Extracts the starting positions of each path in the shape.

  @doc      Extracts the starting positions of each path in the shape by
			a simple linear search.	The starting position is identified
			by a change in path id (saved in the m_vPointAux member).
    
  @return   A vector of path starting positions.
  
*/
std::vector<int> CAAMShape::GetPaths() const {

	std::vector<int> paths;
	int currentPathID=-1;

	for(int i=0;i<m_iNbPoints;i++) {

		if ( currentPathID!=m_vPointAux[i].m_iPathID ) {

			currentPathID = m_vPointAux[i].m_iPathID;
			paths.push_back( i );
		}
	}

	return paths;
}


/**

  @author   Mikkel B. Stegmann
  @version  5-5-2000

  @memo     Returns the length of a path.

  @doc		Returns the length of a path.
  
  @see		GetPaths 
  
  @param    startPosition	The starting position of the path.
  
  @return   The path length.
  
*/
int CAAMShape::PathLen( const int startPosition ) const {

	assert(startPosition<m_iNbPoints && startPosition>=0);

	int path_id = m_vPointAux[startPosition].m_iPathID;

	for(int i=startPosition;i<m_iNbPoints;i++) {

		if (path_id!=m_vPointAux[i].m_iPathID) {

			break;
		}

	}

	return i-startPosition;
}



/**

  @author   Mikkel B. Stegmann
  @version  5-5-2000

  @memo     Extracts one path from a shape.

  @doc		Extracts one path from a shape into a new shape.
  
  @see      PathLen, GetPaths
  
  @param    startPosition	The starting position of the path.
  
  @return   The path as a new shape.
  
*/
CAAMShape CAAMShape::ExtractPath( const int startPosition ) const {

	int len= PathLen( startPosition );

	CAAMShape outShape(len);

    outShape.SetHostImage( this->HostImage() );

	for(int i=0;i<len;i++) {

		CAAMPoint p;
		p = this->GetPoint( i+startPosition );
		outShape.SetPoint( i, p );
	}

	return outShape;	
}



/**

  @author   Mikkel B. Stegmann
  @version  5-5-2000

  @memo     Adds an extra border on each outer path.

  @doc		Adds an extra outer path on each outer path in the
			distance of 'nPixels' along the point normal.

			This method is primary used in conjunction with 
			the "do not use the convex hull" feature. In such
			a case one often still wants a certain neighborhood
			of the shape to be included in the model.
  
  @param	nPixels		The size of the extents.
  
  @return   Nothing.
  
*/
void CAAMShape::AddShapeExtends( int nPixels ) {

	std::vector<int> paths;

	CAAMShape tmpShape( this->NPoints() );

	// make a copy of this shape
	tmpShape = *this;

	paths = tmpShape.GetPaths();

	// for each path in the tmpShape
	for(unsigned int i=0;i<paths.size();i++) {

		// check type
		const CAAMPointInfo &pi = tmpShape.PointAux()[ paths[i] ];			

		if ( pi.IsOuterEdge() && pi.IsClosed() ) {		

			// allright the path is an outer egde and it's closed
			// we can proceed

			// make a copy of the path
			CAAMShape newOuterPath = tmpShape.ExtractPath( paths[i] );

			// expand the new outer path
			newOuterPath.Expand( nPixels );

			// setup new path properties
			CAAMPointInfo newOuterPathPI;

			newOuterPathPI.SetClosed( true );
			newOuterPathPI.SetOriginal( false );
			newOuterPathPI.SetOuterEdge( true );
			newOuterPathPI.SetHole( false );

			// add to this shape
			this->AddPath( newOuterPath, newOuterPathPI );

			// remove the outer path property from the original path
			CAAMPointInfo oldPI;
			oldPI = m_vPointAux[ paths[i] ];
			oldPI.SetOuterEdge( false );
			SetPointInfoFlagsInPath( paths[i], oldPI.m_iTypeFlags );			
		}
	}
}



/**

  @author   Mikkel B. Stegmann
  @version  5-9-2000

  @memo     Sets all flags in one path to the same value.

  @doc		Sets all flags in one path to the same value.
  
  @param    startPosition	Starting position of the path.

  @param    flags			The flags.
  
  @return   Nothing.
  
*/
void CAAMShape::SetPointInfoFlagsInPath( const int startPosition, const int flags ) {

	int len= PathLen( startPosition );
	
	for(int i=0;i<len;i++) m_vPointAux[i+startPosition].m_iTypeFlags = flags;
}


/**

  @author   Mikkel B. Stegmann
  @version  12-12-2000

  @memo     Adds articifial interior points to the shape.

  @doc      Add articifial interior points to the shape by making a 
			Delaunay triangulation and adding the centroid of each
			triangle. This is done iteratively. Default is one iteration
  
  @param    interations	Controls the number of artificial points. 
						One iteration equals one triangulation.
 
  @return   Nothing.
  
*/
void CAAMShape::AddInterior(const int interations) {


	for(int iter=0;iter<interations;iter++) {

		CAAMMesh mesh;
		
		// do the Delaunay triangulation of this shape
		CAAMDelaunay::MakeMesh( *this, mesh, true );

		int n = mesh.NTriangles();

		// make new temporary shape
		CAAMShape tmpShape( this->NPoints() + n );
		
		// copy points and point aux
		for(int i=0;i<NPoints();i++) {

			tmpShape.SetPoint( i, this->GetPoint(i) );			
		}
        tmpShape.PointAux() = this->PointAux();

		// setup the point aux of the new artificial points
		CAAMPointInfo pi;
		pi.SetClosed( false );
		pi.SetHole( false );
		pi.SetOriginal( false );
		pi.SetOuterEdge( false );
		

		std::vector<int> paths = this->GetPaths();
		int npaths = paths.size();

		// add the centroids to the new shape
		for(i=0;i<n;i++) {

			CAAMPoint centriod;

			centriod = mesh.Triangles()[i].CenterPoint();

			int nbp = NPoints()+i;

			// set the point
			tmpShape.SetPoint( nbp, centriod );

			// set the point aux
			pi.m_iConnectFrom = nbp;
			pi.m_iConnectTo = nbp;
			pi.m_iPathID = npaths;
			tmpShape.PointAux()[nbp] = pi;
		}

		// copy the temporary shape into this
		*this = tmpShape;
	}
}



/**

  @author   Mikkel B. Stegmann
  @version  12-12-2000

  @memo     Tests if any interior points has gone outside 
			outer path of the shape.
  @doc      Tests if any interior points has gone outside 
			outer path of the shape.

  @return	True if the shape looks ok, false if not.
  
*/
bool CAAMShape::ConsistencyCheck() {

	int n = NPoints();

	for(int i=0;i<n;i++) {

		// test if any interior points has gone outside 
		// the outerpath of the shape
		if ( PointAux()[i].IsOuterEdge()==false ) {
			
			// the point should belong to the interior of the shape
			CAAMPoint p = this->GetPoint(i);
			if ( this->IsInside( p )==false ) {

				// wooops, it didn't
				return false;				
			}
		}
	}

	// no point failed the test -> we're happy :-)
	return true;
}



/**

  @author   Mikkel B. Stegmann
  @version  7-27-2000

  @memo     Converts a one path shape into a border shape.

  @doc      Converts a one path shape into a border shape by adding
			two symmetric borders: an inside (a hole) and an outside 
			border.

			No checks for folding paths are done as of now.
			
  @param    size	The size of the border in pixels.
    
  @return   Nothing.
  
*/
void CAAMShape::MakeBorderShape( int size ) {
	
	CAAMShape hole;
	hole = *this;
	hole.Expand( -size );
	//hole.ReversePointOrder();
			
	// setup new path properties
	CAAMPointInfo newHolePathPI;

	newHolePathPI.SetClosed( true );
	newHolePathPI.SetOriginal( false );
	newHolePathPI.SetOuterEdge( false );
	newHolePathPI.SetHole( true );

	// update point aux of the hole
	hole.SetPointInfoFlagsInPath( 0, newHolePathPI.m_iTypeFlags );

	// add the hole
	this->AddPath( hole, newHolePathPI );
			
	// add the outside path
	this->AddShapeExtends( size );			
}



/**

  @author   Mikkel B. Stegmann
  @version  10-20-2000

  @memo     Plus operator.

  @doc      Plus operator.
  
  @param    v	Vector to add.
  
  @return   The addition of this and 'v'.
  
*/
CAAMShape CAAMShape::operator+(const CVisDVector &v) const
{
    CAAMShape ret( *this );

    ret += v;
       
    return ret;
}



/**

  @author   Mikkel B. Stegmann
  @version  10-20-2000

  @memo     Minus operator.

  @doc      Minus operator.
  
  @param    v	Vector to add.
  
  @return   The subtraction of this and 'v'.
  
*/
CAAMShape CAAMShape::operator-(const CVisDVector &v) const
{
    CAAMShape ret( *this );

    ret -= v;
       
    return ret;
}


/**

  @author   Mikkel B. Stegmann
  @version  10-24-2000

  @memo     Retrives the image connected to the shape.

  @doc      Retrives the image connected to the shape.
            As of now it's loaded from disk using the HostImage() member
            in the shape.
  
  @param    dest    The destination image.

  @param    path    The path to the .asf file.

  @param	rfactor	Optional reduction factor. Performs a scaling of the
					the shape by 1/rfactor. Default 1 i.e. no scaling.
  
  
  @return   Nothing.

  @throws   CVisFileIOError
  
*/
void CAAMShape::GetHostImage( CDMultiBand<TAAMPixel> &dest, 
                              const CString &path,
                              const int rfactor  ) const {

    assert( m_szHostImage!="" );
    
    dest.ReadBandedFile( CAAMUtil::AddBackSlash( path ) + m_szHostImage );
    dest.SetName( m_szHostImage );

    if (rfactor>1) {

        dest.ReducePyr( rfactor );
    }
}


/**

  @author   Mikkel B. Stegmann
  @version  11-15-2000

  @memo     Returns the total area of the shape (with holes excluded).
  @doc      Returns the total area of the shape (with holes excluded).

  @param    use_covex_hull  Use the convex hull of the shape for
                            area calculation (default false).
            
  @return   The area.
  
*/
double CAAMShape::Area( bool use_covex_hull ) const {

    CAAMMesh mesh;

    // lazy and a bit costly way to calculate the area
    CAAMDelaunay::MakeMesh( *this, mesh, !use_covex_hull );

    return mesh.Area();
}


/**

  @author   Mikkel B. Stegmann
  @version  5-2-2001

  @memo     Calculates the convex hull of each path in the shape.

  @doc      Calculates the convex hull of each path in the shape.
            The calulation is built upon the geometrical fact that the
            homogenous point matrix: [ p1x p2x p3x ; p1y p2y p3y ; 1 1 1 ]
            is positive if p1, p2, p3 is a convex segment and negative if
            concave. Note: this holds for a clock-wise ordering of p1,p2,p3.

            Any open paths are considered cyclic in the concavity calulation.

            Remember that paths should be defined clock-wise in the asf format.

            BUG COMMENT: This does not seem to work with multiple paths.

  @see      IsConvex
    
  @return   A convex version of this shape w.r.t. each path.
  
*/
CAAMShape CAAMShape::CalcConvexHull() const {
    
    CAAMPoint point;
    CDMatrix m(3,3);      
    std::vector<int> paths;

    paths = GetPaths();

    for(unsigned int p=0;p<paths.size();p++) {

        CAAMShape path_shape = this->ExtractPath( paths[p] );
        
        int path_len = PathLen( paths[p] );
        for(int i=0;i<path_len && path_len>2;i++) {

            point = this->GetPoint( i-1>0 ? paths[p]+i-1 : paths[p]+path_len-1 );
            m[0][0] = point.x;
            m[1][0] = point.y;
            m[2][0] = 1.0;

            point = this->GetPoint( paths[p]+i );
            m[0][1] = point.x;
            m[1][1] = point.y;
            m[2][1] = 1.0;

            point = this->GetPoint( i+1<path_len ? paths[p]+i+1 : paths[p] );
            m[0][2] = point.x;
            m[1][2] = point.y;
            m[2][2] = 1.0;
            
            if ( m.Det()<0) {

                // concave segment detected

                // Remove the i-th point and continue on the new shape
                CAAMShape out(*this);

                out.RemovePoint( i );

                return out.CalcConvexHull();
            }            
        }
    }

    //
    // shape was convex from the start
    //                  
    return *this;
}


/**

  @author   Mikkel B. Stegmann
  @version  5-2-2001

  @memo     Tests if the shap is convex w.r.t. each path.

  @doc      Tests if the shap is convex w.r.t. each path.
            This call is rather expensive, since it spaws a call to CalcConvexHull().
  
  @see      CalcConvexHull
    
  @return   True on convex, false on concave.
  
*/
bool CAAMShape::IsConvex() const {

    CAAMShape convex  = CalcConvexHull();

    return convex.NPoints()==this->NPoints();
}


/**

  @author   Mikkel B. Stegmann
  @version  5-2-2001

  @memo     Removes the i-th point from the shape.

  @doc      Removes the i-th point from the shape. Since this require 
            massive reordering of the preceeding point connectivity this is
            actually a very expensive call [a fairly ugly in it's implementation] :-(  
  
  @param    i   The index of the point to remove.
    
  @return   Nothing.
  
*/
void CAAMShape::RemovePoint( const int i ) {

    // get info about the path the i'th point is
    // belonging to
    std::vector<int> paths = this->GetPaths();     
    int path_id      = this->m_vPointAux[i].m_iPathID;
    int path_start   = paths[path_id];
    int path_end     = (path_id==paths.size()-1 ? this->NPoints() : paths[path_id+1])-1;
    int path_len     = path_end-path_start+1;
    bool closed_path = this->m_vPointAux[i].IsClosed();

    // create new shape
    CAAMShape new_shape( this->NPoints()-1 );

    // copy all point + aux data 
    // except the i-th point
    int np = new_shape.NPoints();
    for(int j=0;j<np;j++) {
        
        new_shape.SetPoint( j, this->GetPoint(j>=i ? j+1 : j) );        
        new_shape.m_vPointAux[j] = this->m_vPointAux[j>=i ? j+1 : j];  
    }

    /////////////////////////////////////////////////////////
    // update to, from and path id in the aux data part
    /////////////////////////////////////////////////////////

    // if path_len==1 then we're removing a path totally
    // and hence we need to decrement the preceeding
    // path id's
    for(j=i;j<np && path_len==1;j++) {
         
        --new_shape.m_vPointAux[j].m_iPathID;        
    }

    // decrement all to/from
    int to, from;    
    for(j=i;j<np;j++) {
                 
        from = new_shape.m_vPointAux[j].m_iConnectFrom;
        to   = new_shape.m_vPointAux[j].m_iConnectTo;
        new_shape.m_vPointAux[j].m_iConnectFrom = DEC_TO_ZERO(from);                        
        new_shape.m_vPointAux[j].m_iConnectTo = DEC_TO_ZERO(to);
        
    }

    // In the to/from updates we have the following cases:
    //
    //  1) First point on a closed path
    //  2) Last point on a closed path
    //  3) First point on an open path
    //  4) Last point on an open path
    //  5) Intermediate point on a open/closed path
    //
    // In case 1-4 we need to fix the open/close path property
    //  

    // case 1
    if (i==path_start && closed_path==true) {

        // 'close' the new first point on this path
        new_shape.m_vPointAux[i].m_iConnectFrom = DEC_TO_ZERO(path_end);
    }

    // case 2
    if (i==path_end && closed_path==true) {

        // 'open' the new last point on this path
        new_shape.m_vPointAux[DEC_TO_ZERO(i)].m_iConnectTo = path_start;     
    }

    // case 3
    if (i==path_start && closed_path==false) {

        // 'open' the new first point on this path
        new_shape.m_vPointAux[i].m_iConnectFrom = i;    
    }

    // case 4
    if (i==path_end && closed_path==false) {

        // 'open' the new last point on this path
        new_shape.m_vPointAux[DEC_TO_ZERO(i)].m_iConnectTo = DEC_TO_ZERO(i); 
    }  
    
    *this = new_shape;
}


/**

  @author   Mikkel B. Stegmann
  @version  7-2-2001

  @memo     Returns a reference to a user defined field vector.

  @doc      Returns the reference of the 
            user defined field vector number  'field_nb'.
  @see      
  
  @param    field_nb    Field number [1-3].   
  
  @return   A reference to the specified field vector.
  
*/
std::vector<float> &CAAMShape::UserField( const int field_nb ) {

    assert( field_nb>=0 && field_nb<=3 );

    switch (field_nb) {

        case 1:     return m_vUser1; break;
        case 2:     return m_vUser2; break;
        case 3:     return m_vUser3; break;        
        default:    printf("Error: Wrong field number given. Junk data returned.\n");                   
    }

    // return junk data
    return m_vUser1;    
}


/**

  @author   Mikkel B. Stegmann
  @version  7-2-2001

  @memo     Returns the value of a user defined field vector.

  @doc      Returns the value of the  user defined field 
            vector number 'field_nb'.
  @see      
  
  @param    field_nb    Field number [1-3].    
  
  @return   A float holding the value of the field.
  
*/
const std::vector<float> &CAAMShape::UserField( const int field_nb ) const {

    return ((CAAMShape*)this)->UserField( field_nb );
}
    



/**

  @author   Mikkel B. Stegmann
  @version  7-2-2001

  @memo     Allocates room for the three user defined fields.

  @doc      Allocates room for the three user defined fields.

            Notice that the user defined fields is *not* allocated by default.            

            In order to preserved memory the user must to this explicitly by
            using AllocateUserFields();
  
  @return   Nothing.
  
*/
void CAAMShape::AllocateUserFields() {
    
    m_vUser1.resize( NPoints() );
    m_vUser2.resize( NPoints() );
    m_vUser3.resize( NPoints() );
}



