/* $Id: AAMModel.cpp,v 1.6 2003/04/24 12:01:01 aam Exp $ */
#include "AAMModel.h"
#include "AAMMovie.h"
#include "AAMOptimize.h"
#include "AAMInitialize.h"
#include "AAMTransferFunctions.h"
#include "AAMMathUtil.h"
#include "AAMVisualizer.h"
#include "AAMAnalyzeSynthesize.h"
#include "AAMReferenceFrame.h"
#include "htimer.h"
#include <direct.h>


/**

  @author   Mikkel B. Stegmann
  @version  5-16-2000

  @memo     Constructor. Set up the default settings.

  @doc      Constructor. Set up the default settings.
  
  @return   Nothing.
  
*/
CAAMModel::CAAMModel() {
	
	// setup defaults
	m_iModelReduction	= 1;
	m_dAddExtents		= 0.0;
	m_bUseConvexHull	= false;
	m_bUseTangentSpace	= true;
	m_iLearningMethod	= 1;
	m_iShapeTrunc    	= 95;
    m_iTextureTrunc     = 95;
    m_iCombinedTrunc    = 95;
		
    m_iTextureSamples   = 0;
    m_iNShapes          = 0;
    m_dMeanShapeSize    = 0;
    m_dBuildTime        = .0;

	// current amf format version
	m_dAMFVersion = .99;    
    
    // setup texture transfer function
    // use the identify mapping as default for textures
    m_pTextureTF = new CAAMTFIdentity;

    m_pAnalyzeSynthesize = NULL;
    m_pReferenceFrame    = NULL;
    m_sCurrentAnalyzeId  = "__none__";  
}



/**

  @author   Mikkel B. Stegmann
  @version  5-16-2000

  @memo     Destructor.
  
  @doc      Destructor.
  
  @return   Nothing.
  
*/
CAAMModel::~CAAMModel() {   	

    // deallocate
	if (m_pTextureTF) {

		delete m_pTextureTF;
        m_pTextureTF = NULL;
	}

    if (m_pReferenceFrame) {

        delete m_pReferenceFrame;
        m_pReferenceFrame = NULL;
    }

    if (m_pAnalyzeSynthesize) {

        delete m_pAnalyzeSynthesize;
        m_pAnalyzeSynthesize = NULL;
    }
}


/**

  @author   Mikkel B. Stegmann
  @version  3-6-2000

  @memo     Doing model approximation of an (unseen) example.
  
  @doc      Synthesizes an unseen example by projecting the shape and
			(normalized) texture into c-parameter space, generating a
			model instance and assigning it the appropriate pose 
			(incl. denormalization).
			    
  @param    filename	The base filename of an annotation. Ex. "scan.asf"						
		
  @param    outImg		The output image where the model approximation has
						been overlaid.
  
  @return   true on success, false if the image and/or annotation could not be read.
  
*/
bool CAAMModel::ApproxExample( const CString &filename, CDMultiBand<TAAMPixel> &outImg ) const {

	CAAMShape shape;

	// load image and shape
	CAAMUtil::ReadExample( filename, outImg, shape, m_iModelReduction );
    
    // obtain the combined model parameters, c, and the normalisation    
    CDVector c;
    Shape2Combined( shape, outImg, c );

	//
	// generate model instance (match pose to 'shape')
	// and render the instance into the image 'img'
	//
	ModelImage( c, outImg, &shape );

	// everything went fine
	return true;
}



/**

  @author   Mikkel B. Stegmann
  @version  10-30-2002

  @memo     Sets constraints on the pose parameter updates.

  @doc      Sets constraints on the pose parameter updates.
            
            NOTICE: Currently, these are *not* saved along
                    with the model and thus must be set each
                    time a model is loaded.

  @see      ConstrainSearchParameters
  
  @param    pc  Parameter constraints (in absolute numbers). 
  
  @return   Nothing
  
*/
void CAAMModel::SetPoseParameterUpdateConstraints( const CDVector &pc ) {

    m_vPoseParameterUpdateConstraints = pc;
}



/**

  @author   Mikkel B. Stegmann
  @version  11-4-2002

  @memo     Sets user-specified shape parameter update constraints.

  @doc      Sets user-specified shape parameter update constraints.
			NOTICE: These are currently not saved with the model.
  
  @param    mean	Some shape parameter configuration.
  @param    sd		The standard diviations of the 'mean'.
  
  @return   Nothing.
  
*/
void CAAMModel::SetShapeParameterUpdateConstraints( const CDVector &mean,
                                                    const CDVector &sd ) {

    m_vShapeParameterConstraintsMean = mean;
    m_vShapeParameterConstraintsSD = sd;
}


/**

  @author   Mikkel B. Stegmann
  @version  7-20-2000

  @memo     Constrain the pose and model parameters.

  @doc      Constrain the pose and model parameters to be within some
			reasonable limits.
  
  @param    c		The model parameters.
  @param    pose	The pose parameters.
  
  @return   Nothing.
  
*/
void CAAMModel::ConstrainSearchParameters( CDVector &c, CDVector &pose ) const {

    if (m_vPoseParameterUpdateConstraints.Length()!=4) {

        // use old hard-wired constraints

        // scale
        double scale_limit = .1;
	    if ( fabs( pose[0] ) > scale_limit )  {

		    int sgn = (int)(pose[0]/fabs( pose[0] ));
		    pose[0] = sgn*scale_limit;
	    }

        // theta
        double theta = 5.*M_PI/180.;
        if ( fabs( pose[1] ) > theta )  {

		    int sgn = (int)(pose[1]/fabs( pose[1] ));
		    pose[1] = sgn*theta;
	    }
   
	    // x
        double xlimit = this->ReferenceFrame().RefImageWidth() / 10.;
	    if ( fabs( pose[2] ) > xlimit )  {

		    int sgn = (int)(pose[2]/fabs( pose[2] ));
		    pose[2] = sgn*xlimit;
	    }

	    // y 
        double ylimit = this->ReferenceFrame().RefImageHeight() / 10.;
	    if ( fabs( pose[3] ) > ylimit )  {

		    int sgn = (int)(pose[3]/fabs( pose[3] ));
		    pose[3] = sgn*ylimit;
	    }
    } else {

        // use user-selectable constraints
        for(int i=0;i<4;i++) {

            if ( fabs(pose[i])>m_vPoseParameterUpdateConstraints[i] ) {

                // clamp
                int sgn = (int)(pose[i]/fabs( pose[i] ));
                pose[i] = sgn*m_vPoseParameterUpdateConstraints[i];
            }
        }
    }
	
  
    if ( m_vShapeParameterConstraintsMean.Length()==0 ) {

	    // model parameters
	    int np = c.Length();
	    double val;
	    for(int i=0;i<np;i++) {

		    val = c[i];
		    double limit = 3.*sqrt( m_CombinedPCA.EigenValues()[i] );

		    if ( fabs(val)>limit ) {

			    int sgn = (int)(val/fabs( val ));
			    c[i] = sgn*limit;			
		    }
	    }
    } else {        
        
        //printf("  constraining shape");
        CDVector b_s, b_g;
        this->Combined2ShapeParam( c, b_s );
        this->Combined2TexParam( c, b_g );

        bool constrained = false;
        for(int i=0;i<b_s.Length();i++) {

            double limit = 0.*m_vShapeParameterConstraintsSD[i];
            double val = b_s[i]-m_vShapeParameterConstraintsMean[i];

            if ( fabs(val)>limit) {
                
                //printf(".");
                int sgn = (int)(val/fabs( val ));                
                b_s[i] = m_vShapeParameterConstraintsMean[i] + sgn*limit;
                constrained = true;
            }
        }                
        if (constrained) {

            //printf("c = %s\n", c.ToString() );
            this->ShapeTexParam2Combined( b_s, b_g, c );
            //printf("c = %s\n", c.ToString() );
        }
        
        //printf("\n");
    }
}


/**

  @author   Mikkel B. Stegmann
  @version  3-27-2000

  @memo     Estimate the pose of a shape using the pose regression
			matrix.
	
  @doc      Estimate the pose of a shape using the pose regression
			matrix.
  @see      
  
  @param    image	The image to search in.

  @param    shape	The shape to determine the pose from.
					
  @param	pose	The output pose vector.
  
  @return   True is ok, false if the shape is outside the image.
  
*/
bool CAAMModel::EstimatePose(	const CDMultiBand<TAAMPixel> image, 
								const CAAMShape &shape,
								CDVector &pose	) const {	

	CDVector c, g_s, g_m;    

	// make image instance
	int nSamples = SampleShape( image, shape, g_s );	    
    if (nSamples==0) return false;
	
	// align shape
	CAAMShape alignedShape( shape );			
	alignedShape.AlignTo( m_sMeanAShape );			

	// make model param
	ShapeTex2Combined( alignedShape, g_s, c );

	// make model instance
	TextureInstance( c, g_m );
	
	// form difference
	g_s -= g_m;
	
	// estimate pose
	pose = -m_R_t * g_s;

    return true;
}


/**

  @author   Mikkel B. Stegmann
  @version  2-29-2000

  @memo     Generates a model image based on the parameters in 'c'.

  @doc      Generates a model image based on the parameters in 'c'
			with various options.  
  
  @param    c			A set of model parameters.

  @param    outImg		The output imge.  

  @param    matchPose	A pointer to a shape. If not NULL the model
						generated by the set of c-parameters will be
						aligned wrt. pose to this shape.

  @param    fitTexture  If matchPose is not NULL, the de-mapped and 
                        de-normalized texture will be fitted in a 
                        least squares sense to the texture in outImg 
                        given by the shape. Default true.
                        
  @return   Nothing.
  
*/
void CAAMModel::ModelImage( const CDVector &c,
				            CDMultiBand<TAAMPixel> &outImg,
							const CAAMShape *matchPose,
                            const bool fitTexture ) const {

	// generate the texture instance from the model
	CDVector texture;
	TextureInstance( c, texture );	

	// generate the shape instance from the model
	CAAMShape shape;

	ShapeInstance( c, shape );    

	if (matchPose) {

		shape.AlignTo( *matchPose );
	} else {
	
		shape.Scale( m_dMeanShapeSize );
	}

	// generate the model image
	ModelImageEx( shape, texture, outImg, matchPose!=NULL, fitTexture );
}


/**

  @author   Mikkel B. Stegmann
  @version  2-24-2000

  @memo     Generates a synthetic image from the AAM.			

  @doc      Generates a synthetic image from the AAM using the model
			parameters in 'c'.
  @see      
  
  @param    shape           The shape the texture vector should be mapped to.							

  @param    textureSamples	The texture vector to be warped into an image.
							Will be de-mapped and de-normalized.

  @param    outImg			The output image (sized correctly inside this
							method, if renderInImage is false).

  @param	renderImage		If true:
							
							  1) outImg is expected to be allocated.
							  
							  2) the model is rendered with it's
								 pose unchanged, thus the shape is
                                 expected to lie within the outImg	                                                               
                                 
							This option is used for drawing the final 
							optimization into the image or to draw a
							model approximation into the image 'outImg'.

                            Default false.

  @param    fitTexture      If renderImage is true, the de-mapped and 
                            de-normalized texture will be fitted in a 
                            least squares sense to the texture in outImg 
                            given by the shape. Default true.
			
  @return   Nothing.
  
*/
void CAAMModel::ModelImageEx( const CAAMShape &shape,
							  const CDVector &texture,
							  CDMultiBand<TAAMPixel> &outImg,								  							  
							  const bool renderImage,
                              const bool fitTexture ) const {
    
    CDVector tex(texture);
    TextureTF().DeMap( tex );    

    // fit texture to image (if requested)    
    if ( renderImage && 
         fitTexture  &&
         CAAMUtil::ShapeInsideImage( shape, outImg ) ) {

        // sample image texture
        CDVector imgTexture;        
        this->SampleShape( outImg, shape, imgTexture, false, true, false );
                  
        // least squares fit to image texture
        tex.AlignTo(imgTexture);          

        // clamp
        tex.Clamp( 0., 255. );
                      
    } else {

        // we don't know anything -> use full byte range
        CAAMMathUtil::LinearStretchMinMax( tex, 0, 255 );
    }    
    
    // render the final texture into outImg
    m_pAnalyzeSynthesize->Synthesize( shape, tex, outImg, renderImage );
}


/**

  @author   Mikkel B. Stegmann
  @version  2-21-2000

  @memo     Generates a shape free image using a vector of texture samples.

  @doc      Generates a shape free image (that is; a mean shape image) 
			using a vector of texture samples.			
			
  @see      ModelImage
  
  @param    textureSamples	The texture vector to be warped into an image.
							De-normalizes (and demaps) inside this method.

  @param    outImg			A reference to an output image. Resize of the
							image is done inside this method.                               
							
  @return   Nothing.
  
*/
void CAAMModel::ShapeFreeImage( const CDVector &textureSamples,
							    CDMultiBand<TAAMPixel> &outImg,
                                const bool deMap ) const {

    // demap
    CDVector texture(textureSamples);

    if (deMap) {

        TextureTF().DeMap( texture );
    }

    CAAMMathUtil::LinearStretchMinMax( texture, 0, 255 );   

    // convert to shape-free image
    m_pReferenceFrame->Vector2Image( texture, outImg );	
}



/**

  @author   Mikkel B. Stegmann
  @version  2-21-2000

  @memo     Generates a shape free image using a vector of texture samples.

  @doc      Generates a shape free image (that is; a mean shape image) 
			using a vector of texture samples.			
			
  @see      ModelImage
  
  @param    textureSamples	The texture vector to be warped into an image.
							De-normalizes (and demaps) inside this method.

  @param    m    			A reference to an output image on matrix form. 
                            Resize of the matrix is done inside this method.                               
							
  @return   Nothing.
  
*/
void CAAMModel::ShapeFreeImage( const CDVector &textureSamples,
							    CDMatrix &m,
                                const bool deMap,
                                const bool normalize ) const {

    // demap
    CDVector texture(textureSamples);

    if (deMap) {

        TextureTF().DeMap( texture );
    }

    if (normalize) {

        CAAMMathUtil::LinearStretchMinMax( texture, 0, 255 );   
    }

    // convert to shape-free image
    m_pReferenceFrame->Vector2Matrix( texture, m );	
}



/**

  @author   Mikkel B. Stegmann
  @version  3-27-2000

  @memo     Calculates the pixel difference from a model instance and an image.

  @doc      Calculates the pixel difference from a model instance and an image.
    
  @param	image		The image (input)
  @param    c			Model parameters (in/output).
  @param    estimate	The shape estimate (in/output).
  @param    diff		The pixel difference vector (output) Resized inside.  
  @param	similaritym	Set the used similarity measure:
						
							0	Non-normalised L_2 norm (default)

							1	The "Mahalanobis" distance (texture samples are
								regarded independent to increase performance).

							2	The Lorentzian error norm.

                            3   Absolute auto correlation of the residuals.

  @return   The similarity measure.
  
*/
double CAAMModel::ModelEstimateTexDiff(	const CDMultiBand<TAAMPixel> &image,
									    const CDVector &c, 
										CAAMShape &estimate,
										CDVector &diff, 
										const int similaritym,
                                        const bool useInterpolation ) const {   

	CDVector g_m;
    CDVector &g_s = diff;   // a bit confusing, but this saves 
                            // an extra texture vector
	CAAMShape model_shape;	
	
	// generate model texture
	TextureInstance( c, g_m );    

	// generate model shape
	ShapeInstance( c, model_shape );    

	// Align X_shape to the annotated shape
	model_shape.AlignTo( estimate );
    
	int nSamples = SampleShape( image, model_shape, g_s, true, useInterpolation );
    // remember g_s = diff


	if (nSamples==0) {

		// we're outside        
		return -1.;
	}
    
    // update estimate
	estimate = model_shape;  
	
	// calc pixel difference: g_s - g_m (remember g_s = diff)	
	diff -= g_m;    
    
    // obtain the number of texture samples
	int n = g_m.Length();

	// calc the similarity between the two texture vectors
	int i;
	double sm, tmpval;
	CDVector tmp;
	switch(similaritym) {

		case 0:		// unnormalized L_2 norm
					tmpval = diff.Norm2();		
					sm = tmpval*tmpval;				
					break;

		case 1:		// independent "Mahalanobis" distance								
					tmp.Resize(n);       
					for(i=0;i<n;i++) {

						tmp[i] = diff[i]*diff[i]/m_vTextureVar[i];					
					}
					sm = tmp.Sum()/(double)n;
					break;

		case 2:		// lorentzian norm	
					tmp.Resize(n);       
					for(i=0;i<n;i++) {

						tmp[i] = log( 1.0 + diff[i]*diff[i]/(2.0*sqrt(m_vTextureVar[i]) ) );
					}
					sm = tmp.Sum()/(double)n;
					break;

        case 3:     // absolute auto correlation of the residuals
                    {
                        double mean= diff.Mean();
                        double cov = .0;
                        for(i=0;i<n-1;i++) {
                           
                            cov += (diff[i]-mean)*(diff[i+1]-mean);
                        }
                        cov /= n-1;
                        sm = abs(cov/diff.Var());
                    }
                    break;

        case 4:     // mutual information
                    sm = CAAMMathUtil::MutualInformation( g_m, g_m, 64 ) -
                         CAAMMathUtil::MutualInformation( g_s, g_m, 64 );
                    break;

		default:	printf("ModelEstimateTexDiff: Unknown similarity measure, %i\n", similaritym );
					sm = .0;
	}
   
	// return the used similarity measure
	return sm;
}



/**

  @author   Mikkel B. Stegmann
  @version  3-6-2000

  @memo     Normalizes a texture vector.

  @doc		Normalizes a texture vector.
  
  @param    texture		Texture to be normalized.
  
  @return   Nothing.
  
*/
void CAAMModel::NormalizeTexture(CDVector &texture) const {	

    //    
    //
    // usage:
    //
    // _putenv( "REMOVE_TEX_MEAN_ONLY=1" );
    //
    // default 0
    //
#ifdef USEAAMENV
    CString removeMeanOnly = getenv( "REMOVE_TEX_MEAN_ONLY" );
    bool remove_mean_only = strcmp(removeMeanOnly,"1")==0;
#else
    bool remove_mean_only = false;
#endif
    if (remove_mean_only) { 
        
        double mean = texture.Mean();
        for(int i=0;i<texture.Length();i++) {

            texture[i] -= mean;        
        }
        return;
    } else {

        CAAMMathUtil::ZeroMeanUnitLength( texture );        
        double dp = texture*m_vMeanTextureOrg;	
    
        if (dp!=.0) { 
                
            texture *= 1./dp; 
        } 
#ifdef _DEBUG
        if (dp==.0) {   
            printf("CAAMModel::NormalizeTexture() tried to normalize a zero texture vector.\n");
        }
#endif
    }
}


/**

  @author   Mikkel B. Stegmann
  @version  5-7-2002

  @memo     Performs AAM optimization of a shape.
  
  @doc      Performs AAM optimization of a shape containing initial pose and
			a set of model parameters (c).  
  
  @param    image		The image to search in.

  @param	s			The initial shape (also containing the inital pose, thus;
						not a normalized shape). Actually only the pose of 's' is 
						used (to align the reference shape as the initial shape).

						NOTE: The optimal shape is returned in 's' after execution.

  @param    c			The initial model parameters. If this vector is empty, it is
						resized correctly and set equal to zero, thus the mean model.

                        NOTE: The optimal model parameters are returned in 'c' after 
						execution. 

  @param	maxIterations The maximum iterations allowed.  

  @param    pOptStates  Optional parameter all convergence info can be returned in.
                        See CAAMOptState.
  
  @param    disableDamping Disables the damping steps (default false).

  @return   The results of the optimization in the form of a 'CAAMOptRes' 
			instance.
  
*/
CAAMModel::CAAMOptRes CAAMModel::OptimizeModel( const CDMultiBand<TAAMPixel> &image,
                                                CAAMShape &s, CDVector &c,	
									            const int maxIterations, 
                                                std::vector<CAAMOptState> *pOptStates,
                                                bool disableDamping ) const {

    int iter = 0, np=m_CombinedPCA.NParameters();	
	CDVector delta_g(m_iTextureSamples), delta_c(np), delta_t(4), delta_t_corr(4), c_est(np);	
	double E_previous, E;	       
    std::vector<CAAMOptState> optStates;
    CAAMShape s_est;
    double refSize = ReferenceShape().ShapeSize();
           
	// constants
	const double covergence_level = 0.01; // point at which we declare convergence    
    const int ndamps = 3;
    const double k_values[ndamps] = { 1., .5, .25 };     

    // config
    bool useInterpolation = true;
    bool opt_timings = false;

    // setup timers
    CHTimer timerModelEstimateTexDiff;
    CHTimer timerRt;
    CHTimer timerRc;    
    CHTimer timerTotal;     
    if (opt_timings) timerTotal.start();
      
	// setup 
	if ( c.Length()!=np ) {
 		
		c.Resize( np );             // resize appropriately		
		c = .0;                     // set to mean model                
    } 
	    
    // calculate the initial error vector between the model and the image        
    if (opt_timings) timerModelEstimateTexDiff.start();    
	E = ModelEstimateTexDiff( image, c, s, delta_g, 0, useInterpolation );        
    if (opt_timings) timerModelEstimateTexDiff.stop();    
    if (E==-1.) { return CAAMOptRes(  .0, 0, -1. ); /* shape is outside */ }
    
    // save initial state     
    optStates.push_back( CAAMOptState( E, s, c, 0 ) ); // save state   

	// iterate    
    do {
		E_previous = E; // save old error			        

        // predict pose and model parameter displacement	        
        if (opt_timings) timerRt.start();
		delta_t = m_R_t*delta_g;  // pose			
        if (opt_timings) timerRt.stop();
        
        if (opt_timings) timerRc.start();
		delta_c = m_R_c*delta_g;  // model
        if (opt_timings) timerRc.stop();        
       
        // if the prediction above didn't improve th fit,
        // try amplify and later damp the prediction        
        for(int i=0;i<(disableDamping ? 1 : ndamps);i++) {                                  											

            // make damped c prediction
            c_est = c + delta_c*k_values[i];

            // convert pose prediction to actual size              
            double sizeRatio =  s.ShapeSize() / refSize;
            delta_t_corr = delta_t;       // copy prediction
            delta_t_corr[2] *= sizeRatio;
            delta_t_corr[3] *= sizeRatio;            
            delta_t_corr *= k_values[i];  // damp

            // constrain the updates
            ConstrainSearchParameters( c_est, delta_t_corr );    

            // update pose (damped)
            s_est = s; s_est.Displace( delta_t_corr );              
            
            // calculate the error vector between the predicted model and the image        
            if (opt_timings) timerModelEstimateTexDiff.start();             
	        E = ModelEstimateTexDiff( image, c_est, s_est, delta_g, 0, useInterpolation );                        
            if (opt_timings) timerModelEstimateTexDiff.stop();
            if (E!=-1. && E<E_previous) { break; }    // error improved -> make new prediction
		}        
			
		if ( E!=-1. && E<E_previous ) {
			
			// the error improved, accept the new c and s  
            c = c_est;
            s = s_est;
            optStates.push_back( CAAMOptState(E, s, c, i) ); // save state			            
		}
						
		++iter;

	} while( E!=-1. && E_previous-E>covergence_level*E && iter<maxIterations ); 
    
    int last = optStates.size()-1;  // this is the last good iteration      
    s = optStates[last].shape;      // return the best shape in s
    c = optStates[last].c;          // return the best model param in c                                                                               
   
    // return convergence info (if requested)
    if (pOptStates) *pOptStates = optStates;

    // calculate optimization results	    
    CAAMOptRes results(  m_CombinedPCA.MahalanobisDistance( c ), iter, E );

    if (opt_timings) {

        timerTotal.stop();

        printf("OptimizeModel() timings:\n");
        printf("  ModelEstimateTexDiff()   : %6.2f ms\n", 
                timerModelEstimateTexDiff.getTime()*1000. );
        printf("  Rt multiplication        : %6.2f ms\n", 
                timerRt.getTime()*1000. );
        printf("  Rc multiplication        : %6.2f ms\n", 
                timerRc.getTime()*1000. );
        printf("  Total time               : %6.2f ms\n", 
                timerTotal.getTime()*1000. );
    }


	// return optimization results	
	return results;
}




/**

  @author   Mikkel B. Stegmann
  @version  7-7-2000

  @memo		Perform general-purpose optimization of the AAM. 

  @doc		Perform general-purpose optimization of the AAM using 
			simulated annealing, conjugate gradient, steepest 
			descent, BGFS or pattern search.  
  
  @param    image			The image beeing searched in.

  @param    s				The inital shape pose.

  @param    c				The initial model parameters.

  @param    maxIterations	The maximum allowed number of iterations.

  @param	similaritym	    The used similarity measure for the optimization:
								
							0	Non-normalized L_2 norm (default).

							1	The "Mahalanobis" distance (texture samples are
								regarded independent to increase performance).

							2	The Lorentzian error norm.

  @param	optimizer   Sets the optimer to use:
							
							1	Steepest Descent (default)
							2	Conjugate Gradient
							3	Quasi-Newton, BFGS
							4	Pattern search
							5	Simulated annealing
					
  
  @return   The final fit.
  
*/
CAAMModel::CAAMOptRes CAAMModel::OptimizeModelByFineTuning(	
                                                           
                const CDMultiBand<TAAMPixel> &image,
	       	    CAAMShape &s, CDVector &c,
		        const int maxIterations,
			    const int similaritym,	
				const int optimizer		) const {

	// make an aam optimizer
	CAAMOptimize AAMOpt( *this, s, image, similaritym );
	
	int n = m_CombinedPCA.NParameters();	

	// setup the optimization vector
	int nVx;
	
	nVx = n+4;
	
	CDVector vX( nVx );
	CDVector GradStepVec( nVx );
	
	for(int i=0;i<n;i++) {

		vX[i] = c[i];						// initial model parameters
		GradStepVec[i] = fabs(c[i])*.05;	// step size
	}		
	for(i=n;i<n+4;i++) {

		vX[i] = 0;				// displacement is zero intially		        
	}
    GradStepVec[n]   = .01;								// scale
    GradStepVec[n+1] = CAAMUtil::Deg2Rad(1);			// radians
    GradStepVec[n+2] = ReferenceShape().Width()*.01;    // pixels
    GradStepVec[n+3] = ReferenceShape().Height()*.01;   // pixels
    

	// do the fine-tuning
	CDOptimizeBase *pOptimizer;	
	try {

		switch(optimizer) {

			case 1:		pOptimizer = new CDOptimizeSD;
						break;
			case 2:		pOptimizer = new CDOptimizeCG;
						break;
			case 3:		pOptimizer = new CDOptimizeBFGS;
						break;
			case 4:		pOptimizer = new CDOptimizePS;
						break;
			case 5:		pOptimizer = new CDOptimizeSA;						
						break;
			default:	printf("Unknown optimizer type=5i\n", optimizer );
						break;
		}
		
		// set the maximum number of iterations
		pOptimizer->SetMaxFuncEval( maxIterations );	
		pOptimizer->SetMaxIterations( maxIterations );

		// do the optimization (using numerical gradidents)
		pOptimizer->MinimizeNum( vX, &AAMOpt, GradStepVec );	

	} catch(...) {

		printf("Error allocating general purpose optimizer!\n");
	}


	// get results
	double E;
	AAMOpt.OptResults( c, s, E );

	// calculate optimization results	    
    CAAMOptRes results(  m_CombinedPCA.MahalanobisDistance( c ), 
                         maxIterations, E );
	
	// deallocate
	delete pOptimizer;

    return results;
}



/**

  @author   Mikkel B. Stegmann
  @version  3-10-2000

  @memo     Reads the complete AAMModel from disk.

  @doc      Reads the complete AAMModel from disk.

  @see		WriteModel 
  
  @param    filename	Input filename without any extension.  
						E.g. if the files on disk are
						
							'model.txt' & 'model.amf' 
						  
							-> filename = 'model'
  
  @return   true on success, false on file errors.
  
*/
bool CAAMModel::ReadModel( const CString &filename ) {


	bool success = true;
	FILE *fh;

	fh = fopen( CAAMUtil::ForceExt( filename, "amf" ), "rb" );
	
	if (!fh) return false;
	
	double version;
	fread( &version, sizeof(double), 1, fh );

	if ( version!=m_dAMFVersion ) {

		printf(	"Error: Wrong .amf format version, v%.2f. Ver. %.2f was expected.\n", 
				version, m_dAMFVersion );

		fclose(fh);
		return false;
	}

    try {

	    // read misc settings
	    fread( &m_iModelReduction, sizeof(int), 1, fh );
	    fread( &m_dAddExtents, sizeof(double), 1, fh );
	    int use_hull;
	    fread( &use_hull, sizeof(int), 1, fh );
	    m_bUseConvexHull = use_hull==1;

        // PCA
	    m_ShapePCA.FromFile( fh );	
	    m_TexturePCA.FromFile( fh );	
	    m_CombinedPCA.FromFile( fh );
    
	    m_mShape2PixelWeights.FromFile( fh );
	    m_mQsEV.FromFile( fh );
	    m_mQgEV.FromFile( fh );
	    m_vMeanTexture.FromFile( fh );
        m_vMeanTextureOrg.FromFile( fh );	
	    m_sMeanAShape.FromFile( fh );

        CDVector aux(7);
        aux.FromFile( fh );
	    m_iTextureSamples   = (int)aux[0];
        m_iNShapes          = (int)aux[1];
        m_dMeanShapeSize    = aux[2];        
        m_iLearningMethod   = (int)aux[3];        
        m_iShapeTrunc       = (int)aux[4];
        m_iTextureTrunc     = (int)aux[5];
        m_iCombinedTrunc    = (int)aux[6];
	    
        // Regression matrices.
	    m_R_c.FromFile( fh );
	    m_R_t.FromFile( fh );

	    m_vTextureVar.FromFile( fh );
    	   

        // load texture transfer function 
        if(m_pTextureTF) {

		    delete m_pTextureTF;
	    }
        m_pTextureTF = AAMLoadTransferFunction( fh, this );


        //  initialize the ReferenceFrame and AnalyzeSynthesize objects
        if (m_pReferenceFrame) {

            delete m_pReferenceFrame;
            m_pReferenceFrame = NULL;
        }
        m_pReferenceFrame = new CAAMReferenceFrame;
        m_pReferenceFrame->FromFile( fh );


        if (m_pAnalyzeSynthesize) {

            delete m_pAnalyzeSynthesize;
            m_pAnalyzeSynthesize = NULL;
        }    
        m_pAnalyzeSynthesize = AAMLoadAnalyzerSynthesizer( fh, *this->m_pReferenceFrame ); 


        // close file
	    if ( 0!=fclose(fh) ) return false;


    } catch(...) {

        printf("An error occured during CAAMModel::ReadModel()\n");       
        return false;
    }

	return true;
}



/**

  @author   Mikkel B. Stegmann
  @version  2-21-2000

  @memo     Builds a texture vector from an image and a shape.

  @doc      Builds a texture vector from an image and a shape.
  
  @param    image			The image to sample in.

  @param    textureSamples	The normalized destination texture vector.

  @param	shape			The shape to sample from (in image coordinates).							    

  @param    normalize       Perform normalization after sampling.
                            Default true.

  @param    map             Perform mapping after sampling.
                            Default true.   
  
  @return   The number of samples done (zero if the shape is outside the image).
  
*/
int CAAMModel::SampleShape(	const CDMultiBand<TAAMPixel> &image,
							const CAAMShape &shape, 
							CDVector &textureSamples,							                             
                            const bool normalize,
                            const bool useInterpolation,
                            const bool map ) const {

    // test shape
    bool inside = CAAMUtil::ShapeInsideImage( shape, image );
    if (!inside) {

        // the shape is outside the image        
        return 0;
    }				


    // set input image
    //
    // For the OpenGL version this is a *very*
    // expensive operation and should only be done
    // if really needed, theirfore the small trick below.
    //    
    if ( m_sCurrentAnalyzeId!=image.Name() ) {

        // set image
        m_pAnalyzeSynthesize->SetAnalyzeImage( image );

        // set unique id
        CString id;
        id.Format( "SetAnalyzeImage id=%i", (int)clock() );
        ((CDMultiBand<TAAMPixel>&)image).SetName( id );

        // set current analyze image id
        ((CAAMModel*)this)->m_sCurrentAnalyzeId = id;
    }    

    // sample texture
    m_pAnalyzeSynthesize->Analyze( shape, textureSamples, useInterpolation );
    int nSamples = m_pReferenceFrame->NTextureSamples();


	// sample whiskers
	//       
    // usage:
    //
    // _putenv( "SAMPLE_WHISKERS=1" );
    //
    // default 0
    //
#ifdef USEAAMENV
    CString sampleWhiskersStr = getenv( "SAMPLE_WHISKERS" );
    bool sampleWhiskers = strcmp(sampleWhiskersStr,"1")==0;	
#else
    bool sampleWhiskers = false;
#endif
	if (sampleWhiskers) {

        // an extremely inefficient whiskers implementation
        // made real quick
        //
        // LATER: implement proper weigthning of texture samples
        //
		double scale = shape.ShapeSize() / ReferenceShape().ShapeSize();		
		int np = shape.NPoints();
		double weight = 2.;
		int	w_samples = (int)(.5+(weight*nSamples)/np); // number of samples per whisker

		double x1,x2,y1,y2;
		ReferenceShape().GetPointUS(0, x1, y1 );
		ReferenceShape().GetPointUS(1, x2, y2 );		
		double whiskerLength = 3.*sqrt( (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) );
		double wl =  scale*whiskerLength;	// in 'shape' pixels
		//printf("whiskerLength=%.1f, wl=%.1f, w_samples=%i, nSamples=%i\n", whiskerLength, wl, w_samples, nSamples );
		CAAMPoint p,p_outside, p_inside;
		CDVector whiskerSamples( np*w_samples*BANDS );
		double pix[BANDS];
		int iw = image.Width();
		int ih = image.Height();
		int s = 0;
		for(int i=0;i<np;i++) {

			p = shape.GetPoint(i);
			shape.Normal( i, p_outside, p_inside, wl );

			// sample
			double t,x,y;
			for(int j=0;j<w_samples;j++) {

				t = j/(w_samples-1.);
				x = (1.-t)*p.x + t*p_outside.x;
				y = (1.-t)*p.y + t*p_outside.y;
								
				if (x>0 && x<iw-1 && y>0 && y<ih-1) { image.Pixel1( x, y, pix ); }
				for(int k=0;k<BANDS;k++) { whiskerSamples[s++] = pix[k]; }
			}
		}

		nSamples += whiskerSamples.Length();

		CDVector tmp = textureSamples.VecCat( whiskerSamples );
		textureSamples.Resize( tmp.Length() );
		textureSamples = tmp;
	}

    // normalize the texture vector
    if (normalize) { NormalizeTexture( textureSamples ); }    


    // apply the current texture transfer function
    if (map) { TextureTF().Map( textureSamples ); }

    
	// return
	return nSamples;
}



/**

  @author   Mikkel B. Stegmann
  @version  2-24-2000

  @memo     Generates a shape based on a set of model parameters.

  @doc		Generates a shape based on a set of model parameters.
  
  @param    c			Input model parameters.
  @param	outShape	The generated shape (resizing are done inside this method). 
  
  @return   Nothing.
  
*/
void CAAMModel::ShapeInstance( const CDVector &c, CAAMShape &outShape ) const {

	int rows = m_sMeanAShape.Length();
	int cols = m_CombinedPCA.NParameters();
	
	if ( m_mShapeInstance.NCols()!=cols || m_mShapeInstance.NRows()!=rows	) {

		// cache this very expensive matrix product		
		((CAAMModel*)this)->m_mShapeInstance.Resize( rows, cols );
		((CAAMModel*)this)->m_mShapeInstance = m_ShapePCA.EigenVectors()*m_mShape2PixelWeights.Inverted()*m_mQsEV;
	}

	outShape  = m_sMeanAShape;
    outShape += m_mShapeInstance*c;
}


/**

  @author   Mikkel B. Stegmann
  @version  2-24-2000

  @memo     Generates a texture based on a set of model parameters.

  @doc		Generates a texture based on a set of model parameters.
  
  @param    c			Input model parameters.
  @param	outShape	The generated texture (resizing are done inside this method). 
  
  @return   Nothing.
  
*/
void CAAMModel::TextureInstance( const CDVector &c, CDVector &outTexture ) const {

	const int rows = m_vMeanTexture.Length();
	const int cols = m_CombinedPCA.NParameters();

	const int m  = rows;
	const int kc = cols;
	const int kt = m_TexturePCA.NParameters();
	bool doCache = m*(kc-kt) < kt*kc;		// See FAME paper

	outTexture.Resize( m_vMeanTexture.Length() );	
	outTexture  = m_vMeanTexture;
	if (doCache) {

		if ( m_mTextureInstance.NCols()!=cols || m_mTextureInstance.NRows()!=rows	) {

			// cache this expensive matrix product		
			((CAAMModel*)this)->m_mTextureInstance.Resize( rows, cols );
			((CAAMModel*)this)->m_mTextureInstance = m_TexturePCA.EigenVectors()*m_mQgEV;        
		}
		outTexture += m_mTextureInstance*c;
	} else {

		outTexture += m_TexturePCA.EigenVectors()*(m_mQgEV*c);
	}
}


/**

  @author   Mikkel B. Stegmann
  @version  2-28-2000

  @memo     Generates a shape based on a set of shape b-parameters.

  @doc		Generates a shape based on a set of shape b-parameters.
  
  @param    b_s			Shape parameters
  @param    outShape	Output shape (resized inside method).
  
  @return   Nothing.
  
*/
void CAAMModel::ShapePCAInstance( const CDVector &b_s, CAAMShape &outShape ) const {

    // since the CAAMDeformPCA only handles vectors 
    // all additional data such as point aux info etc.
    // are lost in the pca-deformation
    //
    // hence we need to copy it into the 'outShape'
    // object explicitly
    //
	outShape = m_sMeanAShape;

    // do deformation
    m_ShapePCA.Deform( b_s, outShape );
}


/**

  @author   Mikkel B. Stegmann
  @version  3-27-2000

  @memo     Projects the shape and texture into c-space.

  @doc      Projects the shape and texture into c-space i.e. the
            combined model parameters. 
  
  @param    shape	The input shape aligned to the (aligned) mean shape.
  @param    texture	The corresponding normalized texture.
  @param	c		The resulting model parameters.
  
  @return   Nothing.
  
*/
void CAAMModel::ShapeTex2Combined(	const CAAMShape &shape, 
									const CDVector &texture, 
									CDVector &c ) const {

	CDVector b;
	ShapeTex2Param( shape, texture, b );
	ShapeTexParam2Combined( b, c );
}



/** 

  @author   Mikkel B. Stegmann
  @version  3-6-2000

  @memo     Extract the b-parameters from a shape and corresponding texture.
  @doc      Extract the b-parameters from a shape and corresponding texture by
			inverting the shape and texture pca projection.  

			Assumes that the shape and texture PCA are done beforehand.
  
  @param    shape	The input shape aligned to the (aligned) mean shape.
  @param    texture	The corresponding normalized texture.
  @param	b		The resulting concatenated b vector.
  
  @return   Nothing.
  
*/
void CAAMModel::ShapeTex2Param(	const CAAMShape &shape, 
								const CDVector &texture, 
							    CDVector &b ) const {
			
	int nbTexParam   = m_TexturePCA.NParameters();
	int nbShapeParam = m_ShapePCA.NParameters();   
    
    CDVector b_s( nbShapeParam ), b_g( nbTexParam );
    b.Resize( nbTexParam+nbShapeParam );
    
    b_s	=	m_mShape2PixelWeights *
            m_ShapePCA.EigenVectors().Transposed() *
            (shape-m_sMeanAShape);
    
    b_g	=	m_TexturePCA.EigenVectors().Transposed() *
            (texture-m_vMeanTexture);			
    
    // concat vectors
    for(int j=0;j<nbShapeParam;j++)			b[j] = b_s[j];
	for(j=nbShapeParam;j<b.Length();j++)	b[j] = b_g[j-nbShapeParam];		
}



/**

  @author   Mikkel B. Stegmann
  @version  2-28-2000

  @memo     Transforms the concatenated b-parameters into c-parameters.
  @doc      Transforms the concatenated b-parameters into combined model
			parameters.
  @see      ShapeTex2Param
  
  @param    b	Concatenated shape (weighted) and texture parameters
  @param    c	Combined model parameters (resized inside function).
  
  @return   Nothing.
  
*/
void CAAMModel::ShapeTexParam2Combined(	const CDVector &b, 
											CDVector &c ) const {

	c.Resize( m_CombinedPCA.NParameters() );        
	c = m_CombinedPCA.EigenVectors().Transposed()*b;
}



/**

  @author   Mikkel B. Stegmann
  @version  3-10-2000

  @memo     Writes the complete AAMModel to disk as a .txt and an .amf file.

  @doc      Writes the complete AAMModel to disk as a .txt and an .amf file.

  @see      ReadModel
  
  @param    filename    Output filename without any extension.

  @param    txt_only    If true binary model data is not written.
  
  @return   true on success, false on file errors.
  
*/
bool CAAMModel::WriteModel( const CString &filename, const bool txt_only ) const {

	bool success = true;
	CString s;
	FILE *fh;

	/////////////////////////////////////////////////////
	// write the human readable txt file
	/////////////////////////////////////////////////////
	fh = fopen( filename+CString(".txt"), "wt" );
	if (!fh) return false;

	fprintf( fh, "######################################################################\n" );	
	fprintf( fh, "\nActive Appearance Model File \n");

	CTime t = CTime::GetCurrentTime();
	s = t.Format( "%A %B %d - %Y [%H:%M]" );
	fprintf( fh, "\nWritten                   : %s\n", s);	
	fprintf( fh, "\nFormat version            : %.2f\n", m_dAMFVersion);
    fprintf( fh, "\nBuild time                : %s (%.1f secs)\n", 
                  CAAMUtil::Secs2Mins(m_dBuildTime), 
                  m_dBuildTime );    
	fprintf( fh, "\nShapes                    : %i\n", m_iNShapes );
	fprintf( fh, "\nShape points              : %i\n", m_sMeanAShape.NPoints() );
    fprintf( fh, "\nTexture Bands             : %i\n", this->NBands() );      
    CString szPerBand;
    if (NBands()!=1) szPerBand.Format( " (%i samples/band)", m_iTextureSamples/this->NBands() );
	fprintf( fh, "\nTexture samples           : %i%s\n", m_iTextureSamples, szPerBand );    
    
    fprintf( fh, "\nTexture TF                : %s\n", m_pTextureTF->TypeName() );            
    const CString str = m_pTextureTF->TypeInfo();
    if (str.GetLength()>0) {

        fprintf( fh, "\nTexture TF info           : %s\n", str );            
    }

	fprintf( fh, "\nModel reduction           : %i\n", m_iModelReduction );
	fprintf( fh, "\nAdd Shape Extents         : %.0f\n", m_dAddExtents );
	fprintf( fh, "\nConvex hull used          : %s\n", m_bUseConvexHull ? "Yes" : "No" );	
	fprintf( fh, "\nTangent space used        : %s\n", m_bUseTangentSpace ? "Yes" : "No" );
	fprintf( fh, "\nLearning method           : %i\n", m_iLearningMethod );
	fprintf( fh, "\nShape truncation level    : %i (variance: %.3g/%.3g)%\n", 
			 m_iShapeTrunc, m_ShapePCA.EigenValues().Sum(), m_ShapePCA.EigenValuesOrg().Sum() );
    fprintf( fh, "\nTexture truncation level  : %i (variance: %.3g/%.3g)%\n", 
			 m_iTextureTrunc, m_TexturePCA.EigenValues().Sum(), m_TexturePCA.EigenValuesOrg().Sum() );
    fprintf( fh, "\nCombined truncation level : %i (variance: %.3g/%.3g)%\n",
			 m_iCombinedTrunc, m_CombinedPCA.EigenValues().Sum(), m_CombinedPCA.EigenValuesOrg().Sum() );
	fprintf( fh, "\nParameters used           : %i %\n", m_CombinedPCA.NParameters() );    
    fprintf( fh, "\nMean shape area           : %.2f %\n", ReferenceShape().Area( m_bUseConvexHull ) );        
	double acc;	
    int n;
	
	// modes of variation in the combined model
	fprintf( fh, "\nCombined mode variation : \n");
	acc=0;	
    n = m_CombinedPCA.NParametersOrg();
	for(int i=1;i<=m_CombinedPCA.NParameters();i++) {
		
		acc += 100.*m_CombinedPCA.ParameterWeightOrg( n-i, true );
		fprintf( fh, "\t%i \t%6.2f%% \t(%6.2f%%)\n", i, 
                100.*m_CombinedPCA.ParameterWeightOrg( n-i, true ),
                acc );
	}

	// modes of variation in the shape model
	fprintf( fh, "\nShape mode variation : \n");
	acc=0;	
    n = m_ShapePCA.NParametersOrg();
	for(i=1;i<=m_ShapePCA.NParameters();i++) {
		
		acc += 100.*m_ShapePCA.ParameterWeightOrg( n-i, true );
		fprintf( fh, "\t%i \t%6.2f%% \t(%6.2f%%)\n", i, 
                100.*m_ShapePCA.ParameterWeightOrg( n-i, true ),
                acc );
	}

	// modes of variation in the texture model
	fprintf( fh, "\nTexture mode variation : \n");
	acc=0;	
    n = m_TexturePCA.NParametersOrg();
	for(i=1;i<=m_TexturePCA.NParameters();i++) {
		
		acc += 100.*m_TexturePCA.ParameterWeightOrg( n-i, true );
		fprintf( fh, "\t%i \t%6.2f%% \t(%6.2f%%)\n", i, 
                100.*m_TexturePCA.ParameterWeightOrg( n-i, true ),
                acc );
	}


	fprintf( fh, "\n" );
	fprintf( fh, "######################################################################\n" );

	if ( fclose(fh) ) return false;


	/////////////////////////////////////////////////////
	// write the binary data file
	/////////////////////////////////////////////////////
    if (txt_only!=true) {

	    fh = fopen( filename+CString(".amf"), "wb" );
	    if (!fh) return false;

	    // write version number
	    fwrite( &m_dAMFVersion, sizeof(double), 1, fh );

	    // write misc settings
	    fwrite( &m_iModelReduction, sizeof(int), 1, fh );
	    fwrite( &m_dAddExtents, sizeof(double), 1, fh );
	    int use_hull = m_bUseConvexHull ? 1 : 0;
	    fwrite( &use_hull, sizeof(int), 1, fh );

        // PCA        
	    m_ShapePCA.ToFile( fh );	
	    m_TexturePCA.ToFile( fh );	    // big object: nbTexSamples * nbTParam+1 * 8 bytes
	    m_CombinedPCA.ToFile( fh );	

	    m_mShape2PixelWeights.ToFile( fh );  
	    m_mQsEV.ToFile( fh );   // cache matrix: could be extracted from m_CombinedPCA
	    m_mQgEV.ToFile( fh );   // cache matrix: could be extracted from m_CombinedPCA

	    m_vMeanTexture.ToFile( fh );	// big vector: nbTexSamples * 8 bytes
        m_vMeanTextureOrg.ToFile( fh );	// big vector: nbTexSamples * 8 bytes
	    m_sMeanAShape.ToFile( fh );        

	    CDVector aux(7);
        aux[0] = m_iTextureSamples;
        aux[1] = m_iNShapes;
        aux[2] = m_dMeanShapeSize;    
        aux[3] = m_iLearningMethod;    
        aux[4] = m_iShapeTrunc;
        aux[5] = m_iTextureTrunc;
        aux[6] = m_iCombinedTrunc;
	    aux.ToFile( fh );
	    
        // Regression matrices.
	    m_R_c.ToFile( fh );     // big matrix: nbTexSamples * nbCParam * 8 bytes
	    m_R_t.ToFile( fh );     // big matrix: nbTexSamples * 4 * 8 bytes
	    
	    m_vTextureVar.ToFile( fh ); // big vector: nbTexSamples * 8 bytes
    
        m_pTextureTF->ToFile( fh );

        m_pReferenceFrame->ToFile( fh );
        m_pAnalyzeSynthesize->ToFile( fh );


        // close file
	    if ( fclose(fh) ) return false;
    }

	return success;
}


/**

  @author   Mikkel B. Stegmann
  @version  5-4-2000

  @memo     Plots the pixel variance into the mean shape.

  @doc      Plots the variance of each pixel in the model over
			the training set into the mean shape and saves the
			image.
  
  @param    filename	Output image filename.
  
  @return   Nothing.
  
*/
void CAAMModel::WriteVarianceMap( const CString &filename ) const {
	
	CDVector vByteVariances;
	vByteVariances = m_vTextureVar;
			
	// make variance map
	CDMultiBand<TAAMPixel> image;
	ShapeFreeImage( vByteVariances, image );
	image.WriteBandedFile( filename );
}


/**

  @author   Mikkel B. Stegmann
  @version  3-27-2000

  @memo     Returns the reference shape.

  @doc      Returns the reference shape where all texture sampling
			and comparison should be done. 

            The reference shape is defined as the mean shape size to 
			mean size and moved to the fourth quadrant.
  
  @return   The reference shape.
  
*/
const CAAMShape &CAAMModel::ReferenceShape() const { 
    
    return m_pReferenceFrame->RefShape();
}  


/**

  @author   Mikkel B. Stegmann
  @version  3-22-2000

  @memo     Converts a set of b-vectors to combined model parameters (c-vectors).

  @doc		Converts a set of b-vectors to combined model parameters (c-vectors).
    
  @param    bVectors	The input b-vectors.
  @param    cVectors	The output c-vectors.
  
  @return	Nothing.
  
*/
void CAAMModel::ShapeTexParam2Combined(	const std::vector<CDVector> &bVectors, 
										std::vector<CDVector> &cVectors ) const {

	CDVector c;
	cVectors.clear();

	for(unsigned int i=0;i<bVectors.size();i++) {

		ShapeTexParam2Combined( bVectors[i], c );
		cVectors.push_back( c );
	}
}



/**

  @author   Mikkel B. Stegmann
  @version  10-27-2000

  @memo     Copy constructor.

  @doc      Copy constructor.
   
  @param    m   Object to copy from.  
  
  @return  Nothing. 
  
*/
CAAMModel::CAAMModel( const CAAMModel &m ) : 
    
    m_pTextureTF(NULL),
    m_pReferenceFrame(NULL),
    m_pAnalyzeSynthesize(NULL) {

    *this = m;
}


/**

  @author   Mikkel B. Stegmann
  @version  10-30-2000

  @memo     Assignment operator.

  @doc      Assignment operator.
  
  @param    m   Object to copy from.  
    
  @return   This;
  
*/
CAAMModel& CAAMModel::operator=(const CAAMModel &m) {

    // assignment copy of all members
    m_ShapePCA = m.m_ShapePCA;    
    m_TexturePCA = m.m_TexturePCA;
    m_CombinedPCA = m.m_CombinedPCA;
	m_mShape2PixelWeights = m.m_mShape2PixelWeights;  
	m_dAMFVersion = m.m_dAMFVersion   ;
	m_mQsEV = m.m_mQsEV;
	m_mQgEV = m.m_mQgEV;
    m_vMeanTexture = m.m_vMeanTexture;
    m_vMeanTextureOrg = m.m_vMeanTextureOrg;
    m_sMeanAShape = m.m_sMeanAShape;        
	m_mShapeInstance = m.m_mShapeInstance;	
	m_mTextureInstance = m.m_mTextureInstance;
	m_vTextureVar = m.m_vTextureVar;
	m_iTextureSamples = m.m_iTextureSamples;
    m_iNShapes = m.m_iNShapes;
	m_dMeanShapeSize = m.m_dMeanShapeSize;	
	m_R_c = m.m_R_c;
    m_R_t = m.m_R_t;    
    m_iModelReduction = m.m_iModelReduction;
	m_dAddExtents = m.m_dAddExtents;
	m_bUseConvexHull = m.m_bUseConvexHull;
	m_bUseTangentSpace = m.m_bUseTangentSpace;
	m_iLearningMethod = m.m_iLearningMethod;

	m_iShapeTrunc = m.m_iShapeTrunc;
    m_iTextureTrunc = m.m_iTextureTrunc;
    m_iCombinedTrunc = m.m_iCombinedTrunc;  
    
    m_dBuildTime = m.m_dBuildTime;

    // deallocate
	if (m_pTextureTF) {

		delete m_pTextureTF;
        m_pTextureTF = NULL;
	}
    if (m.m_pTextureTF) {

        m_pTextureTF = m.m_pTextureTF->Clone();        
    }


    if (m_pReferenceFrame) {

        delete m_pReferenceFrame;
        m_pReferenceFrame = NULL;
    }

    if (m.m_pReferenceFrame) {

        m_pReferenceFrame = new CAAMReferenceFrame;
        *m_pReferenceFrame = *m.m_pReferenceFrame;
    }


    if (m_pAnalyzeSynthesize) {

        delete m_pAnalyzeSynthesize;
        m_pAnalyzeSynthesize = NULL;
    }

    if (m.m_pAnalyzeSynthesize) {

        m_pAnalyzeSynthesize = m.m_pAnalyzeSynthesize->Clone( *this->m_pReferenceFrame );
    }   
    
    return *this; 
}



/**

  @author   Mikkel B. Stegmann
  @version  12-5-2000

  @memo     Projects a shape into a set of c parameters.

  @doc      Projects a shape into a set of c parameters.  
  
  @param    shape   The input shape (assumed to be in abs coordinates).
  @param    image   The image where the shape is placed.
  @param    c       The output combined model parameters.  
  
  @return   Nothing.
  
*/
void CAAMModel::Shape2Combined( const CAAMShape &shape, 
                                const CDMultiBand<TAAMPixel> &image,
                                CDVector &c   ) const {
    
	// build texture vector from annotation and host image	
	CDVector texture;
	

    // sample texture
    SampleShape( image, shape, texture, true );
	

	/////////////////////////////////////////////////////
	// approx the model (c) parameters for this example
	/////////////////////////////////////////////////////

	// copy the input shape
	CAAMShape shapeCopy( shape );

	// align the shape to the mean aligned shape
	shapeCopy.AlignTo( m_sMeanAShape );

	// extract model parameters from shape and texture
	CDVector b;
	ShapeTex2Param( shapeCopy, texture, b );
	ShapeTexParam2Combined( b, c );
}


/**

  @author   Mikkel B. Stegmann
  @version  12-5-2000

  @memo     Projects a shape into a set of shape parameters.

  @doc      Projects a shape into a set of shape parameters,            
  
  @param    shape   The input shape (assumed to be in abs parameters).  
  @param    b       The output shape model parameters.  
  
  @return   Nothing.
  
*/
void CAAMModel::Shape2Param( const CAAMShape &shape, 
                             CDVector &b_s ) const {

    CAAMShape alignedShape = shape;
    alignedShape.AlignTo( m_sMeanAShape );
    
    b_s.Resize( m_ShapePCA.NParameters() );
    b_s	=	m_ShapePCA.EigenVectors().Transposed() *
            (alignedShape-m_sMeanAShape);    
}



/**

  @author   Mikkel B. Stegmann
  @version  11-4-2002

  @memo     Converts combined model parameters to shape parameters.

  @doc      Converts combined model parameters to shape parameters.

  @param    c		Combined model parameters.
  
  @param    b_s     Output non-weighted shape parameters.
  
  @return   Nothing.
  
*/
void CAAMModel::Combined2ShapeParam( const CDVector &c, CDVector &b_s ) const {
                                    
    b_s.Resize( m_ShapePCA.NParameters() );                                
    b_s = m_mShape2PixelWeights.Inverted()*(m_mQsEV*c);
}



/**

  @author   Mikkel B. Stegmann
  @version  11-4-2002

  @memo     Converts combined model parameters to texture parameters.

  @doc      Converts combined model parameters to texture parameters.

  @param    c		Combined model parameters.
  
  @param    b_g     Output texture parameters.
  
  @return   Nothing.
  
*/
void CAAMModel::Combined2TexParam( const CDVector &c, CDVector &b_g ) const {

    b_g.Resize( m_TexturePCA.NParameters() );
    b_g = m_mQgEV*c;
}



/**

  @author   Mikkel B. Stegmann
  @version  11-4-2002

  @memo     Projects shape and texture parameters into the combined eigenspace.

  @doc      Projects shape and texture parameters into the combined eigenspace.

  @see      Combined2TexParam, Combined2ShapeParam
  
  @param    b_s     Input shape parameters.
  
  @param    b_g     Input texture parameters.
  
  @param    c		Resulting combined model parameters.
  
  @return   Nothing.
  
*/
void CAAMModel::ShapeTexParam2Combined(	const CDVector &b_s,
                                        const CDVector &b_g, 
									    CDVector &c ) const {

    int ns = b_s.Length();
    int nt = b_g.Length();
    int nc = m_CombinedPCA.NParameters();

    CDVector b_sw( ns ), b( ns+nt );
    b_sw = m_mShape2PixelWeights*b_s;
    b = b_sw.VecCat( b_g );  
    this->ShapeTexParam2Combined( b, c );
}