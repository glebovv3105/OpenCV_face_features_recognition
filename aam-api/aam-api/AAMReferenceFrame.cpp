/* $Id: AAMReferenceFrame.cpp,v 1.3 2003/04/23 14:50:01 aam Exp $ */
#include "AAMReferenceFrame.h"
#include "AAMShape.h"
#include "AAMMesh.h"
#include "AAMDelaunay.h"
#include "DMultiBand.h"

/**

  @author   Mikkel B. Stegmann
  @version  6-7-2002

  @memo     Constructor.

  @doc      Constructor. Sets up an empty reference frame. 
			Use Setup() to make such an object any useful.
  
  @return   Nothing.
  
*/
CAAMReferenceFrame::CAAMReferenceFrame() : 

    m_bUseConvexHull(false),
    m_pReferenceShape(NULL),
    m_pMaskImage(NULL),
    m_pReferenceMesh(NULL) {

}


/**

  @author   Mikkel B. Stegmann
  @version  6-13-2002

  @memo     Assignment operator.

  @doc      Assignment operator.
  
  @param    rf	Object to copy from.
  
  @return   Nothing.
  
*/
CAAMReferenceFrame& CAAMReferenceFrame::operator=(const CAAMReferenceFrame &rf) {

    // deallocate members
    if (m_pReferenceShape) delete m_pReferenceShape;
    if (m_pReferenceMesh)  delete m_pReferenceMesh;
    if (m_pMaskImage)      delete m_pMaskImage;  


    // deep copy of source data members
    m_bUseConvexHull   = rf.m_bUseConvexHull;
    m_iTextureSamples  = rf.m_iTextureSamples;
    
    m_pMaskImage       = new CDMultiBand<TAAMPixel>;
    *m_pMaskImage      = *rf.m_pMaskImage;

    m_pReferenceMesh   = new CAAMMesh;
    *m_pReferenceMesh  = *rf.m_pReferenceMesh;

    m_pReferenceShape  = new CAAMShape;
    *m_pReferenceShape = *rf.m_pReferenceShape;

    m_ScanlineParts    = rf.m_ScanlineParts;

    return *this;
}



/**

  @author   Mikkel B. Stegmann
  @version  6-13-2002

  @memo     Copy constructor.

  @doc      Copy constructor.
  
  @param    rf	Reference frame object to be copied by construction.
  
  @return   Nothing.
  
*/
CAAMReferenceFrame::CAAMReferenceFrame( const CAAMReferenceFrame &rf ) :

    m_pReferenceShape(NULL),
    m_pReferenceMesh(NULL),
    m_pMaskImage(NULL) {

    *this = rf;
}



/**

  @author   Mikkel B. Stegmann
  @version  6-7-2002

  @memo     Sets up the class with the reference shape.

  @doc      Sets up the class with the reference shape 
			and information about how to determine the extent
			of the shape.
  
  @param    referenceShape	The reference shape.

  @param    useConvexHull	If true the convex hull of the reference
							shape is used to determine its extent.
  
  @return   Nothing.
  
*/
void CAAMReferenceFrame::Setup( const CAAMShape &referenceShape,
                                const bool useConvexHull ) {


    if (m_pReferenceShape) delete m_pReferenceShape;
    if (m_pReferenceMesh)  delete m_pReferenceMesh;
    if (m_pMaskImage)      delete m_pMaskImage;  

    m_pReferenceShape = new CAAMShape(referenceShape);
    m_bUseConvexHull  = useConvexHull;

    // move shape to fourth quadrant
    double xMin = m_pReferenceShape->MinX();
    double yMin = m_pReferenceShape->MinY();
    m_pReferenceShape->Translate( -xMin, -yMin );
    
    // calculate mask image and scanline info
    CalcMaskImage( m_bUseConvexHull );
    CalcScanLines();       
}



/**

  @author   Mikkel B. Stegmann
  @version  6-13-2002

  @memo     Writes the class data to disk. 

  @doc      This method writes the class data to disk. Actually it is
			only the reference shape and the convex hull option that
			is written. The rest is reconstructed during read.
  
  @param    fh	Open file handle to a binary file.
  
  @return   Nothing.
  
*/
void CAAMReferenceFrame::ToFile( FILE *fh ) const {

    int i = m_bUseConvexHull ? 1 : 0;

    m_pReferenceShape->ToFile( fh );
    fwrite( &i, sizeof(int), 1, fh );
}



/**

  @author   Mikkel B. Stegmann
  @version  6-13-2002

  @memo     Reads the class data form disk. 

  @doc      This method reads the class data from disk. Actually it is
			only the reference shape and the convex hull option that
			is read. The rest is reconstructed.
  
  @param    fh	Open file handle to a binary file.
  
  @return   Nothing.
  
*/
void CAAMReferenceFrame::FromFile( FILE *fh ) {

    int i;
    CAAMShape shape;

    // read data
    shape.FromFile( fh );
    fread( &i, sizeof(int), 1, fh );

    // reconstruct remaining data members
    this->Setup( shape, i==1 );
}

	

/**

  @author   Mikkel B. Stegmann
  @version  6-7-2002

  @memo     Writes some of the class data to disk.

  @doc      This method writes the reference shape, the mask image 
			and a test of the Image2Vector() and Vector2image() to 
			the current directory.

  @return   Nothing.
  
*/
void CAAMReferenceFrame::DebugDump() {

    printf("mask size: %ix%i\n", m_pMaskImage->Width(), m_pMaskImage->Height() ); 
    m_pReferenceShape->SetHostImage( "refImage.bmp" );
    m_pReferenceShape->WriteASF( "ref.asf", m_pMaskImage->Width(), m_pMaskImage->Height() );
    m_pMaskImage->WriteBandedFile( "ref.bmp" );
    
    CDVector v;
    Image2Vector( *m_pMaskImage, v );
    v.ToMatlab( "vv.m","v","",false);

    CDMultiBand<TAAMPixel> img2;
    Vector2Image( v, img2 );
    img2.WriteBandedFile( "img2.bmp" );
}



/**

  @author   Mikkel B. Stegmann
  @version  6-7-2002

  @memo     Destructor.

  @doc      Destructor. Deletes any reference shape, mesh and mask image.
  
  @return   Nothing.
  
*/
CAAMReferenceFrame::~CAAMReferenceFrame() {

    if (m_pReferenceShape) delete m_pReferenceShape;
    if (m_pReferenceMesh)  delete m_pReferenceMesh;
    if (m_pMaskImage)      delete m_pMaskImage;  
}



/**

  @author   Mikkel B. Stegmann
  @version  6-7-2002

  @memo     Calculates a reference mask image.

  @doc      This method calculates a reference mask image, where the
			shape area is set to 255 and the background to zero.
			The resulting image is stored in 'm_pMaskImage'.
  
  @param    useConvexHull	If true the convex hull is used to define
							the extent of the reference shape.
  
  @return   Nothing.
  
*/
void CAAMReferenceFrame::CalcMaskImage( const bool useConvexHull ) {

    // setup
    TAAMPixel p255[BANDS];                
    SPAWN( p255, 255)
    if (m_pMaskImage) delete m_pMaskImage;  


    // allocate mask image     
    int width  = (int)ceil( m_pReferenceShape->MaxX() );
    int height = (int)ceil( m_pReferenceShape->MaxY() );
    m_pMaskImage = new CDMultiBand<TAAMPixel>( width, height, BANDS, 
											   evisimoptDontAlignMemory );
       
    // set background to black
    m_pMaskImage->FillPixels(0);           
    
          
    // make reference mesh
    m_pReferenceMesh = new CAAMMesh;
    CAAMDelaunay::MakeMesh( *m_pReferenceShape, *m_pReferenceMesh, useConvexHull==false );
    
    int c=0;
    
    // calc mask image (set shape area to 255)
    for(int y=0;y<height;y++) {

        for(int x=0;x<width;x++) {                        

            if ( m_pReferenceMesh->IsInside( CAAMPoint(x,y) ) ) {
                
	            m_pMaskImage->Pixel( x, y ) = 255;
                ++c;
            }
        }
    }    
}



/**

  @author   Mikkel B. Stegmann
  @version  6-7-2002

  @memo     Calculates the scanlines of the mask image.

  @doc      This method calculates the scanlines of the mask image. 
			A scanline is a subpart of a horsontal image line that
			is occupied but a subpart of the reference shape.

			The resulting scanlines are used for quick'n'easy conversion
			from spatial to vector layout (and back).

			Scanlines are stored in 'm_ScanlineParts'.
 
  @return   Nothing.
  
*/
void CAAMReferenceFrame::CalcScanLines() {

    int width  = m_pMaskImage->Width();    
    int height = m_pMaskImage->Height();         

    // calc scanline vector    
    m_iTextureSamples = 0;
    for(int y=0;y<height;y++) {

        int x = 0;
        do {
            
            sScanLinePart s;
            s.x1 = -1;
            s.x2 = -1;
            s.y  =  y;            
            while( m_pMaskImage->Pixel(x,y)!=255 && x<width) ++x;
        
            if (x!=width) {

                s.x1 = x;
                while( m_pMaskImage->Pixel(x,y)==255 && x<width) ++x;                     
                s.x2 = x-1;
            }
            if (s.x1!=-1) {

                m_ScanlineParts.push_back( s );
                m_iTextureSamples += s.x2-s.x1+1;
            }
                                
        } while(x<width);
    }  

    m_iTextureSamples *= BANDS;     // one sample per band
}



/**

  @author   Mikkel B. Stegmann
  @version  6-7-2002

  @memo     Returns the width of the reference shape.

  @doc      Returns the width of the reference shape.
  
  @return   The width.
  
*/
double CAAMReferenceFrame::RefShapeWidth() const {    

    return m_pReferenceShape->Width();
}



/**

  @author   Mikkel B. Stegmann
  @version  6-7-2002

  @memo     Returns the height of the reference shape.

  @doc      Returns the height of the reference shape.
  
  @return   The height.
  
*/
double CAAMReferenceFrame::RefShapeHeight() const {    

    return m_pReferenceShape->Height();
}    



/**

  @author   Mikkel B. Stegmann
  @version  6-7-2002

  @memo     Conversion from shape-free image to a texture vector.

  @doc      This method performs a quick conversion from a 
			shape-free image to a texture vector.

  @see      Vector2Image
  
  @param    refImg	Destination reference image.

  @param    v		Input texture vector.
  
  @return   Nothing.
  
*/
void CAAMReferenceFrame::Image2Vector( const CDMultiBand<TAAMPixel> &refImg, 
                                       CDVector &v ) const {

    assert( refImg.Width()==m_pMaskImage->Width() );
    assert( refImg.Height()==m_pMaskImage->Height() );

    if ( v.Length()!=m_iTextureSamples ) {

        v.Resize( m_iTextureSamples );
    }
    
	bool safe = false;
	if (safe) {

		// safe version: does not assume anything about the
		// memory layout of the image or the vector (slow)
		int c=0, n = m_ScanlineParts.size();           
		for(int i=0;i<n;i++) {
	       
			int y   = m_ScanlineParts[i].y;
			int end = m_ScanlineParts[i].x2;

			for(int x=m_ScanlineParts[i].x1;x<=end;x++) {
	                            
				for(int k=0;k<BANDS;k++) {
	                
					v[c++] = refImg.Pixel( x, y, k );
				}
			}
		}       	      
		assert(c==m_iTextureSamples);

	} else {

		// UNSAFE version (more than 10 times faster)
		register unsigned int n = m_ScanlineParts.size();           		
		TAAMPixel *pPixels = &((CDMultiBand<TAAMPixel>)refImg).Pixel(0,0,0);
		TAAMPixel *pScanline;
		unsigned int scanLineWidth = refImg.Width()*BANDS;
		double *pVec = &(v[0]);	
		for(unsigned int i=0;i<n;i++) {
	       
			register unsigned int x = m_ScanlineParts[i].x1;        
			register unsigned int w = (m_ScanlineParts[i].x2-x+1)*BANDS;			
			pScanline = pPixels+m_ScanlineParts[i].y*scanLineWidth+x*BANDS;							
			for(unsigned int j=0;j<w;j++) { 
								
				// the expensive part here is to access the
				// rather large amount of data in pVec
				//
				// cache prefecting has been tried on the 
				// Athlon FB 1.1 GHz with little effect :-(
				pVec[j] = pScanline[j]; 
			}
			pVec += j;
		}          		
	}
}



/**

  @author   Mikkel B. Stegmann
  @version  6-7-2002

  @memo     Conversion from a texture vector to a shape-free image.

  @doc      This method performs a quick conversion from a 
			texture vector to a shape-free image.

  @see      Vector2Matrix, Image2Vector
  
  @param    refImg	Destination reference image.

  @param    v		Input texture vector.
  
  @return   Nothing.
  
*/
void CAAMReferenceFrame::Vector2Image( const CDVector &v, 
                                       CDMultiBand<TAAMPixel> &outImg ) const {

    if ( outImg.Width() !=m_pMaskImage->Width()  ||
         outImg.Height()!=m_pMaskImage->Height() ||
         outImg.NBands()!=BANDS                     ) {

        outImg = CDMultiBand<TAAMPixel>( m_pMaskImage->Width(),
                                         m_pMaskImage->Height(),
                                         BANDS,
                                         evisimoptDontAlignMemory );
    }

    int c=0, n = m_ScanlineParts.size();       
    for(int i=0;i<n;i++) {
       
        int y   = m_ScanlineParts[i].y;
        int end = m_ScanlineParts[i].x2;

        for(int x=m_ScanlineParts[i].x1;x<=end;x++) {
                            
            for(int k=0;k<BANDS;k++) {
                
                outImg.Pixel( x, y, k ) = (TAAMPixel)v[c++];
            }
        }
    }       
       
    assert(c==m_iTextureSamples);
}



/**

  @author   Mikkel B. Stegmann
  @version  6-7-2002

  @memo     Conversion from a texture vector to a shape-free matrix.

  @doc      This method performs a quick conversion from a 
			texture vector to a shape-free image on matrix 
			form.

			Will currently only work on single band AAMs.

  @see      Vector2Matrix, Image2Vector
  
  @param    refImg	Destination reference image.

  @param    v		Input texture vector.
  
  @return   Nothing.
 
*/
void CAAMReferenceFrame::Vector2Matrix( const CDVector &v, CDMatrix &m ) const {

	assert(BANDS==1);
    
    m.Resize( m_pMaskImage->Height(), m_pMaskImage->Width() );               

    int c=0, n = m_ScanlineParts.size();       
    for(int i=0;i<n;i++) {
       
        int y   = m_ScanlineParts[i].y;
        int end = m_ScanlineParts[i].x2;

        for(int x=m_ScanlineParts[i].x1;x<=end;x++) {
                                                       
            m[y][x] = v[c++];            
        }
    }       
       
    assert(c==m_iTextureSamples);
}



/**

  @author   Mikkel B. Stegmann
  @version  6-7-2002

  @memo     Returns the width of the reference image.

  @doc      Returns the width of the reference image.
  
  @return   The width.
  
*/
int CAAMReferenceFrame::RefImageWidth() const {

    return m_pMaskImage->Width();
}



/**

  @author   Mikkel B. Stegmann
  @version  6-7-2002

  @memo     Returns the height of the reference image.

  @doc      Returns the height of the reference image.
  
  @return   The height.
  
*/
int CAAMReferenceFrame::RefImageHeight() const {

    return m_pMaskImage->Height();
}



/**

  @author   Mikkel B. Stegmann
  @version  6-7-2002

  @memo     Find the corresponding triangle in the source mesh for a point.

  @doc      Find the corresponding triangle in the source mesh for a point. 
  
  @param    p           Input point

  @param    triangle    Triangle index.

  @param    alpha       Relative position on triangle (barycentric coordinate).

  @param    beta        Relative position on triangle (barycentric coordinate).

  @param    gamma       Relative position on triangle (barycentric coordinate).
  
  @return   True if the point is inside the source mesh.
  
*/
bool CAAMReferenceFrame::FindTriangle( const CAAMPoint &p, unsigned short &triangle, 
                                       double &alpha, double &beta, 
                                       double &gamma ) const {

    int t;
    bool ret;

    ret = m_pReferenceMesh->IsInside( p, t, alpha, beta, gamma );
    triangle = (unsigned short)t;

    return ret; 
}