/* $Id: AAMTest.cpp,v 1.6 2003/04/24 12:01:01 aam Exp $ */
#include "AAMVisualizer.h"
#include "AAMTest.h"
#include "AAMModel.h"
#include "AAMShape.h"
#include "AAMUtil.h"
#include "AAMAnalyzeSynthesize.h"
#include "AAMReferenceFrame.h"
#include "htimer.h"
#include "AAMInitialize.h"
#include "AAMModelSeq.h"
#include <direct.h>


/**

  @author   Mikkel B. Stegmann
  @version  3-5-2001

  @memo     Constructor.

  @doc      Constructor.
  
  @return   Nothing.
  
*/
CAAMTest::CAAMTest() {
	
	
}


/**

  @author   Mikkel B. Stegmann
  @version  3-5-2001

  @memo     Destructor.

  @doc      Destructor.

  @return   Nothing.
  
*/
CAAMTest::~CAAMTest() {
	
	
}


/**

  @author   Mikkel B. Stegmann
  @version  3-5-2001

  @memo     Tests CAAMShape:GetRotation() for rotations in the range [0;360].
  
  @doc      Tests CAAMShape:GetRotation() for rotations in the range [0;360].  
  
  @param    s1  First shape.

  @param    s2  Second shape.
  
  @return   Nothing.
  
*/
void CAAMTest::GetRotationTest( const CAAMShape &s1, const CAAMShape &s2 ) {    

    CAAMShape _s1, _s2, _s1_zero;

    _s1 = s1;
    _s2 = s2;

    printf("Shape 1 got %i points.\n", _s1.NPoints() );
    printf("Shape 2 got %i points.\n", _s2.NPoints() );

    double rad_angle = _s1.GetRotation( _s2 );
    printf("\nRotation between shape 1 and 2: %.2f degrees.\n", CAAMUtil::Rad2Deg( rad_angle ) );

    printf("Compensation for rotation.\n" );
    _s1.Rotate( -rad_angle );
    
    rad_angle = _s1.GetRotation( _s2 );
    printf("Rotation between shape 1 and 2: %.2f degrees.\n\n", CAAMUtil::Rad2Deg( rad_angle ) );

    _s1_zero = _s1;

    for(int i=0;i<=360;i+=5) {

        printf("Actual rot. %3i degrees. ", i );

        double angle = CAAMUtil::Deg2Rad( i );

        _s1 = _s1_zero;
        _s1.Rotate( angle );
        double rad_rot = _s1.GetRotation( _s2 );

        printf("Estimate: s1 is rotated %6.1f degrees w.r.t. s2.\n", CAAMUtil::Rad2Deg( rad_rot ) );
    }
}



/**

  @author   Mikkel B. Stegmann
  @version  6-13-2002

  @memo     Benchmarks the software warping method against the OpenGL ditto.

  @doc      Benchmarks the software warping method against the OpenGL ditto.  
  
  @param    refShape    Reference shape, i.e. mean shape scale to mean size.

  @param    s           Input shape in relative or absolute coordinates.

  @param    image       Host image of 's'.

  @param    useConvexHull If true the convex hull is used.
  
  @return   Nothing.
  
*/
void CAAMTest::AnalyzeTest( const CAAMShape &refShape,
                            const CAAMShape &s, 
                            const CDMultiBand<TAAMPixel> &image,
                            const bool useConvexHull ) {
        
    // setup
    CAAMReferenceFrame rf;
    rf.Setup( refShape, useConvexHull );      
    CDVector hard,soft;        
    CHTimer t;
    double software, hardware;
    CAAMShape shape(s);
    shape.Rel2Abs(image.Width(),image.Height());    
    
    int n=30; // number of repetions
    

    ///////////////////////////
    // hardware analyze       
    ///////////////////////////
    CAAMAnalyzeSynthesizeOpenGL as( rf );   
    
    as.ClockReadPixels(); // test readpixel

    hardware = .0;
    t.reset();
    t.start();
    for(int i=0;i<n;i++) {
        
        as.SetAnalyzeImage( image );        
    }
    t.stop();        
    hardware = t.getTime()/n;    
    printf("OpenGL SetAnalyzeImage() : %5.1f ms\n", hardware*1000. );
        
    CDMultiBand<TAAMPixel> refImage;
    int refWidth  = rf.RefImageWidth();
    int refHeight = rf.RefImageHeight();   
    refImage = CDMultiBand<TAAMPixel>( refWidth, refHeight, BANDS,
                                     evisimoptDontAlignMemory );
    hardware = .0;
    t.reset();
    t.start();
    for(i=0;i<n;i++) {
                    
        as.Analyze( shape, refImage, true );        
        rf.Image2Vector( refImage, hard );
    }    
    t.stop();
    hardware = t.getTime()/n;
    refImage.WriteBandedFile( "analyze_hard_refimage.bmp" );    
	rf.Vector2Image( hard, refImage );
	refImage.WriteBandedFile( "analyze_hard.bmp" );    
    hard.ToMatlab("hard.m","h","",false);
    printf("OpenGL Analyze()         : %5.1f ms\n", hardware*1000. );
        

    ///////////////////////////
    // software analyze
    ///////////////////////////
    CAAMAnalyzeSynthesizeSoftware ass( rf );
    ass.SetAnalyzeImage( image );
    software = .0;
    t.reset();
    t.start();
    for(i=0;i<n;i++) {
                       
        ass.Analyze( shape, soft, true );        
    }
    t.stop();
    software = t.getTime()/n;
    rf.Vector2Image( soft, refImage );
    refImage.WriteBandedFile( "analyze_soft.bmp" );    
    printf("Software Analyze()       : %5.1f ms\n", software*1000. );        
    soft.ToMatlab("soft.m","s","",false);    
    printf("Analyze ratio            : %5.1f (software/hardware)\n", software/hardware );


    ///////////////////////////
    // hardware synthesize
    ///////////////////////////
    CDMultiBand<TAAMPixel> destImage(image);    
    hardware = .0;
    t.reset();
    t.start();
    for(i=0;i<n;i++) {
                
        as.Synthesize( shape, hard, destImage, true );        
    }
    t.stop();    
    hardware = t.getTime()*1000./n;            
    printf("\nOpenGL Synthesize()      : %5.1f ms\n", hardware );
    destImage.WriteBandedFile( "synthesize_hard.bmp" );

    
    ///////////////////////////
    // software synthesize
    ///////////////////////////
    software = .0;
    t.reset();
     t.start();
    for(i=0;i<n;i++) {        
        ass.Synthesize( shape, soft, destImage, true );                   
    }
    t.stop();    
    software += t.getTime()*1000./n;    
    printf("Software Synthesize()    : %5.1f ms\n", software );
    destImage.WriteBandedFile( "synthesize_soft.bmp" );           
    printf("Synthesize ratio         : %5.1f (software/hardware)\n", software/hardware );


	///////////////////////////
    // image2vector
    ///////////////////////////    
    double img2vec = .0;
    t.reset();
    t.start();
    for(i=0;i<n;i++) {                           

        rf.Image2Vector( refImage, hard );
    }    
    t.stop();
    img2vec = t.getTime()/n;    
    printf("\nImage2Vector()           : %5.2f ms\n", img2vec*1000. );


	///////////////////////////
    // vector2image
    ///////////////////////////    
    double vec2img = .0;
    t.reset();
    t.start();
    for(i=0;i<n;i++) {                           

        rf.Vector2Image( hard, refImage );
    }    
    t.stop();
    vec2img = t.getTime()/n;    
    printf("Vector2Image()           : %5.2f ms\n", vec2img*1000. );

	// done
    printf("\n# texture samples        : %i\n", soft.Length() );
}



/**

  @author   Mikkel B. Stegmann
  @version  3-27-2000

  @memo     Tests the prediction matrices ability to predict pose
			displacements.

  @doc		Output are returned in the form of eight matlab formatted files
			in the current directory.
            
  @param    model       The AAM to test.   
  
  @param    path		The path where test images and annotations are placed
						(including terminating backslash).  
  
  @return   Nothing.
  
*/
void CAAMTest::TestPosePrediction( const CAAMModel &model, const CString &path ) {
	
	std::vector<CString> vFilenames;
	
	// read files
	vFilenames = CAAMUtil::ScanSortDir( path, "asf" );
	int nShapes = vFilenames.size();

    // setup test ranges
    int n = 11;        
    double rel_displace = .2;    
    double degrees = 15.;
    double scale = .15;
    int nIter = 5;

    CDVector dx,dy,ds,dtheta, dxy_rel;
    double refWidth  = model.ReferenceShape().Width();
    double refHeight = model.ReferenceShape().Height();
    double w = rel_displace*refWidth;
    double h = rel_displace*refHeight;       
    double w10 = refHeight/10.;
    double h10 = refHeight/10.;
    double r10 = 10.*M_PI/180.;
    double avgXerr = .0, avgYerr = .0, avgRotErr = .0, avgScaleErr = .0;
    int XerrCount = 0, YerrCount = 0, RotErrCount = 0, ScaleErrCount = 0;
    dxy_rel.Linspace( -rel_displace, rel_displace, n );
    dx.Linspace( -w, w, n );
    dy.Linspace( -h, h, n );
    ds.Linspace( 1-scale, 1+scale, n );
    dtheta.Linspace( -degrees*M_PI/180., degrees*M_PI/180., n );
	
	CDMatrix vDx( n, nShapes ), vDy( n, nShapes );
    CDMatrix vDs( n, nShapes ), vDtheta( n, nShapes );
	for(int i=0;i<nShapes;i++) {

		CDVector c, g_m, g_s, g_delta, pose(4);	
		CDMultiBand<TAAMPixel> image;
		double s,t,t_x,t_y;
		CAAMShape shape;
		
		// read the image and shape
		CAAMUtil::ReadExample( vFilenames[i], image, shape, model.ModelReduction() );		

		// debug
		printf("Testing '%s' [%i/%i]\n", vFilenames[i], i+1, nShapes ); 		
	
		// make x-translation tests		
		for(int j=0;j<dx.Length();j++) {

            // make curve
			CAAMShape shapeCopy( shape );
			shapeCopy.Translate( dx[j], 0 );
			model.EstimatePose( image, shapeCopy, pose );			
			CAAMShape::PoseVec2Param( pose, s, t, t_x, t_y );
			vDx[j][i] = t_x;
            if (fabs(dx[j])<w10) { avgXerr += fabs(dx[j]-t_x); ++XerrCount; }
		}
		
		// make y-translation tests		
		for(j=0;j<dy.Length();j++) {

			CAAMShape shapeCopy( shape );
			shapeCopy.Translate( 0, dy[j] );
			model.EstimatePose( image, shapeCopy, pose );			
			CAAMShape::PoseVec2Param( pose, s, t, t_x, t_y );
			vDy[j][i] = t_y;
            if (fabs(dy[j])<h10) { avgYerr += fabs(dy[j]-t_y); ++YerrCount; }
		}

		// make scale tests		
		for(j=0;j<ds.Length();j++) {
			
			CAAMShape shapeCopy( shape );			
			shapeCopy.Scale( ds[j], true );			
			model.EstimatePose( image, shapeCopy, pose );			
			CAAMShape::PoseVec2Param( pose, s, t, t_x, t_y );
			vDs[j][i] = s;			
            if (fabs(1-ds[j])<.1) { avgScaleErr += fabs(ds[j]-s); ++ScaleErrCount; }
		}

		// make rotation tests	
		for(j=0;j<dtheta.Length();j++) {
			
			CAAMShape shapeCopy( shape );			
			shapeCopy.Rotate( dtheta[j], true );			
			model.EstimatePose( image, shapeCopy, pose );			
			CAAMShape::PoseVec2Param( pose, s, t, t_x, t_y );
			vDtheta[j][i] = t;
            if (fabs(dtheta[j])<r10) { avgRotErr += fabs(dtheta[j]-t); ++RotErrCount; }
		}
	}
    printf("Avg X error (within +/- 10%% width)     : %.2f pixels (%.1f %%)\n",
            avgXerr/XerrCount, 100.*(avgXerr/XerrCount) / refWidth );
    printf("Avg Y error (within +/- 10%% height)    : %.2f pixels (%.1f %%)\n",
            avgYerr/YerrCount, 100.*(avgYerr/YerrCount) / refHeight );
    printf("Avg rot. error (within +/- 10 degrees) : %.2f degrees\n",
            avgRotErr/RotErrCount * 180./M_PI );
    printf("Avg scale error (within +/- 10%%)       : %.2f %%\n",
            100.*avgScaleErr/ScaleErrCount );

    // write displacements to disk in matlab format	
    dx.ToMatlab( "fdx.m", "dx", "", false );	
	dy.ToMatlab( "fdy.m", "dy", "", false );	
	ds.ToMatlab( "fds.m", "ds", "", false );	
    dxy_rel.ToMatlab( "fdxy_rel.m", "dxy_rel", "", false );	
	dtheta.ToMatlab( "fdtheta.m", "dtheta", "", false );
    
	// write results to disk in matlab format	
	vDx.ToMatlab( "fdrx.m", "drx", "", false );	
	vDy.ToMatlab( "fdry.m", "dry", "", false );	
	vDs.ToMatlab( "fdrs.m", "drs", "", false );	    
	vDtheta.ToMatlab( "fdrtheta.m", "drtheta", "", false );


    /*

    // test convergence
    shapeCopy = shape;
    shapeCopy.Translate( dx[j], 0 );
    for(int iter=0;iter<nIter;iter++) {

        model.EstimatePose( image, shapeCopy, pose );
        CAAMShape::PoseVec2Param( pose, s, t, t_x, t_y );
        shapeCopy.Translate( -t_x, 0 );
        printf("t_x = %.2f\n", t_x);
    }
    double x, y; shape.COG( x, y );
    printf("cog = %.2f, %.2f\n", x, y );
    shapeCopy.COG( x, y );
    printf("cog = %.2f, %.2f\n", x, y );
    printf("\n");

    */
}



/**

  @author   Mikkel B. Stegmann
  @version  4-5-2000

  @memo     Optimizes a set of images and compares the result to a ground truth
			annotation.

  @doc      Optimizes a set of images and compares the result to a ground truth
			annotation. As initialization the ground truth pose is systematically 
			displaced (default) or an automatic initialisation is performed.

  @param	model				The model to evaluate.
  
  @param    gt_path				Path to ground truth images and annotations.  

  @param    result_file			The file to write the results in.

  @param	writeStills			If true two model border images are written; one of 
								the initialisation and one of the optimization.

  @param	writeMovies			It true a movie of the whole optimization is written, 
								one frame per iteration.

  @param	autoinit			If true automatic initialization is performed instead
								of the systematic displacement of the ground truth
								pose. 

  @param	dump2screen			If true, results are written to the screen also (default true).

  @param	pLB					Optional pointer to a CAAMLowerBounds object.
								
  @return   Evaluation results.
  
*/ 
CAAMEvaluationResults CAAMTest::EvaluateModel(	const CAAMModel *pModel, 
												const CString &gt_path, 								
  												const CString &result_file,
		    									const bool writeStills,
			    								const bool writeMovies,
				    							const bool autoinit,
												const bool dump2screen, 
												CAAMLowerBounds *pLB ) {
    
	std::vector<CString> vFilenames;
	CString resDir("__evaluation__"), resFile, resPath;	
    CAAMVisualizer AAMvis(pModel);
    CDMultiBand<TAAMPixel> image;
    CAAMShape groundtruth;
	CAAMModel::CAAMOptRes res;		
    std::vector<CAAMModel::CAAMOptState> optStates;
    CHTimer timer;
	CAAMEvaluationResults evalRes;
	CAAMLowerBounds lbs( *pModel );

	fprintf( stdout, "Evaluating AAM...\n" ); 

	// start total timer
    timer.start();

    // make output directory
    resPath = gt_path+resDir+"\\";
    _mkdir( resPath );

    // test results file
	resFile = resPath+result_file;
	if ( false==CAAMUtil::CreateTest( resFile ) ) {
	
		printf("Result file '%s' could not be opened.\n", resFile );
        return evalRes;		
	}
    

    // find asf files
	vFilenames = CAAMUtil::ScanSortDir( gt_path, "asf" );
	int nImages = vFilenames.size();	
	
	// setup
	int nExperiments = autoinit ? 1 : 4;    // use 4 for xy only and 8 for xy, s, theta
	int totalexp = nExperiments*nImages;	
    double timeSum = .0;
    bool fine_tuning = false;
	bool writeError = false;

    // evaluate
	for(int i=0;i<nImages;i++) {
	
		// read the image and shape
        CDMultiBand<TAAMPixel> image;
		CAAMUtil::ReadExample( vFilenames[i], image, groundtruth, pModel->ModelReduction() );						
		CString shapeName = CAAMUtil::GetFilename( CAAMUtil::RemoveExt( vFilenames[i] ));

		// add shape extents (if requested)
        if (pModel->AddExtents()!=0.0) { groundtruth.AddShapeExtends( (int)(.5+pModel->AddExtents()) ); }

		// do displacements and optimizations		
		for(int exp=0;exp<nExperiments;exp++) {
			
            // use mean texture and mean shape size to mean size
			CDVector c = 0;
            CAAMShape shapeCopy = pModel->ReferenceShape();			
			if (autoinit) {

                // perform automatic initialization				
                CAAMInitializeStegmann Stegmann( *pModel );
                Stegmann.Initialize( image, shapeCopy, c );				
            } else {

                // do displacements	
			    double dtheta = 0.0, scale = 1.0, dx = 0, dy = 0;			    
                double dp = .1;
			    switch(exp) {
                
				    case 0:		dx = -dp*groundtruth.Width();	break;                
				    case 1:		dx =  dp*groundtruth.Width();	break;
				    case 2:		dy = -dp*groundtruth.Height();	break;
				    case 3:		dy =  dp*groundtruth.Height();	break;
				    case 4:		dtheta = -5*M_PI/180.0;			break;
				    case 5:		dtheta =  5*M_PI/180.0;			break;
				    case 6:		scale = 0.95;					break;
				    case 7:		scale = 1.05;					break;
				    default:	printf("No such experiment.\n" ); exit(-1); break;
			    }					    
			    shapeCopy.AlignTo( groundtruth );
			    shapeCopy.Translate( dx, dy );
			    shapeCopy.Rotate( dtheta, true );	// around cog
			    shapeCopy.Scale( scale, true );		// around cog
            }
			
			// write initial model points
			if (writeStills) {

                CString fn; fn.Format("%s_exp%02i_init.bmp", shapeName, exp );
                AAMvis.ShapeStill( image, shapeCopy, resPath+fn ); 			
			}
					
			// do the optimization	
			CHTimer t;
            t.start();
			res = pModel->OptimizeModel( image, shapeCopy, c, 30, &optStates );            			

            //       
			// usage:
			//
			// _putenv( "FINE_TUNING=1" );
			//
			// default 0
			//
#ifdef USEAAMENV
			CString fineTuningStr = getenv( "FINE_TUNING" );
			bool fineTuning = strcmp(fineTuningStr,"1")==0;	
#else
            bool fineTuning = false;
#endif
			if (fineTuning) {
                printf("*** OptimizeModelByFineTuning() ...");
                res = pModel->OptimizeModelByFineTuning( image, shapeCopy, c, 1000, 0, 4 );
                printf(" Done!\n");
            }
			t.stop();
			double ms = 1000.0*t.getTime();


			// add result to list
			evalRes.AddResult( shapeCopy, groundtruth, ms, res );
			lbs.AddGroundtruth( groundtruth, image );
            

			// write optimized model points
			if (writeStills) {

                CString fn; fn.Format("%s_exp%02i_opt.bmp", shapeName, exp );
                AAMvis.ShapeStill( image, shapeCopy, resPath+fn ); 
			}

            // write movie
            if (writeMovies) {
                
                CString mn; mn.Format("%s_exp%02i.avi", shapeName, exp );
                AAMvis.OptimizationMovie( optStates, image, resPath+mn );
            }

            // write error
			if (writeError) {

				CString fn; fn.Format("%s_exp%02i_err.txt", shapeName, exp );
				FILE *fhE = fopen( resPath+fn, "wt" );
				for(unsigned int j=0;j<optStates.size();j++) {

					fprintf( fhE, "%e\t%i\n", optStates[j].error, optStates[j].damps );
				}
				fclose(fhE);
			}

			// write model parameters
			bool writeParameters = true;			
			if (writeParameters) {

				CString fn; 
				fn.Format("%s_exp%02i_c.m", shapeName, exp );
				c.ToMatlab( resPath+fn, "c", "Recovered combined model parameters", false );

				CDVector b_g, b_s;
				pModel->Combined2ShapeParam( c, b_s );
				pModel->Combined2TexParam( c, b_g );

				fn.Format("%s_exp%02i_b_s.m", shapeName, exp );
				b_s.ToMatlab( resPath+fn, "b_s", "Recovered shape model parameters", false );

				fn.Format("%s_exp%02i_b_g.m", shapeName, exp );
				b_g.ToMatlab( resPath+fn, "b_g", "Recovered texture model parameters", false );
			}
	
            // write the shape result            
            CString resASF;
            resASF.Format( "%s_exp%02i.asf", CAAMUtil::GetFilename( CAAMUtil::RemoveExt( vFilenames[i] )), exp );            
            CAAMShape s(shapeCopy);            
            s.SetHostImage( groundtruth.HostImage() );
            s.Scale( pModel->ModelReduction() );            
            s.WriteASF( resPath+resASF, image.Width(), image.Height() );
		}
	}

	// print statistics to screen and file
	if (dump2screen) evalRes.PrintStatistics();
	evalRes.PrintStatistics( resFile );

	if (dump2screen) lbs.PrintStatistics();
	lbs.PrintStatistics( CAAMUtil::GetPath(resFile)+"lower_bounds.txt" );
	if (pLB) *pLB = lbs;

	// print info
	timer.stop();
	fprintf( stdout, "Results are written in '%s'\n", resFile ); 
	printf("Model evaluation took: %.2f secs.\n", timer.getTime() );

	
	// return the evaluation results
    return evalRes;
}



/**

  @author   Mikkel B. Stegmann
  @version  4-5-2000

  @memo     Optimizes a set of images and compares the result to a ground truth
			annotation using a sequence of AAMs.
  @doc      Optimizes a set of images and compares the result to a ground truth
			annotation using a sequence of AAMs. As initialization the ground 
			truth pose is systematically displaced (default) or an automatic 
			initialisation is performed.

  @param	modelSew			A sequence of models.
  
  @param    gt_path				Path to ground truth images and annotations.  

  @param    result_file			The file to write the results in.

  @param	writeStills			If true two model border images are written; one of 
								the initialisation and one of the optimization.

  @param	writeMovies			It true a movie of the whole optimization is written, 
								one frame per iteration.

  @param	autoinit			If true automatic initialization is performed instead
								of the systematic displacement of the ground truth
								pose. 

  @param	dump2screen			If true, results are written to the screen also (default true).

  @param	pLBS				Optional pointer to a CAAMLBShapeModel object.
								
  @return   Evaluation results.
  
*/ 
CAAMEvaluationResults CAAMTest::EvaluateModelSeq(	const CAAMModelSeq &modelSeq, 
													const CString &gt_path,
  													const CString &result_file,
		    										const bool writeStills,
			    									const bool writeMovies,
				    								const bool autoinit,
													const bool dump2screen, 
													CAAMLowerBounds *pLB ) {
    
	
	
	
	// LATER: Hver model burde visualiseres rigtigt (egentligt ikke noget problem)




	std::vector<CString> vFilenames;
	CString resDir("__evaluation__"), resFile, resPath;	
    CAAMVisualizer AAMvis( &modelSeq.FinalModel() );
    CAAMShape groundtruth;
	CAAMModel::CAAMOptRes res;		
    std::vector<CAAMModel::CAAMOptState> optStates;
    CHTimer timer;
	CAAMEvaluationResults evalRes;
	CAAMLowerBounds lbs( modelSeq.FinalModel() );
	CDVector initTime;

	fprintf( stdout, "Evaluating AAM...\n" ); 

	// start total timer
    timer.start();

    // make output directory
    resPath = gt_path+resDir+"\\";
    _mkdir( resPath );

    // test results file
	resFile = resPath+result_file;
	if ( false==CAAMUtil::CreateTest( resFile ) ) {
	
		printf("Result file '%s' could not be opened.\n", resFile );
        return evalRes;		
	}

    // find asf files
	vFilenames = CAAMUtil::ScanSortDir( gt_path, "asf" );
	int nImages = vFilenames.size();	
		// setup
	int nExperiments = autoinit ? 1 : 4;    // use 4 for xy only and 8 for xy, s, theta
	int totalexp = nExperiments*nImages;	
    double ms, timeSum = .0;    
	bool writeError = false;
	int ninit = 0;

	if (autoinit) {

		initTime.Resize(totalexp);
	}

    // for each image
	for(int i=0;i<nImages;i++) {
	
		// read the image and shape
		CDMultiBand<TAAMPixel> imageOrg, imageScaled;
		CAAMUtil::ReadExample( vFilenames[i], imageOrg, groundtruth, 1 );
		CString shapeName = CAAMUtil::GetFilename( CAAMUtil::RemoveExt( vFilenames[i] ));

		// add shape extents (if requested)
		if (modelSeq.FinalModel().AddExtents()!=0.0) { printf("Shape extents not supported."); exit(-1); }

		// scale image (if requested) LATER: this should be improved on later
		imageScaled = imageOrg;
		if ( modelSeq.Model(0).ModelReduction()>1 ) {
			
			imageScaled.ReducePyr( modelSeq.Model(0).ModelReduction() );
		}

		// do displacements and optimizations		
		for(int exp=0;exp<nExperiments;exp++) {

			// use mean texture and mean shape sized to mean size
			CDVector c( modelSeq.Model(0).CombinedPCA().NParameters() );
			c = .0;
			CAAMShape shapeCopy = modelSeq.Model(0).ReferenceShape();			
			if (autoinit) {

				// perform automatic initialization				
				CHTimer init;				
				init.start();

				CAAMInitializeStegmann Stegmann( modelSeq.Model(0) );
				Stegmann.Initialize( imageScaled, shapeCopy, c );

				init.stop();
				initTime[ninit++] = init.getTime();

			} else {

				// scale ground truth to this level
				CAAMShape gt_scaled(groundtruth);
				gt_scaled.Scale( 1./modelSeq.Model(0).ModelReduction() );

				// do displacements	
				double dtheta = 0.0, scale = 1.0, dx = 0, dy = 0;			    
				double dp = .1;
				switch(exp) {
        
					case 0:		dx = -dp*gt_scaled.Width();		break;                
					case 1:		dx =  dp*gt_scaled.Width();		break;
					case 2:		dy = -dp*gt_scaled.Height();	break;
					case 3:		dy =  dp*gt_scaled.Height();	break;
					case 4:		dtheta = -5*M_PI/180.0;			break;
					case 5:		dtheta =  5*M_PI/180.0;			break;
					case 6:		scale = 0.95;					break;
					case 7:		scale = 1.05;					break;
					default:	printf("No such experiment.\n" ); exit(-1); break;
				}
	
				shapeCopy.AlignTo( gt_scaled );
				shapeCopy.Translate( dx, dy );
				shapeCopy.Rotate( dtheta, true );	// around cog
				shapeCopy.Scale( scale, true );		// around cog
			}

			// scale shape to final image coordinates
			modelSeq.ScaleShape2Final( 0, shapeCopy );
			
			// write initial model points
			if (writeStills) {

				CString fn; fn.Format("%s_exp%02i_init.bmp", shapeName, exp );							
				AAMvis.ShapeStill( imageOrg, shapeCopy, resPath+fn );
			}

			
			
			CHTimer t;
			t.start();

			// for each model
			for(int m = 0;m<modelSeq.NModels();m++) {

				// scale image 
				if ( m!=0 ) {

					t.stop();
					imageScaled = imageOrg;
					if ( modelSeq.Model(m).ModelReduction()>1 ) {

						imageScaled.ReducePyr( modelSeq.Model(m).ModelReduction() );
					}
					t.start();
				}

				// scale shape to current image coordinates
				modelSeq.ScaleShape2Model( m, shapeCopy );

				// set initial model parameters
				if (m>0) {

					// sample texture under shape and project into the current model
					// (this sample should really be used to start the OptimizeModel 
					//  below, otherwise we're doing the same work twice.... )			
					modelSeq.Model(m).Shape2Combined( shapeCopy, imageScaled, c );
				} 
			
				// do the optimization	
				res = modelSeq.Model(m).OptimizeModel( imageScaled, shapeCopy, c, 30, &optStates );


				//       
				// usage:
				//
				// _putenv( "FINE_TUNING=1" );
				//
				// default 0
				//
#ifdef USEAAMENV
				CString fineTuningStr = getenv( "FINE_TUNING" );
				bool fineTuning = strcmp(fineTuningStr,"1")==0;	
#else
                bool fineTuning = false;
#endif
				if (fineTuning) {
				
					printf("*** OptimizeModelByFineTuning() ...");
					res = modelSeq.Model(m).OptimizeModelByFineTuning( imageScaled, shapeCopy, c, 1000, 0, 3 );
					printf(" Done!\n");
				}
								
				// scale shape result to final image coordinates
				modelSeq.ScaleShape2Final( m, shapeCopy );
				
				// write optimized model points
				t.stop();
				if (writeStills) {

					CString fn; fn.Format("%s_exp%02i_opt_m%i.bmp", shapeName, exp, m );
					AAMvis.ShapeStill( imageOrg, shapeCopy, resPath+fn ); 
				}
				t.start();
			}

			// correct ground truth scale 
			groundtruth.Scale( 1./modelSeq.FinalModel().ModelReduction() );

			t.stop();
			ms = 1000.0*t.getTime();

			// add result to list
			evalRes.AddResult( shapeCopy, groundtruth, ms, res );
			lbs.AddGroundtruth( groundtruth, imageOrg );
    

			// write movie
			if (writeMovies) {
        
				CString mn; mn.Format("%s_exp%02i.avi", shapeName, exp );
				AAMvis.OptimizationMovie( optStates, imageOrg, resPath+mn );
			}

			// write error
			if (writeError) {

				CString fn; fn.Format("%s_exp%02i_err.txt", shapeName, exp );
				FILE *fhE = fopen( resPath+fn, "wt" );
				for(unsigned int j=0;j<optStates.size();j++) {

					fprintf( fhE, "%e\t%i\n", optStates[j].error, optStates[j].damps );
				}
				fclose(fhE);
			}

			// write model parameters
			bool writeParameters = true;
			if (writeParameters) {

				CString fn; 
				fn.Format("%s_exp%02i_c.m", shapeName, exp );
				c.ToMatlab( resPath+fn, "c", "Recovered combined model parameters", false );

				CDVector b_g, b_s;
				modelSeq.FinalModel().Combined2ShapeParam( c, b_s );
				modelSeq.FinalModel().Combined2TexParam( c, b_g );

				fn.Format("%s_exp%02i_b_s.m", shapeName, exp );
				b_s.ToMatlab( resPath+fn, "b_s", "Recovered shape model parameters", false );

				fn.Format("%s_exp%02i_b_g.m", shapeName, exp );
				b_g.ToMatlab( resPath+fn, "b_g", "Recovered texture model parameters", false );
			}

			// write the shape result            
			CString resASF;
			resASF.Format( "%s_exp%02i.asf", shapeName, exp );            
			CAAMShape s(shapeCopy);            
			s.SetHostImage( groundtruth.HostImage() );		
			s.WriteASF( resPath+resASF, imageOrg.Width(), imageOrg.Height() );
		
		} // for each experiment		
	} // for each image

	// print statistics to screen and file
	if (dump2screen) evalRes.PrintStatistics();

	FILE *fh = fopen( resFile, "wt" );
	evalRes.PrintStatistics( fh );
	if (autoinit) {

		fprintf( fh, "Initialisation time: %.2f (%.2f)  %.2f %.2f %.2f\n", 
					 initTime.Mean(), initTime.Std()/sqrt(initTime.Length()), 
					 initTime.Median(), initTime.Min(), initTime.Max() );
	}
	fclose(fh);

	if (dump2screen) lbs.PrintStatistics();
	lbs.PrintStatistics( CAAMUtil::GetPath(resFile)+"lower_bounds.txt" );
	if (pLB) *pLB = lbs;

	// print info
	timer.stop();
	fprintf( stdout, "Results are written in '%s'\n", resFile ); 
	printf("Model evaluation took: %.2f secs.\n", timer.getTime() );
	
	// return the evaluation results
    return evalRes;
}
