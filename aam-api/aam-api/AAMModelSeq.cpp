#include "AAMModelSeq.h"
#include "AAMBuilder.h"
#include "AAMInitialize.h"
#include "AAMVisualizer.h"


/**

  @author   Mikkel B. Stegmann
  @version  2-20-2003

  @memo     Default multi-scale constructor.

  @doc      Default multi-scale constructor.
  
  @return   Nothing.
  
*/
CAAMModelSeq::CAAMModelSeq() {

}



/**

  @author   Mikkel B. Stegmann
  @version  2-20-2003

  @memo     Destructor.

  @doc      Destructor.
  
  @return   Nothing.
  
*/
CAAMModelSeq::~CAAMModelSeq() {

}



/**

  @author   Mikkel B. Stegmann
  @version  2-20-2003

  @memo     Diver method for model generation.

  @doc      This method automates the model generation as much as possible
			by using the various class methods for all the sequences in the
			task of producing a model.           

  @param    SACF			Filename of a sequence ACF, which is a file 
  							containing a list of acfs, one per line. E.g.
  							
  							<BOF>
							scale4_convex_hull.acf
							scale4_whiskers.acf
							scale4.acf
							scale2.acf
							scale1.acf
  							<EOF>
  
  @param    inDir			Input directory where annotations (.asf) resides.

  @param    excludeShape    Excludes one shape number 'excludeShape'
                            from the input directory. Default -1, i.e.
                            no shapes are removed.

                            Used to perform leave-one-out testing.
  
  @return   Nothing.
  
*/
void CAAMModelSeq::BuildFromSACF(  const CString &SACF,
                                   const CString &inDir,                                                                      
                                   const int excludeShape ) {  
    
    std::vector<CString> asfFiles;    
    asfFiles = CAAMUtil::ScanSortDir( CAAMUtil::AddBackSlash( inDir ), "asf" );
    this->BuildFromSACF( SACF, asfFiles, excludeShape );
}



/**

  @author   Mikkel B. Stegmann
  @version  2-20-2003

  @memo     Diver method for model generation.

  @doc      This method automates the model generation as much as possible
			by using the various class methods for all the sequences in the
			task of producing a model.           

  @param    SACF			Filename of a sequence ACF, which is a file 
  							containing a list of acfs, one per line. E.g.
  							
  							<BOF>
							scale4_convex_hull.acf
							scale4_whiskers.acf
							scale4.acf
							scale2.acf
							scale1.acf
  							<EOF>

  @param    asfFiles        Vector of asf filenames.

  @param    excludeShape    Excludes one shape number 'excludeShape'
                            from the input directory. Default -1, i.e.
                            no shapes are removed.

                            Used to perform leave-one-out testing.  
                            
  @return   Nothing.
  
*/
void CAAMModelSeq::BuildFromSACF(	const CString &SACF,
									const std::vector<CString> &asfFiles,									
									const int excludeShape ) {

    // delete any old models
    m_vModels.clear();

	// read acf filenames
    m_vACF = CAAMUtil::ReadLines( SACF );

    for(int i=0;i<m_vACF.size();i++) {

        printf("\n*** Building model %i/%i in of a sequence AAM ***\n", 
				i+1, m_vACF.size() );

        // build model and add to the sequence
        CAAMBuilder builder;
        CAAMModel model;
		const int reductionMultiplier = 1;
		
        builder.BuildFromFiles( model, asfFiles, m_vACF[i], reductionMultiplier, excludeShape );

		m_vModels.push_back( model );
    }
}



/**

  @author   Mikkel B. Stegmann
  @version  2-20-2003

  @memo     Writes the sequence AAM to disk as a set of .txt and .amf files.

  @doc      Writes the sequence AAM to disk as a set of .txt and .amf files.
  			Filenames are determined from the ACF file names.

  @see      ReadModel
  
  @param	filename 	Filename of the output .samf-file.

  @param    txt_only    If true, binary model data is not written.
  
  @return   true on success, false on file errors.

*/
bool CAAMModelSeq::WriteModels( const CString &filename, const bool txt_only ) const {
    
	bool res = true;
	
	FILE *fh = fopen( CAAMUtil::ForceExt( filename, "samf" ), "wt" );
    for(unsigned int i=0;i<NModels() && res;i++) {

		fprintf( fh, "%s\n", CAAMUtil::RemoveExt( CAAMUtil::GetFilename( m_vACF[i] ) ) );
        CString modelname = CAAMUtil::GetPath(filename) + CAAMUtil::RemoveExt( m_vACF[i] );
		res = Model(i).CAAMModel::WriteModel( modelname, txt_only );
    }
    fclose( fh );

	return res;
}



/**

  @author   Mikkel B. Stegmann
  @version  2-20-2003

  @memo     Reads the complete AAMModel from disk.

  @doc      Reads the complete AAMModel from disk.

  @see		WriteModel 
  
  @param    filename	Input filename (.samf).
  
  @return   true on success, false on file errors.
  
*/
bool CAAMModelSeq::ReadModels( const CString &samf ) {

    // delete any old models
    m_vModels.clear();

	const std::vector<CString> vAMF = CAAMUtil::ReadLines( samf );
    
    bool result = true;       
    for(int i=0;i<vAMF.size();i++) {
             
        CAAMModel model;
        printf("Reading model '%s'...\n", vAMF[i] );
        result = model.ReadModel( vAMF[i] );
        
        if (result) {

			m_vModels.push_back( model );		
        }        
    }
	printf("\nSequence AAM containing %i models succesfully read.\n", vAMF.size() );

	return i>0;
}


/**

  @author   Mikkel B. Stegmann
  @version  2-20-2003

  @memo     Scale a shape defined in 'FinalModel' coordinates 
			to 'model' coordinates.

  @doc      Scale a shape defined in 'FinalModel' coordinates 
			to 'model' coordinates .

  @see      ScaleShape2Final
  
  @param    model   The model the input shape are scaled to.

  @param    shape   Input shape, which going to be scaled.
  
  @return   
  
*/
void CAAMModelSeq::ScaleShape2Model( const int model, CAAMShape &shape ) const {


	double ratio = double(this->Model(model).ModelReduction()) /
				          this->FinalModel().ModelReduction();

	shape.Scale( 1./ratio );
}



/**

  @author   Mikkel B. Stegmann
  @version  2-20-2003

  @memo     Scale a shape defined in 'model' coordinates 
			to 'FinalModel' coordinates.

  @doc      Scale a shape defined in 'model' coordinates 
			to 'FinalModel' coordinates.

  @see      ScaleShape2Model
  
  @param    model   The model the input shape are scaled from.

  @param    shape   Input shape, which going to be scaled.
  
  @return   Nothing.
  
*/
void CAAMModelSeq::ScaleShape2Final( const int model, CAAMShape &shape ) const {


	double ratio = double(this->Model(model).ModelReduction()) /
				          this->FinalModel().ModelReduction();

	shape.Scale( ratio );
}