/* $Id: AAMObject.cpp,v 1.2 2003/01/14 17:28:51 aam Exp $ */
#include "AAMObject.h"
#include "VisCore.h"



static int __AAM_TOTAL_OBJECT_COUNT__ = 0;

/**

  @author   Mikkel B. Stegmann
  @version  10-19-2000

  @memo     Constructor.

  @doc      Constructor.
  
  @return   Nothing.
  
*/
CAAMObject::CAAMObject() {
	
    ++__AAM_TOTAL_OBJECT_COUNT__;
	// printf("**CAAMObject created. Total CAAMobjects = %i.\n", __AAM_TOTAL_OBJECT_COUNT__);
}


/**

  @author   Mikkel B. Stegmann
  @version  10-19-2000

  @memo     Destructor.

  @doc      Destructor.
  
  @return   Nothing.
  
*/
CAAMObject::~CAAMObject() {

    --__AAM_TOTAL_OBJECT_COUNT__;
    // printf("**CAAMObject destroyed. Total CAAMobjects = %i.\n", __AAM_TOTAL_OBJECT_COUNT__ );
}



/**

  @author   Mikkel B. Stegmann
  @version  10-18-2000

  @memo     Write the object to a binary file.

  @doc      Write the object to a binary file. This is 
            a very old-school load/save function. 
            Maybe we should look into serialization some day.

  @see      FromFile
  
  @param    szFilename	Destination filename.
    
  @return   Nothing.

  @throws   CVisError
  
*/
void CAAMObject::ToFile( const char *szFilename ) const {

    FILE *fh;					
    
    // open file
    fh=fopen(szFilename,"wb");	
    if(fh==NULL)
    {
        throw CVisError( "Can not open file.",
            eviserrorOpen, "ToFile", __FILE__, __LINE__ );
    }
    
    // write data
    ToFile( fh );
    
    // close file
    int ret = fclose( fh );											
    if ( ret!=0 ) {
        
        throw CVisError( "Can not close file.",
            eviserrorUnknown, "ToFile", __FILE__, __LINE__ );
    }
}


/**

  @author   Mikkel B. Stegmann
  @version  10-18-2000

  @memo     Write the object to a binary file.

  @doc      Write the object to a binary file. This is 
            a very old-school load/save function. 
            Maybe we should look into serialization some day.

  @see      FromFile
  
  @param    fh  Handle to an open file.
    
  @return   Nothing.

  @throws   CVisError
  
*/
void CAAMObject::ToFile( FILE *fh ) const {

    throw CVisError( "Not implemented.",
                      eviserrorUnknown, "ToFile", __FILE__, __LINE__ );
}


/**

  @author   Mikkel B. Stegmann
  @version  10-18-2000

  @memo     Reads an object from a binary file.

  @doc      Reads the object to a binary file. This is 
            a very old-school load/save function. 
            Maybe we should look into serialization some day.

  @see      ToFile
  
  @param    szFilename	Destination filename.
    
  @return   Nothing.

  @throws   CVisError
  
*/
void CAAMObject::FromFile( const char *szFilename ) {

    FILE *fh;					

    // open file
	fh=fopen(szFilename,"wb");	
	if(fh==NULL)
	{
		throw CVisError( "Can not open file.",
                         eviserrorOpen, "FromFile", __FILE__, __LINE__ );
	}

    // read data
	FromFile( fh );

    // close file
    int ret = fclose( fh );											
    if ( ret!=0 ) {

        throw CVisError( "Can not close file.",
                         eviserrorUnknown, "FromFile", __FILE__, __LINE__ );
    }
}


/**

  @author   Mikkel B. Stegmann
  @version  10-18-2000

  @memo     Reads an object from a binary file.

  @doc      Reads the object to a binary file. This is 
            a very old-school load/save function. 
            Maybe we should look into serialization some day.

  @see      ToFile
  
  @param    fh  Handle to an open file.
    
  @return   Nothing.

  @throws   CVisError
  
*/
void CAAMObject::FromFile( FILE *fh ) {

    throw CVisError( "Not implemented.",
                      eviserrorUnknown, "FromFile", __FILE__, __LINE__ );
}


/**

  @author   Mikkel B. Stegmann
  @version  10-18-2000

  @memo     Writes object specific data to text file(s).

  @doc      Debug method that provides a human readable
            file dump of the object data.

  @param    szPath  Path including terminating backslash   
                    where the data is dumped.
  
  @return   Nothing.
  
  @throws   CVisError
  
*/
void CAAMObject::Dump( const char *szPath ) const {

    throw CVisError( "Not implemented.",
                      eviserrorUnknown, "Dump", __FILE__, __LINE__ );

}