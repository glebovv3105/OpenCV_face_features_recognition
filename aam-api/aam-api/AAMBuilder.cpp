/* $Id: AAMBuilder.cpp,v 1.5 2003/04/24 12:01:01 aam Exp $ */
#include "AAMObject.h"
#include "AAMBuilder.h"
#include "AAMLinearReg.h"
#include "AAMVisualizer.h"
#include "AAMDelaunay.h"
#include "AAMDelaunay.h"
#include "AAMTransferFunctions.h"
#include "AAMMathUtil.h"
#include "AAMVisualizer.h"
#include "AAMTest.h"

#include "AAMReferenceFrame.h"
#include "AAMAnalyzeSynthesize.h"
#include "htimer.h"


/**

  @author   Mikkel B. Stegmann
  @version  4-14-2000

  @memo     Diver method for model generation.

  @doc      This method automates the model generation as much as possible
			by using the various class methods for all the sequences in the
			task of producing a model.           

  @param    model           The generated model.

  @param    inDir			Input directory where annotations (.asf) resides.

  @param    acf				Filename of an AAM configuration file. 
							If omitted defaults are used.

  @param    modelReduction  Model reduction multiplier. Default off == 1.                            
                            Useful when building multi-scale AAMs.

  @param    excludeShape    Excludes one shape number 'excludeShape'
                            from the input directory. Default -1, i.e.
                            no shapes are removed.

                            Used to perform leave-one-out testing.
  
  @return   Nothing.
  
*/
void CAAMBuilder::BuildFromFiles( CAAMModel &model, 
                                  const CString &inDir, 
                                  const CString &acf, 
                                  const int modelReduction,
                                  const int excludeShape ) {      
    std::vector<CString> asfFiles;    
    asfFiles = CAAMUtil::ScanSortDir( CAAMUtil::AddBackSlash( inDir ), "asf" );
    this->BuildFromFiles( model, asfFiles, acf, modelReduction, excludeShape );
}



/**

  @author   Mikkel B. Stegmann
  @version  4-14-2000

  @memo     Diver method for model generation.

  @doc      This method automates the model generation as much as possible
			by using the various class methods for all the sequences in the
			task of producing a model.           

  @param    model           The generated model.

  @param    asfFiles        Vector of asf filenames.

  @param    acf				Filename of an AAM configuration file. 
							If omitted defaults are used.

  @param    modelReduction  Model reduction multiplier. Default off == 1.                            
                            Useful when building multi-scale AAMs.

  @param    excludeShape    Excludes one shape number 'excludeShape'
                            from the input directory. Default -1, i.e.
                            no shapes are removed.

                            Used to perform leave-one-out testing.
  
  @return   Nothing.
  
*/
void CAAMBuilder::BuildFromFiles( CAAMModel &model, 
                                  const std::vector<CString> &asfFiles,
                                  const CString &acf, 
                                  const int modelReduction,
                                  const int excludeShape ) {

    
    // set the internal model pointer that all 
    // private methods work upon.
    m_pModel = &model;

    // set up
    CHTimer timer;

    timer.start();
	

    // check for config file
	if (acf!="") {

		// read config file
		bool ok=ReadACF( acf );

		if (!ok) {
			printf( "Could not open acf file '%s'. Using defaults.\n", LPCTSTR(acf) );
		}
    }         


    // check if a benchmark is requested
    if (m_iWarpMethod==0) {               
        
        // perform software/hardware benchmark            
        CDMultiBand<TAAMPixel> img;
        CAAMShapeCollection shapes;
                
        shapes.ReadShapes( asfFiles );
        shapes.Rel2Abs( m_pModel->m_iModelReduction );

        if (shapes.size()<2) {

            printf("Error: Benchmarking requires a training set of min. two shapes.\n");
            exit(-1);
        }
        CString imgfile = CAAMUtil::GetPath( asfFiles[0] ) + shapes[1].HostImage();               
        printf( "\nBenchmarking warping methods on [%s]\n", imgfile );    
        img.ReadBandedFile( imgfile );     
        if (m_pModel->m_iModelReduction!=1) {

            img.ReducePyr( m_pModel->m_iModelReduction );
        }        
        CAAMShape refShape(shapes[0]);       
        refShape.Rel2Abs(img.Width(),img.Height());                               
        CAAMTest::AnalyzeTest(refShape, shapes[1], img, m_pModel->m_bUseConvexHull );
                        
        printf("\nBenchmarking done.\n");
        exit(0); // a bit brutal though....
    }
        
    
    // set the model reduction (if requested)
    if (modelReduction!=1) { m_pModel->m_iModelReduction = modelReduction; }
    

    // read images and shapes
	printf( "Reading images and annotations...\n" );    
    bool addCompleteImage = false;
    bool valid_shapes = LoadShapes( asfFiles, 
                                    m_Shapes, 
                                    m_pModel->m_iModelReduction,
                                    addCompleteImage,
                                    m_pModel->m_dAddExtents,
                                    excludeShape );        
    if ( valid_shapes==false ) {

        printf("The training set is not valid. Exiting.\n");
        exit(-1);
    }
       

    // align shapes
    DoShapeAlignment( m_pModel->m_bUseTangentSpace );    


    
    //
    // use a different set of shapes for the shape PCA
    //
    // usage:
    //
    // _putenv( "SHAPE_PCA_OVERRIDE=C:\\users\\mbs\\src2\\test\\greyscale\\all" );
    //
#ifdef USEAAMENV
    CString shapePCAoverideDir = getenv( "SHAPE_PCA_OVERRIDE" );
    bool shapePCAoverride = shapePCAoverideDir.GetLength()>0;
#else
    CString shapePCAoverideDir;
    bool shapePCAoverride = false;
#endif
    CAAMShapeCollection orgShapes, orgAlignedShapes;
    if (shapePCAoverride) {        

        // backup shape collections
        orgShapes = m_Shapes;
        orgAlignedShapes = m_AlignedShapes;

        // find files                
        std::vector<CString> shapeAsfFiles;    
        shapeAsfFiles = CAAMUtil::ScanSortDir( CAAMUtil::AddBackSlash( shapePCAoverideDir ), "asf" );

        // load shapes
        bool valid_shapes = LoadShapes( shapeAsfFiles, 
                                        m_Shapes, 
                                        m_pModel->m_iModelReduction,
                                        addCompleteImage,
                                        m_pModel->m_dAddExtents,
                                        excludeShape );        
        
        if ( valid_shapes==false ) {

            printf("The shape PCA training set is not valid. Exiting.\n");
            exit(-1);
        }

        printf("INFO: Shape PCA overidden using %i shapes from:\n  '%s'\n",
                m_Shapes.NShapes(), shapePCAoverideDir );

        // NOTICE
        // using this feature the texture pca will also be slightly 
        // affected, since the referenceFrame is using the reference
        // shape from the new shape PCA
        //
        DoShapeAlignment( m_pModel->m_bUseTangentSpace );

        // set to number of shape to the number of traning textures
        m_pModel->m_iNShapes = orgShapes.NShapes();
    }


    // shape PCA        
	printf( "Doing PCA on the shape data...\n" );		
    for(int s=0;s<m_AlignedShapes.NShapes();s++) {

        m_pModel->m_ShapePCA.InsertDataItem( m_AlignedShapes[s] );
    }
    m_pModel->m_ShapePCA.DoPCA( m_bMakeDocumentation );
    
    // truncate eigenvectors and eigenvalues to
	// satisfy the variance explanation level 
	// constraint (vlec) or by using parallel analysis    
    if (m_pModel->m_iShapeTrunc==-1) {
        
        m_pModel->m_ShapePCA.TruncateParallel();
        m_pModel->m_ShapePCA.ClearDataItems();
    } else {
        m_pModel->m_ShapePCA.ClearDataItems();
        m_pModel->m_ShapePCA.TruncateVar( m_pModel->m_iShapeTrunc/100. );
    }


    // use a different set of shapes for the shape PCA
    if (shapePCAoverride) {

        // restore shape collections
        m_Shapes = orgShapes;
        m_AlignedShapes = orgAlignedShapes;
    }

    

    //  initialize the ReferenceFrame object
	CAAMShape rs;
	m_AlignedShapes.ReferenceShape( rs );
    m_pModel->m_pReferenceFrame = new CAAMReferenceFrame;
    m_pModel->m_pReferenceFrame->Setup( rs, m_pModel->m_bUseConvexHull );


    //  initialize the AnalyzeSynthesize object     
    switch( m_iWarpMethod ) {

        case 0:
        case 1:     // software
                    m_pModel->m_pAnalyzeSynthesize =         
                        new CAAMAnalyzeSynthesizeSoftware( *m_pModel->m_pReferenceFrame ); 
                    break;

        case 2:     // hardware accelerated OpenGL
                    m_pModel->m_pAnalyzeSynthesize =         
                        new CAAMAnalyzeSynthesizeOpenGL( *m_pModel->m_pReferenceFrame ); 
                    break;

        default:	printf("Wrong warping method (method==%i). Aborting!\n", 
                            m_iWarpMethod );
                    exit(-1);
                    break;
    }
       

    // build texture vectors
	printf( "Building texture vectors...\n" );	
	BuildTextureVectors();	 
    

    // normalize textures
	printf( "Normalizing texture vectors...\n" );
	NormalizeTextureVectors();   


    // apply mappings to the texture vectors
    MapTextures();  	


	// calc texture variances
    // (we can't use the diagonal of the covariance matrix due to the Q-R mode trick...)
	printf( "Calculating texture variances...\n" );	
    CAAMMathUtil::CalcElementVar( m_vTexture, m_pModel->m_vTextureVar );	


    // dump var image
	if ( m_bWriteVarImage ) {

		CString fn = "modelvar.bmp";
		printf( "Writing variance image '%s'...\n", LPCTSTR(fn) );		
		m_pModel->WriteVarianceMap( fn );
	}


    // dump registration movie
	if ( m_bWriteRegMovie ) {

		CString movieName = "registration.avi";	
        CAAMVisualizer AAMvis( m_pModel );
        AAMvis.RegistrationMovie( movieName, m_vTexture );		
	} 
    

    // texture PCA
    // (actually rather excessive use of memory.... 
    //  why have two copies of each texture in memory? ...hmmmm...)
	printf( "Doing PCA on the texture data...\n" );		
    for(unsigned int t=0;t<m_vTexture.size();t++) {

        m_pModel->m_TexturePCA.InsertDataItem( m_vTexture[t] );
    }
    m_pModel->m_TexturePCA.DoPCA();     
    
    // truncate eigenvectors and eigenvalues to
	// satisfy the variance explanation level 
	// constraint (vlec) or by using parallel analysis    
    if (m_pModel->m_iTextureTrunc==-1) {
        
        m_pModel->m_TexturePCA.TruncateParallel();
        m_pModel->m_TexturePCA.ClearDataItems();
    } else {
        m_pModel->m_TexturePCA.ClearDataItems();
        m_pModel->m_TexturePCA.TruncateVar( m_pModel->m_iTextureTrunc/100. );
    }


    // do the combined PCA
	printf( "Doing combined PCA...\n" );		
	std::vector<CDVector> bVectors;
	bVectors = DoCombinedPCA();     


    // build regression/gradient matrices
    if (m_iTSSubsampling>0) {

        switch( m_pModel->m_iLearningMethod ) {

            case 0:		printf( "Building regression matrices (method==%i)...\n", 
                                m_pModel->m_iLearningMethod );		
		                EstRegressionMatrices( bVectors, m_iTSSubsampling );		
                        break;

            case 1:     printf( "Building gradient matrices (method==%i)...\n", 
                                m_pModel->m_iLearningMethod );		
                        EstPredictionMatrices( bVectors, m_iTSSubsampling );		
                        break;

            default:	printf("Wrong learning method (method==%i). Aborting!\n", 
                                m_pModel->m_iLearningMethod );
		                exit(-1);
                        break;
        }
	}


    // make doc
	if (m_bMakeDocumentation) {

        DumpModelDoc( bVectors );
        CAAMVisualizer  AAMvis(m_pModel);
        AAMvis.WriteEigenImages();
        AAMvis.WritePredictionImages();        
	}

   
    // we're done: notify and write timings
    printf( "Done...\n" );	
	timer.stop();
	m_pModel->m_dBuildTime = timer.getTime();
	printf( "Time spent: %s (%.1f secs)\n", 
            CAAMUtil::Secs2Mins(m_pModel->m_dBuildTime), 
            m_pModel->m_dBuildTime );    


    // optional pause to check resource consumption etc.
    bool pause = false;
    if (pause) {

        printf("Press return to continue...\n");
        getchar();    
    }
}



/**

  @author   Mikkel B. Stegmann
  @version  11-4-2002

  @memo     Loads (and preprocess) all training shapes.

  @doc      Loads (and preprocess) all training shapes into a CAAMShapeCollection.
			This could as well be placed in CAAMUtil actually.
  
  @param    asfFiles			An array of asf filenames.

  @param    destination			Output shape collection.

  @param	modelReduction		Optional size reduction. Default 1, i.e. no reduction.

  @param	addCompleteImage	Set this to true if you (for wird reasons) would like
								add the corners of the image to the shape. Default false.

  @param	addExtents			Simple and somewhat hacked way to add a shape neighborhood
								(will be removed in later versions).

  @param	excludeShape		If != -1 the the 'excludeShape'-th shape will be excluded.
								Zero is the first shape. Used for leave-one-out evaluation.
  
  @return   Nothing.
  
*/
bool CAAMBuilder::LoadShapes( const std::vector<CString> &asfFiles,
                              CAAMShapeCollection &destination, 
                              int modelReduction, 
                              bool addCompleteImage,                      
                              double addExtents,
                              int excludeShape ) {


    destination.clear();

    bool valid_shapes = destination.ReadShapes( asfFiles );      

    if ( valid_shapes==false ) {
        
        return false;
    }


    if (excludeShape!=-1) {

        printf("Excluding shape #%i.\n", excludeShape );
        destination.erase( destination.begin()+excludeShape );
    }


    // add shape frame - i.e. the complete image now becomes the model    
    if (addCompleteImage) {

        CAAMShape s(4);
        double hi=0.99, lo=0.01;
        s.SetPoint( 0, lo, lo );
        s.SetPoint( 1, hi, lo );
        s.SetPoint( 2, hi, hi );
        s.SetPoint( 3, lo, hi );

        CAAMShape::CAAMPointInfo pi;
        pi.SetClosed();
        pi.SetHole(false);
        pi.SetOuterEdge(true);        
        for(int i=0;i<destination.NShapes();i++) {

            destination[i].AddPath(s, pi);
            printf("BuildFromFiles: Adding shape frame...\n");
        }
    }


    // force absolute coordinates
    destination.Rel2Abs( modelReduction );


	// add shape extents (if requested)
	if ( addExtents!=.0 ) {

		for(int i=0;i<destination.NShapes();i++) {

			destination[i].AddShapeExtends( (int)(.5+addExtents) );			
		}
	}


    return true;
}



/**

  @author   Mikkel B. Stegmann
  @version  1-28-2002

  @memo     Performs a mapping of all textures in 'm_vTexture'.

  @doc      Performs a mapping of all textures in 'm_vTexture' using
            the texture transfer class of the model.      
  
  @return   Nothing. 
  
*/
void CAAMBuilder::MapTextures() {
     
    int n_tex = m_vTexture.size();
    for(int i=0;i<n_tex;i++) {       

        m_pModel->m_pTextureTF->Map( m_vTexture[i] );
    }        

    // update the number of texture samples
    m_pModel->m_iTextureSamples = m_vTexture[0].Length();

    // recalc the mean texture
	RecalcMeanTexture(); 
}



/**

  @author   Mikkel B. Stegmann
  @version  1-28-2002

  @memo     Write additional documentation output.

  @doc      Write additional documentation output.
  
  @param    bVectors  Concatenated and weighted shape and texture vectors
                      over the training set.
  
  @return   Nothing. 
  
*/
void CAAMBuilder::DumpModelDoc( std::vector<CDVector> bVectors ) {

    
    // write additional documentation output		
    printf( "Dumping additional documentation output to current dir...\n" );				
    
    // write the unaligned shapes
    m_Shapes.ToMatlab( "shapes.m", "unaligned_shapes", "Unaligned shapes.", false );	
    
    // write the aligned shapes
    m_AlignedShapes.ToMatlab( "ashapes.m", "aligned_shapes", "Aligned shapes.", false );	
    
    // write the mean shape		
    m_pModel->m_sMeanAShape.ToMatlab( "meanshape.m", "mean_shape", "The mean shape.", false );
    m_pModel->m_sMeanAShape.WriteASF( "meanshape.asf", 1, 1 );
    
    // write the Delaunay triangulation of the meanshape		  
    m_pModel->ReferenceFrame().RefMesh().ToMatlab( "delaunay.m" );
    
    // write pca values for the training set in 'texture_pc.m', 'shape_pc.m' and 'combined_pc.m'
    DumpPCA( bVectors );
    
    // dump  eigenvalues			
    m_pModel->m_CombinedPCA.EigenValues().ToMatlab( "combined_ev.m", "c_ev", "Combined model eigenvalues.", false );		
    m_pModel->m_TexturePCA.EigenValues().ToMatlab( "texture_ev.m", "t_ev", "Texture model eigenvalues.", false );		
    m_pModel->m_ShapePCA.EigenValues().ToMatlab( "shape_ev.m", "s_ev", "Shape model eigenvalues.", false );		
    
    // dump shape eigenvectors
    m_pModel->m_ShapePCA.EigenVectors().ToMatlab( "shape_evec.m", "s_evec", "Shape eigenvectors (nb points x2 rows / nb eigenvalues cols).", false );				

	// dump inverse alignment transformations, i.e. the transformation needed
	// to align the aligned shapes with the unaligned shapes
	int ns = m_Shapes.NShapes();
	CDMatrix iat( ns, 4 );
	for( int i=0;i<ns;i++) {

		double scale, theta;
		CAAMPoint translation;
		m_AlignedShapes[i].AlignTransformation( m_Shapes[i], scale, theta, translation );
		iat[i][0] = scale;
		iat[i][1] = theta;
		iat[i][2] = translation.x;
		iat[i][3] = translation.y;
	}
	iat.ToMatlab( "iat.m", "T", "the transformations needed to align the aligned shapes with the unaligned shapes", false );

	// a more readable version of the above (with stats)
	FILE *fh = fopen("ts_pose.txt", "wt");
	double scale, theta, xc, yc;
	CAAMPoint translation;	
	if(fh) {

		const int n =  m_Shapes.NShapes();
		CDVector size(n), rotation(n), xcog(n), ycog(n);
		for(int i=0;i<n;i++) {

			m_AlignedShapes[i].AlignTransformation( m_Shapes[i], scale, theta, translation );
			m_Shapes[i].COG( xc, yc );
			size[i]     = scale;		
			rotation[i] = CAAMUtil::Rad2Deg( theta );
			xcog[i]     = xc;
			ycog[i]     = yc;
		}
		size /= size.Mean();
		fprintf( fh, "size\trot\tcog_x\tcog_y\n" );
		for(i=0;i<n;i++) {
			fprintf( fh, "%.2f\t%.2f\t%.2f\t%.2f\n", size[i], rotation[i], xcog[i], ycog[i] );
		}
		fprintf( fh, "\n[mean/std.dev./min/max]\n" );
		fprintf( fh, "size   %.2f \t(%.2f) \t%.2f \t%.2f\n", size.Mean(), size.Std(), size.Min(), size.Max() );
		fprintf( fh, "rot    %.2f \t(%.2f) \t%.2f \t%.2f\n", rotation.Mean(), rotation.Std(), rotation.Min(), rotation.Max() );
		fprintf( fh, "cog_x  %.0f \t(%.2f) \t%.0f \t%.0f\n", xcog.Mean(), xcog.Std(), xcog.Min(), xcog.Max() );
		fprintf( fh, "cog_y  %.0f \t(%.2f) \t%.0f \t%.0f\n", ycog.Mean(), ycog.Std(), ycog.Min(), ycog.Max() );
	}
	fclose(fh);
    
    
    // expensive (i.e. time consuming) dumps
    bool do_expensive_dumps = false;
    if (do_expensive_dumps) {
        
        // dump texture vectors
        int ntex = m_vTexture.size();
        int nsamp= m_pModel-> NTextureSamples();
        CDMatrix texvecs( nsamp, ntex );
        for(int i=0;i<ntex;i++) {
            
            texvecs.SetColumn( i, m_vTexture[i] );
        }
        texvecs.ToMatlab( "texvecs.m", "tex", "textures, one per column", false );
        CDMatrix mt( nsamp, 1);
        mt.SetColumn( 0, m_pModel->MeanTexture() );
        mt.ToMatlab("meantex.m", "mt", "mean texture", false );
        
        // dump texture eigenvectors		
        m_pModel->m_TexturePCA.EigenVectors().ToMatlab( "texture_evec.m", "t_evec", "Texture eigenvectors (nb pixels rows / nb eigenvalues cols).", false );
    } 
    
}



/**

  @author   Mikkel B. Stegmann
  @version  3-14-2000

  @memo     Build the regression matrices for pose and parameter prediction.

  @doc      Build the regression matrices for pose and parameter prediction.
            I.e. calculates the member variables 'regR_c', 'regR_t'.
            using principal component regression.
  
  @param    bVectors	    The b-vectors for the training set the current AAM 
						    is built upon.
						    [Can be optained from the DoCombinedPCA() call]    

  @param    ts_subsampling   Controls the sub sampling of the training set - 
                             i.e. to use every fifth shape to build the 
                             regression matrices upon, set
    
                               shape_subsampling = 5;
    
                             The motivation for doing this is reduction of 
                             model building time -- and perhaps most 
                             importantly -- conservation of memory resources.                        
  @return   Nothing.
  
*/
void CAAMBuilder::EstRegressionMatrices( const std::vector<CDVector> &bVectors, 
                                           const int ts_subsampling  ) {	

    // output info
    int nShapes = m_Shapes.NShapes();
    int nbShapeSamples = nShapes%ts_subsampling ? 
                         nShapes/ts_subsampling + 1 : nShapes/ts_subsampling;
    printf("Info: Training set subsampling = %i (%i shapes used)\n", 
            ts_subsampling, nbShapeSamples );	


    // convert b to c parameters
	std::vector<CDVector> cVectors;
	m_pModel->ShapeTexParam2Combined( bVectors, cVectors ); 


    // generate displacement vectors
    std::vector<CDVector> vCDisps, vPoseDisps;    
    DisplacementSets( vCDisps, vPoseDisps, cVectors );
    

    // do model parameter experiments
    {
        CDMatrix X, C;
        DoCParamExperiments( vCDisps, cVectors, X, C, ts_subsampling );        

        // estimate Rc using principal component regression	
        CAAMLinearReg reg;         
        reg.DoRegression( C, X,  m_pModel->m_R_c ); 
    }


    // do pose experiments
    {
        CDMatrix X, C;
        DoPoseExperiments( vPoseDisps, cVectors, X, C, ts_subsampling );
        int n = C.NRows();
        CDVector var(n);
       
        // estimate Rt using principal component regression	
        CAAMLinearReg reg;         
        reg.DoRegression( C, X,  m_pModel->m_R_t );
    }


    // store negative versions to avoid any sign change at each iteration
    m_pModel->m_R_c *= -1;
    m_pModel->m_R_t *= -1;
}



/**

  @author   Mikkel B. Stegmann
  @version  5-13-2002

  @memo     Performs a set of pose parameter displacement experiments.

  @doc      Performs a set of pose parameter displacement experiments on the
			training set given a set of displacment vectors.

  @see      DoPoseExperiments
  
  @param    vPoseDisps	A vector of displacement vectors as obtained from
						PoseDispVectors() or DisplacementSets().

  @param    cVectors	The set of optimum c vectors for the training examples.

  @param	X			Output matrix containing the texture difference vectors
						obtained from the displacements.

  @param	Y			Output matrix containing the model parameter displacements
						carried out.

  @param	ts_subsampling
						Subsampling factor, i.e. ts_subsampling==n will carry out
						displacements on every n-th example in the training set.
  
  @return   Nothing.
  
*/
void CAAMBuilder::DoPoseExperiments( const std::vector<CDVector> &vPoseDisps, 
                                     const std::vector<CDVector> &cVectors, 
                                     CDMatrix &X, CDMatrix &C,
                                     const int ts_subsampling ) const {

    // setup
    int nShapes = m_Shapes.NShapes();    

    // determine subsampling of the training set
    int nbShapeSamples = nShapes%ts_subsampling ? 
                         nShapes/ts_subsampling + 1 : nShapes/ts_subsampling;
    int totalExp = vPoseDisps.size()*nbShapeSamples;
    int nExperiment = 0;    
    X.Resize( m_pModel->m_iTextureSamples, totalExp );
    C.Resize( 4, totalExp );   
    CDVector delta_g;

    // for each training example in the (subsampled) training set     	
	for(int nShape=0;nShape<nShapes;nShape+=ts_subsampling) {

        // get the shape image
        CDMultiBand<TAAMPixel> image; 
        m_Shapes[nShape].GetHostImage( image, m_Shapes.Path(), 
                                       m_pModel->m_iModelReduction );       	
		
		for(unsigned int i=0;i<vPoseDisps.size();i++) {

			// do displacement measures
			PoseDisplacement(	image, m_Shapes[nShape], cVectors[nShape], 
                                vPoseDisps[i], delta_g );

			// insert the results into X and C
			X.SetColumn( nExperiment, delta_g );
			C.SetColumn( nExperiment, vPoseDisps[i] );
			++nExperiment;			
		}
        printf( "Experiment %i of %i done (pose)...\n", nExperiment, totalExp );			
	}	
}



/**

  @author   Mikkel B. Stegmann
  @version  5-13-2002

  @memo     Performs a set of model parameter displacement experiments.

  @doc      Performs a set of model parameter displacement experiments on the
			training set given a set of displacment vectors.

  @see      DoPoseExperiments
  
  @param    vCDisps		A vector of displacement vectors as obtained from
						CParamDispVectors() or DisplacementSets().

  @param    cVectors	The set of optimum c vectors for the training examples.

  @param	X			Output matrix containing the texture difference vectors
						obtained from the displacements.

  @param	Y			Output matrix containing the model parameter displacements
						carried out.

  @param	ts_subsampling
						Subsampling factor, i.e. ts_subsampling==n will carry out
						displacements on every n-th example in the training set.
  
  @return   Nothing.
  
*/
void CAAMBuilder::DoCParamExperiments( const std::vector<CDVector> &vCDisps, 
                                       const std::vector<CDVector> &cVectors, 
                                       CDMatrix &X, CDMatrix &C,
                                       const int ts_subsampling ) const {
                
	// setup
    int nShapes = m_Shapes.NShapes();
    int np = m_pModel->m_CombinedPCA.NParameters();   	

    // determine subsampling of the training set
    int nbShapeSamples = nShapes%ts_subsampling ? 
                         nShapes/ts_subsampling + 1 : nShapes/ts_subsampling;
    int totalExp = vCDisps.size()*nbShapeSamples;    
    int nExperiment = 0;    
    X.Resize( m_pModel->m_iTextureSamples, totalExp );
    C.Resize( np, totalExp );   
    CDVector delta_g;

    // for each training example in the (subsampled) training set
	for(int nShape=0;nShape<nShapes;nShape+=ts_subsampling) {

        // get the shape image
        CDMultiBand<TAAMPixel> image; 
        m_Shapes[nShape].GetHostImage( image, m_Shapes.Path(), 
                                       m_pModel->m_iModelReduction );        	

        for(unsigned int i=0;i<vCDisps.size();i++) {

            // do displacement measures
			ModelDisplacement( image, m_Shapes[nShape], cVectors[nShape], 
                               vCDisps[i], delta_g );

			// insert the results into X and C
			X.SetColumn( nExperiment, delta_g );
			C.SetColumn( nExperiment, vCDisps[i] );
			++nExperiment;					
		}
        printf( "Experiment %i of %i done (c)...\n", nExperiment, totalExp );
	}
}



/**

  @author   Mikkel B. Stegmann
  @version  3-14-2000

  @memo     Generates model parameter and pose displacement sets.

  @doc      Generates model parameter and pose displacement sets.

  @see      CParamDispVectors, PoseDispVectors
  
  @param    vCDisps     Resulting model parameter displacement set.

  @param    vPoseDisps  Resulting pose parameter displacement set.

  @param    cVectors    The set of c vectors over the training set.
  
  @return   Nothing.
  
*/
void CAAMBuilder::DisplacementSets( std::vector<CDVector> &vCDisps, 
                                    std::vector<CDVector> &vPoseDisps,
                                    const std::vector<CDVector> &cVectors ) const {

    ////////////////////////////////////////////////////////////////////
    //
    // NOTICE: sets must *always* be in anti-symmetric pairs with the
    //         largest displacements first, e.g. [ -.5, .5, -.25, .25 ]
    //
    ////////////////////////////////////////////////////////////////////


    // generate c displacement sets    
    CDVector vStdDisp(4);        

    // MBS master's thesis displacement set
    vStdDisp[0] = -.5;
    vStdDisp[1] =  .5;
    vStdDisp[2] = -.25;       
    vStdDisp[3] =  .25;
   
    vCDisps = CParamDispVectors( vStdDisp, cVectors );
    
    // generate pose displacement sets        
    CDVector vXYDisp(6);    
    CDVector vScaleDisp(6);
    CDVector vRotDisp(6);  
    
    // relative displacement set        
    vXYDisp.Resize(4);
    vXYDisp[0] = -.05;
    vXYDisp[1] =  .05;                
    vXYDisp[2] = -.10;
    vXYDisp[3] =  .10;        

    vScaleDisp.Resize(4);
    vScaleDisp[0] =  .85;
    vScaleDisp[1] = 1.15;
    vScaleDisp[2] =  .95;        
    vScaleDisp[3] = 1.05;
    
    vRotDisp.Resize(4);
    vRotDisp[0] = -15.;  // degrees
    vRotDisp[1] =  15.;
    vRotDisp[2] = -5.;        
    vRotDisp[3] =  5.;
    
    CDVector vXDisp(vXYDisp.Length()),vYDisp(vXYDisp.Length());
    vXDisp = vXYDisp*m_pModel->ReferenceShape().Width();
    vYDisp = vXYDisp*m_pModel->ReferenceShape().Height();        
              
    vPoseDisps = PoseDispVectors( vXDisp, vYDisp, vScaleDisp, vRotDisp );
}



/**

  @author   Mikkel B. Stegmann
  @version  3-14-2000

  @memo     Generates a set combined model parameter displacement vectors.

  @doc      Generates a set combined model parameter displacement vectors
            where each parameter is displaced at a time according to the 
            values in vStdDisp.
            
  @see      CParamDispVectors
  
  @param    vStdDisp   A vector of parameter displacements in 
                       standard deviations of the corresponding
                       parameter.        

  @param    cVectors   The set of c vectors over the training set.

  @param    pStart     The first parameter to displace.
                       (default 0).

  @param    pLen       The number of parameters to displace.
                       (default 0, which means all parameters).
  
  @return   A vector of displacement vectors.
  
*/
std::vector<CDVector> CAAMBuilder::CParamDispVectors( const CDVector &vStdDisp, 
                                                      const std::vector<CDVector> &cVectors,
                                                      const int pStart, const int pLen ) const {	

    std::vector<CDVector> cDisplacements;
    int np = m_pModel->m_CombinedPCA.NParameters();  

    // sanity checks
    assert(pStart>=0);
    assert(pLen==0 || (pStart+pLen)<=np);  
       
    // calc var
	CDVector paramVar;
    CAAMMathUtil::CalcElementVar( cVectors, paramVar );

    // make displacement vectors
    int len = pLen==0 ? np : pLen;
    for(int i=pStart;i<pStart+len;i++) {

        // for each parameter
		double std = sqrt( paramVar[i] ); // get standard deviation for this parameter
        for(int j=0;j<vStdDisp.Length();j++) {
		
            // for each displacement
		    CDVector dC(np);
            dC = .0;
            dC[i] = vStdDisp[j]*std;
            cDisplacements.push_back( dC );
        }
    }

    return cDisplacements;
}



/**

  @author   Mikkel B. Stegmann
  @version  3-14-2000

  @memo     Generates a set pose displacement vectors.

  @doc      Generates a set pose displacement vectors.

  @see      CParamDispVectors
  
  @param    vXDisp      A vector of x displacements in pixels.

  @param    vYDisp      A vector of y displacements in pixels.

  @param    vScaleDisp  A vector of scale displacements (1.0=no scaling).

  @param    vRotDisp    A vector of rotation displacements in degrees.   
  
  @return   A vector of displacement vectors.
  
*/
std::vector<CDVector> CAAMBuilder::PoseDispVectors( const CDVector &vXDisp, 
                                                    const CDVector &vYDisp, 
                                                    const CDVector &vScaleDisp, 
                                                    const CDVector &vRotDisp   ) const {	

    std::vector<CDVector> poseDisplacements;

    // add x displacements
    for(int i=0;i<vXDisp.Length();i++) {

        CDVector dT(4);
        dT = .0;
        CAAMShape::Param2PoseVec( 1., .0, vXDisp[i], .0, dT );
        poseDisplacements.push_back( dT );
    }

    // add y displacements
    for(i=0;i<vYDisp.Length();i++) {

        CDVector dT(4);
        dT = .0;
        CAAMShape::Param2PoseVec( 1., .0, .0, vYDisp[i], dT );
        poseDisplacements.push_back( dT );
    }

    // add scale displacements
    for(i=0;i<vScaleDisp.Length();i++) {

        CDVector dT(4);
        dT = .0;
        CAAMShape::Param2PoseVec( vScaleDisp[i], .0, .0, .0, dT );
        poseDisplacements.push_back( dT );
    }

    // add rotation displacements
    for(i=0;i<vRotDisp.Length();i++) {

        CDVector dT(4);
        dT = .0;
        CAAMShape::Param2PoseVec( 1., vRotDisp[i]*M_PI/180., .0, .0, dT );
        poseDisplacements.push_back( dT );
    }

    return poseDisplacements;
}



/**

  @author   Mikkel B. Stegmann
  @version  2-21-2000

  @memo     Samples all shapes in the training set and build the corresponding
			texture vectores.

  @doc      Initializes the private member: 'm_vTexture' by sampling all shapes
			using a warp function.
			Note that this method calculates the mean texture.
  
  @return   Nothing.
  
*/
void CAAMBuilder::BuildTextureVectors() {

	int nSamples, nbShapes = m_Shapes.NShapes();
	CDVector vTexture;	
	
	for(int i=0;i<nbShapes;i++) {       

        // get the shape image
        CDMultiBand<TAAMPixel> image; 
        m_Shapes[i].GetHostImage( image, m_Shapes.Path(), m_pModel->m_iModelReduction );                
	

		// sample the texture of the i-th shape into 'vTexture'        	
        nSamples = m_pModel->SampleShape( image, m_Shapes[i], vTexture, false );		       


        if (i==0) { 

            m_pModel->m_iTextureSamples=nSamples;                        
        }
		m_vTexture.push_back( vTexture );
	}      
}



/**

  @author   Mikkel B. Stegmann
  @version  10-26-2000

  @memo     Constructor.

  @doc      Constructor. Sets up default values for the settings usally
			given by an acf file.
  
  @return   Nothing.
  
*/
CAAMBuilder::CAAMBuilder() :
    m_bVerbose(false),
	m_bWriteRegMovie(false),
	m_bWriteVarImage(false),
	m_bMakeDocumentation(false),
    m_iTSSubsampling(1),
    m_iWarpMethod(1),
    m_pModel(NULL) {
}



/**

  @author   Mikkel B. Stegmann
  @version  10-26-2000

  @memo     Destructor.

  @doc      Destructor.  
  
  @return   Nothing.
  
*/
CAAMBuilder::~CAAMBuilder() {


}



/**

  @author   Mikkel B. Stegmann
  @version  3-6-2000

  @memo     Calculates the pixel-to-shape weights.

  @doc      Calculates the pixel-to-shape weights used in the combined PCA.
			Currently the simple 'split even' strategy is employed, i.e.
			normalise shape and texture variance to be equal.
  
  @return   Nothing.
  
*/
void CAAMBuilder::CalcPixel2ShapeWeights() {
    
    int nbShapeParam = m_pModel->m_ShapePCA.NParameters();        
    
    //
    // well, since we are using this very simple weightning
    // scheme it could be hard-coded in the CAAMModel,
    // providing clarity and smaller model files, etc.
    //
    // however, for now we still retain the full matrix 
    // formulation, not even exploiting that it is a 
    // diagonal matrix
    //
    // since the usage of this matrix is cached inside
    // CAAMModel it does not slow it down....
    //
    m_pModel->m_mShape2PixelWeights.Resize( nbShapeParam,nbShapeParam );
    m_pModel->m_mShape2PixelWeights.Eye();	
    
    // split even
    double val = m_pModel->m_TexturePCA.EigenValues().Sum() /
                 m_pModel->m_ShapePCA.EigenValues().Sum();
    
    m_pModel->m_mShape2PixelWeights *= sqrt( val );
}



/**

  @author   Mikkel B. Stegmann
  @version  3-14-2000

  @memo     Performs one pose regression experiment.

  @doc      Performs one pose regression experiment.

  @see      ModelDisplacement
  
  @param    image		The image corresponding to the equibrilium shape.
  @param    shape		The equibrilium shape.
  @param    c0			The equibrilium model parameters.
  @param    t			The pose displacement parameters.
  @param    pixel_diff	The normalized pixel differences resulting from 
						the pose displacement.  
  
  @return   Nothing.
  
*/
void CAAMBuilder::PoseDisplacement(	const CDMultiBand<TAAMPixel> &image, 
								    const CAAMShape &shape, 
									const CDVector &c0, 
									const CDVector &t, 										
									CDVector &pixel_diff	) const {

	CDVector g_m, g_s;
	CAAMShape X_shape;
	
	// generate model texture
	m_pModel->TextureInstance( c0, g_m );

	// generate model shape
	m_pModel->ShapeInstance( c0, X_shape );

	// calc alignment from X_shape to the annotated shape	
    CAAMPoint translation;
    double scale, theta;
    X_shape.AlignTransformation( shape, scale, theta, translation );

    // align displaced shape onto the image domain           
    X_shape.Scale( scale );

    // calc ratio for this shape
    double sizeRatio = X_shape.ShapeSize()/m_pModel->ReferenceShape().ShapeSize();

	// impose the right pose displacement on X by using t
    CDVector tSizeNormalized(t);
    tSizeNormalized[2] *= sizeRatio;
    tSizeNormalized[3] *= sizeRatio;
	X_shape.Displace( tSizeNormalized );    

    // align the displaced shape onto the image domain        
    X_shape.Rotate( theta );
    X_shape.Translate( translation );
    
	// sample the shape
	int nSamples = m_pModel->SampleShape( image, X_shape, g_s );    
    if( nSamples==0 ) {

        // LATER
        printf("Shape was outside image - texture set to zero.\n" );
        g_s.Resize( g_m.Length() );
        g_s = 0;
    }


	// calc pixel difference
	pixel_diff.Resize( g_m.Length() );
	pixel_diff = g_s;
    pixel_diff -= g_m;
}



/**

  @author   Mikkel B. Stegmann
  @version  3-14-2000

  @memo     Performs one model parameter regression experiment.

  @doc      Performs one model parameter regression experiment.

  @see      PoseDisplacement
  
  @param    image		The image corresponding to the equibrilium shape.
  @param    shape		The equibrilium shape.
  @param    c0			The equibrilium model parameters.
  @param    delta_c		The model parameter displacements.
  @param    pixel_diff	The normalized pixel differences resulting from 
						the pose displacement.  
  
  @return   Nothing.
  
*/
void CAAMBuilder::ModelDisplacement(	const CDMultiBand<TAAMPixel> &image, 
										const CAAMShape &shape, 
										const CDVector &c0, 
										const CDVector &delta_c, 
										CDVector &pixel_diff ) const {

	CDVector c( c0.Length() ), g_m, g_s;
	CAAMShape X_shape;

	c = delta_c+c0;
	
	// generate model texture
	m_pModel->TextureInstance( c, g_m );

	// generate model shape
	m_pModel->ShapeInstance( c, X_shape );

	// Align X_shape to the annotated shape
	X_shape.AlignTo( shape );

	// sample the shape
	int nSamples = m_pModel->SampleShape( image, X_shape, g_s );


    if( nSamples==0 ) {

        // LATER
        printf("Shape was outside image - texture set to zero.\n" );
        g_s.Resize( g_m.Length() );
        g_s = 0;
    }


	// calc pixel difference
	pixel_diff.Resize( g_m.Length() );
	pixel_diff  = g_s;
    pixel_diff -= g_m;	
}


/**

  @author   Mikkel B. Stegmann
  @version  10-24-2000

  @memo     Aligns shapes and calc mean- and reference-shape.

  @doc      Alignes shapes and calc mean- and reference-shape.
            I.e. initializes 'm_AlignedShapes', 'm_sMeanAShape' and
            'm_sReferenceShape'.
   
  @param    fUseTangentSpace    Use the tangent space projection (bool).
  
  @return   Noting.
  
*/
void CAAMBuilder::DoShapeAlignment( const bool fUseTangentSpace ) {

    // copy the unaligned shapes
	m_AlignedShapes = m_Shapes; 

	// align shape with respect to position, scale and rotation
	m_AlignedShapes.AlignShapes( fUseTangentSpace ); 

	// calculate the cached mean shape of the aligned shapes
	m_AlignedShapes.MeanShape( m_pModel->m_sMeanAShape );	
    
    // set the mean shape size and the number of shapes
    m_pModel->m_dMeanShapeSize = m_AlignedShapes.MeanSize();    
    m_pModel->m_iNShapes = m_Shapes.NShapes();
}


/**

  @author   Mikkel B. Stegmann
  @version  5-17-2000

  @memo     Dumps the PC scores of the shape, texture and combined PCA.
  
  @doc      Dumps the PC scores of the shape, texture and combined PCA.
			These are written to the current directory in Matlab format
			as shape_pc.m, texture_pc.m and combined_pc, respectively.
 
  param		bVectors	The b-parameters for all training examples. 
						As obtained from DoCombinedPCA().
				
  @return   Nothing.
  
*/
void CAAMBuilder::DumpPCA( const std::vector<CDVector> &bVectors ) {
	
	int nbTrainingExamples = m_vTexture.size();		
	int nbTexParam   = m_pModel->m_TexturePCA.NParameters();
	int nbShapeParam = m_pModel->m_ShapePCA.NParameters();
    int nbCombinedParam = m_pModel->m_CombinedPCA.NParameters();

	CDMatrix shapePC( nbShapeParam, nbTrainingExamples );
	CDMatrix texturePC( nbTexParam, nbTrainingExamples );
    CDMatrix combinedPC( nbCombinedParam, nbTrainingExamples );
	
	//////////////////////////////////////////////////////////
	// generate c-vectors for all elements in the trainingset
	//////////////////////////////////////////////////////////	
	std::vector<CDVector> cVectors;
    m_pModel->ShapeTexParam2Combined( bVectors, cVectors );
			
	shapePC		= .0;
	texturePC	= .0;
	// generate b-vectors for all elements in the trainingset
	for(int i=0;i<nbTrainingExamples;i++) {
		
		// the format of bVectors[i] is [ <shape pc> <tex pc>]

		// set the shape row
		for(int r=0;r<nbShapeParam;r++) {

			shapePC[r][i] = bVectors[i][r];
		}

		// set the texture row
		for(r=0;r<nbTexParam;r++) {

			texturePC[r][i] = bVectors[i][r+nbShapeParam];
		} 
        
        // set the combined row
		for(r=0;r<nbCombinedParam;r++) {

			combinedPC[r][i] = cVectors[i][r];
		}
	}

    // remove the shape2pixel weights from the b_s
    CDMatrix w = m_pModel->Shape2PixelWeights();
    shapePC = w.Inverted()*shapePC;

	shapePC.ToMatlab( "shape_pc.m", "s_pc", "PC-projection of training shapes. Each example occupy one column. Largest pc (pc1) in last row.", false );
	texturePC.ToMatlab( "texture_pc.m", "t_pc", "PC-projection of training textures. Each example occupy one column. Largest pc (pc1) in last row.", false );
    combinedPC.ToMatlab( "combined_pc.m", "c_pc", "PC-projection of training shapes and textures. Each example occupy one column. Largest pc (pc1) in last row.", false );
}


/**

  @author   Mikkel B. Stegmann
  @version  2-22-2000

  @memo		Iterative normalization of the texture samples.
  
  @doc      Performs normalization of the texture vectors as described 
			by Cootes et al. in "Active Appearance Models" sec. 2.
			Recalculates the mean texture.

  @return	Nothing.
    
*/
void CAAMBuilder::NormalizeTextureVectors() {

	// get sizes
    double diff = 1e306;
	int nbTextures   = m_vTexture.size();
	assert(m_vTexture.size()>0);	
	CDVector lastMeanEstimate;

	// iterate until all texture vectors
	// are normalized using a stable estimate
	// of the mean texture
	m_pModel->m_vMeanTexture = m_vTexture[0];		
    CAAMMathUtil::ZeroMeanUnitLength( m_pModel->m_vMeanTexture );    
    m_pModel->m_vMeanTextureOrg = m_pModel->m_vMeanTexture; // looks strange but see NormalizeTexture()
	int iter=0;
	while(diff>1e-5 && iter<10) {

		// save last mean estimate
		lastMeanEstimate = m_pModel->m_vMeanTexture;						
		
		// normalize all texture vectors to the new mean estimate
		for(int i=0;i<nbTextures;i++) {
                        
            m_pModel->NormalizeTexture( m_vTexture[i] );           			
		}

        // recalc mean texture
		RecalcMeanTexture();
        m_pModel->m_vMeanTextureOrg = m_pModel->m_vMeanTexture; // looks strange but see NormalizeTexture()

        // test if the mean estimate has converged
		diff = ((CDVector)(m_pModel->m_vMeanTexture-lastMeanEstimate)).Norm2();        

		++iter;
	}    
    m_pModel->m_vMeanTextureOrg = m_pModel->m_vMeanTexture;
}


/**

  @author   Mikkel B. Stegmann
  @version  4-17-2000

  @memo     Reads and parses an ACF file.

  @doc      Reads and parses an ACF file. In this way the AAM can be 
			configured using different setting for model generation.
			Note that all parsing is very primitive and in no way 
			robust :-( So be careful about the configuration files.  
  
  @param    filename	The acf file to open.  
  
  @return   true on success, false on file errors.
  
*/
bool CAAMBuilder::ReadACF( const CString &filename ) {

	CAAMPropsReader r(filename);
	int i;

	if ( !r.IsValid() ) return false;

	// read settings
	r.Sync();
	fscanf( r.FH(), "%i", &i);
	m_pModel->m_iModelReduction = i;

	r.Sync();
	fscanf( r.FH(), "%i", &i);
	m_pModel->m_dAddExtents = i;
	
	r.Sync();
	fscanf( r.FH(), "%i", &i);
	m_pModel->m_bUseConvexHull = i!=0;

	r.Sync();
	fscanf( r.FH(), "%i", &i);
	m_bVerbose = i!=0;

	r.Sync();
	fscanf( r.FH(), "%i", &i);
	m_bWriteRegMovie = i!=0;

	r.Sync();
	fscanf( r.FH(), "%i", &i);
	m_bWriteVarImage = i!=0;

	r.Sync();
	fscanf( r.FH(), "%i", &i);
	m_bMakeDocumentation = i!=0;

	r.Sync();
	fscanf( r.FH(), "%i", &i);
	m_pModel->m_bUseTangentSpace = i!=0;
	
	r.Sync();
	fscanf( r.FH(), "%i", &i);
	m_pModel->m_iLearningMethod = i;

	r.Sync();
	fscanf( r.FH(), "%i", &i);
	m_pModel->m_iShapeTrunc = i;	

    r.Sync();
	fscanf( r.FH(), "%i", &i);
	m_pModel->m_iTextureTrunc = i;

    r.Sync();
	fscanf( r.FH(), "%i", &i);
	m_pModel->m_iCombinedTrunc = i;

    r.Sync();
    fscanf( r.FH(), "%i", &i);
    m_iTSSubsampling = i;

    r.Sync();
    fscanf( r.FH(), "%i", &i);
    m_iWarpMethod = i;

	return true;
}


/**

  @author   Mikkel B. Stegmann
  @version  2-22-2000

  @memo		Recalculates the mean texture vector 'm_vMeanTexture'.      

  @doc		Recalculates the mean texture vector 'm_vMeanTexture'.      

  @return	Nothing.
  
*/
void CAAMBuilder::RecalcMeanTexture( ) {

	int nbTextures   = m_vTexture.size();
	assert(nbTextures>0);
	int nbTexSamples = m_vTexture[0].Length();

	// calc the mean texture
	m_pModel->m_vMeanTexture.Resize( nbTexSamples );
	m_pModel->m_vMeanTexture = .0;
	for(int i=0;i<nbTextures;i++) {

		m_pModel->m_vMeanTexture += m_vTexture[i];
	}
	m_pModel->m_vMeanTexture /= nbTextures;       
}


/**

  @author   Mikkel B. Stegmann
  @version  2-23-2000

  @memo		Performs PCA on shape and texture data.

  @doc		Performs principal component analysis on the shape and the
			texture data.
			Uses the Eckhart-Young theorem described in appendix A of
			"Statistical Models of Appearance of Computer Vision" by
			T.F. Cootes et al. if we have fewer samples than dimensions
			which is typically the case.

  @return	The b-parameters for all training examples. The format is
			a vector of b-parameter vectors. This vector is used in the
			BuildRegressionMatrices() call.

*/
std::vector<CDVector> CAAMBuilder::DoCombinedPCA() {
	
    std::vector<CDVector> bVectors;	
	int nbTextures = m_vTexture.size();
	assert(nbTextures>0);
	int nbShapeParam = m_pModel->m_ShapePCA.NParameters();        
    int nbTexParam   = m_pModel->m_TexturePCA.NParameters();
	
	//////////////////////////////////////////////////////////
	// generate b-vectors for all elements in the trainingset
	//////////////////////////////////////////////////////////	
	CDVector b;
	
	// calc the pixel to shape weights
	CalcPixel2ShapeWeights();
	
	// generate b-vectors for all elements in the trainingset    
	for(int i=0;i<nbTextures;i++) {

		// calc parameters
		m_pModel->ShapeTex2Param( m_AlignedShapes[i], m_vTexture[i], b );

		// add
        m_pModel->m_CombinedPCA.InsertDataItem( b );	
        bVectors.push_back( b );
	}
    if (m_pModel->m_iCombinedTrunc==-2) {
    
        // just concatenate vectors
        m_pModel->m_CombinedPCA.UseIdentityTransformation();
    } else {

        // do normal pca
        m_pModel->m_CombinedPCA.DoPCA();     

        // truncate eigenvectors and eigenvalues to
	    // satisfy the variance explanation level 
	    // constraint (velc) or by using parallel analysis    
        if (m_pModel->m_iCombinedTrunc==-1) {
        
            m_pModel->m_CombinedPCA.TruncateParallel();
            m_pModel->m_CombinedPCA.ClearDataItems();
        } else {
            m_pModel->m_CombinedPCA.ClearDataItems();
            m_pModel->m_CombinedPCA.TruncateVar( m_pModel->m_iCombinedTrunc/100. );
        }  
    }

	// extract the shape part of the combined eigen vectors
	m_pModel->m_mQsEV.Resize( nbShapeParam, m_pModel->m_CombinedPCA.EigenVectors().NCols() );	
	m_pModel->m_mQsEV = m_pModel->m_CombinedPCA.EigenVectors().Submatrix( m_pModel->m_mQsEV.NRows(), m_pModel->m_mQsEV.NCols(), 0, 0 );


	// extract the texture part of the combined eigen vectors
	m_pModel->m_mQgEV.Resize( nbTexParam, m_pModel->m_CombinedPCA.EigenVectors().NCols() );
	m_pModel->m_mQgEV = m_pModel->m_CombinedPCA.EigenVectors().Submatrix( m_pModel->m_mQgEV.NRows(), m_pModel->m_mQgEV.NCols(), nbShapeParam, 0 );

	return bVectors;
}



/**

  @author   Mikkel B. Stegmann
  @version  7-18-2002

  @memo     Build the prediction matrices for pose and parameter prediction.

  @doc      Build the prediction matrices for pose and parameter prediction.
            I.e. calculates the member variables 'regR_c', 'regR_t'.
            using estimates of the gradient matrices.
  
  @param    bVectors	    The b-vectors for the training set the current AAM 
						    is built upon.
						    [Can be optained from the DoCombinedPCA() call]    

  @param    ts_subsampling   Controls the sub sampling of the training set - 
                             i.e. to use every fifth shape to build the 
                             regression matrices upon, set
    
                               shape_subsampling = 5;
    
                             The motivation for doing this is reduction of 
                             model building time.
  @return   Nothing.
  
*/
void CAAMBuilder::EstPredictionMatrices( const std::vector<CDVector> &bVectors, 
                                         const int ts_subsampling  ) {	

    // output info
    int nShapes = m_Shapes.NShapes();
    int nbShapeSamples = nShapes%ts_subsampling ? 
                         nShapes/ts_subsampling + 1 : nShapes/ts_subsampling;
    printf("Info: Training set subsampling = %i (%i shapes used)\n", 
            ts_subsampling, nbShapeSamples );	


    // convert b to c parameters
	std::vector<CDVector> cVectors;
	m_pModel->ShapeTexParam2Combined( bVectors, cVectors ); 


    // generate displacement vectors
    std::vector<CDVector> vCDisps, vPoseDisps;    
    DisplacementSets( vCDisps, vPoseDisps, cVectors );

    //
    // NOTICE: assumes that the displacement sets only are 
    //         displacing one parameter at the time
    //

    // do model parameter experiments
    {
        CDMatrix Gparam;
        EstCParamGradientMatrix( vCDisps, cVectors, Gparam, ts_subsampling );

        // estimate Rc          

        // calc pseudo-inverse
        //
        // this is done much more stable using svd, 
        // but this requires to much memory, hence 
        // a simple inversion is used
        //
        CDMatrix Gt = Gparam.Transposed();            
        m_pModel->m_R_c = (Gt*Gparam).Inverted()*Gt;
    }
   

    // do pose experiments
    {
        CDMatrix Gpose;
        EstPoseGradientMatrix( vPoseDisps, cVectors, Gpose, ts_subsampling );

        // estimate Rt           

        // calc pseudo-inverse
        //
        // this is done much more stable using svd, 
        // but this requires to much memory, hence 
        // a simple inversion is used
        //
        CDMatrix Gt = Gpose.Transposed();
        m_pModel->m_R_t = (Gt*Gpose).Inverted()*Gt;
    } 
        
    // store negative versions to avoid any sign change at each iteration
    m_pModel->m_R_c *= -1;
    m_pModel->m_R_t *= -1;
}


/**

  @author   Mikkel B. Stegmann
  @version  7-18-2002

  @memo     Estimates the Jacobian of the pose parameters.

  @doc      Estimates the Jacobian of the pose parameters given a set
			of displacement vectors and the optimum model parameters for
			the training set.

  @see      EstCParamGradientMatrix
  
  @param    vPoseDisps	A vector of displacement vectors as obtained from
						PoseDispVectors() or DisplacementSets().

  @param    cVectors	The set of optimum c vectors for the training examples.

  @param	Gpose		The output Jacobian matrix (or gradient matrix if you like).

  @param	ts_subsampling
						Subsampling factor, i.e. ts_subsampling==n will carry out
						displacements on every n-th example in the training set.
  
  @return	Nothing. 
  
*/
void CAAMBuilder::EstPoseGradientMatrix( const std::vector<CDVector> &vPoseDisps, 
                                         const std::vector<CDVector> &cVectors, 
                                         CDMatrix &Gpose,
                                         const int ts_subsampling ) const {


    // setup
    int nShapes = m_Shapes.NShapes();    

    // determine subsampling of the training set
    int nbShapeSamples = nShapes%ts_subsampling ? 
                         nShapes/ts_subsampling + 1 : nShapes/ts_subsampling;
    int totalExp = vPoseDisps.size()/2.*nbShapeSamples;
    int nExperiment = 0;    

    int nParam = vPoseDisps[0].Length();
    Gpose.Resize( m_pModel->m_iTextureSamples, nParam );    
    CDVector dg1, dg2, cDiff, Gj(m_pModel->m_iTextureSamples);
    CDVector normFactors(nParam);


    // reset gradient matrix
    Gpose = .0;    


    // for each training example in the (subsampled) training set     	
	for(int nShape=0;nShape<nShapes;nShape+=ts_subsampling) {

        // get the shape image
        CDMultiBand<TAAMPixel> image; 
        m_Shapes[nShape].GetHostImage( image, m_Shapes.Path(), 
                                       m_pModel->m_iModelReduction );       	        
		
		for(unsigned int i=0;i<vPoseDisps.size();i+=2) {

            //printf( "vPoseDisps = %s", vPoseDisps[i].ToString() );
            //printf( "vPoseDisps = %s", vPoseDisps[i+1].ToString() );            
			
            // find the index of the displaced parameter
            int j=0;
            while(vPoseDisps[i][j]==0) { ++j; }                                               
       
            // do negative displacement 
			PoseDisplacement( image, m_Shapes[nShape], cVectors[nShape], 
                              vPoseDisps[i], dg1 );

            // do positive displacement 
            PoseDisplacement( image, m_Shapes[nShape], cVectors[nShape], 
                              vPoseDisps[i+1], dg2 );            
        
            // form central difference
            cDiff = (dg2-dg1)/(vPoseDisps[i+1][j]-vPoseDisps[i][j]);            
              
            // accumulate the results into the j-th column
            Gpose.Col( j, Gj );
            Gj += cDiff;
			Gpose.SetColumn( j, Gj );    
			++nExperiment;	
           
            // increment the normalisation factor
            normFactors[j] = normFactors[j] + 1;    

		}
        printf( "Experiment %i of %i done (pose)...\n", 2*nExperiment, 2*totalExp );
	}


    // normalize
    for(int j=0;j<Gpose.NCols();j++) {

        Gpose.Col( j, Gj );
        Gj /= normFactors[j];
	    Gpose.SetColumn( j, Gj );
    }           
}
   

/**

  @author   Mikkel B. Stegmann
  @version  7-18-2002

  @memo     Estimates the Jacobian of the model parameters.

  @doc      Estimates the Jacobian of the model parameters given a set
			of displacement vectors and the optimum model parameters for
			the training set.

  @see      EstPoseGradientMatrix
  
  @param    vCDisps		A vector of displacement vectors as obtained from
						CParamDispVectors() or DisplacementSets().

  @param    cVectors	The set of optimum c vectors for the training examples.

  @param	Gparam		The output Jacobian matrix (or gradient matrix if you like).

  @param	ts_subsampling
						Subsampling factor, i.e. ts_subsampling==n will carry out
						displacements on every n-th example in the training set.
  
  @return	Nothing. 
  
*/
void CAAMBuilder::EstCParamGradientMatrix( const std::vector<CDVector> &vCDisps, 
                                           const std::vector<CDVector> &cVectors, 
                                           CDMatrix &Gparam,
                                           const int ts_subsampling) const {


    // setup
    int nShapes = m_Shapes.NShapes();    

    // determine subsampling of the training set
    int nbShapeSamples = nShapes%ts_subsampling ? 
                         nShapes/ts_subsampling + 1 : nShapes/ts_subsampling;
    int totalExp = vCDisps.size()/2.*nbShapeSamples;
    int nExperiment = 0;    

    int nParam = vCDisps[0].Length();
    Gparam.Resize( m_pModel->m_iTextureSamples, nParam );        
    CDVector dg1, dg2, cDiff, Gj(m_pModel->m_iTextureSamples);
    CDVector normFactors(nParam);


    // reset gradient matrix
    Gparam = .0;
    

    // for each training example in the (subsampled) training set     	
	for(int nShape=0;nShape<nShapes;nShape+=ts_subsampling) {

        // get the shape image
        CDMultiBand<TAAMPixel> image; 
        m_Shapes[nShape].GetHostImage( image, m_Shapes.Path(), 
                                       m_pModel->m_iModelReduction );
		
		for(unsigned int i=0;i<vCDisps.size();i+=2) {

            //printf( "\nvCDisps = %s", vCDisps[i].ToString() );
            //printf( "vCDisps = %s", vCDisps[i+1].ToString() );

            // find the index of the displaced parameter
            int j=0;
            while(vCDisps[i][j]==0) { ++j; }

			// do negative displacement 
			ModelDisplacement( image, m_Shapes[nShape], cVectors[nShape], 
                               vCDisps[i], dg1 );

            // do positive displacement 
            ModelDisplacement( image, m_Shapes[nShape], cVectors[nShape], 
                               vCDisps[i+1], dg2 );           

            // form central difference
            cDiff = (dg2-dg1)/(vCDisps[i+1][j]-vCDisps[i][j]);            

			// accumulate the results into the j-th column
            Gparam.Col( j, Gj );
            Gj += cDiff;
			Gparam.SetColumn( j, Gj );			
			++nExperiment;			

            // increment the normalisation factor
            normFactors[j] = normFactors[j] + 1;            
		}
        printf( "Experiment %i of %i done (c)...\n", 2*nExperiment, 2*totalExp );
	}


    // normalize
    for(int j=0;j<Gparam.NCols();j++) {

        Gparam.Col( j, Gj );
        Gj /= normFactors[j];
	    Gparam.SetColumn( j, Gj );
    }    
}