/* $Id: AAMMovie.cpp,v 1.2 2003/01/14 17:28:51 aam Exp $ */
#include "AAMMovie.h"
#include <io.h>
#include <vfw.h>

// import library (thus avoiding to tamper with the configuration settings)
#pragma comment( lib, "vfw32.lib" )

#define AAM_VIS_STRM_CHKHR(h, msg)  { HRESULT hr = h;\
                                  if( FAILED(hr) )\
                                      return HandleError( msg, __FILE__, __LINE__, hr);\
                                }


/**

  @author   Mikkel B. Stegmann
  @version  9-20-2000

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
CAAMMovieAVI::CAAMMovieAVI( int framesPerSecond ) {

	m_framesPerSecond = framesPerSecond;
	m_fShowCompressionDialog = false;

    m_iRefCount   = 1;

    m_paviFile    = NULL;
    m_paviStream  = NULL;
    m_paviCStream = NULL;
    m_pgfFrame    = NULL;

    m_pData          = NULL;
    m_bOnCreateFirst = FALSE;

    m_pzFileName  = NULL;
    m_uMode       = 0;
    m_uFrameCount = 0;
    m_iHeight     = 0;
    m_uWidth      = 0;
    m_ePixFmt     = evispixfmtNil;
	m_uPixelByteCount = 0;

    m_DoFlip = true;

    AVIFileInit();
}



CAAMMovieAVI::~CAAMMovieAVI()
{
    Close();

    AVIFileExit();
}

int
CAAMMovieAVI::AddRef()
{
    return InterlockedIncrement( &m_iRefCount );
}

int
CAAMMovieAVI::Release()
{
    int iNewCount = InterlockedDecrement( &m_iRefCount );
    if ( iNewCount == 0 )
    {
        delete this;
    }
    return iNewCount;
}

BOOL
CAAMMovieAVI::OpenPgfFrame( BITMAPINFO *pbmi )
{
    assert(m_paviStream);
    
    // request 32bpp first
    // if that fails request 24bpp if that fails give up
    for ( m_uPixelByteCount = 4; m_uPixelByteCount >= 3; m_uPixelByteCount-- )
    {
        pbmi->bmiHeader.biBitCount  = m_uPixelByteCount * 8;
        pbmi->bmiHeader.biSizeImage = m_uWidth * m_iHeight * m_uPixelByteCount;
        
        m_pgfFrame = AVIStreamGetFrameOpen( m_paviStream, &(pbmi->bmiHeader) );
        
        if ( m_pgfFrame )
        {
            break;
        }
        
        if ( m_uPixelByteCount == 3 )
        {
            return HandleError("Can't open avi frames", __FILE__, __LINE__, E_FAIL);
        }
    }
    return TRUE;
}

BOOL
CAAMMovieAVI::Open( const char* filename, UINT open_flags )
{
    Close();

    // Open the AVI file
    m_uMode   = open_flags;
    AAM_VIS_STRM_CHKHR( AVIFileOpen( &m_paviFile, filename, open_flags, NULL ), 
                    "AviFileOpen failed");

    m_pzFileName = strdup(filename);
    if ( m_pzFileName == NULL )
    {
        return HandleError("Out of memeory", __FILE__, __LINE__,
                           E_OUTOFMEMORY);
    }

    m_ePixFmt = evispixfmtRGBAByte;

    BOOL bFileExists = _access(filename,0) != -1;

    if ( bFileExists && !( open_flags & OF_CREATE ) )
    {
        // Open the first video stream in the file
        AAM_VIS_STRM_CHKHR( AVIFileGetStream( m_paviFile, &m_paviStream, streamtypeVIDEO, 0 ),
                        "AviFileGetStream failed");

        m_uFrameCount = AVIStreamLength(m_paviStream);

        BITMAPINFO bmi;
    
        long bmi_size = sizeof(bmi);
        AVIStreamReadFormat(m_paviStream, 0, &bmi, &bmi_size);

        // mbs
        if (bmi.bmiHeader.biHeight<0) {

            m_DoFlip = false;
        }            

        m_iHeight = abs(bmi.bmiHeader.biHeight);
        m_uWidth  = bmi.bmiHeader.biWidth;
    
        // open the avi video frames
        bmi.bmiHeader.biPlanes      = 1;
        
		// MBS bmi.bmiHeader.biHeight      = -m_iHeight;     // first-row-first is desired
		bmi.bmiHeader.biHeight      = m_iHeight;     // first-row-first is desired
	        
		bmi.bmiHeader.biCompression = BI_RGB;

		if( !OpenPgfFrame( &bmi) )
        {
            return FALSE;
        }

        m_bOnCreateFirst = FALSE;
    }
    else
    {
        AVISTREAMINFO      strhdr;
        AVICOMPRESSOPTIONS opts[1];
        AVICOMPRESSOPTIONS *aopts = &opts[0];

        ZeroMemory(&strhdr,sizeof(strhdr));
        strhdr.fccType    = streamtypeVIDEO;
        strhdr.fccHandler = 0;
        strhdr.dwScale    = 1;
        strhdr.dwRate     = m_framesPerSecond;
        AAM_VIS_STRM_CHKHR( AVIFileCreateStream( m_paviFile, &m_paviStream, &strhdr ),
                        "Can't create avi stream" );

		if (m_fShowCompressionDialog) {
			
			AVISaveOptions( NULL, 0, 1, &m_paviStream, &aopts);

		} else {
	
			// MBS Set up 'full frames (no compression)'
			opts[0].fccType = streamtypeVIDEO;
			opts[0].fccHandler = mmioFOURCC('D','I','B',' ');
			opts[0].dwKeyFrameEvery = 1;
			opts[0].dwQuality = 100;
			opts[0].dwBytesPerSecond = 0;
			opts[0].dwFlags = 0;
			opts[0].lpFormat = 0;
			opts[0].cbFormat = 0;
			opts[0].lpParms = 0;
			opts[0].cbParms = 0;
			opts[0].dwInterleaveEvery = 0;
		}

        AAM_VIS_STRM_CHKHR( AVIMakeCompressedStream( &m_paviCStream, m_paviStream, opts, NULL ),
                        "Can't make compressed stream" );

        m_bOnCreateFirst = TRUE;
    }

    m_uCurFrame = 0;

    return TRUE;
}

BOOL
CAAMMovieAVI::ReadFrame( CVisImageBase& img, INT frame_num )
{
    assert(m_pgfFrame);

    // UNDONE mattu: should this handle resizing to incoming image size
    if ( img.IsValid() &&
         (img.Height() != m_iHeight ||
          img.Width()  != m_uWidth) )
    {
        return HandleError("Invalid input image", __FILE__, __LINE__,
                           E_INVALIDARG);
    }

    if ( frame_num == -1 )
    {
        frame_num = m_uCurFrame;
    }    

    if ( (UINT)frame_num >= m_uFrameCount )
    {
        frame_num = m_uFrameCount - 1;
    }
    

    LPBITMAPINFO pBMI = (LPBITMAPINFO)AVIStreamGetFrame( m_pgfFrame, frame_num );

    BYTE* pDataSrc = (BYTE *)(sizeof(BITMAPINFO) + (BYTE*)pBMI);   

    // unfortunately some avi decoders won't supply 32bpp so we have
    // to deal with the 24bpp case
    if ( m_uPixelByteCount == 3 )
    {
        if( m_pData == NULL )
        {
            m_pData = new BYTE[m_uWidth * m_iHeight * 4];
            if ( m_pData == NULL )
            {
                return HandleError("Out of memory", __FILE__, __LINE__,
                                   E_OUTOFMEMORY);
            }
        }

        unsigned * pDataDst = (unsigned*)m_pData;
        for ( UINT y = 0; y < (UINT)m_iHeight; y++ )
        {
            BYTE* pDataSrcNext = pDataSrc + 3 * m_uWidth;
            for ( UINT x = 0; x < m_uWidth; x++ )
            {
                *pDataDst = (0xFF << 24) | (pDataSrc[1] << 16) |
                            (pDataSrc[0] << 8 ) | pDataSrc[2];
                pDataDst++;
                pDataSrc += 3;
            }
            pDataSrc = pDataSrcNext;
        }

        pDataSrc = m_pData;
    }
	else
	{
		// annoying check for all 0 in the alpha channel.  If all-zero
		// is detected this is probably a defect in the codec supplying 
		// the frame, in this case the alpha should be reset to 0xFF.
		BOOL bNonZeroAlpha = FALSE;

        unsigned* pDataDst = (unsigned*)pDataSrc;
        for ( UINT xy = 0; xy < m_uWidth*m_iHeight; xy++, pDataDst++ )
        {
			if( *pDataDst & (0xFF<<24) )
			{
				bNonZeroAlpha = TRUE;
				break;
			}
        }

		if( !bNonZeroAlpha )
		{
			pDataDst = (unsigned*)pDataSrc;
			for ( xy = 0; xy < m_uWidth*m_iHeight; xy++, pDataDst++ )
			{
				*pDataDst |= 0xFF<<24;
			}
		}
	}

    CVisRGBAByteImage tmpImage(m_uWidth, m_iHeight, 1, evisimoptDefault,
                               pDataSrc);
      
    img.Allocate(m_uWidth, m_iHeight);
    if( !tmpImage.CopyPixelsTo(img) )
    {        
        return HandleError("CopyPixelsTo failed", __FILE__, __LINE__,
                           E_FAIL);
    }

    // do flip
    BYTE* pDataSrc2 = img.PbPixel(0,0);
    if (m_DoFlip) {
        
        int w = m_uWidth*4;        
        unsigned char *scanline = new unsigned char[w];        
        for(int y=0;y<m_iHeight/2;y++) {
            
            memcpy( scanline, pDataSrc2+y*w, w );
            memcpy( pDataSrc2+y*w, pDataSrc2+(m_iHeight-y-1)*w, w );            
            memcpy( pDataSrc2+(m_iHeight-y-1)*w, scanline, w );                        
        }
        delete[] scanline;        
    } 
    
    m_uCurFrame = frame_num + 1;
    
    return TRUE;
}

BOOL
CAAMMovieAVI::WriteFrame( CVisImageBase& img, INT frame_num )
{
    if ( img.PixFmt() != evispixfmtRGBAByte )
    {
        return HandleError("Incorrect pixel type", __FILE__, __LINE__,
                           E_INVALIDARG);
    }

    if ( frame_num == -1 )
    {
        frame_num = m_uCurFrame;
    }

    if( (UINT)frame_num > m_uFrameCount )
    {
        m_uFrameCount = frame_num;
    }

    if ( m_bOnCreateFirst )
    {
        m_iHeight = img.Height();
        m_uWidth  = img.Width();
        m_ePixFmt = evispixfmtRGBAByte;

        BITMAPINFO    bmi;
        ZeroMemory( &bmi, sizeof(bmi) );
        bmi.bmiHeader.biSize         = sizeof(BITMAPINFOHEADER);
        // MBS bmi.bmiHeader.biHeight       = -m_iHeight;
		bmi.bmiHeader.biHeight       = m_iHeight;
        bmi.bmiHeader.biWidth        = m_uWidth;
        bmi.bmiHeader.biPlanes       = 1;
        bmi.bmiHeader.biBitCount     = 32;
        bmi.bmiHeader.biCompression  = BI_RGB;

        AAM_VIS_STRM_CHKHR( AVIStreamSetFormat( m_paviCStream, 0, &bmi, sizeof(bmi) ),
                        "Can't set avi stream format" );

        if( (m_uMode & OF_READWRITE) && !m_pgfFrame)
        {
            if( !OpenPgfFrame( &bmi) )
            {
                return FALSE;
            }
        }
        m_bOnCreateFirst = FALSE;
    }
    else
    {
        if ( img.Height() != m_iHeight ||
             img.Width()  != m_uWidth )
        {
            return HandleError("All images must be the same size", __FILE__, __LINE__,
                               E_INVALIDARG);
        }
    }

    unsigned* pData;

    // don't do copy if bytes are already contiguous in the image

	// MBS if ( m_uWidth == img.MemoryWidth() )
    if ( m_uWidth == img.MemoryWidth() && !m_DoFlip ) // MBS
    {
        pData = (unsigned*)img.PbPixel(0,0);
    }
    else
    {
        if( m_pData == NULL )
        {
            // create a buffer in the format that AVIStreamWrite likes
            m_pData = new BYTE[m_uWidth * m_iHeight * 4];
            if ( m_pData == NULL )
            {
                return HandleError("Out of memory", __FILE__, __LINE__,
                                   E_OUTOFMEMORY);
            }
        }

        pData = (unsigned*)m_pData;

        // MBS
        if (m_DoFlip) {

            // MBS
            for ( UINT y = 0; y < (UINT)m_iHeight; y++ ) {
                for ( UINT x = 0; x < m_uWidth; x++ ) {
                    				    
				    *pData++ = *((unsigned*)img.PbPixel(x,m_iHeight-y-1));
                }
            }
        } else {
            
            for ( UINT y = 0; y < (UINT)m_iHeight; y++ ) {
                for ( UINT x = 0; x < m_uWidth; x++ ) {
                                        
                    *pData++ = *((unsigned*)img.PbPixel(x,m_iHeight-y-1));
                }
            }
        }
        pData = (unsigned*)m_pData;
    }

    AAM_VIS_STRM_CHKHR( AVIStreamWrite( m_paviCStream, frame_num, 1, pData,
                                    m_uWidth * m_iHeight * 4,
                                    AVIIF_KEYFRAME, NULL, NULL ),
                    "Write to AVI stream failed" );

    m_uCurFrame = frame_num + 1;

    return TRUE;
}

void
CAAMMovieAVI::Close()
{   
    if ( m_pzFileName )
    {
        free( (void*)m_pzFileName );
        m_pzFileName = NULL;
    }

    m_uMode       = 0;
    m_uFrameCount = 0;
    m_iHeight     = 0;
    m_uWidth      = 0;
    m_ePixFmt     = evispixfmtNil;
	m_uPixelByteCount = 0;

    if ( m_pData )
    {
        delete [] m_pData;
        m_pData = NULL;
    }

    if ( m_pgfFrame )
    {
        AVIStreamGetFrameClose( m_pgfFrame );
        m_pgfFrame = NULL;
    }

    if ( m_paviCStream )
    {
        AVIStreamRelease( m_paviCStream );
        m_paviCStream = NULL;
    }

    if ( m_paviStream )
    {
        AVIStreamRelease( m_paviStream );
        m_paviStream = NULL;
    }

    if ( m_paviFile )
    {
        AVIFileClose(m_paviFile);
        m_paviFile = NULL;
    }
}

const char* CAAMMovieAVI::FileName(void) const
{
    return m_pzFileName;
}

UINT CAAMMovieAVI::Mode(void) const
{
    return m_uMode;
}

int CAAMMovieAVI::FrameCount(void) const
{
    return m_uFrameCount;
}

UINT CAAMMovieAVI::Width(void) const
{
    return m_uWidth;
}

int CAAMMovieAVI::Height(void) const
{
    return m_iHeight;
}

EVisPixFmt CAAMMovieAVI::PixFmt(void) const
{
    return m_ePixFmt;
}



/**

  @author   Mikkel B. Stegmann
  @version  7-22-2000

  @memo     Writes one image as a still in a movie.

  @doc      Writes one image as a still in a movie given a
			certain duration.
    
  @param	image		An RGBA image.  
  @param    duration	Duration in ms.  
  
  @return   Nothing.
  
*/
void CAAMMovieAVI::WriteStill(	CVisImage<CVisRGBABytePixel> image,									
								const int duration /* in ms */  ) {

	double frameDuration = 1000.0/m_framesPerSecond;
	int NFrames = (int)ceil(duration/frameDuration);
    for(int i=0;i<NFrames;i++) {

		this->WriteFrame( image );		
    }
}


/**

  @author   Mikkel B. Stegmann
  @version  7-22-2000

  @memo     Writes one image as a still in a movie.

  @doc      Writes one image as a still in a movie given a
			certain duration.
    
  @param	image		A grayscale image. 
  
  @param    duration	Duration in ms.
  
  @return   Nothing.
  
*/
void CAAMMovieAVI::WriteStill(  CDMultiBand<TAAMPixel> image,						
						        const int duration /* in ms */ ) {

	// convert to rgba
	CVisImage<CVisRGBABytePixel> rgba( image.Width(), image.Height() );
	image.CopyPixelsToRGBA( rgba );
	this->WriteStill( rgba, duration );
}


void CAAMMovieAVI::XFade(   CVisImage<CVisRGBABytePixel> image1,
				            CVisImage<CVisRGBABytePixel> image2,				   
				            const int duration /* in ms */  ) {	

	int w=image1.Width();
	int h=image1.Height();

	CVisImage<CVisRGBABytePixel> frame( w, h, 1, evisimoptDontAlignMemory);

	double frameDuration = 1000.0/m_framesPerSecond;
	int NFrames = (int)ceil(duration/frameDuration);	
	for(int i=0;i<NFrames;i++) {

		double d = i/(NFrames-1.0);

		for(int y=0;y<h;y++) {

			for(int x=0;x<w;x++) {
				frame.Pixel(x,y) = (1.0-d)*image1.Pixel(x,y)+
										 d*image2.Pixel(x,y);
			}
		}		
		this->WriteFrame( frame );		
	}
}
