/* $Id: pbuffer.h,v 1.1.1.1 2003/01/03 19:17:13 aam Exp $ */
#ifndef PBUFFERS_H
#define PBUFFERS_H

#include <windows.h>
#include "GL/gl.h"
#include "GL/wglext.h"
#include "GL/glext.h"

#define MAX_PFORMATS 256
#define MAX_ATTRIBS  32

// Display mode bit masks.
#define MODE_RGB			0
#define MODE_RGBA			GLUT_RGB
#define MODE_INDEX			1
#define MODE_SINGLE			0
#define MODE_DOUBLE			2
#define MODE_ACCUM			4
#define MODE_ALPHA			8
#define MODE_DEPTH			16
#define MODE_STENCIL		32 

class PBuffer
{
private:
    HDC          myDC;      // Handle to a device context.
    HGLRC        myGLctx;   // Handle to a GL context.
    HPBUFFERARB  buffer;    // Handle to a pbuffer.
    unsigned int mode;      // Flags indicating the type of pbuffer.
public:
    int          width;
    int          height;
    PBuffer( int width, int height, unsigned int mode );
    ~PBuffer();
    void HandleModeSwitch();
    void MakeCurrent();
    void Initialize(bool share = false );
};

#endif