#include "AAMModelMS.h"
#include "AAMBuilder.h"
#include "AAMInitialize.h"
#include "AAMVisualizer.h"


/**

  @author   Mikkel B. Stegmann
  @version  12-6-2002

  @memo     Default multi-scale constructor.

  @doc      Default multi-scale constructor.
  
  @return   Nothing.
  
*/
CAAMModelMS::CAAMModelMS() {

	CAAMModel::CAAMModel();
}



/**

  @author   Mikkel B. Stegmann
  @version  12-6-2002

  @memo     multi-scale destructor.

  @doc      multi-scale destructor.
  
  @return   Nothing.
  
*/
CAAMModelMS::~CAAMModelMS() {

	// delete the scale images
	for(int i=1;i<m_vImagePyr.size();i++) {	// notice that level 0 is handled elsewhere...

		delete m_vImagePyr[i];
	}
}



/**

  @author   Mikkel B. Stegmann
  @version  4-14-2000

  @memo     Diver method for model generation.

  @doc      This method automates the model generation as much as possible
			by using the various class methods for all the sequences in the
			task of producing a model.           

  @param    nLevels         The number of levels the multi-scale AAM should be in.
                            Default 3.

  @param    inDir			Input directory where annotations (.asf) resides.

  @param    acf				Filename of an AAM configuration file. 
							If omitted defaults are used.

  @param    modelReduction  Model reduction multiplier. Default off == 1.                            
                            This sets the size of the lowest level in the 
							model pyramid, i.e. level 0.

  @param    excludeShape    Excludes one shape number 'excludeShape'
                            from the input directory. Default -1, i.e.
                            no shapes are removed.

                            Used to perform leave-one-out testing.
  
  @return   Nothing.
  
*/
void CAAMModelMS::BuildAllLevels( const int nLevels,
                                  const CString &inDir, 
                                  const CString &acf, 
                                  const int modelReduction,
                                  const int excludeShape ) {  
    
    std::vector<CString> asfFiles;    
    asfFiles = CAAMUtil::ScanSortDir( CAAMUtil::AddBackSlash( inDir ), "asf" );
    this->BuildAllLevels( nLevels, asfFiles, acf, modelReduction, excludeShape );
}



/**

  @author   Mikkel B. Stegmann
  @version  12-6-2002

  @memo     Diver method for model generation.

  @doc      This method automates the model generation as much as possible
			by using the various class methods for all the sequences in the
			task of producing a model.           

  @param    nLevels         The number of levels the multi-scale AAM should be in.
                            Default 3.

  @param    asfFiles        Vector of asf filenames.

  @param    acf				Filename of an AAM configuration file. 
							If omitted defaults are used.

  @param    modelReduction  Model reduction multiplier. Default off == 1.                            
                            This sets the size of the lowest level in the 
							model pyramid, i.e. level 0.

  @param    excludeShape    Excludes one shape number 'excludeShape'
                            from the input directory. Default -1, i.e.
                            no shapes are removed.

                            Used to perform leave-one-out testing.  
  
  @return   Nothing.
  
*/
void CAAMModelMS::BuildAllLevels(	const int nLevels,
									const std::vector<CString> &asfFiles,
									const CString &acf, 
									const int modelReduction,
									const int excludeShape ) {

    int reductionMultiplier = modelReduction;

    // delete any old models
    m_vModels.clear();
	CAAMModel emptyModel;
	*(CAAMModel*)this = emptyModel;

    for(int i=0;i<nLevels;i++) {

        printf("\n*** Building model for level %i/%i in the multi-scale AAM (re=%i). ***\n", 
				i+1, nLevels, reductionMultiplier );

        // build model and add to the level list
        CAAMBuilder builder;
        CAAMModel *pModel = NULL;

		if (i==0) {

			// we're at the lowest level
			pModel = this;
		} else {

			// any higher level
			pModel = new CAAMModel;
		}

        builder.BuildFromFiles( *pModel, asfFiles, acf, reductionMultiplier, excludeShape );
		
		if (i>0) {

			m_vModels.push_back( *pModel ); // add a copy to the list
		}

		// delete model
		if (pModel!=this) { 

			delete pModel;
		}      

		// move one level up in the scale pyramid -- i.e. scale by 50%
        reductionMultiplier *=2; 
    }
}



/**

  @author   Mikkel B. Stegmann
  @version  12-6-2002

  @memo     Writes the complete multi-scale AAMModel to disk as a set of .txt and an .amf files.

  @doc      Writes the complete multi-scale AAMModel to disk as a set of .txt and an .amf files.

  @see      ReadModel
  
  @param    filename    Output filename without any extension. Multi-scale prefixes will be added.

  @param    txt_only    If true binary model data is not written.
  
  @return   true on success, false on file errors.

*/
bool CAAMModelMS::WriteModel( const CString &filename, const bool txt_only ) const {

    char modelname[512];
	bool res = true;

    for(unsigned int i=0;i<NLevels() && res;i++) {

        sprintf(modelname, "%s_msl%02i", filename, i );
		res = Model(i).CAAMModel::WriteModel( modelname, txt_only );
    }

	return res;
}



/**

  @author   Mikkel B. Stegmann
  @version  12-6-2002

  @memo     Reads the complete AAMModel from disk.

  @doc      Reads the complete AAMModel from disk.

  @see		WriteModel 
  
  @param    filename	Input filename without any extension and scale prefixes.

						E.g. if the files on disk are
						
							'model_msl<level>.txt' & 'model_msl<level>.amf' 
						  
						-> filename = 'model'
  
  @return   true on success, false on file errors.
  
*/
bool CAAMModelMS::ReadModel( const CString &basename ) {

    // delete any old models
    m_vModels.clear();

    char modelname[512];
    bool result = true; 
    int i = 0;    
    while(result) {
        
        sprintf(modelname, "%s_msl%02i", basename, i );
        CAAMModel model;

        printf("Trying to read model '%s'... ", modelname );
        result = model.ReadModel( modelname );
        
        if (result) {

			if (i==0) {

				*(CAAMModel*)this = model;
			} else {

				m_vModels.push_back( model );
			}
        } 
        printf("%s", result ? "succes.\n" : "failure.\n" );

		++i;
    }
	printf("\nMulti-scale AAM containing %i levels succesfully read.\n", --i );

	return i>0;

    
}



/**

  @author   Mikkel B. Stegmann
  @version  12-6-2002

  @memo     Performs AAM optimization of a shape (using a model pyramid).
  
  @doc      Performs AAM optimization of a shape containing initial 
			pose using a model pyramid.
  
  @param    image		The image to search in (in size corresponding to level zero).

  @param	s			The initial shape (also containing the inital pose, thus;
						not a normalized shape). Actually only the pose of 's' is 
						used (to align the reference shape as the initial shape).

						NOTE: The optimal shape is returned in 's' after execution.

						NOTE: 's' is defined at level 0, i.e. shapes should have the
						      same size as when calling CAAMModel::OptimizeModel()
							  directly.

  @param    c			The optimal model parameters at level zero. Unlike
						CAAMModel::OptimizeModel() the content of this vector is not
						used, since the optimisation is started the smallest (i.e. top)
						level of the pyramid.
				
                        NOTE: The optimal model parameters are returned in 'c' after 
						execution. 

  @param	maxIterations The maximum iterations allowed at each level.

  @param    pOptStates  Optional parameter convergence info (at level zero)
						can be returned in. See CAAMOptState.
  
  @param    disableDamping Disables the damping steps (default false).

  @return   The results of the optimization in the form of a 'CAAMOptRes' 
			instance.

*/
CAAMModel::CAAMOptRes CAAMModelMS::OptimizeModel(	 const CDMultiBand<TAAMPixel> &image,
													 CAAMShape &s, CDVector &c,	
													 const int maxIterations, 
													 std::vector<CAAMOptState> *pOptStates,
													 bool disableDamping ) const {

    CAAMModel::CAAMOptRes res;

    int st_level = NLevels()-1;  // start at the smallest level
								 // i.e. the top of the pyramid

	// setup
	bool writeLevelResults = false;

	// make scaled images
	BuildPyr( image );

	// scale shape to fit lowest level
	s.Scale( 1./pow( 2., NLevels()-1 ), false );


	// do the optimisation
    for(int i=st_level;i>=0;i--) {
      
        if (i!=st_level) {

            // size the last result around (0,0)
            s.Scale( 2., false ); 			

			bool project = true;
			if (project && CAAMUtil::ShapeInsideImage(s, *m_vImagePyr[i]) ) {

				
				// sample texture under shape and project into the current model
				// (this sample should really be used to start the OptimizeModel 
				//  below, otherwise we're doing the same work twice.... )
				Model(i).Shape2Combined( s, *m_vImagePyr[i], c );
			} else {

				// reset
				c.Resize( Model(i).CombinedPCA().NParameters() );
				c = 0;
			}
        }
		//printf("Optimising at level %i...\n", i );


        // optimize model
        res = Model(i).CAAMModel::OptimizeModel( *m_vImagePyr[i], s, c, 
												 maxIterations, pOptStates, 
												 disableDamping );        

		// write results for this level
        if ( writeLevelResults ) {  
			
			CString fn;	
			fn.Format( "level%02i_opt.bmp", i );
			CAAMVisualizer::ShapeStill(  *m_vImagePyr[i], s, fn );
		}
    }

    return res;
}



/**

  @author   Mikkel B. Stegmann
  @version  12-6-2002

  @memo     Builds an image pyramid (if its not cached beforehand).

  @doc      Builds an image pyramid (if its not cached beforehand).
			The pyramid is stored in m_VImagePyr.
  
  @param    image	Input image that should be convertet to a pyramid.
					This image is assumed to live to the *complete*
					usage of the pyramid, since level 0 is a pointer
					to this image.
 
  @return   Nothing.
  
*/
void CAAMModelMS::BuildPyr( const CDMultiBand<TAAMPixel> &image ) const {

	bool cachePyr = m_vImagePyr.size()>0			&&
					m_vImagePyr.size()==NLevels()	&& 
					image.Name()==m_vImagePyr[0]->Name();

	// non-const this for caching
	CAAMModelMS *thisNC = const_cast< CAAMModelMS* >(this);
	
	if ( !cachePyr ) {

		//printf("Can't use cache. Building image pyr.\n");

		// make scaled images
		thisNC->m_vImagePyr.clear();
		thisNC->m_vImagePyr.resize( NLevels() );
		thisNC->m_vImagePyr[0] = & const_cast< CDMultiBand<TAAMPixel>& >(image);
		for(int i=1;i<NLevels();i++) {

			CDMultiBand<TAAMPixel> *pImage = new CDMultiBand<TAAMPixel>;
			*pImage = *m_vImagePyr[i-1]; // silly copy only needed due to the design of reducepyr :-(       

			pImage->ReducePyr( 2 );	// reduce image by 50%
			thisNC->m_vImagePyr[i] = pImage;	// add image
		}
	}
}