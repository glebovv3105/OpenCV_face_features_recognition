function type=getasfpathtype( path_no, P )
%
% function [xp,yp]=getasfpath( i, P )
%
% Returns type of the i-th path.
%
% NOTE: Path numbers are zero-based.
%
% $Id: getasfpathtype.m,v 1.1.1.1 2003/01/03 19:18:51 aam Exp $ 
%
[nbpoints d]=size(P);
for i=1:nbpoints
	path = P(i,1);
	if path==path_no
	
		type = P(i,2);		
		break;
	end
end