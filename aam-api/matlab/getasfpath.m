function [xp,yp]=getasfpath( path_no, P )
%
% function [xp,yp]=getasfpath( i, P )
%
% Returns the i-th path number as an x and an y-vector from
% the asf point matrix P.
%
% NOTE: Path numbers are zero-based.
%
% $Id: getasfpath.m,v 1.1.1.1 2003/01/03 19:18:51 aam Exp $ 
%
[nbpoints d]=size(P);
pc = 1;
for i=1:nbpoints

	path = P(i,1);
	if path==path_no
	
		xp(pc) = P(i,3);
		yp(pc) = P(i,4);
		pc = pc+1;
	end
end