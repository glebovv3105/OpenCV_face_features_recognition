function writeasf( P, hostimage, filename );
%
% function writeasf( P, hostimage, filename )
%
% Writes an asf file from a point matrix P and a host image - 
% i.e. the image that the shape belongs to.
%
% $Id: writeasf.m,v 1.1.1.1 2003/01/03 19:18:51 aam Exp $ 
%
[nbpoints d] = size(P);
eval(sprintf('fid=fopen(''%s'', ''wt'');', filename));
fprintf(fid,'######################################################################\n');
fprintf(fid,'#\n');
fprintf(fid,'#    AAM Shape File  -  written: %s\n', datestr(now));
fprintf(fid,'#\n');
fprintf(fid,'######################################################################\n');
fprintf(fid,'\n');
fprintf(fid,'#\n');
fprintf(fid,'# number of model points\n');
fprintf(fid,'#\n');
fprintf(fid,'%i\n',nbpoints);    
fprintf(fid,'\n');
fprintf(fid,'#\n');
fprintf(fid,'# model points\n');
fprintf(fid,'#\n');
fprintf(fid,'# format: <path#> <type> <x rel.> <y rel.> <point#> <connects from> <connects to>\n');
fprintf(fid,'#\n');
for j=1:nbpoints
    fprintf(fid,'%i\t%i\t%.8f\t%.8f\t%i\t%i\t%i\n', P(j,1), P(j,2), P(j,3), P(j,4), P(j,5), P(j,6), P(j,7) );                  
end
fprintf(fid,'\n');
fprintf(fid,'#\n');
fprintf(fid,'# host image\n');
fprintf(fid,'#\n');
fprintf(fid,'%s\n',hostimage);  
fclose(fid);