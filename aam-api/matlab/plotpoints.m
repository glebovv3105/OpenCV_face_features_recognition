function plotpoints( P, w, h, hi, line_fmt, ptnb ) 
%
% function plotpoints( P, w, h, hi, line_fmt, ptnb ) 
%
% Plots the shape points from the point matrix P
% (obtained from e.g. reafasf()) into a figure.
%
% X points are scaled by w (ignored if a host image is given)
% Y points are scaled by h (ignored if a host image is given)
%
% 'hi' is an optional host image name that will be loaded
% and layered below the shape. Use an empty string to
% disable this feature.
%
% 'line_fmt' is the line format string used in the plot call.
% If empty 'b*-' is used.
%
% Set 'ptnb' == 1 for point numbering, use '0' otherwise.
%
% $Id: plotpoints.m,v 1.1.1.1 2003/01/03 19:18:51 aam Exp $ 
%
global AAM_DEFAULT_LINE_WIDTH
if 0==exist('AAM_DEFAULT_LINE_WIDTH') | isempty(AAM_DEFAULT_LINE_WIDTH)
	
	AAM_DEFAULT_LINE_WIDTH = .5;
end
AAM_DEFAULT_LINE_WIDTH = 1;

nbpaths=max(P(:,1)+1);
hold on;	
axis equal;
axis ij;
if length(hi)>0

	% load and plot hostimage
	[img,cmap] = imread( hi );
	image( img );
	colormap( cmap );	
	s=size(img);
	h=s(1);		
	w=s(2);
else 
	axis off;
end
axis image;	
pn=1;

%%%%%%%%%%%%%%%%%%%%%
% setup plot style
%%%%%%%%%%%%%%%%%%%%%
if isempty(line_fmt)==1 
	line_fmt = 'b*-';
end	
for p=0:nbpaths-1;
	[x,y]=getasfpath( p, P );
	type = getasfpathtype( p, P );
	isOpenPath = bitand( type, 4 ) > 1;
	x=x*w;
	y=y*h;
	if isOpenPath
		plot( x, y, line_fmt, 'LineWidth', AAM_DEFAULT_LINE_WIDTH );
	else			
		plot([x x(1)], [y y(1)], line_fmt, 'LineWidth',  AAM_DEFAULT_LINE_WIDTH );
		%plot([x x(1)], [y y(1)], 'b-', 'LineWidth',  AAM_DEFAULT_LINE_WIDTH );
		%plot([x x(1)], [y y(1)], 'r*', 'LineWidth',  AAM_DEFAULT_LINE_WIDTH );
	end		
	
	if ptnb
		for i=1:length(x)		
			eval(sprintf('text_handle=text(x(i),y(i),''  %d'');',pn));
			set(text_handle,'Color',[1 1 1])
			pn=pn+1;
		end	
	end
end	
% imwrite(img, 'img_ann.bmp', 'bmp');