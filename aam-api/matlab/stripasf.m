function shapes = stripasf( width, height )
%
% function shapes = stripasf( width, height )
%
% strips the shape points of the *.asf files 
% in the current directory into a <nb. points * 2> x < nb. shape files>
% matrix that is written to 'shapes.txt' (separated with spaces)
%
% width and height is the size of the hostimages
% (needed in order to give the points the right aspect)
%
% $Id: stripasf.m,v 1.2 2003/01/15 11:47:25 aam Exp $ 
%
dirlist=dir('*.asf');
aspect = width/height;

shapes = [];
for i=1:length(dirlist)

	P = readasf( dirlist(i).name );
	M = P(:,3:4);
	M(:,1) = aspect*M(:,1);
	R = [ M(:,1) ; M(:,2) ];
	shapes = [ shapes R ];
end
dlmwrite('shapes.txt',shapes, ' ');