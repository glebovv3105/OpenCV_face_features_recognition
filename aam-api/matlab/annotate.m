function annotate( imagetype )
%
% function annotate( imagetype )
%
%
% Reads and displays all images in the current dir one by one.
%
% When an image is displayed, annotation can be done by left-
% clicking the mouse. Press 'e' to end annotation and write
% a corresponding asf-file in the current dir. Use plotasf() 
% to view the resulting asf-file.
% 
% Example usage:  annotate('bmp');
%
% Write 'help imread' to see supported filetypes.
%
% $Id: annotate.m,v 1.1.1.1 2003/01/03 19:18:51 aam Exp $ 
%
eval( sprintf('dirlist=dir(''*.%s'');', imagetype ) ); % get a dir list of all images of type 'imagetype'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% for each image in the current directory
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i=1:length(dirlist)

	%%%%%%%%%%%%%%%%%%%%%%
	% load and show image
	%%%%%%%%%%%%%%%%%%%%%%      
    image_filename=dirlist(i).name;    
    eval(sprintf('[img,cmap]=imread(''%s'');',image_filename));        
    figure;    
	axis ij;	
    imagesc(img);
    axis image;
    channels = size(img,3);
    if channels==1,    
        if isempty(cmap),       
            colormap( gray );
        else
            colormap( cmap );
        end         
    end
    eval(sprintf('title(''%s'');',image_filename));
    s=size(img);
    height=s(1);
    width=s(2);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % annotate image with points
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    end_botton = 0;
    point_count = 0;
    path_count = 0;
    while end_botton~='e' & end_botton~='b'
    
    	[points, end_botton]=markpath( point_count );
    	s=size(points);            	    
    	npathpoints = s(1);    	
    	if npathpoints==0    	    	
    		break;
		end    		

		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	    % create a path point matrix
	    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	    P_path 		= zeros( npathpoints, 7 );    
	    P_path(:,1) = path_count;								% path number
	    if end_botton=='o' 
	    	P_path(:,2) = 4;									% point type = open path
	    else
	    	P_path(:,2) = 0;									% point type = closed path
	    end
	    
	    P_path(:,3) = points(:,1)/width;  						% relative x positions
	    P_path(:,4) = points(:,2)/height; 						% relative y positions
	    P_path(:,5) = [point_count:point_count+npathpoints-1]';	% point number
	    
	    if end_botton=='c'
	    
	    	% closed path
	    	P_path(:,6) = [point_count+npathpoints-1 point_count:point_count+npathpoints-2]';  	% connects_from
	    	P_path(:,7) = [point_count+1:point_count+npathpoints-1 point_count]';				% connects_to
		else
			
			% open path (default)
			P_path(:,6) = [point_count point_count:point_count+npathpoints-2]';  				% connects_from
	    	P_path(:,7) = [point_count+1:point_count+npathpoints-1 point_count+npathpoints-1]';	% connects_to			    	
	    end	
	    	    	
    	if point_count==0
    	
    		% create point matrix
    		P = P_path;
		else
			% add to point matrix    		
			P = [P; P_path];
    	end
    	
    	% update the total number of points and paths
    	point_count = point_count + npathpoints;
    	path_count  = path_count + 1;
    end
                
    %%%%%%%%%%%%%%%%%%%%%%%%%%    
    % write the asf file
    %%%%%%%%%%%%%%%%%%%%%%%%%%
    afs_file=replaceext(image_filename, 'asf');
    writeasf( P, image_filename, afs_file );

    close;     
end




