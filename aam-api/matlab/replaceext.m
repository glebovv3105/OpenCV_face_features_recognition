function str=replaceext( filename, newext )
%
% function str=replaceext( filename, newext )
%
%
% Replaces the extension of 'filename' with 'newext'.
%
% $Id: replaceext.m,v 1.1.1.1 2003/01/03 19:18:51 aam Exp $ 
%
str=removeext( filename );
str=strcat(str,'.',newext);