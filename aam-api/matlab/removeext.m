function str=removeext( filename )
%
% function str=removeext( filename )
%
%
% Returns 'filename' without extension and the seperating dot '.'.
%
% $Id: removeext.m,v 1.1.1.1 2003/01/03 19:18:51 aam Exp $ 
%
v = findstr( filename, '.' );
dotpos=v(length(v));
str = filename(1:dotpos-1);