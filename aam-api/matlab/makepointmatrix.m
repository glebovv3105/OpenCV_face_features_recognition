function P = makepointmatrix( X, Y, width, height )
%
%  function P = makepointmatrix( X, Y )
%
%  Constructs an ASF point matrix from the pixel coordinates
%  given in the two input vectors. Width and height denotes
%  the size of the image that these are defined in.
%
%  Points are assumed to represent one closed curve.
%  The resulting point matrix P can be written to disk using
%  writeasf()
%
%  Author: M. B. Stegmann, Jan 2003
%
%  $Id: makepointmatrix.m,v 1.1 2003/04/23 11:04:37 aam Exp $ 
%
n = length( X(:) );
z = zeros( n, 1 );
con = [ 0:n-1 ; n-1 0:n-2 ; 1:n-1 0 ]';
P = [ z  z  X(:)/width  Y(:)/height con ];
