function plotasf( filename, ptnb ) 
%
% function plotasf( filename, ptnb ) 
%
% Reads an asf file and plots the shape overlayed 
% on hostimage (if it exists).
%
% Set 'ptnb' == 1 for point numbering, if ommited 0 is used
%
% $Id: plotasf.m,v 1.1.1.1 2003/01/03 19:18:51 aam Exp $ 
%
[P,host_image]=readasf( filename  );
fid = -1;
if length(host_image)>0
	fid = fopen( host_image, 'r' );
end

if fid==-1		
	host_image = '';
	disp('Info: Host image not found. Plotting annotation (aspect ratio may be wrong).');
else
	fclose(fid);	
end
if (nargin<2) 
	ptnb = 0;
end
plotpoints(P,1,1,host_image,'w.-', ptnb );		

% turn off ticks
set(gca,'XTickLabel',{''});
set(gca,'YTickLabel',{''});