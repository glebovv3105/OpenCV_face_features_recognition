*****************************************************************************

    HOWTO -- Use matlab in conjunction with the aam-api

    by Mikkel B. Stegmann - $Revision: 1.2 $ $Date: 2003/01/03 19:31:17 $ 

*****************************************************************************


Abstract
********

This document describes the set of utility functions placed in /matlab.
These are designed for use with MatLab v5.3 (or higher), a power mathematical
toolbox from MathWorks (http://www.mathworks.com/)

Notice that detailed information about each function is printed by entering
'help <function name>' in the matlab prompt - e.g. 'help annotate'.


Requirements
************

MatLab v5.3 (or higher) is needed to use all functions unmodified. Some 
functions will work with earlier versions and you may succeed in modifying 
the functions to work with earlier versions. 


Setup
*****

Add the /matlab directory inside the AAM-API package to your matlab path.
This can be done by selecting File->Set Path... and then Path->Add to Path..


Usage
*****

This is not an exhaustive list of functions, but merely a short introduction 
to the essential things you can do.


Annotation of a training set: 

     Make sure that you've changed directory to the place of your training
     images (use 'pwd' or 'ls').

     Write 'annotate( <ext> )' where <ext> is the extension of you images -
     e.g. 'annotate( 'bmp' )' and follow the instructions.


Input and Output of .asf files:

     The functions 'readasf' and 'writeasf' performs .asf I/O.
     Notice that the matlab format of an .asf is simply a matrix
     of all the data lines inside the .asf file. This is called a point
     matrix.


Plotting a shape:

     By using plotpoints() a point matrix can be plotted on either a clean 
     canvas or as overlay to an image. A shape is loaded into a point
     matrix by 'readasf'.

     To plot an asf file directly use plotasf().



