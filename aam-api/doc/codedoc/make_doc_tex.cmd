@echo off
REM
REM	Reads all source files in ..\..\aam\ 
REM and builds documentation in HTML format
REM
REM Author: Mikkel B. Stegmann
REM 
@echo off
@echo off
cd ..\codedoc\print
docxx -t -S -p -eo 10pt -l  -ep a4wide -d .\ ..\..\..\inc\*.* ..\..\..\aam-api\*.*  > doc.tex

REM Run LaTeX twice to get all references updated
REM
latex.exe aamapi.tex
latex.exe aamapi.tex

REM Make PostScript version
REM
dvips.exe aamapi.dvi

REM Make PDF version of the documenation.
REM
dvipdfm -p a4 aamapi.dvi

