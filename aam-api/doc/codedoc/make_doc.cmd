@echo off
REM
REM	Reads all aam-api source files and builds documentation in LaTeX format
REM
REM Author: Mikkel B. Stegmann
REM 
docxx -B html_footer.html -m -k -f -S -p -j -d html ..\..\inc\*.* ..\..\aam-api\*.* 

REM Uncomment the line below to start the internet explorer 
REM with your documentation automatically.
REM
REM start iexplore.exe html\HIER.html
pause