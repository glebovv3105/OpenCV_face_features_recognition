//****************************************************************************
//
// Copyright � 2000, 2001, 2002, 2003 by Mikkel B. Stegmann
// 
// IMM, Informatics & Mathmatical Modelling
// Technical University of Denmark
// Richard Petersens Plads
// Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/
//
// Author: Mikkel B. Stegmann - http://www.imm.dtu.dk/~aam/ - aam@imm.dtu.dk
//
// DISCLAIMER:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on;
//
//		- The Microsoft Vision Software Developers Kit: VisSDK, Microsoft
//		- The DTU Image Viewer: DIVA, Rune Fisker et al., IMM, DTU
//
//****************************************************************************

#if !defined(__C_AAMCONSOLE__)
#define __C_AAMCONSOLE__

#include "stdafx.h"
#include "vector"

/**

  @author   Mikkel B. Stegmann
  @version  4-13-2000

  @memo     AAM-API Console Interface.
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
class CAAMConsoleMode {

protected:
	int m_NMinParam;
	int m_NMaxParam;
	CString m_Name;
	CString m_Usage;
	CString m_Desc;
	CString m_LongDesc;	
	CString m_ProgName;
 
public:	
	CAAMConsoleMode( const CString &progname = "" ) : m_ProgName(progname) {}
	const CString& Name()		const	{ return m_Name;		}
	const CString& Usage()		const	{ return m_Usage;		}
	const CString& Desc()		const	{ return m_Desc;		}
	const CString& LongDesc()	const	{ return m_LongDesc;	}	
	virtual int Main(int argc, char **argv) const;
	void PrintUsage() const;

};



/**

  @author   Mikkel B. Stegmann
  @version  4-13-2000

  @memo     AAM-API Console Interface.
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
class CAAMConsole {
	
	int m_argc;
	char **m_argv;

	std::vector<CAAMConsoleMode*> m_Modes;

private:

	int IsModeAllowed( const CString &modeStr ) const;
	void PrintUsage() const;	
	void PrintHelp() const;
	void PrintFullHelp() const;
	
public:

	/// Constructor.
	CAAMConsole( int argc, char **argv );	

	static bool IsHelpSwitch( const CString &arg );
};


   
/**

  @author   Mikkel B. Stegmann
  @version  4-13-2000

  @memo     AAM-API Console Interface.
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
class CAAMConsoleModeW : public CAAMConsoleMode {

public:
	CAAMConsoleModeW( const CString &progname );
	int Main(int argc, char **argv) const;	
};



/**

  @author   Mikkel B. Stegmann
  @version  4-13-2000

  @memo     AAM-API Console Interface.
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
class CAAMConsoleModeB : public CAAMConsoleMode {

public:
	CAAMConsoleModeB( const CString &progname );
	int Main(int argc, char **argv) const;
};


/**

  @author   Mikkel B. Stegmann
  @version  4-14-2000

  @memo		AAM-API Console Interface.  
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
class CAAMConsoleModeE : public CAAMConsoleMode {

public:
	CAAMConsoleModeE( const CString &progname );
	int Main(int argc, char **argv) const;
};


/**

  @author   Mikkel B. Stegmann
  @version  4-17-2000

  @memo     AAM-API Console Interface.
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
class CAAMConsoleModeS : public CAAMConsoleMode {

public:
	CAAMConsoleModeS( const CString &progname );
	int Main(int argc, char **argv) const;
};


/**

  @author   Mikkel B. Stegmann
  @version  4-18-2000

  @memo     AAM-API Console Interface.
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
class CAAMConsoleModeM : public CAAMConsoleMode {

public:
	CAAMConsoleModeM( const CString &progname );
	int Main(int argc, char **argv) const;
};



/**

  @author   Mikkel B. Stegmann
  @version  4-19-2000

  @memo     AAM-API Console Interface.
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
class CAAMConsoleModeR : public CAAMConsoleMode {

public:
	CAAMConsoleModeR( const CString &progname );
	int Main(int argc, char **argv) const;
};


/**

  @author   Mikkel B. Stegmann
  @version  5-3-2000

  @memo		AAM-API Console Interface. 
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
class CAAMConsoleModeD : public CAAMConsoleMode {

public:
	CAAMConsoleModeD( const CString &progname );
	int Main(int argc, char **argv) const;
};


/**

  @author   Mikkel B. Stegmann
  @version  5-4-2000

  @memo     AAM-API Console Interface. 
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
class CAAMConsoleModeSM : public CAAMConsoleMode {

public:
	CAAMConsoleModeSM( const CString &progname );
	int Main(int argc, char **argv) const;
};

/**

  @author   Mikkel B. Stegmann
  @version  5-4-2000

  @memo     AAM-API Console Interface. 
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
class CAAMConsoleModeCM : public CAAMConsoleMode {

public:
	CAAMConsoleModeCM( const CString &progname );
	int Main(int argc, char **argv) const;
};


/**

  @author   Mikkel B. Stegmann
  @version  6-24-2000

  @memo     AAM-API Console Interface.
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
class CAAMConsoleModeT : public CAAMConsoleMode {

public:
	CAAMConsoleModeT( const CString &progname );
	int Main(int argc, char **argv) const;
};


/**

  @author   Mikkel B. Stegmann
  @version  6-24-2000

  @memo     AAM-API Console Interface.
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
class CAAMConsoleModeU : public CAAMConsoleMode {

public:
	CAAMConsoleModeU( const CString &progname );
	int Main(int argc, char **argv) const;
};


/**

  @author   Mikkel B. Stegmann
  @version  12-11-2000

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
class CAAMConsoleModeP : public CAAMConsoleMode {

public:
	CAAMConsoleModeP( const CString &progname );
	int Main(int argc, char **argv) const;
};


/**

  @author   Mikkel B. Stegmann
  @version  7-22-2002

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
class CAAMConsoleModeLOO : public CAAMConsoleMode {

public:
	CAAMConsoleModeLOO( const CString &progname );
	int Main(int argc, char **argv) const;
};



/**

  @author   Mikkel B. Stegmann
  @version  7-22-2002

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
class CAAMConsoleModeREG : public CAAMConsoleMode {

public:
	CAAMConsoleModeREG( const CString &progname );
	int Main(int argc, char **argv) const;
};


#endif /* __C_AAMCONSOLE__ */
