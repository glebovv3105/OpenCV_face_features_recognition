#include "AAM.h"
#include "AAMtest.h"
#include "htimer.h"
#include "AAMConsole.h"
#include <direct.h>


// configuration of which model and builder classes to use
// (not very elegant.... I know)
#ifndef C_AAMMODEL
#define C_AAMMODEL CAAMModel
#endif
#ifndef C_AAMBUILDER
#define C_AAMBUILDER CAAMBuilder
#endif


/**

  @author   Mikkel B. Stegmann
  @version  4-13-2000

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/ 
CAAMConsole::CAAMConsole(int argc, char **argv) {

	// store command line
	m_argv = argv;
	m_argc = argc;

	// insert the implemented modes
	// not very robust but ok
	// as long as this method is ended
	// with a exit(0) :-)
	CAAMConsoleModeB b( m_argv[0] );
	m_Modes.push_back( &b );	
	CAAMConsoleModeE e( m_argv[0] );
	m_Modes.push_back( &e );    ;
    CAAMConsoleModeLOO loo( m_argv[0] );
    m_Modes.push_back( &loo );
    CAAMConsoleModeM m( m_argv[0] );
	m_Modes.push_back( &m );
    CAAMConsoleModeP p( m_argv[0] );
	m_Modes.push_back( &p );
	CAAMConsoleModeR r( m_argv[0] );
	m_Modes.push_back( &r );	
    CAAMConsoleModeREG reg( m_argv[0] );
	m_Modes.push_back( &reg );	
	CAAMConsoleModeS s( m_argv[0] );
	m_Modes.push_back( &s );
	CAAMConsoleModeT t( m_argv[0] );
	m_Modes.push_back( &t );
    CAAMConsoleModeU u( m_argv[0] );
	m_Modes.push_back( &u );
	CAAMConsoleModeW w( m_argv[0] );
	m_Modes.push_back( &w );
	CAAMConsoleModeSM sm( m_argv[0] );
	m_Modes.push_back( &sm );
	CAAMConsoleModeCM cm( m_argv[0] );
	m_Modes.push_back( &cm );    
	CAAMConsoleModeD d( m_argv[0] );
	m_Modes.push_back( &d );	    
	
	// parse command line
	if ( argc<2 ) {

		// we need at least two arguments to do something
		PrintUsage();
		exit(1);
	}

	// print help if requested
	if ( IsHelpSwitch( argv[1]) ) {

		PrintHelp();
		exit(0);
	}

	if ( CString(argv[1]) =="-full"	) {

		// print full help
		PrintFullHelp();
		exit(0);
	}


	int modeID = IsModeAllowed( argv[1] );

	if ( modeID==-1 ) {
	
		// the specified mode was not allowed
		PrintUsage();
		exit(1);
	}

	// main call
	CString pnuc(m_argv[0]);
	pnuc.MakeUpper();
    try {

        // remove the mode info from the command line
	    // and run the mode
	    m_Modes[modeID]->Main( argc-2, &(argv[2]) );

    } catch( CVisError e ) {
			
            printf("%s: Exception thrown. The message was:\n", pnuc );
            printf("   @ line %i in file %s\n", e.SourceLine(), e.SourceFile() );
            printf("   function : %s()\n", e.FunctionName() );
            printf("   message  : %s\n", e.ErrorMessage() );

// #ifdef _DEBUG
            throw; // rethrow
// #else
            exit(-1);
// #endif

        }
        catch(...) {

            printf("%s: Unknown exception thrown. Exiting.\n", pnuc );

// #ifdef _DEBUG
            throw; // rethrow exception
// #else
            exit(-1);
// #endif      
            
        }

	// the mode was handled successfully if we reach this point
	exit(0);
}



/**

  @author   Mikkel B. Stegmann
  @version  4-25-2000

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
bool CAAMConsole::IsHelpSwitch( const CString &arg ) {

	return  arg =="-help"	||
			arg =="--h"	||
			arg =="-h"	||
			arg =="-?"	||
			arg =="?";
}


/**

  @author   Mikkel B. Stegmann
  @version  4-13-2000

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
void CAAMConsole::PrintHelp() const {

	fprintf( stdout, "The tool '%s' is the console interface to the AAM-API.\n", m_argv[0] );
}



/**

  @author   Mikkel B. Stegmann
  @version  4-19-2000

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
void CAAMConsole::PrintFullHelp() const {

	PrintUsage();
	PrintHelp();

	for(unsigned int i=0;i<m_Modes.size();i++) {

		// for each mode
		fprintf( stdout, "\n---------------------------------------------------------------------\n" );
		fprintf( stdout, "MODE: %s - %s\n",
						
				m_Modes[i]->Name(),
				m_Modes[i]->Desc()	);
		fprintf( stdout, "---------------------------------------------------------------------\n\n" );
		m_Modes[i]->PrintUsage();
	}    
}


/**

  @author   Mikkel B. Stegmann
  @version  4-13-2000

  @memo     Checks if the given mode is allowed.
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   The mode id.
  
*/
int CAAMConsole::IsModeAllowed( const CString &modeStr ) const {

		
	for(unsigned int i=0;i<m_Modes.size();i++) {

		if ( modeStr==m_Modes[i]->Name() ) {
			
			// the mode was found
			return i;
		}
	}
	
	// we never found the mode
	return -1;
}



/**

  @author   Mikkel B. Stegmann
  @version  4-13-2000

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
void CAAMConsole::PrintUsage() const {

	CString pnuc(m_argv[0]);
	pnuc.MakeUpper();
	fprintf( stdout, "%s - Active Appearance Model Console Interface - version 0.98\n", pnuc );
	fprintf( stdout, "Copyright (c) Mikkel B. Stegmann 2003, mbs@imm.dtu.dk. All rights reserved.\n" );
	fprintf( stdout, "For further information see http://www.imm.dtu.dk/~aam/\n\n" );
    fprintf( stdout, "This version of %s was built on: %s %s\n\n", pnuc, __DATE__, __TIME__ );
	fprintf( stdout, "USAGE:\n" );
	fprintf( stdout, "       %s <", m_argv[0] );
	for(unsigned int i=0;i<m_Modes.size();i++) 
		fprintf( stdout, "%s%s", i ? "|":"", m_Modes[i]->Name() );
	fprintf( stdout, "> [additional mode arguments]\n\n" );

	fprintf( stdout, "MODES:\n");
	for(i=0;i<m_Modes.size();i++) 
		fprintf( stdout, "       %s %s %s\n", m_argv[0], m_Modes[i]->Name(), m_Modes[i]->Usage() );

	fprintf( stdout, "\nFor futher help write: '%s -help', '%s -full' or '%s <modename> -help'\n\n", 
			 m_argv[0], m_argv[0], m_argv[0] );
}



/**

  @author   Mikkel B. Stegmann
  @version  4-13-2000

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
void CAAMConsoleMode::PrintUsage() const {

	fprintf(stdout, "USAGE:\n      %s %s %s\n\n", m_ProgName, m_Name, m_Usage );
	fprintf(stdout, "DESCRIPTION:\n\n%s\n\n%s\n", m_Desc, m_LongDesc );
}


/**

  @author   Mikkel B. Stegmann
  @version  4-13-2000

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
int CAAMConsoleMode::Main(int argc, char **argv) const {
		
	// check for help param
	if (argc==1) {

		if ( CAAMConsole::IsHelpSwitch( argv[0] ) ) {

			PrintUsage();
			exit(0);
		}
	}

	// check param amount
	if ( !(argc>=m_NMinParam && argc<=m_NMaxParam) ) {

		PrintUsage();
		exit(1);
	}

	return 0;
}


/**

  @author   Mikkel B. Stegmann
  @version  4-14-2000

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
CAAMConsoleModeW::CAAMConsoleModeW( const CString &progname ) {


	m_ProgName  = progname;
	m_NMinParam = 2;
	m_NMaxParam = 3;

	
	m_Name		= "w";
	m_Usage		= "<write options> <annotation> [reduction factor]";
	m_Desc		= CString("Plots an annotation into it's host image.\n\n")+
                  CString("<write options>    Set one or several of the following options to get\n")+
                  CString("                   the desired result (not sensitive to ordering):\n\n")+
                  CString("                     s   Plots the shape as connecting lines\n")+
                  CString("                     p   Plots all shape points as cross-hairs\n")+
                  CString("                     i   Dots the interior of the shape\n")+
                  CString("                     n   Draws point normals\n")+
                  CString("                     m   Plots a Delaunay triangulation of the shape\n")+
                  CString("                     t   Similar to 'm' but removes concave triangles and holes\n\n")+                  
                  CString("<annotation>       An .asf-file.\n\n")+
                  CString("[reduction factor] Optional integer image/annotation reduction factor [2-n]\n\n")+
				  CString("Example usage:\n\n       ") + m_ProgName + CString(" w si c6302.asf\n" );		          
    m_LongDesc	= "";
}


/**

  @author   Mikkel B. Stegmann
  @version  4-14-2000

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
int CAAMConsoleModeW::Main(int argc, char **argv) const {		
	
	// call base implementation
	CAAMConsoleMode::Main( argc, argv );
	int rfactor = 1;
	
	// setup
	CDMultiBand<TAAMPixel> image;    
	CAAMShape shape;
    CString outFile;
    CString options  = argv[0];
    CString asf_file = argv[1];
    
    // determine write options
    bool drawLines         = NULL!=strchr( options, 's' );
    bool drawPoints        = NULL!=strchr( options, 'p' );
    bool drawInterior      = NULL!=strchr( options, 'i' );
    bool drawNormals       = NULL!=strchr( options, 'n' );
    bool drawDelaunay      = NULL!=strchr( options, 'm' );
    bool drawDelaunayClean = NULL!=strchr( options, 't' );
    
	if (argc==3) {

		// scale factor was incluced		
		rfactor = atoi( argv[2] );
	}
	
    // read input
	CAAMUtil::ReadExample( asf_file, image, shape, rfactor );
    
    TAAMPixel p255[BANDS];
    SPAWN( p255, 255)

    // plot delaunay mesh
    if (drawDelaunay || drawDelaunayClean ) {
        CAAMMesh mesh;
        CAAMDelaunay::MakeMesh( shape, mesh, drawDelaunayClean );
        CAAMUtil::PlotMeshIntoImage( image, mesh, p255, p255);
    }

    // plot shape	
	CAAMUtil::PlotShapeIntoImage( image, shape, p255, p255, drawNormals, drawInterior, drawPoints, drawLines );	        

    // write the result to disk
	outFile =	CAAMUtil::RemoveExt( asf_file ) + CString(".ann") + ".bmp";	
	image.WriteBandedFile( outFile, "bmp" );
	
	fprintf( stdout, "Annotation rendered and written into '%s'.\n", outFile );
	
	return 0;
}	



/**

  @author   Mikkel B. Stegmann
  @version  4-14-2000

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
CAAMConsoleModeB::CAAMConsoleModeB( const CString &progname ) {

	m_ProgName  = progname;
	m_NMinParam = 2;
	m_NMaxParam = 3;
	
	m_Name		= "b";
	m_Usage		= "<input dir> <out model> [acf file]";
	m_Desc		= "Builds an Active Appearance Model.";
	m_LongDesc	= CString("In this mode the principal component analysis, parameter optimization training\n")+
				  CString("etc. are done.\n\n" )+
				  CString("input dir : Directory containing images and annotations.\n")+				  
				  CString("out model : Filename (and path) of the output model file. Ex. 'model42'.\n")+
				  CString("acf file  : AAM configuration file.\n");				  
}


/**

  @author   Mikkel B. Stegmann
  @version  4-14-2000

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
int CAAMConsoleModeB::Main(int argc, char **argv) const {
	
	// call base implementation
	CAAMConsoleMode::Main( argc, argv );

    C_AAMBUILDER builder;
	C_AAMMODEL model;
	fprintf( stdout, "Building Active Appearence Model.\n" );

	// setup input parameters
	CString inDir    = argv[0];		
	CString outModel = argv[1];	
	CString acf		 = argc>=3 ? argv[2] : "";

    // test if the files can be opened
	bool ok = CAAMUtil::CreateTest( outModel );
	if (!ok) { 
		
		printf( "Error creating model file. Exiting...\n" );
		return -1; 
	}

	CString ext = CAAMUtil::GetExt( acf );
	if ( ext=="acf" || acf=="" ) {
	
		// build model
		builder.BuildFromFiles( model, inDir, acf );

		// write
		model.WriteModel( outModel );

	} else {

		if ( ext=="sacf" ) {

			// model sequence
			CAAMModelSeq modelSeq;
			modelSeq.BuildFromSACF( acf, inDir );

			// write
			modelSeq.WriteModels( outModel );

		} else {

			printf("Error: unknown configuration extension.\n");
			exit(-1);
		}
	}


	// we're done
	return 0;
}



/**

  @author   Mikkel B. Stegmann
  @version  4-14-2000

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
CAAMConsoleModeE::CAAMConsoleModeE( const CString &progname ) {

	m_ProgName  = progname;
	m_NMinParam = 2;
	m_NMaxParam = 4;
	
	m_Name		= "e";
	m_Usage		= "<model> <dir> [still|movie|both|none*] [pseudo|auto*]";
	m_Desc		= "Evaluates an Active Appearance Model.";
	m_LongDesc	= CString("model              : The model .amf that should be evaluated.\n")+
				  CString("dir                : Directory containing images and ground truth annotations.\n")+				  
				  CString("[still|movie|both] : Write stills of the initial and optimized model\n")+
				  CString("                     and/or movies of the complete optimization.\n")+
				  CString("[pseudo|auto*]     : Initialisation method.\n")+				  
				  CString("\nOutput is written in the input dir in the file 'results.txt'\n")+
				  CString("Default settings are marked with an asterisk (*)\n");
}
				  

/**

  @author   Mikkel B. Stegmann
  @version  4-17-2000

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
int CAAMConsoleModeE::Main(int argc, char **argv) const {
	
	// call base implementation
	CAAMConsoleMode::Main( argc, argv );

	// setup input parameters
	CString inModel		= argv[0];	
	CString inPath		= CAAMUtil::AddBackSlash( argv[1] );			
	CString docOutput	= argc>=3 ? argv[2] : "";
	bool writeMovies	= docOutput=="movie" || docOutput=="both";
	bool writeStills	= docOutput=="still" || docOutput=="both";
	bool initialisation = true;	
    
	if (argc>=4) {

		initialisation = !(0==strcmp( argv[3], "pseudo" ));
	}

	CString ext = CAAMUtil::GetExt( inModel );
	if (ext=="amf") {

		C_AAMMODEL model;	

		// read model
		bool ok=model.ReadModel( inModel );

		if (!ok) {
	
			fprintf( stdout, "Could not read model file. Exiting...\n" ); 
			exit(1);
		}          

		CAAMTest::EvaluateModel( &model, inPath, "results.txt", 
								 writeStills, writeMovies,
		    					 initialisation, true );

	}  else {

		if (ext=="samf") {

			CAAMModelSeq modelSeq;

			// read model sequence
			bool ok = modelSeq.ReadModels( inModel );

			if (!ok) {
	
				fprintf( stdout, "Could not read model file. Exiting...\n" ); 
				exit(1);
			}          

			CAAMTest::EvaluateModelSeq( modelSeq, inPath, "results.txt", 
										writeStills, writeMovies,
		    							initialisation, true );
						
		} else {

			printf("Error: unknow model extension.\n");
			exit(-1);
		}
	}
	  

	// we're done
	return 0;
}


/**

  @author   Mikkel B. Stegmann
  @version  4-17-2000

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
CAAMConsoleModeS::CAAMConsoleModeS( const CString &progname ) {

	m_ProgName  = progname;
	m_NMinParam = 2;
	m_NMaxParam = 3;
	
	m_Name		= "s";
	m_Usage		= "<input model.amf> <input dir> [movie filename]";
	m_Desc		= "Active Appearance Model search.";
    m_LongDesc	= "Search output is written in the input dir.\nAutomatic initialisation is performed on all BMP images.\n";
}
				  


/**

  @author   Mikkel B. Stegmann
  @version  4-17-2000

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
int CAAMConsoleModeS::Main(int argc, char **argv) const {
	
	bool writeMovie = false;
    bool writeImages = true;

	// call base implementation
	CAAMConsoleMode::Main( argc, argv );

	C_AAMMODEL aam;	

	// setup input parameters	
	CString inModel		= argv[0];	
	CString inPath      = argv[1];	
	writeMovie			= argc==3;
	CString movieName   = "";
	if (argc==5) movieName = argv[2];

	CString outPath		= CAAMUtil::AddBackSlash(inPath);

	// read model
	bool ok=aam.ReadModel( inModel );

	if (!ok) {
	
		fprintf( stdout, "Could not read model file '%s'. Exiting...\n", inModel ); 
		exit(1);
	}


    std::vector<CString> vFilenames = CAAMUtil::ScanSortDir( inPath, "bmp");
    int nImages = vFilenames.size();  

    for(int i=0;i<nImages;i++) {

        CHTimer timer;
        timer.start();

	    fprintf( stdout, "Performing search using auto init on '%s'.\n", vFilenames[i] ); 

	    // do search
	    CDVector c( aam.CombinedPCA().NParameters() );
	    CDMultiBand<TAAMPixel> image;	

	    c = 0;

	    // read image
	    try	{				
		    image.ReadBandedFile( vFilenames[i] );				
	    }
	    catch (CVisFileIOError e) {	AfxMessageBox( e.FullMessage() );  return -1; }

	    // reduce to fit the model
        int rc = aam.ReductionFactor();
	    if (rc>1) image.ReducePyr( rc );
		    
	    // get meanshape meansize
	    CAAMShape s = aam.ReferenceShape();

        // initialize
        CAAMInitializeStegmann Stegmann( aam );
        
        Stegmann.Initialize( image, s, c );
        
        if ( writeImages ) {
            
			CString initfile = outPath+CAAMUtil::GetFilename(vFilenames[i])+"_init.bmp";
			CAAMVisualizer::ShapeStill( image, s, initfile );           
        }
        
        // do the actual optimization
        std::vector<C_AAMMODEL::CAAMOptState> optStates;
	    aam.OptimizeModel(	image, s, c, 30, &optStates );
        
        // write movie
        if (writeMovie) {
            
            CAAMVisualizer AAMvis(&aam);
			CString moviefile = CAAMUtil::ForceExt( outPath+CAAMUtil::GetFilename(vFilenames[i])+movieName, "avi" );
            AAMvis.OptimizationMovie( optStates, image, moviefile );
        }

        // write asf
        CString asffile = outPath+CAAMUtil::GetFilename(vFilenames[i])+"_opt.asf";
        s.WriteASF( asffile, image.Width(), image.Height() );

	    timer.stop();
	    double secs = timer.getTime();

	    printf( "Time spent: %.1f mins. (=%.1f secs.)\n", secs/60.0, secs );	

	    if ( writeImages ) {
		 
            CString optfile = outPath+CAAMUtil::GetFilename(vFilenames[i])+"_opt.bmp";
			CAAMVisualizer::ShapeStill( image, s, optfile );
	    }
    }

	// we're done
	return 0;
}


/**

  @author   Mikkel B. Stegmann
  @version  4-18-2000

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
CAAMConsoleModeM::CAAMConsoleModeM( const CString &progname ) {

	m_ProgName  = progname;
	m_NMinParam = 2;
	m_NMaxParam = 6;
	
	m_Name		= "m";
	m_Usage		= "<model.amf> <all|shape|texture|combined> [#modes] [#frames] [white|black*] [range]";
	m_Desc		= "Writes Active Appearance Model mode movies.";
	m_LongDesc	= CString("This mode documents the given AAM by generating movies showing the shape,\n")+
				  CString("texture and combined variation resulting from the PCAs.\n")+
				  CString("\nOutput are written in current dir.\n\n")+
                  CString("[#modes]        : The number of model modes to render (3).\n")+
                  CString("[#frames]       : The frames to render each mode in (10).\n")+
                  CString("[white|black]   : Background colour (black).\n")+                  
                  CString("[range]         : Parameter range (3).\n");                  
}
				  

/**

  @author   Mikkel B. Stegmann
  @version  4-18-2000

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
int CAAMConsoleModeM::Main(int argc, char **argv) const {
	
	bool writeMovie = false, writeImages = true;

	// call base implementation
	CAAMConsoleMode::Main( argc, argv );

	// setup input parameters	
	CString inModel		= argv[0];		
	CString movieType	= argv[1];	
	int nModes			= argc>=3 ? atoi( argv[2] )   : 3;
	int nSteps			= argc>=4 ? atoi( argv[3] )/2 : 10;
    bool bWhiteBG       = argc>=5 ? strcmp(argv[4],"white")==0 : false;
    double range        = argc>=6 ? atof(argv[5]) : 3;

	// test movie type
	if ( !(	movieType=="all"			||
			movieType=="shape"			||
			movieType=="texture"		||
			movieType=="combined" ) ) {
		
		fprintf( stdout, "Error: Movie type '%s' is not supported.\n\n", movieType );
		PrintUsage();
		return 1;
	}

	C_AAMMODEL aam;

	// read model
	bool ok=aam.ReadModel( inModel );

	if (!ok) {
	
		fprintf( stdout, "Could not read model file '%s'. Exiting...\n", inModel ); 		
		exit(1);
	}

	fprintf( stdout, "Generating movie type '%s'...\n", movieType );

    // make movie object
    CAAMVisualizer AAMvis( &aam );

    if (movieType=="texture" || movieType=="all" ) {

        AAMvis.TextureMovie( "texture", nModes, range, nSteps, bWhiteBG );
	}

	if (movieType=="shape" || movieType=="all" ) {
        
		AAMvis.ShapeMovie( "shape", nModes, range, nSteps, bWhiteBG );
	}	

	if (movieType=="combined" || movieType=="all" ) {

		AAMvis.CombinedMovie( "combined", nModes, range, nSteps, bWhiteBG );
	}

	// we're done
	return 0;	
}


/**

  @author   Mikkel B. Stegmann
  @version  4-19-2000

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
CAAMConsoleModeR::CAAMConsoleModeR( const CString &progname ) {

	m_ProgName  = progname;
	m_NMinParam = 2;
	m_NMaxParam = 2;
	
	m_Name		= "r";
	m_Usage		= "<model.amf> <input dir>";
	m_Desc		= "Tests the regression prediction in an AAM.";
	m_LongDesc	= "Output is written in the current dir in matlab format.";
}
				  


/**

  @author   Mikkel B. Stegmann
  @version  4-18-2000

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
int CAAMConsoleModeR::Main(int argc, char **argv) const {


		// call base implementation
	CAAMConsoleMode::Main( argc, argv );

	// setup input parameters	
	CString inModel		= argv[0];		
	CString inDir		= argv[1];		

	C_AAMMODEL aam;

	// read model
	bool ok=aam.ReadModel( inModel );

	if (!ok) {
	
		fprintf( stdout, "Could not read model file '%s'. Exiting...\n", inModel ); 		
		exit(1);
	}

	// test the prediction accuracy
    CAAMTest::TestPosePrediction( aam, inDir );


	// we're done
	return 0;
}


/**

  @author   Mikkel B. Stegmann
  @version  5-3-2000

  @memo     Debug/Test console mode
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
CAAMConsoleModeD::CAAMConsoleModeD( const CString &progname ) {

	m_ProgName  = progname;
	m_NMinParam = 0;
	m_NMaxParam = 100;
	
	m_Name		= "d";
	m_Usage		= "";
	m_Desc		= "Debug/Test console mode. Don't use!";
	m_LongDesc	= "";
}
	


/**

  @author   Mikkel B. Stegmann
  @version  4-18-2000

  @memo     Debug function.

  @doc      Debug function.
  
  @param    
  @param    
  
  @return   
  
*/
int CAAMConsoleModeD::Main(int argc, char **argv) const {


	// call base implementation
	CAAMConsoleMode::Main( argc, argv );

	CAAMModelSeq seq;

	//seq.BuildFromSACF( "test.sacf", "data" );

	//seq.WriteModels( "seqtest" );

	seq.ReadModels( "seqtest.samf" );
	

	// we're done
	return 0;
}



/**

  @author   Mikkel B. Stegmann
  @version  5-4-2000

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
CAAMConsoleModeSM::CAAMConsoleModeSM( const CString &progname ) {

	m_ProgName  = progname;
	m_NMinParam = 1;
	m_NMaxParam = 2;
	
	m_Name		= "sm";
	m_Usage		= "<input movie> [output extension]";
	m_Desc		= "Split movie file (.avi) into frames.";
	m_LongDesc.Format( "Example usage:\n\n     %s sm shape01.avi bmp\n", m_ProgName );
}
				  


/**

  @author   Mikkel B. Stegmann
  @version  4-18-2000

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
int CAAMConsoleModeSM::Main(int argc, char **argv) const {

	

	// call base implementation
	CAAMConsoleMode::Main( argc, argv );

	// setup input parameters	
	CString inMovie     = argv[0];			
	CString ext			= argc>=2 ? argv[1] : "bmp";

	
	// read movie
	CAAMMovieAVI avi;

	try {

		avi.Open( inMovie, 0 );

        int fc = avi.FrameCount();
		for(int i=0;i<fc;i++) {

			CVisImage<CVisRGBABytePixel> image;
			int ok=avi.ReadFrame( image, i );

			if (!ok) break;
			
			// write image
			CString s;
            if (fc<100) {
			    s.Format(".%02i.", i );
            } else {
                if (fc<1000) {
                    s.Format(".%03i.", i );
                } else {
                    s.Format(".%04i.", i );
                }
            }
			image.WriteFile( CAAMUtil::RemoveExt(inMovie)+s+ext );
		}
		
		avi.Close();
	}
	catch (CVisFileIOError e) {
			
		AfxMessageBox( e.FullMessage() ); 
	}
		
	// we're done
	return 0;
}





/**

  @author   Mikkel B. Stegmann
  @version  5-4-2000

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
CAAMConsoleModeCM::CAAMConsoleModeCM( const CString &progname ) {

	m_ProgName  = progname;
	m_NMinParam = 5;
	m_NMaxParam = 6;
	
	m_Name		= "cm";
	m_Usage		= "<input dir> <ext> <output avi> <frame rate> <gray|rgb> [comp]";
	m_Desc		=  CString("Collects a movie file from BMP frames.\n\n")+
                   CString("If the optional 'comp' is equal to '1' a compression dialog\n")+
                   CString("is shown. In all other cases the AVI format is uncompressed.\n");
	m_LongDesc	= "Example usage:\n\n     " + m_ProgName + " cm frames\\ bmp movie.avi 4 rgb 1\n";
}
				  


/**

  @author   Mikkel B. Stegmann
  @version  4-18-2000

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
int CAAMConsoleModeCM::Main(int argc, char **argv) const {

	

	// call base implementation
	CAAMConsoleMode::Main( argc, argv );

	// setup input parameters	
	CString inDir	    = argv[0];			
	CString inExt       = argv[1];			
	CString outMovie    = argv[2];			
	int frameRate		= atoi(argv[3]);
	CString mode		= argv[4];	
    bool showCompressionDlg = argc==6 ? 0==strcmp("1", argv[5]) : false;
	
	if (mode!="gray" && mode!="rgb") {

		printf("\nUnknown destination format '%s'. Must be either 'gray' or 'rgb'.\n\n",
				mode );	
		exit(-1);
	}

	std::vector<CString> vFrames;
	
	vFrames = CAAMUtil::ScanSortDir( inDir, inExt );
	
	try {
		

		CAAMMovieAVI avi( frameRate );
        avi.ShowCompressionDialog( showCompressionDlg );        
		avi.Open( outMovie, OF_CREATE|OF_WRITE  );
		for(unsigned int i=0;i<vFrames.size();i++) {

			printf("Reading frame #%i/%i '%s'.\n", i, vFrames.size(), vFrames[i] );

			// ugly but fast :-(
			if (mode=="gray") {
			
				CVisImage<CVisBytePixel> img;
				try	{				
					img.ReadFile( vFrames[i] );								                    
				}			
				catch (CVisFileIOError e) {
					
					AfxMessageBox( e.FullMessage() ); 				
					exit(-1);
				}			
				
				// convert and write frame
				CVisImage<CVisRGBABytePixel> rgbImage( img.MemoryRect() );		
				img.CopyPixelsTo( rgbImage );		
				avi.WriteFrame( rgbImage );

			} else {

				CVisImage<CVisRGBABytePixel> img;
				try	{				
					img.ReadFile( vFrames[i] );								
				}			
				catch (CVisFileIOError e) {
					
					AfxMessageBox( e.FullMessage() ); 				
					exit(-1);
				}			
				
				// write frame
				avi.WriteFrame( img );
			}			
		}
	}
	catch (CVisFileIOError e) {
		
		AfxMessageBox( e.FullMessage() ); 		
	}
		
	// we're done
	return 0;
}



/**

  @author   Mikkel B. Stegmann
  @version  6-24-2000

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
CAAMConsoleModeT::CAAMConsoleModeT( const CString &progname ) {

	m_ProgName  = progname;
	m_NMinParam = 2;
	m_NMaxParam = 2;
	
	m_Name		= "t";
	m_Usage		= "<model.amf> <input movie>";
	m_Desc		= "Performs tracking in a movie file (.avi).";
	m_LongDesc	= CString("This mode performs a through initialisation of the AAM in the first frame and\n")+
				  CString("uses the convergened position as initial pose in the next frame (and so on...).\n\n")+
				  CString("Example usage:\n\n     " + m_ProgName + " t mymodel.amf hand.avi\n");
}
				  


/**

  @author   Mikkel B. Stegmann
  @version  6-24-2000

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
int CAAMConsoleModeT::Main(int argc, char **argv) const {

	

	// call base implementation
	CAAMConsoleMode::Main( argc, argv );


	// setup input parameters	
	CString inModel		= argv[0];		
	CString inMovie     = argv[1];				
	

	C_AAMMODEL aam;

	// read model
	bool ok=aam.ReadModel( inModel );

	if (!ok) {
	
		fprintf( stdout, "Could not read model file '%s'. Exiting...\n", inModel ); 		
		exit(1);
	}
	
	// read movie
	CAAMMovieAVI avi, aviOut;

	CDVector c;
	CAAMShape shape;

	shape = aam.ReferenceShape();

    CAAMInitializeStegmann Stegmann( aam );

	try {

		// open the input movie
		avi.Open( inMovie, 0 );

		// open the putput movie
		aviOut.Open( CAAMUtil::RemoveExt(inMovie)+".track.avi", OF_CREATE|OF_WRITE );

        // set the frame rate to match input source
        aviOut.SetFrameRate( avi.FrameRate() );

        // do tracking
        FILE *fh = fopen("tracking_results.txt", "wt");
		for(int i=0;i<avi.FrameCount();i++) {

			CVisImage<CVisRGBABytePixel> image;
			int ok=avi.ReadFrame( image, i );
			if (!ok) break;
			int w = image.Width();
			int h = image.Height();
			
			// convert to multiband	
			CDMultiBand<TAAMPixel> gray( w, h, BANDS, evisimoptDontAlignMemory );
			for(int y=0;y<h;y++) {
				for(int x=0;x<w;x++) {
                                        TAAMPixel pgray[BANDS];
                                        gray. FromRGB( image.Pixel(x,y), pgray);
					gray.set_pixel(x,y,pgray);
				}
			}           
		
			if (i==0) {

				// it's the first frame -> initialize               	
	            Stegmann.Initialize( gray, shape, c );				               

                // write initial model points			
				{
					// make a copy of the multiband gray image
					CDMultiBand<TAAMPixel> grayCopy( w, h, BANDS, evisimoptDontAlignMemory );
					gray.CopyPixelsTo( grayCopy );		

					// plot into gray image
                                        TAAMPixel p255[BANDS];
                                        SPAWN( p255, 255)
					CAAMUtil::PlotShapeIntoImage( grayCopy, shape, p255, p255, 
                                          false, false, false );

					// convert to rgba
					CVisImage<CVisRGBABytePixel> rgba( w, h, 1, evisimoptDontAlignMemory	);						
					grayCopy.CopyPixelsToRGBA( rgba );		

					// write frame
					aviOut.WriteFrame( rgba );										

					// write still
					grayCopy.WriteBandedFile("init.bmp");
				}
            } 


            // debug output
            printf("Tracking frame %3i... ", i );
									
			// do the optimization
            C_AAMMODEL::CAAMOptRes res;
			res = aam.OptimizeModel( gray, shape, c );

            // debug output
            double cog_x,cog_y;
            shape.COG( cog_x, cog_y );
            printf("done, used %2i iterations, E=%9.2e, COG=(%3.0f, %3.0f)\n",
                    res.NIter(), 
                    res.SimilarityMeasure(),                    
                    cog_x, cog_y );

            // dump output
            CString asf;
            asf.Format( "%s.%04i.asf", CAAMUtil::RemoveExt(CAAMUtil::GetFilename( inMovie )), i );
            shape.WriteASF( asf, gray.Width(), gray.Height() );

            fprintf(fh, "%i\t%i\t%f\t%f\t%f\t%f\n", 
                         i,                          
                         res.NIter(),
                         res.SimilarityMeasure(), 
                         cog_x, 
                         cog_y,
                         res.Mahalanobis() );
            fflush(fh);

					
			// write optimized model points
			{	
				// plot into gray image
                TAAMPixel p255[BANDS];
                SPAWN( p255, 255)
				CAAMUtil::PlotShapeIntoImage( gray, shape, p255, p255, 
                                              false, false, false );
				
				// convert to rgba
				CVisImage<CVisRGBABytePixel> rgba( w, h, 1, evisimoptDontAlignMemory	);										
				gray.CopyPixelsToRGBA( rgba );						

				// write frame
				aviOut.WriteFrame( rgba );	

                // write still
                //CString str; str.Format("track.%04i.bmp", i );
                //gray.WriteBandedFile(str);
			}		
		}
		
		// close movies and files
		avi.Close();
		aviOut.Close();
        fclose(fh);
	}
	catch (CVisFileIOError e) {
			
		AfxMessageBox( e.FullMessage() ); 
	}
		
	// we're done
	return 0;
}



/**

  @author   Mikkel B. Stegmann
  @version  11-15-2000

  @memo     Utility functions.
    
*/
CAAMConsoleModeU::CAAMConsoleModeU( const CString &progname ) {

	m_ProgName  = progname;
	m_NMinParam = 1;
	m_NMaxParam = 50;
	
	m_Name		= "u";
	m_Usage		= "<utility function> arguments ...";
	m_Desc		= "Utility functions";
	m_LongDesc	= CString("\nPrint area utility\n\nUsage:\n\n     " + m_ProgName + " u printarea\n");
}
				  


/**

  @author   Mikkel B. Stegmann
  @version  11-15-2000

  @memo     Utility functions.
    
*/
int CAAMConsoleModeU::Main(int argc, char **argv) const {

	
	// call base implementation
	CAAMConsoleMode::Main( argc, argv );

	// setup input parameters	
	CString utility = argv[0];			
	//CString ext		= argc>=2 ? argv[1] : "bmp";

    if (utility=="printarea") {

        // scan the current dir for all annotations
        std::vector<CString> annotations;
        
        annotations = CAAMUtil::ScanSortDir( ".\\", "asf" );

        CAAMShapeCollection collection;

        // calculate and print area for all annotations
        // in the current dir
        for(unsigned int i=0;i<annotations.size();i++) {

            // read shape
            CAAMShape shape;
            shape.ReadASF( annotations[i] );
            collection.Insert( shape );
        }
        collection.Rel2Abs(); // convert to absolute coordinates
        printf( "Annotation name \tArea\n" );
        for(i=0;i<annotations.size();i++) {
            
            // print name and area
            printf( "%s\t%f", annotations[i], collection[i].Area() );

            // print the area of each path (if more than one)
            std::vector<int> paths = collection[i].GetPaths();
            for(unsigned int p=0;p<paths.size() && paths.size()>1;p++) {

                CAAMShape s = collection[i].ExtractPath( paths[p] );
                printf( "\t%f", s.Area() );
            }
            printf( "\n" );
        }

        // ended with sucess -> return
        return 0;
    }


	if (utility=="comicstrip") {

        // scan the current dir for all bmp files
        std::vector<CString> images;
        
        images = CAAMUtil::ScanSortDir( ".\\", "bmp" );

		const int n = images.size();
		const int nCols = atoi( argv[1] );
		const int nRows = ceil( (double)n/nCols );
		int w,h;
		{
			CVisImage<TAAMPixel> image;
			image.ReadFile( images[0] );
			w=image.Width();
			h=image.Height();
		}

		CVisImage<TAAMPixel> outImage( nCols*w, nRows*h, 1, evisimoptDontAlignMemory );

		outImage.FillPixels( 255 );

		printf( "Creating %ix%i image matrix (%ix%i pixels)...\n",
				nRows, nCols, outImage.Width(), outImage.Height() );

		int i = 0;
		for(int r=0;r<nRows;r++) {

			for(int c=0;c<nCols;c++) {
				
				if (i>=images.size()) break;

				printf("reading image #%i: %s\n", i, images[i] );
				CVisImage<TAAMPixel> image;
				image.ReadFile( images[i] );
				
				image.CopyPixelsTo( outImage, CPoint( c*w, r*h ) );

				++i;
			}

		}

		outImage.WriteFile( "comicstrip.bmp" );
		printf("Result is written as 'comicstrip.bmp'.\n");

		// ended with sucess -> return
        return 0;
	}



    // unknown utility
    printf("The utility '%s' is unknown.\n", utility );
    return -1;
}


/**

  @author   Mikkel B. Stegmann
  @version  12-11-2000

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
CAAMConsoleModeP::CAAMConsoleModeP( const CString &progname ) {

	m_ProgName  = progname;
	m_NMinParam = 1;
	m_NMaxParam = 100;
	
	m_Name		= "p";
	m_Usage		= "[b|s|e|loo] <nb. levels> <usual set of parameters>";
	m_Desc		= "N-level pyramidal (multi-scale) AAM.";

	m_LongDesc	= CString("This mode implements the modes b,s,e and loo using an N-level aam.\n")+
		          CString("Parameters after the mode name are set as in the 1-level modes.\n");
}
				  


/**

  @author   Mikkel B. Stegmann
  @version  11-15-2000

  @memo     
    
*/
int CAAMConsoleModeP::Main(int argc, char **argv) const {

	

	// call base implementation
	CAAMConsoleMode::Main( argc, argv );

	// setup input parameters	
	CString mode = argv[0];	        

    // sanity checks
    if ( (mode!="b" && mode!="s" && mode!="e" && mode!="loo")  ||
         (mode=="b"   && (argc<4 || argc>5) )		|| 
		 (mode=="e"   && (argc<4 || argc>6) )		|| 
		 (mode=="loo" && (argc<3 || argc>6) )		|| 
         (mode=="s"   && (argc<4 || argc>5) ) ) {

        PrintUsage(); 
        exit(-1);
    }


	// the number of levels in the model
	int nLevels = atoi( argv[1] );
	
    
    // build/use the multi scale AAM    
    if ( mode=="b" ) {

        // build multi-scale AAM
		CAAMModelMS multiscale;
        CString inputdir = argv[2];	    
        CString outmodel = argv[3];	    
        CString acf      = argv[4];	            

        CHTimer timer;
        timer.start();        
        multiscale.BuildAllLevels( nLevels, inputdir, acf );
        timer.stop();
        multiscale.WriteModel( outmodel );
        printf("Total build time for %i levels: %.3f secs.\n", nLevels, timer.getTime() ); 
		return 0;
    } 

    // search multi-scale AAM
    if ( mode=="s" ) {
        
		CAAMModelMS multiscale;
        CString inmodel  = argv[2];	    
        CString imagedir = argv[3];	    

        multiscale.ReadModel( inmodel );       
        std::vector<CString> fn;
        
        // scan for bmp and files
        fn = CAAMUtil::ScanSortDir( imagedir, "bmp" );
        
        for(unsigned int i=0;i<fn.size();i++) {

            CDMultiBand<TAAMPixel> image, smallImage;            
            CDVector c;
            image.ReadBandedFile( fn[i] );         		           

			CAAMShape s;
			smallImage = image;
			int rf = (int)(pow( 2., multiscale.NLevels()-1 ));
			if (rf>1) smallImage.ReducePyr( rf );
			
			bool cheat = false;
			if (cheat) {
				
				s.ReadASF( CAAMUtil::RemoveExt( fn[i] )+".asf" );
				s.Scale( 1./pow( 2., multiscale.NLevels()-1 ), false );
				s.Rel2Abs( image.Width(), image.Height() );
			} else {

				CAAMInitializeStegmann init( multiscale.GetSmallest() );
				init.Initialize( smallImage, s, c );

				CAAMVisualizer::ShapeStill( smallImage, s, "init.bmp" );

				// scale result back to level o
				s.Scale( pow( 2., multiscale.NLevels()-1 ), false );
			}

			CHTimer timer;
            timer.start();  
			multiscale.OptimizeModel( image, s, c );
            timer.stop();    
			
			CAAMVisualizer::ShapeStill( image, s, "opt.bmp" );
			

            printf("Multi-scale search took %.3f secs.\n", timer.getTime() ); 
        }
		return 0;
    }

	// evaluate mul-scale AAM
	if ( mode=="e" ) {

		CAAMModelMS *model = new CAAMModelMS;	

		// setup input parameters
		CString inModel		= argv[2];	
		CString inPath		= CAAMUtil::AddBackSlash( argv[3] );
		CString docOutput	= argc>=5 ? argv[4] : "";
		bool writeMovies	= docOutput=="movie" || docOutput=="both";
		bool writeStills	= docOutput=="still" || docOutput=="both";
		bool initialisation = true;	
    
		if (argc>=6) {

			initialisation = !(0==strcmp( argv[5], "pseudo" ));
		}

		// build model
		bool ok=model->ReadModel( inModel );

		if (!ok) {
		
			fprintf( stdout, "Could not read model file. Exiting...\n" ); 
			exit(1);
		}          

		CAAMTest::EvaluateModel( const_cast<CAAMModelMS*>( model ), inPath, "results.txt", 
								 writeStills, writeMovies,
		    					 initialisation, true );  
		delete model;
		return 0;
	}


	if ( mode=="loo" ) {

		CHTimer timer, thisLooTime;
		timer.start();

		// setup input parameters	
		CString inDir = argv[2];
		CString acf	  = argc>=4 ? argv[3] : "";
		CString docOutput	= argc>=5 ? argv[4] : "";
		bool writeMovies	= docOutput=="movie" || docOutput=="both";
		bool writeStills	= docOutput=="still" || docOutput=="both";
		bool initialisation = true;	    
		if (argc>=6) {

			initialisation = !(0==strcmp( argv[5], "pseudo" ));
		}    
		CString resultDir = "loo_results";    
		CString s;

		// scan shapes
		std::vector<CString> shapefns = CAAMUtil::ScanSortDir( inDir, "asf" );

		// make result dir
		_mkdir(resultDir);

		// do loo    
		CString shapeDestFile, imageDestFile;
		unsigned int n = shapefns.size();
		double totalTime =.0, modelParam = .0, modelSize =.0;
		int reduction_factor;
		CDVector ptpt(n), ptcrv(n), iter(n), time(n);
		CAAMEvaluationResults evalResults;
		CAAMTest::CAAMLowerBounds LowerBounds;
		for(unsigned int i=0;i<n;i++) {

			thisLooTime.reset();
			thisLooTime.start();

			// read shape
			CAAMShape shape;
			shape.ReadASF( shapefns[i] );
			CString shapefn = CAAMUtil::GetFilename( shapefns[i] );
			CString image   = shape.HostImage();
        			
			fprintf( stdout, "\n********************************************************\n" );
			fprintf( stdout, "Building AAM - %i/%i (leaving out '%s').\n",
					 i+1, shapefns.size(), shapefn );
			fprintf( stdout, "********************************************************\n" );
	        		
			// build model - excluding the i-th shape
			CAAMModelMS *model = new CAAMModelMS;	
			model->BuildAllLevels( nLevels, inDir, acf, 1, i );

			// make result dir
			s.Format("%s\\%s", resultDir, shapefn );        
			_mkdir(s);  

			// write model        
			s.Format("%s\\%s\\loo_model%02i", resultDir, shapefn, i );        
			model->WriteModel( s, true );
			reduction_factor = model->ReductionFactor();

			// calc stats 
			modelSize  += model->NTextureSamples();
			modelParam += model->CombinedPCA().NParameters();
              
			// copy shape        
			shapeDestFile.Format( "%s\\%s\\%s", resultDir, shapefn, shapefn );        
			CopyFile( shapefns[i], shapeDestFile, 0 );        

			// copy image
			imageDestFile.Format( "%s\\%s\\%s", resultDir, shapefn, image );        
			CopyFile( inDir+"\\"+image, imageDestFile, 0 );        

			// evaluate  
			fprintf( stdout, "Evaluating AAM on '%s'...\n", shapefn );
			CAAMEvaluationResults res;
			CAAMTest::CAAMLowerBounds lbs;
			res = CAAMTest::EvaluateModel(	model,
											resultDir+"\\"+shapefn+"\\", 
										   "results.txt", 
										   writeStills, 
										   writeMovies,
		      							   initialisation,
										   false,
										   &lbs  );

			evalResults.AddResults( res );   
			LowerBounds.AddGroundtruths( lbs );

			// delete shape and image files
			unlink( shapeDestFile );
			unlink( imageDestFile );

			// calc and write timings
			thisLooTime.stop();
			totalTime        += thisLooTime.getTime();
			double estAllLOOs = n*totalTime/(i+1.);
			double eta        = totalTime*(n/(i+1.)-1.);

			delete model;

			printf("This experiment: %.0f secs. Total time: %.1f mins. ETA: %.1f mins.\n",
					thisLooTime.getTime(), estAllLOOs/60., eta/60. );
		}    
		timer.stop();  
		
		// open result file
		FILE *fh = fopen( resultDir+"\\"+"loo_results.txt", "wt" );    

		evalResults.PrintStatistics( fh );
		evalResults.PrintStatistics();

		LowerBounds.PrintStatistics( resultDir+"\\lower_bounds.txt" );

		double totaltime = timer.getTime();
		fprintf( fh, "\n");    
		fprintf( fh, "Leave-one-out time    %s mins\n", CAAMUtil::Secs2Mins( totaltime)  );
		fprintf( fh, "Mean model parameters %7.1f\n", modelParam/n );
		fprintf( fh, "Mean texture samples  %7.0f\n", modelSize/n );
    
		fprintf( fh, "\n%g\t%g\t%g\t%g\n",
				 (double)reduction_factor,
				 totaltime,
				 modelParam/n,
				 modelSize/n			);
    
		// close files
		fclose( fh );
		return 0;
	}

    return -1;
}


/**

  @author   Mikkel B. Stegmann
  @version  7-22-2002

  @memo     
    
*/
CAAMConsoleModeLOO::CAAMConsoleModeLOO( const CString &progname ) {

	m_ProgName  = progname;
	m_NMinParam = 1;
	m_NMaxParam = 4;
	
	m_Name		= "loo";
	m_Usage		= "<input dir> [acf file] [still|movie|both|none*] [pseudo|auto*]";
	m_Desc		= "Leave-one-out evaluation of a training set.";
	m_LongDesc	= CString("This mode performs cross-validation of a training set.\n\n")+				  
				  CString("input dir          : Directory containing images and annotations.\n")+				  				  
				  CString("acf file           : AAM configuration file.\n")+    
				  CString("[still|movie|both] : Write stills of the initial and optimized model\n")+
				  CString("                     and/or movies of the complete optimization.\n")+
				  CString("[pseudo|auto*]     : Initialisation method.\n")+				  
				  CString("\nOutput is written in the directory 'loo_results'.\n")+
				  CString("Default settings are marked with an asterisk (*)\n");
}
				  


/**

  @author   Mikkel B. Stegmann
  @version  7-22-2002

  @memo     
    
*/
int CAAMConsoleModeLOO::Main(int argc, char **argv) const {

    // call base implementation
	CAAMConsoleMode::Main( argc, argv );
    CHTimer timer, thisLooTime;
    timer.start();

	// setup input parameters	
	CString inDir = argv[0];
    CString acf	  = argc>=2 ? argv[1] : "";
    CString docOutput	= argc>=3 ? argv[2] : "";
	bool writeMovies	= docOutput=="movie" || docOutput=="both";
	bool writeStills	= docOutput=="still" || docOutput=="both";
	bool initialisation = true;	    
	if (argc>=4) {

		initialisation = !(0==strcmp( argv[3], "pseudo" ));
	}    
    CString resultDir = "loo_results";    
    CString s;

    // scan shapes
    std::vector<CString> shapefns = CAAMUtil::ScanSortDir( inDir, "asf" );

    // make result dir
    _mkdir(resultDir);

    // do loo    
    CString shapeDestFile, imageDestFile;
    unsigned int n = shapefns.size();
    double totalTime =.0, modelParam = .0, modelSize =.0;
    int reduction_factor;
    CDVector ptpt(n), ptcrv(n), iter(n), time(n);
	CAAMEvaluationResults evalResults;
	CAAMTest::CAAMLowerBounds LowerBounds;
    for(unsigned int i=0;i<n;i++) {

        thisLooTime.reset();
        thisLooTime.start();

        // read shape
        CAAMShape shape;
        shape.ReadASF( shapefns[i] );
        CString shapefn = CAAMUtil::GetFilename( shapefns[i] );
        CString image   = shape.HostImage();
        
        C_AAMBUILDER builder;
	    C_AAMMODEL model;
		CAAMModelSeq modelSeq;
        fprintf( stdout, "\n********************************************************\n" );
	    fprintf( stdout, "Building AAM - %i/%i (leaving out '%s').\n",
                 i+1, shapefns.size(), shapefn );
        fprintf( stdout, "********************************************************\n" );
	        	
		// make result dir
        s.Format("%s\\%s", resultDir, shapefn );        
        _mkdir(s);  

	    // build model - excluding the i-th shape
		CString ext = CAAMUtil::GetExt( acf );
		s.Format("%s\\%s\\loo_model%02i", resultDir, shapefn, i );        
		if ( ext=="acf" ) {
	
			// build model
			builder.BuildFromFiles( model, inDir, acf, 1, i );

			// write model        
			model.WriteModel( s, true );

			reduction_factor = model.ReductionFactor();

			// calc stats 
			modelSize  += model.NTextureSamples();
			modelParam += model.CombinedPCA().NParameters();

		} else {

			if ( ext=="sacf" ) {

				// model sequence				
				modelSeq.BuildFromSACF( acf, inDir, i );

				// write
				modelSeq.WriteModels( s, true );

				reduction_factor = model.ReductionFactor();

				// calc stats 
				modelSize  += modelSeq.FinalModel().NTextureSamples();
				modelParam += modelSeq.FinalModel().CombinedPCA().NParameters();
			} else {

				printf("Error: unknown configuration extension.\n");
				exit(-1);
			}
		}
              
        // copy shape        
        shapeDestFile.Format( "%s\\%s\\%s", resultDir, shapefn, shapefn );        
        CopyFile( shapefns[i], shapeDestFile, 0 );        

        // copy image
        imageDestFile.Format( "%s\\%s\\%s", resultDir, shapefn, image );        
        CopyFile( inDir+"\\"+image, imageDestFile, 0 );        

        // evaluate  
        fprintf( stdout, "Evaluating AAM on '%s'...\n", shapefn );
		CAAMEvaluationResults res;
		CAAMTest::CAAMLowerBounds lbs;

		if ( ext=="acf" ) {

			res = CAAMTest::EvaluateModel(	&model,
											resultDir+"\\"+shapefn+"\\", 
										   "results.txt", 
										   writeStills, 
										   writeMovies,
		      							   initialisation,
										   false,
										   &lbs );
		}
		if ( ext=="sacf" ) {

			res = CAAMTest::EvaluateModelSeq( modelSeq, 
											  resultDir+"\\"+shapefn+"\\", 
											  "results.txt", 
											  writeStills, 
											  writeMovies,
		    								  initialisation, 
											  false,
											  &lbs );
		}

		evalResults.AddResults( res ); 
		LowerBounds.AddGroundtruths( lbs );

        // delete shape and image files
        unlink( shapeDestFile );
        unlink( imageDestFile );

        // calc and write timings
        thisLooTime.stop();
        totalTime        += thisLooTime.getTime();
        double estAllLOOs = n*totalTime/(i+1.);
        double eta        = totalTime*(n/(i+1.)-1.);

        printf("This experiment: %.0f secs. Total time: %.1f mins. ETA: %.1f mins.\n",
                thisLooTime.getTime(), estAllLOOs/60., eta/60. );
    }    
    timer.stop();  
	
	// open result file
    FILE *fh = fopen( resultDir+"\\loo_results.txt", "wt" );    

	evalResults.PrintStatistics( fh );
	evalResults.PrintStatistics();
	LowerBounds.PrintStatistics( resultDir+"\\lbs.txt" );

    double totaltime = timer.getTime();
    fprintf( fh, "\n");    
    fprintf( fh, "Leave-one-out time    %s mins\n", CAAMUtil::Secs2Mins( totaltime)  );
    fprintf( fh, "Mean model parameters %7.1f\n", modelParam/n );
    fprintf( fh, "Mean texture samples  %7.0f\n", modelSize/n );
    
    fprintf( fh, "\n%g\t%g\t%g\t%g\n",
             (double)reduction_factor,
             totaltime,
             modelParam/n,
             modelSize/n			);
    
    // close files
	fclose( fh );

    return 0;
}



/**

  @author   Mikkel B. Stegmann
  @version  7-22-2002

  @memo     
    
*/
CAAMConsoleModeREG::CAAMConsoleModeREG( const CString &progname ) {

	m_ProgName  = progname;
	m_NMinParam = 1;
	m_NMaxParam = 2;
	
	m_Name		= "reg";
	m_Usage		= "<input dir>";
	m_Desc		= "Registration from a set of shapes.";
	m_LongDesc	= CString("This mode creates a registration movie from a set of shapes.\n\n")+				  
				  CString("input dir          : Directory containing images and annotations.\n")+				  				  
				  CString("<convex|no-convex*>: Usage of the convex hull as shape extent.\n");
}
				  
				  


/**

  @author   Mikkel B. Stegmann
  @version  7-22-2002

  @memo     
    
*/
int CAAMConsoleModeREG::Main(int argc, char **argv) const {

    // call base implementation
	CAAMConsoleMode::Main( argc, argv );

    CString asfPath = argv[0];
    bool useConvexHull = argc>=2 ? 0==strcmp(argv[1],"convex") : false;
    bool writeRefShape = false;
    
    CAAMUtil::RegistrationMovie( "registration.avi", asfPath, useConvexHull, writeRefShape );


    return 0;
}

