///////////////////////////////////////////////////////////////////////////////
//
// Final image class used direct in the DIVA. This class should be keept empty 
// except for the constructors, destructor and if nessesary an assign function.
//
// Copyright � 1999
//
// DTU Image Viever and Analyser (DIVA)
// Section for Image Analysis
// Department of Mathematical Modelling
// Technical University of Denmark (DTU), Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/~diva
//
// author: Rune Fisker 
//
// Disclaimer:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on the Microsoft Vision Software Developers Kit VisSDK
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(CD_IMAGE_APP)
#define CD_IMAGE_APP

//#include "DImageMyClass.h"
#include "DImageMyClassGray.h"

template <class TPixel>
class CDImageApp :	public virtual CDImageMyClassGray<TPixel>
{
public:
	//------------------------------------------------------------------
	// @group Constructors

	// @cmember
	// Default constructor.  The image will not be useable until another
	// image is assigned to it.
	CDImageApp(void){};

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageApp(int width, int height, int nbands = 1,
		int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(width,height,nbands,imopts,pbData){};

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageApp(SIZE size, int nbands = 1,
			int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(size,nbands,imopts,pbData){};

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageApp(const RECT& rect, int nbands = 1,
			int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(rect,nbands,imopts,pbData){};

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageApp(const RECT& rect, CString sName, CString sHistory, int nbands = 1,
			int imopts = evisimoptDefault, BYTE *pbData = 0)
			:CVisImage<TPixel>(rect,nbands,imopts,pbData){ SetName(sName); SetHistory(sHistory); }

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageApp(const CVisShape& shape,
			int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(shape,imopts,pbData){};

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageApp(CVisMemBlock& refmemblock, int width, int height,
			int nbands = 1, int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,width,height,nbands,imopts){};

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageApp(CVisMemBlock& refmemblock, SIZE size, int nbands = 1,
			int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,size,nbands,imopts){};

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageApp(CVisMemBlock& refmemblock, const RECT& rect,
			int nbands = 1, int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,rect,nbands,imopts){};

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageApp(CVisMemBlock& refmemblock, const CVisShape& shape,
			int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,shape,imopts){};

	// @cmember
	// Construct an image from another image.
	// Warning:  Derived classes should provide their own copy constructor
	// that first constructs a default object and then uses the assignment
	// operator to synchronize  image creation, assignment, and
	// destruction.  Derived classes should not call the copy constructor
	// in the base class.
	CDImageApp(const CDImageApp<TPixel>& imageSrc)
		:CVisImage<TPixel>(imageSrc){ *this = imageSrc;}

	// Assignment operator.
	//	Warning: If you have added member attributes to the image classes, which 
	//	are derived into this class, rember to copy the attributes in the this assignment
	//	operator
	CDImageApp& operator=(const CDImageApp<TPixel>& imageSrc);
};

////////////////////////////////////////////////////////////////////////////
//
//	Assignment operator.
//	Warning: If you have added member attributes to the image classes, which 
//	are derived into this class, rember to copy the attributes in the this assignment
//	operator.
//
//	input
//		imageSrc: assign attributes from the input image
//
//	Author:	Rune Fisker, 19/4-1999
template <class TPixel>
inline CDImageApp<TPixel>& CDImageApp<TPixel>::operator=(const CDImageApp<TPixel>& imageSrc)
{
	((CVisImage<TPixel>&) *this) = ((const CVisImage<TPixel>&) imageSrc);

	// copy the attributes in CDImageInitial
	CopyInitialAttributes(imageSrc);

	return *this;
}


#endif // CD_IMAGE_APP