/////////////////////////////////////////////////////////////////////////////
//
// Image class for personale functions
//
// Copyright � 1998
//
// IMM, Department of Mathematical Modelling
// Technical University of Denmark, Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk
//
// author: S�ren Falch
//
// Disclaimer:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on the Microsoft Vision Software Developers Kit VisSDK
//
/////////////////////////////////////////////////////////////////////////////


#if !defined(CD_IMAGE_FFT)
#define CD_IMAGE_FFT

#include "DImageBasic.h"
#include <DMatrix.h>
#include <DImageInitial.h>
#include <complex>

// Global function declarations

// enumerated constants regarding padding
enum EDivaPadImage {
	edivaFFTMaxRectangle,
	edivaFFTMinRectangle
};
// enumerated constants regarding windowing
enum EDivaFFTWindow {
	edivaFFTRectangularWindow,
	edivaFFTBartlettWindow,
	edivaFFTHanningWindow,
	edivaFFTHammingWindow,
	edivaFFTAlphaTaperWindow,
	edivaFFTKaiserBesselWindow
};



// End of global function declarations

template <class TPixel>
class CDImageFFT : public virtual CDImageBasic<TPixel>
{
private:
	inline double Magn(const TPixel Real, const TPixel Imag);
	inline double Phase(const TPixel Real, const TPixel Imag);
	inline double EucDist(int x1,int y1, int x2, int y2);
public:
	//------------------------------------------------------------------
	// @group Constructors

	// @cmember
	// Default constructor.  The image will not be useable until another
	// image is assigned to it.
	CDImageFFT(void){};

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageFFT(int width, int height, int nbands = 1,
		int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(width,height,nbands,imopts,pbData){};

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageFFT(SIZE size, int nbands = 1,
			int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(size,nbands,imopts,pbData){};

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageFFT(const RECT& rect, int nbands = 1,
			int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(rect,nbands,imopts,pbData){};

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageFFT(const CVisShape& shape,
			int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(shape,imopts,pbData){};

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageFFT(CVisMemBlock& refmemblock, int width, int height,
			int nbands = 1, int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,width,height,nbands,imopts){};

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageFFT(CVisMemBlock& refmemblock, SIZE size, int nbands = 1,
			int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,size,nbands,imopts){};

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageFFT(CVisMemBlock& refmemblock, const RECT& rect,
			int nbands = 1, int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,rect,nbands,imopts){};

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageFFT(CVisMemBlock& refmemblock, const CVisShape& shape,
			int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,shape,imopts){};

	// @cmember
	// Construct an image from another image.
	// Warning:  Derived classes should provide their own copy constructor
	// that first constructs a default object and then uses the assignment
	// operator to synchronize  image creation, assignment, and
	// destruction.  Derived classes should not call the copy constructor
	// in the base class.
	CDImageFFT(const CDImageFFT<TPixel>& imageSrc)
		:CVisImage<TPixel>(imageSrc){};

	//------------------------------------------------------------------
	// image manipulation functions

	void FFT(CDImageFFT<float> &imageMagn, CDImageFFT<float> &imagePhase);
	void FFT(CDImageFFT<float> &imageRCPack2D);
	void FFTCrossCorrelate(CDImageFFT &Img, CDImageFFT<float> &ImgOut);
	void FFTCC(CDImageFFT<float> &Img, CDImageFFT<float> &ImgOut);
	void FFTPad(CDImageFFT &imageOut, int Size, int Window, int Position);
	std::complex<float> RCPack2D(const int X, const int Y);

	void IFFT();
	void QuadFlip();
	void Taper();
	void RCPack2DToComplex(CDImageFFT<std::complex<float> > &Tmp);
	void RCPack2DToMagnPhase(CDImageFFT<float> &Magn, CDImageFFT<float> &Phase);
	void MagPhase(CDImageFFT<float> &imgMagn, CDImageFFT<float> &imgPhase);
	
	void BartlettWindow(CDImageFFT<float> &Out, const int OffsetX, const int OffsetY);
	void HanningWindow(CDImageFFT<float> &Out, const int OffsetX, const int OffsetY);
	void HammingWindow(CDImageFFT<float> &Out, const int OffsetX, const int OffsetY);

};

#include "DImageFFT.inl"



#endif // CD_IMAGE_MORPHOLOGY