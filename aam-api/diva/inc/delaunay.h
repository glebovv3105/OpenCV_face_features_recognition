
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the DELAUNAY_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// DELAUNAY_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef DELAUNAY_EXPORTS
#define DELAUNAY_API __declspec(dllexport)
#else
#define DELAUNAY_API __declspec(dllimport)
#endif

DELAUNAY_API int ATTDelaunay(double *x,double *y, int nbPoints, int* triangles);

