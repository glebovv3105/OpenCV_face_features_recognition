/////////////////////////////////////////////////////////////////////////////
//
// This sequence provides some basic sequence functions
//
// Copyright � 1999
//
// IMM, Department of Mathematical Modelling
// Section for Image Analysis
// Technical University of Denmark, Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/~diva
//
// author: Rune Fisker 
//
// Disclaimer:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on the Microsoft Vision Software Developers Kit VisSDK
//
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//	reads the image sequence from a file.
//
//	author:	Rune Fisker, 12/9-1998
//	modified: Rune Fisker, 21/9-1999, extension compare corrected to handle differeent cases
//	DIVAUPDATE
template<class TPixel>
void CDSequenceBasic<TPixel>::ReadFile(SVisFileDescriptor& filedescriptorT)
{
	// insert image to be read
	Insert(0);

	// read the image
	At(0).ReadFile(filedescriptorT);

	// handle if sequence
	if (filedescriptorT.frame_number > 1)
	{
		// handle hips sequence
		CString strName(filedescriptorT.filetype);

		if ((strName.CompareNoCase("HIPS") != NULL) || (strName.CompareNoCase("HIP") != NULL))
		{
			// get and modify image shape
			CVisShape shape = At(0).Shape();
			shape.SetBottom(shape.Bottom()/filedescriptorT.frame_number);
			
			// #frames shapes
			Insert(0,filedescriptorT.frame_number,shape);
			
			// create src and dest. structures
			CRect rectScr = At(0).Rect();			
			int nRows = rectScr.bottom;
			CPoint pointDest(0,0);

			// copy each frame to the indiv. images
			for (int i=0;i<filedescriptorT.frame_number;i++)
			{
				At(filedescriptorT.frame_number).CopyPixelsTo(At(i),rectScr,pointDest,evisnormalizeCopyBytesSameType);
				rectScr.top += nRows;
				rectScr.bottom += nRows;

				// copy relevante attributes
				At(i).SetName(At(filedescriptorT.frame_number).Name(true));
			}

			// destruct the read image
			Erase(filedescriptorT.frame_number);
		}
		else
		{
			throw CVisFileIOError(filedescriptorT.filename,"This sequences format is not supported",eviserrorFormat,"ReadFile()","DVisSequenceBasic.inl", __LINE__);
		}
	}
}


/////////////////////////////////////////////////////////////////////////////
//	writes the image sequence to a file
//
//	author:	Rune Fisker, 12/9-1998
template<class TPixel>
void CDSequenceBasic<TPixel>::WriteFile(SVisFileDescriptor& filedescriptorT)
{
	assert(Length() >= 1);

	if (Length() == 1)
	{
		At(0).WriteFile(filedescriptorT);
	}
	else
	{
		// handle hips sequence
		if (strstr(filedescriptorT.filename,"hips") != NULL)
		{
			// get and modify image shape
			CVisShape shape = At(0).Shape();
			shape.SetBottom(shape.Bottom()*Length());

			// temp image
			T_Image image(shape);

			// source and destination structures
			CRect rectScr(0,0,shape.Right(),At(0).Shape().Bottom());
			CPoint pointDest(0,0);

			// copy the seq. to the temp. image
			for (int i=0;i<Length();i++)
			{
				At(i).CopyPixelsTo(image,rectScr,pointDest,evisnormalizeCopyBytesSameType);
				pointDest.y += At(0).Shape().Bottom();
			}

			// set file descriptor
			filedescriptorT.frame_number = Length();

			// write temp image to file
			image.WriteFile(filedescriptorT);
		}
		else
			throw CVisError("This sequences format is not supported",eviserrorFormat,"ReadFile()","DVisSequenceBasic.inl", __LINE__);
	}
}



/////////////////////////////////////////////////////////////////////////////
//	insert a copy of the full image indenpendtly of the actual ROI
//
//	author:		Rune Fisker, 12/1-1999
//	Modified	Rune Fisker, 26/5-1999
//DIVAUPDATE
template<class TPixel>
void CDSequenceBasic<TPixel>::InsertCopyFull(int i, T_Image& refimage) 
{
	assert(&refimage != NULL);
	
	// set input image ROI equal to full image
	CRect rectOld = refimage.Rect();
	refimage.SetRect(refimage.MemoryRect());

	// create new image (do in this way to copy the attributes as well)
	T_Image imageCopy(refimage);
	
	// allocate own pixels memory
	imageCopy.Allocate(refimage.MemoryRect());

	// copy image pixels to new memory
	refimage.CopyPixelsTo(imageCopy,evisnormalizeCopyBytesSameType); 
	
	// insert copy of input image
	Insert(i,imageCopy); 

	// set input image ROI back
	refimage.SetRect(rectOld);
}


/////////////////////////////////////////////////////////////////////////////
// create sequece history from the indiviual image histories
//
//	author:	Rune Fisker, 12/1-1999
template<class TPixel>
void CDSequenceBasic<TPixel>::History(CString& strHistory)
{
	strHistory = "";

	for (int i=0;i<Length();i++) 
	{
		CString str;
		str.Format("##### Frame %i #####\r\n",i);
		strHistory += str;
		strHistory += At(i).History() + "\r\n";
	}
}