/////////////////////////////////////////////////////////////////////////////
//
// This sequence is (almost) identical to the VisSDK CVisSequence with the 
// exception, that this sequence handle CDImageApp images
//
// This file contains a full copy of the VisSequence class code 
// from the file vissequence.inl 
//
// Copyright � 1999
//
// IMM, Department of Mathematical Modelling
// Section for Image Analysis
// Technical University of Denmark, Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/~diva
//
// author: Rune Fisker 
//
// Disclaimer:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on the Microsoft Vision Software Developers Kit VisSDK
//
/////////////////////////////////////////////////////////////////////////////



#ifndef __VIS_APP_IMAGEQUEUE_INL__
#define __VIS_APP_IMAGEQUEUE_INL__

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#ifdef _DEBUG
#undef new
#define new DEBUG_NEW
#undef THIS_FILE
#define THIS_FILE __FILE__
#endif // _DEBUG

template<class TPixel>
inline CDVisSequence<TPixel>::CDVisSequence(int nLength,
		int evissequence)
  : CVisSequenceBase(EVisPixFmtGet(), evissequence)
{
	Assign(nLength);
}

template<class TPixel>
inline CDVisSequence<TPixel>::CDVisSequence(int nLength,
		const CDVisSequence<TPixel>::T_Image& refimage, int evissequence)
  : CVisSequenceBase(EVisPixFmtGet(), evissequence),
	m_IsDying(false)
{
	Assign(nLength, refimage);
}
 

template<class TPixel>
inline CDVisSequence<TPixel>::CDVisSequence(int nLength,
		const CVisShape& refshape, int evissequence)
  : CVisSequenceBase(EVisPixFmtGet(), evissequence),
	m_IsDying(false)
{
	Assign(nLength, refshape);
}

template<class TPixel>
inline CDVisSequence<TPixel>::CDVisSequence(
		const CDVisSequence<TPixel>& refsequence, int iFirst,
		int iLim)
  : CVisSequenceBase(EVisPixFmtGet(), refsequence.SequenceOptions()),
	m_IsDying(false)
{
	assert(PixFmt() == refsequence.PixFmt());

	Assign(refsequence, iFirst, iLim);

	CopyPImageSource(&(refsequence.ImageSource()));
}

template<class TPixel>
inline CDVisSequence<TPixel>::CDVisSequence(
		const CDVisSequence<TPixel>& refsequence, int iFirst,
		int iLim, int evissequence)
  : CVisSequenceBase(EVisPixFmtGet(), evissequence),
	m_IsDying(false)
{
	assert(PixFmt() == refsequence.PixFmt());

	Assign(refsequence, iFirst, iLim);

	CopyPImageSource(&(refsequence.ImageSource()));
}

template<class TPixel>
inline CDVisSequence<TPixel>& CDVisSequence<TPixel>::operator=(
		const CDVisSequence<TPixel>& refsequence)
{
	CVisSequenceBase::operator=(refsequence);
	return *this;
}

template<class TPixel>
inline CDVisSequence<TPixel>::~CDVisSequence(void)
{
	m_IsDying = true;
	SequenceIsDying();
}

template<class TPixel>
inline const CDVisSequence<TPixel>::T_Image&
	CDVisSequence<TPixel>::Front(void) const
{
	return (const CDVisSequence<TPixel>::T_Image&)
			CVisSequenceBase::Front();
}

template<class TPixel>
inline CDVisSequence<TPixel>::T_Image&
	CDVisSequence<TPixel>::Front(void)
{
	return (CDVisSequence<TPixel>::T_Image&)
			CVisSequenceBase::Front();
}

template<class TPixel>
inline const CDVisSequence<TPixel>::T_Image&
	CDVisSequence<TPixel>::Back(void) const
{
	return (const CDVisSequence<TPixel>::T_Image&)
			CVisSequenceBase::Back();
}

template<class TPixel>
inline CDVisSequence<TPixel>::T_Image&
	CDVisSequence<TPixel>::Back(void)
{
	return (CDVisSequence<TPixel>::T_Image&)
			CVisSequenceBase::Back();
}

template<class TPixel>
inline const CDVisSequence<TPixel>::T_Image&
	CDVisSequence<TPixel>::At(int i) const
{
	// old line was not working for CVisApp<> only for CVisImage<> (rf) 
	return (dynamic_cast<const T_Image &> (CVisSequenceBase::At(i)) );

//	return (const CDVisSequence<TPixel>::T_Image&) CVisSequenceBase::At(i);
}

template<class TPixel>
inline CDVisSequence<TPixel>::T_Image&
	CDVisSequence<TPixel>::At(int i)
{
	// old line was not working for CVisApp<> only for CVisImage<> (rf) 
	return (dynamic_cast<T_Image &> (CVisSequenceBase::At(i)) );

	//	return (CDVisSequence<TPixel>::T_Image&) CVisSequenceBase::At(i);
}

template<class TPixel>
inline const CDVisSequence<TPixel>::T_Image&
	CDVisSequence<TPixel>::operator[](int i) const
{
	// old line was not working for CVisApp<> only for CVisImage<> (rf) 
	return (dynamic_cast<const T_Image &> (CVisSequenceBase::At(i)) );

	//	return (const CDVisSequence<TPixel>::T_Image&) CVisSequenceBase::At(i);
}

template<class TPixel>
inline CDVisSequence<TPixel>::T_Image&
	CDVisSequence<TPixel>::operator[](int i)
{
	// old line was not working for CVisApp<> only for CVisImage<> (rf) 
	return (dynamic_cast<T_Image &> (CVisSequenceBase::At(i)) );

	//	return (CDVisSequence<TPixel>::T_Image&) CVisSequenceBase::At(i);
}

template<class TPixel>
inline bool CDVisSequence<TPixel>::GetFront(
		CDVisSequence<TPixel>::T_Image& refimage, DWORD cmsTimeout)
{
	return CVisSequenceBase::GetFront(refimage, cmsTimeout);
}

template<class TPixel>
inline bool CDVisSequence<TPixel>::GetBack(
		CDVisSequence<TPixel>::T_Image& refimage, DWORD cmsTimeout)
{
	return CVisSequenceBase::GetBack(refimage, cmsTimeout);
}

template<class TPixel>
inline bool CDVisSequence<TPixel>::GetAt(int i,
		CDVisSequence<TPixel>::T_Image& refimage, DWORD cmsTimeout)
{
	return CVisSequenceBase::GetAt(i, refimage, cmsTimeout);
}

template<class TPixel>
inline void CDVisSequence<TPixel>::Push(void)
{
	CVisSequenceBase::PushBack();
}

template<class TPixel>
inline void CDVisSequence<TPixel>::Push(
		const CDVisSequence<TPixel>::T_Image& refimage)
{
	CVisSequenceBase::PushBack(refimage);
}

template<class TPixel>
inline void CDVisSequence<TPixel>::Push(const CVisShape& refshape)
{
	CVisSequenceBase::PushBack(refshape);
}

template<class TPixel>
inline bool CDVisSequence<TPixel>::Pop(void)
{
	return CVisSequenceBase::PopFront();
}

template<class TPixel>
inline bool CDVisSequence<TPixel>::Pop(DWORD cmsTimeout)
{
	return CVisSequenceBase::PopFront(cmsTimeout);
}

template<class TPixel>
inline bool CDVisSequence<TPixel>::Pop(
		CDVisSequence<TPixel>::T_Image& refimage, DWORD cmsTimeout)
{
	return CVisSequenceBase::PopFront(refimage, cmsTimeout);
}

template<class TPixel>
inline void CDVisSequence<TPixel>::PushFront(void)
{
	CVisSequenceBase::PushFront();
}

template<class TPixel>
inline void CDVisSequence<TPixel>::PushFront(
		const CDVisSequence<TPixel>::T_Image& refimage)
{
	CVisSequenceBase::PushFront(refimage);
}

template<class TPixel>
inline void CDVisSequence<TPixel>::PushFront(const CVisShape& refshape)
{
	CVisSequenceBase::PushFront(refshape);
}

template<class TPixel>
inline bool CDVisSequence<TPixel>::PopFront(void)
{
	return CVisSequenceBase::PopFront();
}

template<class TPixel>
inline bool CDVisSequence<TPixel>::PopFront(
		CDVisSequence<TPixel>::T_Image& refimage, DWORD cmsTimeout)
{
	return CVisSequenceBase::PopFront(refimage, cmsTimeout);
}

template<class TPixel>
inline void CDVisSequence<TPixel>::PushBack(void)
{
	CVisSequenceBase::PushBack();
}

template<class TPixel>
inline void CDVisSequence<TPixel>::PushBack(
		const CDVisSequence<TPixel>::T_Image& refimage)
{
	CVisSequenceBase::PushBack(refimage);
}

template<class TPixel>
inline void CDVisSequence<TPixel>::PushBack(const CVisShape& refshape)
{
	CVisSequenceBase::PushBack(refshape);
}

template<class TPixel>
inline bool CDVisSequence<TPixel>::PopBack(void)
{
	return CVisSequenceBase::PopBack();
}

template<class TPixel>
inline bool CDVisSequence<TPixel>::PopBack(
		CDVisSequence<TPixel>::T_Image& refimage, DWORD cmsTimeout)
{
	return CVisSequenceBase::PopBack(refimage, cmsTimeout);
}

template<class TPixel>
inline void CDVisSequence<TPixel>::Assign(int n)
{
	CVisSequenceBase::Assign(n);
}

template<class TPixel>
inline void CDVisSequence<TPixel>::Assign(int n,
		const CDVisSequence<TPixel>::T_Image& refimage)
{
	CVisSequenceBase::Assign(n, refimage);
}

template<class TPixel>
inline void CDVisSequence<TPixel>::Assign(int n,
		const CVisShape& refshape)
{
	CVisSequenceBase::Assign(n, refshape);
}

template<class TPixel>
inline void CDVisSequence<TPixel>::Assign(
		const CDVisSequence<TPixel>& refsequence, int iFirst,
		int iLim)
{
	CVisSequenceBase::Assign(refsequence, iFirst, iLim);
}

#ifdef LATER // Build problems
template<class TPixel>
inline void CDVisSequence<TPixel>::Assign(
		CDVisSequence<TPixel>& refsequence, int iFirst,
		int iLim)
{
	CVisSequenceBase::Assign(refsequence, iFirst, iLim);
}
#endif // LATER

template<class TPixel>
inline void CDVisSequence<TPixel>::Resize(int n)
{
	CVisSequenceBase::Resize(n);
}

template<class TPixel>
inline void CDVisSequence<TPixel>::Resize(int n,
		const CDVisSequence<TPixel>::T_Image& refimage)
{
	CVisSequenceBase::Resize(n, refimage);
}

template<class TPixel>
inline void CDVisSequence<TPixel>::Resize(int n,
		const CVisShape& refshape)

{
	CVisSequenceBase::Resize(n, refshape);
}

template<class TPixel>
inline void CDVisSequence<TPixel>::Insert(int i)
{
	CVisSequenceBase::Insert(i);
}

template<class TPixel>
inline void CDVisSequence<TPixel>::Insert(int i, int n)
{
	CVisSequenceBase::Insert(n, n);
}

template<class TPixel>
inline void CDVisSequence<TPixel>::Insert(int i,
		const CVisShape& refshape)
{
	CVisSequenceBase::Insert(i, refshape);
}

template<class TPixel>
inline void CDVisSequence<TPixel>::Insert(int i, int n,
		const CVisShape& refshape)
{
	CVisSequenceBase::Insert(i, n, refshape);
}

template<class TPixel>
inline void CDVisSequence<TPixel>::Insert(int i,
		const CDVisSequence<TPixel>::T_Image& refimage)
{
	CVisSequenceBase::Insert(i, refimage);
}

template<class TPixel>
inline void CDVisSequence<TPixel>::Insert(int i, int n,
		const CDVisSequence<TPixel>::T_Image& refimage)
{
	CVisSequenceBase::Insert(i, n, refimage);
}

template<class TPixel>
inline void CDVisSequence<TPixel>::Insert(int i,
		const CDVisSequence<TPixel>& refsequence, int iFirst, int iLim)

{
	CVisSequenceBase::Insert(i, refsequence, iFirst, iLim);
}

#ifdef LATER // Build problems
template<class TPixel>
inline void CDVisSequence<TPixel>::Insert(int i,
		CDVisSequence<TPixel>& refsequence, int iFirst, int iLim)

{
	CVisSequenceBase::Insert(i, refsequence, iFirst, iLim);
}
#endif // LATER

template<class TPixel>
inline bool CDVisSequence<TPixel>::Swap(int i)
{
	return CVisSequenceBase::Swap(i);
}

template<class TPixel>
inline bool CDVisSequence<TPixel>::Swap(int i, int j)
{
	return CVisSequenceBase::Swap(i, j);
}

template<class TPixel>
inline void CDVisSequence<TPixel>::Swap(
		CDVisSequence<TPixel>& refsequence)
{
	CVisSequenceBase::Swap(refsequence);
}


template<class TPixel>
inline const CDVisSequence<TPixel>::T_Image&
	CDVisSequence<TPixel>::Cur(void) const
{
	return At(ICur());
}

template<class TPixel>
inline CDVisSequence<TPixel>::T_Image&
	CDVisSequence<TPixel>::Cur(void)
{
	return At(ICur());
}

template<class TPixel>
inline bool CDVisSequence<TPixel>::GetCur(
		CDVisSequence<TPixel>::T_Image& refimage)
{
	return GetAt(ICur(), refimage);
}



template<class TPixel>
inline const type_info& CDVisSequence<TPixel>::PixelTypeInfo(void) const
{
	return typeid(TPixel);
}

template<class TPixel>
inline const type_info& CDVisSequence<TPixel>::ImageTypeInfo(void) const
{
	return typeid(CDVisSequence<TPixel>::T_Image);
}

template<class TPixel>
inline const type_info& CDVisSequence<TPixel>::ObjectTypeInfo(int iLevel) const
{
	if (iLevel == 0)
		return typeid(CVisSequenceBase);
	
	return typeid(CDVisSequence<TPixel>);
}

template<class TPixel>
inline CVisSequenceBase *CDVisSequence<TPixel>::Clone(bool fCopyThis) const
{
	if (fCopyThis)
		return new CDVisSequence<TPixel>(*this);

	return new CDVisSequence<TPixel>;
}



template<class TPixel>
inline CVisImageBase *CDVisSequence<TPixel>::PImageNew(void) const
{
	return new CDVisSequence<TPixel>::T_Image;
}

template<class TPixel>
inline CVisImageBase *CDVisSequence<TPixel>::PImageNew(
		const CVisShape& refshape) const
{
	return new CDVisSequence<TPixel>::T_Image(refshape);
}



template<class TPixel>
inline CVisImageBase *CDVisSequence<TPixel>::PImageNew(
		const CVisImageBase& refimage) const
{
	assert(&refimage != 0);
	assert(refimage.PixelTypeInfo() == PixelTypeInfo());

	// get CVisImage reference and assert (rf)
	const CDVisSequence<TPixel>::T_Image& refimageapp = dynamic_cast<const CDVisSequence<TPixel>::T_Image &> (refimage);
	assert(&refimageapp != 0);

	return new CDVisSequence<TPixel>::T_Image(refimageapp);
	// old line not working for CVisApp<> only for CVisImage<> (rf) 
	// return new CDVisSequence<TPixel>::T_Image(const CDVisSequence<TPixel>::T_Image &) refimage);
}


//
template<class TPixel>
inline EVisPixFmt CDVisSequence<TPixel>::EVisPixFmtGet(void)
{
	static EVisPixFmt evispixfmt = evispixfmtNil;

	if (evispixfmt == evispixfmtNil)
	{
		evispixfmt = VisPixFmtGetTPixel(TPixel());
	}

	return evispixfmt;
}


template<class TPixel>
inline void CDVisSequence<TPixel>::AddCallback(T_PfnCallback pfnCallback,
		void *pvUser, bool fCallOnChange, bool fCallWhenDying)
{
	SCallbackInfo callbackinfo;
	callbackinfo.m_pfnCallback = pfnCallback;
	callbackinfo.m_pvUser = pvUser;
	callbackinfo.m_fCallOnChange = fCallOnChange;
	callbackinfo.m_fCallWhenDying = fCallWhenDying;

	m_rgcallback.push_back(callbackinfo);
}


template<class TPixel>
inline void CDVisSequence<TPixel>::RemoveCallback(T_PfnCallback pfnCallback)
{
	T_Rgcallback::iterator iteratorCur = m_rgcallback.begin();
	T_Rgcallback::iterator iteratorLim = m_rgcallback.end();
	for (; iteratorCur != iteratorLim; ++iteratorCur)
	{
		if (iteratorCur->m_pfnCallback == pfnCallback)
		{
			m_rgcallback.erase(iteratorCur);
			break;
		}
	}
}


template<class TPixel>
inline bool CDVisSequence<TPixel>::IsDying(void) const
{
	return m_IsDying;
}


template<class TPixel>
inline void CDVisSequence<TPixel>::SequenceChanged(void)
{
	T_Rgcallback::iterator iteratorCur = m_rgcallback.begin();
	T_Rgcallback::iterator iteratorLim = m_rgcallback.end();
	for (; iteratorCur != iteratorLim; ++iteratorCur)
	{
		assert(iteratorCur->m_pfnCallback != 0);
		if (iteratorCur->m_fCallOnChange)
		{
			((T_PfnCallback) (iteratorCur->m_pfnCallback))(*this,
					iteratorCur->m_pvUser);
		}
	}
}


template<class TPixel>
inline void CDVisSequence<TPixel>::SequenceIsDying(void)
{
	T_Rgcallback::iterator iteratorCur = m_rgcallback.begin();
	T_Rgcallback::iterator iteratorLim = m_rgcallback.end();
	for (; iteratorCur != iteratorLim; ++iteratorCur)
	{
		assert(iteratorCur->m_pfnCallback != 0);
		if (iteratorCur->m_fCallWhenDying)
		{
			((T_PfnCallback) (iteratorCur->m_pfnCallback))(*this,
					iteratorCur->m_pvUser);
		}
	}
}

#endif // __VIS_APP_IMAGEQUEUE_INL__
