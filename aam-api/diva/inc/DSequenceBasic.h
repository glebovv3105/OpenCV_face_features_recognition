/////////////////////////////////////////////////////////////////////////////
//
// This sequence provides some basic sequence functions
//
// Copyright � 1999
//
// IMM, Department of Mathematical Modelling
// Section for Image Analysis
// Technical University of Denmark, Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/~diva
//
// author: Rune Fisker 
//
// Disclaimer:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on the Microsoft Vision Software Developers Kit VisSDK
//
/////////////////////////////////////////////////////////////////////////////

#include "DVisSequence.h"
//#include "StdAfx.h"
#include "DImageApp.h"

#if !defined(CDVIS_SEQUENCE_BASIC)
#define CDVIS_SEQUENCE_BASIC

template <class TPixel>
class CDSequenceBasic : public virtual CDVisSequence<TPixel> 
{
public:
	// reads image seq.
	void ReadFile(SVisFileDescriptor& filedescriptorT);

	// write image seq.
	void WriteFile(SVisFileDescriptor& filedescriptorT);

	// insert a copy of the image
	void InsertCopy(int i, const T_Image& refimage) { T_Image image(refimage.MemoryShape()); image.Copy(refimage); Insert(i,image); }

	// insert a copy of the full image indenpendtly of the actual ROI
	void InsertCopyFull(int i, T_Image& refimage);

	/////////////////////////////////////////////////////////////////////////////////////////////
	// Region Of Interest functions
	void SetRect(CRect rect) {  for (int i=0;i<Length();i++) At(i).SetRect(rect); }
	void ClearRect() { for (int i=0;i<Length();i++) At(i).SetRect(At(i).MemoryShape()); }

	/////////////////////////////////////////////////////////////////////////////////////////////
	// History functions
	void History(CString& strHistory);
	

private:

};

#include "DSequenceBasic.inl"

#endif // CDVIS_SEQUENCE_BASIC