/////////////////////////////////////////////////////////////////////////////
//
// Image class containning basic image analysis functions
//
// Copyright � 1998
//
// IMM, Department of Mathematical Modelling
// Technical University of Denmark, Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk
//
// author: Rune Fisker 
//
// Disclaimer:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on the Microsoft Vision Software Developers Kit VisSDK
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(CD_IMAGE_BASIC)
#define CD_IMAGE_BASIC

#include <DImageStatistic.h>
#include <math.h>
#include <DMatrix.h>

template <class TPixel>
class CDImageBasic : public virtual CDImageStatistic<TPixel>
{  
public:
	//------------------------------------------------------------------
	// @group Constructors

	// @cmember
	// Default constructor.  The image will not be useable until another
	// image is assigned to it.
	CDImageBasic(void){};

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageBasic(int width, int height, int nbands = 1,
		int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(width,height,nbands,imopts,pbData){};

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageBasic(SIZE size, int nbands = 1,
			int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(size,nbands,imopts,pbData){};

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageBasic(const RECT& rect, int nbands = 1,
			int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(rect,nbands,imopts,pbData){};

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageBasic(const CVisShape& shape,
			int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(shape,imopts,pbData){};

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageBasic(CVisMemBlock& refmemblock, int width, int height,
			int nbands = 1, int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,width,height,nbands,imopts){};

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageBasic(CVisMemBlock& refmemblock, SIZE size, int nbands = 1,
			int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,size,nbands,imopts){};

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageBasic(CVisMemBlock& refmemblock, const RECT& rect,
			int nbands = 1, int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,rect,nbands,imopts){};

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageBasic(CVisMemBlock& refmemblock, const CVisShape& shape,
			int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,shape,imopts){};

	// @cmember
	// Construct an image from another image.
	// Warning:  Derived classes should provide their own copy constructor
	// that first constructs a default object and then uses the assignment
	// operator to synchronize  image creation, assignment, and
	// destruction.  Derived classes should not call the copy constructor
	// in the base class.
	CDImageBasic(const CDImageBasic<TPixel>& imageSrc)
		:CVisImage<TPixel>(imageSrc){};

	// Remove labeled blobs closer to the border that a given distance
	void BlobRemoveBorder(int nDist);
		
	// Calc. the first order moment for labeled blobs
	void BlobFirstMoment(CVisDVector& vXC, CVisDVector& vYC);
	
	// graphics
	bool DIB2Image(LPVOID pDIB);
	LPVOID Image2DIB(int &nSizeDIB); // returns DIB created from the image
	void DrawLine(int x1 ,int y1 ,int x2 ,int y2, TPixel tVal, bool fInsideCheck = false);
	void DrawRect(CRect rFrame, TPixel tVal);

	// Resize
	void EnlargePyr(int nFactor = 2);
	void ReducePyr(int nFactor = 2);

	// flip vertical or horizontal respectively
	void FlipVertical();
	void FlipHorizontal();

	
	// label
	void Label(); 

	// threshold
	void Threshold(TPixel); 
	void ThresholdInv(TPixel); 
	void ThresholdSoft(TPixel); 
	void ThresholdSoftInv(TPixel); 
	
	// math functions
	void Sqr();
	void Sqrt();
	void Log();
    void Exp();

	// operator overloads
	CDImageBasic<TPixel> operator+(const double dbl);
	CDImageBasic<TPixel> operator+(const CDImageBasic<TPixel>& imageSrc) const;
	CDImageBasic<TPixel> operator-(const double dbl);
	CDImageBasic<TPixel> operator*(const double dbl);
	CDImageBasic<TPixel> operator/(const double dbl);
	CDImageBasic<TPixel>& operator/=(const double dbl);
	CDImageBasic<TPixel>& operator/=(const CDImageBasic<TPixel>& image);
	CDImageBasic<TPixel>& operator+=(const double dbl);
	CDImageBasic<TPixel>& operator+=(const CDImageBasic<TPixel>& image);
	CDImageBasic<TPixel>& operator-=(const double dbl);
	CDImageBasic<TPixel>& operator-=(const CDImageBasic<TPixel>& image);
	CDImageBasic<TPixel>& operator*=(const double dbl);
	CDImageBasic<TPixel>& operator*=(const CDImageBasic<TPixel>& image);


	// The default colormap for 4-bit grayscale images.
	static const unsigned long s_mpbulNibbleColorMapGrayBasic[16];
	// The default colormap for 4-bit color images.
	static const unsigned long s_mpbulNibbleColorMapDefaultBasic[16];
	// The default colormap for 8-bit grayscale images.
	static const unsigned long s_mpbulByteColorMapGrayBasic[256];

// sfj code begins
	inline double PixelI(float x, float y) const;	// Default interpolation

	inline TPixel Pixel0(float x, float y) const;	// Nearest neighbour interpolation
	inline double Pixel1(float x, float y) const;	// Bilinear interpolation
	inline double Pixel2(float x, float y) const;	// Biquadratic interpolation
	inline double Pixel3(float x, float y) const;	// Bicubic interpolation
	inline double CB(TPixel y1, TPixel y2, double d) const;
	inline double CQ(TPixel y1, TPixel y2, TPixel y3, double d) const;
	inline double CC(TPixel y1, TPixel y2, TPixel y3, TPixel y4, double d) const;
	inline double CC2(TPixel y1, TPixel y2, TPixel y3, TPixel y4, double d) const;

// SFJ code ends


    //////////////////////////////
    // Klaus B. Hilger code
    //////////////////////////////
    void Invert();
    void Brightness();


	//linear mappings
	void GeneralLinMap(const double& dG, const double& dB);
	void MeanSTDLinMap(const double& dDesiredMean, const double& dDesiredSTD);
	void MaxMinLinMap(const double& dDesiredMax, const double& dDesiredMin);

	//non-linear mappings
	void GammaMapping(const double& dG);
	void HyperbolicMapping(const double& dK);
	void LogarithmicMapping(const double& dK);
	void ExponentialMapping(const double& dK);

	void Histogram(const int& iBins, const double& dMin, const double& dMax, CDVector& vHistogram);
	void BetaHistogram(CDVector& vHistogram, CDVector& vCumHistogram, const int iBins, const double& dAlpha, const double& dBeta); 
	void HistogramMatching(CDVector& vRefHistogram, CDVector& vCumRefHistogram, CDVector& vImageHistogram, const double& dAlpha, const double& dBeta);

	void BetaHistogramMatching(const double& dMin, const double& dMax,const CDVector& vOrgHistogram, const double& dAlpha, const double& dBeta);

private:

	void Swap(int& x, int& y) {int tmp = x; x = y; y = tmp;}
};



//
//  Global template operator and function overloads for CVisRGBA pixels
//
//  Functions: sqrt, log, exp, pow 
//
//  Operators: /, *
//
//  The alpha channel are left untouched in all functions.
//
template<typename TNum> inline CVisRGBA<TNum> log( const CVisRGBA<TNum> &p ) {
   
    return CVisRGBA<TNum>( log( p.R() ), log( p.G() ), log( p.B() ), p.A() );
}

template<typename TNum> inline CVisRGBA<TNum> exp( const CVisRGBA<TNum> &p ) {

    return CVisRGBA<TNum>( exp( p.R() ), exp( p.G() ), exp( p.B() ), p.A() );
}

template<typename TNum> inline CVisRGBA<TNum> pow( const CVisRGBA<TNum> &p, const double y ) {

    return CVisRGBA<TNum>( pow( p.R(), y ), pow( p.G(), y ), pow( p.B(), y ), p.A() );
}

template<typename TNum> inline CVisRGBA<TNum> sqrt( const CVisRGBA<TNum> &p, const double y ) {

    return CVisRGBA<TNum>( sqrt( p.R(), y ), sqrt( p.G(), y ), sqrt( p.B(), y ), p.A() );
}

template<typename TNum> inline CVisRGBA<TNum> operator/( const CVisRGBA<TNum> &p1, const CVisRGBA<TNum> &p2 ) {

     return CVisRGBA<TNum>( p1.R()/p2.R(), p1.G()/p2.G(), p1.B()/p2.B(), p1.A()/p2.A() );
}

template<typename TNum> inline CVisRGBA<TNum> operator*( const CVisRGBA<TNum> &p1, const CVisRGBA<TNum> &p2 ) {

     return CVisRGBA<TNum>( p1.R()*p2.R(), p1.G()*p2.G(), p1.B()*p2.B(), p1.A()*p2.A() );
}

// in-line include (due to templated classes)
#include <DImageBasic.inl>

#endif // CD_IMAGE_BASIC
