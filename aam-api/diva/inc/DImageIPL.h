/////////////////////////////////////////////////////////////////////////////
//
// Image class for personale functions
//
// Copyright � 1998
//
// IMM, Department of Mathematical Modelling
// Technical University of Denmark, Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk
//
// author: Rune Fisker 
//
// Disclaimer:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on the Microsoft Vision Software Developers Kit VisSDK
//
/////////////////////////////////////////////////////////////////////////////


#if !defined(CD_IMAGE_IPL)
#define CD_IMAGE_IPL

#include <IPL.h>
#include <DMatrix.h>
#include <DImageInitial.h>

template <class TPixel>
class CDImageIPL : public virtual CDImageInitial<TPixel>
{
public:
	//------------------------------------------------------------------
	// @group Constructors

	// @cmember
	// Default constructor.  The image will not be useable until another
	// image is assigned to it.
	CDImageIPL(void){}

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageIPL(int width, int height, int nbands = 1,
		int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(width,height,nbands,imopts,pbData){}

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageIPL(SIZE size, int nbands = 1,
			int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(size,nbands,imopts,pbData){}

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageIPL(const RECT& rect, int nbands = 1,
			int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(rect,nbands,imopts,pbData){}

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageIPL(const CVisShape& shape,
			int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(shape,imopts,pbData){}

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageIPL(CVisMemBlock& refmemblock, int width, int height,
			int nbands = 1, int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,width,height,nbands,imopts){}

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageIPL(CVisMemBlock& refmemblock, SIZE size, int nbands = 1,
			int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,size,nbands,imopts){}

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageIPL(CVisMemBlock& refmemblock, const RECT& rect,
			int nbands = 1, int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,rect,nbands,imopts){}

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageIPL(CVisMemBlock& refmemblock, const CVisShape& shape,
			int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,shape,imopts){}

	// @cmember
	// Construct an image from another image.
	// Warning:  Derived classes should provide their own copy constructor
	// that first constructs a default object and then uses the assignment
	// operator to synchronize  image creation, assignment, and
	// destruction.  Derived classes should not call the copy constructor
	// in the base class.
	CDImageIPL(const CDImageIPL<TPixel>& imageSrc)
		:CVisImage<TPixel>(imageSrc){}

	//////////////////////////////////////////////////////////////////////////////
	//
	// image manipulation functions
	//
	//////////////////////////////////////////////////////////////////////////////

	IplImage* Image2IplImage();

	void MirrorIpl(int nFlipAxis);

	void SetBorderModeIpl(IplImage *src, int mode, int border, int constVal);
	void SetBorderModeIpl(IplImage *src);

	void ThresholdIpl(int nThreshold);

	void WarpBilinearIpl(CDVector& va, CDVector& vb, int nWarpFlag, int nInterpolate);

	// linear filtering functions
	void BlurIpl(int nCols, int nRows);
	void FixedFilterIpl(IplFilter filter);

	IplConvKernelFP* CreateConvKernelFPIpl(const CVisImage<float>& imageFilter);

	void Convolve2DFPIpl(const CVisImage<float>& imageFilter); 
	void Convolve2DFPIpl(const CVisSequence<float>& seqFilters, int iCombineMethod); 

    void RemapIpl(CDImageIPL<CVisFloatPixel>& dx, CDImageIPL<CVisFloatPixel>& dy,
				  const int nInterpolate,
				  CDImageIPL<CVisBytePixel>& destImage);
};

#include <vector> // for OnTools

#include "DImageIPL.inl"




#endif // CD_IMAGE_IPL