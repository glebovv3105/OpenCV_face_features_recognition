/////////////////////////////////////////////////////////////////////////////
//
// Initail image class. All DIVA image classes should be derived from this
// class, i.e. this class is placed direct after CVisImage in the class tree
//
// Copyright � 1999
//
// IMM, Department of Mathematical Modelling
// Technical University of Denmark, Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk
//
// author: Rune Fisker 
//
// Disclaimer:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on the Microsoft Vision Software Developers Kit VisSDK
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(CD_IMAGE_INITIAL)
#define CD_IMAGE_INITIAL

#include <math.h>
#include "VisCore.h"

/////////////////////////////////////////////////////////////////////////////
//
//	DIVA Error Codes for various errors that might occur, which are not cover 
//	by the VisSDK enum EVisError
//
//	author:	Rune Fisker 21/4-1999
enum EDVisError
{
	// window should be odd integer
	edviserrorNotOddWindow		=	1000,

	// function cancel by user
	edviserrorCancelByUser		=	1001,

	// function cancel by user
	edviserrorDivisionByZero	=	1002

};

template <class TPixel>
class CDImageInitial : public virtual CVisImage<TPixel>
{
public:
	//------------------------------------------------------------------
	// @group Constructors

	// @cmember
	// Default constructor.  The image will not be useable until another
	// image is assigned to it.
	CDImageInitial(void){m_evispad = evispadZero; m_fHistory = true; m_dXResolution = 0; m_dYResolution = 0; m_dZResolution = 0;}

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageInitial(int width, int height, int nbands = 1,
		int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(width,height,nbands,imopts,pbData){m_evispad = evispadZero;m_fHistory = true;m_dXResolution = 0; m_dYResolution = 0; m_dZResolution = 0;}

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageInitial(SIZE size, int nbands = 1,
			int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(size,nbands,imopts,pbData){m_evispad = evispadZero;m_fHistory = true;m_dXResolution = 0; m_dYResolution = 0; m_dZResolution = 0;}

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageInitial(const RECT& rect, int nbands = 1,
			int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(rect,nbands,imopts,pbData){m_evispad = evispadZero;m_fHistory = true;m_dXResolution = 0; m_dYResolution = 0; m_dZResolution = 0;}

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageInitial(const CVisShape& shape,
			int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(shape,imopts,pbData){m_evispad = evispadZero;m_fHistory = true;m_dXResolution = 0; m_dYResolution = 0; m_dZResolution = 0;}

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageInitial(CVisMemBlock& refmemblock, int width, int height,
			int nbands = 1, int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,width,height,nbands,imopts){m_evispad = evispadZero;m_fHistory = true;m_dXResolution = 0; m_dYResolution = 0; m_dZResolution = 0;}

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageInitial(CVisMemBlock& refmemblock, SIZE size, int nbands = 1,
			int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,size,nbands,imopts){m_evispad = evispadZero;m_fHistory = true;m_dXResolution = 0; m_dYResolution = 0; m_dZResolution = 0;}

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageInitial(CVisMemBlock& refmemblock, const RECT& rect,
			int nbands = 1, int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,rect,nbands,imopts){m_evispad = evispadZero;m_fHistory = true;m_dXResolution = 0; m_dYResolution = 0; m_dZResolution = 0;}

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageInitial(CVisMemBlock& refmemblock, const CVisShape& shape,
			int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,shape,imopts){m_evispad = evispadZero;m_fHistory = true;m_dXResolution = 0; m_dYResolution = 0; m_dZResolution = 0;}

	// @cmember
	// Construct an image from another image.
	// Warning:  Derived classes should provide their own copy constructor
	// that first constructs a default object and then uses the assignment
	// operator to synchronize  image creation, assignment, and
	// destruction.  Derived classes should not call the copy constructor
	// in the base class.
	CDImageInitial(const CDImageInitial<TPixel>& imageSrc)
		:CVisImage<TPixel>(imageSrc){m_evispad = evispadZero;m_fHistory = true;m_dXResolution = 0; m_dYResolution = 0; m_dZResolution = 0;}

	//////////////////////////////////////////////////////////////////////////////
	//
	// general non image analysis functions, which are used by other image classes
	//
	//////////////////////////////////////////////////////////////////////////////

	// history functions
	bool HistoryEnable() { return m_fHistory; }
	void SetHistoryEnable(bool fHistory) { m_fHistory = fHistory; }

	const CString History() const {return m_sHistory; }
	void SetHistory(const CString& sHistory) {if (m_fHistory) m_sHistory = sHistory; }
	void AddToHistory(const CString& sHistory) {if (m_fHistory) m_sHistory += sHistory + "\r\n"; }
	void ClearHistory() {m_sHistory = ""; }

	// border mode funtions
	EVisPad BorderMode() const { return m_evispad; }
	void SetBorderMode(EVisPad evispad) { m_evispad = evispad;}

	// copy and conversion rutines
	bool CopyFull(CVisImageBase& imagebase, EVisNormalize evisnormalize);
	void CopyFull(CVisImageBase& imagebase);
	bool CopyFullPixelsTo(CVisImageBase& refimageDest, EVisNormalize evisnormalize = evisnormalizeDefault, double dblMin = 1.0, double dblMax = - 1.0) const;
	bool IntensityFromRGBA(CVisImageBase& imagebase);

	// size of the pixels in bytes
	int SizeofPixel() {return sizeof(TPixel);}

	// Gets/Sets Resolution
	double XResolution() const {return m_dXResolution;}
	double YResolution() const {return m_dYResolution;}
	double ZResolution() const {return m_dZResolution;}

	void SetXResolution(const double dXResolution) {m_dXResolution=dXResolution;}
	void SetYResolution(const double dYResolution) {m_dYResolution=dYResolution;}
	void SetZResolution(const double dZResolution) {m_dZResolution=dZResolution;}

	// copy the attributes in this class
	void CopyInitialAttributes(const CDImageInitial<TPixel> &image);

private:
	// default border mode
	EVisPad m_evispad;

	// history 
	CString m_sHistory;
	bool m_fHistory;

	// Pixel and greyscale resolution in meters (standard internationale)
	double m_dXResolution, m_dYResolution, m_dZResolution;
};

#include "DImageInitial.inl"

#endif // CD_VIS_INITIAL
