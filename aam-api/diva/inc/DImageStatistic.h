/////////////////////////////////////////////////////////////////////////////
//
// Image class containing statistical function
//
// Copyright � 1998
//
// IMM, Department of Mathematical Modelling
// Technical University of Denmark, Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk
//
// author: Rune Fisker 
//
// Disclaimer:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on the Microsoft Vision Software Developers Kit VisSDK
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(CD_IMAGE_STATISTIC)
#define CD_IMAGE_STATISTIC

#include <DImageIPL.h>

template <class TPixel>
class CDImageStatistic : public virtual CDImageIPL<TPixel>
{
public:
	void CentralMomentPixel(double& dMoment, int nOrder);
	void CentralMomentPixel(CVisRGBADoublePixel& dMoment, int nOrder);

	TPixel Max();
	void Mean(double& dMean);
	void Mean(CVisRGBADoublePixel& rgbMean);
	TPixel Min();

	void Skewness(double& dSkewness);
	void Std(double& dStd);
	void Var(double& dVar);
};

#include <DImageStatistic.inl>

#endif // CVISSTATISTIC
