/////////////////////////////////////////////////////////////////////////////
//
// This file contains the definition for Rosenbrock test class. 
// The class is used to test the optimization methods
//
// Copyright � 1999
//
// IMM, Department of Mathematical Modelling
// Technical University of Denmark, Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/~diva
//
// author: Rune Fisker, 26/1-1999
//
// Disclaimer:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on the Microsoft Vision Software Developers Kit VisSDK
//
/////////////////////////////////////////////////////////////////////////////


#if !defined(CD_OPTIMIZE_ROSENBROCK)
#define CD_OPTIMIZE_ROSENBROCK

#include "DOptimizeBase.h"

// test values
// initial x0 = -1.2, x1 = 1
// optimal x0* = 1, x1* = 1 

class CDOptimizeRosenbrock: public CDOptimizeFuncBase
{
	// evaluates the function value at postion vX
	double EvalFunction(CDVector& vX);

	// Gradient of Rosenbrock test function
	void EvalGradient(CDVector& vX, CDVector& vGradient);
};


// Rosenbrock test function (Function to be minimized)
inline double CDOptimizeRosenbrock::EvalFunction(CDVector& vX)
{	
	return (100.0*pow(vX[1] - vX[0]*vX[0],2)+pow(1.0 - vX[0],2)); // Rosenbrock's function
}


// Gradient of Rosenbrock test function
inline void CDOptimizeRosenbrock::EvalGradient(CDVector& vX, CDVector& vGradient)
{  
	// Gradient of the Rosenbrock's function
	vGradient[0] = -400.0*vX[0]*( vX[1] - vX[0]*vX[0] ) - 2.0*( 1.0 - vX[0] );
	vGradient[1] =  200.0*     ( vX[1] - vX[0]*vX[0] );
}

#endif // CD_OPTIMIZE_ROSENBROCK