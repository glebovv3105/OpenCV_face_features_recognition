/////////////////////////////////////////////////////////////////////////////
//
// This sequence is (alomost) identical to the VisSDK CVisSequence with the 
// exception, that this sequence handle CDImageApp images
//
// Copyright � 1999
//
// IMM, Department of Mathematical Modelling
// Section for Image Analysis
// Technical University of Denmark, Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/~diva
//
// author: Rune Fisker 
//
// Disclaimer:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on the Microsoft Vision Software Developers Kit VisSDK
//
/////////////////////////////////////////////////////////////////////////////

#include "DImageApp.h"

#ifndef __VIS_APP_SEQUENCE_H__
#define __VIS_APP_SEQUENCE_H__

template<class TPixel>
class CDVisSequence : public CVisSequenceBase
{
public:
    //------------------------------------------------------------------
	// @group Type definitions

//	typedef TPixel					T_Pixel;
	typedef CDImageApp<TPixel>		T_Image;
//	typedef CVisImage<TPixel>		T_Image;
	typedef void (VISAPI *T_PfnCallback)
			(CDVisSequence<TPixel>& refsequence, void *pvUser);


    //------------------------------------------------------------------
	// @group Constructors, assignement operator, and destructors

	// Note:  Use nLengthMax == - 1 to place no limits on the sequence length.
	CDVisSequence(int nLength = - 1, int evissequence = evissequenceDefault);
	CDVisSequence(int nLength, const T_Image& refimage,
			int evissequence = evissequenceDefault);
	CDVisSequence(int nLength, const CVisShape& refshape,
			int evissequence = evissequenceDefault);
	CDVisSequence(const CDVisSequence<TPixel>& refsequence,
			int iFirst = 0, int iLim = - 1);
	CDVisSequence(const CDVisSequence<TPixel>& refsequence,
			int iFirst, int iLim, int evissequence);
	CDVisSequence<TPixel>& operator=(
			const CDVisSequence<TPixel>& refsequence);
	virtual ~CDVisSequence(void);	

    //------------------------------------------------------------------
	// @group Synchronized image access

	// These methods do synchronize threads accessing the sequence.
	// With internal thread synchronization, these can't be const methods.
	bool GetFront(T_Image& refimage, DWORD cmsTimeout = ecmsTimeoutDefault);
	bool GetBack(T_Image& refimage, DWORD cmsTimeout = ecmsTimeoutDefault);
	bool GetAt(int i, T_Image& refimage, DWORD cmsTimeout = ecmsTimeoutDefault);


    //------------------------------------------------------------------
	// @group Methods based on the std::deque methods

	// Warning:  These methods do not synchronize threads accessing the
	// sequence.  They may not work right (and may cause access violations) if
	// called when other threads are modifying the sequence.  Bad things could
	// also happen if the image specified is not in the sequence.
	const T_Image& Front(void) const;
	T_Image& Front(void);
	const T_Image& Back(void) const;
	T_Image& Back(void);
	const T_Image& At(int i) const;
	T_Image& At(int i);
	const T_Image& operator[](int i) const;
	T_Image& operator[](int i);

	void Push(void);
	void Push(const T_Image& refimage);
	void Push(const CVisShape& refshape);
	bool Pop(void);
	bool Pop(DWORD cmsTimeout);
	bool Pop(T_Image& refimage, DWORD cmsTimeout = ecmsTimeoutDefault);

	void PushFront(void);
	void PushFront(const T_Image& refimage);
	void PushFront(const CVisShape& refshape);
	bool PopFront(void);
	bool PopFront(DWORD cmsTimeout);
	bool PopFront(T_Image& refimage, DWORD cmsTimeout = ecmsTimeoutDefault);

	void PushBack(void);
	void PushBack(const T_Image& refimage);
	void PushBack(const CVisShape& refshape);
	bool PopBack(void);
	bool PopBack(DWORD cmsTimeout);
	bool PopBack(T_Image& refimage, DWORD cmsTimeout = ecmsTimeoutDefault);

	void Assign(int n);
	void Assign(int n, const T_Image& refimage);
	void Assign(int n, const CVisShape& refshape);
	void Assign(const CDVisSequence<TPixel>& refsequence,
			int iFirst = 0, int iLim = - 1);
#ifdef LATER // Build problems
	void Assign(CDVisSequence<TPixel>& refsequence,
			int iFirst = 0, int iLim = - 1);
#endif // LATER

	// Should the name of this method be something like SetLength?
	// Why does this take an object while Assign takes a reference?
	void Resize(int n);
	void Resize(int n, const T_Image& refimage);
	void Resize(int n, const CVisShape& refshape);

	void Insert(int i);
	void Insert(int i, int n);
	void Insert(int i, const CVisShape& refshape);
	void Insert(int i, int n, const CVisShape& refshape);
	void Insert(int i, const T_Image& refimage);
	void Insert(int i, int n, const T_Image& refimage);
	void Insert(int i, const CDVisSequence<TPixel>& refsequence,
			int iFirst = 0, int iLim = - 1);
#ifdef LATER // Build problems
	void Insert(int i, CDVisSequence<TPixel>& refsequence,
			int iFirst = 0, int iLim = - 1);
#endif // LATER

    bool Swap(int i);
    bool Swap(int i, int j);
	void Swap(CDVisSequence<TPixel>& refsequence);


    //------------------------------------------------------------------
	// @group The current image

	const T_Image& Cur(void) const;
	T_Image& Cur(void);

	bool GetCur(T_Image& refimage);


    //------------------------------------------------------------------
	// @group C++ type information

	virtual const type_info& PixelTypeInfo(void) const;
	virtual const type_info& ImageTypeInfo(void) const;
	virtual const type_info& ObjectTypeInfo(int iLevel = - 1) const;

	virtual CVisSequenceBase *Clone(bool fCopyThis = true) const;


    //------------------------------------------------------------------
	// @group Notification callback functions

	// Notification callbacks (called when sequence is changed or destroyed).
	void AddCallback(T_PfnCallback pfnCallback, void *pvUser = 0,
			bool fCallOnChange = true, bool fCallWhenDying = true);
	void RemoveCallback(T_PfnCallback pfnCallback);

	bool IsDying(void) const;

protected:
	virtual CVisImageBase *PImageNew(void) const;
	virtual CVisImageBase *PImageNew(const CVisShape& refshape) const;
	virtual CVisImageBase *PImageNew(const CVisImageBase& refimage) const;

	// The default implementations of these methods call the notification
	// callback functions.
	virtual void SequenceChanged(void);
	virtual void SequenceIsDying(void);

private:
	// UNDONE (in Rel1):  The list of callback functions should really be stored
	// and managed in the base class.
	typedef std::vector<SVisSequenceCallbackInfo> T_Rgcallback;

	bool m_IsDying;
	T_Rgcallback m_rgcallback;

	static EVisPixFmt EVisPixFmtGet(void);
};


#include "DVisSequence.inl"

#endif // __VIS_APP_SEQUENCE_H__

