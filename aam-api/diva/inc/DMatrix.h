//****************************************************************************
//
// Copyright � 1999-2001 by Informatics & Mathmatical Modelling, 
//                          Section for Image Analysis
// 
// IMM, Informatics & Mathmatical Modelling
// Technical University of Denmark
// Richard Petersens Plads
// Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/~diva/
//
// Original author: Rune Fisker
//
// Current author:  Mikkel B. Stegmann - mbs@imm.dtu.dk
//
// Contributions:   Lars Pedersen - lap@imm.dtu.dk, 
//                  Henrik Aanaes - haa@imm.dtu.dk, 
//                  + several other peoples at IMM.
//
// DISCLAIMER:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on;
//
//		- The Microsoft Vision Software Developers Kit, VisSDK
//
//****************************************************************************
#if !defined(__CD_MATRIX__)
#define __CD_MATRIX__

#ifndef DIVAMatrixExport
#define DIVAMatrixExport __declspec(dllimport)
#endif // DIVAMatrixExport

// VisSDK includes
#include "VisMatrix.h"


// on other platforms than Win(MFC) CDString will 
// have to be reimplemented (or changed to an stl string).
typedef CString CDString;

// forward declarations
class CDMatrix;



/**

  @author   Rune Fisker, Mikkel B. Stegmann, 
            Henrik Aan�s, Lars Pedersen et al.

  @version  2-5-2001

  @memo     Vector class with double precision.

  @doc      Vector class with double precision based upon 
            the VisSDK vector class CVisDVector.

  @see      CDMatrix
    
*/
class CDVector : public virtual CVisDVector
{
public:
    // Constructors (default destructor and copy constructor)
    DIVAMatrixExport CDVector(void):CVisDVector(){};
    DIVAMatrixExport CDVector(int length, double *storage = 0):CVisDVector(length,storage) {};
    DIVAMatrixExport CDVector(const CDVector &vec):CVisDVector(vec) {};
    DIVAMatrixExport CDVector(const CVisDVector &vec):CVisDVector(vec) {};

    // Assignment
    DIVAMatrixExport CDVector& operator=(const CVisDVector &vIn);
	DIVAMatrixExport CDVector& operator=(const CDVector &vIn);
    DIVAMatrixExport CDVector& operator=(double value);

    // Statistical functions
    DIVAMatrixExport double AutoCorrelation( const int lag = 1 ) const;
    DIVAMatrixExport double Max() const;
	DIVAMatrixExport double Mean() const;
	DIVAMatrixExport double Median() const;
	DIVAMatrixExport CDVector Trim( const double percentage = .10 ) const;
	DIVAMatrixExport double TrimmedMean( const double percentage = .10 ) const;
	DIVAMatrixExport double TrimmedVar( const double percentage = .10 ) const;
	DIVAMatrixExport double TrimmedStd( const double percentage = .10 ) const;
	DIVAMatrixExport double Min() const;
	DIVAMatrixExport double Min(int& iPos) const;
	DIVAMatrixExport double Skewness() const;
	DIVAMatrixExport double Std() const;	
    DIVAMatrixExport double Sum() const;
    DIVAMatrixExport double Var() const { return this->Var( NULL ); }
	DIVAMatrixExport double Var( double *pMean ) const;

    DIVAMatrixExport void Rand();
	DIVAMatrixExport void Rand( const int st, const int end );
    DIVAMatrixExport void Ceil();
    DIVAMatrixExport void Floor();
    DIVAMatrixExport void Round();
    DIVAMatrixExport void Linspace( const double x1, const double x2, const int n );


    // Fitting
    DIVAMatrixExport void AlignTo( const CDVector &v, double *pA = NULL, double *pB = NULL );

    // Sorting
    DIVAMatrixExport void Sort( bool ascending = true );

    // radomization
    DIVAMatrixExport void Shuffle();


    // basic functions    
	DIVAMatrixExport CDString ToString(const bool fNewline = true) const;

	DIVAMatrixExport void ElementMultiply(const CDVector& vector);
	DIVAMatrixExport void ElementDivide(const CDVector& vector);
		
	DIVAMatrixExport void Normalize2();

    // norms
    DIVAMatrixExport double Norm1() const;
	DIVAMatrixExport double Norm2() const;
    DIVAMatrixExport double NormInf() const;    

    // math functions
    DIVAMatrixExport void Abs();
    DIVAMatrixExport void Clamp( const double min, const double max );
	DIVAMatrixExport void Pow(double dP);
	DIVAMatrixExport void Sqrt();
	DIVAMatrixExport void Sqr();
    DIVAMatrixExport void Log();
    DIVAMatrixExport void CrossProduct(const CDVector &v1, const CDVector &v2);

	// Comunication with MatLab routines.
	DIVAMatrixExport void ToMatlab(const CDString& sFilename,const CDString& sName,const CDString& sComment="",bool fAppend=true) const;
	DIVAMatrixExport void FromMatlab(const CDString& sFilename,const CDString& sName);

	// Saving and retrieving from binary file (Much faster then the MatLab Comunication routines).
	DIVAMatrixExport void ToFile(const CDString& sFilename) const;
	DIVAMatrixExport void FromFile(const CDString& sFilename);
	DIVAMatrixExport void ToFile( FILE *fh ) const;
	DIVAMatrixExport void FromFile( FILE *fh );
	DIVAMatrixExport void Reverse();
    

    // Conversions and manipulations
    DIVAMatrixExport CDVector Range( const int st, const int end) const;
    DIVAMatrixExport CDVector VecCat( const CDVector &v ) const;
    DIVAMatrixExport void ToMatrix( const int nRows, const int nCols, CDMatrix &m, bool rowWise =true );

    // Comparison

	// Elementwise equal to
	DIVAMatrixExport void Eq(const double B, CDVector& C) const;
	DIVAMatrixExport void Eq(const CDVector& B, CDVector& C) const;

	// Elementwise not equal to
	DIVAMatrixExport void Ne(const double B, CDVector& C) const;
	DIVAMatrixExport void Ne(const CDVector& B, CDVector& C) const;

	// Elementwise less than
	DIVAMatrixExport void Lt(const double B, CDVector& C) const;
	DIVAMatrixExport void Lt(const CDVector& B, CDVector& C) const;

	// Elementwise less than or equal
	DIVAMatrixExport void Le(const double B, CDVector& C) const;
	DIVAMatrixExport void Le(const CDVector& B, CDVector& C) const;

	// Elementwise greater than
	DIVAMatrixExport void Gt(const double B, CDVector& C) const;
	DIVAMatrixExport void Gt(const CDVector& B, CDVector& C) const;

	// Elementwise greater than or equal
	DIVAMatrixExport void Ge(const double B, CDVector& C) const;
	DIVAMatrixExport void Ge(const CDVector& B, CDVector& C) const;
};


/**

  @author   Rune Fisker, Mikkel B. Stegmann, 
            Henrik Aan�s, Lars Pedersen et al.

  @version  2-5-2001

  @memo     Matrix class with double precision.

  @doc      Matrix class with double precision based upon the VisSDK
            matrix class CVisDMatrix.

  @see      CDVector
    
*/
class CDMatrix : public virtual CVisDMatrix
{
public:
    // Constructors (default destructor and copy constructor)
    DIVAMatrixExport CDMatrix(void):CVisDMatrix(){};
    DIVAMatrixExport CDMatrix(int rows, int cols, double *storage = 0):CVisDMatrix(rows,cols,storage) {};
    DIVAMatrixExport CDMatrix(const CDMatrix &mat):CVisDMatrix(mat) {};
    DIVAMatrixExport CDMatrix(const CVisDMatrix &mat):CVisDMatrix(mat) {};

    // Assignment
    DIVAMatrixExport CDMatrix& operator=(const CVisDMatrix &mat);
	DIVAMatrixExport CDMatrix& operator=(const CDMatrix &mat);
    DIVAMatrixExport CDMatrix& operator=(double value);

    // Statistical functions
    DIVAMatrixExport double Mean() const;
	DIVAMatrixExport void MeanCol(CDVector& vMean) const;
	DIVAMatrixExport double Std() const;
	DIVAMatrixExport void StdCol(CDVector& vStd) const;
	DIVAMatrixExport double Sum() const;
	DIVAMatrixExport void SumCol(CDVector& vSum) const;
	DIVAMatrixExport double Var() const;
	DIVAMatrixExport void VarCol(CDVector& vVar) const;
	DIVAMatrixExport void OneWayANOVA(double& dZ, int& nDFModel, int& nDFError) const;
	DIVAMatrixExport void TTest(const int iCol1,const int iCol2, double& dZ, int& nDF) const;

    // matrix manipulation
	DIVAMatrixExport void Col(int i, CDVector& vCol) const;
	DIVAMatrixExport void Row(int i, CDVector& vRow) const;	

	DIVAMatrixExport void CombVert(CVisDMatrix& Top,CVisDMatrix& Bottom);

	DIVAMatrixExport void Diag(const CDVector& vec);
	DIVAMatrixExport CDString ToString() const;

	DIVAMatrixExport void ElementDivide(const CDMatrix& matrix);
	DIVAMatrixExport void ElementMultiply(const CDMatrix& matrix);

	// math functions
	DIVAMatrixExport void Sqr();
	DIVAMatrixExport void Sqrt();
    DIVAMatrixExport void Log();

	// Comunication with MatLab routines.
	DIVAMatrixExport void ToMatlab(const CDString& sFilename,const CDString& sName,const CDString& sComment="",bool fAppend=true) const;
	DIVAMatrixExport void FromMatlab(const CDString& sFilename,const CDString& sName);

	//Saving and retirving from binary file (Much faster then the MatLab Comunication routines).
	DIVAMatrixExport void ToFile(const CDString& sFilename) const;
	DIVAMatrixExport void FromFile(const CDString& sFilename);
	DIVAMatrixExport void ToFile( FILE *fh ) const;
	DIVAMatrixExport void FromFile( FILE *fh );

	// LaTeX formatted output
	DIVAMatrixExport void ToLaTeX(	const CDString& sFilename, 
									const CDString& sVarName		= "M", 
									const CDString& sComment		= "", 
									bool fWriteAsTable			= false,
									const CDString& sNumFmt		= "%.2f",
									const CDString& sBracketType	= "[",
									bool fAppend				= true ) const;


    // misc
	DIVAMatrixExport void Kron(CDMatrix& mX,CDMatrix& mY);	
	DIVAMatrixExport void Eye();
    DIVAMatrixExport void Rand();
    DIVAMatrixExport void OuterProduct(const CDVector &v1, const CDVector &v2);

    // Extract upper triangular part of matrix.
	DIVAMatrixExport void TriU(CDMatrix& matrix, const int K) const;

	// Extract lower triangular part of matrix.
	DIVAMatrixExport void TriL(CDMatrix& matrix, const int K) const;

    // Flips
    DIVAMatrixExport void FlipLR();
    DIVAMatrixExport void FlipUD();

    // Conversions and manipulations
    DIVAMatrixExport void ToVector( CDVector &v, bool rowWise = true );


	// Comparison

	// Elementwise equal to
	DIVAMatrixExport void Eq(const double B, CDMatrix& C) const;
	DIVAMatrixExport void Eq(const CDMatrix& B, CDMatrix& C) const;

	// Elementwise not equal to
	DIVAMatrixExport void Ne(const double B, CDMatrix& C) const;
	DIVAMatrixExport void Ne(const CDMatrix& B, CDMatrix& C) const;

	// Elementwise less than
	DIVAMatrixExport void Lt(const double B, CDMatrix& C) const;
	DIVAMatrixExport void Lt(const CDMatrix& B, CDMatrix& C) const;

	// Elementwise less than or equal
	DIVAMatrixExport void Le(const double B, CDMatrix& C) const;
	DIVAMatrixExport void Le(const CDMatrix& B, CDMatrix& C) const;

	// Elementwise greater than
	DIVAMatrixExport void Gt(const double B, CDMatrix& C) const;
	DIVAMatrixExport void Gt(const CDMatrix& B, CDMatrix& C) const;

	// Elementwise greater than or equal
	DIVAMatrixExport void Ge(const double B, CDMatrix& C) const;
	DIVAMatrixExport void Ge(const CDMatrix& B, CDMatrix& C) const;	    

    // Determinant, Trace
    DIVAMatrixExport double CDMatrix::Det() const;
    DIVAMatrixExport double CDMatrix::Trace() const;
};


#endif // __CD_MATRIX__