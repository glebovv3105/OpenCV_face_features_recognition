#if !defined(CD_OPTIMIZE_SD)
#define CD_OPTIMIZE_SD


#include "DOptimizeBase.h"


class CDOptimizeSD : public CDOptimizeBase
{

public:
	// constructor/destructor
	CDOptimizeSD() {};
	~CDOptimizeSD(){};
	
	// the Minimize function using analytic gradient
	DIVAOptExport virtual ETermCode Minimize(CDVector& x, CDOptimizeFuncBase* pFuncEvalBase); 

	// name of optimization methode
	virtual CString Name() {return "Steepest Descent";}
	virtual EOptMethod OptMethod() {return eoptSteepestDescent;}

};

#endif // CD_OPTIMIZE_SD