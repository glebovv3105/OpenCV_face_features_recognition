/////////////////////////////////////////////////////////////////////////////
//
// Header-file for the simulated annealing optimization class.
// Simulated annealing [1,2] is implemented with the Metroplis algorithm [3],
// random walk and temperature scheme T_t+1 = k T_t [1]
//
// [1]	Kirkpatrick, S. and Gellant, C. D. and Vecchi, M. P.,
//		Optimization by simulated annealing, 
//		Science, 1983, vol. 220, pp.671-680
// [2]	Cerny, V., 
//		Thermodynamical approach to the traveling salesman problem: an efficient simulation algorithm},
//		Jour. of Optimization Theory and Applications,
//		1985, vol. 45, pp. 41-45
// [3]  Metropolis, N. and Rosenbluth, A. W. and Rosenbluth, M. N. and Teller A. H. and Teller E.,
//		Equations of state calculations by fast computing machines,
//		Jour. Chemical Physics,
//		1953, 21, 1087-1092
//
// Copyright � 2000
//
// IMM, Department of Mathematical Modelling
// Technical University of Denmark, Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/~diva
//
// author: Rune Fisker 
//
// Disclaimer:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on the Microsoft Vision Software Developers 
// Kit VisSDK and CLAPACK.
//
/////////////////////////////////////////////////////////////////////////////


#include "DOptimizeBase.h"

#if !defined(CD_OPTIMIZE_SA)
#define CD_OPTIMIZE_SA


// seed strategy for random generator
enum eSeedToRandom
{
	eseedNo,		// no seed
	eseedRandom,	// random seed
	essedConstant	// constant seed
};


class CDOptimizeSA : public CDOptimizeBase
{

public:

	// constructor/destructor
	DIVAOptExport CDOptimizeSA();
	~CDOptimizeSA(){};
	
	// the Minimize function using analytic gradient
	DIVAOptExport virtual ETermCode Minimize(CDVector& x, CDOptimizeFuncBase* pFuncEvalBase); 

	// name of optimization methode
	virtual CString Name() {return "Simulated Annealing";}
	virtual EOptMethod OptMethod() {return eoptSimulatedAnnealing;}

	// start temperature
	void SetStartTemperature(const double dStartTemperature) { m_dStartTemperature = dStartTemperature; }
	double StartTemperature() const { return m_dStartTemperature; }

	// decrease factor in the temperature scheme
	void SetDecFac(const double dK) { m_dK = dK; }
	double DecFac() const { return m_dK; }

	// flag for random seed in random walk
	eSeedToRandom RandomSeed() const { return m_eSeedToRand; }
	void SetRandomSeed(const eSeedToRandom eSeedToRand) { m_eSeedToRand = eSeedToRand; }
	
private:
	// generate random Gauss numbers
	double Gauss();

	// start temperature
	double m_dStartTemperature;

	// decrease factor in the temperature scheme
	double m_dK;

	// flag for random seed in random walk
	eSeedToRandom m_eSeedToRand;
};

#endif // CD_OPTIMIZE_SA