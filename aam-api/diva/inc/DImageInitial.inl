////////////////////////////////////////////////////////////////////////////
//
//	Converts the the full input image to the format of the current image. The function dos not support
//	RGBA to Gray image, use IntensityFromRGBA for this conversion. The function is only coping
//	the attributes, which are copied by Alias().
//
//	input
//		image: assign attributes from the input image
//
//	Author:	Rune Fisker, 19/4-1999
template <class TPixel>
inline bool CDImageInitial<TPixel>::CopyFull(CVisImageBase& imagebase, EVisNormalize evisnormalize)
{
	// remove ROI
	CRect rROI = imagebase.Rect();
	imagebase.SetRect(imagebase.MemoryRect());

	// if identical types
	bool fSucces;
	if (PixelTypeInfo() == imagebase.PixelTypeInfo())
	{
		// allocate image of same size
		Allocate(imagebase.Shape());

		// copy pixels
		fSucces = imagebase.CopyPixelsTo(*this,evisnormalize);
	}
	else
	{
		// alias image
		Alias(imagebase);

		// allocate image of same size
		Allocate(imagebase.Shape());

		// copy pixels
		fSucces = imagebase.CopyPixelsTo(*this,evisnormalize);
	}

	// set ROI back
	imagebase.SetRect(rROI);

	// add to history
	if (fSucces)
	{
		CString strSrc = imagebase.PixelTypeInfo().name();
		CString strDst = PixelTypeInfo().name();
		AddToHistory("DIVA: Copy image from " + strSrc +" to " + strDst + " (CopyFull)");
	}

	return fSucces;
}

template <class TPixel>
inline bool CDImageInitial<TPixel>::CopyFullPixelsTo(CVisImageBase& refimageDest, EVisNormalize evisnormalize, double dblMin, double dblMax) const 
{
	assert(refimageDest.MemoryRect() == MemoryRect());

	// remove ROI
	CRect rROI = refimageDest.Rect();
	refimageDest.SetRect(refimageDest.MemoryRect());

	CRect rROIthis = Rect();
	SetRect(MemoryRect());

	// copy pixels
	fSucces = CopyPixelsTo(refimageDest,evisnormalize,dblMin,dblMax);

	// set ROI back
	refimageDest.SetRect(rROI);
	SetRect(rROIthis);

	return fSucces;
}


////////////////////////////////////////////////////////////////////////////
//
//	Create intensity image from RGBA image.
//
//	input
//		imagebase: RGBA image
//
//	Author:	Rune Fisker, 28/4-1999
template <class TPixel>
inline bool CDImageInitial<TPixel>::IntensityFromRGBA(CVisImageBase& imagebase)
{
	if (imagebase.PixFmt() & evispixfmtRGBA)
	{
		CDImageInitial<CVisRGBA<TPixel> > imageRGBA;

		if (imageRGBA.CopyFull(imagebase,evisnormalizeCastValues))
		{
			const CVisImage<CVisRGBA<TPixel> >& refimageRGBA = imageRGBA;
			// call a secrect VisSDK function, which create the intensity
			VisMap2(CVisFnOpRGBAIntensity(), *this, const_cast<CVisImage<CVisRGBA<TPixel> >&>(refimageRGBA));

		}
		else
			return false;
	}
	else
		return false;

	// add to history
	CString strSrc = imagebase.PixelTypeInfo().name();
	CString strDst = PixelTypeInfo().name();
	AddToHistory("DIVA: Convert image from " + strSrc +" to " + strDst + " (IntensityFromRGBA)");
	
	return true;
}

////////////////////////////////////////////////////////////////////////////
//
//	Equal to Copy(), beside it creates a copy of the full image independent of the ROI
//
//	input
//		imagebase: image
//
//	Author:	Rune Fisker, 28/4-1999
template <class TPixel>
inline void CDImageInitial<TPixel>::CopyFull(CVisImageBase& imagebase)
{
	// remove ROI
	CRect rROI = imagebase.Rect();
	imagebase.SetRect(imagebase.MemoryRect());

	// alias image
	Copy(imagebase);

	// set ROI back
	imagebase.SetRect(rROI);
}

////////////////////////////////////////////////////////////////////////////
//
//	Copies the extended attributes of an image
//
//	input
//		imagebase: image
//
//	Author:	S�ren Falch & Rune Fisker, 21/5-1999
template <class TPixel>
inline void CDImageInitial<TPixel>::CopyInitialAttributes(const CDImageInitial<TPixel> &image) 
{
	SetHistory(image.History());
	SetXResolution(image.XResolution());
	SetYResolution(image.YResolution());
	SetZResolution(image.ZResolution());
	SetBorderMode(image.BorderMode());
}
////////////////////////////////////////////////////////////////////////////
//
//	Sets the resolution
//
//	input
//		img: image
//		dX,dY,dZ: resolutions
//
//	Author:	S�ren Falch & Rune Fisker, 21/5-1999
template <class TPixel>
void SetResolutions(CVisImageBase& img, const double dX, const double dY, const double dZ)
{
		CDImageInitial<TPixel>* pimage = dynamic_cast<CDImageInitial<TPixel>*> (&img);
		assert(pimage!=NULL);

		pimage->SetXResolution(dX);
		pimage->SetYResolution(dY);
		pimage->SetZResolution(dZ);
}