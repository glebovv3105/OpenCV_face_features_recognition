/////////////////////////////////////////////////////////////////////////////
//
// Image class for personale functions
//
// Copyright � 1998
//
// IMM, Department of Mathematical Modelling
// Technical University of Denmark, Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk
//
// author: Rune Fisker 
//
// Disclaimer:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on the Microsoft Vision Software Developers Kit VisSDK
//
/////////////////////////////////////////////////////////////////////////////

//#include "stdafx.h"

/////////////////////////////////////////////////////////////////////////////
//	Performs Mathematical Erotion on image using a flat structure element (gray scale only). 
//
//	author:	S�ren Falch 14/1-99
template <class TPixel>
void CDImageMorphology<TPixel>::Erode(DWORD dwSize)
{
	CDImageMorphology<TPixel> Tmp(MemoryShape());
	Tmp.SetRect(Rect());

	for (int s=0; s<dwSize/2; s++) {
		// Vertical

		// Do Top Edge
		for (int x=Left();x<Right();x++) 
			Tmp.Pixel(x,Top()) = min(Pixel(x,Top()),Pixel(x,Top()+1));

		// Do Center
		for (int y=Top()+1;y<Bottom()-1;y++)  {
			for (x=Left();x<Right();x++) {
				Tmp.Pixel(x,y)=min(min(Pixel(x,y-1),Pixel(x,y)),Pixel(x,y+1));
			}
		}

		// Do Bottom Edge
		for (x=Left();x<Right();x++) 
			Tmp.Pixel(x,Bottom()-1) = min(Pixel(x,Bottom()-1),Pixel(x,Bottom()-2));
			
		// Horizontal

		// Do Left Edge
		for (y=Top();y<Bottom();y++) 
			Pixel(Left(),y)=min(Tmp.Pixel(Left(),y),Tmp.Pixel(Left()+1,y));

		// Do Middle
		for (y=Top();y<Bottom();y++) 
			for (x=Left()+1;x<Right()-1;x++) {
				Pixel(x,y)=min(min(Tmp.Pixel(x-1,y),Tmp.Pixel(x,y)),Tmp.Pixel(x+1,y));
			}

		// Do Right
		for (y=Top();y<Bottom();y++) 
			Pixel(Right()-1,y)=min(Tmp.Pixel(Right()-1,y),Tmp.Pixel(Right()-2,y));
	}

	CString Msg;
	Msg.Format("DIVA: Erotion with %dx%d square",dwSize,dwSize);
	AddToHistory(Msg);
}


/////////////////////////////////////////////////////////////////////////////
//	Performs Mathematical Dilation on image using a flat structure element (gray scale only). 
//
//	author:	S�ren Falch 14/1-99
template <class TPixel>
void CDImageMorphology<TPixel>::Dilate(DWORD dwSize)
{
	CDImageMorphology<TPixel> Tmp(MemoryShape());
	Tmp.SetRect(Rect());

	for (int s=0; s<dwSize/2; s++) {
		// Vertical

		// Do Top Edge
		for (int x=Left();x<Right();x++) 
			Tmp.Pixel(x,Top()) = max(Pixel(x,Top()),Pixel(x,Top()+1));

		// Do Center
		for (int y=Top()+1;y<Bottom()-1;y++)  {
			for (x=Left();x<Right();x++) {
				Tmp.Pixel(x,y)=max(max(Pixel(x,y-1),Pixel(x,y)),Pixel(x,y+1));
			}
		}

		// Do Bottom Edge
		for (x=Left();x<Right();x++) 
			Tmp.Pixel(x,Bottom()-1) = max(Pixel(x,Bottom()-1),Pixel(x,Bottom()-2));
			
		// Horizontal

		// Do Left Edge
		for (y=Top();y<Bottom();y++) 
			Pixel(Left(),y)=max(Tmp.Pixel(Left(),y),Tmp.Pixel(Left()+1,y));

		// Do Middle
		for (y=Top();y<Bottom();y++) 
			for (x=Left()+1;x<Right()-1;x++) {
				Pixel(x,y)=max(max(Tmp.Pixel(x-1,y),Tmp.Pixel(x,y)),Tmp.Pixel(x+1,y));
			}

		// Do Right
		for (y=Top();y<Bottom();y++) 
			Pixel(Right()-1,y)=max(Tmp.Pixel(Right()-1,y),Tmp.Pixel(Right()-2,y));
	}
	CString Msg;
	Msg.Format("DIVA: Dilation with %dx%d square",dwSize,dwSize);
	AddToHistory(Msg);
}
/////////////////////////////////////////////////////////////////////////////
//	Performs Mathematical Opening on image using a flat structure element (gray scale only). 
//
//	author:	S�ren Falch 14/1-99
template <class TPixel>
void CDImageMorphology<TPixel>::Open(DWORD dwSize)
{
	Erode(dwSize);
	Dilate(dwSize);

	CString Msg;
	Msg.Format("DIVA: Opening with %dx%d square",dwSize,dwSize);
	AddToHistory(Msg);
}

/////////////////////////////////////////////////////////////////////////////
//	Performs Mathematical Closing on image using a flat structure element (gray scale only). 
//
//	author:	S�ren Falch 14/1-99
template <class TPixel>
void CDImageMorphology<TPixel>::Close(DWORD dwSize)
{
	Dilate(dwSize);
	Erode(dwSize);

	CString Msg;
	Msg.Format("DIVA: Closeing with %dx%d square",dwSize,dwSize);
	AddToHistory(Msg);
}

