/////////////////////////////////////////////////////////////////////////////
//
// This file contains the implementation of the conjugate gradient
// optimization algorithm. Conjugate gradient is implemented 
// with Fletcher-Reeves update, soft line search and optional resetting. 
// See e.g. Dennis and Schnabel, Numerical Methods for Unconstrained Optimization and 
// Nonlinear Equations 1983, Prentice-Hall for a description of conjugate gradient.
//
// Copyright � 2000
//
// DTU Image Viever and Analyser (DIVA)
// Department of Mathematical Modelling
// Technical University of Denmark (DTU), Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/~diva
//
// author: Rune Fisker 
//
// Disclaimer:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on the Microsoft Vision Software Developers Kit VisSDK
//
/////////////////////////////////////////////////////////////////////////////


#include "DOptimizeBase.h"

#if !defined(CD_OPTIMIZE_CG)
#define CD_OPTIMIZE_CG


class CDOptimizeCG : public CDOptimizeBase
{

public:
	// constructor/destructor
	DIVAOptExport CDOptimizeCG();
	~CDOptimizeCG(){};
	
	// the Minimize function using analytic gradient
	DIVAOptExport virtual ETermCode Minimize(CDVector& x, CDOptimizeFuncBase* pFuncEvalBase); 

	// name of optimization methode
	DIVAOptExport virtual CString Name() {return "Conjugate Gradient";}
	DIVAOptExport virtual EOptMethod OptMethod() {return eoptConjugateGradient;}

	// resetting function
	DIVAOptExport bool Resetting() const { return m_fReset; }
	DIVAOptExport void SetResetting(const bool fReset) { m_fReset = fReset; }

	// reset every n iterations
	DIVAOptExport void ResetIteNumber(const int nResetIteN) { m_nResetIteN = nResetIteN; } 
	DIVAOptExport int SetResetIteNumber() const { return m_nResetIteN; } 

private:
	// reset flag
	bool m_fReset;

	// reset every n iterations
	int m_nResetIteN;
};

#endif // CD_OPTIMIZE_CG