/////////////////////////////////////////////////////////////////////////////
//
// Image class for filtering functions
//
// Copyright � 1999
//
// IMM, Department of Mathematical Modelling
// Technical University of Denmark, Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/~diva
//
// author: Rune Fisker & Lars Gunder Knudsen 
//
// Disclaimer:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on the Microsoft Vision Software Developers Kit VisSDK
//
/////////////////////////////////////////////////////////////////////////////


#if !defined(CD_IMAGE_CONV_CLASS)
#define CD_IMAGE_CONV_CLASS

#include <DImageBasic.h>
#include <list>

// enumerated constants for fixed filters
enum EDivaFixedFilter {
	edivafixedfilterLAPLACIAN3x3,
	edivafixedfilterLAPLACIAN5x5,
	edivafixedfilterGAUSSIAN3x3,
	edivafixedfilterGAUSSIAN5x5,
	edivafixedfilterHIGHPASS3x3,
	edivafixedfilterHIGHPASS5x5,
	edivafixedfilterSHARPEN3x3,
	edivafixedfilterSOBEL
};

// enumerated constants for combine methods for more than one filter
enum EDivaFilterCombine {
	edivafiltercombineSUM,
	edivafiltercombineSUMSQ,
	edivafiltercombineSUMSQROOT,
	edivafiltercombineMAX,
	edivafiltercombineMIN
};

template <class TPixel>
class CDImageConv : public virtual CDImageBasic<TPixel>
{
public:
	//------------------------------------------------------------------
	// @group Constructors

	// @cmember
	// Default constructor.  The image will not be useable until another
	// image is assigned to it.
	CDImageConv(void){};

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageConv(int width, int height, int nbands = 1,
		int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(width,height,nbands,imopts,pbData){};

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageConv(SIZE size, int nbands = 1,
			int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(size,nbands,imopts,pbData){};

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageConv(const RECT& rect, int nbands = 1,
			int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(rect,nbands,imopts,pbData){};

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageConv(const CVisShape& shape,
			int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(shape,imopts,pbData){};

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageConv(CVisMemBlock& refmemblock, int width, int height,
			int nbands = 1, int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,width,height,nbands,imopts){};

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageConv(CVisMemBlock& refmemblock, SIZE size, int nbands = 1,
			int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,size,nbands,imopts){};

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageConv(CVisMemBlock& refmemblock, const RECT& rect,
			int nbands = 1, int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,rect,nbands,imopts){};

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageConv(CVisMemBlock& refmemblock, const CVisShape& shape,
			int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,shape,imopts){};

	// @cmember
	// Construct an image from another image.
	// Warning:  Derived classes should provide their own copy constructor
	// that first constructs a default object and then uses the assignment
	// operator to synchronize  image creation, assignment, and
	// destruction.  Derived classes should not call the copy constructor
	// in the base class.
	CDImageConv(const CDImageConv<TPixel>& imageSrc)
		:CVisImage<TPixel>(imageSrc){};

	//------------------------------------------------------------------
	// image manipulation functions

	// filtering
	void Conv2D(const CVisImage<float>& imgFilter);
	void Conv2D(const CVisSequence<float>& seqFilters, EDivaFilterCombine edivafiltercombine);
	void Conv2DReal(const CVisImage<float>& mFilter);

	void MeanFilter(int nCols, int nRows);
	void FixedFilter(EDivaFixedFilter edivafixedfilter);

	// correlate
	void Corr2D(CDImageConv<float>& imageDest, const CVisImage<float>& imageMask, CDImageConv<float>* pimageSqr = NULL);
	void Corr2D(CDImageConv<float>& imageDest, const CVisImage<float>& imageMask, const CDImageConv<float>& imageCares, CDImageConv<float>* pimageSqr = NULL);
	void Corr2D(CDImageConv<float>& imageDest, const CVisImage<float>& imageMask, const float flDontCare = FLT_MIN, CDImageConv<float>* pimageSqr = NULL);	

	// predefined filters
	void GenFilterSobel(CVisSequence<float> &seqFilters);
	void GenFilterPrewitt(CVisSequence<float> &seqFilters);
	void GenFilterLaplace4n(CVisSequence<float> &seqFilters);
	void GenFilterLaplace8n(CVisSequence<float> &seqFilters);
	void GenFilterMean(CVisSequence<float> &seqFilters,int nX,int nY);
	void GenFilterGauss(CVisSequence<float> &seqFilters,int nX,int nY, double dStd = -1);
	void GenFilterLoG(CVisSequence<float> &seqFilters,int nX,int nY, double dParam1);
};

#include "DImageConv.inl"




#endif // CD_IMAGE_CONV_CLASS