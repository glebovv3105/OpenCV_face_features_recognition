/////////////////////////////////////////////////////////////////////////////
//
// Image class for personale functions
//
// Copyright � 1999
//
// IMM, Department of Mathematical Modelling
// Technical University of Denmark, Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk
//
// author: Rune Fisker 
//
// Disclaimer:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on the Microsoft Vision Software Developers Kit VisSDK
//
/////////////////////////////////////////////////////////////////////////////
/*
#include <DImageConv.h>
#include <DImageMorphology.h>
#include <DImageFFT.h>
*/


#if !defined(CDVIS_MY_CLASS_GRAY)
#define CDVIS_MY_CLASS_GRAY

// HPMB
enum eDInterPol {

	nearestNeighbour,
	bilinear,
	biquadratic,
	bicubic
};     
// hpmb

#include "DImageMyClass.h"

/*
class CDImageMyClassGray :	public virtual CDImageConv<TPixel>,
						public virtual CDImageMorphology<TPixel>,
						public virtual CDImageFFT<TPixel>
*/
template <class TPixel>
class CDImageMyClassGray :	public virtual CDImageMyClass<TPixel>
{
public:
	//------------------------------------------------------------------
	// @group Constructors

	// @cmember
	// Default constructor.  The image will not be useable until another
	// image is assigned to it.
	CDImageMyClassGray(void){};

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageMyClassGray(int width, int height, int nbands = 1,
		int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(width,height,nbands,imopts,pbData){};

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageMyClassGray(SIZE size, int nbands = 1,
			int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(size,nbands,imopts,pbData){};

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageMyClassGray(const RECT& rect, int nbands = 1,
			int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(rect,nbands,imopts,pbData){};

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageMyClassGray(const CVisShape& shape,
			int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(shape,imopts,pbData){};

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageMyClassGray(CVisMemBlock& refmemblock, int width, int height,
			int nbands = 1, int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,width,height,nbands,imopts){};

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageMyClassGray(CVisMemBlock& refmemblock, SIZE size, int nbands = 1,
			int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,size,nbands,imopts){};

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageMyClassGray(CVisMemBlock& refmemblock, const RECT& rect,
			int nbands = 1, int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,rect,nbands,imopts){};

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageMyClassGray(CVisMemBlock& refmemblock, const CVisShape& shape,
			int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,shape,imopts){};

	// @cmember
	// Construct an image from another image.
	// Warning:  Derived classes should provide their own copy constructor
	// that first constructs a default object and then uses the assignment
	// operator to synchronize  image creation, assignment, and
	// destruction.  Derived classes should not call the copy constructor
	// in the base class.
	CDImageMyClassGray(const CDImageMyClassGray<TPixel>& imageSrc)
		:CVisImage<TPixel>(imageSrc){};

	// image functions
	void Exp();

	TPixel MinPos(int& x, int &y);
	TPixel MaxPos(int& x, int &y);
	void Sum(double& dSum);

	void Sqr0(CDImageMyClassGray<TPixel>& imageOut);

	void Normalize();

	void Get2DProfile(	CDVector p1, CDVector p2, 
						CDVector &outVector, 
						eDInterPol interPolationMethod = nearestNeighbour );

};

//#include "DImageMyClass.inl"
/*
CDImageMyClassGray<char>;
CDImageMyClassGray<unsigned char>;
CDImageMyClassGray<int>;
CDImageMyClassGray<short>;
CDImageMyClassGray<unsigned short>;
CDImageMyClassGray<float>;
CDImageMyClassGray<double>;
*/


#endif // CDVIS_MY_CLASS