/////////////////////////////////////////////////////////////////////////////
//
// Image class for personale functions
//
// Copyright � 1999
//
// IMM, Department of Mathematical Modelling
// Technical University of Denmark, Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk
//
// author: Rune Fisker 
//
// Disclaimer:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on the Microsoft Vision Software Developers Kit VisSDK
//
/////////////////////////////////////////////////////////////////////////////

#include <DImageConv.h>
#include <DImageMorphology.h>
#include <DImageFFT.h>

#if !defined(CDVIS_MY_CLASS)
#define CDVIS_MY_CLASS

template <class TPixel>
class CDImageMyClass :	public virtual CDImageConv<TPixel>,
						public virtual CDImageMorphology<TPixel>,
						public virtual CDImageFFT<TPixel>

{
public:
	//------------------------------------------------------------------
	// @group Constructors

	// @cmember
	// Default constructor.  The image will not be useable until another
	// image is assigned to it.
	CDImageMyClass(void){};

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageMyClass(int width, int height, int nbands = 1,
		int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(width,height,nbands,imopts,pbData){};

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageMyClass(SIZE size, int nbands = 1,
			int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(size,nbands,imopts,pbData){};

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageMyClass(const RECT& rect, int nbands = 1,
			int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(rect,nbands,imopts,pbData){};

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDImageMyClass(const CVisShape& shape,
			int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(shape,imopts,pbData){};

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageMyClass(CVisMemBlock& refmemblock, int width, int height,
			int nbands = 1, int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,width,height,nbands,imopts){};

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageMyClass(CVisMemBlock& refmemblock, SIZE size, int nbands = 1,
			int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,size,nbands,imopts){};

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageMyClass(CVisMemBlock& refmemblock, const RECT& rect,
			int nbands = 1, int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,rect,nbands,imopts){};

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDImageMyClass(CVisMemBlock& refmemblock, const CVisShape& shape,
			int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,shape,imopts){};

	// @cmember
	// Construct an image from another image.
	// Warning:  Derived classes should provide their own copy constructor
	// that first constructs a default object and then uses the assignment
	// operator to synchronize  image creation, assignment, and
	// destruction.  Derived classes should not call the copy constructor
	// in the base class.
	CDImageMyClass(const CDImageMyClass<TPixel>& imageSrc)
		:CVisImage<TPixel>(imageSrc){};

	// image functions
//	void Exp();

	void Combine3Channels(CDImageMyClass<unsigned char>& imageR, CDImageMyClass<unsigned char>& imageG, CDImageMyClass<unsigned char>& imageB);
	void ExtractContour(CDVector& vX, CDVector& vY, const double dLabel);
	void FullPathNoExt(CString& strOut);
	void Shift(const int x, const int y, bool fCyclic);
	void Resize(const int nWidth, const int nHeight);
	void ClearRect() {SetRect(MemoryRect());}

	void FillInside(const double dVal, int iXSeed, int iYSeed);
	void Crop();
	//	TPixel MinPos(int& x, int &y);
//	TPixel MaxPos(int& x, int &y);

private:
	void FillRow(const double dVal, int iXSeed1, int iXSeed2, int iYSeed1);
};

//#include "DImageMyClass.inl"
/*
CDImageMyClass<char>;
CDImageMyClass<unsigned char>;
CDImageMyClass<int>;
CDImageMyClass<short>;
CDImageMyClass<unsigned short>;
CDImageMyClass<float>;
CDImageMyClass<double>;
*/


#endif // CDVIS_MY_CLASS