/////////////////////////////////////////////////////////////////////////////
//
// Wrapper sequence classes (Gray and RGB) used to interface between the true sequence class and 
// the interface. The use of this class avoid switch statements. The seperation between Gray 
// and RGB is introduced to avoid the problems, which occures when a function is not defined 
// for both formats. 
//
// Copyright � 1999
//
// IMM, Department of Mathematical Modelling
// Technical University of Denmark, Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk
//
// author: Rune Fisker 
//
// Disclaimer:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on the Microsoft Vision Software Developers Kit VisSDK
//
/////////////////////////////////////////////////////////////////////////////

#include "DIVAWrapBase.h"
#include "DSequenceApp.h"

#if !defined(CDIVA_WRAP)
#define CDIVA_WRAP

// wrapper class containing functions, which are defined for all pixel types
template<class TPixel>
class CDIVAWrap : public CDIVAWrapBase
{
public:
	/////////////////////////////////////////////////////////////////////////////////////////////
	// constructor and destructor
	CDIVAWrap(void) { SetSeqBase(&m_seq); }	
	virtual ~CDIVAWrap(void) {};

	/////////////////////////////////////////////////////////////////////////////////////////////
	// Region Of Interest functions
	virtual void SetROI(CRect rROI) { if (ROIOn()) m_seq.SetRect(rROI); }
	virtual void ClearROI() { if (ROIOn()) m_seq.ClearRect(); }

	/////////////////////////////////////////////////////////////////////////////////////////////
	// Get the current active image
	const CDImageApp<TPixel>& Image() const { return m_seq[m_seq.ICur()]; } 
	CDImageApp<TPixel>& Image() { return m_seq[m_seq.ICur()]; } 

	/////////////////////////////////////////////////////////////////////////////////////////////
	// Get the seq
	const CDSequenceApp<TPixel>& Sequence() const { return m_seq; } 
	CDSequenceApp<TPixel>& Sequence() { return m_seq; } 

	/////////////////////////////////////////////////////////////////////////////////////////////
	// sequence function
	virtual void Insert(int iPos, CVisImageBase& refimage) { m_seq.Insert(iPos, dynamic_cast<CDImageApp<TPixel>&> (refimage)); }
	virtual void Insert(int iPos, const CVisShape& refshape) { m_seq.Insert(iPos, refshape); }
	virtual void Insert(int iPos, int nLength, const CVisShape& refshape) { m_seq.Insert(iPos, nLength, refshape); }
	virtual void InsertCopyFull(int iPos, CVisImageBase& refimage) { m_seq.InsertCopyFull(iPos, dynamic_cast<CDImageApp<TPixel>&> (refimage)); } //dm

	/////////////////////////////////////////////////////////////////////////////////////////////
	// history functions
	virtual CString ImageHistory() {return Image().History();}
	virtual void SetImageHistory(const CString& sHistory) { Image().SetHistory(sHistory); } 
	virtual void AddToImageHistory(const CString& sHistory) { Image().AddToHistory(sHistory); }
	virtual void SeqHistory(CString& sHistory) { m_seq.History(sHistory); }

	/////////////////////////////////////////////////////////////////////////////////////////////
	// miscellaneous attributes functions
	virtual double XResolution() {return Image().XResolution();}
	virtual double YResolution() {return Image().YResolution();}
	virtual double ZResolution() {return Image().ZResolution();}

	virtual void SetXResolution(const double dXResolution) {Image().SetXResolution(dXResolution);}
	virtual void SetYResolution(const double dYResolution) {Image().SetYResolution(dYResolution);}
	virtual void SetZResolution(const double dZResolution) {Image().SetZResolution(dZResolution);}

	virtual EVisPad BorderMode() const { return Image().BorderMode(); }
	virtual void SetBorderMode(EVisPad evispad) {Image().SetBorderMode(evispad);}

	/////////////////////////////////////////////////////////////////////////////////////////////
	// file handler functions
	virtual void ReadFile(SVisFileDescriptor& filedescriptorT) { m_seq.ReadFile(filedescriptorT); }
	virtual void WriteFile(SVisFileDescriptor& filedescriptorT) { m_seq.WriteFile(filedescriptorT); }

	/////////////////////////////////////////////////////////////////////////////////////////////
	// drawing functions
	virtual void DrawLine(int x1 ,int y1 ,int x2 ,int y2, double tVal) { Image().DrawLine(x1 ,y1 ,x2 ,y2, tVal); }
	virtual void DrawRect(CRect rFrame, double dVal) {Image().DrawRect(rFrame, dVal); }
	virtual void FillPixels(double dValue) {Image().FillPixels(dValue);}

	/////////////////////////////////////////////////////////////////////////////////////////////
	// image arithmetic functions
	virtual void Add(const double dbl) { Image() += dbl; }
	virtual void Subtract(const double dbl) { Image() -= dbl; }
	virtual void Multiply(const double dbl) { Image() *= dbl; }
	virtual void Divide(const double dbl) { Image() /= dbl; }

	virtual void Add(const CVisImageBase& imagebase) { Image() += dynamic_cast<const CDImageApp<TPixel>&> (imagebase); }
	virtual void Subtract(const CVisImageBase& imagebase) { Image() -= dynamic_cast<const CDImageApp<TPixel>&> (imagebase); }

	/////////////////////////////////////////////////////////////////////////////////////////////
	// miscellaneous image functions
	virtual int	SizeofPixel() {return Image().SizeofPixel(); }
	virtual void FlipVertical(void) {Image().FlipVertical();}
	virtual void FlipHorizontal(void) {Image().FlipHorizontal();}

	/////////////////////////////////////////////////////////////////////////////////////////////
	// image copy functions
	virtual bool CopyFull(CVisImageBase& imagebase, EVisNormalize evisnormalize) { return Image().CopyFull(imagebase,evisnormalize);}

	/////////////////////////////////////////////////////////////////////////////////////////////
	// your image functions (add your own image function here)
	virtual void ExtractContour(CDVector& vX, CDVector& vY, const double dLabel) { Image().ExtractContour(vX, vY, dLabel); }
	virtual void FullPathNoExt(CString& strOut) { Image().FullPathNoExt(strOut); }

	virtual	void Shift(const int x, const int y, bool fCyclic) { Image().Shift(x,y,fCyclic); }
	virtual	void Resize(const int nWidth, const int nHeight) { Image().Resize(nWidth, nHeight); }
	virtual void FillInside(const double dVal, int iXSeed, int iYSeed) {Image().FillInside(dVal, iXSeed, iYSeed); }
	virtual void Crop() { Image().Crop(); }

private:

	/////////////////////////////////////////////////////////////////////////////////////////////
	// THE IMAGE SEQUENCE
	CDSequenceApp<TPixel> m_seq;

};

// wrapper class used for gray scale images.
// remember to implement all functions in the CDIVAWrapRGBA as well 
template<class TPixel>
class CDIVAWrapGray : public CDIVAWrap<TPixel>
{
public:
	/////////////////////////////////////////////////////////////////////////////////////////////
	// constructor and destructor
	CDIVAWrapGray(void) {}	
	virtual ~CDIVAWrapGray(void) {};

	/////////////////////////////////////////////////////////////////////////////////////////////
	// image arithmetic functions
    virtual void Multiply(const CVisImageBase& imagebase) { Image() *= dynamic_cast<const CDImageApp<TPixel>&> (imagebase); }
	virtual void Divide(const CVisImageBase& imagebase) { Image() /= dynamic_cast<const CDImageApp<TPixel>&> (imagebase); }

	/////////////////////////////////////////////////////////////////////////////////////////////
	// image binarization/threshold functions
	virtual void Threshold(double dThreshhold) {Image().Threshold((TPixel) dThreshhold); }
	virtual void ThresholdInv(double dThreshhold) {Image().ThresholdInv((TPixel) dThreshhold);}
	virtual void ThresholdSoft(double dThreshold) {Image().ThresholdSoft(dThreshold); }
	virtual void ThresholdSoftInv(double dThreshold) {Image().ThresholdSoftInv(dThreshold); }

	/////////////////////////////////////////////////////////////////////////////////////////////
	// image blob analysis functions
	virtual void Label(void) {Image().Label(); }
	virtual void BlobFirstMoment(CVisDVector& vXC, CVisDVector& vYC) { Image().BlobFirstMoment(vXC, vYC); }

	/////////////////////////////////////////////////////////////////////////////////////////////
	// linear filtering functions
	virtual void FixedFilter(EDivaFixedFilter edivafixedfilter) { Image().FixedFilter(edivafixedfilter); }
	virtual void MeanFilter(int nRows,int nCols) { Image().MeanFilter(nRows,nCols); }
	virtual void Corr2D(CDImageApp<float>& imageDest, CDImageApp<float>& mCorrFilter, float flDontCare = FLT_MIN, CDImageApp<float>* pimageSqr = NULL)
	{ Image().Corr2D(imageDest,mCorrFilter,flDontCare,pimageSqr); }

	/////////////////////////////////////////////////////////////////////////////////////////////
	// non-linear filtering functions
	virtual void Erode(int nSize) { Image().Erode(nSize);  }
	virtual void Dilate(int nSize) { Image().Dilate(nSize); }
	virtual void Open(int nSize) { Image().Open(nSize); }
	virtual void Close(int nSize) { Image().Close(nSize); }

	/////////////////////////////////////////////////////////////////////////////////////////////
	// image statistics
	virtual void Max(double& dMax) { dMax = (double) Image().Max(); }
	virtual void Mean(double& dMean) {Image().Mean(dMean);}
	virtual void Mean(CVisRGBADoublePixel& rgbMean) {throw CVisError("This function dos not support the pixel format RGB",eviserrorPixFmt,"Mean()","DIVAWrap.h", __LINE__);}
	virtual void Min(double& dMin) { dMin = (double) Image().Min();  }
	virtual void Std(double& dStd) {Image().Std(dStd);  }

	/////////////////////////////////////////////////////////////////////////////////////////////
	// math functions
	virtual void Log() {Image().Log();}
	virtual void Sqr() {Image().Sqr(); }
	virtual void Sqrt() {Image().Sqrt(); }

	/////////////////////////////////////////////////////////////////////////////////////////////
	// image conversion functions
	virtual bool IntensityFromRGBA(CVisImageBase& imagebase) { return Image().IntensityFromRGBA(imagebase); }

	/////////////////////////////////////////////////////////////////////////////////////////////
	// miscellaneous image functions

	virtual double PixelGray(int x, int y) {return Image().Pixel(x, y); }
	virtual void SetPixelGray(int x, int y, double dVal) {Image().Pixel(x, y) = dVal; }

	virtual void EnlargePyr(int nFactor) {Image().EnlargePyr(nFactor); };
	virtual void ReducePyr(int nFactor) {Image().ReducePyr(nFactor); }

	/////////////////////////////////////////////////////////////////////////////////////////////
	// non-linear filtering functions
//	virtual void FFT(CDImageFFT<std::complex<float> > &imageDest) { Image().FFT(imageDest);  }
	virtual void FFT(CDImageFFT<float> &imageMagn, CDImageFFT<float> &imagePhase) { Image().FFT(imageMagn, imagePhase);  }
	virtual void FFT(CDImageFFT<float> &imageRCPack2D) { Image().FFT(imageRCPack2D); }
	virtual void IFFT(void) { Image().IFFT(); }
	virtual void QuadFlip(void) { Image().QuadFlip(); }
	virtual void Taper(void) { Image().Taper(); }
	virtual void RCPack2DToComplex(CDImageApp<std::complex<float> > &Tmp) { Image().RCPack2DToComplex(Tmp); }
	virtual void RCPack2DToMagnPhase(CDImageApp<float> &Magn, CDImageApp<float> &Phase) { Image().RCPack2DToMagnPhase(Magn, Phase); }
	virtual void FFTCrossCorrelate(CVisImageBase &imagebase, CDImageApp<float> &ImgOut) { 
		CDImageApp <TPixel> &Tmp = dynamic_cast<CDImageApp<TPixel>&> (imagebase); 
		Image().FFTCrossCorrelate(Tmp, ImgOut); 
	}
	virtual void FFTPad(CVisImageBase &imagebase, int Size, int Window, int Position) {
		CDImageApp <TPixel> &Tmp = dynamic_cast<CDImageApp<TPixel>&> (imagebase); 
		Image().FFTPad(Tmp, Size, Window, Position);
	} 

	/////////////////////////////////////////////////////////////////////////////////////////////
	// your image functions (add your own image function here)

	// HPMB
	virtual void Profile1D(	CDVector p1, CDVector p2, CDVector &outVector, eDInterPol interPolationMethod = nearestNeighbour ) 
	{
		Image().Get2DProfile( p1, p2, outVector, interPolationMethod );
	}
	// hpmb

	virtual void Exp() {Image().Exp();}
	virtual void Sqr(CVisImageBase& imageOut) 
	{
		CDImageApp<TPixel>& img = dynamic_cast<CDImageApp<TPixel>&> (imageOut); 
		assert(&img); 
		Image().Sqr0(img); 
	}
};


// wrapper class used for RGB images.
// remember to implement all functions in the CDIVAWrapGray as well 
template<class TPixel>
class CDIVAWrapRGBA : public CDIVAWrap<TPixel>
{
public:
	/////////////////////////////////////////////////////////////////////////////////////////////
	// constructor and destructor
	CDIVAWrapRGBA(void) {}		
	virtual ~CDIVAWrapRGBA(void) {};

	/////////////////////////////////////////////////////////////////////////////////////////////
	// image arithmetic functions
	virtual void Multiply(const CVisImageBase& imagebase) { throw CVisError("This function dos not support the pixel format RGB",eviserrorPixFmt,"Threshold()","DIVAWrap.h", __LINE__); }
	virtual void Divide(const CVisImageBase& imagebase) { throw CVisError("This function dos not support the pixel format RGB",eviserrorPixFmt,"Threshold()","DIVAWrap.h", __LINE__); }

	/////////////////////////////////////////////////////////////////////////////////////////////
	// image binarization/threshold functions
	virtual void Threshold(double dThreshhold) { throw CVisError("This function dos not support the pixel format RGB",eviserrorPixFmt,"Threshold()","DIVAWrap.h", __LINE__); }
	virtual void ThresholdInv(double dThreshhold) { throw CVisError("This function dos not support the pixel format RGB",eviserrorPixFmt,"ThresholdInv()","DIVAWrap.h", __LINE__); }
	virtual void ThresholdSoft(double dThreshold) {throw CVisError("This function dos not support the pixel format RGB",eviserrorPixFmt,"ThresholdSoft()","DIVAWrap.h", __LINE__);}
	virtual void ThresholdSoftInv(double dThreshold) {throw CVisError("This function dos not support the pixel format RGB",eviserrorPixFmt,"ThresholdInvSoft()","DIVAWrap.h", __LINE__);}

	/////////////////////////////////////////////////////////////////////////////////////////////
	// image blob analysis functions
	virtual void Label(void) {throw CVisError("This function dos not support the pixel format RGB",eviserrorPixFmt,"Label()","DIVAWrap.h", __LINE__);}
	virtual void BlobFirstMoment(CVisDVector& vXC, CVisDVector& vYC) { throw CVisError("This function dos not support the pixel format RGB",eviserrorPixFmt,"BlobFirstMoment()","DIVAWrap.h", __LINE__);}

	/////////////////////////////////////////////////////////////////////////////////////////////
	// linear filtering functions
	virtual void FixedFilter(EDivaFixedFilter edivafixedfilter) { Image().FixedFilter(edivafixedfilter); }
	virtual void MeanFilter(int nRows,int nCols) { Image().BlurIpl(nRows,nCols); }
	virtual void Corr2D(CDImageApp<float>& imageDest, CDImageApp<float>& mCorrFilter, float flDontCare = FLT_MIN, CDImageApp<float>* pimageSqr = NULL) {throw CVisError("This function dos not support the pixel format RGB",eviserrorPixFmt,"Corr2D()","DIVAWrap.h", __LINE__);}

	/////////////////////////////////////////////////////////////////////////////////////////////
	// non-linear filtering functions
	virtual void Erode(int nSize) { throw CVisError("This function dos not support the pixel format RGB",eviserrorPixFmt,"Erode()","DIVAWrap.h", __LINE__); }
	virtual void Dilate(int nSize) { throw CVisError("This function dos not support the pixel format RGB",eviserrorPixFmt,"Dilate()","DIVAWrap.h", __LINE__); }
	virtual void Open(int nSize) { throw CVisError("This function dos not support the pixel format RGB",eviserrorPixFmt,"Open()","DIVAWrap.h", __LINE__); }
	virtual void Close(int nSize) { throw CVisError("This function dos not support the pixel format RGB",eviserrorPixFmt,"Close()","DIVAWrap.h", __LINE__); }

	/////////////////////////////////////////////////////////////////////////////////////////////
	// image manipulation functions
	virtual void FillPixels(double dValue) {Image().FillPixels(dValue);}
	virtual void FlipVertical(void) {Image().FlipVertical();}
	virtual void FlipHorizontal(void) {Image().FlipHorizontal();}

	/////////////////////////////////////////////////////////////////////////////////////////////
	// image statistics and math functions
	virtual void Max(double& dMax) { throw CVisError("This function dos not support the pixel format RGB",eviserrorPixFmt,"Max()","DIVAWrap.h", __LINE__); }
	virtual void Mean(double& dMean) { throw CVisError("This function dos not support the pixel format RGB",eviserrorPixFmt,"Mean()","DIVAWrap.h", __LINE__);}
	virtual void Mean(CVisRGBADoublePixel& rgbMean) {Image().Mean(rgbMean); }
	virtual void Min(double& dMin) { throw CVisError("This function dos not support the pixel format RGB",eviserrorPixFmt,"Min()","DIVAWrap.h", __LINE__); }
	virtual void Std(double& dStd) {throw CVisError("This function dos not support the pixel format RGB",eviserrorPixFmt,"Std()","DIVAWrap.h", __LINE__); }

	/////////////////////////////////////////////////////////////////////////////////////////////
	// math functions
	virtual void Log() { throw CVisError("This function dos not support the pixel format RGB",eviserrorPixFmt,"Log()","DIVAWrap.h", __LINE__); }
	virtual void Sqr() {throw CVisError("This function dos not support the pixel format RGB",eviserrorPixFmt,"Sqrt()","DIVAWrap.h", __LINE__);}
	virtual void Sqrt() {throw CVisError("This function dos not support the pixel format RGB",eviserrorPixFmt,"Sqrt()","DIVAWrap.h", __LINE__);}

	/////////////////////////////////////////////////////////////////////////////////////////////
	// image conversion functions
	virtual bool IntensityFromRGBA(CVisImageBase& imagebase) { throw CVisError("This function dos not support the pixel format RGB",eviserrorPixFmt,"IntensityFromRGBA()","DIVAWrap.h", __LINE__); return false; };

	/////////////////////////////////////////////////////////////////////////////////////////////
	// miscellaneous image functions
	virtual double PixelGray(int x, int y) {return (Image().Pixel(x, y).R()+Image().Pixel(x, y).G()+Image().Pixel(x, y).B())/3.0; }
	virtual void SetPixelGray(int x, int y, double dVal) {Image().Pixel(x, y).SetR(dVal); Image().Pixel(x, y).SetG(dVal); Image().Pixel(x, y).SetB(dVal);}

	virtual void ReducePyr(int nFactor) {throw CVisError("This function dos not support the pixel format RGB",eviserrorPixFmt,"Threshold()","DIVAWrap.h", __LINE__);}
	virtual void EnlargePyr(int nFactor) {Image().EnlargePyr(nFactor); };

	/////////////////////////////////////////////////////////////////////////////////////////////
	// non-linear filtering functions
	virtual void FFT(CDImageFFT<float> &imageMagn, CDImageFFT<float> &imagePhase) { throw CVisError("This function does not support the pixel format RGB",eviserrorPixFmt,"FFT()","DIVAWrap.h", __LINE__); }
	virtual void IFFT(void) { throw CVisError("This function does not support the pixel format RGB",eviserrorPixFmt,"IFFT()","DIVAWrap.h", __LINE__); }
	virtual void QuadFlip(void) { throw CVisError("This function does not support the pixel format RGB",eviserrorPixFmt,"QuadFlip()","DIVAWrap.h", __LINE__); }
	virtual void Taper(void) { throw CVisError("This function does not support the pixel format RGB",eviserrorPixFmt,"Taper()","DIVAWrap.h", __LINE__); }
	virtual void RCPack2DToComplex(CDImageApp<std::complex<float> > &Tmp) { throw CVisError("This function does not support the pixel format RGB",eviserrorPixFmt,"RCPack2DToComplex()","DIVAWrap.h", __LINE__); }
	virtual void RCPack2DToMagnPhase(CDImageApp<float> &Magn, CDImageApp<float> &Phase) { throw CVisError("This function does not support the pixel format RGB",eviserrorPixFmt,"RCPack2DToMagnPhase()","DIVAWrap.h", __LINE__); }
	virtual void FFTCrossCorrelate(CVisImageBase& imagebase, CDImageApp<float> &ImgOut) { throw CVisError("This function does not support the pixel format RGB",eviserrorPixFmt,"FFTCrossCorrelate()","DIVAWrap.h", __LINE__); }
	virtual void FFTPad(CVisImageBase& imagebase, int Size, int Window, int Position) { throw CVisError("This function does not support the pixel format RGB",eviserrorPixFmt,"FFTPad()","DIVAWrap.h", __LINE__); } 

	/////////////////////////////////////////////////////////////////////////////////////////////
	// your image functions (add your own image function here)

	virtual void FFT(CDImageFFT<float> &imageRCPack2D) { throw CVisError("This function does not support the pixel format RGB",eviserrorPixFmt,"FFT()","DIVAWrap.h", __LINE__); }
	virtual void Exp() { throw CVisError("This function dos not support the pixel format RGB",eviserrorPixFmt,"Exp()","DIVAWrap.h", __LINE__); }
	virtual void Sqr(CVisImageBase& imageOut) {throw CVisError("This function dos not support the pixel format RGB",eviserrorPixFmt,"Sqrt()","DIVAWrap.h", __LINE__);}

//	virtual void ExtractContour(CDVector& vX, CDVector& vY, const double iLabel, const bool fNeighbourhood8 = false)
//		{ throw CVisError("This function dos not support the pixel format RGB",eviserrorPixFmt,"Exp()",__FILE__, __LINE__); }

	// HPMB
	virtual void Profile1D(	CDVector p1, CDVector p2,	CDVector &outVector, eDInterPol interPolationMethod = nearestNeighbour ) 
	{ 	
		throw CVisError("This function dos not support the pixel format RGB",eviserrorPixFmt,"Get2DProfile()","DIVAWrap.h", __LINE__); 
	}
	// hpmb

};


#endif // CDIVAWRAP


