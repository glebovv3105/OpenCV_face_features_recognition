/////////////////////////////////////////////////////////////////////////////
//
// Base wrapper sequence class used to interface between the true seqeunce class and 
// the interface. The use of this class avoid switch statements.
//
// Copyright � 1998
//
// IMM, Department of Mathematical Modelling
// Technical University of Denmark, Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk
//
// author: Rune Fisker 
//
// Disclaimer:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on the Microsoft Vision Software Developers Kit VisSDK
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(CDIVAWRAPBASE)
#define CDIVAWRAPBASE


class CDIVAWrapBase
{
public:

	/////////////////////////////////////////////////////////////////////////////////////////////
	// constructor and destructor
	CDIVAWrapBase() { m_fROI = true; }
	virtual ~CDIVAWrapBase(void) {};

	/////////////////////////////////////////////////////////////////////////////////////////////
	// Region Of Interest functions
	virtual bool ROIOn() { return m_fROI; }
	virtual void SetROIOn(bool fROI) { m_fROI = fROI; if (!m_fROI) ClearROI();}
	virtual const CRect& ROI() { return ImageBase().Rect(); }
	virtual void SetROI(CRect rROI) = 0;
	virtual void ClearROI() = 0;

	/////////////////////////////////////////////////////////////////////////////////////////////
	// Get the current active base image
	const CVisImageBase& ImageBase() const { return (*m_pseqbase)[m_pseqbase->ICur()]; } 
	CVisImageBase& ImageBase() { return (*m_pseqbase)[m_pseqbase->ICur()]; } 

	/////////////////////////////////////////////////////////////////////////////////////////////
	// Get the base seq
	const CVisSequenceBase& SeqBase() const { return *m_pseqbase; } 
	CVisSequenceBase& SeqBase() { return *m_pseqbase; } 

	/////////////////////////////////////////////////////////////////////////////////////////////
	// sequence function
	void SetICur(int iCur) { m_pseqbase->SetICur(iCur);} 
	int ICur() {return m_pseqbase->ICur();}; 

	int Length() {return m_pseqbase->Length();}; // returns length of seq.
	void Clear() {m_pseqbase->Clear();}; // removes all images from the sequence

	EVisPixFmt PixFmt(void) { return m_pseqbase->PixFmt(); }

	void Erase(int iPos) { m_pseqbase->Erase(iPos); } // erase image at iPos
	virtual void Insert(int iPos, CVisImageBase& refimage) = 0;
	virtual void Insert(int iPos, int nLength, const CVisShape& refshape) = 0;
	virtual void Insert(int iPos, const CVisShape& refshape) = 0;
	virtual void InsertCopyFull(int iPos, CVisImageBase& refimage) = 0;

	/////////////////////////////////////////////////////////////////////////////////////////////
	// history functions
	virtual CString ImageHistory() = 0;
	virtual void SetImageHistory(const CString& sHistory) = 0;
	virtual void AddToImageHistory(const CString& sHistory) = 0;
	virtual void SeqHistory(CString& sHistory) = 0;

	/////////////////////////////////////////////////////////////////////////////////////////////
	// miscellaneous attributes functions
	virtual double XResolution() = 0;
	virtual double YResolution() = 0;
	virtual double ZResolution() = 0;
	
	virtual void SetXResolution(const double dXResolution) = 0;
	virtual void SetYResolution(const double dYResolution) = 0;
	virtual void SetZResolution(const double dZResolution) = 0;

	virtual EVisPad BorderMode() const = 0;
	virtual void SetBorderMode(EVisPad evispad) = 0;

	/////////////////////////////////////////////////////////////////////////////////////////////
	// file handler functions
	virtual void ReadFile(SVisFileDescriptor& filedescriptorT) = 0;
	virtual void WriteFile(SVisFileDescriptor& filedescriptorT) = 0;

	/////////////////////////////////////////////////////////////////////////////////////////////
	// drawing functions
	virtual void DrawLine(int x1 ,int y1 ,int x2 ,int y2, double tVal) = 0;
	virtual void DrawRect(CRect rFrame, double dVal) = 0;
	virtual void FillPixels(double dValue) = 0;

	/////////////////////////////////////////////////////////////////////////////////////////////
	// image arithmetic functions
	virtual void Add(const double dbl) = 0;
	virtual void Subtract(const double dbl) = 0;
	virtual void Multiply(const double dbl) = 0;
	virtual void Divide(const double dbl) = 0;
	virtual void Add(const CVisImageBase& imagebase) = 0;
	virtual void Subtract(const CVisImageBase& imagebase) = 0;
	virtual void Multiply(const CVisImageBase& imagebase) = 0;
	virtual void Divide(const CVisImageBase& imagebase) = 0;

	/////////////////////////////////////////////////////////////////////////////////////////////
	// image binarization functions
	virtual void Threshold(double dThreshhold) = 0;
	virtual void ThresholdInv(double dThreshhold) = 0;
	virtual void ThresholdSoft(double dThreshold) = 0;
	virtual void ThresholdSoftInv(double dThreshold) = 0;

	/////////////////////////////////////////////////////////////////////////////////////////////
	// image blob analysis functions
	virtual void Label(void) = 0;
	virtual void BlobFirstMoment(CVisDVector& vXC, CVisDVector& vYC) = 0;

	/////////////////////////////////////////////////////////////////////////////////////////////
	// linear filtering functions
	virtual void FixedFilter(EDivaFixedFilter edivafixedfilter) = 0;
	virtual void MeanFilter(int nRows,int nCols) = 0;
	virtual void Corr2D(CDImageApp<float>& imageDest, CDImageApp<float>& mCorrFilter, float flDontCare = FLT_MIN, CDImageApp<float>* pimageSqr = NULL) = 0;

	/////////////////////////////////////////////////////////////////////////////////////////////
	// non-linear filtering functions
	virtual void Erode(int nSize) = 0;
	virtual void Dilate(int nSize) = 0;
	virtual void Open(int nSize) = 0;
	virtual void Close(int nSize) = 0;

	/////////////////////////////////////////////////////////////////////////////////////////////
	// image arithmetic functions
	virtual void Max(double& dMax) = 0;
	virtual void Mean(double& dMean) = 0;
	virtual void Mean(CVisRGBADoublePixel& rgbMean) = 0;
	virtual void Min(double& dMin) = 0;
	virtual void Std(double& dStd) = 0;

	/////////////////////////////////////////////////////////////////////////////////////////////
	// math functions
	virtual void Sqr() = 0;
	virtual void Sqrt() = 0;
	virtual void Log() = 0;

	/////////////////////////////////////////////////////////////////////////////////////////////
	// image conversion functions
	virtual bool CopyFull(CVisImageBase& imagebase, EVisNormalize evisnormalize) = 0;
	virtual bool IntensityFromRGBA(CVisImageBase& imagebase) = 0;

	/////////////////////////////////////////////////////////////////////////////////////////////
	// image miscellaneous functions
	virtual void FlipVertical(void) = 0;
	virtual void FlipHorizontal(void) = 0;

	virtual double PixelGray(int x, int y) = 0;
	virtual void SetPixelGray(int x, int y, double dVal) = 0;
	virtual int	SizeofPixel() = 0;

	virtual void ReducePyr(int nFactor) = 0;
	virtual void EnlargePyr(int nFactor) = 0;

	/////////////////////////////////////////////////////////////////////////////////////////////
	// Fast Fourier Transform functions
//	virtual void FFT(CDImageFFT<std::complex<float> > &imageDest) = 0;
	virtual void FFT(CDImageFFT<float> &imageMagn, CDImageFFT<float> &imagePhase) = 0;
	virtual void IFFT(void) = 0;
	virtual void QuadFlip(void) = 0;
	virtual void Taper(void) = 0;
	virtual void RCPack2DToComplex(CDImageApp<std::complex<float> > &Tmp) = 0;
	virtual void RCPack2DToMagnPhase(CDImageApp<float> &Magn, CDImageApp<float> &Phase) = 0;
	virtual void FFTPad(CVisImageBase& imagebase, int Size, int Window, int Position) = 0;
	virtual void FFTCrossCorrelate(CVisImageBase& imagebase, CDImageApp<float> &ImgOut) = 0;

	/////////////////////////////////////////////////////////////////////////////////////////////
	// your image functions (add your own image function here)

	// HPMB
	virtual void Profile1D(	CDVector p1, CDVector p2, CDVector &outVector, eDInterPol interPolationMethod = nearestNeighbour ) = 0;
	// hpmb

	virtual void Exp() = 0;
	virtual void ExtractContour(CDVector& vX, CDVector& vY, const double dLabel) = 0;
	virtual void FullPathNoExt(CString& strOut) = 0;
	virtual	void Shift(const int x, const int y, bool fCyclic) = 0;
	virtual	void Resize(const int nWidth, const int nHeight) = 0;
	virtual void Sqr(CVisImageBase& imageOut) = 0;
	virtual void FFT(CDImageFFT<float> &imageRCPack2D) = 0;
	virtual const CRect& MemoryRect() const { return ImageBase().MemoryRect(); }

	virtual void FillInside(const double dVal, int iXSeed, int iYSeed) = 0;
	virtual void Crop() = 0;

protected:

	/////////////////////////////////////////////////////////////////////////////////////////////
	// sets the image sequence base (should only be used in the CDIVAWrap class)
	void SetSeqBase(CVisSequenceBase* pseqbase) { m_pseqbase = pseqbase; assert(m_pseqbase != 0); }

private:
	// flag to indicate if ROI should be used
	bool m_fROI;

	/////////////////////////////////////////////////////////////////////////////////////////////
	// pointer to the image sequence base
	CVisSequenceBase* m_pseqbase;

};


#endif 