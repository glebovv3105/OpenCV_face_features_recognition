@echo off
REM
REM	Reads all source files in ..\..\src\DivaMatrix and ..\..\inc\DMatrix.h
REM and builds documentation in HTML format
REM
REM Author: Mikkel B. Stegmann
REM 
docxx.exe -B html_footer.html -m -k -a -f -S -p -d html ..\..\inc\DMatrix.h ..\..\src\DivaMatrix\*.*

REM Uncomment the line below to start the internet explorer 
REM with your documentation automatically.
REM
REM start iexplore.exe html\HIER.html