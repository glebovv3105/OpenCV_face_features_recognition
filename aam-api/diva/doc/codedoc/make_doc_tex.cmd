@echo off
REM
REM	Reads all source files in ..\..\src\DivaMatrix and ..\..\inc\DMatrix.h
REM and builds documentation in LaTeX format
REM
REM Author: Mikkel B. Stegmann
REM 
@echo off
@echo off
cd print
docxx.exe -t -S -p -eo 10pt -l  -ep a4wide -d .\ ..\..\..\inc\DMatrix.h ..\..\..\src\DivaMatrix\*.* > doc.tex

REM Run LaTeX twice to get all references updated
REM
latex.exe diva.tex
latex.exe diva.tex

REM Make PostScript version
REM
dvips.exe diva.dvi

REM Make PDF version of the documenation.
REM
dvipdfm -p a4 diva.dvi