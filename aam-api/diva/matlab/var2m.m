% var2m(var, name, comment, filename)
%
% Writes the vector or matrix 'var' into a .m file
% with the name 'name' as if the variable was typed 
% in manually.
%
% The purpose of this function to enable export of MatLab
% into a format that the DIVA matrix class, CDMatrix can 
% read using the FromMatlab() method.
%
% Exporting matrices (and vectors) from DIVA to MatLab is 
% accomplished using CDMatrix::ToMatLab()
%
function var2m(var, name, comment, filename)

fid = fopen(filename,'wt');

fprintf(fid,'%% %s\n',comment);
fprintf(fid,'\n%s=[\n',name);
rows = size(var,1);
for i=1:rows,
   fprintf(fid,'%f ',var(i,:));
   fprintf(fid,';\n');   
end,
fprintf(fid,'];\n\n');
fclose(fid);

