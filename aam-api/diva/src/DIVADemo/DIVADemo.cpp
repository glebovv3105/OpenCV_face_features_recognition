// DIVADemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <math.h>
#include "DIVADemo.h"

// DIVA includes
#include "DMatrix.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace std;


// define the pi constant if needed
#ifndef M_PI
	#define M_PI 3.141592653589793				 
#endif /* M_PI */



/**

  @author   Mikkel B. Stegmann
  @version  1-25-2002

  @memo     Calculates the histogram.

  @doc      Calculates the histogram of a vector within [min;max]
            with optional nomalization. 
  
  @param    v           Input vector.  

  @param    min         Value for the first bin (values lower are clamped to min).

  @param    max         Value for the last bin (values lower are clamped to max).

  @param    hist        Output histogram vector.

  @param    nbins       The number of bins. Default 256.

  @param    normalize   Optional normalization, i.e. make the histogram entries sum to 1.

  @param    transform   Optional transformation of v into 'nbins' integer intervals between [min;max].
  
  @return   Nothing.
  
*/
void Hist( CDVector &v, const double min, const double max, 
           CDVector &hist, const int nbins = 256, bool normalize = true, 
           bool transform = false  ) {

    hist.Resize(nbins);

    hist = .0;
    double val, mag = max-min; 
    unsigned int bin, len = v.Length();    
    for(int i=0;i<len;i++) {

        val = v[i];
        val = val > max ? max : (val<min ? min : val);                
        bin = (unsigned int)(.5+(nbins-1)*(val-min)/mag);
        ++hist[bin];
        if (transform) v[i] = bin;                
    }

    if (normalize) {

        hist /= len;;
    }
}



/**

  @author   Mikkel B. Stegmann
  @version  1-25-2002

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
void CumSum( const CDVector &v, CDVector &cumsum, bool normalize = true ) {

    int len=v.Length();
    cumsum.Resize(len);
    cumsum[0] = v[0];
    for(int i=1;i<len;i++) {

        cumsum[i] = cumsum[i-1] + v[i];
    }

    if (normalize) {

        cumsum /= cumsum[len-1];
    }
}

/**

  @author   Mikkel B. Stegmann
  @version  1-25-2002

  @memo     
  @doc      
  @see      
  
  @param    
  @param    
  
  @return   
  
*/
void GaussianHistogramMathing( const CDVector &v, CDVector &out, const int nbins = 256 ) {

    int v_len = v.Length();
    out.Resize(v_len);
    CDVector gauss(nbins);
    CDVector gauss_accum(nbins);
    CDVector x(nbins);

    // calc gauss frq
    double k = 1.0/sqrt(2*M_PI);
    double tail = 3.5;
    for(int i=0;i<nbins;i++) {

        x[i] = tail*( 2.*(i-1.)/(nbins-2.) - 1.);
        gauss[i] = k*exp(-.5*x[i]*x[i]);
    }

    // calc accum gauss dist
    CumSum( gauss, gauss_accum, true );    

    // make hist of incoming vector
    CDVector hist, accum_hist;
    out = v;
    Hist( out, out.Min(), out.Max(), hist, nbins, true, true );
    CumSum( hist, accum_hist, true );

    // make a lookup table that maps 'hist' into 
    // a gaussian distribution
    CDVector gausslut(nbins);        
    int binhit = 0;
    for(i=0;i<nbins;i++) {
       
        // use monotonicity and find the closest class in
        // the accumulative gaussian distribution         
        while( fabs(accum_hist[i]-gauss_accum[binhit+1]) < 
               fabs(accum_hist[i]-gauss_accum[binhit])    ) {

            ++binhit;
        }

        // map the i-th class of v into x 
        gausslut[i] = x[binhit];        
    }       

    // transform 
    for(i=0;i<v_len;i++) {

        out[i] = gausslut[out[i]];
    }
}


/**

  @author   Mikkel B. Stegmann
  @version  1-22-2002

  @memo     Calulates the Mutual Information (MI) between two vectors.

  @doc      Calulates the Mutual Information (MI) between two vectors.
            See e.g. C. Studholme et al. or Viola et al.
    
  @param    v1      Signal 1.
  @param    v2      Signal 2.
  @param    nbins   The number of bins in the single and joint histograms.
                    Default 256.

  @return   The Mutual Information (MI).
  
*/
double MutualInformation( const CDVector &v1, const CDVector &v2, int nbins = 256 ) {

    assert( v1.Length()==v2.Length() );
    
    double v1min, v1max, v2min, v2max;
    v1min = v1.Min();
    v1max = v1.Max();
    v2min = v2.Min();
    v2max = v2.Max();
    int v1bin, v2bin, len = v1.Length();    

    // creat hists and reset them
    CDVector hist1( nbins);
    CDVector hist2( nbins);        
    CDMatrix hist12( nbins, nbins );
    hist1  = .0;
    hist2  = .0;
    hist12 = .0;

    // make and normalize hists
    double mag1 = v1max-v1min;
    double mag2 = v2max-v2min;
    for(int i=0;i<len;i++) {

        v1bin = (int)(.5+(nbins-1)*(v1[i]-v1min)/mag1);
        v2bin = (int)(.5+(nbins-1)*(v2[i]-v2min)/mag2);

        ++hist1[v1bin];
        ++hist2[v2bin];
        ++hist12[v1bin][v2bin];
    }
    hist1  /= len;
    hist2  /= len;
    hist12 /= len;

    // calculate entropy of the signals 
    double eps = 2.220446049250313e-016;
    double hi, H1=.0, H2=.0, log2 = log(2);    
    for(i=0;i<nbins;i++) {

        hi = hist1[i]; H1 -= hi*log(hi+eps)/log2;
        hi = hist2[i]; H2 -= hi*log(hi+eps)/log2;
    }
    // calculate joint entropy
    double H12=.0;
    for(i=0;i<nbins;i++) {
        for(int j=0;j<nbins;j++) {

            hi = hist12[i][j]; 
            H12 -= hi*log(hi+eps)/log2;
        }
    }
      
    // calculate the mutual information
    return H1+H2-H12;

}


/**

  @author   Mikkel B. Stegmann
  @version  2-2-2001

  @memo     A small demonstration of the DIVA project.

  @doc      A small demonstration of the DIVA project.
   
  @param    argc    The number of command line arguments.

  @param    argv    An array of char pointers.
  
  @return   Nothing.
  
*/
void DIVAMain(int argc, char **argv) {
    
    
    CDMatrix A(6,2), P;   
    A.Rand();    
    A.ToMatlab("c:\\aa.m", "a", "", false );



    int m = A.NRows();
    int n = A.NCols();
    CDMatrix U(m,n), V(n,n), SI;
    CDVector s(n);  
    
    VisDMatrixSVD( A, s, U, V, 1, 1 );

    // construct SI              
    SI.Resize( n, n );
    SI = .0;
    double si,eps = 1e-16;

    // a tolerance setting that fell down from matlab heaven
    double tolerance = max( m, n ) * s.Max() * eps;

    for(int i=0;i<n;i++) {

        si = s[i];       
        SI[i][i] = si>tolerance ? 1./si : .0;
    }

    // calc pinv(A)
    //
    // beware: potentially very large temporary objects ahead(!)
    // any zero-multiplications below could also be avoided
    //
    P = V*SI*U.Transposed();



    CDMatrix pi = (A.Transposed()*A).Inverted()*A.Transposed();
    printf( "M = \n%s\n", A.ToString() );
    printf( "P = \n%s\n", P.ToString() );
    printf( "P = \n%s\n", pi.ToString() );

	CDVector v;

	v.Linspace( 1, 20, 20 );
	//v.Shuffle();

	printf("v = %s", v.ToString() );
	printf("Mean(v) = %f\n", v.Mean() );
	printf("trimmedMean(v) = %f\n", v.TrimmedMean() );
	printf("Std(v) = %f\n", v.Std() );
	printf("trimmedStd(v) = %f\n", v.TrimmedStd() );
	printf("Var(v) = %f\n", v.Var() );
	printf("trimmedVar(v) = %f\n", v.TrimmedVar() );

	v[19] = 100000;

	printf("v = %s", v.ToString() );
	printf("Mean(v) = %f\n", v.Mean() );
	printf("trimmedMean(v) = %f\n", v.TrimmedMean() );
	printf("Std(v) = %f\n", v.Std() );
	printf("trimmedStd(v) = %f\n", v.TrimmedStd() );
	printf("Var(v) = %f\n", v.Var() );
	printf("trimmedVar(v) = %f\n", v.TrimmedVar() );


	v.Shuffle();
	CDVector v2 = v.Trim( 1 );
	v2.Sort();
	printf("v = %s", v2.ToString() );

	CDVector big(5);

	big.Rand( 0, 1000);
	big.Rand( 0, 1000);
	printf("min = %.0f, max = %.0f\n", big.Min(), big.Max() );
	
}





/////////////////////////////////////////////////////////////////////////////
// The one and only application object

CWinApp theApp;

using namespace std;

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	int nRetCode = 0;

	// initialize MFC and print and error on failure
	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		// TODO: change error code to suit your needs
		cerr << _T("Fatal Error: MFC initialization failed") << endl;
		nRetCode = 1;
	}
	else
	{
		DIVAMain(argc, argv);		
	}

	return nRetCode;
}
