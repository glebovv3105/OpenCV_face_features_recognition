/////////////////////////////////////////////////////////////////////////////
//
// This file contains the implementation of the steepest descent
// optimization algorithm. Steepest descent is implemented 
// with soft line search. See e.g. Dennis and Schnabel, Numerical 
// Methods for Unconstrained Optimization and Nonlinear Equations 
// 1983, Prentice-Hall for a description of steepest descent.
//
// Copyright � 2000
//
// DTU Image Viever and Analyser (DIVA)
// Department of Mathematical Modelling
// Technical University of Denmark (DTU), Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/~diva
//
// author: Rune Fisker 
//
// Disclaimer:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on the Microsoft Vision Software Developers Kit VisSDK
//
/////////////////////////////////////////////////////////////////////////////

#include "DIVAOpt.h"

#include "DOptimizeSD.h"

// ----------------------------[ Minimize ]----------------------------
// conjugate gradient
//
// Input: start point for x
// Output in x: x* - optimal point
ETermCode CDOptimizeSD::Minimize(CDVector& x,CDOptimizeFuncBase* pFuncEvalBase)
{
	// set the optimization function pointer
	SetFuncEvalBase(pFuncEvalBase);

	// initialize counters and vector
	m_nFuncEval = 0;
	m_nGradEval = 0;
	m_vNFuncEval = -1;
	m_vvFuncParm.clear();

	// eval func.
	double fc = EvalFunction(x);

	// and gradient
	CDVector gc(x.Length());
	EvalGradient(x,gc,fc);

	ETermCode eTermCode = UmStop0(x, fc, gc);
	
	if (eTermCode != etermNoStop) 
	{
		throw("Minimum at initial position");
	}
  	
	double fplus;
	CDVector xplus(x.Length()), gcOld(x.Length()), gplus(x.Length()), sn(x.Length()); 
	bool maxtaken;
	int retcode;
  	
	// save func. value and number of func. values
	if (m_fLogFuncValues) 
	{
		m_vFuncVal[0] = fc;
		m_vNFuncEval[0] = 0;  
		m_vvFuncParm.push_back(x);
	}
	
	//
	// Iteration section
	//

	m_nIterations = 1;
	while (eTermCode == etermNoStop) 
	{
		// set the decent direction equal to the negative gradient
		sn = -gc;

		// perform line search
		retcode = LineSearch(x, fc, gc, sn, xplus, fplus, maxtaken);
		
		// calc. new gradient (if needed)
		if (retcode == 0)
		{
			EvalGradient(xplus,gplus,fplus);
		}

		// stop ??
		eTermCode = UmStop(x, xplus, fc, fplus, gplus, retcode, maxtaken);
		
		if ( eTermCode > 0 ) 
		{		
			x = xplus;

			// update
			pFuncEvalBase->Update(x);

		}
		else 
		{
			x = xplus;
			fc = fplus;
			gc = gplus;

			// update
			pFuncEvalBase->Update(x);
		}

		// save func. value and number of func. values
		if (m_fLogFuncValues) 
		{
			m_vFuncVal[m_nIterations] = fc;
			m_vNFuncEval[m_nIterations] = m_nFuncEval;  
			m_vvFuncParm.push_back(x);
		}

		// increase number of number iterations
		m_nIterations++;
	}	

	return eTermCode;
}


