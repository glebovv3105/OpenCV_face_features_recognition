/////////////////////////////////////////////////////////////////////////////
//
// This file contains the implementation of the quasi-newton BFGS 
// optimization algorithm. BFGS is implemented according to 
//
// Dennis and Schnabel, Numerical Methods for Unconstrained Optimization and 
// Nonlinear Equations 1983, Prentice-Hall
//
// Copyright � 1999
//
// DTU Image Viever and Analyser (DIVA)
// Department of Mathematical Modelling
// Technical University of Denmark (DTU), Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/~diva
//
// author: Per Andresen and Rune Fisker 
//
// Disclaimer:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on the Microsoft Vision Software Developers Kit VisSDK
//
/////////////////////////////////////////////////////////////////////////////

#include "VisMatrix.h"
#include <math.h>

#include "DIVAOpt.h"

#include "DOptimizeBFGS.h"

// ----------------------------[ InitHessUnFac ]----------------------------
// Algorithm A9.4.3 p. 359
void CDOptimizeBFGS::InitHessUnFac(double fc, CVisDMatrix& Hc)
{  
//	const double temp = max(abs(fc), m_dTypF);
	const double temp = 1;
	
	Hc = 0; // set all elements to zero
	for(int i = Hc.NRows()-1; i >= 0; --i)
	{
		Hc[i][i] = temp; //  temp * m_Sx(i) * m_Sx(i);
	}
}


//	----------------------------[ ModelHess ]----------------------------
//	Algorithm D5.5.1 (look at page 315-318 for code)
// 
//	Description: 
//	Make Hc has positive definite
//	Only step 13 is implemented, because of shortcut by the use of LAPACK
void CDOptimizeBFGS::ModelHess(CVisDMatrix& mH)
{
	// step 13
	double dMaxEv = mH[0][0];
	double dMinEv = mH[0][0];
	double dMaxAdd = mH[0][0];

	for (int i=0;i<mH.NRows();i++)
	{
		double dOffRow = 0;

		for (int j=0;j<i;j++)
			dOffRow += mH[j][i];

		for (j=i+1;j<mH.NCols();j++)
			dOffRow += mH[j][i];

		dMaxEv = max(dMaxEv,dOffRow + mH[i][i]);
		dMinEv = min(dMaxEv,dOffRow - mH[i][i]);
		dMaxAdd = max(dMaxAdd,mH[i][i]);
	}

	double dSdd = (dMaxEv-dMinEv) * sqrt(m_dMachEps) - dMinEv;
	dSdd = max(dSdd,0);

	double dMy = min(dMaxAdd,dSdd);

	assert(dMy != 0);

	// add my to diagonal
	for (i=0;i<mH.NRows();i++)
	{
		mH[i][i] += dMy;
	}
}

// ----------------------------[ BFGSUnFac ]----------------------------
// Algorithm A9.4.1 p. 355
//
// Input: x, xplus, gc, gplus
// Input-Output: H
//
// Description: The BFGS update is made unless either: i) y'*s < (macheps)^0.5*||y|| or
//   ii) for every i, |(y-Hs)[i]| is less than the estimated noise in y[i].
//   The estimated noise in y[i] is calculated to tol*(|gc[i]| + |gplus[i]|) where tol=eta
//   if analytic gradients are being used, tol=eta^0.5 if finite difference gradients
//   are being used.
void CDOptimizeBFGS::BFGSUnFac(CDVector& xc, CDVector& xplus, CDVector& gc, CDVector& gplus, CVisDMatrix& Hc)
{  
	CDVector s(xc.Length()), y(xc.Length()), t(xc.Length());
	double temp1, temp2;
	double tol = m_dEta; 
	bool skipupdate;
	int i,j;
	
	s = xplus - xc;
	y = gplus - gc;
	temp1 = y*s; //y^T dot s

	if ( temp1 > sqrt(m_dMachEps)*sqrt(s*s)*sqrt(y*y) ) //  sqrt(y*y) calculates 2-norm of the vector y;
	{ 
		skipupdate = true;
		t = Hc*s; // I'm not using that H is symmetric !!!
		
		for (i = xc.Length() - 1; i >= 0; --i) 
		{
			if ( abs( y[i] - t[i] ) >= tol*max( abs(gc[i]), abs(gplus[i]) ) ) 
			{
				skipupdate = false;
			}
		}

		if ( skipupdate == false) 
		{
			assert(temp1 != 0);
		
			temp2 = s*t; // s^T dot t <=> s^T*H*s
			assert(temp2 != 0);

			for (i = xc.Length() - 1; i >= 0; --i) 
			{
				for (j = xc.Length() - 1; j >= 0; --j) 
				{
					Hc[i][j] += y[i]*y[j]/temp1 - t[i]*t[j]/temp2;
				}
			}
			assert(Hc.IsSymmetric() == true);
		}	
	}
}




// ----------------------------[ Minimize ]----------------------------
// Algorithm D6.1.1 (look at page 274-275 for code)
// Input: start point for x
// Output in x: x* - optimal point
ETermCode CDOptimizeBFGS::Minimize(CDVector& x,CDOptimizeFuncBase* pFuncEvalBase)
{
	double fc, fplus;
	CDVector xplus(x.Length());
	CDVector gc(x.Length()), gplus(x.Length());
	CDVector sn(x.Length()); 
	CVisDMatrix Hc(x.Length(),x.Length());
	bool maxtaken;
	int retcode;

	// set the optimization function pointer
	SetFuncEvalBase(pFuncEvalBase);
	
	// initialize counters and vector
	m_nFuncEval = 0;
	m_nGradEval = 0;
	m_vNFuncEval = -1;
	m_vvFuncParm.clear();

	fc = EvalFunction(x);

	EvalGradient(x,gc,fc);

	ETermCode eTermCode = UmStop0(x, fc, gc);
	
	if (eTermCode == etermNoStop)
	{
		InitHessUnFac(fc,Hc);
		assert(Hc.IsSymmetric() == true);
  	
		// save func. value and number of func. values
		if (m_fLogFuncValues) 
		{
			m_vFuncVal[0] = fc;
			m_vNFuncEval[0] = 0;  
			m_vvFuncParm.push_back(x);
		}
		
		//
		// Iteration section
		//
		
		m_nIterations = 1;
		while (eTermCode == etermNoStop) 
		{
			try
			{
				// compared to the original algorithme we do a shortcut by letting LAPACK
				// solve Hc*sn = -gc for sn. If Hc is singular VisDMatrixSolve throws an exception
				sn = VisDMatrixSolve(Hc,-gc);
			}
			catch (...)
			{			
				// make Hc have positive definite
				ModelHess(Hc);
				
				// solve again
				sn = VisDMatrixSolve(Hc,-gc);
			}
			
			// perform line search
			retcode = LineSearch(x, fc, gc, sn, xplus, fplus, maxtaken);
			
			// calc. new gradient (if needed)
			if (retcode == 0)
			{
				EvalGradient(xplus,gplus,fplus);
			}
			
			// stop ??
			eTermCode = UmStop(x, xplus, fc, fplus, gplus, retcode, maxtaken);
			
			if ( eTermCode > 0 ) 
			{
				x = xplus;

				// update
				pFuncEvalBase->Update(x);

			}
			else 
			{
				BFGSUnFac(x,xplus, gc, gplus, Hc);
				assert(Hc.IsSymmetric() == true);
				
				assert(fplus < fc);
				x = xplus;
				fc = fplus;
				gc = gplus;

				// update
				pFuncEvalBase->Update(x);
			}
			
			// save func. value and number of func. values
			if (m_fLogFuncValues) 
			{
				m_vFuncVal[m_nIterations] = fc;
				m_vNFuncEval[m_nIterations] = m_nFuncEval;  
				m_vvFuncParm.push_back(x);
			}
			
			// increase number of number iterations
			m_nIterations++;
		}
	}

	return eTermCode;
}


