/////////////////////////////////////////////////////////////////////////////
//
// Source-file for the simulated annealing optimization class.
// Simulated annealing [1,2] is implemented with the Metroplis algorithm [3],
// random walk with random and temperature scheme T_t+1 = k T_t [1]
//
// [1]	Kirkpatrick, S. and Gellant, C. D. and Vecchi, M. P.,
//		Optimization by simulated annealing, 
//		Science, 1983, vol. 220, pp.671-680
// [2]	Cerny, V., 
//		Thermodynamical approach to the traveling salesman problem: an efficient simulation algorithm},
//		Jour. of Optimization Theory and Applications,
//		1985, vol. 45, pp. 41-45
// [3]  Metropolis, N. and Rosenbluth, A. W. and Rosenbluth, M. N. and Teller A. H. and Teller E.,
//		Equations of state calculations by fast computing machines,
//		Jour. Chemical Physics,
//		1953, 21, 1087-1092
//
// Copyright � 2000
//
// IMM, Department of Mathematical Modelling
// Technical University of Denmark, Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/~diva
//
// author: Rune Fisker 
//
// Disclaimer:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on the Microsoft Vision Software Developers 
// Kit VisSDK and CLAPACK.
//
/////////////////////////////////////////////////////////////////////////////
  
#include "DIVAOpt.h"

#include "DOptimizeSA.h"
#include <time.h>

CDOptimizeSA::CDOptimizeSA()
{
	// set start temperature
	m_dStartTemperature = 0.01; 

	// decay factor
	m_dK = 0.98;

	// flag for random seed in random walk
	m_eSeedToRand = eseedRandom;
}

ETermCode CDOptimizeSA::Minimize(CDVector& x, CDOptimizeFuncBase* pFuncEvalBase)
{	
	// set the optimization function pointer
	SetFuncEvalBase(pFuncEvalBase);

	// initialize log counters and vector
	m_nFuncEval = 0;
	m_nGradEval = 0;
	m_vNFuncEval = -1;
	m_vvFuncParm.clear();
	
	CDVector xNew(x.Length());

	CDVector vParStd(x.Length());
	vParStd = MethodPar();

	// random, constant or no seed
	if (m_eSeedToRand == eseedRandom)
	{
		srand( (unsigned)time( NULL ) );
	}
	else if (m_eSeedToRand == essedConstant)
	{
		srand( 10000 );
	}

	// eval func.
	double dFx = EvalFunction(x);

	// log if enabled
	if (m_fLogFuncValues)
	{
		// save func. value and number of func. values
		m_vFuncVal[0] = dFx;
		m_vNFuncEval[0] = 0;  
		m_vvFuncParm.push_back(x);
	}

	//
	// Iteration section
	//
	double dTemperature = m_dStartTemperature;

	m_nIterations = 1;
	while (m_nIterations < MaxIterations()) 
	{
		// generate new configuration by random walk
		for (int iPar=0;iPar<x.Length();iPar++)
		{
			double dG = Gauss();
			xNew[iPar] = x[iPar] + vParStd[iPar]*dG;
		}

		// eval new configuration
		double dFxNew = EvalFunction(xNew);

		// calc. acceptance probability before exp
		double dExpon = -(dFxNew - dFx)/dTemperature;
		
		// stop if dProbAccept equals zero anyway
		// note exp(-20) = 2.0612e-009
		if (dExpon > -20)
		{
			double dProbAccept = 1;
			double UniRand = 0;

			// only change if needed
			if (dExpon < 0)
			{
				// update dProbAccept
				dProbAccept = min(1, exp( dExpon ) );

				// generate uniform rand number [0;1]
				UniRand = ((double) rand())/RAND_MAX;			
			}
			
			// accept if better
			if (dProbAccept >= UniRand)
			{
				x = xNew;
				dFx = dFxNew;
				
				// update
				pFuncEvalBase->Update(x);
				
			}
		}

		// update temperature
		dTemperature = m_dK * dTemperature;

		// save func. value and number of func. values
		if (m_fLogFuncValues)
		{
			m_vFuncVal[m_nIterations] = dFx;
			m_vNFuncEval[m_nIterations] = m_nFuncEval; 
			m_vvFuncParm.push_back(x);
		}
		
		// increase iterations
		m_nIterations++;
	}

	return etermMaxIterations;
}

// ##### make randomize gauss #####
double CDOptimizeSA::Gauss()
{
	static int iset=0;
	static double gset;
	double  fac,r,v1,v2;
	
	if (iset == 0) 
	{
		do 
		{
			double dRand1 = ((double) rand())/RAND_MAX;
			double dRand2 = ((double) rand())/RAND_MAX;
			v1=2.0*dRand1-1.0;
			v2=2.0*dRand2-1.0;
			r=v1*v1+v2*v2;
		} 
		while (r>=1.0);
		
		fac=sqrt(-2.0*log(r)/r);
		gset=v1*fac;
		iset=1;
		return v2*fac;
	} 
	else 
	{
		iset=0;
		return gset;
	}
}
