#if !defined(__DIVA_OPT_DLL__)
#define __DIVA_OPT_DLL__


#ifndef DIVAOptExport 
#define DIVAOptExport __declspec(dllexport)
#define EXPIMP
#endif // DIVAOptExport


/*

  DLL issues:

  http://support.microsoft.com/support/kb/articles/Q168/9/58.ASP

*/

#endif // __DIVA_OPT_DLL__