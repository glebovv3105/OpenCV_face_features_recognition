/////////////////////////////////////////////////////////////////////////////
//
// This file contains the implementation of the pattern search [1]
// optimization algorithm.
//
// [1]	Hooke, R. and Jeeves, T. A., Direct search: solution of numerical and statistical problems,
//		Jour. Assoc. Comput., 1961, vol. 8, pp. 212-229
//
// Copyright � 2000
//
// DTU Image Viever and Analyser (DIVA)
// Department of Mathematical Modelling
// Technical University of Denmark (DTU), Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/~diva
//
// author: Rune Fisker 
//
// Disclaimer:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on the Microsoft Vision Software Developers Kit VisSDK
//
/////////////////////////////////////////////////////////////////////////////

#include "DIVAOpt.h"

#include "DOptimizePS.h"

ETermCode CDOptimizePS::Minimize(CDVector& x, CDOptimizeFuncBase* pFuncEvalBase)
{
	// set the optimization function pointer
	SetFuncEvalBase(pFuncEvalBase);
	
	// initialize log counters and vector
	m_nFuncEval = 0;
	m_nGradEval = 0;
	m_vNFuncEval = -1;
	m_vvFuncParm.clear();

	CDVector vXHat(x.Length()),vDelta(x.Length()),vGPlus(x.Length());
	vGPlus = 1;

	vXHat = x; 
	vDelta = MethodPar();

	ETermCode eTermCode = etermNoStop;

	double dFX = EvalFunction(x);
	double dFXHat = dFX;

	// save func. value and number of func. values
	if (m_fLogFuncValues) 
	{
		m_vFuncVal[0] = dFX;
		m_vNFuncEval[0] = 0;  			
		m_vvFuncParm.push_back(x);
	}

	m_nIterations = 1;
	while (eTermCode == etermNoStop)
	{
		// generate new x
		Explore(vXHat,vDelta,dFXHat);

		// check for stop
		if (dFX == dFXHat)
			eTermCode = UmStop(x, vXHat, dFX*1.1, dFXHat, vGPlus, 0, false); // cheating for stopping because dFX == dFXHat
		else
			eTermCode = UmStop(x, vXHat, dFX, dFXHat, vGPlus, 0, false);

		// accept new x if better
		if (dFX > dFXHat)
		{		
			x = vXHat; 
			dFX = dFXHat;
 
			// update
			pFuncEvalBase->Update(x);

		}
		else // or update delta
		{
			vDelta /= 2.0;
		}

		// save func. value and number of func. values
		if (m_fLogFuncValues) 
		{
			m_vFuncVal[m_nIterations] = dFX;
			m_vNFuncEval[m_nIterations] = m_nFuncEval; 
			m_vvFuncParm.push_back(x);
		}

		// increase number of number iterations
		m_nIterations++;
	}

	return eTermCode;
}


void CDOptimizePS::Explore(CDVector& vX,CDVector& vDelta, double& dFVal)
{
	double dFMin,dFValPlus,dFValMinus;

	CDVector vE(vX.Length()),vTmp(vX.Length());

	// change each paramter
	for (int i=0; i<vX.Length(); i++)
	{
		vE = 0;
		vE[i] = vDelta[i];

		vTmp = vX+vE;
		dFValPlus = EvalFunction(vTmp);
		vTmp = vX-vE;
		dFValMinus = EvalFunction(vTmp);

		dFMin = min(dFValPlus,dFValMinus);

		// update if better
		if (dFMin < dFVal)
		{
			if (dFMin == dFValPlus)
			{
				vX = vX+vE;
				dFVal = dFMin;
			}
			else if (dFMin == dFValMinus)
			{
				vX = vX-vE;
				dFVal = dFMin;
			}
		}
	}
}