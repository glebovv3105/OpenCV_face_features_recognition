/////////////////////////////////////////////////////////////////////////////
//
// cpp-file for the optimization base class
//
// Copyright � 2000
//
// IMM, Department of Mathematical Modelling
// Technical University of Denmark, Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/~diva
//
// author: Rune Fisker 
//
// Disclaimer:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on the Microsoft Vision Software Developers 
// Kit VisSDK and CLAPACK.
//
/////////////////////////////////////////////////////////////////////////////

#include "VisMatrix.h"
#include <math.h>

#include "DIVAOpt.h"

#include "DOptimizeBFGS.h"

//////////////////////////////////////////////////////////////////////////////////////
// constructor
//
// Author: Rune Fisker, 5/9-2000
CDOptimizeBase::CDOptimizeBase() 
{
	m_nMaxFuncEval = 1;	// need to be assign a value before call to SetMaxIterarions() or SetMaxFuncEval()
	m_nMaxIterations = 1; // need to be assign a value before call to SetMaxIterarions() or SetMaxFuncEval()

	// set iteration and func eval. pars
	SetMaxFuncEval(300);
	SetMaxIterations(300);
	m_nFuncEval = 0; // counter to the number of function evaluations
	m_nGradEval = 0; // counter to the number of function evaluations

	// set other pars
	SetMachineEps(); // set eps
	m_nFDigits = -1;
	
	m_dGradTol = pow(m_dMachEps, 1.0/3.0);
	m_dStepTol = pow(m_dMachEps, 2.0/3.0);
	m_nConsecMaxStepMax = 5;
	m_iStopCriteria = etermMaxFuncEval | etermMaxIterations | etermLineSearch | etermDeltaFuncVal;

	m_nIterations = 0;
	m_nConsecMax = 0;
	m_dDeltaFuncVal = 0.0001;
	m_dMaxStep = 1e3;

	m_fAnalyticGrad = true;

	m_eNumGrad = CentralDifference;
//	m_eNumGrad = FitLine;
//	m_eNumGrad = ForwardDifference;

	m_fLogFuncValues = true; // Store function evaluation data

	
	if (m_nFDigits < 0) 
	{
		m_dEta = m_dMachEps;
	}
	else 
	{
		m_dEta = max(m_dMachEps,pow(10,-m_nFDigits));
	}
}


//////////////////////////////////////////////////////////////////////////////////////
// set limit for the number of iterations
//
// Author: Rune Fisker, 5/9-2000
void CDOptimizeBase::SetMaxIterations(const int nMaxIterations) 
{ 
	m_nMaxIterations = nMaxIterations;
	
	int nSize = max(m_nMaxFuncEval,m_nMaxIterations);

	m_vFuncVal.Resize(nSize+1); 
	m_vNFuncEval.Resize(nSize+1);

} 

//////////////////////////////////////////////////////////////////////////////////////
// set limit for the number of function evaluations
//
// Author: Rune Fisker, 5/9-2000
void CDOptimizeBase::SetMaxFuncEval(const int nMaxFuncEval) 
{ 
	m_nMaxFuncEval = nMaxFuncEval;
	
	int nSize = max(m_nMaxFuncEval,m_nMaxIterations);

	m_vFuncVal.Resize(nSize+1); 
	m_vNFuncEval.Resize(nSize+1);
} 


//////////////////////////////////////////////////////////////////////////////////////
// ----------------------------[ MachineEps ]----------------------------
// Calculate machine epsilon
//
// Algorithme A1.3.1 - p. 303 
// Dennis and Schnabel, Numerical Methods for Unconstrained Optimization and Nonlinear Equations
// 1983, Prentice-Hall
//
// author: Per Andresen and Rune Fisker, 6/4-1999
void CDOptimizeBase::SetMachineEps()
{
	double macheps = 1.0;

	do
	{
		macheps /= 2.0;
	} 
	while (1.0 + macheps > 1.0);

	m_dMachEps = 2.0 * macheps;
}



//////////////////////////////////////////////////////////////////////////////////////
// ----------------------------[ UmStop0 ]----------------------------
// Decide wether to terminate minimization at iteration zero
//
// Algorithm A7.2.1 p. 348
// Dennis and Schnabel, Numerical Methods for Unconstrained Optimization and Nonlinear Equations
// 1983, Prentice-Hall
//
// intput:
//	x0: parameter
//	functionValue: function value
//	gradient: gradient
//
// output:
//	return termination code
//
// author: Per Andresen and Rune Fisker, 6/4-1999
ETermCode CDOptimizeBase::UmStop0(CDVector& x0, const double functionValue, CDVector& gradient)
{
	ETermCode  eTermCode = etermNoStop;
	double maxScaledLength = 0;
	double tmpMaxScaledLength;
	int i;
	
	for(i = x0.Length()-1; i >= 0; --i){
		tmpMaxScaledLength = abs( gradient[i] ) * max( abs(x0[i]), 1.0 ) / max( abs(functionValue), m_dTypF ); 
		
		maxScaledLength = max(maxScaledLength, tmpMaxScaledLength);
	}

	if (maxScaledLength <= 1e-3 * m_dGradTol) {
		eTermCode = etermGradTol;
	}
	
	return eTermCode;
	
}


//////////////////////////////////////////////////////////////////////////////////////
// ----------------------------[ UmStop ]----------------------------
// Decide wether to terminate minimization
//
// Modified version of Algorithm A7.2.1 p. 347
// Dennis and Schnabel, Numerical Methods for Unconstrained Optimization and Nonlinear Equations
// 1983, Prentice-Hall
//
// Input: 
//	x: parameter
//	xplus: new parameter
//	fplus: function value for new parameter
//	g: gradient at x
//	retcode: return code from line search
//	maxtaken: max taken in line search
//
// Output: 
//	return termination code
//
// author: Per Andresen and Rune Fisker, 6/4-1999
ETermCode CDOptimizeBase::UmStop(const CDVector& x,const CDVector& xplus,const double f,const double fplus,const CDVector& g, const int retcode, const bool maxtaken)
{
	if (m_iStopCriteria == 0)
	{
		throw CVisError("No stop criteria enabled!!",eviserrorUnknown,"UmStop",__FILE__, __LINE__);	
	}
	
	ETermCode eTermCode = etermNoStop;
	
	if ( m_iStopCriteria & etermLineSearch )
	{
		if (retcode == 1) 
		{
			eTermCode = etermLineSearch;
		}
	}
	
	if ((m_iStopCriteria & etermGradTol) && (eTermCode == etermNoStop))
	{
		double absMaxGrad = 0.0;
		for (int i = x.Length() - 1; i >= 0; --i) {
			absMaxGrad = max( absMaxGrad, abs( g[i]*xplus[i]/fplus ) );
		}

		if ( absMaxGrad <= m_dGradTol )
		{
			eTermCode = etermGradTol;
		}
		
	}	
	
	if ((m_iStopCriteria & etermStepTol)  && (eTermCode == etermNoStop)) 
	{
		double absMaxStep = 0.0;
		for (int i = x.Length() - 1; i >= 0; --i) 
		{
			absMaxStep = max( absMaxStep, abs( (xplus[i] - x[i])/xplus[i] ) );
		}

		if ( absMaxStep <= m_dStepTol ) 
		{
			eTermCode = etermStepTol;
		}
	}
	
	if ((m_iStopCriteria & etermMaxIterations) && (eTermCode == etermNoStop))
	{
		if (m_nIterations >= MaxIterations()) 
		{
			eTermCode = etermMaxIterations;
		}
	}
	
	if ((m_iStopCriteria & etermDeltaFuncVal) && (eTermCode == etermNoStop))
	{	
		if (abs(f - fplus) < m_dDeltaFuncVal)
		{
			eTermCode = etermDeltaFuncVal;
		}
	}
	
	if ((m_iStopCriteria & etermMaxFuncEval) && (eTermCode == etermNoStop))
	{
		if (m_nFuncEval >= MaxFuncEval()) 
		{
			eTermCode = etermMaxFuncEval;
		}
	}
	
	if ((m_iStopCriteria & etermConsecMaxStepMax) && (eTermCode == etermNoStop))
	{
		if (maxtaken) 
		{
			++m_nConsecMax;
			if ( m_nConsecMax >= m_nConsecMaxStepMax ) 
			{
				eTermCode = etermConsecMaxStepMax;
			}
		}
		else
		{
			m_nConsecMax = 0;
		}
	}
		
	return eTermCode;
}


//////////////////////////////////////////////////////////////////////////////////////
// Perform line search
//
// Input: 
//	xc: parameter
//	fc: function value at xc
//	g: gradient at xc
//	p: search direction
//	xplus: new parameter
//	fplus: function value for new parameter
//
// Output: 
//	maxtaken: max taken in line search
//	return termination code
//
// author: Per Andresen and Rune Fisker, 6/4-1999
int CDOptimizeBase::LineSearch(const CDVector& xc, const double fc, const CDVector& g, const CDVector& p,CDVector& xplus, double& fplus, bool& maxtaken, bool fSoft)
{
	if (fSoft)
	{
		return SoftLineSearch(xc, fc, g,  p, xplus, fplus, maxtaken);
	}
	else
	{
		return ExactLineSearch(xc, fc, g,  p, xplus, fplus, maxtaken);
	}
}

//////////////////////////////////////////////////////////////////////////////////////
// ----------------------------[ LineSearch ]----------------------------
// Perform exact line search
//
// Input: 
//	xc: parameter
//	fc: function value at xc
//	g: gradient at xc
//	p: search direction
//
// Output: 
//	xplus: new parameter
//	fplus: function value for new parameter
//	maxtaken: max taken in line search
//	return termination code
//
// author: Rune Fisker, 6/4-1999
int CDOptimizeBase::ExactLineSearch(const CDVector& xc, const double fc, const CDVector& g, const CDVector& p,CDVector& xplus, double& fplus, bool& maxtaken)
{
	double dLambda,dLambdaMax=0.5,dLambdaStep = 0.05,dFTmp;

	CDVector vXTmp(xc.Length());

	fplus = fc;
	xplus = xc;

	for (dLambda=dLambdaStep; dLambda<=dLambdaMax;dLambda+=dLambdaStep)
	{
		vXTmp = xc + p*dLambda;

		dFTmp = EvalFunction(vXTmp);

		if (dFTmp < fplus)
		{
			fplus = dFTmp;
			xplus = vXTmp;
			maxtaken = true;
		}
	}

	return 0;
}

//////////////////////////////////////////////////////////////////////////////////////
// ----------------------------[ LineSearch ]----------------------------
// Perform line search
// Given g'p < 0 and alpha < 1/2 (alpha = 1e-4 is used), find
// plus = xc + lambda p,lambda in [0;1], such that
// f(xplus) <= f(xc) + alpha * lambda * g'p, using backtracking
// line search
//
// Algorithm A6.3.1 p. 325
// Dennis and Schnabel, Numerical Methods for Unconstrained Optimization and Nonlinear Equations
// 1983, Prentice-Hall
//
// Input: 
//	xc: parameter
//	fc: function value at xc
//	g: gradient at xc
//	sn: search direction
//
// Output: 
//	xplus: new parameter
//	fplus: function value for new parameter
//	maxtaken: max taken in line search
//	return termination code
//
// author: Per Andresen and Rune Fisker, 6/4-1999
int CDOptimizeBase::SoftLineSearch(const CDVector& xc, const double fc, const CDVector& g, const CDVector& sn,CDVector& xplus, double& fplus, bool& maxtaken)
{
	// create a copy of the search dir., which is normalized later
	CDVector p(sn.Length());
	p = sn;

	double retcode; /* retcode = 0: satisfactory xplus found
				   retcode = 1: routine failed to locate satisfactory
				   xplus sufficiently distinct from x */

	double alpha = 1e-4;
	double initslope = g*p; // initslope = g' dot p
	int i;
	double rellength; // Relative length of p as calculated in the stopping routine
	double minlambda; // minlambda is the minimum allowable steplength
	double lambda,lambdaPrev, lambdaTemp;
	double a,b,disc;
	double fplusPrev;
	
	maxtaken = false;
	retcode = 2; // initial value to keep the do..while loop 'alive'
  	
	// definition of dnrm2 found in /usr/include/cblas.h
	//  Newtlen = dnrm2(p.Size(),p.Addr(),1); // calculates 2-norm of the vector p;
	
	// calculates 2-norm of the vector p, i.e. Norm2 := sqrt( x'*x )   	
	double Newtlen = sqrt(p*p);
	
	if (Newtlen > m_dMaxStep) {
		p *= m_dMaxStep/Newtlen;
		Newtlen = m_dMaxStep;
	}
	
	//initslope = g*p; - allready done
	rellength = 0.0;
	for(i = xc.Length() - 1; i >= 0; --i) 
	{
		rellength = max( rellength, abs(p[i]) / max(abs(xc[i]), 1.0) );
	}
	
	minlambda = m_dStepTol / rellength;
	
	lambda = 1.0;
	bool fExact = false, fTerminate = false;
	double dSlopeLambda;

	do 
	{
		xplus = xc + p * lambda;
//		fplus = (*m_pFN)(xplus);
		fplus = EvalFunction(xplus);

		if (fExact)
		{
			// dos not very well
			CDVector vGradPlus(xplus.Length());

			EvalGradient(xplus,vGradPlus,fplus);

			dSlopeLambda = vGradPlus*p; 

			fTerminate = (abs(dSlopeLambda) < -0.25*initslope) && (fplus <= fc);
		}
		else
		{
			fTerminate = (fplus <= fc + alpha * lambda * initslope);
		}

//		if (fplus <= fc + alpha * lambda * initslope) 
		if (fTerminate)
		{
			// satisfactory xplus found
			retcode = 0;
			if (lambda == 1.0 && Newtlen > 0.99*m_dMaxStep ) 
			{
				maxtaken = true;
			}
		}
		else 
		{
			if ( lambda < minlambda ) 
			{
				// No satisfactory xplus can be found sufficiently distinct from x
				retcode = 1;
				xplus = xc;
			}
			else 
			{ // reduce lambda
				if (lambda == 1.0) 
				{
					// first backtrack, quadratic fit
					lambdaTemp = -initslope/(2.0 * (fplus - fc - initslope));
				}
				else 
				{
					// All subsequent backtracks, cubic fit
					//
					// a & b is found in Mathematica (notebook named LineSeach.nb)
					//
					a = (-(fplusPrev*pow(lambda,2)) + lambdaPrev*
						(initslope*pow(lambda,2) + fplus*lambdaPrev -
						initslope*lambda*lambdaPrev) + 
						fc*(pow(lambda,2) - pow(lambdaPrev,2)))/
						(pow(lambda,2)*(lambda - lambdaPrev)*pow(lambdaPrev,2));
					
					b = (fplusPrev*pow(lambda,3) - initslope*pow(lambda,3)*lambdaPrev - 
						fplus*pow(lambdaPrev,3) + initslope*lambda*pow(lambdaPrev,3) + 
						fc*(-pow(lambda,3) + pow(lambdaPrev,3)))/
						(pow(lambda,2)*(lambda - lambdaPrev)*pow(lambdaPrev,2));
					
					disc = b*b - 3.0*a*initslope;
					
					if ( a == 0.0 ) 
					{
						// cubic is quadratic
						lambdaTemp = -initslope/(2.0 * b);
					}
					else 
					{
						// legitimate cubic
						lambdaTemp = (-b + sqrt(disc))/(3.0*a);
					}

					if ( lambdaTemp > 0.5*lambda ) 
					{
						lambdaTemp = 0.5 * lambda;
					}
				}

				lambdaPrev = lambda;
				fplusPrev = fplus;
				
				if ( lambdaTemp <= 0.1 * lambda ) 
				{
					lambda = 0.1 * lambda;
				}
				else 
				{
					lambda = lambdaTemp;
				}
			}
		}	
	} 
	while (retcode == 2);
	
	return retcode;
}

//////////////////////////////////////////////////////////////////////////////////////
// calculate nummerical gradient function usingdecided gradient calculation method
//
// input:
//	x: parameter
//	dFuncVal: function value in x
//
// output:
//	gradient: gradient
//
// author: Rune Fisker, 6/4-1999
void CDOptimizeBase::NumGrad(CDVector& x, CDVector& gradient, const double dFuncVal)
{
	// calculate gradient using forward difference
	if (m_eNumGrad == ForwardDifference)
	{
		double dPlus;
		CDVector vecTemp(x.Length());
		
		for (int i=0;i < x.Length();i++)
		{
			vecTemp = x;

			vecTemp[i] += m_vMethodPar[i];
			dPlus = EvalFunction(vecTemp);

			gradient[i] = (dPlus-dFuncVal)/m_vMethodPar[i];
		}
	}
	// calculate gradient using central difference
	else if (m_eNumGrad == CentralDifference)
	{
		double dPlus,dMinus;
		CDVector vecTemp(x.Length());

		for (int i=0;i < x.Length();i++)
		{
			vecTemp = x;

			vecTemp[i] -= m_vMethodPar[i];
			dMinus = EvalFunction(vecTemp);

			vecTemp[i] += 2*m_vMethodPar[i];
			dPlus = EvalFunction(vecTemp);

			gradient[i] = (dPlus-dMinus)/(2*m_vMethodPar[i]);
		}
	}
	// calculate gradient by fitting a line throu a number of function values.
	// The gradient is then found as the slope. This is equavilent to fitting 
	// a parabel and differentated it.
	else if (m_eNumGrad == FitLine)
	{
		int nFitPoints = 5; // 3 points equals central difference
		CDVector vTemp(x.Length()),vX(nFitPoints),vY(nFitPoints),vTheta(2);
		CDMatrix mA(nFitPoints,2);
		mA = 1;

		// estimate the gradient by fitting line to each parameter dimension
		for (int i=0;i < x.Length();i++)
		{
			vTemp = x;

			// calc. function vaules for differet setting in one parameter dim.
			int iFP = 0;
			for (int j = -nFitPoints/2; j <= nFitPoints/2; j++)
			{
				vX[iFP] = x[i] - j * m_vMethodPar[i];
				vTemp[i] = vX[iFP];
				vY[iFP++] = EvalFunction(vTemp);

				// do not calc. func. value equal to j = 0 again
				if (j == -1)
				{
					j++;
					vX[iFP] = x[i] - j * m_vMethodPar[i];
					vY[iFP++] = dFuncVal;
				}
			}

			// find LSQ solution (the fit of a line)
			mA.SetColumn(1,vX);

			VisDMatrixLeastSquares(mA,vY,vTheta);

			gradient[i] = vTheta[1];
		}
	}
}


//////////////////////////////////////////////////////////////////////////////////////
// the Minimize function using nummerical gradient
//
// input:
//	x: parameter
//	dFuncVal: function value in x
//
// output:
//	gradient: gradient
//
// author: Rune Fisker, 6/4-1999
ETermCode CDOptimizeBase::MinimizeNum(CDVector& x, CDOptimizeFuncBase *pFuncEvalBase ,CDVector& vMethodPar)
{
	// set gradient to nummerical
	m_fAnalyticGrad = false;

	// set the method par
	if (vMethodPar.Length() != x.Length())
	{
		throw CVisError("The number of parameters to optimize and the number of method parameters should be the same.",eviserrorUnknown,"MinimizeNum",__FILE__, __LINE__);	
	}
	else
	{
		m_vMethodPar.Resize(x.Length());
		m_vMethodPar = vMethodPar;
	}

	// set function to minized
	SetFuncEvalBase(pFuncEvalBase);

	// minimize using NumGrad to calc. gradient
	return Minimize(x,pFuncEvalBase);
}
