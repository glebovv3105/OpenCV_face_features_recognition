/*
 * This file is considered to be public domain software.  We hereby give
 * permission for anyone to make any use of this code, including copying the
 * code, including it with freely distributed software, including it with
 * commercially available software, and including it in ftp-able code.
 * We do not assert that this software is completely bug-free (although we hope
 * it is), and we do not support the software (officially) in any way.  The
 * intention is to make it possible for people to read and write standard HIPS
 * formatted image sequences, and write conversion programs to other formats,
 * without owning a license for HIPS-proper.  However, we do require that all
 * distributed copies of these source files include the following copyright
 * notice.
 *
 ******************************************************************************
 *
 * Copyright (c) 1992 Michael Landy
 *
 * Disclaimer:  No guarantees of performance accompany this software,
 * nor is any responsibility assumed on the part of the authors.  All the
 * software has been tested extensively and every effort has been made to
 * insure its reliability.
 *
 ******************************************************************************
 */

/*
 * hpub_rdhdr.c - HIPS Picture Format Header read (public domain version)
 *
 * Michael Landy - 9/30/92
 * added cmap support - msl - 5/26/95
 * added general xparam support - msl - 10/31/95
 * added ANSI-C support - jfh - 01/01/97
 */

#include "hipspub2.h"

int hpub_swallownl(FILE *fp);
int hpub_fgets(char *s, int n, FILE *fp);


int hpub_rdhdr(int *format,int *rows,int *cols,int *frames,int *colors)
{
  hipsstruct hd;
  hd.fp = stdin;
  hd.format = *format;
  hd.rows = *rows;
  hd.cols = *cols;
  hd.numframes = *frames;
  hd.colors = *colors;

  if (hpub_frdhdr(&hd) == FALSE) return FALSE;

  *format = hd.format;
  *rows = hd.rows;
  *cols = hd.cols;
  *frames = hd.numframes;
  *colors = hd.colors;
  return TRUE;
}

int hpub_rdhdrc(int *format,int *rows,int *cols,int *frames,int *colors,int *ncmap, byte **r,byte **g,byte **b)
{
  hipsstruct hd;
  hd.fp = stdin;
  hd.format = *format;
  hd.rows = *rows;
  hd.cols = *cols;
  hd.numframes = *frames;
  hd.colors = *colors;
  hd.ncmap = *ncmap;

  if (hpub_frdhdrc(&hd) == FALSE) return FALSE;

  if(hd.ncmap>0){
	*r = malloc(hd.ncmap);
	memcpy(*r, hd.r, hd.ncmap);
	*g = malloc(hd.ncmap);
	memcpy(*g, hd.g, hd.ncmap);
	*b = malloc(hd.ncmap);
	memcpy(*b, hd.b, hd.ncmap);
	free(hd.r);
	free(hd.g);
	free(hd.b);
  }

  *format = hd.format;
  *rows = hd.rows;
  *cols = hd.cols;
  *frames = hd.numframes;
  *colors = hd.colors;
  *ncmap = hd.ncmap;
  return TRUE;
}

int hpub_rdhdrx(int *format,int *rows,int *cols,int *frames,int *colors,struct hpub_xparlist *xpar)
{
  hipsstruct hd;
  hd.fp = stdin;
  hd.format = *format;
  hd.rows = *rows;
  hd.cols = *cols;
  hd.numframes = *frames;
  hd.colors = *colors;

  if (hpub_frdhdrx(&hd,xpar) == FALSE) return FALSE;

  *format = hd.format;
  *rows = hd.rows;
  *cols = hd.cols;
  *frames = hd.numframes;
  *colors = hd.colors;

  return TRUE;
}


int hpub_frdhdr(hipsstruct *hd)
{
  struct hpub_xparlist xpar;
  int *ndepth;
  int count;
  if (hpub_frdhdrx(hd,&xpar) == FALSE) return FALSE;
  
  if (!hpub_checkparam(&xpar,"ndepth")) {
	hd->ndepth = 1;
  } else {
	ndepth = hpub_getparami2(&xpar,"ndepth",&count);
	if(count==1)
	  hd->ndepth = *ndepth;
  }
  return TRUE;
}

/*int hpub_frdhdrc(FILE *fp,int *format,int *rows,int *cols,int *frames,int *colors,int *ncmap,byte **r,byte **g,byte **b)
{
  struct hpub_xparlist xpar;
  byte *cmap;
  int count;

  hpub_frdhdrx(fp,format,rows,cols,frames,colors,&xpar);
  if (!hpub_checkparam(&xpar,"cmap")) {
	*ncmap = 0;
	return TRUE;
  }
  cmap = hpub_getparamb2(&xpar,"cmap",&count);
  if (count%3) return FALSE;
  *ncmap = count/3;
  *r = cmap;
  *g = cmap + *ncmap;
  *b = cmap + 2*(*ncmap);
  return TRUE;
}*/

int hpub_frdhdrc(hipsstruct *hd)
{
  struct hpub_xparlist xpar;
  byte *cmap;
  int *ndepth;
  int count;

  if (hpub_frdhdrx(hd,&xpar) == FALSE) return FALSE;

  if (!hpub_checkparam(&xpar,"cmap")) {
	hd->ncmap = 0;
	return TRUE;
  }
  cmap = hpub_getparamb2(&xpar,"cmap",&count);
  if (count%3) return FALSE;
  hd->ncmap = count/3;

  if (!hpub_checkparam(&xpar,"ndepth")) {
	hd->ndepth = 1;
  } else {
	ndepth = hpub_getparami2(&xpar,"ndepth",&count);
	if(count==1)
	  hd->ndepth = *ndepth;
  }

  if(hd->ncmap>0){
	hd->r = malloc(hd->ncmap);
	memcpy(hd->r, cmap, hd->ncmap);
	hd->g = malloc(hd->ncmap);
	memcpy(hd->g, cmap + hd->ncmap, hd->ncmap);
	hd->b = malloc(hd->ncmap);
	memcpy(hd->b, cmap + 2*(hd->ncmap), hd->ncmap);
  }
  free(cmap);

  return TRUE;
}


int hpub_frdhdrx(hipsstruct *hd,struct hpub_xparlist *xpar)
{
 FILE *fp;
 int format=hd->format;
 int rows=hd->rows;
 int cols=hd->cols;
 int frames=hd->numframes;
 int colors=hd->colors;

 int roirows;
 int roicols;
 int frow;
 int fcol;
 
 char inp[LINELENGTH],ptypes[20];
 byte *buf;
 int i,j,sz,np,curroffset,sizebin;
 float f;
 struct hpub_extpar *xp,*lastxp=NULL,tmpxp;

 fp=hd->fp;
 if (fp==NULL) return FALSE;
 hpub_fgets(inp,LINELENGTH,fp);	/* magic */
 if (strcmp(inp,"HIPS\n") != 0) {
 	if (xpar != (struct hpub_xparlist *) 0) xpar->numparam = 0;
 	return FALSE;
 }
 hpub_fgets(hd->onm,LINELENGTH,fp);	/* onm */
 hpub_fgets(hd->snm,LINELENGTH,fp);	/* snm */
 if (fscanf(fp,"%d",&frames) != 1) return FALSE; 
 hpub_swallownl(fp);
 hpub_fgets(hd->odt,LINELENGTH,fp);	/* odt */
 if (fscanf(fp,"%d",&rows) != 1) return FALSE;                              
 if (fscanf(fp,"%d",&cols) != 1) return FALSE;                              
 if (fscanf(fp,"%d",&roirows) != 1)	/* roirows */ return FALSE;                              
 if (fscanf(fp,"%d",&roicols) != 1)	/* roicols */ return FALSE;                              
 if (fscanf(fp,"%d",&frow) != 1)	/* frow */ return FALSE;                              
 if (fscanf(fp,"%d",&fcol) != 1)	/* fcol */ return FALSE;                              
 if (fscanf(fp,"%d",&format) != 1) return FALSE;                              
 if (fscanf(fp,"%d",&colors) != 1) return FALSE;                              ;
 if (fscanf(fp,"%d",&sz) != 1)	/* szhist */ return FALSE;                              

/* set structure variables */
 hd->format=format;
 hd->rows=rows;
 hd->cols=cols;
 hd->numframes=frames;
 hd->colors=colors;

 /* calculate region of interest */
 if (frow<0) frow=0;
 if (fcol<0) fcol=0;
 if (roirows<0) roirows=0;
 if (roicols<0) roicols=0;

 hd->roi_r1=frow;
 hd->roi_c1=fcol;
 hd->roi_r2=frow+roirows-1;
 hd->roi_c2=fcol+roicols-1;

 if (hd->roi_c1>=hd->cols) hd->roi_c1=hd->cols-1;
 if (hd->roi_r1>=hd->rows) hd->roi_r1=hd->rows-1;
 if (hd->roi_c2>=hd->cols) hd->roi_c2=hd->cols-1;
 if (hd->roi_r2>=hd->rows) hd->roi_r2=hd->rows-1;

 hd->roi_z1 = 0;
 hd->roi_z2 = hd->numframes-1;


 /* read history */
 hd->sizehistory = sz;
 
 hpub_swallownl(fp);
 if (sz) {
 	if ((hd->history = (char *) malloc(sz)) == (char *) 0) return FALSE;
 	if (fread(hd->history,sz,1,fp) != 1) return FALSE;                              
 	
 }

 /* read description */
 if (fscanf(fp,"%d",&sz) != 1)	/* szdesc */ return FALSE;                              
 hd->sizedescription = sz;

 hpub_swallownl(fp);
 if (sz) {
 	if ((hd->description = (char *) malloc(sz)) == (char *) 0) return FALSE;
 	if (fread(hd->description,sz,1,fp) != 1) return FALSE;                              
 }

 /* read extended parameters */
 if (fscanf(fp,"%d",&np) != 1)	/* nparam */ return FALSE;                              
 hpub_swallownl(fp);
 if (xpar != (struct hpub_xparlist *) 0) {
 	xpar->numparam = np;
 	xpar->params = HP_NULLPAR;
 }
 for (i=0;i<np;i++) {
 	if (xpar != (struct hpub_xparlist *) 0) {
 		if ((xp = (struct hpub_extpar *) malloc(sizeof(struct hpub_extpar))) == HP_NULLPAR) return FALSE;
 		if (i == 0)
 			lastxp = xpar->params = xp;
 		else {
 			lastxp->nextp = xp;
 			lastxp = xp;
 		}
 		xp->nextp = HP_NULLPAR;
 	}
 	else
 		xp = &tmpxp;
 	if (fscanf(fp,"%s %s %d",inp,ptypes,&(xp->count)) != 3) return FALSE;                              
 	xp->name = hpub_strsave(inp);
 	switch(ptypes[0]) {
 	case 'c': xp->format = PFASCII; break;
 	case 'b': xp->format = PFBYTE; break;
 	case 'i': xp->format = PFINT; break;
 	case 'f': xp->format = PFFLOAT; break;
 	case 's': xp->format = PFSHORT; break;
 	default: return FALSE;                              
 	}
 	if (xp->count == 1) {
 		switch(xp->format) {
 		case PFASCII:
 		case PFBYTE:	if (fscanf(fp,"%d",&j) != 1) return FALSE;                              
 				xp->val.v_b = (byte)j;
 				break;
 		case PFSHORT:	if (fscanf(fp,"%d",&j) != 1) return FALSE;                              
 				xp->val.v_s = (short)j;
 				break;
 		case PFINT:	if (fscanf(fp,"%d",&j) != 1) return FALSE;                              
 				xp->val.v_i = j;
 				break;
 		case PFFLOAT:	if (fscanf(fp,"%f",&f) != 1) return FALSE;                              
 				xp->val.v_f = f;
 				break;
 		}
 	}
 	else {
 		if (fscanf(fp,"%d",&(xp->offset)) != 1) return FALSE;                              
 	}
 	hpub_swallownl(fp);
 }
 if (fscanf(fp,"%d",&sizebin) != 1) return FALSE;                 
 hpub_swallownl(fp);
 if (xpar == (struct hpub_xparlist *) 0) {
 	if (sizebin == 0) return TRUE;
 	if ((buf = (byte *) malloc(sizebin)) == (byte *) 0) return FALSE;
 	if (fread(buf,sizebin,1,fp) != 1) return FALSE;
 	free(buf);
 	return TRUE;
 }
 curroffset = 0;
 xp = xpar->params;
 while (xp != HP_NULLPAR) {
 	if (xp->count > 1) {
 		if (xp->offset != curroffset) return FALSE;
 		switch(xp->format) {
 		case PFASCII:
 		case PFBYTE:	sz = sizeof(byte); break;
 		case PFSHORT:	sz = sizeof(short); break;
 		case PFINT:	sz = sizeof(int); break;
 		case PFFLOAT:	sz = sizeof(float); break;
 		default:   	return FALSE;	
 		}
 		i = xp->count * sz;
 		i = (i+3) & ~03;
 		if ((xp->val.v_pb = (byte *) malloc(i)) == (byte *) 0) return FALSE;
 		if (fread(xp->val.v_pb,i,1,fp) != 1) return FALSE;
 		curroffset += i;
 	}
 	xp = xp->nextp;
 }
 if (curroffset != sizebin) return FALSE;

 return TRUE;
} 

int hpub_swallownl(FILE *fp)
{
  int i;

  while ((i = getc(fp)) != '\n') {
    if (i == EOF) return FALSE;
  }
  return TRUE;
}

int hpub_fgets(char *s, int n, FILE *fp)
{
  int i;

  fgets(s,n,fp);
  i=strlen(s);
  if (i==0 || s[i-1]!='\n') return FALSE;
  return TRUE;
}

char *hpub_strsave(char *s)
{
  char *news;

  if ((news = (char *) malloc(strlen(s)+1)) == (char *) 0) {
    hpub_perr("hpub_strsave: error allocating memory");
  }
  return strcpy(news,s);
}
