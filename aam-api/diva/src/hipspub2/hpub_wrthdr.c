/*
 * This file is considered to be public domain software.  We hereby give
 * permission for anyone to make any use of this code, including copying the
 * code, including it with freely distributed software, including it with
 * commercially available software, and including it in ftp-able code.
 * We do not assert that this software is completely bug-free (although we hope
 * it is), and we do not support the software (officially) in any way.  The
 * intention is to make it possible for people to read and write standard HIPS
 * formatted image sequences, and write conversion programs to other formats,
 * without owning a license for HIPS-proper.  However, we do require that all
 * distributed copies of these source files include the following copyright
 * notice.
 *
 ******************************************************************************
 *
 * Copyright (c) 1992 Michael Landy
 *
 * Disclaimer:  No guarantees of performance accompany this software,
 * nor is any responsibility assumed on the part of the authors.  All the
 * software has been tested extensively and every effort has been made to
 * insure its reliability.
 *
 ******************************************************************************
 */

/*
 * hpub_wrthdr.c - HIPS image header write (public domain version)
 *
 * Michael Landy - 9/30/92
 * Added ANSI-C support 01/01/97 
 */

#include "hipspub2.h"

int hpub_wnocr(FILE *fp, char *s);
int hpub_dfprintf(FILE *fp, int i);

int hpub_wrthdr(int format,int rows,int cols,int frames,int colors)
{
  hipsstruct hd;
  hd.fp = stdout;
  hd.format = format;
  hd.rows = rows;
  hd.cols = cols;
  hd.numframes = frames;
  hd.colors = colors;

  return hpub_fwrthdr(&hd);
}

int hpub_wrthdrc(int format,int rows,int cols,int frames,int colors,int ncmap,byte *r,byte *g,byte *b)
{
  hipsstruct hd;
  int retVal;
  hd.fp = stdout;
  hd.format = format;
  hd.rows = rows;
  hd.cols = cols;
  hd.numframes = frames;
  hd.colors = colors;
  hd.ncmap = ncmap;

  if(ncmap>0){
	hd.r = malloc(ncmap);
	memcpy(hd.r, r, ncmap);
	hd.g = malloc(hd.ncmap);
	memcpy(hd.g, g, ncmap);
	hd.b = malloc(hd.ncmap);
	memcpy(hd.b, b, ncmap);
  }

  retVal = hpub_fwrthdrc(&hd);

  free(hd.r);
  free(hd.g);
  free(hd.b);
  return retVal;
}

int hpub_wrthdrx(int format,int rows,int cols,int frames,int colors,struct hpub_xparlist *xpar)
{
  hipsstruct hd;
  hd.fp = stdout;
  hd.format = format;
  hd.rows = rows;
  hd.cols = cols;
  hd.numframes = frames;
  hd.colors = colors;

  return hpub_fwrthdrx(&hd,xpar);
}

int hpub_fwrthdr(hipsstruct *hd)
{
  struct hpub_xparlist xpar;

  xpar.numparam = 0;
  xpar.params = HP_NULLPAR;
  return hpub_fwrthdrx(hd,&xpar);
}

int hpub_fwrthdrc(hipsstruct *hd)
{
 struct hpub_xparlist xpar;
 unsigned char *cmap;
 int i;

 xpar.numparam = 0;
 xpar.params = HP_NULLPAR;
 if ((cmap = (unsigned char *) malloc(3*hd->ncmap)) == (unsigned char *) 0) return FALSE;
 for (i=0;i<hd->ncmap;i++) {
 	cmap[i] = hd->r[i];
 	cmap[hd->ncmap+i] = hd->g[i];
 	cmap[2*hd->ncmap+i] = hd->b[i];
 }
 hpub_setparamb2(&xpar,"cmap",hd->ncmap*3,cmap);
 return hpub_fwrthdrx(hd,&xpar);
}

int hpub_fwrthdrx(hipsstruct *hd, struct hpub_xparlist *xpar)
{
 int i,j,offset,temp;
 char s[LINELENGTH];
 struct hpub_extpar *xp;

 if (hd->fp==NULL) return FALSE;
 fprintf(hd->fp,"HIPS\n");
 i = 5;
 i += hpub_wnocr(hd->fp,hd->onm);	/* onm - original name */
 i += hpub_wnocr(hd->fp,hd->snm); /* snm - seq. name*/
 i += hpub_dfprintf(hd->fp,hd->numframes);
 i += hpub_wnocr(hd->fp,hd->odt); /* o. date */
 i += hpub_dfprintf(hd->fp,hd->rows);	/* orows */
 i += hpub_dfprintf(hd->fp,hd->cols);	/* ocols */
 i += hpub_dfprintf(hd->fp,hd->roi_r2-hd->roi_r1+1);   /* was:rows */
 i += hpub_dfprintf(hd->fp,hd->roi_c2-hd->roi_c1+1);   /* was:cols */
 i += hpub_dfprintf(hd->fp,hd->roi_r1);	/* frow */
 i += hpub_dfprintf(hd->fp,hd->roi_c1);	/* fcol */
 i += hpub_dfprintf(hd->fp,hd->format);
 i += hpub_dfprintf(hd->fp,hd->colors);

 if (hd->sizehistory > 0) {
	 i += hpub_dfprintf(hd->fp,hd->sizehistory);	/* szhist */
	 i += hd->sizehistory;
	 if (fwrite(hd->history,hd->sizehistory,1,hd->fp) != 1) return FALSE;
 } else {
	 temp = 13;
	 i += hpub_dfprintf(hd->fp,temp);	/* temp = szhist */
	 i += temp;
	 if (fwrite("Fake History\n",temp,1,hd->fp) != 1) return FALSE;
 }

 if (hd->sizedescription > 0) {
	 i += hpub_dfprintf(hd->fp,hd->sizedescription);	/* szdesc */
	 i += hd->sizedescription;
	 if (fwrite(hd->description,hd->sizedescription,1,hd->fp) != 1) return FALSE; 
 } else {
	 temp = 17;
	 i += hpub_dfprintf(hd->fp,temp);	/* temp = szhist */
	 i += temp;
	 if (fwrite("Fake Description\n",temp,1,hd->fp) != 1) return FALSE;
 }

 i += hpub_dfprintf(hd->fp,xpar->numparam);
 xp = xpar->params;
 offset = 0;
 while (xp != HP_NULLPAR) {
 	if (xp->count == 1) {
 		switch (xp->format) {
 		case PFASCII:	sprintf(s,"%s c 1 %d\n",xp->name,
 					(int) xp->val.v_b); break;
 		case PFBYTE:	sprintf(s,"%s b 1 %d\n",xp->name,
 					(int) xp->val.v_b); break;
 		case PFSHORT:	sprintf(s,"%s s 1 %d\n",xp->name,
 					(int) xp->val.v_s); break;
 		case PFINT:	sprintf(s,"%s i 1 %d\n",xp->name,
 					xp->val.v_i); break;
 		case PFFLOAT:	sprintf(s,"%s f 1 %f\n",xp->name,
 					xp->val.v_f); break;
 		default: 	return FALSE;	
 		}
 	}
 	else {
 		switch (xp->format) {
 		case PFASCII:	sprintf(s,"%s c %d %d\n",xp->name,
 					xp->count,offset);
 				offset += xp->count *
 					sizeof(unsigned char);
 				break;
 		case PFBYTE:	sprintf(s,"%s b %d %d\n",xp->name,
 					xp->count,offset);
 				offset += xp->count *
 					sizeof(unsigned char);
 				break;
 		case PFSHORT:	sprintf(s,"%s s %d %d\n",xp->name,
 					xp->count,offset);
 				offset += xp->count * sizeof(short);
 				break;
 		case PFINT:	sprintf(s,"%s i %d %d\n",xp->name,
 					xp->count,offset);
 				offset += xp->count * sizeof(int);
 				break;
 		case PFFLOAT:	sprintf(s,"%s f %d %d\n",xp->name,
 					xp->count,offset);
 				offset += xp->count * sizeof(float);
 				break;
 		default:	return FALSE;
 		}
 		offset = (offset+3) & (~03); /* round up to 4 bytes */
 	}
 	j = strlen(s);
 	i += j;
 	if (fwrite(s,j,1,hd->fp) != 1) return FALSE;
 	xp = xp->nextp;
 }
 sprintf(s,"%d\n",offset);	/* the size of the binary area */
 j = strlen(s);
 i += j;
 while ((i & 03) != 0) {		/* pad binary area size line with
 				   blanks so that entire header
 				   comes out to be an even multiple
 				   of 4 bytes - the binary area itself
 				   is guaranteed to be an even
 				   multiple because each individual
 				   entry is padded */
 	putc(' ',hd->fp);
 	i++;
 }
 if (fwrite(s,j,1,hd->fp) != 1) return FALSE; /* write size of binary area */
 xp = xpar->params;
 offset = 0;
 while (xp != HP_NULLPAR) {
 	if (xp->count > 1) {
 		switch (xp->format) {
 		case PFASCII:
 		case PFBYTE:	i = xp->count * sizeof(unsigned char);
 				break;
 		case PFSHORT:	i = xp->count * sizeof(short); break;
 		case PFINT:	i = xp->count * sizeof(int); break;
 		case PFFLOAT:	i = xp->count * sizeof(float); break;
 		default:	return FALSE;
 		}
 		if (fwrite(xp->val.v_pb,i,1,hd->fp) != 1) return FALSE;
 		offset += i;
 		while ((offset & 03) != 0) {
 			putc('\0',hd->fp);
 			offset++;
 		}
 	}
 	xp = xp->nextp;
 }
 return TRUE;
}

int hpub_wnocr(FILE *fp, char *s)
{
 char *t;
 int i;

 t = s;
 i = 0;
 while (*t != '\n' && *t != '\0') {
 	putc(*t++,fp);
 	i++;
 }
 putc('\n',fp);
 return(i+1);
}

int hpub_dfprintf(FILE *fp, int i)
{
 char s[30];
 int j;

 sprintf(s,"%d\n",i);
 j = strlen(s);
 if (fwrite(s,j,1,fp) != 1)
 	hpub_perr("error during header write");
 return(j);
}
