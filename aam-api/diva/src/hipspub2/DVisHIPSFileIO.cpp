/////////////////////////////////////////////////////////////////////////////
//
// Read and write HIPS image files.
// Defines and instantiates subclass of CVisHipsFileHandler class.
//
// Copyright � 1998
//
// IMM, Department of Mathematical Modelling
// Technical University of Denmark, Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk
//
// author: Lars GK & Rune Fisker, 10/9-1998
//
// Disclaimer:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on the Microsoft Vision Software Developers Kit VisSDK
//
/////////////////////////////////////////////////////////////////////////////

// This should be the first preprocessor statement in the file so that
// automatic precompiled headers work correctly.
#include "..\VisCore\VisCorePch.h"
#include "..\VisCore\VisCoreProj.h"

//#include <..\VisCore\VisCoreDLL.h>

#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

//#include <..\VisCore\VisFileIO.h>

extern "C" {
#include "hipspub2.h"
}

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif // _DEBUG



/////////////////////////////////////////////////////////////////////////////
// @class
// File handler to read and write HIPS image files.
// @base public | CVisFileHandler
// @xref <c CVisFileHandler>
//
//	author:	Lars GK & Rune Fisker, 10/9-1998
class VisCoreExport CVisHIPSFileHandler : public CVisFileHandler {
public:
	// constructor
	CVisHIPSFileHandler(void);

	// @cmember
	// Does the handler support images of the specified pixel type with
	// the specifed number of bands?
    virtual BOOL SupportsPixelType(int evispixfmt, int nbands);

	// @cmember
	// Does the specified extension match the extension of a file type
	// supported by this file handler?
    virtual BOOL MatchExtension(const char *szFiletype);

	// @cmember
 	// Attempt to read the file header.  Return TRUE is successful.
 	// (This may return FALSE or throw an exception if not successful.)
    virtual BOOL ReadHeader(SVisFileDescriptor &fd, class CVisImageBase &img);

	// @cmember
  	// Attempt to read the file body.  Return TRUE is successful.
 	// (This may return FALSE or throw an exception if not successful.)
    virtual BOOL ReadBody(SVisFileDescriptor &fd, class CVisImageBase &img);

	// @cmember
  	// Attempt to write the file header.  Return TRUE is successful.
 	// (This may return FALSE or throw an exception if not successful.)
    virtual BOOL WriteHeader(SVisFileDescriptor &fd, class CVisImageBase &img);

	// @cmember
   	// Attempt to write the file body.  Return TRUE is successful.
 	// (This may return FALSE or throw an exception if not successful.)
    virtual BOOL WriteBody(SVisFileDescriptor &fd, class CVisImageBase &img);

private:
	// @cmember Helper function to swap bytes.
	void SwapBytes(BYTE* pDest,BYTE* pSrc, int nPixelSize,int nImageSize);

	static bool s_fMakeInvisibleWhite;

	// hips struct
	hipsstruct m_Hips;
};

bool CVisHIPSFileHandler::s_fMakeInvisibleWhite = false;


/////////////////////////////////////////////////////////////////////////////
// @func Returns a handler for HIPS image file i/o.
// @rdesc File handler for i/o.
//
//	author:	Lars GK & Rune Fisker, 10/9-1998
CVisFileHandler *CVisHIPSFileHandler_Init()
{
    static CVisHIPSFileHandler HIPS_handler;
    return &HIPS_handler;
}

/////////////////////////////////////////////////////////////////////////////
// @mfunc constructor        
//
//	author:	Lars GK & Rune Fisker, 10/9-1998
CVisHIPSFileHandler::CVisHIPSFileHandler(void)
{
	m_Hips.pp = NULL;
	m_Hips.r = NULL;
	m_Hips.g = NULL;
	m_Hips.b = NULL;
	m_Hips.history = NULL;
	m_Hips.description = NULL;
}

/////////////////////////////////////////////////////////////////////////////
// @mfunc Does the handler support images of the specified pixel type with
// the specifed number of bands?
//
//	author:	Lars GK & Rune Fisker, 10/9-1998
int CVisHIPSFileHandler::SupportsPixelType(
                                          int evispixfmt,     // @parm pixel type
                                          int nbands)               // @parm number of bands
{
    return ((evispixfmt == evispixfmtGrayByte)
				|| (evispixfmt == evispixfmtGrayInt)
				|| (evispixfmt == evispixfmtGrayFloat)
				|| (evispixfmt == evispixfmtGrayDouble)
				|| (evispixfmt == evispixfmtRGBAByte))
			&& (nbands <= 1);
}

/////////////////////////////////////////////////////////////////////////////
// @mfunc Does the specified extension match the extension of a file type
// supported by this file handler?
//
//	author:	Lars GK & Rune Fisker, 10/9-1998
int CVisHIPSFileHandler::MatchExtension(
                                       const char *filetype)  // @parm Image file type (HIPS, bmp, etc.)
{
    return
        CVisFileHandler::MatchIgnoringCase(filetype, "HIPS") ||
        CVisFileHandler::MatchIgnoringCase(filetype, "HIP");
}


/////////////////////////////////////////////////////////////////////////////
// @mfunc Attempt to read the file header.  Return TRUE is successful.
//
//	author:	Lars GK & Rune Fisker, 10/9-1998
int CVisHIPSFileHandler::ReadHeader(
                                   SVisFileDescriptor &fd,  // @parm File descriptor
                                   CVisImageBase &img)          // @parm Image to be read in.
{
	SetClientName("CVisHIPSFileHandler::ReadHeader()");
/*
	#ifdef _DEBUG
		CMemoryState oldMemState, newMemState, diffMemState;
		oldMemState.Checkpoint();
	#endif
*/
    FILE *stream = fd.stream;
    
	assert(&img != 0);
	
	char lpszPathName[128];

	imm_SetHipsStruct(&m_Hips,(char *)lpszPathName,stream,TRUE);
/*
	#ifdef _DEBUG
		newMemState.Checkpoint();
		if( diffMemState.Difference( oldMemState, newMemState ) )
		{
			AfxMessageBox("Memory leaked end of readheader1!\n");
		}
	#endif
*/
	if (imm_ReadHeader(&m_Hips)==FALSE) {
		//AfxMessageBox("error reading header",MB_OK,0);
		return FALSE;
	}

	fd.cols = m_Hips.cols;
	fd.rows = m_Hips.numframes*m_Hips.rows;
	fd.frame_number = m_Hips.numframes;
	
	switch(m_Hips.format){
	case PFFLOAT:
	    fd.bits_per_pixel = 32;
	    fd.evispixfmt = evispixfmtGrayFloat;// : evispixfmtGrayByte;
		break;
	case PFDOUBLE:
	    fd.bits_per_pixel = 64;
	    fd.evispixfmt = evispixfmtGrayDouble;// : evispixfmtGrayByte;
		break;
	case PFINT:
	    fd.bits_per_pixel = 32;
	    fd.evispixfmt = evispixfmtGrayInt;// : evispixfmtGrayByte;
		break;
	case PFBYTE:
	    fd.bits_per_pixel = 8;
	    fd.evispixfmt = evispixfmtGrayByte;// : evispixfmtGrayByte;
		break;
	case PFRGB:
	    fd.bits_per_pixel = 24;
	    fd.evispixfmt = evispixfmtRGBAByte;// : evispixfmtGrayByte;
		break;

	}

	fd.bands = 1;
    fd.ascii = 0;

	if (m_Hips.history != NULL)
	{
		free(m_Hips.history);
		m_Hips.history = NULL;
	}

	if (m_Hips.description != NULL)
	{
		free(m_Hips.description);
		m_Hips.description = NULL;
	}
/*
	#ifdef _DEBUG
		newMemState.Checkpoint();
		if( diffMemState.Difference( oldMemState, newMemState ) )
		{
			AfxMessageBox("Memory leaked end of readheader!\n");
		}
	#endif
*/
    return 0;
}

/////////////////////////////////////////////////////////////////////////////
// @mfunc Attempt to read the file body.  Return TRUE is successful.
//
//	author:	Lars GK & Rune Fisker, 10/9-1998
int CVisHIPSFileHandler::ReadBody(
                                 SVisFileDescriptor &fd,    // @parm File descriptor
                                 CVisImageBase &img)            // @parm Image to be read in.
{
	SetClientName("CVisHIPSFileHandler::ReadBody()");
/*
	#ifdef _DEBUG
		CMemoryState oldMemState, newMemState, diffMemState;
		oldMemState.Checkpoint();
	#endif
*/	
    FILE *stream = fd.stream;

    // Ascii (not raw) mode not currently supported
    assert(fd.ascii == 0);
	
	if (imm_AllocHipsStructAllFrames(&m_Hips)==FALSE) {
		//AfxMessageBox("error allocating memory",MB_OK,0);
		return FALSE;
	}
	if (imm_ReadAllFrames(&m_Hips)==FALSE) {
		//AfxMessageBox("error reading image data",MB_OK,0);
		return FALSE;
	}

	if ((m_Hips.format==PFDOUBLE) || (m_Hips.format==PFFLOAT) || (m_Hips.format==PFINT)){

		BYTE *pDest = (BYTE *) img.PbPixel(img.Left(), img.Top(), 0);
		BYTE *pSrc = (BYTE*) m_Hips.pp;
		
		SwapBytes(pDest,pSrc,fd.bits_per_pixel/8,img.NPixels());
	}
	else if (m_Hips.format==PFBYTE) {
		BYTE *pDest = (BYTE *) img.PbPixel(img.Left(), img.Top(), 0);
		BYTE *pSrc = (BYTE*) m_Hips.pp;
		
		memcpy(pDest,pSrc,img.NPixels());
	}
	else if(m_Hips.format==PFRGB){
		BYTE *p = (BYTE *) img.PbPixel(img.Left(), img.Top(), 0);
		CVisRGBABytePixel *q = (CVisRGBABytePixel *) p;
		int r,c,i=0;
	    int Height = img.Height(), Width = img.Width();
		DWORD ppOffset = 0;

		for(r=0;r<Height;r++)
			for(c=0;c<Width;c++){
				q[i].SetR((BYTE) m_Hips.pp[ppOffset]);
				q[i].SetG((BYTE) m_Hips.pp[ppOffset+1]);
				q[i].SetB((BYTE) m_Hips.pp[ppOffset+2]);
				q[i].SetA(255);
				ppOffset+=3;
				i++;
			}
	}

	if(m_Hips.pp != NULL){
		free(m_Hips.pp);
		m_Hips.pp = NULL;
	}
	if(m_Hips.r != NULL){
		free(m_Hips.r);
		m_Hips.r = NULL;
	}
	if(m_Hips.g != NULL){
		free(m_Hips.g);
		m_Hips.g = NULL;
	}
	if(m_Hips.b != NULL){
		free(m_Hips.b);
		m_Hips.b = NULL;
	}

    return 0;
}

/////////////////////////////////////////////////////////////////////////////
// @mfunc Attempt to write the file header.  Return TRUE is successful.
//
//	author:	Lars GK & Rune Fisker, 10/9-1998
int CVisHIPSFileHandler::WriteHeader(
                                    SVisFileDescriptor &fd,     // @parm File descriptor
                                    CVisImageBase &img)             // @parm Image to be written out.
{
	FILE *stream = fd.stream;

	if (fd.frame_number == 0)
		fd.frame_number = 1;

	m_Hips.rows = img.Height()/fd.frame_number;
	m_Hips.cols = img.Width();
	
	m_Hips.fp = fd.stream;
	switch(img.PixFmt()){
        case evispixfmtFloat:
        case evispixfmtGrayFloat:
			m_Hips.format = PFFLOAT;
			m_Hips.nsize=m_Hips.rows*m_Hips.cols*4;
			m_Hips.sizepix=4;
			break;
        case evispixfmtDouble:
        case evispixfmtGrayDouble:
			m_Hips.format = PFDOUBLE;
			m_Hips.nsize=m_Hips.rows*m_Hips.cols*8;
			m_Hips.sizepix=8;
			break;
        case evispixfmtInt:
        case evispixfmtGrayInt:
			m_Hips.format = PFINT;
			m_Hips.nsize=m_Hips.rows*m_Hips.cols*4;
			m_Hips.sizepix=2;
			break;
        case evispixfmtGrayByte:
			m_Hips.format = PFBYTE;
			m_Hips.nsize=m_Hips.rows*m_Hips.cols;
			m_Hips.sizepix=1;
			break;
        case evispixfmtRGBAByte:
			m_Hips.format = PFRGB;
			m_Hips.nsize=m_Hips.rows*m_Hips.cols;
			m_Hips.sizepix=3;
			break;
		default:
			return false;
	}
	m_Hips.nsize=m_Hips.rows*m_Hips.cols;
	m_Hips.colors=256;
	m_Hips.ncmap=FALSE;
	m_Hips.numframes=fd.frame_number;
	m_Hips.ndepth=1;
	m_Hips.nvar=1;
	m_Hips.ntime=1;

	m_Hips.roi_c1 = 0;
	m_Hips.roi_c2 = m_Hips.cols-1;
	m_Hips.roi_r1 = 0;
	m_Hips.roi_r2 = m_Hips.rows-1;
	m_Hips.roi_z1 = 0;
	m_Hips.roi_z2 = m_Hips.numframes-1;

	m_Hips.sizehistory = 0;
	m_Hips.sizedescription = 0;

	imm_SetHipsStruct(&m_Hips,(char *)fd.filename/*(char *)lpszPathName*/,stream,TRUE);
	if (imm_WriteHeader(&m_Hips)==false) {
		//AfxMessageBox("error writing header",MB_OK,0);
		return false;
	}
	
    return 0;
}


/////////////////////////////////////////////////////////////////////////////
// @mfunc Attempt to write the file body.  Return TRUE is successful.
//
//	author:	Lars GK & Rune Fisker, 10/9-1998
int CVisHIPSFileHandler::WriteBody(
                                  SVisFileDescriptor &fd,   // @parm File descriptor
                                  CVisImageBase &img)           // @parm Image to be written out.
{
	SetClientName("CVisHIPSFileHandler::WriteBody()");

    FILE *stream = fd.stream;
	m_Hips.fp = stream;

	int Height = img.Height(), Width = img.Width(), r, c, i;

	if (imm_AllocHipsStructAllFrames(&m_Hips)==FALSE) {
		//AfxMessageBox("error in memory allocation",MB_OK,0);
		return false;
	}

	if ((m_Hips.format==PFDOUBLE) || (m_Hips.format==PFFLOAT) || (m_Hips.format==PFINT))
	{
		BYTE *pSrc = (BYTE *) img.PbPixel(img.Left(), img.Top(), 0);
		BYTE *pDest = (BYTE*) m_Hips.pp;

		int nPixelSize; 
		
		if ((m_Hips.format==PFFLOAT) || (m_Hips.format==PFINT))
			nPixelSize = 4; 
		else if (m_Hips.format==PFDOUBLE)
			nPixelSize = 8;
		else 
			return false;

		SwapBytes(pDest,pSrc,nPixelSize,img.NPixels());
	}
	else if (m_Hips.format==PFBYTE) 
	{
		BYTE *pSrc = (BYTE *) img.PbPixel(img.Left(), img.Top(), 0);
		BYTE *pDest = (BYTE*) m_Hips.pp;
		
		memcpy(pDest,pSrc,img.NPixels());
	}
    else if(m_Hips.format==PFRGB){
		BYTE *p = (BYTE *) img.PbPixel(img.Left(), img.Top(), 0);
		CVisRGBABytePixel *q = (CVisRGBABytePixel *) p;
		i=0;
		DWORD ppOffset = 0;
		for(r=0;r<Height;r++)
			for(c=0;c<Width;c++){
				m_Hips.pp[ppOffset] = (BYTE)q[i].R();
				m_Hips.pp[ppOffset+1] = (BYTE)q[i].G();
				m_Hips.pp[ppOffset+2] = (BYTE)q[i].B();
				ppOffset+=3;
				i++;
			}
	}
	else
		return false;
		
	if (imm_WriteAllFrames(&m_Hips)==FALSE) {
		//AfxMessageBox("error writing image data",MB_OK,0);
		return false;
	}

	fclose(m_Hips.fp);

	if(m_Hips.pp != NULL){
		free(m_Hips.pp);
		m_Hips.pp = NULL;
	}
	if(m_Hips.r != NULL){
		free(m_Hips.r);
		m_Hips.r = NULL;
	}
	if(m_Hips.g != NULL){
		free(m_Hips.g);
		m_Hips.g = NULL;
	}
	if(m_Hips.b != NULL){
		free(m_Hips.b);
		m_Hips.b = NULL;
	}

	return 0;
}

/////////////////////////////////////////////////////////////////////////////
// @mfunc Helper function to swap bytes.
//
//	author:	Lars GK & Rune Fisker, 10/9-1998
void CVisHIPSFileHandler::SwapBytes(BYTE* pDest,BYTE* pSrc, int nPixelSize,int nImageSize)
{
	int i;

	pSrc += nPixelSize-1;		

	for(int j=0;j<nImageSize;j++){
		for (i=0;i<nPixelSize;i++)
		{
			*pDest++ = *pSrc--;
		}
		pSrc += 2*nPixelSize;
	}
}
