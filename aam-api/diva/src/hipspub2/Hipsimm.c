/********************************************************
	immhips.c                                                 
                                                         
	A part of the hipspub2 library 
	(TOOLS FOR THE HIPS WIZARD)
	Writer: Jorgen Folm-Hansen
	Date: dec. 11th 1997 

	Improved april/may 1998
********************************************************/

#include "hipspub2.h"

byte *imm_AllocMem(int size)
{
  return (byte *)calloc(size,1);
}

int imm_GetByteSizeOf(int format)
{
	if (format==PFBYTE) return sizeof(byte);
	if (format==PFSBYTE) return sizeof(signed char);
	if (format==PFINT) return sizeof(int);
	if (format==PFUINT) return sizeof(unsigned int);
	if (format==PFSHORT) return sizeof(short);
	if (format==PFUSHORT) return sizeof(unsigned short);
	if (format==PFFLOAT) return sizeof(float);
	if (format==PFDOUBLE) return sizeof(double);
	if (format==PFASCII) return sizeof(double);
	if (format==PFCOMPLEX) return 2*sizeof(float);
	if (format==PFDBLCOM) return 2*sizeof(double);
	if (format==PFRGB) return 3*sizeof(byte);
	if (format==PFRGBZ) return 4*sizeof(byte);
	if (format==PFRGBFLT) return 3*sizeof(float);
	return 0;
}


void imm_ResetHipsStruct(hipsstruct *hd)
{
	hd->fp = NULL;
	hd->pp = NULL;
	hd->flag = FALSE;
	hd->format = PFBYTE;
	hd->rows = 0;
	hd->cols = 0;
	hd->nsize = 0;
	hd->sizepix = 0;
	hd->numframes = 0;
	hd->colors = 1;
	hd->ndepth = 1;
	hd->ntime = 1;
	hd->nvar = 1;

	hd->roi_c1 = 0;
	hd->roi_c2 = 0;
	hd->roi_r1 = 0;
	hd->roi_r2 = 0;
	hd->roi_z1 = 0;
	hd->roi_z2 = 0;

	hd->ncmap = 0;
	hd->r = NULL;
	hd->g = NULL;
	hd->b = NULL;
	hd->history=NULL;
	hd->description=NULL;
	hd->sizehistory=0;
	hd->sizedescription=0;
}


void imm_CopyHipsStruct(hipsstruct *hd1, hipsstruct *hd2)
{
  hd1->format = hd2->format;
  hd1->rows = hd2->rows;
  hd1->cols = hd2->cols;
  hd1->numframes = hd2->numframes;
  hd1->colors = hd2->colors;
  hd1->ndepth = hd2->ndepth;
  hd1->ntime = hd2->ntime;
  hd1->nvar = hd2->nvar;
  hd1->nsize = hd2->nsize;
  hd1->sizepix = hd2->sizepix;
  hd1->ncmap = hd2->ncmap;
  memcpy(hd1->name,hd2->name,256);
  if(hd1->ncmap>0){
	  hd1->r = malloc(hd1->ncmap);
	  memcpy(hd1->r,hd2->r,hd1->ncmap);
	  hd1->g = malloc(hd1->ncmap);
	  memcpy(hd1->g,hd2->g,hd1->ncmap);
	  hd1->b = malloc(hd1->ncmap);
	  memcpy(hd1->b,hd2->b,hd1->ncmap);
  } else {
	  hd1->r = NULL;
	  hd1->g = NULL;
	  hd1->b = NULL;
  }

  hd1->roi_c1 = hd2->roi_c1;
  hd1->roi_c2 = hd2->roi_c2;
  hd1->roi_r1 = hd2->roi_r1;
  hd1->roi_r2 = hd2->roi_r2;
  hd1->roi_z1 = hd2->roi_z1;
  hd1->roi_z2 = hd2->roi_z2;

  memcpy(hd1->onm,hd2->onm,LINELENGTH);
  memcpy(hd1->snm,hd2->snm,LINELENGTH);
  memcpy(hd1->odt,hd2->odt,LINELENGTH);

  hd1->sizehistory = hd2->sizehistory;
  if (hd1->sizehistory>0) {
    hd1->history = (char *)malloc(hd1->sizehistory);
    memcpy(hd1->history,hd2->history,hd2->sizehistory);
  } else hd1->history = NULL;

  hd1->sizedescription = hd2->sizedescription;
  if (hd1->sizedescription > 0) {
    hd1->description = (char *)malloc(hd1->sizedescription);
	memcpy(hd1->description,hd2->description,hd2->sizedescription);
  } else hd1->description=NULL;
}

void imm_SetHipsStruct(hipsstruct *hd, char *name, FILE *fp, int flag)
{
  hd->fp=fp;
  strcpy(hd->name,name);
  hd->flag=flag;
}


void imm_SetNewHistory(hipsstruct *hd, char *History)
{
	hd->sizehistory = strlen(History);
	if (hd->sizehistory>0) {
		if (hd->history!=NULL) free(hd->history);
		hd->history = calloc(hd->sizehistory+1,1);
		memcpy(hd->history, History, hd->sizehistory);
	} else hd->history=NULL;
}


void imm_AddToHistory(hipsstruct *hd, char *History)
{
	char *oldhistory;

	/* Make copy of old history */
	oldhistory = (char *)malloc(hd->sizehistory);
	memcpy(oldhistory, hd->history, hd->sizehistory);

	/* Make new history */
	if (hd->history!=NULL) free(hd->history);
	hd->history = calloc(strlen(History)+hd->sizehistory+1,1);

	/* copy new history into memory */
	memcpy(hd->history, oldhistory, hd->sizehistory);
	memcpy(hd->history+hd->sizehistory, History, strlen(History));
	hd->sizehistory += strlen(History);

	/* delete temporary memory */
	free(oldhistory);
}


void imm_SetNewDescription(hipsstruct *hd, char *Description)
{
	hd->sizedescription = strlen(Description);
	if (hd->sizedescription>0) {
		if (hd->description!=NULL) free(hd->description);
		hd->description = calloc(hd->sizedescription+1,1);
		memcpy(hd->description, Description, hd->sizedescription);
	} else hd->description=NULL;
}



/*int imm_ReadHeader(hipsstruct *hd)
{ int r;
  if (hd->fp==NULL) return FALSE;
  hd->ncmap=0;
  if (hd->r==NULL || hd->g==NULL || hd->b==NULL) {
    r=hpub_frdhdr(hd->fp,&hd->format,&hd->rows,&hd->cols,&hd->numframes,&hd->colors);
  } else {
    r=hpub_frdhdrc(hd->fp,&hd->format,&hd->rows,&hd->cols,&hd->numframes,&hd->colors,&hd->ncmap,&hd->r,&hd->g,&hd->b);
  }
  hd->nsize = hd->rows*hd->cols;
  hd->sizepix = imm_GetByteSizeOf(hd->format);
  hd->ndepth = 1;
  hd->ntime = 1;
  hd->nvar = 1;
  hd->roi_c1 = 0;
  hd->roi_c2 = hd->cols-1;
  hd->roi_r1 = 0;
  hd->roi_r2 = hd->rows-1;
  hd->roi_z1 = 0;
  hd->roi_z2 = hd->numframes-1;
  return r;
}*/
int imm_ReadHeader(hipsstruct *hd)
{ int r;
  if (hd->fp==NULL) return FALSE;
  hd->ncmap=0;
  if (hd->r==NULL || hd->g==NULL || hd->b==NULL) {
    r=hpub_frdhdr(hd);
  } else {
    r=hpub_frdhdrc(hd);
  }

  hd->nsize = hd->rows*hd->cols;
  hd->sizepix = imm_GetByteSizeOf(hd->format);
  hd->ndepth = 1;
  hd->ntime = 1;
  hd->nvar = 1;
/*hd->roi_c1 = 0;
  hd->roi_c2 = hd->cols-1;
  hd->roi_r1 = 0;
  hd->roi_r2 = hd->rows-1;
  hd->roi_z1 = 0;
  hd->roi_z2 = hd->ndepth-1;*/
  hd->roi_z1 = 0;
  hd->roi_z2 = hd->numframes-1;

  return r;
}

int imm_AllocHipsStruct(hipsstruct *hd)
{ 
  if (hd->nsize<1) return FALSE;
      
  if (hd->ndepth==1) {
    hd->pp = imm_AllocMem(hd->nsize*imm_GetByteSizeOf(hd->format));
  } else {
    hd->pp = imm_AllocMem(hd->ndepth*hd->nsize*imm_GetByteSizeOf(hd->format));
  }

  if (hd->pp==NULL) return FALSE;
  return TRUE;
}

int imm_AllocHipsStructAllFrames(hipsstruct *hd)
{ 
  if (hd->nsize<1) return FALSE;
      
  hd->pp = imm_AllocMem(hd->numframes*hd->nsize*imm_GetByteSizeOf(hd->format));

  if (hd->pp==NULL) return FALSE;
  return TRUE;
}

int imm_WriteHeader(hipsstruct *hd)
{ int r;
  if (hd->fp==NULL) return FALSE;
  if (hd->ncmap<=0) {
    r=hpub_fwrthdr(hd);
  } else {
    r=hpub_fwrthdrc(hd);
  }
  return r;
}

int imm_ReadNextFrame(hipsstruct *hd)
{ unsigned int totsize=hd->nsize*hd->ndepth;
  if (hd->fp==NULL) return FALSE;
  if (hd->pp==NULL) return FALSE;
  if (fread(hd->pp,hd->sizepix,(size_t)totsize,hd->fp)!=totsize) {
    return FALSE;
  }
  return TRUE;
}

int imm_ReadAllFrames(hipsstruct *hd)
{ unsigned int totsize=hd->nsize*hd->numframes;
  if (hd->fp==NULL) return FALSE;
  if (hd->pp==NULL) return FALSE;
  if (fread(hd->pp,hd->sizepix,(size_t)totsize,hd->fp)!=totsize) {
    return FALSE;
  }
  return TRUE;
}

double imm_GetPixelValue(hipsstruct *hd, int x, int y)
{
  signed char *psc;
  float *pf;
  double *pd;
  short *ps;
  unsigned short *pus;
  long *pi;
  unsigned long *pui;
  unsigned char *pp;

  if (hd->pp==NULL) return 0.0;
  if (y>=hd->rows) y=hd->rows-1; else if (y<0) y=0;
  if (x>=hd->cols) x=hd->cols-1; else if (x<0) x=0;

  switch (hd->format) {
    case PFBYTE :
      return (double) ((byte) *(hd->pp+x+y*hd->cols));
    case PFRGB :
      pp = hd->pp + 3*(x+y*hd->cols);
      return (double) (*pp + *(pp+1) + *(pp+2)) / 3.0;
    case PFRGBZ :
      pp = hd->pp + 4*(x+y*hd->cols);
      return (double) (*pp + *(pp+1) + *(pp+2)) / 3.0;
    case PFRGBFLT :
      pf = (float *)(hd->pp+3*(x+y*hd->cols)*sizeof(float));
      return (double) (*pf + *(pf+1) + *(pf+2)) / 3.0;
    case PFSBYTE :
	  psc = (signed char *)(hd->pp+x+y*hd->cols);
      return (double) (*psc);
    case PFSHORT :
	  ps = (short *)(hd->pp+(x+y*hd->cols)*sizeof(short));
      return (double) (*ps);
    case PFUSHORT :
	  pus = (unsigned short *)(hd->pp+(x+y*hd->cols)*sizeof(unsigned short));
      return (double) (*pus);
    case PFINT :
	  pi = (long *)(hd->pp+(x+y*hd->cols)*sizeof(long));
      return (double) (*pi);
    case PFUINT :
 	  pui = (unsigned long *)(hd->pp+(x+y*hd->cols)*sizeof(unsigned long));
      return (double) (*pui);
    case PFFLOAT :
	  pf = (float *)(hd->pp+(x+y*hd->cols)*sizeof(float));
      return (double) (*pf);
    case PFDOUBLE :
 	  pd = (double *)(hd->pp+(x+y*hd->cols)*sizeof(double));
      return (double) (*pd);
    case PFCOMPLEX :
	  pf = (float *)(hd->pp+(x+y*hd->cols)*2*sizeof(float));
      return (double) (*pf);
    default :
      return 0.0;
  }
}


void imm_GetPixelVector(hipsstruct *hd, int x, int y, double *vector)
{
  signed char *psc;
  float *pf;
  double *pd;
  short *ps;
  unsigned short *pus;
  long *pi;
  unsigned long *pui;

  if (hd->pp==NULL) return;
  if (y>=hd->rows) y=hd->rows-1; else if (y<0) y=0;
  if (x>=hd->cols) x=hd->cols-1; else if (x<0) x=0;

  switch (hd->format) {
    case PFBYTE :
      vector[0] = (double) ((byte) *(hd->pp+x+y*hd->cols));
	  break;
    case PFSBYTE :
	  psc = (signed char *)(hd->pp+x+y*hd->cols);
      vector[0] = (double) (*psc);
	  break;
    case PFSHORT :
	  ps = (short *)(hd->pp+(x+y*hd->cols)*sizeof(short));
      vector[0] = (double) (*ps);
	  break;
    case PFUSHORT :
	  pus = (unsigned short *)(hd->pp+(x+y*hd->cols)*sizeof(unsigned short));
      vector[0] = (double) (*pus);
	  break;
    case PFINT :
	  pi = (long *)(hd->pp+(x+y*hd->cols)*sizeof(long));
      vector[0] = (double) (*pi);
	  break;
    case PFUINT :
 	  pui = (unsigned long *)(hd->pp+(x+y*hd->cols)*sizeof(unsigned long));
      vector[0] = (double) (*pui);
	  break;
    case PFFLOAT :
	  pf = (float *)(hd->pp+(x+y*hd->cols)*sizeof(float));
      vector[0] = (double) (*pf);
	  break;
    case PFDOUBLE :
 	  pd = (double *)(hd->pp+(x+y*hd->cols)*sizeof(double));
      vector[0] = (double) (*pd);
	  break;
    case PFCOMPLEX :
	  pf = (float *)(hd->pp+(x+y*hd->cols)*2*sizeof(float));
      vector[0] = (double) pf[0];
	  vector[1] = (double) pf[1];
	  break;
    case PFRGB :
      vector[0] = (double) ((byte) *(hd->pp+3*x + 3*y*hd->cols));
      vector[1] = (double) ((byte) *(hd->pp+3*x+1+3*y*hd->cols));
      vector[2] = (double) ((byte) *(hd->pp+3*x+2+3*y*hd->cols));
	  break;
    case PFRGBZ :
      vector[0] = (double) ((byte) *(hd->pp+4*x + 4*y*hd->cols));
      vector[1] = (double) ((byte) *(hd->pp+4*x+1+4*y*hd->cols));
      vector[2] = (double) ((byte) *(hd->pp+4*x+2+4*y*hd->cols));
      vector[3] = (double) ((byte) *(hd->pp+4*x+3+4*y*hd->cols));
	  break;
    case PFRGBFLT :
	  pf = (float *)(hd->pp+3*(x+y*hd->cols)*sizeof(float));
      vector[0] = (double) *pf;
      vector[1] = (double) *(pf+1);
      vector[2] = (double) *(pf+2);
	  break;
    default :
      vector[0] = 0.0;
	  break;
  }
  return;
}


int imm_SetPixelValue(hipsstruct *hd, int *x, int *y, double *val)
{
  byte *pb;
  signed char *psc;
  float *pf;
  double *pd;
  short *ps;
  unsigned short *pus;
  long *pi;
  unsigned long *pui;

  if (hd->pp==NULL) return FALSE;
  if (*y>=hd->rows || *y<0) return FALSE;
  if (*x>=hd->cols || *x<0) return FALSE;

  switch (hd->format) {
    case PFBYTE :
      pb = hd->pp+(*x + *y * hd->cols);
      *pb = (byte) *val;
	  break;
    case PFSBYTE :
      psc = (signed char *)(hd->pp+(*x + *y * hd->cols));
      *psc = (signed char) *val;
	  break;
    case PFRGB :
      pb = hd->pp+(*x + *y * hd->cols);
      *pb = (byte) val[0];  
	  pb++;
      *pb = (byte) val[1]; 
	  pb++;
      *pb = (byte) val[2];
	  break;
    case PFRGBZ :
      pb = hd->pp+(*x + *y * hd->cols);
      *pb = (byte) val[0];  
	  pb++;
      *pb = (byte) val[1]; 
	  pb++;
      *pb = (byte) val[2];
	  pb++;
      *pb = (byte) 0;
	  break;
    case PFRGBFLT :
	  pf = (float *)(hd->pp+3*(*x + *y * hd->cols)*sizeof(float));
      *pf = (float) val[0];  
	  pf++;
      *pf = (float) val[1]; 
	  pf++;
      *pf = (float) val[2];
	  break;
    case PFSHORT :
	  ps = (short *)(hd->pp+(*x + *y * hd->cols)*sizeof(short));
      *ps = (short) *val;
	  break;
    case PFUSHORT :
	  pus = (unsigned short *)(hd->pp+(*x + *y * hd->cols)*sizeof(unsigned short));
      *pus = (unsigned short) *val;
	  break;
    case PFINT :
	  pi = (long *)(hd->pp+(*x + *y * hd->cols)*sizeof(long));
      *pi = (long) *val;
      break;
    case PFUINT :
      pui = (unsigned long *)(hd->pp+(*x + *y * hd->cols)*sizeof(unsigned long));
      *pui = (unsigned long) *val;
	  break;
    case PFFLOAT :
      pf = (float *)(hd->pp+(*x + *y * hd->cols)*sizeof(float));
      *pf = (float) *val;
	  break;
    case PFDOUBLE :
      pd = (double *)(hd->pp+(*x + *y * hd->cols)*sizeof(double));
      *pd = (double) *val;
	  break;
    default :
      return FALSE;
  }
  return TRUE;
}

int imm_WriteFrame(hipsstruct *hd)
{
  if (hd->fp==NULL) return FALSE;
  if (hd->pp==NULL) return FALSE;
  if (fwrite(hd->pp,imm_GetByteSizeOf(hd->format),hd->nsize,hd->fp)!=(unsigned int)hd->nsize) {
    return FALSE;
  }
  return TRUE;
}

int imm_WriteAllFrames(hipsstruct *hd)
{ unsigned int totsize=hd->numframes*hd->nsize;
  if (hd->fp==NULL) return FALSE;
  if (hd->pp==NULL) return FALSE;
  if (fwrite(hd->pp,imm_GetByteSizeOf(hd->format),(size_t)totsize,hd->fp)!=totsize) {
    return FALSE;
  }
  return TRUE;
}

void imm_CloseHipsStruct(hipsstruct *hd)
{
	if (hd->flag && hd->fp!=NULL) fclose(hd->fp);
	if (hd->pp!=NULL) free(hd->pp);
	if (hd->ncmap>0) {
		if (hd->r!=NULL) free(hd->r);
		if (hd->g!=NULL) free(hd->g);
		if (hd->b!=NULL) free(hd->b);
	}
	if (hd->history!=NULL && hd->sizehistory>0) free(hd->history);
	if (hd->description!=NULL && hd->sizedescription>0) free(hd->description);
	hd->sizehistory=0;
	hd->history=NULL;
	hd->sizedescription=0;
	hd->description=NULL;
}

