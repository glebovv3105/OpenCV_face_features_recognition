/////////////////////////////////////////////////////////////////////////////
//
// Image class for personale functions
//
// Copyright � 1998
//
// IMM, Department of Mathematical Modelling
// Technical University of Denmark, Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk
//
// author: Rune Fisker 
//
// Disclaimer:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on the Microsoft Vision Software Developers Kit VisSDK
//
/////////////////////////////////////////////////////////////////////////////

#include <Afx.h>
#include "DImageMyClass.h"
/*
CDImageMyClass<byte>;
CDImageMyClass<int>;
CDImageMyClass<short>;
CDImageMyClass<unsigned short>;
CDImageMyClass<float>;
CDImageMyClass<double>;
*/

template <class TPixel>
void CDImageMyClass<TPixel>::Combine3Channels(CDImageMyClass<unsigned char>& imageR, CDImageMyClass<unsigned char>& imageG, CDImageMyClass<unsigned char>& imageB)
{

	assert(false);
/*
	// only works for RGB byte
	if (PixFmt() != evispixfmtRGBAByte)
	{
		throw CVisError("This function only works for RGB byte images",edviserrorCancelByUser,"OnToolsCoversion3channelsrgb",__FILE__, __LINE__);
	}

	// reallocate memory
	Allocate(imageR.Width(),imageR.Height());

	// insert pixels
	int yRGB=0;

	for (int y=imageR.Top(); y < imageR.Bottom(); y++,yRGB++)
	{
		int xRGB=0;

		for (int x=imageR.Left(); x < imageR.Right(); x++,xRGB++)
		{
			Pixel(xRGB,yRGB).SetR(imageR.Pixel(x,y));
			Pixel(xRGB,yRGB).SetG(imageG.Pixel(x,y));
			Pixel(xRGB,yRGB).SetB(imageB.Pixel(x,y));
			Pixel(xRGB,yRGB).SetA(0);
		}
	}
*/
}

////////////////////////////////////////////////////////////////////////////
//
//	Extract the contour of an object using 8 neighbourhood. The object is 
//	defined as sammenhaengned gruop of pixels with the value dLabel
//
//	input
//		dLabel: the label/pixel value of the object to extract the contour from
//	output:
//		vX,Vy: (x,y) coordinates
//
//	Author:	Rune Fisker, 22/9-1999
template <class TPixel>
void CDImageMyClass<TPixel>::ExtractContour(CDVector& vX, CDVector& vY, const double dLabel)
{
	vX.Resize(1000);
	vY.Resize(vX.Length());

	bool fNotFound = true;

	// find first point on the object
	TPixel tLabel = dLabel; // now RGB is able to used it as well
	int x,y;
	for (x=Left();x<Right() && fNotFound;x++)
	{
		for (y=Top();y<Bottom() && fNotFound;y++)
		{
			if (Pixel(x,y) == tLabel)
			{
				fNotFound = false;
			}
		}
	}

	// correct (x,y)
	x--;
	y--;

	if (fNotFound)
		throw CVisError("No object with that label",eviserrorUnknown,"ExtractContour",__FILE__, __LINE__);

	// define neighbourhood
//	static signed char pX4[] = { 1, 0, -1 ,0, 1}; // the last is equal to the first 
//	static signed char pY4[] = { 0, 1, 0 ,-1, 0};

	static signed char pX8[] = { 1, 1, 0, -1, -1, -1,  0,  1, 1}; // the last is equal to the first 
	static signed char pY8[] = { 0, 1, 1,  1,  0, -1, -1, -1, 0};

	// set neighbourhood
	int nNeighbours;
	signed char* pXDirect;
	signed char* pYDirect;


//	if (fNeighbourhood8)
//	{
		nNeighbours = 8;

		pXDirect = pX8;
		pYDirect = pY8;
/*	}
	else
	{
		nNeighbours = 4;

		pXDirect = pX4;
		pYDirect = pY4;
	}
*/
	// trace contour
	int xStart=x;
	int yStart=y;
	int nPt = 0, xLastLast, yLastLast;

	do
	{
		// copy and resize vectors if needed
		if (nPt >= vX.Length())
		{
			CDVector vTmp(vX.Length());

			vTmp = vX;
			vX.Resize(2*vTmp.Length());
			memcpy(&vX[0],&vTmp[0],sizeof(double)*vTmp.Length());

			vTmp = vY;
			vY.Resize(2*vTmp.Length());
			memcpy(&vY[0],&vTmp[0],sizeof(double)*vTmp.Length());
		}
		

		// save coordinates
		vX[nPt] = x;
		vY[nPt] = y;

		if (nPt == 0)
		{
			xLastLast = -1;
			yLastLast = -1;
		}
		else
		{
			xLastLast = vX[nPt-1];
			yLastLast = vY[nPt-1];
		}

		// find next point on contour
		int i = 0, iDirectFrom = -1;
		
		int xNext = max( min(vX[nPt] + pXDirect[i],Right()), Left());
		int yNext = max( min(vY[nPt] + pYDirect[i],Bottom()), Top());

		do
		{
			do
			{
				// update x and y
				x = xNext;
				y = yNext;
				
				// calc. next (x,y)
				i++;
				xNext = max( min(vX[nPt] + pXDirect[i],Right()), Left());
				yNext = max( min(vY[nPt] + pYDirect[i],Bottom()), Top());
				
//				assert(i <= nNeighbours);
			}
			while (Pixel(x,y) == Pixel(xNext,yNext));
			
			// chose the right pixel
			if (Pixel(x,y) != tLabel)
			{
				x = xNext;
				y = yNext;
			}

		}
		while (((xLastLast == x) && (yLastLast == y))); // make sure it is not the same at salt time

		nPt++;		
	}
	while (!( ( x == xStart) && ( y == yStart) ) );

	// copy and resize vectors
	CDVector vTmp(vX.Length());
	
	vTmp = vX;
	vX.Resize(nPt);
	memcpy(&vX[0],&vTmp[0],sizeof(double)*nPt);
	
	vTmp = vY;
	vY.Resize(nPt);
	memcpy(&vY[0],&vTmp[0],sizeof(double)*nPt);

}

template <class TPixel>
void CDImageMyClass<TPixel>::FullPathNoExt(CString& strOut)
{	
	// make new name
	strOut = Name(true);
	
	int iPos = 0;
	
	while (strOut[iPos] != '.')
		iPos++;
	
	strOut.Delete( iPos, strOut.GetLength() - iPos );
}

template <class TPixel>
void CDImageMyClass<TPixel>::Shift(const int x, const int y, bool fCyclic)
{
	CRect rROI,rShift,rMemory,rROIOld;

	if (!fCyclic)
	{
		// get ROI
		rShift = Rect();
		rROIOld = Rect();

		// shift it 
		rShift.OffsetRect(x,y);

		// limit shift rect to inside image
		rMemory = MemoryRect();

		if (!rROI.IntersectRect(rShift,rMemory))
		{
			throw CVisError("Non the shifted area is within the image",edviserrorCancelByUser,"Shift",__FILE__, __LINE__);
		}

		rShift = rROI;

		// get corresponding ROI
		rROI.OffsetRect(-x,-y);

		// create tmp image
		CDImageMyClass<TPixel> imageTmp(rROI.Width(),rROI.Height());

		// copy relevant pixels to tmp
		SetRect(rROI);
		CopyPixelsTo(imageTmp,evisnormalizeCopyBytesSameType);

		// copy pixels back into the shifted area
		SetRect(rShift);
		imageTmp.CopyPixelsTo(*this,evisnormalizeCopyBytesSameType);

		// set old ROI back
		SetRect(rROIOld);
	}
	else
	{
		// create tmp image
		CDImageMyClass<TPixel> imageTmp(Width(),Height());
		CopyPixelsTo(imageTmp,evisnormalizeCopyBytesSameType);

		int yOut,xOut;

		int yPositive = y + Height();
		int xPositive = x + Width();

		for (int yTmp=imageTmp.Top();yTmp<imageTmp.Bottom();yTmp++)
		{
			yOut = ( (yPositive + yTmp) % Height() ) + Top();
			for (int xTmp=imageTmp.Left();xTmp<imageTmp.Right();xTmp++)
			{
				xOut = ( (xPositive + xTmp) % Width() ) + Left();
				Pixel(xOut,yOut) = imageTmp.Pixel(xTmp,yTmp);
			}
		}
	}

	// add to history
	CString str;
	if (fCyclic)
	{
		str.Format("Image Shifted (%i,%i) ",x,y);
	}
	else
	{
		str.Format("Image Shifted (%i,%i)",x,y);
	}

	AddToHistory(str);
}

template <class TPixel>
void CDImageMyClass<TPixel>::Resize(const int nWidth, const int nHeight)
{
	// create image ROI to move
	CRect rROI = Rect();
	rROI.right = rROI.left + min(nWidth,Width());
	rROI.bottom = rROI.top + min(nHeight,Height());

	SetRect(rROI);

	// create tmp image
	CDImageMyClass<TPixel> imageTmp(Width(),Height());
	CopyPixelsTo(imageTmp,evisnormalizeCopyBytesSameType);

	// reallocate
	Allocate(nWidth,nHeight);
	
	// fill zeros if needed
	if ((Width() < nWidth) || (Height() < nHeight))
		FillPixels(0);

	// copy pixels back
	SetRect(0,0,rROI.Width(),rROI.Height());
	imageTmp.CopyPixelsTo(*this,evisnormalizeCopyBytesSameType);

	ClearRect();
}

template <class TPixel>
void CDImageMyClass<TPixel>::FillInside(const double dVal, int iXSeed, int iYSeed)
{
	FillRow(dVal, iXSeed, iXSeed, iYSeed);
}

template <class TPixel>
void CDImageMyClass<TPixel>::FillRow(const double dVal, int iXSeed1, int iXSeed2, int iYSeed1)
{
	int xf1 = iXSeed1;
	int xf2 = iXSeed2;

	if (Pixel(xf1,iYSeed1) != dVal)
	{
		while (xf1>0 && Pixel(xf1-1,iYSeed1) != dVal) xf1--;
	}
	
	if (Pixel(xf2,iYSeed1) != dVal)
	{
		while (xf2<Right()-1 && Pixel(xf2+1,iYSeed1) != dVal) xf2++;
	}
	
	TPixel* ppixel = &Pixel(xf1,iYSeed1);
	int fill_on = (*ppixel != dVal);

	int ist=xf1;
	for (int i=xf1;i<=xf2+1;i++,ppixel++) 
	{
		if (fill_on) 
		{
			if (i==xf2+1 || *ppixel == dVal) 
			{
				fill_on=0;
				if (iYSeed1>0) 
					FillRow(dVal,ist,i-1,iYSeed1-1);
			
				if (iYSeed1<Bottom()-1) 
					FillRow(dVal,ist,i-1,iYSeed1+1);
			}
		}
		else 
		{
			if (*ppixel != dVal) 
			{
				fill_on=1;
				ist=i;
			}
		}

		
		if (i<Right()) 
			*ppixel= dVal;
	}
}

/*
template <class TPixel>
void CDImageMyClass<TPixel>::FillInside(const double dVal, int xf1, int xf2, int iYSeed)
{
	int	i,ist;
	int	fill_on;
	
	if (Pixel(xf1,iYSeed) != dVal)
	{
		while (xf1>0 && Pixel(xf1-1,iYSeed) != dVal) xf1--;
	}
	
	if (Pixel(xf2,iYSeed) != dVal)
	{
		while (xf2<Right()-1 && Pixel(xf2+1,iYSeed) != dVal) xf2++;
	}
	
	TPixel* ppixel = &Pixel(xf1,iYSeed);
	fill_on = (*ppixel != dVal);

	for (i=ist=xf1;i<=xf2+1;i++,ppixel++) 
	{
		if (fill_on) 
		{
			if (i==xf2+1 || *ppixel == dVal) 
			{
				fill_on=0;
				if (iYSeed>0) 
					FillInside(dVal,ist,i-1,iYSeed-1);
			
				if (iYSeed<Bottom()-1) 
					FillInside(dVal,ist,i-1,iYSeed+1);
			}
		}
		else 
		{
			if (*ppixel != dVal) 
			{
				fill_on=1;
				ist=i;
			}
		}

		
		if (i<Right()) 
			*ppixel= dVal;
	}
}
*/

template <class TPixel>
void CDImageMyClass<TPixel>::Crop()
{	
	CDImageMyClass<TPixel> tmp = *this;

	Allocate(Width(),Height());
	tmp.CopyPixelsTo(*this,evisnormalizeCopyBytesSameType);
	
	CString txt;
	txt.Format("Image cropped to %d,%d",Width(),Height());
	AddToHistory(txt);
}


// explicit class instantiation

template CDImageMyClass<signed char>;
template CDImageMyClass<unsigned char>;
//template CDImageMyClass<int>;
//template CDImageMyClass<short>;
template CDImageMyClass<unsigned short>;
template CDImageMyClass<float>;
template CDImageMyClass<double>;

template CDImageMyClass<CVisRGBABytePixel>;
