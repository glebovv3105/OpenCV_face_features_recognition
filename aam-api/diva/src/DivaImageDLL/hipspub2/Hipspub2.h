/********************************************************
   hipspub2.h  (an extended hipspub.h)                                               
							 
   The HIPSPUB stuff is Copyrighted (c) 1992 Michael Landy
   See also the hpub_*.c comments.

   TOOLS FOR THE HIPSPUB WRITING
   LAYOUT: Jorgen Folm-Hansen 
   Date: may. 6th 1997 
******************************************************** 
Disclaimer:  No guarantees of performance accompany this 
software, nor is any responsibility assumed on the part of
the authors. All the software has been tested extensively 
and every effort has been made to insure its reliability.
********************************************************/

#ifndef _HIPSPUB2

#define _HIPSPUB2


/****** INCLUDES ******/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


/****** CONSTANTS ******/
#ifndef TRUE
#define TRUE 1
#endif /*TRUE*/
#ifndef FALSE
#define FALSE 0
#endif /* FALSE */

#define byte unsigned char

#define PFBYTE          0       /* Bytes interpreted as unsigned integers */
#define PFSHORT         1       /* Short integers (2 bytes) */
#define PFINT           2       /* Integers (4 bytes) */
#define PFFLOAT         3       /* Float's (4 bytes)*/
#define PFCOMPLEX       4       /* 2 Float's interpreted as (real,imaginary) */
#define PFASCII         5       /* ASCII rep, with linefeeds after each row */
#define PFDOUBLE        6       /* Double's (8 byte floats) */
#define PFDBLCOM        7       /* Double complex's (2 Double's) */
#define PFMSBF          30      /* packed, most-significant-bit first */
#define PFLSBF          31      /* packed, least-significant-bit first */
#define PFSBYTE         32      /* signed bytes */
#define PFUSHORT        33      /* unsigned shorts */
#define PFUINT          34      /* unsigned ints */
#define PFRGB           35      /* RGBRGBRGB bytes */
#define PFRGBZ          36      /* RGB0RGB0RGB0 bytes */
#define PFRGBFLT        37      /* RGBRGBRGB floats */
#define LINELENGTH		200     /* max characters per line in header vars */
#define MSBFIRST		1       /* HIPS-1 bit_packing value for PFMSBF */
#define LSBFIRST		2       /* HIPS-1 bit_packing value for PFLSBF */


/****** STRUCTURES *****/
typedef struct hipsfilestat {
  FILE *fp;
  byte *pp;
  char name[256];
  int flag;
  int format;
  int rows;
  int cols;
  int nsize;
  int sizepix;
  int colors;
  int numframes;
  int ncmap;
  byte *r;
  byte *g;
  byte *b;
  int ndepth;
  int nvar;
  int ntime;
  int roi_c1;
  int roi_c2;
  int roi_r1;
  int roi_r2;
  int roi_z1;
  int roi_z2;

  char onm[LINELENGTH];
  char snm[LINELENGTH];
  char odt[LINELENGTH];


  int sizehistory;
  int sizedescription;
  char *history;
  char *description;

} hipsstruct;


/* extended parameter list structure */
struct hpub_xparlist {
  int   numparam;       /* Count of additional parameters */
  struct hpub_extpar *params;   /* Additional parameters */
};

/* the extended parameter item structure */
struct hpub_extpar {
  char *name;           /* name of this variable */
  int format;           /* format of values (PFBYTE, PFINT, etc.) */
  int count;            /* number of values */
  int offset;           /* offset into binary area for arrays */
  union {
	unsigned char v_b;      /* PFBYTE/PFASCII, count = 1 */
	int v_i;        /* PFINT, count = 1 */
	short v_s;      /* PFSHORT, count = 1 */
	float v_f;      /* PFFLOAT, count = 1 */
	unsigned char *v_pb;    /* PFBYTE/PFASCII, count > 1 */
	int *v_pi;      /* PFINT, count > 1 */
	short *v_ps;    /* PFSHORT, count > 1 */
	float *v_pf;    /* PFFLOAT, count > 1 */
  } val;
  struct hpub_extpar *nextp;    /* next parameter in list */
};

#define HP_NULLPAR      ((struct hpub_extpar *) 0)


/****** PROTOTYPES ******/

/* from hpub_xparams.c */
void hpub_perr(char *s);
void hpub_initparams(struct hpub_xparlist *xpar);
void hpub_setparamb(struct hpub_xparlist *xpar, char *name, unsigned char value);
void hpub_setparamc(struct hpub_xparlist *xpar, char *name, unsigned char value);
void hpub_setparams(struct hpub_xparlist *xpar, char *name, short value);
void hpub_setparami(struct hpub_xparlist *xpar, char *name, int value);
void hpub_setparamf(struct hpub_xparlist *xpar, char *name, float value);
void hpub_setparamb2(struct hpub_xparlist *xpar,char *name,int count,unsigned char *value);
void hpub_setparamc2(struct hpub_xparlist *xpar,char *name,int count,unsigned char *value);
void hpub_setparams2(struct hpub_xparlist *xpar, char *name, int count, short *value);
void hpub_setparami2(struct hpub_xparlist *xpar, char *name, int count, int *value);
void hpub_setparamf2(struct hpub_xparlist *xpar, char *name, int count, float *value);
struct hpub_extpar *hpub_xpar_findalloc(struct hpub_xparlist *xpar, char *name);
unsigned char hpub_getparamb(struct hpub_xparlist *xpar, char *name);
unsigned char hpub_getparamc(struct hpub_xparlist *xpar, char *name);
short hpub_getparams(struct hpub_xparlist *xpar, char *name);
int hpub_getparami(struct hpub_xparlist *xpar, char *name);
float hpub_getparamf(struct hpub_xparlist *xpar, char *name);
unsigned char *hpub_getparamb2(struct hpub_xparlist *xpar, char *name, int *count);
unsigned char *hpub_getparamc2(struct hpub_xparlist *xpar, char *name, int *count);
short *hpub_getparams2(struct hpub_xparlist *xpar, char *name, int *count);
int *hpub_getparami2(struct hpub_xparlist *xpar, char *name, int *count);
float *hpub_getparamf2(struct hpub_xparlist *xpar, char *name, int *count);
struct hpub_extpar *hpub_checkfind(struct hpub_xparlist *xpar, char *name);
int hpub_checkparam(struct hpub_xparlist *xpar, char *name);
void hpub_clearparam(struct hpub_xparlist *xpar, char *name);
struct hpub_extpar *hpub_findparam(struct hpub_xparlist *xpar, char *name);
int hpub_checkname(char *name);

/* from hpub_wrthdr.c */
int hpub_wrthdr(int format,int rows,int cols,int frames,int colors);
int hpub_wrthdrc(int format,int rows,int cols,int frames,int colors,int ncmap,byte *r,byte *g,byte *b);
int hpub_wrthdrx(int format,int rows,int cols,int frames,int colors,struct hpub_xparlist *xpar);
int hpub_fwrthdr(hipsstruct *hd);
int hpub_fwrthdrc(hipsstruct *hd);
int hpub_fwrthdrx(hipsstruct *hd,struct hpub_xparlist *xpar);

/* from hpub_rdhdr.c */
int hpub_rdhdr(int *format,int *rows,int *cols,int *frames,int *colors);
int hpub_rdhdrc(int *format,int *rows,int *cols,int *frames,int *colors,int *ncmap, byte **r,byte **g,byte **b);
int hpub_rdhdrx(int *format,int *rows,int *cols,int *frames,int *colors,struct hpub_xparlist *xpar);
int hpub_frdhdr(hipsstruct *hd);
/*int hpub_frdhdr(FILE *fp,int *format,int *rows,int *cols,int *frames,int *colors);*/
/*int hpub_frdhdrc(FILE *fp,int *format,int *rows,int *cols,int *frames,int *colors,int *ncmap,byte **r,byte **g,byte **b);*/
int hpub_frdhdrc(hipsstruct *hd);
int hpub_frdhdrx(hipsstruct *hd,struct hpub_xparlist *xpar);
/*int hpub_frdhdrx(FILE *fp,int *format,int *rows,int *cols,int *frames,int *colors,struct hpub_xparlist *xpar);*/
char *hpub_strsave(char *);

/* from hipsimm.c */
void imm_ResetHipsStruct(hipsstruct *);
void imm_CopyHipsStruct(hipsstruct *, hipsstruct *);
void imm_SetHipsStruct(hipsstruct *, char *, FILE *, int);
void imm_SetNewHistory(hipsstruct *, char *);
void imm_AddToHistory(hipsstruct *, char *);
void imm_SetNewDescription(hipsstruct *, char *);

byte *imm_AllocMem(int);
int imm_GetByteSizeOf(int);
int imm_AllocHipsStruct(hipsstruct *);
int imm_AllocHipsStructAllFrames(hipsstruct *);
int imm_ReadHeader(hipsstruct *);
int imm_WriteHeader(hipsstruct *);
int imm_ReadNextFrame(hipsstruct *);
int imm_ReadAllFrames(hipsstruct *);
double imm_GetPixelValue(hipsstruct *, int, int);
void imm_GetPixelVector(hipsstruct *, int, int, double *);
int imm_SetPixelValue(hipsstruct *, int *, int *, double *);
int imm_WriteFrame(hipsstruct *);
int imm_WriteAllFrames(hipsstruct *);
void imm_CloseHipsStruct(hipsstruct *);

#endif /* _HIPSPUB2 */











