

/*

  DLL issues:

  http://support.microsoft.com/support/kb/articles/Q168/9/58.ASP

*/

#ifndef DIVAImageExport
#define DIVAImageExport __declspec(dllexport)
#endif // DIVAImageExport



DIVAImageExport DIVAImageDLL_foo();
