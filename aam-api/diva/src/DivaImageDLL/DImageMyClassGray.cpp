/////////////////////////////////////////////////////////////////////////////
//
// Image class for personale functions
//
// Copyright � 1998
//
// IMM, Department of Mathematical Modelling
// Technical University of Denmark, Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk
//
// author: Rune Fisker 
//
// Disclaimer:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on the Microsoft Vision Software Developers Kit VisSDK
//
/////////////////////////////////////////////////////////////////////////////

#include <Afx.h>
#include "DImageMyClassGray.h"
/*
CDImageMyClassGray<byte>;
CDImageMyClassGray<int>;
CDImageMyClassGray<short>;
CDImageMyClassGray<unsigned short>;
CDImageMyClassGray<float>;
CDImageMyClassGray<double>;
*/
/////////////////////////////////////////////////////////////////////////////
//	Takes log of each pixel. Supports only gray images
//
//	author:	Rune Fisker, 29/4-1998
template <class TPixel>
void CDImageMyClassGray<TPixel>::Exp()
{
	TPixel t = 0;
	TPixel tMinRange = VisRangeMin(t,false);

	for (int y = Top();y < Bottom(); y++)
	{
		for (int x = Left();x < Right(); x++)
		{
			Pixel(x,y) = exp(Pixel(x,y));
		}
	}

	// update history
	AddToHistory("DIVA: Exp of each pixel (Exp)");

}


template <class TPixel>
void CDImageMyClassGray<TPixel>::FullPathNoExt(CString& strOut)
{	
	// make new name
	strOut = Name(true);
	
	int iPos = 0;
	
	while (strOut[iPos] != '.')
		iPos++;
	
	strOut.Delete( iPos, strOut.GetLength() - iPos );
}

template <class TPixel>
TPixel CDImageMyClassGray<TPixel>::MinPos(int& x, int &y)
{
	assert(NBands() == 1);

	TPixel tMin = Pixel(Left(),Top());
	x = Left();
	y = Top();

	for (int r = Top();r < Bottom(); r++)
	{
		TPixel *ptPixel = RowPointer(r);
		for (int c = Left();c < Right();c++)
		{
			if (ptPixel[c] < tMin)
			{
				tMin = ptPixel[c];
				x = c;
				y = r;
			}
		}
	}

	return tMin;
}

template <class TPixel>
TPixel CDImageMyClassGray<TPixel>::MaxPos(int& x, int &y)
{
	assert(NBands() == 1);

	TPixel tMax = Pixel(Left(),Top());
	x = Left();
	y = Top();

	for (int r = Top();r < Bottom(); r++)
	{
		TPixel *ptPixel = RowPointer(r);
		for (int c = Left();c < Right();c++)
		{
			if (ptPixel[c] > tMax)
			{
				tMax = ptPixel[c];
				x = c;
				y = r;
			}
		}
	}

	return tMax;
}

template <class TPixel>
void CDImageMyClassGray<TPixel>::Normalize()
{
	double dMean,dStd;

	Mean(dMean);
	Std(dStd);

	*this -= dMean;

	*this /= dStd;
}

/////////////////////////////////////////////////////////////////////////////
// takes square of each pixel. Supports only gray images
//
// author:	Rune Fisker 8/4-1999
template <class TPixel>
void CDImageMyClassGray<TPixel>::Sqr0(CDImageMyClassGray<TPixel>& imageOut) 
{
	try
	{
		// try ipl first
		IplImage *iplSrcImage	= Image2IplImage();
		IplImage *iplDestImage	= imageOut.Image2IplImage();

		iplSquare(iplSrcImage,iplDestImage);

	}
	catch(CVisError error)
	{
		assert(NBands() == 1);

		if ( ( imageOut.Width() < Width() ) || ( imageOut.Height() < Height() ) )
		{
			imageOut.Allocate(Width(),Height());
		}

		TPixel *ppixel;
		TPixel *ppixelOut;
		
		int yOut = imageOut.Top();

		for (int y = Top();y < Bottom(); y++)
		{
			ppixel = &(RowPointer(y)[Left()]);
			ppixelOut = &(imageOut.RowPointer(yOut)[imageOut.Left()]);
			
			for (int x = Left();x < Right();x++)
			{
				*ppixelOut++ = *ppixel*(*ppixel);
				*ppixel++;
			}

			yOut++;
		}
	}

	// update history
	imageOut.AddToHistory("DIVA: Square of each pixel (Sqr)");
}


template <class TPixel>
void CDImageMyClassGray<TPixel>::Sum(double& dSum)
{
	dSum = 0;

	for (int y = Top();y < Bottom(); y++)
	{
		TPixel* ppixel = &(Pixel(Left(),y));
		
		for (int x = Left();x < Right();x++)
		{
			dSum += *ppixel++;
		}
	}
}

//
// author: mikkel b. stegmann
//
template <class TPixel>
void CDImageMyClassGray<TPixel>::Get2DProfile(	
										  
					CDVector p1, CDVector p2, 
					CDVector &outVector, 
					eDInterPol interPolationMethod = nearestNeighbour ) {

	const int X=0;
	const int Y=1;
	double a, b, c, d, x, y, t;
	
	// calc line parameters
	a = p2[X]-p1[X];
	b = p1[X];	
	c = p2[Y]-p1[Y];
	d = p1[Y];

	// calc number of samples along the line
	int nbSamples = outVector.Length();
	if (nbSamples<2) {

		// calc distance in pixels between p1 and p2
		int len = (int)(.5+sqrt( (p2[X]-p1[X])*(p2[X]-p1[X]) + 
								 (p2[Y]-p1[Y])*(p2[Y]-p1[Y])	) );

		nbSamples = len;
		outVector.Resize( nbSamples );
	}

	// sample along the line
	for(int i=0;i<nbSamples;i++) {

		t = (double)i/(nbSamples-1);
		x = a*t + b;
		y = c*t + d;
/*
		switch(interPolationMethod) {

		case nearestNeighbour:

				xp = (int)(x+.5);
				yp = (int)(y+.5);
				
				// clamp to edges
				xp = xp>=Width() ? Width()-1 : xp;
				xp = xp<0        ? 0         : xp;
				
				yp = yp>=Height() ? Height()-1 : yp;
				yp = yp<0         ? 0          : yp;

				outVector[i] = Pixel( xp, yp );
				break;

		default:
				break;

		}
*/		
		switch(interPolationMethod) {

			case nearestNeighbour:	outVector[i] = Pixel0( x, y );
									break;
			case bilinear:			outVector[i] = Pixel1( x, y );
									break;
			case biquadratic:		outVector[i] = Pixel2( x, y );
									break;
			case bicubic:			outVector[i] = Pixel3( x, y );
									break;
			default:				outVector[i] = Pixel0( x, y );
									break;
		}
	}
}


// explicit class instantiation

template CDImageMyClassGray<signed char>;
template CDImageMyClassGray<unsigned char>;
//template CDImageMyClassGray<int>;
//template CDImageMyClassGray<short>;
template CDImageMyClassGray<unsigned short>;
template CDImageMyClassGray<float>;
template CDImageMyClassGray<double>;

//template CDImageMyClassGray<CVisRGBABytePixel>;

