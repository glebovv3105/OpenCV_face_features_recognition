//****************************************************************************
//
// Copyright � 1999-2001 by Informatics & Mathmatical Modelling, 
//                          Section for Image Analysis
// 
// IMM, Informatics & Mathmatical Modelling
// Technical University of Denmark
// Richard Petersens Plads
// Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/~diva/
//
// Original author: Rune Fisker
//
// Current author:  Mikkel B. Stegmann - mbs@imm.dtu.dk
//
// Contributions:   Lars Pedersen - lap@imm.dtu.dk, 
//                  Henrik Aanaes - haa@imm.dtu.dk, 
//                  + several other peoples at IMM.
//
// $Id: DIVAMatrix.h,v 1.1.1.1 2003/01/03 19:18:14 aam Exp $
//
// DISCLAIMER:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on;
//
//		- The Microsoft Vision Software Developers Kit, VisSDK
//
//****************************************************************************
#if !defined(__DIVA_MATRIX_DLL__)
#define __DIVA_MATRIX_DLL__


/*

  DLL issues:

  http://support.microsoft.com/support/kb/articles/Q168/9/58.ASP

*/
#ifndef DIVAMatrixExport
#define DIVAMatrixExport __declspec(dllexport)
#define EXPIMP
#endif // DIVAMatrixExport


#endif // __DIVA_MATRIX_DLL__