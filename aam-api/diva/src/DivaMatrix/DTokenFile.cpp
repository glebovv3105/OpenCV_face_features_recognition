/* $Id: DTokenFile.cpp,v 1.1.1.1 2003/01/03 19:18:16 aam Exp $ */
// TokenFile.cpp: implementation of the CTokenFile class.
//
//////////////////////////////////////////////////////////////////////

//#include "stdafx.h"
//#include "ColorCalibration.h"
#include "DTokenFile.h"
#include <string.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif
 
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDTokenFile::CDTokenFile()
{
	m_CurString.Empty(); // make string info empty
	m_Delimiter = " ,\n\t";
	strtok((char *)(LPCTSTR)m_CurString,m_Delimiter);
}

CDTokenFile::~CDTokenFile()
{

}

BOOL CDTokenFile::NextToken(CString& tok)
{
	char *tpos;
	BOOL rval;

	if ((tpos = strtok(NULL,m_Delimiter)) == NULL) // is there any valid data left in string ?
	{
		m_CurString.Empty();
		while ((rval = this->ReadString(m_CurString)) && m_CurString.IsEmpty()) {}
		if (!rval) return FALSE; // we reached end of file

		tpos = strtok((char *)(LPCTSTR)m_CurString,m_Delimiter);
		if (tpos == NULL) return FALSE;
	}

	tok = tpos;
	return TRUE;
}
