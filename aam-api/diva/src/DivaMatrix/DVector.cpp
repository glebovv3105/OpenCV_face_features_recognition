/* $Id: DVector.cpp,v 1.1.1.1 2003/01/03 19:18:16 aam Exp $ */
#include "DIVAMatrix.h"
#include "DMatrix.h"
#include "DTokenFile.h"
#include <fstream.h>
#include <stdlib.h>
#include <Afxtempl.h>


/**

  @author   Rune Fisker
  @version  2-5-2001

  @memo     Assignment operator.

  @doc      Assignment operator. 
  
  @param    vIn     Input vector.
  
  @return   This.
  
*/
DIVAMatrixExport CDVector& CDVector::operator=(const CVisDVector &vIn) {
	
	CVisDVector& vTmp = *this; vTmp = vIn; return *this; 
}


/**

  @author   Rune Fisker
  @version  2-5-2001

  @memo     Assignment operator.

  @doc      Assignment operator. 
  
  @param    vIn     Input vector.
  
  @return   This.
  
*/
DIVAMatrixExport CDVector& CDVector::operator=(const CDVector &vIn) {
	
	CVisDVector& vTmp = *this; vTmp = vIn; return *this; 
}


/**

  @author   Rune Fisker
  @version  2-5-2001

  @memo     Assignment operator.

  @doc      Assignment operator. Sets all element to the input value. 
  
  @param    vIn     Input double.
  
  @return   This.
  
*/
DIVAMatrixExport CDVector& CDVector::operator=(double value) {
	
	CVisDVector& vTmp = *this; vTmp = value; return *this; 
}


/**

  @author   Rune Fisker
  @version  3-5-1999

  @memo     Finds the maximum element in the vector.

  @doc      Finds the maximum element in the vector.  
  
  @return   The maximum value of the elements.
  
*/
DIVAMatrixExport double CDVector::Max() const
{
	double dMax = (*this)[0];

	for (int i=1;i < Length(); i++)
	{
		dMax = max(dMax,(*this)[i]);
	}

	return dMax;
}


/**

  @author   Rune Fisker
  @version  3-5-1999

  @memo     Finds the minimum element in the vector.

  @doc      Finds the minimum element in the vector.  
  
  @return   The minimum value of the elements.
  
*/
DIVAMatrixExport double CDVector::Min() const
{
	double dMin = (*this)[0];

	for (int i=1;i < Length(); i++)
	{
		dMin = min(dMin,(*this)[i]);
	}

	return dMin;
}


/**

  @author   Rune Fisker
  @version  1-9-1999

  @memo     Finds the minimum element in the vector and its position.

  @doc      Finds the minimum element in the vector and its position.
  
  @return   The minimum value of the elements and its position.
  
*/
DIVAMatrixExport double CDVector::Min(int& iPos) const
{
	double dMin = (*this)[0];
	iPos = 0;

	for (int i=1;i < Length(); i++)
	{
		if (dMin > (*this)[i])
		{
			dMin = (*this)[i];
			iPos = i;
		}
	}

	return dMin;
}


/**

  @author   Rune Fisker
  @version  3-5-1999

  @memo     Calculates the mean value of the vector.

  @doc      Calculates the mean value of the vector.  
  
  @return   The mean value.
  
*/
DIVAMatrixExport double CDVector::Mean() const
{
	return Sum()/Length();
}


/**

  @author   Rune Fisker
  @version  3-5-1999

  @memo     Calculates the skewness of the vector.

  @doc      Calculates the skewness the vector.  
  
  @return   The skewness.
  
*/
DIVAMatrixExport double CDVector::Skewness() const
{
	double dSkewness = 0, dMean = Mean(), dTmp;

	for (int i=0;i < Length(); i++)
	{
		dTmp = (*this)[i] - dMean;
		dSkewness += dTmp*dTmp*dTmp;
	}

	double dStd = Std();
	return dSkewness/(Length()*dStd*dStd*dStd);
}


/**

  @author   Rune Fisker
  @version  3-5-1999

  @memo     Calculates the standard deviation of the vector.

  @doc      Calculates the standard deviation the vector.  
  
  @return   The skewness.
  
*/
DIVAMatrixExport double CDVector::Std() const
{
	return sqrt(Var());
}


/**

  @author   Rune Fisker
  @version  3-5-1999

  @memo     Calculates the sum of the vector.

  @doc      Calculates the sum of the vector.
  
  @return   The sum.
  
*/
DIVAMatrixExport double CDVector::Sum() const
{
	double dSum = 0;

	for (int i=0;i < Length(); i++)
	{
		dSum += (*this)[i];
	}

	return dSum;
}


/**

  @author   Rune Fisker
  @version  3-5-1999

  @memo     Calculates the variance of the vector.

  @doc      Calculates the variance the vector.  

  @param    pMean   Optional pointer, where the mean is returned.
  
  @return   The variance.
  
*/
DIVAMatrixExport double CDVector::Var( double *pMean ) const
{
	double dVar = 0, dMean = Mean();

	for (int i=0;i < Length(); i++)
	{
		dVar += ((*this)[i] - dMean)*((*this)[i] - dMean);
	}

    if (pMean) {

        *pMean = dMean;
    }

	return dVar/Length();
}


/*
    static compare functions for the CDVector::Sort() function
*/
static int __dbl_cmp_asc( const void *arg1, const void *arg2 ) {   

    double v1 = *(double*)arg1;
    double v2 = *(double*)arg2;

    if (v1<v2)
        return -1;
    if (v1>v2)
        return  1;

    return 0;
}
static int __dbl_cmp_des( const void *arg1, const void *arg2 ) {   

    double v1 = *(double*)arg1;
    double v2 = *(double*)arg2;

    if (v1>v2)
        return -1;
    if (v1<v2)
        return  1;

    return 0;
}

/**

  @author   Mikkel B. Stegmann
  @version  2-2-2001

  @memo     Sorts the vector.

  @doc      Sorts the vector in either ascending (default) or descending order.  
            Implemented using the standard quicksort, qsort().

			The STL sort is most likely faster, though.
  
  @param    ascending   If true (default) the vector is sorted in ascending
                        order. If false its sorted in descending order.  
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::Sort( bool ascending ) {

    qsort(  &((*this)[0]), (size_t)this->Length(), sizeof( double ), 
            ascending ? __dbl_cmp_asc : __dbl_cmp_des   );
}


/**

  @author   Rune Fisker
  @version  1-5-1999

  @memo     Squares each element.

  @doc      Squares each element.  
    
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::Sqr()
{

	for (int i=0;i < Length(); i++)
	{
		(*this)[i] *= (*this)[i];
	}
}


/**

  @author   Rune Fisker
  @version  1-5-1999

  @memo     Takes the square root of each element.

  @doc      Takes the square root of each element.
    
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::Sqrt()
{

	for (int i=0;i < Length(); i++)
	{
		(*this)[i] = sqrt((*this)[i]);
	}
}


/**

  @author   Rune Fisker
  @version  1-5-1999

  @memo     Takes the power 'dP' of each element.

  @doc      Takes the power 'dP' of each element.

  @param    dP      The exponent.
    
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::Pow(double dP)
{
	for (int i=0;i < Length(); i++)
	{
		(*this)[i] = pow((*this)[i],dP);
	}
}


/**

  @author   Rune Fisker
  @version  1-5-1999

  @memo     Multiplies two vectors element-wise.

  @doc      Multiplies two vectors element-wise.
            Corresponding MatLab operator ".*".

  @param    vector  The input vector to multiply this vector with.
    
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::ElementMultiply(const CDVector& vector)
{
	assert(Length() == vector.Length());

	for (int i=0;i < Length(); i++)
	{
		(*this)[i] *= vector[i];
	}
}


/**

  @author   Rune Fisker
  @version  1-5-1999

  @memo     Divide two vectors element-wise.

  @doc      Divide two vectors element-wise.
            Corresponding MatLab operator "./".

  @param    vector  The input vector to divide this vector with.
    
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::ElementDivide(const CDVector& vector)
{
	assert(Length() == vector.Length());

	for (int i=0;i < Length(); i++)
	{
		(*this)[i] /= vector[i];
	}
}


/**

  @author   Rune Fisker
  @version  1-5-1999

  @memo     Returns a string representing the vector. 

  @doc      Returns a string representing the vector. 

  @param    fNewline    If true, the string is terminated with a new line.
    
  @return   The output string.
  
*/
DIVAMatrixExport CDString CDVector::ToString(const bool fNewline) const
{
	CDString strOut = "";

	CDString strTmp;
	
	for(int i=0;i<Length();i++)
	{
		strTmp.Format("%8.3f ",(*this)[i]);
		strOut += strTmp;
	}

	if (fNewline)
		strOut += "\r\n";	

    return strOut;
}


/**

  @author   Mikkel B. Stegmann
  @version  2-5-2001

  @memo     Calculates the one-norm (L1) of the vector.

  @doc      Calculates the one-norm (L1) of the vector. 
            Also known as the city block metric.

            L1(v) = |x_1| + |x_2| .. + |x_n|            
    
  @return   The L1 norm.
  
*/
DIVAMatrixExport double CDVector::Norm1() const
{
	double *pData = NULL, norm=0;
    int len = Length();

    pData = &((*(CDVector*)this)[0]);

	for(int c=0;c<len;c++)	{        
		norm+= fabs(pData[c]);        
	}
	return norm;
}


/**

  @author   Mikkel B. Stegmann
  @version  2-5-2001

  @memo     Calculates the two-norm (L2) of the vector.            

  @doc      Calculates the two-norm (L2) of the vector.
            Also known as the Euclidean length.

            L2(v) = sqrt( x_1^2 + x_2^2  .. + x_n^2 )
    
  @return   The L2 norm.
  
*/
DIVAMatrixExport double CDVector::Norm2() const
{
	double *pData = NULL, norm=0;
    int len = Length();

    pData = &((*(CDVector*)this)[0]);

	for(int c=0;c<len;c++)	{        
		norm += pData[c]*pData[c];        
	}
	return sqrt(norm);
}


/**

  @author   Mikkel B. Stegmann
  @version  2-5-2001

  @memo     Calculates the infinity-norm (Linf) of the vector.            

  @doc      Calculates the infinity-norm (Linf) of the vector.
            Also known as the Chebyshev Norm.

            Linf(v) = max( |x_1|, |x_2| ... , |x_n| )
    
  @return   The L2 norm.
  
*/
DIVAMatrixExport double CDVector::NormInf() const
{
	double val, *pData = NULL, max=-1e306;
    int len = Length();

    pData = &((*(CDVector*)this)[0]);

	for(int c=0;c<len;c++)	{    
        
        val = fabs(*pData++);
        max =  val > max ? val : max;		        
	}
	return max;
}


/**

  @author   Mikkel B. Stegmann
  @version  2-5-2001

  @memo     Normalizes the vector to unit length, using the 2-norm.

  @doc      Normalizes the vector to unit length, using the 2-norm.
    
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::Normalize2()
{
	double norm;
    int len = this->Length();

	norm=this->Norm2();

	for(int c=0;c<len;c++)
	{
		(*this)[c]/=norm;
	}

}


/**

  @author   Henrik Aan�s
  @version  2-5-2001

  @memo     Writes the vector to disk in MatLab (.m) format.

  @doc      Writes the vector to disk in MatLab (.m) format.
            To read the vector into MatLab write e.g. 
            'my_vector.m' at the MatLab prompt.  

            Notice that this should be used for storage a (really)
            large vectors, due to the computational and i/o overhead
            induced by the simple MatLab text format.

            Also, remember that MatLab can't read (.m) files with lines
            longer than 4096 bytes.

            If no communication with MatLab is needed, but merely 
            to/from disk functionality within a DIVA program, it is
            suggested to use the fast binary i/o methods ToFile() and
            FromFile().      
  
  @param    sFilename   Output file name. Should have the extension '.m'.

  @param    sName       Name of destination matlab variable.

  @param    sComment    Optional comment inside the file.

  @param    fAppend     If true, the vector is appended to the file 
                        'sFilename'.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::ToMatlab(const CDString& sFilename,const CDString& sName,const CDString& sComment,bool fAppend) const
{
	assert(Length() > 0);

	CDMatrix mOut(Length(),1);

	memcpy(mOut[0],&((*this)[0]),sizeof(double)*Length());
	mOut.ToMatlab(sFilename,sName,sComment,fAppend);
}


/**

  @author   Henrik Aan�s
  @version  2-5-2001

  @memo     Reads a vector from disk in MatLab (.m) format.

  @doc      Reads a vector from disk in MatLab (.m) format into
            'this'. 

            Notice that this should be used for storage a (really)
            large vectors, due to the computational and i/o overhead
            induced by the simple MatLab text format.

            Also, remember that MatLab can't read (.m) files with lines
            longer than 4096 bytes.

            If no communication with MatLab is needed, but merely 
            to/from disk functionality within a DIVA program, it is
            suggested to use the fast binary i/o methods ToFile() and
            FromFile().                        
  
  @param    sFilename   Input file name.

  @param    sName       The name to search for (and load) inside the matlab file.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::FromMatlab(const CDString& sFilename,const CDString& sName)
{
	CDMatrix mOut;
	mOut.FromMatlab(sFilename,sName);

	Resize(mOut.NRows());
	memcpy(&((*this)[0]),mOut[0],sizeof(double)*Length());
}


/**

  @author   Mikkel B. Stegmann
  @version  2-5-2001

  @memo     Writes the vector to disk in binary format.

  @doc      Writes the vector to disk in binary format.
            Use 'readbin.m' to load such a file into MatLab
            (placed in the diva/matlab dir).              
  
  @param    sFilename   Output file name.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::ToFile( const CDString& sFilename ) const
{
	assert(Length() > 0);

	CDMatrix mOut(1,Length());

	memcpy(mOut[0],&((*this)[0]),sizeof(double)*Length());
	mOut.ToFile(sFilename);
} 


/**

  @author   Mikkel B. Stegmann
  @version  2-5-2001

  @memo     Reads a vector from disk in binary format.

  @doc      Reads a vector from disk in binary format.                        
  
  @param    sFilename   Input file name.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::FromFile( const CDString& sFilename )
{
	CDMatrix mOut;
	mOut.FromFile(sFilename);

	Resize(mOut.NCols());
	memcpy(&((*this)[0]),mOut[0],sizeof(double)*Length());
}


/**

  @author   Mikkel B. Stegmann
  @version  2-5-2001

  @memo     Writes the vector to disk in binary format.

  @doc      Writes the vector to disk in binary format.
            Use 'readbin.m' to load such a file into MatLab
            (placed in the diva/matlab dir).              
  
  @param    fh   Open file handle (opened using fopen())
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::ToFile( FILE *fh ) const
{
	assert(Length() > 0);

	CDMatrix mOut(1,Length());

	memcpy(mOut[0],&((*this)[0]),sizeof(double)*Length());
	mOut.ToFile( fh );
}


/**

  @author   Mikkel B. Stegmann
  @version  2-5-2001

  @memo     Reads a vector from disk in binary format.

  @doc      Reads a vector from disk in binary format.                        
  
  @param    fh   Open file handle (opened using fopen())
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::FromFile( FILE *fh )
{
	CDMatrix mOut;
	mOut.FromFile( fh );

	Resize(mOut.NCols());
	memcpy(&((*this)[0]),mOut[0],sizeof(double)*Length());
}


/**

  @author   Lars Pedersen
  @version  7-12-2000

  @memo     Equal - compares a vector and a double.            

  @doc      Compares of a vector and a double and returns
            the result as a binary vector i.e.:

                C(i) = 1 if A(i) == B
                C(i) = 0 else 

            Corresponding MatLab function EQ()
  
  @param    B   Input double.

  @param    C   Output result vector.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::Eq(const double B, CDVector& C ) const
{
	// CHECK: A and C must have same length
	assert(Length() == C.Length());

	int n = Length();

	for (int r = 0; r < n; r++)
		C[r] = ((*this)[r] == B) ? 1 : 0;
}


/**

  @author   Lars Pedersen
  @version  7-12-2000

  @memo     Equal - compares a vector and a vector.            

  @doc      Compares of a vector and a vector and returns
            the result as a binary matrix i.e.:

                C(i) = 1 if A(i) == B
                C(i) = 0 else 

            Corresponding MatLab function EQ()
  
  @param    B   Input vector.

  @param    C   Output result vector.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::Eq( const CDVector& B, CDVector& C ) const
{
	// CHECK: A, B and C must have same length
	assert(Length() == B.Length());
	assert(Length() == C.Length());

	int n = Length();
	
	for (int r = 0; r < n; r++)
		C[r] = ((*this)[r] == B[r]) ? 1 : 0;
}


/**

  @author   Lars Pedersen
  @version  7-12-2000

  @memo     Not Equal - compares a double and a vector.            

  @doc      Compares of a double and a vector and returns
            the result as a binary matrix i.e.:

                C(i) = 1 if A(i) != B
                C(i) = 0 else 

            Corresponding MatLab function NE()
  
  @param    B   Input double.

  @param    C   Output result vector.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::Ne(const double B, CDVector& C ) const
{
	// CHECK: A and C must have same length
	assert(Length() == C.Length());

	int n = Length();

	for (int r = 0; r < n; r++)
		C[r] = ((*this)[r] != B) ? 1 : 0;
}


/**

  @author   Lars Pedersen
  @version  7-12-2000

  @memo     Not Equal - compares a vector and a vector.            

  @doc      Compares of a vector and a vector and returns
            the result as a binary matrix i.e.:

                C(i) = 1 if A(i) != B
                C(i) = 0 else 

            Corresponding MatLab function NE()
  
  @param    B   Input vector.

  @param    C   Output result vector.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::Ne( const CDVector& B, CDVector& C ) const
{
	// CHECK: A, B and C must have same length
	assert(Length() == B.Length());
	assert(Length() == C.Length());

	int n = Length();

	for (int r = 0; r < n; r++)
		C[r] = ((*this)[r] != B[r]) ? 1 : 0;
}


/**

  @author   Lars Pedersen
  @version  7-12-2000

  @memo     Less Than - compares a double and a vector.            

  @doc      Compares of a double and a vector and returns
            the result as a binary matrix i.e.:

                C(i) = 1 if A(i) < B
                C(i) = 0 else 

            Corresponding MatLab function LT()
  
  @param    B   Input double.

  @param    C   Output result vector.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::Lt(const double B, CDVector& C ) const
{
	// CHECK: A and C must have same length
	assert(Length() == C.Length());

	int n = Length();

	for (int r = 0; r < n; r++)
		C[r] = ((*this)[r] < B) ? 1 : 0;
}


/**

  @author   Lars Pedersen
  @version  7-12-2000

  @memo     Less Than - compares a vector and a vector.            

  @doc      Compares of a vector and a vector and returns
            the result as a binary matrix i.e.:

                C(i) = 1 if A(i) < B
                C(i) = 0 else 

            Corresponding MatLab function LT()
  
  @param    B   Input vector.

  @param    C   Output result vector.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::Lt( const CDVector& B, CDVector& C ) const
{
	// CHECK: A, B and C must have same length
	assert(Length() == B.Length());
	assert(Length() == C.Length());

	int n = Length();

	for (int r = 0; r < n; r++)
		C[r] = ((*this)[r] < B[r]) ? 1 : 0;
}


/**

  @author   Lars Pedersen
  @version  7-12-2000

  @memo     Less Than or Equal - compares a double and a vector.            

  @doc      Compares of a double and a vector and returns
            the result as a binary matrix i.e.:

                C(i) = 1 if A(i) <= B
                C(i) = 0 else 

            Corresponding MatLab function LE()
  
  @param    B   Input double.

  @param    C   Output result vector.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::Le(const double B, CDVector& C ) const
{
	// CHECK: A and C must have same length
	assert(Length() == C.Length());

	int n = Length();

	for (int r = 0; r < n; r++)
		C[r] = ((*this)[r] <= B) ? 1 : 0;
}


/**

  @author   Lars Pedersen
  @version  7-12-2000

  @memo     Less Than or Equal - compares a vector and a vector.            

  @doc      Compares of a vector and a vector and returns
            the result as a binary matrix i.e.:

                C(i) = 1 if A(i) <= B
                C(i) = 0 else 

            Corresponding MatLab function LE()
  
  @param    B   Input vector.

  @param    C   Output result vector.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::Le( const CDVector& B, CDVector& C ) const
{
	// CHECK: A, B and C must have same length
	assert(Length() == B.Length());
	assert(Length() == C.Length());

	int n = Length();

	for (int r = 0; r < n; r++)
		C[r] = ((*this)[r] <= B[r]) ? 1 : 0;
}


/**

  @author   Lars Pedersen
  @version  7-12-2000

  @memo     Greater Than - compares a double and a vector.            

  @doc      Compares of a double and a vector and returns
            the result as a binary matrix i.e.:

                C(i) = 1 if A(i) > B
                C(i) = 0 else 

            Corresponding MatLab function GT()
  
  @param    B   Input vector.

  @param    C   Output result vector.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::Gt(const double B, CDVector& C ) const
{
	// CHECK: A and C must have same length
	assert(Length() == C.Length());

	int n = Length();

	for (int r = 0; r < n; r++)
		C[r] = ((*this)[r] > B) ? 1 : 0;
}


/**

  @author   Lars Pedersen
  @version  7-12-2000

  @memo     Greater Than - compares a vector and a vector.            

  @doc      Compares of a vector and a vector and returns
            the result as a binary matrix i.e.:

                C(i) = 1 if A(i) > B
                C(i) = 0 else 

            Corresponding MatLab function GT()
  
  @param    B   Input vector.

  @param    C   Output result vector.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::Gt( const CDVector& B, CDVector& C ) const
{
	// CHECK: A, B and C must have same length
	assert(Length() == B.Length());
	assert(Length() == C.Length());

	int n = Length();

	for (int r = 0; r < n; r++)
		C[r] = ((*this)[r] > B[r]) ? 1 : 0;
}



/**

  @author   Lars Pedersen
  @version  7-12-2000

  @memo     Greater Than or Equal - compares a double and a vector.            

  @doc      Compares of a double and a vector and returns
            the result as a binary matrix i.e.:

                C(i) = 1 if A(i) >= B
                C(i) = 0 else 

            Corresponding MatLab function GE()
  
  @param    B   Input double.

  @param    C   Output result vector.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::Ge(const double B, CDVector& C ) const
{
	// CHECK: A and C must have same length
	assert(Length() == C.Length());

	int n = Length();

	for (int r = 0; r < n; r++)
		C[r] = ((*this)[r] >= B) ? 1 : 0;
}


/**

  @author   Lars Pedersen
  @version  7-12-2000

  @memo     Greater Than or Equal - compares a vector and a vector.            

  @doc      Compares of a vector and a vector and returns
            the result as a binary matrix i.e.:

                C(i) = 1 if A(i) >= B
                C(i) = 0 else 

            Corresponding MatLab function GE()
  
  @param    B   Input vector.

  @param    C   Output result vector.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::Ge( const CDVector& B, CDVector& C ) const
{
	// CHECK: A, B and C must have same length
	assert(Length() == B.Length());
	assert(Length() == C.Length());

	int n = Length();

	for (int r = 0; r < n; r++)
		C[r] = ((*this)[r] >= B[r]) ? 1 : 0;
}


/**

  @author   Lars Pedersen
  @version  7-12-2000

  @memo     Takes the natural logarithm of each element.

  @doc      Takes the natural logarithm of each element.                
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::Log()
{
	for(int j=0;j<Length();j++)
		(*this)[j] = log((*this)[j]);
}



/**

  @author   Mikkel B. Stegmann
  @version  5-7-2002

  @memo     Takes the absolute valueof each element.

  @doc      Takes the absolute valueof each element.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::Abs() {

    for(int j=0;j<Length();j++) {

        (*this)[j] = fabs((*this)[j]);
    }
}


/**

  @author   Mikkel B. Stegmann
  @version  3-5-2000

  @memo     Reverses the vector.

  @doc      Reverses the vector.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::Reverse() {

	double t;
	int len = Length();

	for (int i=0;i<len/2;i++) {

		// swap elements
		t = (*this)[i];
		(*this)[i] = (*this)[len-i-1];
		(*this)[len-i-1] = t;
	}
}


/**

  @author   Mikkel B. Stegmann
  @version  3-9-2001

  @memo     Uniformly distributed random numbers.

  @doc      Inserts uniformly distributed random numbers in
            the range [0;1].

  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::Rand() {

    int len = Length();
    
    for(int i=0;i<len;i++) {

        (*this)[i] = rand()/(double)RAND_MAX; 
    }
}


/**

  @author   Mikkel B. Stegmann
  @version  3-9-2001

  @memo     Uniformly distributed integer random numbers.

  @doc      Inserts uniformly distributed integer random numbers in
            the range [st;end].

  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::Rand( const int st, const int end ) {

    int len = Length();
    double width = end-st;

    for(int i=0;i<len;i++) {

        (*this)[i] = (int)(.5 + width*(rand()/(double)RAND_MAX) + st); 
    }
}


DIVAMatrixExport void Rand( const int st, const int end );

/**

  @author   Mikkel B. Stegmann
  @version  3-16-2001

  @memo     Forms the cross product of two vectors.

  @doc      Forms the cross product of two vectors and store the result in this.
            If the vector does have the correct size, it will be resized.
  
  @param    v1  Input vector.
  @param    v2  Input vector.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::CrossProduct( const CDVector &v1, const CDVector &v2) {


    if ( this->Length() != v1.Length() ) {

        this->Resize( v1.Length() );
    }
    VisCrossProduct( v1, v2, *this );
}


/**

  @author   Mikkel B. Stegmann
  @version  5-10-2002

  @memo     Takes ceil() of each element.

  @doc      Takes ceil() of each element.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::Ceil() {

    int len = Length();   
    for(int i=0;i<len;i++) {

        (*this)[i] = ceil( (*this)[i] );
    }
}


/**

  @author   Mikkel B. Stegmann
  @version  5-10-2002

  @memo     Takes floor() of each element.

  @doc      Takes floor() of each element.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::Floor() {

    int len = Length();   
    for(int i=0;i<len;i++) {

        (*this)[i] = floor( (*this)[i] );
    }
}


/**

  @author   Mikkel B. Stegmann
  @version  5-10-2002

  @memo     Takes round() of each element.

  @doc      Takes round() of each element.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::Round() {

    int len = Length();   
    for(int i=0;i<len;i++) {

        (*this)[i] = (int)(.5+(*this)[i]);
    }
}
   


/**

  @author   Mikkel B. Stegmann
  @version  5-14-2002

  @memo     Generates a vector of linearly equally spaced points 
            between x1 and x2 (inclusive).

  @doc      Generates a vector of linearly equally spaced points 
            between x1 and x2 (inclusive).  
  
  @param    x1  Starting point.

  @param    x2  Ending point.

  @param    n   Number of points.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::Linspace( const double x1, 
                                          const double x2, const int n ) {

    assert( n>0 );
    assert( x1<x2 );

    Resize(n);
    double mul = (x2-x1)/(n-1.);
    for(int i=0;i<n;i++) {

        (*this)[i] = x1 + i*mul;
    }
}



/**

  @author   Mikkel B. Stegmann
  @version  5-14-2002

  @memo     Returns a sub range of a vector.

  @doc      Returns a sub range of a vector.
     
  @param    st  Starting posistion.

  @param    end End postion.
  
  @return   A sub range in a vector.
  
*/
DIVAMatrixExport CDVector CDVector::Range( const int st, const int end) const {

    assert( st<end );
    assert( end<Length() );

    int len=end-st+1;
    CDVector range( len );
    for( int i=0;i<len;i++) {

        range[i] = (*this)[i+st];
    }

    return range;
}



/**

  @author   Mikkel B. Stegmann
  @version  5-15-2002

  @memo     Converts this vector to a matrix.

  @doc      Converts this vector to a matrix, either by row (default) or column.  
  
  @param    nRows   Number of rows in the matrix.

  @param    nCols   Number of cols in the matrix.

  @param    m       Output matrix.

  @param    rowWise If true (default) vector data is extracted into rows.
                    If false vector data is extracted into cols.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::ToMatrix( const int nRows, const int nCols, 
                                          CDMatrix &m, bool rowWise ) {

    assert( nRows>0 && nCols>0 );
    assert( nRows*nCols==this->Length() );

    int i=0;
    m.Resize( nRows, nCols );
    if (rowWise) {

        for(int r=0;r<nRows;r++) {
            for(int c=0;c<nCols;c++) {

                m[r][c] = (*this)[i++];
            }
        }
    } else {
      
        for(int c=0;c<nCols;c++) {
            for(int r=0;r<nRows;r++) {

                m[r][c] = (*this)[i++];
            }
        }
    }
}


/**

  @author   Mikkel B. Stegmann
  @version  5-15-2002

  @memo     Concatenates a vector to the end of this vector.

  @doc      Concatenates a vector to the end of this vector.  
  
  @param    v   Input vector.  
  
  @return   Concatenated vector.
  
*/
DIVAMatrixExport CDVector CDVector::VecCat( const CDVector &v ) const {
   
    int tlen = this->Length();
    CDVector out( tlen+v.Length() );    

    for(int i=0;i<out.Length();i++) {
        
        out[i] = i<tlen ? (*this)[i] : v[i-tlen];
    }

    return out;
}


/**

  @author   Mikkel B. Stegmann
  @version  6-6-2002

  @memo     Clamps the vector to [min,max].

  @doc      Clamps the vector to [min,max]. 

  @param    min     Minumim value.
  
  @param    max     Maximum value.
       
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::Clamp( const double min, const double max ) {


    int len = this->Length();
    double *val;

    for(int i=0;i<len;i++) {

        val = &((*this)[i]);
        *val = *val<min ? min : (*val>max ? max : *val);
    }
}


/**

  @author   Mikkel B. Stegmann
  @version  6-6-2002

  @memo     Linear alignment of this vector to another vector using the L2 norm.

  @doc      Linear alignment of this vector to another vector using the L2 norm.  
  
  @param    v   Vector that this vector is being least squares fitted to.  

  @param	a	Optional pointer to store the resulting transformation in.

  @param	b	Optional pointer to store the resulting transformation in.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::AlignTo(	const CDVector &v, 
											double *pA,
											double *pB	) {

	assert( this->Length()==v.Length() );

    int n = this->Length();
    assert(n==v.Length() );

    // calc sums
    double x, y, a, b, Sx = .0, Sy = .0, Sxx = .0, Sxy = .0;
    for(int i=0;i<n;i++) {

        x    = (*this)[i];
        y    = v[i];
        Sx  += x;
        Sy  += y;
        Sxx += x*x;
        Sxy += x*y;
    }

    // in an L2 sense, the optimal scaling and offset of this vector
    double d = n*Sxx-Sx*Sx;
    a = (n*Sxy-Sx*Sy)/d;
    b = (Sy*Sxx-Sx*Sxy)/d;           

    // apply tranformation
    for(i=0;i<n;i++) {

        (*this)[i] = a * (*this)[i] + b;
    }

	if (pA) { *pA = a; }
	if (pB) { *pB = b; }
}


/**

  @author   Mikkel B. Stegmann
  @version  6-14-2002

  @memo     Calculates the autocorrelation of the vector.

  @doc      Calculates the autocorrelation of the vector with
            a given lag (default lag is 1).
  
  @param    lag     The lag.  
  
  @return   The autocorrelation.
  
*/
DIVAMatrixExport double CDVector::AutoCorrelation( const int lag ) const {

    int n = this->Length();
    double mean = .0;
    double d = n*this->Var( &mean );    
    double s = .0;


    for(int i=0;i<n-lag;i++) {

        s += ( (*this)[i]-mean )*( (*this)[i+lag]-mean ); 
    }
    

    return s/d;
}



/**

  @author   Mikkel B. Stegmann
  @version  7-17-2002

  @memo     Shuffles (randomizes) the vector.

  @doc      Shuffles (randomizes) the vector.
  
  @see      Rand 
 
  @return   Nothing.
  
*/
DIVAMatrixExport void CDVector::Shuffle() {

    double tmp;
    int n = Length();

    int shuffle_amount = 2;

    for(int s=0;s<shuffle_amount;s++) {

        for(int i=0;i<n;i++) {

            int index = (int)(.5+(n-1)*rand()/(double)RAND_MAX); 

            // swap 'i' and 'index'
            tmp = (*this)[i];
            (*this)[i] = (*this)[index];
            (*this)[index] = tmp;
        }
    }
}




/* ************************************************************************** */
/*
 *  This Quickselect routine is based on the algorithm described in
 *  "Numerical recipes in C", Second Edition,
 *  Cambridge University Press, 1992, Section 8.5, ISBN 0-521-43108-5
 *  This code by Nicolas Devillard - 1998. Public domain.
 */


#define ELEM_SWAP(a,b) { register double t=(a);(a)=(b);(b)=t; }

double quick_select(double arr[], int n) 
{
    int low, high ;
    int median;
    int middle, ll, hh;

    low = 0 ; high = n-1 ; median = (low + high) / 2;
    for (;;) {
        if (high <= low) /* One element only */
            return arr[median] ;

        if (high == low + 1) {  /* Two elements only */
            if (arr[low] > arr[high])
                ELEM_SWAP(arr[low], arr[high]) ;
            return arr[median] ;
        }

    /* Find median of low, middle and high items; swap into position low */
    middle = (low + high) / 2;
    if (arr[middle] > arr[high])    ELEM_SWAP(arr[middle], arr[high]) ;
    if (arr[low] > arr[high])       ELEM_SWAP(arr[low], arr[high]) ;
    if (arr[middle] > arr[low])     ELEM_SWAP(arr[middle], arr[low]) ;

    /* Swap low item (now in position middle) into position (low+1) */
    ELEM_SWAP(arr[middle], arr[low+1]) ;

    /* Nibble from each end towards middle, swapping items when stuck */
    ll = low + 1;
    hh = high;
    for (;;) {
        do ll++; while (arr[low] > arr[ll]) ;
        do hh--; while (arr[hh]  > arr[low]) ;

        if (hh < ll)
        break;

        ELEM_SWAP(arr[ll], arr[hh]) ;
    }

    /* Swap middle item (in position low) back into correct position */
    ELEM_SWAP(arr[low], arr[hh]) ;

    /* Re-set active partition */
    if (hh <= median)
        low = ll;
        if (hh >= median)
        high = hh - 1;
    }
}

#undef ELEM_SWAP

/* ************************************************************************** */



/**

  @author   Mikkel B. Stegmann
  @version  12-10-2002

  @memo     Calculates the median.

  @doc      Calculates the median.
  
  @see      Trim
   
  @return   The median.
  
*/
DIVAMatrixExport double CDVector::Median() const {

	CDVector copy(*this);
	double median;
	bool doSorting = false;

	if (doSorting) {

		copy.Sort();
		const int len = copy.Length();
		const bool isOdd = len%2!=0;
		median = isOdd ? copy[len/2] : .5*(copy[len/2-1]+copy[len/2]);
	} else {
	
		// *much* faster alternative kindly provided by Nicolas Devillard
		// see code snip above
		median = quick_select( &(copy[0]), copy.Length() );
	}

	return median;
}



/**

  @author   Mikkel B. Stegmann
  @version  12-10-2002

  @memo     Removes the extreme 'percentage' part of the vector.

  @doc      Removes the extreme 'percentage' part of the vector.
			E.g. v.Trim( .10 ) removes the 5% of the lower tail and
			5% of the upper tail.

			By convention Trim( 1. ) returns the median.

  @see      TrimmedMean, TrimmedVar, TrimmedStd
  
  @param    percentage	The amount of data to strip from the vector.
  
  @return   A trimmed version of the vector.
  
*/
DIVAMatrixExport CDVector CDVector::Trim( const double percentage ) const {

	assert( percentage>=.0 && percentage<=1. );

	if ( percentage>=1. ) {

		// by convention
		CDVector v(1);
		v[0] = this->Median();
		return v;

	} else {

		// remove tails
		const int len = this->Length();
		const int head_tail_len = (int)(.5+len*percentage/2.);

		CDVector trimmedVec(*this);
		trimmedVec.Sort();
		return trimmedVec.Range( head_tail_len, len-head_tail_len-1 );
	}
}



/**

  @author   Mikkel B. Stegmann
  @version  12-10-2002

  @memo     Calculates the trimmed mean.
 
  @doc      Removes the extreme 'percentage' part of the vector and 
			calculates the mean of the remaining data.

  @see		Trim
  
  @param    percentage	The amount of data to strip from the vector.
  
  @return   The trimmed mean.
  
*/
DIVAMatrixExport double CDVector::TrimmedMean( const double percentage ) const {

	CDVector trimmedVec = this->Trim( percentage );
	return trimmedVec.Mean();
}



/**

  @author   Mikkel B. Stegmann
  @version  12-10-2002

  @memo     Calculates the trimmed variance.
 
  @doc      Removes the extreme 'percentage' part of the vector and 
			calculates the variance of the remaining data.

  @see		Trim
  
  @param    percentage	The amount of data to strip from the vector.
  
  @return   The trimmed mean.
  
*/
DIVAMatrixExport double CDVector::TrimmedVar( const double percentage ) const {

	CDVector trimmedVec = this->Trim( percentage );
	return trimmedVec.Var();
}



/**

  @author   Mikkel B. Stegmann
  @version  12-10-2002

  @memo     Calculates the trimmed standard deviation.
 
  @doc      Removes the extreme 'percentage' part of the vector and 
			calculates the standard deviation of the remaining data.

  @see		Trim
  
  @param    percentage	The amount of data to strip from the vector.
  
  @return   The trimmed mean.
  
*/
DIVAMatrixExport double CDVector::TrimmedStd( const double percentage ) const {

	CDVector trimmedVec = this->Trim( percentage );
	return trimmedVec.Std();
}
