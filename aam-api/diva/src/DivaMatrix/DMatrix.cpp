/* $Id: DMatrix.cpp,v 1.1.1.1 2003/01/03 19:18:16 aam Exp $ */
#include "DIVAMatrix.h"
#include "DMatrix.h"
#include "DTokenFile.h"
#include <fstream.h>
#include <stdlib.h>
#include <Afxtempl.h>


/**

  @author   Rune Fisker
  @version  3-14-2001

  @memo     Assignment operator.

  @doc      Assignment operator (CVisDMatrix). 
  
  @param    mat     Input matrix.
  
  @return   This.
  
*/
DIVAMatrixExport CDMatrix& CDMatrix::operator=(const CVisDMatrix &mat) {
	
	CVisDMatrix& mTmp = *this; mTmp = mat; return *this; 
}


/**

  @author   Rune Fisker
  @version  3-14-2001

  @memo     Assignment operator.

  @doc      Assignment operator (CDMatrix). 
  
  @param    mat     Input matrix.
  
  @return   This.
  
*/
DIVAMatrixExport CDMatrix& CDMatrix::operator=(const CDMatrix &mat) {
	
	CVisDMatrix& mTmp = *this; mTmp = mat; return *this; 
}


/**

  @author   Rune Fisker
  @version  3-14-2001

  @memo     Assignment operator.

  @doc      Assignment operator (double). Set all values equal to 'value'.
  
  @param    value     Input double.
  
  @return   This.
  
*/
DIVAMatrixExport CDMatrix& CDMatrix::operator=(double value) {
	
	CVisDMatrix& mTmp = *this; mTmp = value; return *this; 
}


/**

  @author   Rune Fisker
  @version  3-14-2001

  @memo     Calc the sum of each column into a vector.

  @doc      Calc the sum of each column into a vector. 
            The vector will be resized if it doesn't have the right length.
  
  @param    vSum     Output vector.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::SumCol(CDVector& vSum) const
{
    if ( vSum.Length()!=NCols() ) {

        vSum.Resize( NCols() );          
    }

	vSum = 0;
	for (int r=0;r<NRows();r++)
	{
		for (int c=0;c<NCols();c++)
		{
			vSum[c] += (*this)[r][c];

		}
	}
}


/**

  @author   Rune Fisker
  @version  3-14-2001

  @memo     Calc the mean of each column into a vector.

  @doc      Calc the mean of each column into a vector.
            The vector will be resized if it doesn't have the right length.
  
  @param    vMean     Output vector.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::MeanCol(CDVector& vMean) const
{
    if ( vMean.Length()!=NCols() ) {

        vMean.Resize( NCols() );          
    }
	SumCol(vMean);

	vMean /= NRows();
}


/**

  @author   Rune Fisker
  @version  3-14-2001

  @memo     Calc the variance of each column into a vector.

  @doc      Calc the variance of each column into a vector.
            The vector will be resized if it doesn't have the right length.
  
  @param    vVar     Output vector.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::VarCol(CDVector& vVar) const
{
	assert(NCols() == vVar.Length());

	CDVector vCol(NRows());

	for (int c=0;c<NCols();c++)
	{
		Col(c,vCol);
		vVar[c] = vCol.Var();
	}
}


/**

  @author   Rune Fisker
  @version  3-14-2001

  @memo     Calc the standard deviation of each column into a vector.

  @doc      Calc the standard deviation of each column into a vector.
            The vector will be resized if it doesn't have the right length.
  
  @param    vStd     Output vector.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::StdCol(CDVector& vStd) const
{
	VarCol(vStd);

	vStd.Sqrt();
}


/**

  @author   Rune Fisker
  @version  3-14-2001

  @memo     Calc the total sum of the matrix.

  @doc      Calc the total sum of the matrix.              
  
  @return   The sum.
  
*/
DIVAMatrixExport double CDMatrix::Sum() const
{
	CDVector vTmp(NCols());

	SumCol(vTmp);

	return vTmp.Sum();
}


/**

  @author   Rune Fisker
  @version  3-14-2001

  @memo     Calcs the mean value of the matrix.

  @doc      Calcs the mean value of the matrix.               
  
  @return   The mean value.
  
*/
DIVAMatrixExport double CDMatrix::Mean() const
{
	return Sum()/(NRows()*NCols());
}


/**

  @author   Rune Fisker
  @version  3-14-2001

  @memo     Calc the variance of the matrix.

  @doc      Calc the variance of the matrix.               
  
  @return   The variance value.
  
*/
DIVAMatrixExport double CDMatrix::Var() const
{
	double dMean = Mean(), dVar = 0;

	for (int r=0;r<NRows();r++)
	{
		for (int c=0;c<NCols();c++)
		{
			dVar += pow((*this)[r][c] - dMean,2);
		}
	}

	return dVar/(NRows()*NCols());
}


/**

  @author   Rune Fisker
  @version  3-14-2001

  @memo     Calc the standard diviation of the matrix.

  @doc      Calc the standard diviation of the matrix.               
  
  @return   The standard diviation  value.
  
*/
DIVAMatrixExport double CDMatrix::Std() const
{
	return sqrt(Var());
}


/**

  @author   Rune Fisker
  @version  3-14-2001

  @memo     One-way analysis of variance (ANOVA).

  @doc      Determination of the fluctuations observed 
            in a sample, and their dependencies in the 
            form of a one-way analysis of variance
            (ANOVA).

  @param    dZ 
  @param    nDFModel
  @param    nDFError
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::OneWayANOVA(double& dZ, int& nDFModel, int& nDFError) const
{
	int nN = NRows()*NCols();
	int nK = NCols();

	// calc. variance within groups
	CDVector vVarCol(NCols());
	VarCol(vVarCol);

	double dSSError = NRows()*vVarCol.Sum();

	// calc. variance between groups
	CDVector vMeanCol(NCols()),vMean(NCols());
	vMean = Mean();
	MeanCol(vMeanCol);

	vMean -= vMeanCol;
	vMean.Sqr();

	double dSSModel = NRows()*vMean.Sum();

	// calc. degrees of freedom
	nDFError = nN - nK;
	nDFModel = nK - 1;

	// calc. test 
	dZ = (dSSModel/nDFModel)/(dSSError/nDFError);
}


/**

  @author   Rune Fisker
  @version  3-14-2001

  @memo     Student's T-test.

  @doc      Student's T-test.

  @param    iCol1 
  @param    iCol2
  @param    dZ
  @param    nDF
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::TTest( const int iCol1, const int iCol2, 
                                       double& dZ, int& nDF ) const
{
	assert(iCol1 < NCols());
	assert(iCol2 < NCols());

	// calc. column mean
	CDVector vMean(NCols());
	MeanCol(vMean);

	// degress of freedom
	nDF = NCols()*NRows() - NCols();

	// calc. pooled variance for each gruop of obs. (MSE)
	CDVector vVarCol(NCols());
	VarCol(vVarCol);

	double dSSError = NRows()*vVarCol.Sum();
	double dRootMSE = sqrt(dSSError/nDF);
	
	// calc. test
	double dTop = vMean[iCol1] - vMean[iCol2];
	double dBottom = dRootMSE*sqrt(2.0/NRows());
	
	dZ = dTop/dBottom;
}


/**

  @author   Mikkel B. Stegmann
  @version  3-14-2001

  @memo     Return the i-th row of the matrix.            

  @doc      Return the i-th row of the matrix.
            The vector will be resized if it doesn't have the right length.

            Notice that due to the row major nature of matrices, row
            read/writes are *much* faster than col read/writes.

            Alternatively, one could use the more costly
            CVisDVector CVisDMatrix::Row(int r) method.                        

            To set the i-th row use:
            void CVisDMatrix::SetRow(int r, const CVisDVector &v)
  
  @param    i       The row number.
  @param    vRow    Output vector;
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::Row(int i, CDVector& vRow) const
{
    int nc = NCols();

    if( nc!= vRow.Length() ) {

        vRow.Resize( nc );
    }

    // assumes that matrices are row major
    memcpy(&vRow[0], &(*this)[i][0], nc*sizeof(double));	
}


/**

  @author   Rune Fisker
  @version  3-14-2001

  @memo     Return the i-th column of the matrix.            

  @doc      Return the i-th column of the matrix.
            The vector will be resized if it doesn't have the right length.

            Alternatively, one could use the more costly
            CVisDVector CVisDMatrix::Column(int r) method. 
            
            To set the i-th col use:
            void CVisDMatrix::SetColumn(int c, const CVisDVector &v)
  
  @param    i       The column number.
  @param    vCol    Output vector;
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::Col(int i, CDVector& vCol) const
{
	int nr = NRows();

    if( nr!= vCol.Length() ) {

        vCol.Resize( nr );
    }

	for (int r=0;r<nr;r++)
	{
		vCol[r] = (*this)[r][i];
	}
}


/**

  @author   Rune Fisker
  @version  3-14-2001

  @memo     Make the matrix a diagonal matrix containing 'vec'.            

  @doc      This function transforms the matrix into a diagonal matrix,
            with the values of 'vec' in the diagonal.
  
  @param    vec     Vector to place in the diagonal.  
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::Diag(const CDVector &vec)
{
    // The maximum amount of values it is posible to transfer.
	int stop=min(min(NRows(),NCols()),vec.Length() );

	*this = 0.0;
	for(int i=0;i<stop;i++)
	{
		(*this)[i][i]=vec[i];
	}
}


/**

  @author   Rune Fisker
  @version  3-14-2001

  @memo     Returns a string representing the matrix.          

  @doc      Returns a string representing the matrix. 
  
  @return   A string.
  
*/
DIVAMatrixExport CDString CDMatrix::ToString() const
{
	CDString strOut = "";

	CDString strTmp;

	for(int i=0;i<NRows();i++)
	{
		for(int j=0;j<NCols();j++)
		{
			strTmp.Format("%8.3f ",(*this)[i][j]);
			strOut += strTmp;
		}
		strOut += "\r\n";
	}

    return strOut;
}


/**

  @author   Rune Fisker
  @version  3-14-2001

  @memo     Composes the matrix of Top and Bottom (on top of each other).

  @doc      Composes the matrix of Top and Bottom (on top of each other).

  @param    Top     Matrix to be placed in the top of this matrix.
  @param    Bottom  Matrix to be placed in the bottom of this matrix.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::CombVert(CVisDMatrix &Top, CVisDMatrix &Bottom)
{
	assert(Top.NCols()==Bottom.NCols());
	assert(Top.NCols()==(*this).NCols());
	assert(Top.NRows()+Bottom.NRows()==(*this).NRows());

	int j,i;

	for(j=0;j<Top.NRows();j++)
	{
		for(i=0;i<(*this).NCols();i++)
		{
			(*this)[j][i]=Top[j][i];
		}
	}
	for(;j<(*this).NRows();j++)
	{
		for(i=0;i<(*this).NCols();i++)
		{
			(*this)[j][i]=Bottom[j-Top.NRows()][i];
		}
	}
}


/**

  @author   Henrik Aan�s
  @version  14-7-1999

  @memo     Writes the matrix to disk in MatLab (.m) format.

  @doc      Writes the matrix to disk in MatLab (.m) format.
            To read the matrix into MatLab write e.g. 
            'my_matrix.m' at the MatLab prompt.  

            Notice that this should be used for storage a (really)
            large matrices, due to the computational and i/o overhead
            induced by the simple MatLab text format.

            Also, remember that MatLab can't read (.m) files with lines
            longer than 4096 bytes.

            If no communication with MatLab is needed, but merely 
            to/from disk functionality within a DIVA program, it is
            suggested to use the fast binary i/o methods ToFile() and
            FromFile().      
  
  @param    sFilename   Output file name. Should have the extension '.m'.

  @param    sName       Name of destination matlab variable.

  @param    sComment    Optional comment inside the file.

  @param    fAppend     If true, the vector is appended to the file 
                        'sFilename'.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::ToMatlab(   const CDString& sFilename,
							                const CDString& sName,
							                const CDString& sComment,
							                bool fAppend) const
{
	CDString		strTmp;
	if(fAppend)
	{
		ofstream	fstreamOut(sFilename,ios::app);	//open stream

		if(sComment!="")
		{
			fstreamOut	<<	"%";
			fstreamOut	<<	sComment;
			fstreamOut	<<	"\n";

		}

		fstreamOut	<<	sName;						//Write the name of the matrix.
		fstreamOut	<<	"=[\n";

		//Writes the matrix contents to sToStr.
		for(int i=0;i<NRows();i++)
		{
			for(int j=0;j<NCols();j++)
			{
				strTmp.Format("%6.6g ",(*this)[i][j]);
				fstreamOut	<<  strTmp;
			}
			fstreamOut	<<	";\n";
		}
		fstreamOut	<<	"];\n"; 
	}
	else
	{
		ofstream	fstreamOut(sFilename,ios::trunc);	//open stream

		if(sComment!="")
		{
			fstreamOut	<<	"%";
			fstreamOut	<<	sComment;
			fstreamOut	<<	"\n";

		}

		
		fstreamOut	<<	sName;						//Write the name of the matrix.
		fstreamOut	<<	"=[\n";

		//Writes the matrix contents to sToStr.
		for(int i=0;i<NRows();i++)
		{
			for(int j=0;j<NCols();j++)
			{
				strTmp.Format("%6.6g ",(*this)[i][j]);
				fstreamOut	<<  strTmp;
			}
			fstreamOut	<<	";\n";
		}
		fstreamOut	<<	"];\n"; 
	}

}


/**

  @author   Rune Fisker
  @version  5-1-1999

  @memo     Takes the square root of each element.

  @doc      Takes the square root of each element.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::Sqrt()
{
	for(int j=0;j<(*this).NRows();j++)
	{
		for(int i=0;i<(*this).NCols();i++)
		{
			(*this)[j][i] = sqrt((*this)[j][i]);
		}
	}

}


/**

  @author   Rune Fisker
  @version  5-1-1999

  @memo     Takes the power of two of each element.

  @doc      Takes the power of two of each element.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::Sqr()
{
	for (int r=0;r<NRows();r++)
	{
		for (int c=0;c<NCols();c++)
		{
			(*this)[r][c] *= (*this)[r][c];
		}
	}
}


/**

  @author   Rune Fisker
  @version  5-1-1999

  @memo     Element-wise matrix multiply.

  @doc      Multiply each element in the matrix with 
            the corresponding element of the input matrix.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::ElementMultiply(const CDMatrix& matrix)
{
	assert(NCols() == matrix.NCols());
	assert(NRows() == matrix.NRows());

	for (int r=0;r<NRows();r++)
	{
		for (int c=0;c<NCols();c++)
		{
			(*this)[r][c] *= matrix[r][c];
		}
	}
}


/**

  @author   Rune Fisker
  @version  5-1-1999

  @memo     Element-wise matrix division.

  @doc      Divide each element in the matrix with 
            the corresponding element of the input matrix.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::ElementDivide(const CDMatrix& matrix)
{
	assert(NCols() == matrix.NCols());
	assert(NRows() == matrix.NRows());

	for (int r=0;r<NRows();r++)
	{
		for (int c=0;c<NCols();c++)
		{
			(*this)[r][c] /= matrix[r][c];
		}
	}
}


/**

  @author   Henrik Aan�s
  @version  14-7-1999

  @memo     Reads a matrix from disk in MatLab (.m) format.

  @doc      Reads a matrix from disk in MatLab (.m) format into
            'this'. 

            Notice that this should be used for storage a (really)
            large matrices, due to the computational and i/o overhead
            induced by the simple MatLab text format.

            Also, remember that MatLab can't read (.m) files with lines
            longer than 4096 bytes.

            If no communication with MatLab is needed, but merely 
            to/from disk functionality within a DIVA program, it is
            suggested to use the fast binary i/o methods ToFile() and
            FromFile().                        
  
  @param    sFilename   Input file name.

  @param    sName       The name to search for (and load) inside the matlab file.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::FromMatlab(const CDString& sFilename,const CDString& sName)
{
	CDTokenFile	DIVABuf;					//Buffer or stream to the file.
	CDString		sTokenString;				//String for retriving tokings, or element entities from DIVAbuf.
	bool		bFound=false;				//Indicator for the soughtafter matrix has been found.
	CArray<double,double>	Entry;			//Container for the entries or numbers untile size om matrix is known.
	int			cCols=0;					//Number of columns in the matrix.
	int			cRows=0;					//Number of rows in the matrix.
	int			cEntry=0;					//Counter for wich entry is to be writen to the matrix.

	//Open stream/buffer.
	if(! DIVABuf.Open(sFilename,CFile::modeRead,NULL))
	{
		throw CVisError("The requested file does not exist.",eviserrorUnknown,"FromMatlab","DMatrixBasic.cpp.cpp", __LINE__);
	}


	//Find the sought after matrix or fail.
	do{
		if(!DIVABuf.NextToken(sTokenString))
		{
			throw CVisError("The requested variable is not in the requested file.",eviserrorUnknown,"FromMatlab","DMatrixBasic.cpp.cpp", __LINE__);
		}
	}while(sTokenString!=sName+"=[");


	//Parse the first row of the matrix, and there by retrive the number of columns.
	//Get a token from the stream.
	if(!DIVABuf.NextToken(sTokenString))
	{
		throw CVisError("Error reading from file.",eviserrorUnknown,"FromMatlab","DMatrixBasic.cpp.cpp", __LINE__);
	}
	while(sTokenString!=";")				//";" marks the end of a line.
	{
		Entry.Add(atof(LPCTSTR(sTokenString)));		
		//Get a token from the stream.
		if(!DIVABuf.NextToken(sTokenString))
		{
			throw CVisError("Error reading from file.",eviserrorUnknown,"FromMatlab","DMatrixBasic.cpp.cpp", __LINE__);
		}
		cCols++;
	}

	//Parse the rest of the matrix 
	while(sTokenString!="];")				//"];" marks the end of a matrix.
	{
		if(sTokenString==";")				//";" marks the end of a line.
		{
			cRows++;
		}
		else
		{
			Entry.Add(atof(LPCTSTR(sTokenString)));
		}
		//Get a token from the stream.
		if(!DIVABuf.NextToken(sTokenString))
		{
			throw CVisError("Error reading from file.",eviserrorUnknown,"FromMatlab","DMatrixBasic.cpp.cpp", __LINE__);
		}
	}

	//Adjust the size of the (output)matrix.
	Resize(cRows,cCols);

	//Transfere the values.
	for(int cI=0;cI<cRows;cI++)
	{
		for(int cJ=0;cJ<cCols;cJ++)
		{
			(*this)[cI][cJ]=Entry[cEntry];
			cEntry++;
		}
	}
}


/**

  @author   Henrik Aan�s
  @version  7-22-1999

  @memo     Writes the matrix to disk in binary format.

  @doc      Writes the matrix to disk in binary format.
            The dimensions are saved as two doubles (!!?)
            (rows,cols) in the start.           
            
            Modified: Mikkel B. Stegmann 14/3-00
  
  @param    sFilename   Input file name.  

  @see      FromFile
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::ToFile(const CDString & sFilename) const
{
	FILE *Stream;					//Stream for output.
	Stream=fopen(sFilename,"wb");								//Open the stream.
	if(Stream==NULL)
	{
		throw CVisError("Can not open file.",eviserrorOpen,"ToFile","DMatrixBasic.cpp", __LINE__);
	}

	ToFile( Stream );

	fclose( Stream );											//Close the stream.
}


/**

  @author   Mikkel B. Stegmann
  @version  4-14-1999

  @memo     Writes the matrix to disk in binary format.

  @doc      Writes the matrix to disk in binary format.
            The dimensions are saved as two doubles (!!?)
            (rows,cols) in the start.  
            The matrix is written to the binary file 'fh' 
            at the current position of the file pointer.
            
            Modified: Mikkel B. Stegmann 14/3-00
  
  @param    fh   Open file handle.

  @see      FromFile
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::ToFile( FILE *fh ) const
{
	

	FILE *Stream;					//Stream for output.
	int cIndex=2;					//Counter for elements in the matrix.
	int cSize=NCols()*NRows()+2;	//The number of elements to save.
	double*Elem;					//Pointer to all the element to save.

	assert( fh!=NULL );
	Stream = fh;

	Elem=(double*)malloc(cSize*sizeof(double));					//Allocate memory to Elem.
	if(Elem==NULL)
	{
		throw CVisError("Out of memory.",eviserrorMemory,"ToFile","DMatrixBasic.cpp", __LINE__);
	}

	Elem[0]=NRows();											//Save the dimensions of the matrix.
	Elem[1]=NCols();

	for(int cI=0;cI<NRows();cI++)								//Write the elements of the matrix to Elem.
	{
		for(int cJ=0;cJ<NCols();cJ++)
		{
			Elem[cIndex]=(*this)[cI][cJ];
			cIndex++;
		}
	}
	
	if(fwrite((void*)Elem,sizeof(double),cSize,Stream) < cSize) //Save the data.
	{
		throw CVisError("Problem writing to file.",eviserrorWrite,"ToFile","DMatrixBasic.cpp", __LINE__);
	}
	
	free(Elem);													//Free memory.
}


/**

  @author   Mikkel B. Stegmann
  @version  3-14-2000

  @memo     Reads a matrix from disk in binary format.

  @doc      Reads a matrix from disk in binary format, as
            written from CDMatrix::ToFile().                                   

            The matrix is resizew if it doesn't fit the
            disk matrix.
  
  @param    sFilename   Input file name.  

  @see      ToFile
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::FromFile(const CDString & sFilename)
{		
	FILE *Stream;				//Stream for output.

	Stream=fopen(sFilename,"rb");										//Open the stream.
	if(Stream==NULL)
	{
		throw CVisError("Can not open file.",eviserrorOpen,"FromFile","DMatrixBasic.cpp", __LINE__);
	}

	FromFile( Stream );

	fclose( Stream );													//Close the stream.
}


/**

  @author   Mikkel B. Stegmann
  @version  3-14-2000

  @memo     Reads a matrix from disk in binary format.

  @doc      Reads a matrix from disk in binary format (from 
            the current position of the file pointer), as
            written from CDMatrix::ToFile().                                   

            The matrix is resizew if it doesn't fit the
            disk matrix.
  
  @param    fh   Open file handle.

  @see      ToFile
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::FromFile( FILE *fh )
{

	FILE *Stream;				//Stream for output.
	int cIndex=0;				//Counter for elements in the matrix.
	double Dim[2];				//The dimensions of the loaded matrix.
	double* Elem;				//Pointer to the elements the matrix.

	assert( fh!=NULL );
	Stream = fh;

	if(fread((void*)Dim,sizeof(double),2,Stream) <2 )					//Retrive the dimensions.
	{
		throw CVisError("Problem reading from file.",eviserrorWrite,"FromFile","DMatrixBasic.cpp", __LINE__);
	}

	Resize(Dim[0],Dim[1]);												//Resize the matrix to fit the loaded data.

	Elem=(double*)malloc(Dim[0]*Dim[1]*sizeof(double));					//Allocate memory to retrive the data.
	if(Elem==NULL)
	{
		throw CVisError("Out of memory.",eviserrorMemory,"FromFile","DMatrixBasic.cpp", __LINE__);
	}

	if(fread((void*)Elem,sizeof(double),Dim[0]*Dim[1],Stream) < Dim[0]*Dim[1] ) //Retrive the data.
	{
		throw CVisError("Problem reading from file.",eviserrorWrite,"FromFile","DMatrixBasic.cpp", __LINE__);
	}
	

	for(int cI=0;cI<NRows();cI++)										//Put the elements into the matrix.
	{
		for(int cJ=0;cJ<NCols();cJ++)
		{
			(*this)[cI][cJ]=Elem[cIndex];
			cIndex++;
		}
	}

	free(Elem);															//Free allocated memory.
}


/**

  @author   Rune Fisker
  @version  5-1-1999

  @memo     Forms the Kronecker tensor product of two matrices.

  @doc      Forms the Kronecker tensor product of two matrices.
            The result is placed in this.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::Kron(CDMatrix &mX, CDMatrix &mY)
{
	Resize(mX.NRows()*mY.NRows(),mX.NCols()*mY.NCols());

	for(int cI=0;cI<NRows();cI++)
	{
		for(int cJ=0;cJ<NCols();cJ++)
		{
			(*this)[cI][cJ]=    mX[cI/mY.NRows()][cJ/mY.NCols()] *
				                mY[cI%mY.NRows()][cJ%mY.NCols()];
		}
	}
}


/**

  @author   Rune Fisker
  @version  5-1-1999

  @memo     Converts the matrix in the identity matrix.

  @doc      Converts the matrix in the identity matrix -
            i.e. zeros all-over except the ones in the diagonal.            
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::Eye()
{
	(*this) = 0;

    int t = NRows()<NCols() ? NRows() : NCols();
    
	for (int r=0;r<t;r++)
	{
		(*this)[r][r] = 1;
	}
}



/**

  @author   Mikkel B. Stegmann
  @version  4-5-2000

  @memo     Writes the matrix in LaTeX format. Throws: CVisError.

  @doc      Writes the matrix in LaTeX format. Throws: CVisError.

  @param    sFilename       Destination file name.
  @param    sVarName        Name of matrix.
  @param    sComment        Comment in the LaTeX file.
  @param    fWriteAsTable   If true the matrix is formatted as a table.
  @param    sNumFmt         printf c-stype format of numbers, e.g. "%.2f".
  @param    sBracketType    Either "[" or "(".
  @param    fAppend         If true 'sFilename' is append with this matrix.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::ToLaTeX( const CDString& sFilename, 
							 const CDString& sVarName, 
							 const CDString& sComment, 
							 bool fWriteAsTable,
							 const CDString& sNumFmt,
							 const CDString& sBracketType,
							 bool fAppend ) const {

	FILE *fh;
	CDString fmtStr;	

	assert( sBracketType=="[" || sBracketType=="(" );

	CDString bracketRight = sBracketType=="[" ? "]" : ")";

	// open file
	fh = fopen( sFilename, fAppend ? "a" : "w" );
	assert(fh!=NULL);
	if ( fh==NULL ) {

		throw CVisError("Problem opening file.",eviserrorUnknown,"FromFile","ToLaTex() DMatrixBasic.cpp", __LINE__);
		return;
	}

	// write header
	if (sComment!="") fprintf( fh, "%% %s\n", sComment );
	if ( fWriteAsTable ) {


		fprintf( fh, "\\begin{table}\\begin{center}\n" );
		fprintf( fh, "\t\\begin{tabular}{" );		
		for(int r=0;r<NCols();r++) fprintf( fh, "|r" );	
		fprintf( fh, "|}\n\t\\hline\n");

	} else {
		
		fprintf( fh, "\\begin{equation}\n" );
		fprintf( fh, "\t\\mathbf{%s} = \n", sVarName );
		fprintf( fh, "\t\t\\left%s  \\begin{array}{", sBracketType );		
		for(int r=0;r<NCols();r++) fprintf( fh, "r" );	
		fprintf( fh, "}\n");
	}
	
	// write data
	fmtStr.Format( "%%s %s ", sNumFmt );
	for(int r=0;r<NRows();r++) {

		fprintf( fh, "\t\t");
		for(int c=0;c<NCols();c++) {

			fprintf( fh, fmtStr, c==0 ? "" : "&", (*this)[r][c] );
		}
		fprintf( fh, "\\\\ \n");
	}

	// write footer
	if ( fWriteAsTable ) {

		fprintf( fh, "\t\\hline\n\t\\end{tabular}\\end{center}\n" ); 
		fprintf( fh, "\t\\caption{%s}\n", sComment=="" ? sVarName : sComment );
		fprintf( fh, "\\end{table}\n" ); 

	} else {
	
		fprintf( fh, "\t\\end{array} \\right%s\n", bracketRight ); 
		fprintf( fh, "\\end{equation}\n" );
	}

	// close file
	if ( 0!=fclose( fh ) ) {

		throw CVisError("Problem closing file.",eviserrorUnknown,"FromFile","ToLaTex() DMatrixBasic.cpp", __LINE__);
	}

}


/**

  @author   Lars Pedersen
  @version  5-12-2000

  @memo     Extract upper triangular part of matrix.

  @doc      Extracts the elements on and above the K-th diagonal. 
            K = 0 is the main diagonal, K > 0 is above the main 
            diagonal and K < 0 is below the main diagonal.

  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::TriU( CDMatrix &matrix, const int K ) const
{

	assert( (NRows()==matrix.NRows()) & (NCols()==matrix.NCols()) );
	
	matrix = *this; // copy this matrix
	
	int M = NRows();
	int N = NCols();

	for (int r = 0; r < M; r++) { // for each row
		for (int c = 0; (c < r - K) & (c < N); c++) { // for each column from left (0) to the diagonal - 1 
			matrix[r][c] = 0;             // is set to zero
		}
	}
}


/**

  @author   Lars Pedersen
  @version  6-12-2000

  @memo     Extract lower triangular part of matrix.

  @doc      Extracts the elements on and below the K-th diagonal. 
            K = 0 is the main diagonal, K > 0 is above the main 
            diagonal and K < 0 is below the main diagonal.

  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::TriL( CDMatrix &matrix, const int K ) const
{

	assert( (NRows()==matrix.NRows()) & (NCols()==matrix.NCols()) );

	matrix = *this; // copy this matrix

	int M = NRows();
	int N = NCols();

	for (int r = 0; r < M; r++) { // for each row
		for (int c = r + K + 1; c < N; c++) { // for each column from the diagonal + 1 to right (N)
			matrix[r][c] = 0;                 // is set to zero
		}
	}
}


/**

  @author   Lars Pedersen
  @version  6-12-2000

  @memo     Element wise equal to. Comparison of a matrix and a double.

  @doc      Compares each element in this matrix (A) with double B. 
            Result matrix C has elements with values 1 or 0.

            C(i,j) = 1 if A(i,j) == B
            C(i,j) = 0 else 

  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::Eq(const double B, CDMatrix& C ) const
{
	// CHECK: A and C must have same dimensions
	assert((NRows() == C.NRows()) & (NCols() == C.NCols()));

	int n = NRows();
	int m = NCols();

	for (int r = 0; r < n; r++)
		for (int c = 0; c < m; c++)
			C[r][c] = ((*this)[r][c] == B) ? 1 : 0;
}


/**

  @author   Lars Pedersen
  @version  7-12-2000

  @memo     Element wise equal to.  Comparison of two matrices.

  @doc      Compares each element in this matrix (A) with 
            corresponding element in B. 
            Result matrix C has elements with values 1 or 0.

            C(i,j) = 1 if A(i,j) == B(i,j)
            C(i,j) = 0 else 

  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::Eq( const CDMatrix& B, CDMatrix& C ) const
{
	// CHECK: A, B and C must have same dimensions
	assert((NRows() == B.NRows()) & (NCols() == B.NCols()));
	assert((NRows() == C.NRows()) & (NCols() == C.NCols()));

	int n = NRows();
	int m = NCols();

	for (int r = 0; r < n; r++)
		for (int c = 0; c < m; c++)
			C[r][c] = ((*this)[r][c] == B[r][c]) ? 1 : 0;
}


/**

  @author   Lars Pedersen
  @version  7-12-2000

  @memo     Element wise not equal to. Comparison of a matrix and a double.

  @doc      Compares each element in this matrix (A) with double B. 
            Result matrix C has elements with values 1 or 0.

            C(i,j) = 1 if A(i,j) != B
            C(i,j) = 0 else 

  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::Ne(const double B, CDMatrix& C ) const
{
	// CHECK: A and C must have same dimensions
	assert((NRows() == C.NRows()) & (NCols() == C.NCols()));

	int n = NRows();
	int m = NCols();

	for (int r = 0; r < n; r++)
		for (int c = 0; c < m; c++)
			C[r][c] = ((*this)[r][c] != B) ? 1 : 0;
}


/**

  @author   Lars Pedersen
  @version  7-12-2000

  @memo     Element wise not equal to. Comparison of two matrices.

  @doc      Compares each element in this matrix (A) with 
            corresponding element in B. 
            Result matrix C has elements with values 1 or 0.

            C(i,j) = 1 if A(i,j) != B(i,j)
            C(i,j) = 0 else 

  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::Ne( const CDMatrix& B, CDMatrix& C ) const
{
	// CHECK: A, B and C must have same dimensions
	assert((NRows() == B.NRows()) & (NCols() == B.NCols()));
	assert((NRows() == C.NRows()) & (NCols() == C.NCols()));

	int n = NRows();
	int m = NCols();

	for (int r = 0; r < n; r++)
		for (int c = 0; c < m; c++)
			C[r][c] = ((*this)[r][c] != B[r][c]) ? 1 : 0;
}


/**

  @author   Lars Pedersen
  @version  7-12-2000

  @memo     Element wise less than. Comparison of a matrix and a double.

  @doc      Compares each element in this matrix (A) with double B. 
            Result matrix C has elements with values 1 or 0.

            C(i,j) = 1 if A(i,j) < B
            C(i,j) = 0 else 

  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::Lt(const double B, CDMatrix& C ) const
{
	// CHECK: A and C must have same dimensions
	assert((NRows() == C.NRows()) & (NCols() == C.NCols()));

	int n = NRows();
	int m = NCols();

	for (int r = 0; r < n; r++)
		for (int c = 0; c < m; c++)
			C[r][c] = ((*this)[r][c] < B) ? 1 : 0;
}


/**

  @author   Lars Pedersen
  @version  7-12-2000

  @memo     Element wise less than. Comparison of two matrices.

  @doc      Compares each element in this matrix (A) with 
            corresponding element in B. 
            Result matrix C has elements with values 1 or 0.

            C(i,j) = 1 if A(i,j) < B(i,j)
            C(i,j) = 0 else 

  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::Lt( const CDMatrix& B, CDMatrix& C ) const
{
	// CHECK: A, B and C must have same dimensions
	assert((NRows() == B.NRows()) & (NCols() == B.NCols()));
	assert((NRows() == C.NRows()) & (NCols() == C.NCols()));

	int n = NRows();
	int m = NCols();

	for (int r = 0; r < n; r++)
		for (int c = 0; c < m; c++)
			C[r][c] = ((*this)[r][c] < B[r][c]) ? 1 : 0;
}


/**

  @author   Lars Pedersen
  @version  7-12-2000

  @memo     Element wise less than or equal. Comparison of a matrix and a double.

  @doc      Compares each element in this matrix (A) with double B. 
            Result matrix C has elements with values 1 or 0.

            C(i,j) = 1 if A(i,j) <= B
            C(i,j) = 0 else 

  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::Le(const double B, CDMatrix& C ) const
{
	// CHECK: A and C must have same dimensions
	assert((NRows() == C.NRows()) & (NCols() == C.NCols()));

	int n = NRows();
	int m = NCols();

	for (int r = 0; r < n; r++)
		for (int c = 0; c < m; c++)
			C[r][c] = ((*this)[r][c] <= B) ? 1 : 0;
}


/**

  @author   Lars Pedersen
  @version  7-12-2000

  @memo     Element wise less than or equal. Comparison of two matrices.

  @doc      Compares each element in this matrix (A) with 
            corresponding element in B. 
            Result matrix C has elements with values 1 or 0.

            C(i,j) = 1 if A(i,j) <= B(i,j)
            C(i,j) = 0 else 

  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::Le( const CDMatrix& B, CDMatrix& C ) const
{
	// CHECK: A, B and C must have same dimensions
	assert((NRows() == B.NRows()) & (NCols() == B.NCols()));
	assert((NRows() == C.NRows()) & (NCols() == C.NCols()));

	int n = NRows();
	int m = NCols();

	for (int r = 0; r < n; r++)
		for (int c = 0; c < m; c++)
			C[r][c] = ((*this)[r][c] <= B[r][c]) ? 1 : 0;
}


/**

  @author   Lars Pedersen
  @version  7-12-2000

  @memo     Element wise greater than. Comparison of a matrix and a double.

  @doc      Compares each element in this matrix (A) with double B. 
            Result matrix C has elements with values 1 or 0.

            C(i,j) = 1 if A(i,j) > B
            C(i,j) = 0 else 

  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::Gt(const double B, CDMatrix& C ) const
{
	// CHECK: A and C must have same dimensions
	assert((NRows() == C.NRows()) & (NCols() == C.NCols()));

	int n = NRows();
	int m = NCols();

	for (int r = 0; r < n; r++)
		for (int c = 0; c < m; c++)
			C[r][c] = ((*this)[r][c] > B) ? 1 : 0;
}


/**

  @author   Lars Pedersen
  @version  7-12-2000

  @memo     Element wise greater than. Comparison of two matrices.

  @doc      Compares each element in this matrix (A) with 
            corresponding element in B. 
            Result matrix C has elements with values 1 or 0.

            C(i,j) = 1 if A(i,j) > B(i,j)
            C(i,j) = 0 else  

  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::Gt( const CDMatrix& B, CDMatrix& C ) const
{
	// CHECK: A, B and C must have same dimensions
	assert((NRows() == B.NRows()) & (NCols() == B.NCols()));
	assert((NRows() == C.NRows()) & (NCols() == C.NCols()));

	int n = NRows();
	int m = NCols();

	for (int r = 0; r < n; r++)
		for (int c = 0; c < m; c++)
			C[r][c] = ((*this)[r][c] > B[r][c]) ? 1 : 0;
}


/**

  @author   Lars Pedersen
  @version  7-12-2000

  @memo     Element wise greater than or equal. Comparison of a matrix and a double.

  @doc      Compares each element in this matrix (A) with double B. 
            Result matrix C has elements with values 1 or 0.

            C(i,j) = 1 if A(i,j) >= B
            C(i,j) = 0 else 

  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::Ge(const double B, CDMatrix& C ) const
{
	// CHECK: A and C must have same dimensions
	assert((NRows() == C.NRows()) & (NCols() == C.NCols()));

	int n = NRows();
	int m = NCols();

	for (int r = 0; r < n; r++)
		for (int c = 0; c < m; c++)
			C[r][c] = ((*this)[r][c] >= B) ? 1 : 0;
}


/**

  @author   Lars Pedersen
  @version  7-12-2000

  @memo     Element wise greater than or equal. Comparison of two matrices.

  @doc      Compares each element in this matrix (A) with 
            corresponding element in B. 
            Result matrix C has elements with values 1 or 0.

            C(i,j) = 1 if A(i,j) <= B(i,j)
            C(i,j) = 0 else 

  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::Ge( const CDMatrix& B, CDMatrix& C ) const
{
	// CHECK: A, B and C must have same dimensions
	assert((NRows() == B.NRows()) & (NCols() == B.NCols()));
	assert((NRows() == C.NRows()) & (NCols() == C.NCols()));

	int n = NRows();
	int m = NCols();

	for (int r = 0; r < n; r++)
		for (int c = 0; c < m; c++)
			C[r][c] = ((*this)[r][c] >= B[r][c]) ? 1 : 0;
}



/**

  @author   Lars Pedersen
  @version  7-12-2000

  @memo     Takes the natural logarithm (base e=2.71..) of each matrix element.

  @doc      Takes the natural logarithm (base e=2.71..) of each matrix element.

  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::Log()
{
	for(int i=0;i<NRows();i++)
		for(int j=0;j<NCols();j++)
			(*this)[i][j] = log((*this)[i][j]);
}



/**

  @author   Mikkel B. Stegmann
  @version  3-9-2001

  @memo     Flip matrix in left/right direction.
  
  @doc      FlipLR modifies the matrix with row preserved 
            and columns flipped in the left/right direction.

  @see      FlipUD
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::FlipLR() {

    int cols = NCols();
    int rows = NRows();    
    double tmp, *e_left=NULL, *e_right=NULL;
    int c2 = cols/2;

    for(int c=0;c<c2;c++) {

        e_left  = &((*this)[0][c]);
        e_right = &((*this)[0][cols-c-1]);

        for(int r=0;r<rows;r++) {

            tmp = *e_left;
            *e_left = *e_right;
            *e_right = tmp;

            e_left += cols;
            e_right+= cols;
        }
    }       
}



/**

  @author   Mikkel B. Stegmann
  @version  3-9-2001

  @memo     Flip matrix in up/down direction.

  @doc      FlipUD(X) modifies the matrix with columns preserved 
            and rows flipped in the up/down direction.

  @see      FlipLR
    
  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::FlipUD() {

    int cols = NCols();
    int rows = NRows();    
    double *e_top=NULL, *e_bottom=NULL;
    int r2 = rows/2;
    int row_size = cols*sizeof(double);
    double *tmp_row = new double[cols];

    for(int r=0;r<r2;r++) {
        
        e_top    = &((*this)[r][0]);
        e_bottom = &((*this)[rows-r-1][0]);        

        memcpy( tmp_row, e_top, row_size );
        memcpy( e_top, e_bottom, row_size );
        memcpy( e_bottom, tmp_row, row_size );
    }  
    
    delete[] tmp_row;
}


/**

  @author   Mikkel B. Stegmann
  @version  3-9-2001

  @memo     Uniformly distributed random numbers.

  @doc      Inserts uniformly distributed random numbers in
            the range [0;1].

  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::Rand() {

    int cols = NCols();
    int rows = NRows();

    for(int r=0;r<rows;r++) {

        for(int c=0;c<cols;c++) {

            (*this)[r][c] = rand()/(double)RAND_MAX;
        }
    }
}


/**

  @author   Mikkel B. Stegmann
  @version  3-16-2001

  @memo     Forms the outer product of two vectors.

  @doc      Forms the outer product of two vectors and store the result in this.
            If the matrix does have the correct size, it will be resized.
  
  @param    v1  Input vector.
  @param    v2  Input vector.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::OuterProduct(const CDVector &v1, const CDVector &v2) {


    if ( NRows() != v1.Length() || NCols() != v2.Length() ) {

        this->Resize( v1.Length(), v2.Length() );
    }

    *this = VisOuterProduct( v1, v2 );
}


/**

  @author   Mikkel B. Stegmann
  @version  6-25-2001

  @memo     Calculates the determinant of a square matrix up to the 3rd order.

  @doc      Calculates the determinant of a square matrix up to the 3rd order.
  
  @return   The determinant.
  
*/
DIVAMatrixExport double CDMatrix::Det() const {

    double d;

    assert( NRows()==NCols() );    

    if( NRows()==3 ) {

        // 3x3
        d =   (*this)[0][0]*(*this)[1][1]*(*this)[2][2] + (*this)[0][1]*(*this)[1][2]*(*this)[2][0] + (*this)[0][2]*(*this)[1][0]*(*this)[2][1] 
            - (*this)[0][0]*(*this)[1][2]*(*this)[2][1] - (*this)[0][1]*(*this)[1][0]*(*this)[2][2] - (*this)[0][2]*(*this)[1][1]*(*this)[2][0];
    }     
    if ( NRows()==2 ) { 

        // 2x2
        d =   (*this)[0][0]*(*this)[1][1] - (*this)[1][0]*(*this)[0][1];
    }
    if ( NRows()==1 ) {

        // 1x1
        d = (*this)[0][0];
    }
    if ( NRows()>3 ) {
        
        // NxN
        d = Determinant();
    }

    return d;
}


/**

  @author   Mikkel B. Stegmann
  @version  6-25-2001

  @memo     Calculates the trace - i.e. the sum of diagonal elements.

  @doc      Calculates the trace - i.e. the sum of diagonal elements.
  
  @return   The trace.
  
*/
DIVAMatrixExport double CDMatrix::Trace() const {

    double trace = .0;

    assert( NRows()==NCols() );
    for(int i=0;i<NRows();i++) {

        trace += (*this)[i][i];
    }

    return trace;
}



/**

  @author   Mikkel B. Stegmann
  @version  5-15-2002

  @memo     Converts this matrix to a vector.

  @doc      Converts this matrix to a vector, either by row (default) or column.    
  
  @param    v   Output vector  

  @param    rowWise If true (default) matrix data is extracted row-wise.
                    If false matrix data is extracted column-wise.
  
  @return   Nothing.
  
*/
DIVAMatrixExport void CDMatrix::ToVector( CDVector &v, bool rowWise ) {
  
    int i=0;
    v.Resize( NRows()*NCols() );
    if (rowWise) {

        for(int r=0;r<NRows();r++) {
            for(int c=0;c<NCols();c++) {

                v[i++] = (*this)[r][c];
            }
        }
    } else {
      
        for(int c=0;c<NCols();c++) {
            for(int r=0;r<NRows();r++) {

                v[i++] = (*this)[r][c];
            }
        }
    }
}