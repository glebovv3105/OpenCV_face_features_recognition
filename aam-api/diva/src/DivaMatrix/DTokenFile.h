/* $Id: DTokenFile.h,v 1.1.1.1 2003/01/03 19:18:16 aam Exp $ */
// TokenFile.h: interface for the CTokenFile class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TOKENFILE_H__EDCF5BD1_C997_11D2_8BB2_92A3095DE671__INCLUDED_)
#define AFX_TOKENFILE_H__EDCF5BD1_C997_11D2_8BB2_92A3095DE671__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "afx.h"

class CDTokenFile : public CStdioFile  
{
public:
	virtual BOOL NextToken(CString& tok);
	CDTokenFile();
	virtual ~CDTokenFile();
private:
	CString m_CurString;
	CString m_Delimiter;
};

#endif // !defined(AFX_TOKENFILE_H__EDCF5BD1_C997_11D2_8BB2_92A3095DE671__INCLUDED_)
