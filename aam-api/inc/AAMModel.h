//****************************************************************************
//
// Copyright � 2000, 2001, 2002, 2003 by Mikkel B. Stegmann
// 
// IMM, Informatics & Mathmatical Modelling
// Technical University of Denmark
// Richard Petersens Plads
// Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/
//
// Author: Mikkel B. Stegmann - http://www.imm.dtu.dk/~aam/ - aam@imm.dtu.dk
//
// $Id: AAMModel.h,v 1.5 2003/02/14 21:49:23 aam Exp $
//
// DISCLAIMER:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on;
//
//		- The Microsoft Vision Software Developers Kit: VisSDK, Microsoft
//		- The DTU Image Viewer: DIVA, Rune Fisker et al., IMM, DTU
//
//****************************************************************************
#ifndef __C_AAMMODEL__
#define __C_AAMMODEL__

#include <stdio.h>

#include "AAMObject.h"
#include "AAMDeform.h"
#include "AAMShape.h"
#include "AAMUtil.h"



// forword decl to aviod unnecessary includes
class CAAMTransferFunction;
class CAAMAnalyzeSynthesize;
class CAAMReferenceFrame;
class CAAMOptRes;


/**

  @author   Mikkel B. Stegmann
  @version  10-19-2000

  @memo     The core Active Apperance Model object.

  @doc      The core Active Apperance Model object that hold all eigenmodels,
			prediction matrices etc. Build by a CAAMBuilder.

  @see      CAAMBuilder
  
*/
class CAAMModel : public CAAMObject {

public: 
      
	/**

      @author   Mikkel B. Stegmann
      @version  4-10-2000

      @memo     Stores interates from the optimization process.

      @doc      Stores interates from the optimization process.

    */
    struct CAAMOptState {

		/// Constructor.
        CAAMOptState( double e, const CAAMShape &s, const CDVector &_c, int d ) :
                      error(e), shape(s), c(_c), damps(d) {};

        double error;       // error (dependent upon the chosen similarity measure)
        CAAMShape shape;    // shape
        CDVector c;         // model parameters    
        int damps;          // number of dampings used in the iteration

		/// Assignment operator.
		CAAMOptState& operator=(const CAAMOptState &os) {
			
			error = os.error;
			shape = os.shape;
			c.Resize( os.c.Length() ); 
			c = os.c;
			damps = os.damps;

			return *this;
		}

    };


    /**

      @author   Mikkel B. Stegmann
      @version  4-10-2000

      @memo     Stores optimization results.

      @doc      Stores optimization results.
	  
    */
    class CAAMOptRes {

	    /// The number of iterations.
	    int m_nIterations;

	    /// The Mahalanobis Distance of the model parameters.
	    double m_dMaha;

	    /// The similarity measure value at optimum
	    double m_dSimilarityMeasure;

    public:

	    /// Constructor.
	    CAAMOptRes() { m_dMaha = m_dSimilarityMeasure = .0; m_nIterations = 0; }

	    /// Constructor.
	    CAAMOptRes( const double maha,
				    const int nIterations,
				    const double sm			) {

		    m_dMaha			= maha;
		    m_nIterations	= nIterations;
		    m_dSimilarityMeasure = sm;
	    }	

	    /// Returns the number of iterations.
	    inline int NIter() const { return m_nIterations; }
	    
	    /// Returns the Mahalanobis Distance of the model parameters.
	    inline double Mahalanobis() const { return m_dMaha; }

		/// Returns the similarity measure.
	    inline double SimilarityMeasure() const { return m_dSimilarityMeasure; }
    };

protected:

    friend class CAAMBuilder; // grant access to private members
    friend class CAAMWLBuilder; // grant access to private members

    CAAMAnalyzeSynthesize *m_pAnalyzeSynthesize;
    CAAMReferenceFrame *m_pReferenceFrame;

    /// The shape PCA
    CAAMDeformPCA m_ShapePCA;

    /// The texture PCA
    CAAMDeformPCA m_TexturePCA;
	
    /// The combined PCA
    CAAMDeformPCA m_CombinedPCA;

    /// The shape-to-pixel weights.
	CDMatrix m_mShape2PixelWeights;

    /// The AMF format version.
	double m_dAMFVersion;

    /// The shape part of the combined eigenvectors
	CDMatrix m_mQsEV;

	/// The texture part of the combined eigenvectors
	CDMatrix m_mQgEV;

    /// The mean texture.
	CDVector m_vMeanTexture;
    CDVector m_vMeanTextureOrg;

    /// Cached mean shape
    CAAMShape m_sMeanAShape;

	/// Cache matrix.
	CDMatrix m_mShapeInstance;

	/// Cache matrix.
	CDMatrix m_mTextureInstance;
    
   	/// The variance of each normalized pixel in the texture model.
	CDVector m_vTextureVar;

    /// The number of texture samples in the model.
	int m_iTextureSamples;

    int m_iNShapes;
    
	double m_dMeanShapeSize;

	/// The parameter prediction matricies
	CDMatrix m_R_c, m_R_t;

    /// The texture transfer function
    CAAMTransferFunction *m_pTextureTF;
	
    int m_iModelReduction;
	double m_dAddExtents;
	bool m_bUseConvexHull;
	bool m_bUseTangentSpace;
	int m_iLearningMethod;
    int m_iShapeTrunc;
    int m_iTextureTrunc;
	int m_iCombinedTrunc;     
	      
    CString m_sCurrentAnalyzeId;

    // experimental stuff *not* saved in the model file
    double m_dBuildTime; // secs
    CDVector m_vPoseParameterUpdateConstraints;
    CDVector m_vShapeParameterConstraintsMean, m_vShapeParameterConstraintsSD;
	CAAMDeformPCA m_DisplacementPCA;

    
public:

	/// Texture parameter update prediction matrix.
    const CDMatrix &Rc() const { return m_R_c; }

	/// Pose parameter update prediction matrix.
    const CDMatrix &Rt() const { return m_R_t; }

	/// The texture part of the combined PCA eigenvectors.
    const CDMatrix &Qg() const { return m_mQgEV; }

	/// The shape part of the combined PCA eigenvectors.
    const CDMatrix &Qs() const { return m_mQsEV; }

	/// Returns the reference frame of the model.
    const CAAMReferenceFrame &ReferenceFrame() const { return *m_pReferenceFrame; }

    /// Returns the number of bands in the model
    inline int NBands() const { return BANDS; }    

    inline const CAAMTransferFunction &TextureTF() const { return *m_pTextureTF; }

	CAAMModel();	// default constructor
	~CAAMModel();	// default destructor

    CAAMModel( const CAAMModel &m );    // copy constructor
    CAAMModel& operator=(const CAAMModel &m);

	/// Returns the number of samples in the texture model.
    int NTextureSamples() const { return m_iTextureSamples; }

    bool ApproxExample( const CString &filename, CDMultiBand<TAAMPixel> &outImg ) const;

    void SetPoseParameterUpdateConstraints( const CDVector &pc );
    void SetShapeParameterUpdateConstraints( const CDVector &mean, const CDVector &sd );
    void ConstrainSearchParameters( CDVector &c, CDVector &pose ) const;

    bool EstimatePose(	const CDMultiBand<TAAMPixel> image, 
                        const CAAMShape &shape, CDVector &pose	) const;    

	/// Returns true if the texture model is based on the convex hull.
    inline bool IsConvexHullUsed() const { return m_bUseConvexHull; }

	/// Returns the amount shape extents added (warning: shape extents will be remove in later versions).
	inline double AddExtents() const { return m_dAddExtents; }

    void ModelImage( const CDVector &c,
					 CDMultiBand<TAAMPixel> &outImg,						 					 
					 const CAAMShape *matchPose = NULL,
                     const bool fitTexture = true ) const;

    void ModelImageEx( const CAAMShape &shape,
		    	       const CDVector &texture,
				       CDMultiBand<TAAMPixel> &outImg,
				       const bool renderImage = false,
                       const bool fitTexture = true ) const;
    
    void ShapeFreeImage( const CDVector &textureSamples,
						 CDMultiBand<TAAMPixel> &outImg,
                         const bool deMap = true ) const;    

    void ShapeFreeImage( const CDVector &textureSamples,
						 CDMatrix &m,
                         const bool deMap = true,
                         const bool normalize = false ) const;    
    
    /// Returns the mean shape.
    inline const CAAMShape &MeanShape() const { return m_sMeanAShape; }

    /// Returns the mean texture.
    inline const CDVector &MeanTexture()        const { return m_vMeanTexture;               }

    double ModelEstimateTexDiff( const CDMultiBand<TAAMPixel> &outImg,
								 const CDVector &c, 
								 CAAMShape &estimate,
								 CDVector &diff,											
								 const int similaritym = 0,
                                 const bool useInterpolation = true ) const;  

    void NormalizeTexture( CDVector &texture ) const;

    virtual CAAMOptRes OptimizeModel(  const CDMultiBand<TAAMPixel> &image,
					                   CAAMShape &s, CDVector &c,	
								       const int maxIterations = 30,
									   std::vector<CAAMOptState> *pOptStates = NULL,
		                               bool disableDamping = false ) const;

    CAAMOptRes OptimizeModelByFineTuning( const CDMultiBand<TAAMPixel> &image,
			    						CAAMShape &s, CDVector &c,
				    					const int maxIterations,
					    				const int similaritym = 0,	
						    			const int optimizer = 1	) const;

    virtual bool ReadModel( const CString &filename );

	/// Returns the reduction factor of the training set that this model was built on.
    inline int ReductionFactor() const { return m_iModelReduction; }

    // Returns the reference shape i.e. the mean shape scaled to mean size.
    const CAAMShape &ReferenceShape() const;

	int SampleShape(	const CDMultiBand<TAAMPixel> &image,
						const CAAMShape &shape, 
						CDVector &textureSamples,						
                        const bool normalize = true,
                        const bool useInterpolation = true,
                        const bool map = true ) const;

    void ShapeInstance( const CDVector &c, CAAMShape &outShape ) const;
	
	void TextureInstance( const CDVector &c, CDVector &outTexture ) const;
	
	void ShapePCAInstance( const CDVector &b_s, CAAMShape &outShape ) const;

    void ShapeTexParam2Combined(	const CDVector &b, 
									CDVector &c ) const;

    void ShapeTexParam2Combined(	const CDVector &b_s,
                                    const CDVector &b_g, 
		    					    CDVector &c ) const;
	
	void ShapeTexParam2Combined(	const std::vector<CDVector> &bVectors, 
									std::vector<CDVector> &cVectors ) const;

    void Shape2Combined(    const CAAMShape &shape, 
                            const CDMultiBand<TAAMPixel> &image, 
                            CDVector &c     ) const;
    void Shape2Param(	    const CAAMShape &shape, 
							CDVector &b_s ) const;

    void Combined2ShapeParam( const CDVector &c, CDVector &b_s ) const;
    void Combined2TexParam( const CDVector &c, CDVector &b_g ) const;

	void ShapeTex2Param(	const CAAMShape &shape, 
							const CDVector &texture, 
							CDVector &b ) const;

	void ShapeTex2Combined(	const CAAMShape &shape, 
							const CDVector &texture, 
							CDVector &c ) const;

    const CDMatrix &Shape2PixelWeights() const { return m_mShape2PixelWeights; };

    /// Returns the shape PCA.
    inline const CAAMDeformPCA &ShapePCA()         const { return m_ShapePCA;                   }

	/// Returns the texture PCA.
    inline const CAAMDeformPCA &TexturePCA()       const { return m_TexturePCA;                 }

	/// Returns the combined PCA.
    inline const CAAMDeformPCA &CombinedPCA()      const { return m_CombinedPCA;                }

    inline int ModelReduction() const { return m_iModelReduction; }

   	virtual bool WriteModel( const CString &filename, const bool txt_only = false ) const;

    void WriteVarianceMap( const CString &filename ) const;

	/// Returns the mean shape size, i.e. the size of the reference shape.
    inline double MeanShapeSize() const { return m_dMeanShapeSize; }
};


#endif 
