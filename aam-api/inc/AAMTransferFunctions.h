//****************************************************************************
//
// Copyright � 2000, 2001, 2002, 2003 by Mikkel B. Stegmann
// 
// IMM, Informatics & Mathmatical Modelling
// Technical University of Denmark
// Richard Petersens Plads
// Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/
//
// Author: Mikkel B. Stegmann - http://www.imm.dtu.dk/~aam/ - aam@imm.dtu.dk
//
// $Id: AAMTransferFunctions.h,v 1.3 2003/01/20 10:29:16 aam Exp $
//
// DISCLAIMER:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on;
//
//		- The Microsoft Vision Software Developers Kit: VisSDK, Microsoft
//		- The DTU Image Viewer: DIVA, Rune Fisker et al., IMM, DTU
//
//****************************************************************************
#ifndef __C_AAMTRANSFERFUNCTIONS__H__
#define __C_AAMTRANSFERFUNCTIONS__H__

#include "AAMTransferFunction.h"


/**

  @author   Mikkel B. Stegmann
  @version  1-25-2002

  @memo     Transfer function that implements the identity transformation.

  @doc      Transfer function that implements the identity transformation.
			Actually this function is one of the reasons for the design
			of the Map and DeMap methods. An identity transform should
			not induce any computational overhead whatsoever.
  
*/
class CAAMTFIdentity : public CAAMTransferFunction {
    
public:
    CAAMTFIdentity() { m_Id = tfIdentity; }    
    ~CAAMTFIdentity() { }    

    CAAMTransferFunction *Clone() const { return new CAAMTFIdentity(*this); }    
    const char *TypeName() const { return "identity"; }

    // Mapping
    inline void Map( CDVector &v ) const {}
    inline void DeMap( CDVector &v ) const {}          
};



/**

  @author   Mikkel B. Stegmann
  @version  1-28-2002

  @memo     Transfer function that implements a lookup table.

  @doc      Transfer function that implements a lookup table, which
			is useful for e.g. histogram equalisations.
			
*/
class CAAMTFLookUp: public CAAMTransferFunction {
    
    CDVector m_LUT, m_InvLUT;

public:

	/// Constructor.
    CAAMTFLookUp() { m_Id = tfLookUp; }

	/// Destructor.
    ~CAAMTFLookUp() { }    

	/// Clone function (conveys type info).
    CAAMTransferFunction *Clone() const { return new CAAMTFLookUp(*this); }

	/// Returns the clear-text type name.
    const char *TypeName() const { return "lookup table"; }

    // Setup 
    void LoadLUT( const CDVector &lut );    

    // Mapping
    void Map( CDVector &v ) const;
    void DeMap( CDVector &v ) const;    

    // I/O
    void FromFile( FILE *fh );
    void ToFile( FILE *fh ) const;
};



/**

  @author   Mikkel B. Stegmann
  @version  1-25-2002

  @memo     Performs a uniform stretch into [0;255] on the demapping side.

  @doc      Performs a uniform stretch into [0;255] on the demapping side 
			(yes, a really degenerate class... I know...).
  
*/
class CAAMTFUniformStretch : public CAAMTransferFunction {    

public:

	/// Constructor.
    CAAMTFUniformStretch() { m_Id = tfUniformStretch; }

	/// Destructor.
    ~CAAMTFUniformStretch() { };

	/// Clone function (conveys type info).
    CAAMTransferFunction *Clone() const { return new CAAMTFUniformStretch(); }  

	/// Returns the clear-text type name.
    const char *TypeName() const { return "uniform stretch"; }

    // Mapping
    void Map( CDVector &v ) const;
    void DeMap( CDVector &v ) const;       
};


// forword decl to aviod unnecessary includes
class CAAMModel;


CAAMTransferFunction *AAMLoadTransferFunction( FILE *fh, CAAMModel *pModel );


#endif /* __C_AAMTRANSFERFUNCTIONS__H__ */
