/* $Id: htimer.h,v 1.1.1.1 2003/01/03 19:18:50 aam Exp $ */
#ifndef __HTIMER_H__
#define __HTIMER_H__

class CHTimer {
protected:
	long	mStartTick;
	bool	mIsRunning;
	double	mTime;
	bool	mHighPerformanceTimer;
	double	mFrequency;
	double	mHighPerformanceTimerStartCount;

public:
	CHTimer(bool startNow=false);
	void	start();
	void	stop();
	void	reset();
	double	getTime() const;
};

template <class T> T& operator<<(T& os, CHTimer& timer)
{
	os << timer.getTime();
	return os;
}

#endif