//****************************************************************************
//
// Copyright � 2000, 2001, 2002, 2003 by Mikkel B. Stegmann
// 
// IMM, Informatics & Mathmatical Modelling
// Technical University of Denmark
// Richard Petersens Plads
// Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/
//
// Author: Mikkel B. Stegmann - http://www.imm.dtu.dk/~aam/ - aam@imm.dtu.dk
//
// $Id: AAMUtil.h,v 1.7 2003/04/23 14:50:00 aam Exp $
//
// DISCLAIMER:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on;
//
//		- The Microsoft Vision Software Developers Kit: VisSDK, Microsoft
//		- The DTU Image Viewer: DIVA, Rune Fisker et al., IMM, DTU
//
//****************************************************************************
#ifndef __C_AAMUTIL__
#define __C_AAMUTIL__

#include "AAMShapeCollection.h"
#include "AAMReferenceFrame.h"
#include "AAMdef.h"


/**

  @author   Mikkel B. Stegmann
  @version  4-6-2000

  @memo     Utility methods for the AAM project.

  @doc      This class consists of methods that are self-containing and
			could not logically fit into any other AAM class.

			Thus all methods are static, so remember that there is never
			need for an	instantiation of this class.  

*/
class CAAMUtil : public CAAMObject {

private:

	static double ProjPointOnLine(	const double dXL1, 
							const double dYL1, 
							const double dXL2, 
							const double dYL2, 
							const double dXP, 
							const double dYP, 
							double& dXProj, 
							double& dYProj	);

	static void PointOnAssBorder(	const CDVector& vX1, 
									const CDVector& vY1, 
									const CDVector& vX2, 
									const CDVector& vY2, 
									CDVector& vXBorder, 
									CDVector& vYBorder, 
									CDVector& vDist		);

public:

	virtual void DoNotInstantiateThisClassCuzAllMethodsAreStatic() = 0;

	static std::vector<CString> ScanSortDir( const CString &path, 
											 const CString &extension );

	static void PlotShapeIntoImage( CDMultiBand<TAAMPixel> image, 
									const CAAMShape &shape,
									TAAMPixel *point_color, 
									TAAMPixel *line_color,
									bool drawNormals = true,
									bool drawArea = true,
									bool drawPoints = true,
                                                                        bool drawLines = true );

	static void PlotMeshIntoImage( CDMultiBand<TAAMPixel> image, 
									const CAAMMesh &mesh,
									TAAMPixel *point_color, 
									TAAMPixel *line_color );

	static void VecCat3( CDVector &dest, const CDVector &v1, 
						 const CDVector &v2, const CDVector &v3 );

	static void ReadExample( const CString &filename, 
							 CDMultiBand<TAAMPixel> &img, 
							 CAAMShape &shape,
							 int rfactor = 1 );
	
	static double DistEuclidianPoints(	const CAAMShape &s1,
										const CAAMShape &s2			);

	static double DistEuclidianAssBorder(	const CAAMShape &s1,
											const CAAMShape &s2,											
											CDVector *pvDist = NULL );

	static double DistEuclidianPoints(	const CDVector& vX1, 
										const CDVector& vY1, 
										const CDVector& vX2, 
										const CDVector& vY2 );

	static double DistEuclidianAssBorder(	const CDVector& vX1, 
											const CDVector& vY1, 
											const CDVector& vX2,
											const CDVector& vY2,
											CDVector *pvDist  );

	static double SymmetricPtCrv( const CAAMShape &s1, const CAAMShape &s2 );

	static double ShapeOverlap( const CAAMShape &model, const CAAMShape &gt );

    // Tests if a shape is inside an image.
    static bool ShapeInsideImage( const CAAMShape &s, const CDMultiBand<TAAMPixel> &img );

	/// Converts from degrees to radians.
	static double Deg2Rad( double deg ) { return deg*M_PI/180.0; }

	/// Convert from radians to degrees.
	static double Rad2Deg( double rad ) { return rad*180.0/M_PI; }

	// Remove file extension.
	static CString RemoveExt( const CString &s );

	// Get file extension.
	static CString GetExt( const CString &s );	

	// Tests if the file can be created (if it exits it's deleted!).
	static bool CreateTest( const CString &file );

	// Adds a terminating backslash to a path (if it doesn't exits already).
	static CString AddBackSlash( const CString &path );

	// Extract the dir from a file with full path (including term backslash).
	static CString GetPath( const CString &fullfilename );

	// Extracts the filename from a fully qualified filename (that's incl. path).
	static CString GetFilename( const CString &filename );

    // Checks if a file exist.
    static bool FileExists( const CString &filename );

    static CString FindVacantFilename( const CString &filename_suggestion );

	// Checks if a given extension is present, if not one is padded.
	static CString ForceExt( const CString &filename, const CString &ext );								

    static void ExpandImg2DyadicSize(  const CDMultiBand<TAAMPixel> &img, 
                                       CDMultiBand<TAAMPixel> &out );        

    static void MirrorEdge( CDMatrix &m, const CDMultiBand<TAAMPixel> &mask, int edgeWidth = 10 );  
    static void MirrorEdge( CDMultiBand<TAAMPixel> &img, const CDMultiBand<TAAMPixel> &mask, int edgeWidth = 10 );  


    static void CalcShapeDistances( const CAAMShape &optimized, 
		                		    const CAAMShape &groundTruth,
		                            double &ptpt,
		                            double &ptcrv,
			                        CDVector *pvDists = NULL ); 

    static CString Secs2Mins( double secs );


    static void RegistrationMovie( const CString &filename,
                                   const CString asfPath,
                                   const bool useConvexHull = true,
                                   const bool writeRefShape = false );

    static void RegistrationMovie( const CString &filename,
                                   const std::vector<CDVector> &vTexture,
                                   const CAAMReferenceFrame &rf );
     
    static void SampleTextures( const CAAMShapeCollection &unalignedShapes,                                
                                std::vector< CDVector > &vTextures,
                                CAAMReferenceFrame &outputRF,
                                const int imageReduction = 1,
                                const bool removeMean = false,
                                const bool useTSP = true,
                                const bool useConvexHull = true );

	static void ASF2PTS( const CString &path );

	static std::vector<CString> ReadLines( const CString &filename );
};

template <class TPixel>
		void DIVAEnlageImage( CVisImage<TPixel> &img, int nFactor);

template <class TPixel>
		void DIVAReduceImage( CVisImage<TPixel> &img, int nFactor);


/**

  @author   Mikkel B. Stegmann
  @version  4-17-2000

  @memo     Simple lo-fi property reader.

  @doc      Simple lo-fi property reader. Used as naive parser/scanner
			for the .asf, acf files et cetera.

  @see		CAAMBuilder::ReadACF(), CAAMShape::ReadASF()
      
*/
class CAAMPropsReader  : public CAAMObject {

	FILE *fh;
	void SkipRestOfLine();
	void SkipComments();
    void SkipWhiteSpace();
    const char CR;
    const char LF;
    const char COMMENT_CHAR;

public:

	CAAMPropsReader( const CString &filename );
	~CAAMPropsReader();
	bool MoreNonWhiteSpaceOnLine();

	/// Skips whitespace and any commments preceeding the current file position.
	void Sync() { SkipWhiteSpace(); SkipComments(); }

	/// Returns true if the file is valid.
	bool IsValid() { return fh!=NULL; }
	
	/// Current open file.
	FILE *FH() { return fh; }
};

#endif /* __C_AAMUTIL__ */
