//****************************************************************************
//
// Copyright � 2000, 2001, 2002, 2003 by Mikkel B. Stegmann
// 
// IMM, Informatics & Mathmatical Modelling
// Technical University of Denmark
// Richard Petersens Plads
// Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/
//
// Author: Mikkel B. Stegmann - http://www.imm.dtu.dk/~aam/ - aam@imm.dtu.dk
//
// $Id: AAMTransferFunction.h,v 1.3 2003/01/20 10:29:16 aam Exp $
//
// DISCLAIMER:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on;
//
//		- The Microsoft Vision Software Developers Kit: VisSDK, Microsoft
//		- The DTU Image Viewer: DIVA, Rune Fisker et al., IMM, DTU
//
//****************************************************************************
#ifndef __C_AAMTRANSFERFUNCTION__H__
#define __C_AAMTRANSFERFUNCTION__H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "AAMObject.h"
#include "DVector.h"

enum eTFid { tfBase, tfIdentity, tfLookUp, tfUniformStretch, tfWavelet };

/**

  @author   Mikkel B. Stegmann
  @version  1-25-2002

  @memo     Abstract base class for all transfer functions.

  @doc      This class defines an interface for the so-called
			transfer functions, which are nothing but differnt
			mapping from one vector space to another.
  
*/
class CAAMTransferFunction : public CAAMObject  {

protected:
    eTFid m_Id; 
public:
    CAAMTransferFunction() : m_Id(tfBase) {}
    virtual ~CAAMTransferFunction() { };

    const eTFid &Type() const { return m_Id; }
    virtual const char *TypeName() const = 0;
    virtual const CString TypeInfo() const { CString s; return s; }

    virtual inline void Map( CDVector &v ) const = 0;
    virtual inline void DeMap( CDVector &v ) const = 0;
    virtual CAAMTransferFunction *Clone() const = 0;

    // I/O
    virtual void FromFile( FILE *fh );
    virtual void ToFile( FILE *fh ) const;
};

#endif /* __C_AAMTRANSFERFUNCTION__H__ */
