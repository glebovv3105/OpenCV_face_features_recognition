//****************************************************************************
//
// Copyright � 2000, 2001, 2002, 2003 by Mikkel B. Stegmann
// 
// IMM, Informatics & Mathmatical Modelling
// Technical University of Denmark
// Richard Petersens Plads
// Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/
//
// Author: Mikkel B. Stegmann - http://www.imm.dtu.dk/~aam/ - aam@imm.dtu.dk
//
// $Id: AAMReferenceFrame.h,v 1.4 2003/04/24 11:25:32 aam Exp $
//
// DISCLAIMER:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on;
//
//		- The Microsoft Vision Software Developers Kit: VisSDK, Microsoft
//		- The DTU Image Viewer: DIVA, Rune Fisker et al., IMM, DTU
//
//****************************************************************************
#ifndef __C_AAMREFERENCEFRAME__H__
#define __C_AAMREFERENCEFRAME__H__

#include "AAMdef.h"


// disable warning C4786: symbol greater than 255 character,
// okay to ignore
#pragma warning(disable: 4786)


// forward declarations
class CAAMMesh;
class CAAMShape;
template <class TAAMPixel> class CDMultiBand;
class CDVector;
class CAAMPoint;



/**

  @author   Mikkel B. Stegmann
  @version  6-7-2002

  @memo     The geometrical reference frame (or shape-free frame).

  @doc      This class defined the geomtrical reference frame of an
			AAM. Hence, it is the spatial layout where all texture
			sampling takes place. 
			
			The main objective of this class is to provide fast 
			conversions from a shape-free image to a texture vector
			and vice versa.
  
*/
class CAAMReferenceFrame {

public:

	// defines a subpart of a horisontal line in an image
    struct sScanLinePart { int y, x1, x2; };    // x1 and x2 are inclusive

private:

    CAAMShape *m_pReferenceShape;
    CAAMMesh *m_pReferenceMesh;
    CDMultiBand<TAAMPixel> *m_pMaskImage;
    std::vector<sScanLinePart> m_ScanlineParts;
    int m_iTextureSamples;
    bool m_bUseConvexHull;
    
    void CalcScanLines();
    void CalcMaskImage( const bool useConvexHull );    
    
    void DebugDump(); 
	
public:    

	CAAMReferenceFrame();    
	~CAAMReferenceFrame();

    CAAMReferenceFrame( const CAAMReferenceFrame &rf );          // copy constructor
    CAAMReferenceFrame& operator=(const CAAMReferenceFrame &m); // assignment operator

    void Setup( const CAAMShape &referenceShape, const bool useConvexHull = false );

    void Image2Vector( const CDMultiBand<TAAMPixel> &refImg, CDVector &v ) const;
    void Vector2Image( const CDVector &v, CDMultiBand<TAAMPixel> &outImg ) const;
    void Vector2Matrix( const CDVector &v, CDMatrix &m ) const;

	double RefShapeWidth() const;
    double RefShapeHeight() const;
    
    int RefImageWidth() const;
    int RefImageHeight() const;

	/// Returns true of the convex hull determines the shape extent.
	bool UseConvexHull() const { return m_bUseConvexHull; }

	/// Returns the reference shape.
    const CAAMShape &RefShape() const { return *m_pReferenceShape; }
    
	/// Returns the number of texture samples in the texture model.
    int NTextureSamples() const { return m_iTextureSamples; }

	/// Returns the reference mesh.
    const CAAMMesh &RefMesh() const { return *m_pReferenceMesh; }

	/// Returns the mask image.
    const CDMultiBand<TAAMPixel> &MaskImage() const { return *m_pMaskImage; }

    void FromFile( FILE *fh );
	void ToFile( FILE *fh ) const;

	bool FindTriangle( const CAAMPoint &p, unsigned short &triangle, 
                       double &alpha, double &beta, double &gamma ) const;    

    // Direct access to the scanline parts (not commonly used)
    const std::vector<sScanLinePart> &ScanlineParts() const { return m_ScanlineParts; }
};

#endif /* __C_AAMMOBJECT__ */