//****************************************************************************
//
// Copyright � 2000, 2001, 2002, 2003 by Mikkel B. Stegmann
// 
// IMM, Informatics & Mathmatical Modelling
// Technical University of Denmark
// Richard Petersens Plads
// Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/
//
// Author: Mikkel B. Stegmann - http://www.imm.dtu.dk/~aam/ - aam@imm.dtu.dk
//
// $Id: AAMdef.h,v 1.2 2003/01/20 10:29:15 aam Exp $
//
// DISCLAIMER:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on;
//
//		- The Microsoft Vision Software Developers Kit: VisSDK, Microsoft
//		- The DTU Image Viewer: DIVA, Rune Fisker et al., IMM, DTU
//
//****************************************************************************
#ifndef __AAM_DEF__
#define __AAM_DEF__


// disable warning C4786: symbol greater than 255 character,
// okay to ignore
#pragma warning(disable: 4786)


/*

  @author   Mikkel B. Stegmann
  @version  5-8-2000

  @memo     AAMdef.H - Central include file and container for central defines etc.
  
  @doc		AAMdef.H - Central include file and container for central defines etc.

*/
// mfc/stl includes
//#include "stdafx.h"
#include <vector>

// ansi c includes
#include <stdlib.h>




// Diva includes
#include "DImageInitial.h"
#include "DMatrix.h"


// Global defines
#define AAM_MAX2(a,b) (a>b?a:b)
#define AAM_MAX3(a,b,c) AAM_MAX2( AAM_MAX2(a,b), c )
#define AAM_MIN2(a,b) (a<b?a:b)
#define AAM_MIN3(a,b,c) AAM_MIN2( AAM_MIN2(a,b), c )
#define AAMRound(x) (int)(.5+x)


// defines the pixeltype operated on in all functions
#define TAAMPixel CVisBytePixel

// defines the number of bands used in the AAM
#ifdef AAM_3BAND
#define BANDS 3
#else
#define BANDS 1
#endif


// define the pi constant if needed
#ifndef M_PI
	#define M_PI 3.141592653589793				 
#endif /* M_PI */


#endif /* __AAM_DEF__ */
