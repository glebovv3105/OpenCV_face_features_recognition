/* $Id: DMultiBand.h,v 1.1.1.1 2003/01/03 19:18:50 aam Exp $ */
#if !defined(_MULTIBAND_H_)
#define _MULTIBAND_H_ 

/*
   Implement multiband functionality into AAM project.
   Dmitry Karasik, 18.01.2002
*/

#include "AAMDef.h"

#define SPAWN(where,val) { int i = BANDS; while(i--) (where)[i] = (val); }

template <class TPixel>
class CDMultiBand : public virtual CDImageInitial<TPixel>
{
public:
	//------------------------------------------------------------------
	// @group Constructors

	// @cmember
	// Default constructor.  The image will not be useable until another
	// image is assigned to it.
	CDMultiBand(void) : CVisImage<TPixel>(){};

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDMultiBand(int width, int height, int nbands = BANDS,
		int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(width,height,nbands,imopts,pbData){ };

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDMultiBand(SIZE size, int nbands = BANDS,
			int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(size,nbands,imopts,pbData){ };

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDMultiBand(const RECT& rect, int nbands = BANDS,
			int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(rect,nbands,imopts,pbData){ };

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDMultiBand(const RECT& rect, CString sName, CString sHistory, int nbands = BANDS,
			int imopts = evisimoptDefault, BYTE *pbData = 0)
			:CVisImage<TPixel>(rect,nbands,imopts,pbData){ 
                        SetName(sName); 
                        SetHistory(sHistory); 
                        }

	// @cmember
	// Construct an image of a given size, allocating memory needed
	// for the image or using memory provided.
	CDMultiBand(const CVisShape& shape,
			int imopts = evisimoptDefault, BYTE *pbData = 0)
		:CVisImage<TPixel>(shape,imopts,pbData){};

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDMultiBand(CVisMemBlock& refmemblock, int width, int height,
			int nbands = BANDS, int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,width,height,nbands,imopts){ };

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDMultiBand(CVisMemBlock& refmemblock, SIZE size, int nbands = BANDS,
			int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,size,nbands,imopts){};

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDMultiBand(CVisMemBlock& refmemblock, const RECT& rect,
			int nbands = BANDS, int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,rect,nbands,imopts){};

	// @cmember
	// Construct an image of a given size using the memory provided.
	CDMultiBand(CVisMemBlock& refmemblock, const CVisShape& shape,
			int imopts = evisimoptDefault)
		:CVisImage<TPixel>(refmemblock,shape,imopts){};

	// @cmember
	// Construct an image from another image.
	// Warning:  Derived classes should provide their own copy constructor
	// that first constructs a default object and then uses the assignment
	// operator to synchronize  image creation, assignment, and
	// destruction.  Derived classes should not call the copy constructor
	// in the base class.
	CDMultiBand(const CDMultiBand<TPixel>& imageSrc)
		:CVisImage<TPixel>(imageSrc){ *this = imageSrc;}


        // Convertors from and to RGBA format. 
        //    -- pixel-wise
        void FromRGB(CVisRGBA<CVisBytePixel> rgb, TPixel * pixel);
        CVisRGBA<CVisBytePixel> ToRGB( TPixel * pixel);

        //    -- image-wise
        void CopyPixelsToRGBA( CVisImage<CVisRGBABytePixel> color); 
        void CopyPixelsToRGBA( CVisImage<CVisRGBABytePixel> color, LPRECT rect); 
        void CopyPixelsFromRGBA( CVisImage<CVisRGBABytePixel> color); 

        // compares all pixel bands to a value, returns 1 if all equal
        int isa(TPixel * pixel, int value);

        // Along with standard Pixel() functions, these two can be used 
        // as convenience operators
        void set_pixel( int x, int y, TPixel * pixel);
        void get_pixel( int x, int y, TPixel * pixel) const;

        
        // DImageBasic overloaded methods - see DIVA for description
        void ReducePyr(int nFactor = 2);
        void DrawLine(int x1 ,int y1 ,int x2 ,int y2, TPixel * tVal, bool fInsideCheck = false);
        inline void Pixel1(float x, float y, double * result) const;   // Bilinear interpolation        
        inline void CB(TPixel * y1, TPixel * y2, double d, double * result) const;

        // Use these methods instead ReadFile and WriteFile.
        // Maybe there is a way of how one does squeak M$-VisSDK to
        // use its native ReadFile/WriteFile, but I personally didnt find any.
        void ReadBandedFile( const char * szFilename);
        void WriteBandedFile( const char * szFilename = 0, const char * szFormat = 0);

private:
        inline void Swap(int& x, int& y) {int tmp = x; x = y; y = tmp;}
        
};

#include "DMultiBand.inl"

#endif 

