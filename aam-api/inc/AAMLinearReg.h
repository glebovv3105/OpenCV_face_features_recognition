//****************************************************************************
//
// Copyright � 2000, 2001, 2002, 2003 by Mikkel B. Stegmann
// 
// IMM, Informatics & Mathmatical Modelling
// Technical University of Denmark
// Richard Petersens Plads
// Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/
//
// Author: Mikkel B. Stegmann - http://www.imm.dtu.dk/~aam/ - aam@imm.dtu.dk
//
// $Id: AAMLinearReg.h,v 1.2 2003/01/20 10:29:15 aam Exp $
//
// DISCLAIMER:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on;
//
//		- The Microsoft Vision Software Developers Kit: VisSDK, Microsoft
//		- The DTU Image Viewer: DIVA, Rune Fisker et al., IMM, DTU
//
//****************************************************************************
#ifndef __C_AAMLINEARREG__
#define __C_AAMLINEARREG__


#include "DMatrix.h"
#include "AAMObject.h"

/**

  @author   Mikkel B. Stegmann
  @version  3-15-2000

  @memo     Performs multi-variate linear regression on a set of experiments.

  @doc      Performs multi-variate linear regression on a set of experiments.  
  
*/
class CAAMLinearReg : public CAAMObject {

	int m_nExperiments;
	int m_nPixelsDiffs;
	int m_nParams;
	CDVector m_vEigenVal;
	CDMatrix m_mEigenVec;

	int CAAMLinearReg::EstimateK( const CDMatrix &C, CDMatrix &EigenVec_k, CDMatrix &EigenVal_k );

public:
	CAAMLinearReg();
	~CAAMLinearReg();
	
	int DoRegression( const CDMatrix &C, const CDMatrix &X, CDMatrix &R );
};


#endif /* __C_AAMLINEARREG__ */