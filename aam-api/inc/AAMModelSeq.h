//****************************************************************************
//
// Copyright � 2000, 2001, 2002, 2003 by Mikkel B. Stegmann
// 
// IMM, Informatics & Mathmatical Modelling
// Technical University of Denmark
// Richard Petersens Plads
// Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/
//
// Author: Mikkel B. Stegmann - http://www.imm.dtu.dk/~aam/ - aam@imm.dtu.dk
//
// $Id: AAMModelSeq.h,v 1.1 2003/04/23 14:49:59 aam Exp $
//
// DISCLAIMER:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on;
//
//		- The Microsoft Vision Software Developers Kit: VisSDK, Microsoft
//		- The DTU Image Viewer: DIVA, Rune Fisker et al., IMM, DTU
//
//****************************************************************************
#ifndef __C_AAMMODELSEQ__
#define __C_AAMMODELSEQ__

#include "AAMObject.h"
#include "AAMModel.h"
#include <stdio.h>
#include <vector>

/**

  @author   Mikkel B. Stegmann
  @version  2-20-2003

  @memo     AAM sequence object.

  @doc      AAM sequence object. This is a generalisation of a multi-scale model.
	
  @see      CAAMModel, CAAMModelMS
  
*/
class CAAMModelSeq : public CAAMObject {

protected:
    std::vector<CAAMModel> m_vModels;
    std::vector<CString> m_vACF;
	
public:
	CAAMModelSeq();		// default constructor
	~CAAMModelSeq();	// default destructor   
	
	void BuildFromSACF(  const CString &SACF,
					     const CString &inDir,
                         const int excludeShape = -1 );

    void BuildFromSACF( const CString &SACF,
						const std::vector<CString> &asfFiles,						
						const int excludeShape = -1 );

    bool WriteModels( const CString &filename, const bool txt_only = false ) const;

    bool ReadModels( const CString &samf );

    /// Get the model at level i.
    inline const CAAMModel &Model( const int i ) const { assert(i>=0&&i<NModels()); return m_vModels[i]; }

    /// Get the number of models.
    inline const int NModels() const { return m_vModels.size(); }

	/// Get the final model
	inline const CAAMModel &FinalModel() const { return Model( NModels()-1 ); }

	/// Returns the model reduction at that level
	int ModelScale(int i) const { return Model(i).ModelReduction(); }

	// Scale a shape defined in 'FinalModel' coordinates to 'model' coordinates 
	void ScaleShape2Model( const int model, CAAMShape &shape ) const;

	// Scale a shape defined in 'model' coordinates to 'FinalModel' coordinates
	void ScaleShape2Final( const int model, CAAMShape &shape ) const;
};


#endif /* __C_AAMMODELSEQ__ */