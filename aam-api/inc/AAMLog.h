//****************************************************************************
//
// Copyright � 2000, 2001, 2002, 2003 by Mikkel B. Stegmann
// 
// IMM, Informatics & Mathmatical Modelling
// Technical University of Denmark
// Richard Petersens Plads
// Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/
//
// Author: Mikkel B. Stegmann - http://www.imm.dtu.dk/~aam/ - aam@imm.dtu.dk
//
// $Id: AAMLog.h,v 1.2 2003/01/20 10:29:15 aam Exp $
//
// DISCLAIMER:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on;
//
//		- The Microsoft Vision Software Developers Kit: VisSDK, Microsoft
//		- The DTU Image Viewer: DIVA, Rune Fisker et al., IMM, DTU
//
//****************************************************************************
#ifndef __C_AAMLOG__
#define __C_AAMLOG__

#include <stdio.h>
#include "aamobject.h"

enum eLogLevel { INFO, WARNING, ERROR, FATAL, SILENCE };

/**

  @author   Hans P. Palb�l & Mikkel B. Stegmann
  @version  03-05-2001

  @memo     Log facility class.

  @doc      Log facility class. 
  			Built directly upon the CInfo class from the Verticle 
  			project by Hans P. Palb�l.
  
*/
class CAAMLog : public CAAMObject {
	
	/// Common filname of the logfile.	 
	static char *m_logfile;

	/// Common file handle.
	static FILE *m_file_hndl;

	/// Name identifying the service.
	char *m_service;

	/// Notify level.
	eLogLevel m_level;

	///  Used to set whether file logging should be used or not.
	bool m_bFileLogging;
	
	/// Keeps track of whether the info service has been initialized.
	static int m_init;
	
    void GetTime( char *str );

public:
	CAAMLog( const char *service, const eLogLevel level = INFO );	    // default constructor
	~CAAMLog();	// default destructor    
		
	void InitLogFile( const char *filename );
	virtual void Printf( eLogLevel level, char *message, ... );
	void SetLevel( eLogLevel level );    
};

#endif /* __C_AAMLOG__ */