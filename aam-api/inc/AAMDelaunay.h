//****************************************************************************
//
// Copyright � 2000, 2001, 2002, 2003 by Mikkel B. Stegmann
// 
// IMM, Informatics & Mathmatical Modelling
// Technical University of Denmark
// Richard Petersens Plads
// Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/
//
// Author: Mikkel B. Stegmann - http://www.imm.dtu.dk/~aam/ - aam@imm.dtu.dk
//
// $Id: AAMDelaunay.h,v 1.3 2003/01/20 10:29:15 aam Exp $
//
// DISCLAIMER:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on;
//
//		- The Microsoft Vision Software Developers Kit: VisSDK, Microsoft
//		- The DTU Image Viewer: DIVA, Rune Fisker et al., IMM, DTU
//
//****************************************************************************
#ifndef __C_AAMDELAUNAY__
#define __C_AAMDELAUNAY__

#include "AAMObject.h"
#include "AAMShape.h"
#include "AAMMesh.h"

/**

  @author	Mikkel B. Stegmann
  @version	02-11-2000

  @memo		Delaunay Triangulator. 

  @doc		This class act as a wrapper for the delaunay triangulator by
			Steven Fortune. Copyright (c) 1994 by AT&T Bell Laboratories.
			AT&T, Bell Laboratories.			

			The full diclaimer for the delaunay triangulator is:

			The author of this software is Steven Fortune.  Copyright (c) 1994 by AT&T
			Bell Laboratories.
			Permission to use, copy, modify, and distribute this software for any
			purpose without fee is hereby granted, provided that this entire notice
			is included in all copies of any software which is or includes a copy
			or modification of this software and in all copies of the supporting
			documentation for such software.

			THIS SOFTWARE IS BEING PROVIDED "AS IS", WITHOUT ANY EXPRESS OR IMPLIED
			WARRANTY.  IN PARTICULAR, NEITHER THE AUTHORS NOR AT&T MAKE ANY
			REPRESENTATION OR WARRANTY OF ANY KIND CONCERNING THE MERCHANTABILITY
			OF THIS SOFTWARE OR ITS FITNESS FOR ANY PARTICULAR PURPOSE.
  
  @see		CAAMMesh

*/
class CAAMDelaunay : public CAAMObject {
	
public:

	// Constructor
	CAAMDelaunay();	

	// Destructor
	~CAAMDelaunay();
	
	// Makes a 2d mesh using the Delaunay Triangulization algorithm.
	static void MakeMesh( const CAAMShape &s, CAAMMesh &m,
						  bool bConcaveCleanUp = false );
};


#endif