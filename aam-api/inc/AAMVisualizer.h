//****************************************************************************
//
// Copyright � 2000, 2001, 2002, 2003 by Mikkel B. Stegmann
// 
// IMM, Informatics & Mathmatical Modelling
// Technical University of Denmark
// Richard Petersens Plads
// Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/
//
// Author: Mikkel B. Stegmann - http://www.imm.dtu.dk/~aam/ - aam@imm.dtu.dk
//
// $Id: AAMVisualizer.h,v 1.3 2003/01/20 10:29:16 aam Exp $
//
// DISCLAIMER:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on;
//
//		- The Microsoft Vision Software Developers Kit: VisSDK, Microsoft
//		- The DTU Image Viewer: DIVA, Rune Fisker et al., IMM, DTU
//
//****************************************************************************
#ifndef __C_AAMVISUALIZER__
#define __C_AAMVISUALIZER__

#include "AAMObject.h"
#include "AAMModel.h"
#include <vector>


/**

  @author   Mikkel B. Stegmann
  @version  5-8-2002

  @memo     Class that visualizes different aspects of a model.

  @doc      Class that visualizes different aspects of a model.

            For visualization of e.g. annotations without a model
            see CAAMUtil.

  @see      CAAMUtil
  
*/
class CAAMVisualizer {

    const CAAMModel *m_pModel;
	
public:
	CAAMVisualizer( const CAAMModel *pModel );	// default constructor
	~CAAMVisualizer();	                        // default destructor    

    void OptimizationMovie( const std::vector<CAAMModel::CAAMOptState> &optStates,
                            const CDMultiBand<TAAMPixel> &img,
                            const char *filename = "aamopt.avi", 
                            const int frameRate = 5 ) const;        

    bool TextureMovie( const CString &filename, 
                       const int nbModes=3, 
                       const double range=3.0, 
                       const int steps=10,
                       const bool whiteBackground = false  ) const;

    bool ShapeMovie(  const CString &filename,
                      const int nbModes=3, 
                      const double range=3.0, 
                      const int steps=10,
                      const bool whiteBackground = false ) const;	

    bool CombinedMovie( const CString &filename, 
                        const int nbModes=3, 
                        const double range=3.0, 
                        const int steps=10,
                        const bool whiteBackground = false ) const;

    bool RegistrationMovie( const CString &filename, 
                            const std::vector<CDVector> &m_vTexture ) const;

    static void ShapeStill( const CDMultiBand<TAAMPixel> &img,
							const CAAMShape &shape,
							const char *filename = "aamstill.bmp" );

    void WriteEigenImages() const;
    void WritePredictionImages() const;
};

#endif /* __C_AAMVISUALIZER__ */