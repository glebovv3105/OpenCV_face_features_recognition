//****************************************************************************
//
// Copyright � 2000, 2001, 2002, 2003 by Mikkel B. Stegmann
// 
// IMM, Informatics & Mathmatical Modelling
// Technical University of Denmark
// Richard Petersens Plads
// Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/
//
// Author: Mikkel B. Stegmann - http://www.imm.dtu.dk/~aam/ - aam@imm.dtu.dk
//
// $Id: AAMDeform.h,v 1.4 2003/04/23 14:49:59 aam Exp $
//
// DISCLAIMER:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on;
//
//		- The Microsoft Vision Software Developers Kit: VisSDK, Microsoft
//		- The DTU Image Viewer: DIVA, Rune Fisker et al., IMM, DTU
//
//****************************************************************************
#ifndef __C_AAMDEFORM__
#define __C_AAMDEFORM__

#include "AAMObject.h"
#include "DMatrix.h"

/**

  @author   Mikkel B. Stegmann
  @version  10-17-2000

  @memo     Abstract deformation basis.

  @doc      Abstract deformation basis.
  
*/
class CAAMDeform : public CAAMObject {

protected:

	/// Flags that signals everything has been setup correctly.
	bool m_bValid;
	
public:	
	CAAMDeform() { m_bValid = false; }
    ~CAAMDeform() { }
	virtual int NParameters() const = 0 ;
	virtual int NParametersOrg() const = 0;
	inline bool IsTruncated() { return NParameters()!=NParametersOrg(); }		
	inline bool IsValid() { return m_bValid; }
	virtual double ParameterWeight( const int i, bool asPercentage = false ) const = 0;
	virtual double ParameterWeightOrg( const int i, bool asPercentage = false ) const = 0;    
	virtual void Deform( const CDVector &params, CDVector &object ) const = 0;	
    virtual void Truncate( const int n ) {}
    virtual void ClearDataItems() {}
    virtual int TruncateVar( const double retained_variance ) { return 0; }
    virtual int TruncateParallel() { return 0; }
};



/**

  @author   Mikkel B. Stegmann
  @version  10-17-2000

  @memo     Performs Principal Component Analysis on a set of data vectors.

  @doc      Performs Principal Component Analysis on a set of data vectors.
			The PCA basis can then be used for e.g. shape deformation.

  @see      CAAMDeform
  
*/
class CAAMDeformPCA : public CAAMDeform {
	
	CDMatrix m_mEigenVectors;
	CDVector m_vEigenValues;
	CDVector m_vEigenValuesOrg;
    std::vector<CDVector> m_vvData;
    CDVector m_vDataMean;  
    
    int CalcNParam( const double retained_variance ) const;

    void ShuffleData();

public:
    void InsertDataItem( const CDVector &v );

    void DoPCA( bool bWriteCoVarMarix = false );
    void UseIdentityTransformation();
    const CDVector &MeanDataItem() const;	

	/// Returns the eigen values in vector form.
	inline CDVector &EigenValues()  { return m_vEigenValues; }
	
	/// Returns the eigen values in const vector form.
	inline const CDVector &EigenValues()  const { return m_vEigenValues; }

	/// Returns the orignal eigen values in const vector form.
	inline const CDVector &EigenValuesOrg()  const { return m_vEigenValuesOrg; }

	/// Returns the eigen vectors in matrix form.
	inline CDMatrix &EigenVectors() { return m_mEigenVectors; }

	/// Returns the eigen vectors in const matrix form.
	inline const CDMatrix &EigenVectors() const { return m_mEigenVectors; }

	/// Returns the number of princal parameters.
	inline int NParameters() const { return m_vEigenValues.Length(); }

	/// Returns the number of princal parameters before any truncation.
    inline int NParametersOrg() const { return m_vEigenValuesOrg.Length(); }

	double ParameterWeight( const int i, bool asPercentage = false ) const;
	double ParameterWeightOrg( const int i, bool asPercentage = false ) const;
    void Deform( const CDVector &params, CDVector &object ) const;	
    double MahalanobisDistance( const CDVector &params ) const;
    void Truncate( const int n );
    int TruncateVar( const double retained_variance );
    int TruncateParallel();    

	/// Deletes the data matrix.
    void ClearDataItems() { m_vvData.clear(); }

	/// Returns the number of data items this basis is based on.
	const int NDataItems() const { return m_vvData.size(); }
        
    // i/o
    void ToFile( FILE *fh ) const;    
    void FromFile( FILE *fh );

    // debug
    void Dump( const char *szPath ) const;

	void Project( const CDVector &obs, CDVector &param ) const;
	void BackProject( const CDVector &param, CDVector &synth_obs ) const;
	void Filter( CDVector &obs ) const;
};

#endif /* __C_AAMDEFORM__ */


