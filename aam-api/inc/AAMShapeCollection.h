//****************************************************************************
//
// Copyright � 2000, 2001, 2002, 2003 by Mikkel B. Stegmann
// 
// IMM, Informatics & Mathmatical Modelling
// Technical University of Denmark
// Richard Petersens Plads
// Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/
//
// Author: Mikkel B. Stegmann - http://www.imm.dtu.dk/~aam/ - aam@imm.dtu.dk
//
// $Id: AAMShapeCollection.h,v 1.3 2003/01/20 10:29:16 aam Exp $
//
// DISCLAIMER:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on;
//
//		- The Microsoft Vision Software Developers Kit: VisSDK, Microsoft
//		- The DTU Image Viewer: DIVA, Rune Fisker et al., IMM, DTU
//
//****************************************************************************
#ifndef __C_AAMSHAPECOLLECTION__
#define __C_AAMSHAPECOLLECTION__

#include "AAMShape.h"

/**

  @author	Mikkel B. Stegmann
  @version	02-08-2000

  @memo		Shape collection container and shape-aligner.
  @doc		This class act as a container for a set of shapes. Secondary it can
			align the set of shapes to a normalised reference frame with respect
			to position, scale and orientation.

  @see		CAAMShape

*/
class CAAMShapeCollection : public std::vector<CAAMShape>, CAAMObject {

	/// The meanshape prior to tangent space projection (calculated during alignment).
	CAAMShape m_MeanShapeBeforeTS;

	/// The average shape size (calculated during alignment).
	double m_dAvgSize;

    /// Path to the .asf files (if any).
    CString m_szPath;

public:

	// Constructor
	CAAMShapeCollection();

	// Destructor
	~CAAMShapeCollection();

	// Collection minimum in the x-direction.
	double MinX() const;

	// Collection maximum in the x-direction.
	double MaxX() const;

	// Collection minimum in the y-direction.
	double MinY() const;

	// Collection maximum in the y-direction.	
	double MaxY() const;

	/// The number of shapes in the collection.
	inline int NShapes() const { return size(); }

	/// The number of points in each of shape.
	inline int NPoints() const { return NShapes()>0 ? (*this)[0].NPoints() : 0; }

	// Align the shapes with respect to position, scale and orientation.
	int AlignShapes( bool use_tangentspace = true );

	// Returns the mean shape of the collection.
	void MeanShape(CAAMShape &meanShape) const;

    // Returns the mean shape of the collection size to mean size.
	void ReferenceShape(CAAMShape &refShape) const;

	// Writes all shapes to a matlab file.	
	int ToMatlab(const CString& sFilename,const CString& sName,const CString& sComment="",bool fAppend=false) const;

	// Insert a shape at the end of the vector.
	void Insert(const CAAMShape &s);

	/// Returns the average shape size *before* the aligment process.
	double MeanSize() const { return m_dAvgSize; }

	// Scales all shapes.
	void Scale( const double s, const bool aroundCOG = false );

	// Expand all shapes
	void Expand( int nPixels );
	
	void ToFile(const CString& sFilename) const;
	void FromFile(const CString& sFilename);
	void ToFile( FILE *fh ) const;
	void FromFile( FILE *fh );

    bool ReadDir( const CString &path, bool validate = false );
    bool ReadShapes( const std::vector<CString> &asfFiles, bool validate = false );

	/// Returns the path of all shapes.
    const CString &Path() const { return m_szPath; }

    void Rel2Abs( int rfactor = 1 );

	/// Assignment operator.
    //CAAMShapeCollection& operator=(const CAAMShapeCollection &s);
};

#endif /* __C_AAMSHAPECOLLECTION__ */