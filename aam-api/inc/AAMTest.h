//****************************************************************************
//
// Copyright � 2000, 2001, 2002, 2003 by Mikkel B. Stegmann
// 
// IMM, Informatics & Mathmatical Modelling
// Technical University of Denmark
// Richard Petersens Plads
// Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/
//
// Author: Mikkel B. Stegmann - http://www.imm.dtu.dk/~aam/ - aam@imm.dtu.dk
//
// $Id: AAMTest.h,v 1.6 2003/04/23 14:49:59 aam Exp $
//
// DISCLAIMER:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on;
//
//		- The Microsoft Vision Software Developers Kit: VisSDK, Microsoft
//		- The DTU Image Viewer: DIVA, Rune Fisker et al., IMM, DTU
//
//****************************************************************************
#ifndef __C_AAMTEST__
#define __C_AAMTEST__

#include "AAMdef.h"
#include "AAMObject.h"



class CAAMShape;
class CAAMModel;
class CAAMModelSeq;
template <class TAAMPixel> class CDMultiBand;


/**

  @author   Mikkel B. Stegmann
  @version  12-2-2002

  @memo     Container to store evaluation results in.

  @doc      Container to store evaluation results in.
			Further, it can print out the results along
			with some simple statistics.
  
*/
class CAAMEvaluationResults {

	// one evalutation result
	class CAAMEvalRes {	

		CAAMShape m_sModelShape, m_sGroundTruth;
		double m_dPtPt, m_dPtCrv, m_dOverlap, m_dTime;
		CDVector m_vPtCrvDists;
		CAAMModel::CAAMOptRes m_OptRes;

		friend CAAMEvaluationResults;
	
	public:
		
		// constructor
		CAAMEvalRes( const CAAMShape &model_shape,
				 	 const CAAMShape &ground_truth, 	
					 const double &time,
					 const CAAMModel::CAAMOptRes &optRes ) :
															 m_sModelShape( model_shape ), 
															 m_sGroundTruth( ground_truth ), 
															 m_dTime( time ),
															 m_OptRes( optRes ) {
				 					 
			CAAMUtil::CalcShapeDistances( m_sModelShape, m_sGroundTruth, m_dPtPt, m_dPtCrv, &m_vPtCrvDists );
			m_dOverlap = CAAMUtil::ShapeOverlap( m_sModelShape, m_sGroundTruth );
		}
	};


	// vector of segmentation results
	std::vector<CAAMEvalRes> m_vSegResults;

public:

	// return the segmentation results
	const std::vector<CAAMEvalRes> &Data() const { return m_vSegResults; }

	
	/// Adds a new result to the back.
	void AddResult( const CAAMShape &model_shape,
					const CAAMShape &ground_truth, 					
					const double &time,
					const CAAMModel::CAAMOptRes &optRes ) {

		m_vSegResults.push_back( CAAMEvalRes( model_shape, ground_truth, time, optRes ) );
	}


	/// Adds a new set of results to the back.
	void AddResults( const CAAMEvaluationResults &results ) {

		for(int i=0;i<results.Data().size();i++) {

			m_vSegResults.push_back( results.Data()[i] );
		}
	}


	/// �Prints all results and some statistics to either file or screen (default).
	void PrintStatistics( FILE *fh = stdout ) {

		CString s = CTime::GetCurrentTime().Format( "%A %B %d - %Y [%H:%M]" );
		fprintf( fh, "######################################################################\n" );
		fprintf( fh, "#\n#    AAM Evaluation  -  written: %s\n#\n", s );
		fprintf( fh, "######################################################################\n\n" );

		int n = m_vSegResults.size();
		CDVector ptpt(n);
		CDVector ptcrv(n);
		CDVector overlap(n);
		CDVector iter(n);
		CDVector maha(n);
		CDVector error(n);
		CDVector time(n);

		// print header + entries
		fprintf( fh, "     Exp   Pt.pt.   Pt.crv.  Overlap    Iter     Maha    Error     Time\n");
		for(int i=0;i<n;i++) {

			// print entry
			fprintf( fh, "%#8i %#8.4f %#8.4f %#8.4f %#8i %#8.4f %#8.4f %#8.4f\n", 
					 i,
					 m_vSegResults[i].m_dPtPt, 
					 m_vSegResults[i].m_dPtCrv, 
					 m_vSegResults[i].m_dOverlap, 
					 m_vSegResults[i].m_OptRes.NIter(), 
					 m_vSegResults[i].m_OptRes.Mahalanobis(),
					 m_vSegResults[i].m_OptRes.SimilarityMeasure(),
					 m_vSegResults[i].m_dTime );

			// collect data
			ptpt[i]    = m_vSegResults[i].m_dPtPt;
			ptcrv[i]   = m_vSegResults[i].m_dPtCrv;
			overlap[i] = m_vSegResults[i].m_dOverlap;
			iter[i]    = m_vSegResults[i].m_OptRes.NIter(), 
			maha[i]    = m_vSegResults[i].m_OptRes.Mahalanobis(),
			error[i]   = m_vSegResults[i].m_OptRes.SimilarityMeasure(),
			time[i]    = m_vSegResults[i].m_dTime;
		}
		fprintf( fh, "\n\n" );

		// print statistics
		fprintf( fh, "SUMMARY\n\n" );
		fprintf( fh, "               Mean  Std.err   Median      Min      Max\n");
		fprintf( fh, "Pt.pt.     %8.2f %8.2f %8.2f %8.2f %8.2f\n", 
				 ptpt.Mean(), ptpt.Std()/sqrt(n), ptpt.Median(), ptpt.Min(), ptpt.Max() );
		fprintf( fh, "Pt.crv.    %8.2f %8.2f %8.2f %8.2f %8.2f\n", 
				 ptcrv.Mean(), ptcrv.Std()/sqrt(n), ptcrv.Median(), ptcrv.Min(), ptcrv.Max() );
		fprintf( fh, "Overlap    %8.2f %8.2f %8.2f %8.2f %8.2f\n", 
				  overlap.Mean(), overlap.Std()/sqrt(n), overlap.Median(), overlap.Min(), overlap.Max() );
		fprintf( fh, "Iterations %8.2f %8.2f %8.2f %8.2f %8.2f\n", 
				 iter.Mean(), iter.Std()/sqrt(n), iter.Median(), iter.Min(), iter.Max() );
		fprintf( fh, "Mahlanobis %8.2f %8.2f %8.2f %8.2f %8.2f\n", 
				 maha.Mean(), maha.Std()/sqrt(n), maha.Median(), maha.Min(), maha.Max() );
		fprintf( fh, "Error      %8.2f %8.2f %8.2f %8.2f %8.2f\n", 
				 error.Mean(), error.Std()/sqrt(n), error.Median(), error.Min(), error.Max() );
		fprintf( fh, "Time       %8.2f %8.2f %8.2f %8.2f %8.2f\n", 
				 time.Mean(), time.Std()/sqrt(n), time.Median(), time.Min(), time.Max() );
		fprintf( fh, "\n\n" );		
	}

	
	/// Prints all results to a file.
	int PrintStatistics( const CString &filename ) {

		FILE *fh = fopen( filename, "wt" );

		if ( fh==NULL ) {

			printf("Result file '%s' could not be opened.\n", filename );
			return -1;	
		}

		PrintStatistics( fh );

		return fclose( fh )==0 ? 0 : -1;

	}
};



/**

  @author   Mikkel B. Stegmann
  @version  03-05-2001

  @memo     Container for all sorts of test functions.

  @doc      Container for all sorts of test functions. In this way a
            history of all mock-up test functions developed during
            debugging and testing are kept.   

            All functions are implemented as static functions. Thus
            no instantiation the CAAMTest are needed.            
*/
class CAAMTest : public CAAMObject {

public:

	class CAAMLowerBounds {

		const CAAMModel *m_pModel;
		std::vector<double> m_vPtPt;
		std::vector<double> m_vPtCrv;
		std::vector<double> m_vOverlap;
		std::vector<double> m_vTextureErrorSeparate;

	public:

		//
		//
		//
		CAAMLowerBounds() : m_pModel( NULL ) {}


		//
		//
		//
		CAAMLowerBounds( const CAAMModel &model ) : m_pModel( &model ) {}


		//
		//
		//
		CAAMLowerBounds& operator=(const CAAMLowerBounds &lb ) {

			this->m_pModel					= lb.m_pModel;
			this->m_vPtPt					= lb.m_vPtPt;
			this->m_vPtCrv					= lb.m_vPtCrv;
			this->m_vOverlap				= lb.m_vOverlap;
			this->m_vTextureErrorSeparate	= lb.m_vTextureErrorSeparate;

			return *this;
		}


		//
		//
		//
		void AddGroundtruths( const CAAMLowerBounds &gt ) {

			for(int i=0;i<gt.m_vPtPt.size();i++) {

				this->m_vPtPt.push_back( gt.m_vPtPt[i] );
				this->m_vPtCrv.push_back( gt.m_vPtCrv[i] );
				this->m_vOverlap.push_back( gt.m_vOverlap[i] );
				this->m_vTextureErrorSeparate.push_back( gt.m_vTextureErrorSeparate[i] );
			}
		}

		//
		//
		//
		void AddGroundtruth( const CAAMShape &gt, const CDMultiBand<TAAMPixel> &image ) {

			const CAAMDeformPCA &shapePCA = m_pModel->ShapePCA();
			

			// calc model approximation of the ground truth shape
			CAAMShape gt_copy(gt);
			CAAMShape meanShape( shapePCA.MeanDataItem() );
			gt_copy.AlignTo( meanShape );

		    CDVector b_s;
			b_s	= shapePCA.EigenVectors().Transposed() *(gt_copy-meanShape);    

			CAAMShape reconstructed_shape( meanShape + shapePCA.EigenVectors()*b_s );
			reconstructed_shape.AlignTo( gt );


			// calc shape distances to the model approximation
			double ptpt, ptcrv;
			CAAMUtil::CalcShapeDistances( reconstructed_shape, gt, ptpt, ptcrv, NULL );
			m_vPtPt.push_back( ptpt );
			m_vPtCrv.push_back( ptcrv );


			// calc shape overlap to the mode approximation
			double overlap;
			overlap = CAAMUtil::ShapeOverlap( reconstructed_shape, gt );
			m_vOverlap.push_back( overlap );


			// calc texture error assuming the shape and texture are not coupled
			CDVector texture;
			m_pModel->SampleShape( image, reconstructed_shape, texture, true, true, true );		

			CDVector modelTexture( texture );
			m_pModel->TexturePCA().Filter( modelTexture );	// make model equivalent

			texture -= modelTexture;	// form difference
			double e = texture.Norm2();	// calc L_2 norm of the difference
			m_vTextureErrorSeparate.push_back( e );
		}


		//
		//
		//
		void PrintStatistics( const CString &filename ) const { 
			
			FILE *fh = fopen( filename, "wt" ); 
			this->PrintStatistics(fh);
			fclose(fh);
		}


		//
		//
		//
		void PrintStatistics( FILE *fh = stdout ) const {

			CString s = CTime::GetCurrentTime().Format( "%B %d - %Y [%H:%M]" );
			fprintf( fh, "######################################################################\n" );
			fprintf( fh, "#\n#    Lower bounds for an AAM  -  written: %s\n#\n", s );
			fprintf( fh, "######################################################################\n\n" );

			// lazy, ugly way to do statistics... I know...
			int n = m_vPtPt.size();
			CDVector ptpt(n);
			CDVector ptcrv(n);
			CDVector overlap(n);
			CDVector textureErrorSeparate(n);
			
			// print header + entries
			fprintf( fh, "     Exp   Pt.pt.  Pt.crv.  Overlap ErrorSep\n");
			for(int i=0;i<n;i++) {

				// print entry
				fprintf( fh, "%#8i %#8.4f %#8.4f %#8.4f %#8.4f\n", 
						 i,
						 m_vPtPt[i], 
						 m_vPtCrv[i],
						 m_vOverlap[i],
						 m_vTextureErrorSeparate[i] );

				// collect data
				ptpt[i]  = m_vPtPt[i];
				ptcrv[i] = m_vPtCrv[i];	
				overlap[i] = m_vOverlap[i];
				textureErrorSeparate[i] = m_vTextureErrorSeparate[i];
			}
			fprintf( fh, "\n\n" );

			// print statistics
			// print statistics
			fprintf( fh, "SUMMARY\n\n" );
			fprintf( fh, "               Mean  Std.err   Median      Min      Max\n");
			fprintf( fh, "Pt.pt.     %8.2f %8.2f %8.2f %8.2f %8.2f\n", 
					 ptpt.Mean(), ptpt.Std()/sqrt(n), ptpt.Median(), ptpt.Min(), ptpt.Max() );
			fprintf( fh, "Pt.crv.    %8.2f %8.2f %8.2f %8.2f %8.2f\n", 
					 ptcrv.Mean(), ptcrv.Std()/sqrt(n), ptcrv.Median(), ptcrv.Min(), ptcrv.Max() );
			fprintf( fh, "Overlap    %8.2f %8.2f %8.2f %8.2f %8.2f\n", 
					  overlap.Mean(), overlap.Std()/sqrt(n), overlap.Median(), overlap.Min(), overlap.Max() );
			fprintf( fh, "ErrorSep   %8.2f %8.2f %8.2f %8.2f %8.2f\n", 
					  textureErrorSeparate.Mean(), textureErrorSeparate.Std()/sqrt(n), textureErrorSeparate.Median(), textureErrorSeparate.Min(), textureErrorSeparate.Max() );
			fprintf( fh, "\n\n" );
		}
	};
	




	CAAMTest();	    // default constructor
	~CAAMTest();	// default destructor    

    static void GetRotationTest( const CAAMShape &s1, const CAAMShape &s2 );    

    static void AnalyzeTest( const CAAMShape &refShape,
                             const CAAMShape &s, 
                             const CDMultiBand<TAAMPixel> &image,
                             const bool useConvexHull );

    static void TestPosePrediction(	const CAAMModel &model, const CString &path );

	static CAAMEvaluationResults EvaluateModel(	const CAAMModel *pModel, 
												const CString &gt_path, 					
											    const CString &result_file = "results.txt",						
												const bool writeStills = false,
												const bool writeMovies = false,				
												const bool autoinit = false,											
												const bool dump2screen = true,
												CAAMLowerBounds *pLB = NULL );

	static CAAMEvaluationResults EvaluateModelSeq(	const CAAMModelSeq &modelSeq, 
													const CString &gt_path, 					
													const CString &result_file = "results.txt",						
													const bool writeStills = false,
													const bool writeMovies = false,				
													const bool autoinit = false,											
													const bool dump2screen = true,
													CAAMLowerBounds *pLB = NULL );
};

#endif /* __C_AAMTEST__ */