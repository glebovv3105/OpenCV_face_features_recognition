//****************************************************************************
//
// Copyright � 2000, 2001, 2002, 2003 by Mikkel B. Stegmann
// 
// IMM, Informatics & Mathmatical Modelling
// Technical University of Denmark
// Richard Petersens Plads
// Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/
//
// Author: Mikkel B. Stegmann - http://www.imm.dtu.dk/~aam/ - aam@imm.dtu.dk
//
// $Id: AAMMovie.h,v 1.2 2003/01/20 10:29:15 aam Exp $
//
// DISCLAIMER:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on;
//
//		- The Microsoft Vision Software Developers Kit: VisSDK, Microsoft
//		- The DTU Image Viewer: DIVA, Rune Fisker et al., IMM, DTU
//
//****************************************************************************
#ifndef __C_AAMMOVIE__
#define __C_AAMMOVIE__

#include "AAMObject.h"
#include "AAMdef.h"
#include "DMultiBand.h"
#include <vfw.h>

/**

  @author   Mikkel B. Stegmann
  @version  9-20-2000

  @memo     Implements a simple AVI writer.

  @doc		Implements a simple AVI writer. Based on the
			old CVisAviStreamHandler code, but partly 
			rewritten and extented. Credit to the VisionSDK group.
			
*/
class CAAMMovieAVI {
public:
    CAAMMovieAVI( int framesPerSecond = 4 );
    virtual ~CAAMMovieAVI();

    virtual int  AddRef();
    virtual int  Release();

    virtual BOOL Open( const char* filename, UINT open_flags );
    virtual BOOL ReadFrame(  CVisImageBase& img, INT frame_num = -1 );
    virtual BOOL WriteFrame( CVisImageBase& img, INT frame_num = -1 );
    virtual void Close( );

    virtual const char* FileName(void)   const;
    virtual UINT        Mode()           const;
    virtual int         FrameCount(void) const;
    virtual UINT        Width(void)      const;
    virtual int         Height(void)     const;
    virtual EVisPixFmt  PixFmt(void)     const;

	void SetFrameRate( int framesPerSecond ) { m_framesPerSecond = framesPerSecond; }
    int FrameRate() const { return m_framesPerSecond; }    
	void ShowCompressionDialog( bool f = true ) { m_fShowCompressionDialog = f; }

    BOOL HandleError(char* szErrorMessage, char* szFileName, 
                     UINT uErrorLine, HRESULT hr)
    {
        throw CVisError(szErrorMessage, hr, "",
                       szFileName, uErrorLine);        
        return FALSE;
    }

    // movie composition methods
    void WriteStill(	CVisImage<CVisRGBABytePixel> image,							
						const int duration /* in ms */ );
									
	void WriteStill( CDMultiBand<TAAMPixel> image,							
					const int duration /* in ms */	);

	void XFade(	CVisImage<CVisRGBABytePixel> image1,
				CVisImage<CVisRGBABytePixel> image2,						
				const int duration /* in ms */ );	

protected:
    BOOL    OpenPgfFrame( BITMAPINFO *pbmi );

	int m_framesPerSecond;	// the frame rate, default = 4 frames/second
	bool m_fShowCompressionDialog; // default = false

    long           m_iRefCount;

    PAVIFILE       m_paviFile;      // The AVI file
    PAVISTREAM     m_paviStream;    // The AVI stream
    PAVISTREAM     m_paviCStream;   // The compressed AVI stream
    PGETFRAME      m_pgfFrame;      // Ptr in the stream to get a frame

    // avi description members
    char*          m_pzFileName;
    UINT           m_uMode;
    UINT           m_uFrameCount;
    INT            m_iHeight;
    UINT           m_uWidth;
    EVisPixFmt     m_ePixFmt;
	UINT           m_uPixelByteCount;

	BYTE          *m_pData;         // tmp storage
    BOOL           m_bOnCreateFirst;

    UINT           m_uCurFrame;

    bool           m_DoFlip;    // mbs
};

#endif /* __C_AAMMOVIE__ */