//****************************************************************************
//
// Copyright � 2000, 2001, 2002, 2003 by Mikkel B. Stegmann
// 
// IMM, Informatics & Mathmatical Modelling
// Technical University of Denmark
// Richard Petersens Plads
// Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/
//
// Author: Mikkel B. Stegmann - http://www.imm.dtu.dk/~aam/ - aam@imm.dtu.dk
//
// $Id: AAM.h,v 1.3 2003/04/23 14:49:59 aam Exp $
//
// DISCLAIMER:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on;
//
//		- The Microsoft Vision Software Developers Kit: VisSDK, Microsoft
//		- The DTU Image Viewer: DIVA, Rune Fisker et al., IMM, DTU
//
//****************************************************************************
#ifndef __CENTRAL_AAM__
#define __CENTRAL_AAM__



/*

  @author   Mikkel B. Stegmann
  @version  5-8-2000

  @memo     AAM.H - Central include file for the AAM-API.
  
  @doc		AAM.H - Central include file for the AAM-API.

*/

// AAM includes
#include "AAMObject.h"
#include "AAMdef.h"
#include "AAMShape.h"
#include "AAMModel.h"
#include "AAMBuilder.h"
#include "AAMMesh.h"
#include "AAMUtil.h"
#include "AAMDelaunay.h"
#include "AAMShapeCollection.h"
#include "AAMLinearReg.h"
#include "AAMInitialize.h"
#include "AAMVisualizer.h"
#include "AAMMovie.h"
#include "AAMDeform.h"
#include "AAMModelMS.h"
#include "AAMReferenceFrame.h"
#include "AAMAnalyzeSynthesize.h"
#include "AAMMathUtil.h"
#include "AAMModelSeq.h"


#endif /* __CENTRAL_AAM__ */
