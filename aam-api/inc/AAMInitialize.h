//****************************************************************************
//
// Copyright � 2000, 2001, 2002, 2003 by Mikkel B. Stegmann
// 
// IMM, Informatics & Mathmatical Modelling
// Technical University of Denmark
// Richard Petersens Plads
// Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/
//
// Author: Mikkel B. Stegmann - http://www.imm.dtu.dk/~aam/ - aam@imm.dtu.dk
//
// $Id: AAMInitialize.h,v 1.4 2003/02/14 21:49:23 aam Exp $
//
// DISCLAIMER:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on;
//
//		- The Microsoft Vision Software Developers Kit: VisSDK, Microsoft
//		- The DTU Image Viewer: DIVA, Rune Fisker et al., IMM, DTU
//
//****************************************************************************
#ifndef __C_AAMINITIALIZE__
#define __C_AAMINITIALIZE__

#include "AAMShape.h"

// forward declaration
class CAAMModel;


/**

  @author   Mikkel B. Stegmann
  @version  7-22-2000

  @memo     Abstract base class for all AAM initialization 
			classes.

  @doc      Abstract base class for all AAM initialization 
			classes.
    
*/
class CAAMInitialize : public CAAMObject {

protected:
	const CAAMModel *m_pModel;
	
public:	

	/// Default constructor.
	CAAMInitialize() { m_pModel=NULL; }
	
	CAAMInitialize( const CAAMModel &aammodel );

	/// General initialization function.
	virtual int Initialize( const CDMultiBand<TAAMPixel> &image, CAAMShape &s, CDVector &c ) = 0;	
};



/**

  @author   Mikkel B. Stegmann
  @version  7-22-2000

  @memo     Implementation of a simple intialization method.			

  @doc      Implementation of a simple intialization method.			
  
  @see      CAAMInitCandidates, CAAMInitEntry.
    
*/
class CAAMInitializeStegmann : public CAAMInitialize	{

	CAAMShape m_sCentredRefshape;
    CDVector m_vScaleSteps;
    CDVector m_vRotationSteps;
    std::vector<CDVector> m_vModelParameterSteps;
    double m_dXspacing, m_dYspacing;
    double m_dXmin, m_dXmax, m_dYmin, m_dYmax;
    int m_nIterations1stPass, m_nIterations2ndPass;
    int m_nCandidates;


    //////////////////////////////////////////////
    // nested classes
    //////////////////////////////////////////////

    /**

      @author   Mikkel B. Stegmann
      @version  7-19-2000

      @memo		Container for initialization results. 

      @doc      Container for initialization results.

      @see      CAAMInitialize, CAAMInitCandidates

    */
    class CAAMInitEntry {
	    
	    double m_dE_fit;
	    CAAMShape m_Shape;
	    CDVector m_vC;

    public:
	    
	    /// Constructor.
	    CAAMInitEntry(	const double e_fit, 
					    const CAAMShape s, 
                        const CDVector &c ) : m_dE_fit(e_fit), m_Shape(s), m_vC(c) { }

	    /// Returns the fit measure.
	    inline double E_fit() const { return m_dE_fit; }

	    /// Returns the corresponding shape.
	    inline CAAMShape Shape() const { return m_Shape; }

	    /// Returns the corresponding model parameters.
	    inline CDVector C() const { return m_vC; }
    };


    /**

      @author   Mikkel B. Stegmann
      @version  7-19-2000

      @memo     Initialization candidate containiner.

      @doc      Initialization candidate containiner. 
			    Holds the 'n' best candidates that has applieed
			    for acceptance.

      @see      CAAMInitEntry, CAAMInitialize

    */
    class CAAMInitCandidates {

	    int m_iNMaxCand;
	    std::vector<CAAMInitEntry> m_vInitEntries;

    public:
	    CAAMInitCandidates( int n );

	    bool ApplyForAcceptance( const CAAMInitEntry e );

	    /// Returns the number of candidates.
	    int NCandidates() const { return m_vInitEntries.size(); }

	    /// Returns the i-th candidate.
	    CAAMInitEntry &Candidate(int i) { return m_vInitEntries[i]; }
    };
    
    
public:	         

    // main
	CAAMInitializeStegmann( const CAAMModel &aammodel );
	int Initialize( const CDMultiBand<TAAMPixel> &image, CAAMShape &s, CDVector &c );	        

    // setup methods
    void SetNCandidates( const int i ) { m_nCandidates = i; }

	/// Sets the number of interations in the coarse search.
    void SetIterations1stPass( const int i ) { m_nIterations1stPass = i; }

	/// Sets the number of interations in the refinement search.
    void SetIterations2ndPass( const int i ) { m_nIterations2ndPass = i; }

	/// Sets the spacing of the search grid in x and y.
    void SetXYSpacing( const double xs, const double ys ) { m_dXspacing = xs; m_dYspacing = ys; }
    
	/// Sets the number of scale steps.
    void SetScaleSteps( const CDVector &v ) { m_vScaleSteps.Resize( v.Length() ); m_vScaleSteps = v; }    

	/// Sets the number of rotation steps.
    void SetRotationSteps( const CDVector &v ) { m_vRotationSteps.Resize( v.Length() ); m_vRotationSteps = v; }

	/// Sets the number of model parameter steps.
    void SetModelParameterSteps( const std::vector<CDVector> &av ) { m_vModelParameterSteps = av; }
};



#endif /* __C_AAMINITIALIZE__ */