//****************************************************************************
//
// Copyright � 2000, 2001, 2002, 2003 by Mikkel B. Stegmann
// 
// IMM, Informatics & Mathmatical Modelling
// Technical University of Denmark
// Richard Petersens Plads
// Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/
//
// Author: Mikkel B. Stegmann - http://www.imm.dtu.dk/~aam/ - aam@imm.dtu.dk
//
// $Id: AAMObject.h,v 1.3 2003/01/20 10:29:15 aam Exp $
//
// DISCLAIMER:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on;
//
//		- The Microsoft Vision Software Developers Kit: VisSDK, Microsoft
//		- The DTU Image Viewer: DIVA, Rune Fisker et al., IMM, DTU
//
//****************************************************************************
#ifndef __C_AAMOBJECT__
#define __C_AAMOBJECT__

#include <stdio.h>


// disable warning C4786: symbol greater than 255 character,
// okay to ignore
#pragma warning(disable: 4786)


/**

  @author   Mikkel B. Stegmann
  @version  10-19-2000

  @memo     Abstract base object for the AAM-API.

  @doc      Abstract base object for the AAM-API. Doesn't do much
			aside from devising a common way to save and dump objects.
  
*/
class CAAMObject {
	
public:
	CAAMObject();	// default constructor
	~CAAMObject();	// default destructor
    void ToFile( const char *szFilename ) const;
    void FromFile( const char *szFilename );

    // can be overridden
    virtual void ToFile( FILE *fh ) const;
    virtual void FromFile( FILE *fh );
    virtual void Dump( const char *szPath ) const;
};

#endif /* __C_AAMMOBJECT__ */