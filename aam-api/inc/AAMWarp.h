//****************************************************************************
//
// Copyright � 2000, 2001, 2002, 2003 by Mikkel B. Stegmann
// 
// IMM, Informatics & Mathmatical Modelling
// Technical University of Denmark
// Richard Petersens Plads
// Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/
//
// Author: Mikkel B. Stegmann - http://www.imm.dtu.dk/~aam/ - aam@imm.dtu.dk
//
// $Id: AAMWarp.h,v 1.3 2003/01/20 10:29:16 aam Exp $
//
// DISCLAIMER:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on;
//
//		- The Microsoft Vision Software Developers Kit: VisSDK, Microsoft
//		- The DTU Image Viewer: DIVA, Rune Fisker et al., IMM, DTU
//
//****************************************************************************
#ifndef __C_AAMWARP__
#define __C_AAMWARP__

#include "AAMShape.h"


/**

  @author	Mikkel B. Stegmann
  @version	02-14-2000

  @memo		Base class for 2D warp classes.
  @doc		CAAMWarp defines a 2D warp function between two shapes with
			an equal amount of points.

  @see		CAAMWarpLinear
	
*/
class CAAMWarp : public CAAMObject {      

	/// Source shape extents.
	double m_dSrcShapeMinX;

	/// Source shape extents.
	double m_dSrcShapeMaxX;

	/// Source shape extents.
	double m_dSrcShapeMinY;

	/// Source shape extents.
	double m_dSrcShapeMaxY;    

protected:


	/// Allows warping inside the convex hull.
	bool m_bUseConvexHull;

    CAAMShape m_SrcShape;

public:

	/// Constructor
	CAAMWarp() {}
	
	// Destructor
	~CAAMWarp();

	/**

	  @author	Mikkel B. Stegmann
	  @version	02-14-2000

	  @memo		Warps point 'in' to point 'out'.
	  @doc		Warps the point 'in' to the point 'out' using the two 
				shapes as irregular point clouds defining a 2D warp 
				function.
				'in' defines a point contained in the source shape and
				'out' is the corrosponding point in the destination
				shape.

	  @param	in	Input point.
	  @param	out	Output point.

	  @return	True if the warp can be done, false if not.
		
	*/
	virtual inline bool Warp(const CAAMPoint &in, CAAMPoint &out) const = 0 ;

	// Sets the shape to warp from.
	virtual void SetSrcShape(const CAAMShape &s);

	/// Sets the shape to warp to.
	virtual void SetDestShape(const CAAMShape &s) = 0;
		
	/// Allows warping inside the convex hull (default=off).
	void UseConvexHull( bool enable = true ) { m_bUseConvexHull=enable; }

	inline double SrcMinX() const { return m_dSrcShapeMinX; }
	inline double SrcMaxX() const { return m_dSrcShapeMaxX; }
	inline double SrcMinY() const { return m_dSrcShapeMinY; }
	inline double SrcMaxY() const { return m_dSrcShapeMaxY; }
};


/**

  @author	Mikkel B. Stegmann
  @version	02-14-2000

  @memo		Piece-wise affine warping between two shapes.
  @doc		This class implements a piece-wise affine warping 
			between two shapes using a Delaunay Triangulisation
			of the source shape. 
			This triangulisation is the used for both the 
			source and destination shape. The 
			corrospondance between points and triangles gives
			a continuous deformation field inside the triangles.
			Since the deformation is performed on a linear 
			per-triangle basis, the field is not smooth across
			triangles.

  @see		CAAMWarp	
  
	
*/
class CAAMWarpLinear : public CAAMWarp {

	/// The Delaunay Triangulisation of the source or dest shape.
	CAAMMesh m_Mesh;

    bool m_bUseSrcDelaunay;

	/// The points of the destination shape.
	std::vector<CAAMPoint> m_vDestPoints;	

    int m_PreviousTriangleHit;

public:
	
	CAAMWarpLinear( bool useSrcDelaunay = true );
	
	virtual void SetSrcShape(const CAAMShape &s);
	
	virtual void SetDestShape(const CAAMShape &s);
	
	virtual inline bool Warp(const CAAMPoint &in, CAAMPoint &out) const;			

	/// Returns true if the source shape has been set.
	bool HasSrcShape() const { return m_Mesh.NPoints()>0 && m_Mesh.NTriangles()>0; }	
};


#endif /* __C_AAMWARP__ */