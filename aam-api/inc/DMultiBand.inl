/* $Id: DMultiBand.inl,v 1.1.1.1 2003/01/03 19:18:50 aam Exp $ */
/*
   Implement multiband functionality into AAM project.
   Dmitry Karasik, 18.01.2002
*/

/////////////////////////////////////////////////////////////////////////////
// Reduce the image 
//
// input:
//      nFactor: the factor to reduce the width and height of the image
//
// author:      Rune Fisker 22/2-1999

#undef ASSERT
#define ASSERT(x) if (!(x)) { printf("assertion '%s' failed in %s:%d\n", #x, __FILE__, __LINE__); throw; }

template <class TPixel>
void CDMultiBand<TPixel>::ReducePyr(int nFactor)
{
        ASSERT(nFactor > 1);
        ASSERT( NBands() == BANDS);
        
        CDMultiBand<TPixel> imageDest(Width()/nFactor, Height()/nFactor, BANDS,evisimoptDontAlignMemory);

        double dSum[BANDS];
        int nDiv = nFactor*nFactor;
        int yDest = 0;
        int nNewRight = nFactor*imageDest.Width()+Left();
        int nNewBottom = nFactor*imageDest.Height()+Top();


        for (int ySrc=Top();ySrc<nNewBottom;yDest++,ySrc+= nFactor)
        {
                TPixel *ppixelDest = imageDest.PixelAddress(0,yDest,0);

                for (int xSrc=Left();xSrc<nNewRight;xSrc += nFactor)
                {                               
                        //dSum = 0;
                        SPAWN(dSum,0);

                        // calc mean in nFactor x nFactor window
                        for (int i=0;i<nFactor;i++)
                        {
                                TPixel *ppixelSrc = PixelAddress(xSrc,ySrc+i,0);

                                for (int j=0;j<nFactor;j++)
                                {
                                        for ( int k=0;k<BANDS;k++) {
                                            dSum[k] += *ppixelSrc++;
                                        }
                                }
                        }

                        for ( int k=0;k<BANDS;k++) {
                           *ppixelDest++ = (TPixel)(dSum[k]/nDiv+.5);
                        }
                }
        }


        Alias(imageDest);

        // update history
        CString str;
        str.Format("DIVA:  reduce by a factor %i (ReducePyr)",nFactor);
        AddToHistory(str);

}

template <class TPixel>
void CDMultiBand<TPixel>::FromRGB(CVisRGBA<CVisBytePixel> rgb, TPixel * pixel)
{
#if BANDS == 1
   *pixel = (TPixel)(.5 + .3*rgb.R() + .59*rgb.G() + .11*rgb.B()); 
#elif BANDS == 3
   *(pixel++) = rgb.R(); 
   *(pixel++) = rgb.G(); 
   *(pixel++) = rgb.B(); 
#else
#error unsupported BANDS number
#endif
}

template <class TPixel>
CVisRGBA<CVisBytePixel> CDMultiBand<TPixel>::ToRGB( TPixel * pixel)
{
   CVisRGBA<CVisBytePixel> result;
#if BANDS == 1
   result = *pixel;
#elif BANDS == 3
   result.SetRGB( pixel[0], pixel[1], pixel[2]);
#else
#error unsupported BANDS number
#endif
   return result;
}

template <class TPixel>
inline int CDMultiBand<TPixel>::isa(TPixel * pixel, int value)
{
   int i;
   ASSERT( NBands() == BANDS);
   for ( i = 0; i < BANDS; i++)
      if ( pixel[i] != value) return 0;
   return 1;
}

template <class TPixel>
inline void CDMultiBand<TPixel>::CB(TPixel * y1, TPixel * y2, double d, double * result) const
{
   int i;
   for ( i = 0; i < BANDS; i++) result[i] = d*(y2[i]-y1[i])+y1[i];
}

template <class TPixel>
inline void CDMultiBand<TPixel>::Pixel1(float x, float y, double * result) const
{
        
        register int Y = (int)y;
        register int X = (int)x;
                
        register double c=y-Y;
        register double d=x-X;
        
        register double t1[BANDS], t2[BANDS];
        CB((TPixel*)PixelAddress(X,Y,0), (TPixel*)PixelAddress(X,Y+1,0), c, t1);
        CB((TPixel*)PixelAddress(X+1,Y,0), (TPixel*)PixelAddress(X+1,Y+1,0), c, t2);
        int i;
        for ( i = 0; i < BANDS; i++) result[i] = d*(t2[i]-t1[i])+t1[i];
}

template <class TPixel>
void CDMultiBand<TPixel>::DrawLine(int x1 ,int y1 ,int x2 ,int y2, TPixel * tVal, bool fInsideCheck)
{
	bool     flip=true;
        ASSERT( NBands() == BANDS);
	
	if (abs(x2-x1) < abs(y2-y1)) 
	{
		Swap(x1,y1);
		Swap(x2,y2);
		flip = false;
	}
	
	if (x1>x2) 
	{
		Swap(x1,x2);
		Swap(y1,y2);
	}
	
	int yincr;

	if (y2>y1) 
		yincr =  1;
	else
		yincr = -1;
	
	double dx=x2-x1;
	double dy=abs(y2-y1);
	double d=2*dy-dx;
	int aincr=(int)(2*(dy-dx));
	int bincr=(int)(2*dy);
	int xx=x1;
	int yy=y1;
	int nPoints = 0;


	if (fInsideCheck)
	{
		if (!flip) 
		{
			if (ContainsPoint(yy,xx))
				set_pixel(yy,xx,tVal);
		}
		else 
		{
			if (ContainsPoint(xx,yy))
				set_pixel(xx,yy,tVal);
		}

		for (xx=x1+1;xx<=x2;xx++) 
		{
			if (d>=0) 
			{
				yy+=yincr;
				d+=aincr;
			}
			else 
				d+=bincr;

			if (!flip) 
			{
				if (ContainsPoint(yy,xx))
					set_pixel(yy,xx,tVal);
			}
			else 
			{
				if (ContainsPoint(xx,yy))
					set_pixel(xx,yy,tVal);
			}
		}       
	}
	else
	{
		if (!flip) 
		{
			set_pixel(yy,xx,tVal);
		}
		else 
		{
			set_pixel(xx,yy,tVal);
		}

		for (xx=x1+1;xx<=x2;xx++) 
		{
			if (d>=0) 
			{
				yy+=yincr;
				d+=aincr;
			}
			else 
				d+=bincr;
			
			if (!flip) 
			{
				set_pixel(yy,xx,tVal);
			}
			else 
			{
				set_pixel(xx,yy,tVal);
			}
		}       
		
	}

	// update history
	CString str;
	str.Format("DIVA: draw line from (l=%i,t=%i) to (r=%i,b=%i) (DrawLine)",x1,y1,x2,y2);
	AddToHistory(str);
}


template <class TPixel>
inline void CDMultiBand<TPixel>::set_pixel( int x, int y, TPixel * pixel)
{
   int i;
   ASSERT( BANDS == NBands());
   for ( i = 0; i < BANDS; i++) 
      Pixel( x, y, i) = pixel[i];
}

template <class TPixel>
inline void CDMultiBand<TPixel>::get_pixel( int x, int y, TPixel * pixel) const
{
   int i;
   ASSERT( BANDS == NBands());
   for ( i = 0; i < BANDS; i++) 
      pixel[i] = Pixel( x, y, i);
}

template <class TPixel>
void CDMultiBand<TPixel>::ReadBandedFile( const char * szFilename)
{
#if BANDS == 1
     ReadFile( szFilename);
     if ( NBands() != BANDS) {
        printf("(AAMC) warning: %s is not a %d-banded file\n", szFilename, BANDS);
        throw;
     }
#elif BANDS == 3
     CVisImage<CVisRGBABytePixel> img;
     img.ReadFile( szFilename);
     CDMultiBand<TPixel> gray(img.Width(),img.Height(),BANDS,evisimoptDontAlignMemory);
     if ( img.NBands() != 1) {
        printf("(AAMC) warning: %s is not a single-banded file\n", szFilename);
        throw;
     }
     gray.CopyPixelsFromRGBA(img);
     Alias(gray);
#else
#error This reading method must be implemented separately
#endif
}

template <class TPixel>
void CDMultiBand<TPixel>::WriteBandedFile( const char * szFilename, const char * szFormat)
{
    ASSERT( NBands() == BANDS);
#if BANDS == 1
    WriteFile( szFilename, szFormat);
#elif BANDS == 3
    CVisImage<CVisRGBABytePixel> img(Width(), Height(),1,evisimoptDontAlignMemory); 
    CopyPixelsToRGBA( img);
    img.WriteFile( szFilename, szFormat);
#else
#error This writing method must be implemented separately
#endif
}

template <class TPixel>
void CDMultiBand<TPixel>::CopyPixelsToRGBA( CVisImage<CVisRGBABytePixel> color)
{
   ASSERT( NBands() == BANDS);
   ASSERT( color.PixFmt() & evispixfmtRGBA );
   
   for (int y=Top();y<Bottom();y++) {
      for (int x=Left();x<Right();x++) {
         color.Pixel( x, y) = ToRGB(( TPixel*) PixelAddress(x,y,0));
      }
   }
}

template <class TPixel>
void CDMultiBand<TPixel>::CopyPixelsToRGBA( CVisImage<CVisRGBABytePixel> color, LPRECT rect)
{
   ASSERT( NBands() == BANDS);
   ASSERT( color.PixFmt() & evispixfmtRGBA );
   for (int y=rect->top, yd=Top();y<rect->bottom;y++,yd++) {
      for (int x=rect->left, xd = Left();x<rect->right;x++,xd++) {
         color.Pixel( xd, yd) = ToRGB(( TPixel*) PixelAddress(x,y,0));
      }
   }
}


template <class TPixel>
void CDMultiBand<TPixel>::CopyPixelsFromRGBA( CVisImage<CVisRGBABytePixel> color)
{
   ASSERT( NBands() == BANDS);
   ASSERT( color.PixFmt() & evispixfmtRGBA );
   TPixel p[BANDS];
   for (int y=Top();y<Bottom();y++) {
      for (int x=Left();x<Right();x++) {
           FromRGB( color.Pixel(x,y), p);
           set_pixel( x, y, p);
      }
   }
}
