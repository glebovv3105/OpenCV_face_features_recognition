//****************************************************************************
//
// Copyright � 2000, 2001, 2002, 2003 by Mikkel B. Stegmann
// 
// IMM, Informatics & Mathmatical Modelling
// Technical University of Denmark
// Richard Petersens Plads
// Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/
//
// Author: Mikkel B. Stegmann - http://www.imm.dtu.dk/~aam/ - aam@imm.dtu.dk
//
// $Id: AAMMesh.h,v 1.4 2003/01/20 10:29:15 aam Exp $
//
// DISCLAIMER:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on;
//
//		- The Microsoft Vision Software Developers Kit: VisSDK, Microsoft
//		- The DTU Image Viewer: DIVA, Rune Fisker et al., IMM, DTU
//
//****************************************************************************
#ifndef __C_AAMMESH__
#define __C_AAMMESH__

// forward declarations
class CAAMPoint;
class CAAMShape;

#include "AAMObject.h"
#include "AAMShape.h"


/**

  @author	Mikkel B. Stegmann

  @version	02-10-2000

  @memo		Point container.

  @doc		This class act as a very simple container for
			the concept 'a real-precision point'. It's merely a struct
			with a constructor. Used in the mesh and triangle  
			representation.

  @see		CAAMTriangle, CAAMMesh
	
*/
class CAAMPoint {

public:

	/// x-position.
	double x;
	/// y position.
	double y;		

	/// Constructor
	CAAMPoint() { x=.0; y=.0; }

	/// Constructor
	CAAMPoint(const double _x, const double _y) { x=_x; y=_y; }		
};


/**

  @author	Mikkel B. Stegmann
  @version	02-10-2000

  @memo		Triangle container with built-in hit test.
  @doc		This class is a simple triangle structure defined by
			three indexes to a list of points.
			Included functionality is a hit test.

  @see		CAAMMesh, CAAMPoint.
 	
*/
class CAAMTriangle {

    friend class CAAMMesh;
	
	/// The point vector from which the triangle is defined.
	std::vector<CAAMPoint> *m_pPoints;

	/// Index to point 1.
	int m_v1;
	/// Index to point 2.
	int m_v2;
	/// Index to point 3.
	int m_v3;

	double m_dD;

    void Calc_dD();

public:

	CAAMTriangle( int _v1, int _v2, int _v3,
				  std::vector<CAAMPoint> *pPoints ); 


	/// Returns index to point 1.
	inline int V1() const { return m_v1; }

	/// Returns index to point 2.
	inline int V2() const { return m_v2; }

	/// Returns index to point 3.
	inline int V3() const { return m_v3; }

	// Performs a hit test.
	bool IsInside(const CAAMPoint &p) const;

	// Performs a hit test and return the relative position of p.
	bool IsInside(const CAAMPoint &p, double &alpha, double &beta, double &gamma) const;

	// returns the center of the triangle
	CAAMPoint CenterPoint() const;
  
    double Area() const;
};


/**

  @author	Mikkel B. Stegmann

  @version	02-10-2000

  @memo		2D triangular mesh container.

  @doc		This class implements the concept of a 2D triangular mesh.
			Included functionality is a hit test and matlab export
			capability.
  
*/
class CAAMMesh : public CAAMObject {
	
	/// A vector of triangles.
	std::vector<CAAMTriangle> m_vTriangles;

	/// A vector of points.
	std::vector<CAAMPoint> m_vPoints;

	int m_iPrevTriangle;

public:
	
	CAAMMesh();	
	~CAAMMesh();

	void Insert( const CAAMPoint &p );
	void Insert( const CAAMTriangle &t );
	void ReplacePoints(const CAAMShape &s);

    CAAMMesh& operator=(const CAAMMesh &m); // assignment operator
    CAAMMesh( const CAAMMesh &m );          // copy constructor

	/// Returns a vector of triangles
	std::vector<CAAMTriangle> &Triangles() { return m_vTriangles; }

	/// Returns a vector of points
	std::vector<CAAMPoint> &Points() { return m_vPoints; }

	/// Returns a const vector of triangles
	const std::vector<CAAMTriangle> &Triangles() const { return m_vTriangles; }

	/// Returns a const vector of points
	const std::vector<CAAMPoint> &Points() const { return m_vPoints; }

    // Performs a hit test.
	inline bool IsInside( const CAAMPoint &p ) const; 
    inline bool IsInside( const CAAMPoint &p, int &triangle, 
                          double &alpha, double &beta, double &gamma ) const; 

	/// Returns the total number of points in the mesh.
	inline int NPoints() const { return m_vPoints.size(); }

	/// Returns the total number of triangles in the mesh.
	inline int NTriangles() const { return m_vTriangles.size(); }

	// Exports the mesh structure to matlab.
	int ToMatlab(const CString& sFilename) const;

	/// Deletes all points and triangles.
	void Clear() { m_vTriangles.clear(); m_vPoints.clear(); }	

    double Area() const;
};


/**

  @author   Mikkel B. Stegmann
  @version  2-14-2000
  
  @doc		Performs a hit test on the point p.                        
  
  @param    p		The point to test  
			
  @return   True if the point is inside, otherwise false.
  
*/
bool CAAMMesh::IsInside( const CAAMPoint &p ) const {

    int triangle;
    double alpha, beta, gamma;

    return this->IsInside( p, triangle, alpha, beta, gamma );
}


/**

  @author   Mikkel B. Stegmann
  @version  2-14-2000
  
  @doc		Performs a hit test on the point p.

  @memo     Performs a hit test on the point p.
  
  @param    p		 The point to test  

  @param    triangle Triangle index.

  @param    alpha    Relative position on triangle (barycentric coordinate).

  @param    beta     Relative position on triangle (barycentric coordinate).

  @param    gamme    Relative position on triangle (barycentric coordinate).
			
  @return   True if the point is inside, otherwise false.
  
*/
bool CAAMMesh::IsInside( const CAAMPoint &p,
                         int &triangle, 
                         double &alpha, double &beta, double &gamma ) const {

    // try the previous triangle
    bool bFound = m_vTriangles[m_iPrevTriangle].IsInside( p, alpha, beta, gamma );

    if (bFound) {

        triangle = m_iPrevTriangle;
        return true;
    }


    // do *slow* linear search of all triangles
    //
    // LATER: use a kD-tree for this (i.e. quad-tree)    
    //
    int n = m_vTriangles.size();
    for(int i=0;i<n;i++) {

        if (i==m_iPrevTriangle) { continue; }

        if ( m_vTriangles[i].IsInside( p, alpha, beta, gamma ) ) {

            const_cast<CAAMMesh*>(this)->m_iPrevTriangle = i;
            triangle = i;            
            return true;
        }
    }

    return false;
}


#endif /* __C_AAMMESH__ */