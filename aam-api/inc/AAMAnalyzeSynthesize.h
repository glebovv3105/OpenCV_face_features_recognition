//****************************************************************************
//
// Copyright � 2000, 2001, 2002, 2003 by Mikkel B. Stegmann
// 
// IMM, Informatics & Mathmatical Modelling
// Technical University of Denmark
// Richard Petersens Plads
// Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/
//
// Author: Mikkel B. Stegmann - http://www.imm.dtu.dk/~aam/ - aam@imm.dtu.dk
//
// $Id: AAMAnalyzeSynthesize.h,v 1.4 2003/01/20 10:29:15 aam Exp $
//
// DISCLAIMER:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on;
//
//		- The Microsoft Vision Software Developers Kit: VisSDK, Microsoft
//		- The DTU Image Viewer: DIVA, Rune Fisker et al., IMM, DTU
//
//****************************************************************************
#ifndef __C_AAMANALYZESYNTHESIZE__H__
#define __C_AAMANALYZESYNTHESIZE__H__

#include "AAMdef.h"


// disable warning C4786: symbol greater than 255 character,
// okay to ignore
#pragma warning(disable: 4786)


// forward declarations
class CAAMShape;
class CAAMReferenceFrame;
class PBuffer;
template <class TAAMPixel> class CDMultiBand;


// CAAMAnalyzeSynthesize types
enum eASid { asSoftware, asOpenGL };


/**

  @author   Mikkel B. Stegmann
  @version  6-7-2002

  @memo     Abstract base class for the analyze/synthesize classes.

  @doc      This class serves as an abstract base class for the 
			analyze/synthesize classes so that both the 
			software and�hardware implementation will have a common
			interface.

			To core task of such classes is to sample pixels inside
			a shape placed on an image (the analyze part) and render
			a texture vector into a shape given in image coordinates
			(the synthesize part).
 
*/
class CAAMAnalyzeSynthesize {

protected:
    
    const CAAMReferenceFrame *m_pReferenceFrame;    
    eASid m_Id;
	
public:

    CAAMAnalyzeSynthesize( const CAAMReferenceFrame &rf ) : m_pReferenceFrame(&rf) {}
    virtual ~CAAMAnalyzeSynthesize() {}
    
    virtual void SetAnalyzeImage( const CDMultiBand<TAAMPixel> &img ) = 0; 

    virtual bool Analyze( const CAAMShape &shape, CDVector &texture, 
                          const bool useInterpolation = true ) const = 0;

    virtual bool Analyze( const CAAMShape &shape, CDMultiBand<TAAMPixel> &refImg,
                          const bool useInterpolation = true ) const = 0;

    virtual bool Synthesize( const CAAMShape &shape, 
                             const CDVector &texture,
                             CDMultiBand<TAAMPixel> &destImage,
                             bool renderOntoImage = false ) const = 0;

    virtual CAAMAnalyzeSynthesize *Clone( const CAAMReferenceFrame &rf ) const = 0;

    virtual void FromFile( FILE *fh );
    virtual void ToFile( FILE *fh ) const;
};



/**

  @author   Mikkel B. Stegmann
  @version  6-12-2002

  @memo     Hardware implementation of the analysis and synthesis functions.

  @doc      This class utilises the presence of an OpenGL compliant graphics
			board to carry out analysis and synthesis.

			The board should supprot certain OpenGL extensions. However, this
			requirement can easily be switched of inside the methods. Refer 
			to the actual code to do this.

			However, a reasonable new GPU will support the used extensions, 
			e.g. a GeForce 2 MX will do. And to be honest, turning of the 
			need for extentions can easily make the hardware warping *slower*
			than the software warping. 

			If the OpenGL drivers are really crappy it can actually be 
			slower even without turning the extensions off. So remember to 
			do some benchmarking on your system before permenantly switching
			to OpenGL warping.

  @see      CAAMAnalyzeSynthesize
  
*/
class CAAMAnalyzeSynthesizeOpenGL : public CAAMAnalyzeSynthesize {
   
    PBuffer *m_pAnalyzePBuffer;
    PBuffer *m_pSynthesizePBuffer;
    CFrameWnd *m_pWnd;
    HDC m_DC;
    HGLRC m_RC;

    unsigned int m_iSynthesizeTextureID;  
    unsigned int m_iAnalyzeTextureID;  
    CDMultiBand<TAAMPixel> *m_pAnalyzeImage;
    CDMultiBand<TAAMPixel> *m_pSynthesizeImage;

    void SetupPBuffer( const int w, const int h, PBuffer **pPBuffer );
    void ExtractChannel( const int pixelSize,
                         const int channelNo,
                         unsigned char *pPixels,
                         CDMultiBand<TAAMPixel> &dest );

    void LoadTexture( const CDMultiBand<TAAMPixel> &textureImage, unsigned int *pTextureId );

    bool EnsureSize( const int w, const int h, PBuffer **pPBuffer );

    void SetImage( const CDMultiBand<TAAMPixel> &img, 
                   CDMultiBand<TAAMPixel> **destImage,
                   unsigned int *pTextureId );    

    int m_TextureModeID;
    bool m_bUseRectangleExt;
	
public:

    CAAMAnalyzeSynthesizeOpenGL( const CAAMReferenceFrame &rf );
	~CAAMAnalyzeSynthesizeOpenGL();	// default destructor    

    void SetAnalyzeImage( const CDMultiBand<TAAMPixel> &img ); 
    
    bool Analyze( const CAAMShape &shape, CDVector &texture,
                  const bool useInterpolation = true ) const;

    bool Analyze( const CAAMShape &shape, CDMultiBand<TAAMPixel> &refImg,
                  const bool useInterpolation = true ) const;

    bool Synthesize( const CAAMShape &shape, 
                     const CDVector &texture,
                     CDMultiBand<TAAMPixel> &destImage,
                     bool renderOntoImage = false ) const;

    CAAMAnalyzeSynthesize *Clone( const CAAMReferenceFrame &rf ) const;

    void ClockReadPixels();
};



/**

  @author   Mikkel B. Stegmann
  @version  6-12-2002

  @memo     Software implementation of the analysis and synthesis functions.
  
  @doc      This class contains a fairly fast software implementation of 
			the analyze/synthesize interface.

  @see      CAAMAnalyzeSynthesize
  
*/
class CAAMAnalyzeSynthesizeSoftware : public CAAMAnalyzeSynthesize {

public:

	// warp cache entry
    struct sWarpEntry { unsigned short int triangle; double alpha, beta, gamma; };    

private:

    std::vector<sWarpEntry> m_WarpTable;
    const CDMultiBand<TAAMPixel> *m_pAnalyzeImage;    
    void BuildWarpTable();
	
public:

    CAAMAnalyzeSynthesizeSoftware( const CAAMReferenceFrame &rf );
    ~CAAMAnalyzeSynthesizeSoftware();

    void SetAnalyzeImage( const CDMultiBand<TAAMPixel> &img ); 
    bool Analyze( const CAAMShape &shape, CDVector &texture,
                  const bool useInterpolation = true  ) const;
    bool Analyze( const CAAMShape &shape, CDMultiBand<TAAMPixel> &refImg,
                  const bool useInterpolation = true ) const;

    bool Synthesize( const CAAMShape &shape, 
                     const CDVector &texture,
                     CDMultiBand<TAAMPixel> &destImage,
                     bool renderOntoImage = false ) const;

    CAAMAnalyzeSynthesize *Clone( const CAAMReferenceFrame &rf ) const;
};


CAAMAnalyzeSynthesize *AAMLoadAnalyzerSynthesizer( FILE *fh, const CAAMReferenceFrame &rf );


#endif /* __C_AAMANALYZESYNTHESIZE__H__ */