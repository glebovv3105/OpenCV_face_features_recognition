//****************************************************************************
//
// Copyright � 2000, 2001, 2002, 2003 by Mikkel B. Stegmann
// 
// IMM, Informatics & Mathmatical Modelling
// Technical University of Denmark
// Richard Petersens Plads
// Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/
//
// Author: Mikkel B. Stegmann - http://www.imm.dtu.dk/~aam/ - aam@imm.dtu.dk
//
// $Id: AAMModelMS.h,v 1.4 2003/04/23 14:49:59 aam Exp $
//
// DISCLAIMER:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on;
//
//		- The Microsoft Vision Software Developers Kit: VisSDK, Microsoft
//		- The DTU Image Viewer: DIVA, Rune Fisker et al., IMM, DTU
//
//****************************************************************************
#ifndef __C_AAMMODELMS__
#define __C_AAMMODELMS__

#include "AAMObject.h"
#include "AAMModel.h"
#include <stdio.h>
#include <vector>

/**

  @author   Mikkel B. Stegmann
  @version  12-6-2002

  @memo     Multi-scale derivation of CAAModel.

  @doc      Multi-scale derivation of CAAModel. 
			Smaller models are stored in m_vModels.

  @see      CAAMModel
  
*/
class CAAMModelMS : public CAAMModel {

protected:
    std::vector<CAAMModel> m_vModels;	// Models
	std::vector< CDMultiBand<TAAMPixel>* > m_vImagePyr;

	void BuildPyr( const CDMultiBand<TAAMPixel> &image ) const;
	
public:
	CAAMModelMS();	// default constructor
	~CAAMModelMS();	// default destructor   
	
	void BuildAllLevels( const int nLevels,
                         const CString &inDir, 
                         const CString &acf = "", 
                         const int modelReduction = 1,
                         const int excludeShape = -1 );

    void BuildAllLevels( const int nLevels,
						 const std::vector<CString> &asfFiles,
						 const CString &acf = "", 
						 const int modelReduction = 1,
						 const int excludeShape = -1 );

    bool WriteModel( const CString &filename, const bool txt_only = false ) const;

    bool ReadModel( const CString &basename );

    CAAMModel::CAAMOptRes OptimizeModel( const CDMultiBand<TAAMPixel> &image,
                                         CAAMShape &s, CDVector &c,	
									     const int maxIterations = 30, 
                                         std::vector<CAAMOptState> *pOptStates = NULL,
                                         bool disableDamping = false ) const;
    /// Get the model at level i.
    inline const CAAMModel &Model( const int i ) const { assert(i>=0&&i<NLevels()); return i==0 ? (CAAMModel&)*this : m_vModels[i-1]; }

    /// Get the number of levels.
    inline const int NLevels() const { return m_vModels.size()+1; }

	inline const CAAMModel &GetSmallest() const { return Model( NLevels()-1 ); }
};


#endif /* __C_AAMMODELMS__ */