/****************************************************************************
//
// Copyright � 2000, 2001, 2002, 2003 by Mikkel B. Stegmann
// 
// IMM, Informatics & Mathmatical Modelling
// Technical University of Denmark
// Richard Petersens Plads
// Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/
//
// Author: Mikkel B. Stegmann - http://www.imm.dtu.dk/~aam/ - aam@imm.dtu.dk
//
// $Id: AAMShape.h,v 1.3 2003/04/23 14:49:59 aam Exp $
//
// DISCLAIMER:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on;
//
//		- The Microsoft Vision Software Developers Kit: VisSDK, Microsoft
//		- The DTU Image Viewer: DIVA, Rune Fisker et al., IMM, DTU
//
//****************************************************************************/
#ifndef __C_AAMSHAPE__
#define __C_AAMSHAPE__

#include "AAMObject.h"
#include "AAMdef.h"
#include "AAMMesh.h"
#include "DMultiBand.h"


/**

  @author	Mikkel B. Stegmann
  @version	02-08-2000

  @memo		Shape container.
  @doc		This class act as a container for a shape. Essentially it's just
			a set of 2D points stored in a vector in the format xxxyyy.

  @see		CAAMShapeCollection

	
*/
class CAAMShape : public CDVector, CAAMObject {

public:


    /**

      @author   Mikkel B. Stegmann
      @version  4-26-2000

      @memo		Auxiliary point data.
      @doc      Auxiliary point data. Needed as a part of the
			    enhanced shape representation extension [Which explains
			    why it's layered upon the CAAMShape class and not merged 
			    into it :-(].

      @see      CAAMShape
      
    */
    class CAAMPointInfo {
    
    public:	
	    unsigned char m_iPathID;
        unsigned char m_iTypeFlags;
	    unsigned short int m_iConnectFrom;
	    unsigned short int m_iConnectTo;

	    /// Constructor
        CAAMPointInfo() : 
          m_iPathID(0), m_iTypeFlags(0), m_iConnectFrom(0), m_iConnectTo(0) { }	

	    /// Point info method.
	    inline bool IsOuterEdge() const { return !(m_iTypeFlags & 0x1); }

	    /// Point info method.
	    inline bool IsOriginal() const	{ return !(m_iTypeFlags & 0x2); }

	    /// Point info method.
	    inline bool IsClosed()	const { return !(m_iTypeFlags & 0x4); }

	    /// Point info method.
	    inline bool IsHole() const { return 0x8 == (m_iTypeFlags & 0x8); }

	    /// Sets whether the point belongs to an outer edge or not.
	    inline void SetOuterEdge( bool enable = true ) { if (!enable) m_iTypeFlags |= 0x1; else m_iTypeFlags &= UCHAR_MAX - 0x1;}

	    /// Sets whether the point is an original model point or an artificial generated point.
	    inline void SetOriginal( bool enable = true ) { if (!enable) m_iTypeFlags |= 0x2; else m_iTypeFlags &= UCHAR_MAX - 0x2;}

	    /// Sets whether the point belongs to a closed or opened path.
	    inline void SetClosed( bool enable = true ) { if (!enable) m_iTypeFlags |= 0x4; else m_iTypeFlags &= UCHAR_MAX - 0x4;}

	    /// Sets whether the point belongs to an hole in the model.
	    inline void SetHole( bool enable = true ) { if (enable)  m_iTypeFlags |= 0x8; else m_iTypeFlags &= UCHAR_MAX - 0x8; }
    };

private:

    /****************************************

     Remember to modify the CopyData() 
     method, each time a new data field is
     added.

     ****************************************/
	
	/// Auxillary point data.
	std::vector<CAAMPointInfo> m_vPointAux;

    /// User-defined field 1 
    std::vector<float> m_vUser1;
    /// User-defined field 2
    std::vector<float> m_vUser2;
    /// User-defined field 3
    std::vector<float> m_vUser3;

    /// Current ASF version number
    CString m_szASFVer;

	/// The number of points.
	int m_iNbPoints;
	
	/// Indicates if the point coordinates is in relative or absolute format.
	bool m_bAbsPointCoordinates;	

    /// Optional 'host image' filename including full path.    
    CString m_szHostImage;

    void CopyData( const CAAMShape &s );

    // un-safe pointer to the first point data element
    double *m_pData;

    // Public ASF readers/writers
    bool ReadASF0_90( const CString &filename );
    bool WriteASF0_90( const CString &filename, const int image_width, const int image_height );
	
public:
		
	CAAMShape();	
	CAAMShape(int nbPoints);
    CAAMShape( const CAAMShape &v );
	CAAMShape( const CDVector &v );
	
	~CAAMShape();

	double MinX() const;
	double MaxX() const;
	double MinY() const;
	double MaxY() const;

    /// Host image (if any).
    inline const CString &HostImage() const { return m_szHostImage; }

    /// Returns the host image (if any). 
    void SetHostImage( const CString &hostImageFilename ) { m_szHostImage = hostImageFilename; }

	/// Shape width.
	double Width() const { return MaxX()-MinX(); }

	/// Shape height
	double Height() const { return MaxY()-MinY(); }
	
	/// The number of shape points.
	inline const int NPoints() const { return m_iNbPoints; }
	
	CAAMPoint COG() const;	
	void COG( double &x, double &y ) const;	

	void Rotate( const double theta, const bool aroundCOG = false );

	void Translate( const double x, const double y );	
	void Translate( const CAAMPoint &p );	

	void Scale( const double s, const bool aroundCOG = false );

    double ShapeSize() const;
	
	double Normalize();
	
    // operators
	CAAMShape& operator=(const CAAMShape &s);
    CDVector& operator=(const CVisDVector &vIn);	
    CDVector& operator=(double value);
    CAAMShape operator+(const CVisDVector &v) const;
    CAAMShape operator-(const CVisDVector &v) const;
		
	int GetPoint( int i, double &x, double &y) const;	
	CAAMPoint GetPoint( int i ) const;

    // un-safe (but fast) version of GetPoint
    //
    // un-safe (and dirty) since it depends on assumptions regarding the
    // memory layout the parent of CAAMShape, namely CVisDVector
    //
    inline void GetPointUS( int i, double *x, double *y) { x = &(m_pData[i]); y = &(m_pData[i+m_iNbPoints]); }
    inline void GetPointUS( int i, double &x, double &y) const { x = m_pData[i]; y = m_pData[i+m_iNbPoints]; }

	int SetPoint( const int i, const double &x, const double &y);
	int SetPoint( const int i, const CAAMPoint &p );	
	
	void Resize(int length, double *storage=NULL);
	
	double GetRotation( const CAAMShape &ref ) const;

	double AlignTo( const CAAMShape &ref, double *pTheta = NULL );
    void AlignTransformation( const CAAMShape &ref, double &scale, double &theta, CAAMPoint &t ) const;

	void Displace( const CDVector &poseVec );

	static void PoseVec2Param( const CDVector &poseVec, 
						 	   double &scale, double &theta, 
							   double &tx, double &ty 		); 

	static void Param2PoseVec( const double scale, const double theta, 
							   const double tx, const double ty,
							   CDVector &poseVec ); 

	void FromFile( FILE *fh );
	void ToFile( FILE *fh ) const;
	
	bool IsInside( const CAAMPoint &p, bool bBoundTest = false ) const;
	bool IsInsidePath( const CAAMPoint &p, const int path_start ) const;

	void Expand( int nPixels );

	void Normal( const int i, CAAMPoint& p1, CAAMPoint& p2, const double dist ) const;
    void NormalDisplacement( const int i, const double dist );

    bool WriteASF( const CString &filename,
				   const int image_width, const int image_height );
	bool ReadASF( const CString &filename );

	/// Returns true if the shape is in absolute coordinates.
	bool IsAbs() const { return m_bAbsPointCoordinates; }

   	void Abs2Rel( const CString hostImagePath = ".\\" );
	void Rel2Abs( const CString hostImagePath = ".\\" );
	void Abs2Rel( const int image_width, const int image_height );
	void Rel2Abs( const int image_width, const int image_height );

	void SetClosedPathConnectivity();

	/// Returns the complete point aux vector of the shape.
	std::vector<CAAMPointInfo> &PointAux() { return m_vPointAux; }

    /// Returns the complete point aux vector of the shape.
	const std::vector<CAAMPointInfo> &PointAux() const { return m_vPointAux; }

	void AddPath( const CAAMShape &shape, const CAAMPointInfo &pointType );

	CAAMShape ExtractPath( const int startPosition  ) const;

	void SetPointInfoFlagsInPath( const int startPosition, const int flags );

	int PathLen( const int startPosition ) const;
	std::vector<int> GetPaths() const;

	void AddShapeExtends( int nPixels );

	void ReversePointOrder();

	void AddInterior(const int interations = 1);

	bool ConsistencyCheck();

	void MakeBorderShape( int size );

    void GetHostImage( CDMultiBand<TAAMPixel> &dest, const CString &path, const int rfactor ) const;        

    double Area( bool use_covex_hull = false ) const;

    CAAMShape CalcConvexHull() const;

    bool IsConvex() const;

    void RemovePoint( const int i );

    void AllocateUserFields();
    std::vector<float> &UserField( const int field_nb );
    const std::vector<float> &UserField( const int field_nb ) const;    
};


#endif /* __C_AAMSHAPE__ */