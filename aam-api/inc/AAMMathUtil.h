//****************************************************************************
//
// Copyright � 2000, 2001, 2002, 2003 by Mikkel B. Stegmann
// 
// IMM, Informatics & Mathmatical Modelling
// Technical University of Denmark
// Richard Petersens Plads
// Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/
//
// Author: Mikkel B. Stegmann - http://www.imm.dtu.dk/~aam/ - aam@imm.dtu.dk
//
// $Id: AAMMathUtil.h,v 1.2 2003/01/20 10:29:15 aam Exp $
//
// DISCLAIMER:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on;
//
//		- The Microsoft Vision Software Developers Kit: VisSDK, Microsoft
//		- The DTU Image Viewer: DIVA, Rune Fisker et al., IMM, DTU
//
//****************************************************************************
#ifndef __C_AAMMATHUTIL__H__
#define __C_AAMMATHUTIL__H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DVector.h"

/**

  @author   Mikkel B. Stegmann
  @version  1-28-2002

  @memo     Utility math/statistical methods for the AAM project.

  @doc      This is the mathematical / statistical garbage bin.

            It consists of methods that for some reason haven't found its
            way into other (and more meaningful) classes.			

			All methods are static, so there is never need for an
            instantiation of this class.  
*/
class CAAMMathUtil {
	
    virtual void DoNotInstantiateThisClassCuzAllMethodsAreStatic() = 0;

public:    

	static void Hist(	CDVector &v, const double min, const double max, 
		        		CDVector &hist, const int nbins = 256, bool normalize = true, 
                   		bool transform = false  );

    static void CumSum( const CDVector &v, CDVector &cumsum, bool normalize = true );

    static double MutualInformation( const CDVector &v1, const CDVector &v2, int nbins = 256 );

    static void GaussianHistogramMatching(   const CDVector &v, CDVector &out, 
                                             const int nbins = 256, CDVector *gaussLUT = NULL );

    static void ExpandMatrix2DyadicSize( const CDMatrix &m, CDMatrix &dyad );

    static void CalcElementVar( const std::vector<CDVector> &vVectors, CDVector &varVec,
                                CDVector *vpMean = NULL );		

    static void LinearStretchMinMax( CDVector &v, 
                                     const double new_min, const double new_max );

    static void LinearStretchClamp(  CDVector &v, 
                                     const double x1, const double x2,
                                     const double new_min, const double new_max );

    static void MeanFilter( const CDMatrix &in, CDMatrix &out );

    static void ZeroMeanUnitLength( CDVector &v );

    static void PseudoInv( const CDMatrix &A, CDMatrix &P );
};



#endif /* __C_AAMMATHUTIL__H__ */
