//****************************************************************************
//
// Copyright � 2000, 2001, 2002, 2003 by Mikkel B. Stegmann
// 
// IMM, Informatics & Mathmatical Modelling
// Technical University of Denmark
// Richard Petersens Plads
// Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/
//
// Author: Mikkel B. Stegmann - http://www.imm.dtu.dk/~aam/ - aam@imm.dtu.dk
//
// $Id: AAMOptimize.h,v 1.3 2003/02/14 21:49:23 aam Exp $
//
// DISCLAIMER:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on;
//
//		- The Microsoft Vision Software Developers Kit: VisSDK, Microsoft
//		- The DTU Image Viewer: DIVA, Rune Fisker et al., IMM, DTU
//
//****************************************************************************
#ifndef __C_AAMOPTIMIZE__
#define __C_AAMOPTIMIZE__

#include "AAMObject.h"
#include "AAMUtil.h"
#include "DOptimizeBase.h"

// opt includes
#include "DOptimizeBFGS.h"
#include "DOptimizeSD.h"
#include "DOptimizeCG.h"
#include "DOptimizeSA.h"
#include "DOptimizePS.h"

// forward declaration
class CAAMModel;



/**

  @author   Mikkel B. Stegmann
  @version  7-8-2000

  @memo		General purpose optimization of the AAM.

  @doc		Class for optimizing the AAM using traditional optimization
			methods such as simluated annealingm, conjugated gradient, 
			steepest descent, BGFS and pattern search.

*/
class CAAMOptimize : public CDOptimizeFuncBase, CAAMObject {
	
	/// The AAM.
	const CAAMModel *m_pModel;

	/// The initial shape pose.
	const CAAMShape *m_pShape;	

	/// The image where the optimization shuold be done.
	const CDMultiBand<TAAMPixel> *m_pImage;

	/// The similarity measure to be used.
	int m_iSimilarityMeasure;

	/// The difference vector
	CDVector m_vDiff;

	/// The minimum fit
	double m_dMinFit;

	/// The minimum fit shape
	CAAMShape m_sMinShape;

	/// The minimum fit model parameters
	CDVector m_vMinC;

	inline double EvalFunction(CDVector& vX);

    double m_dFit;

public:
    double FinalFrror() const { return m_dFit; }

	CAAMOptimize(const CAAMModel &aammodel, const CAAMShape &s, const CDMultiBand<TAAMPixel> &image, const int similaritym );

	void OptResults( CDVector &c, CAAMShape &s, double &fit );
};


#endif /* __C_AAMOPTIMIZE__ */
