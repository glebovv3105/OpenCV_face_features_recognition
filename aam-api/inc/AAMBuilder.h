//****************************************************************************
//
// Copyright � 2000, 2001, 2002, 2003 by Mikkel B. Stegmann
// 
// IMM, Informatics & Mathmatical Modelling
// Technical University of Denmark
// Richard Petersens Plads
// Building 321
// DK-2800 Lyngby, Denmark
// http://www.imm.dtu.dk/
//
// Author: Mikkel B. Stegmann - http://www.imm.dtu.dk/~aam/ - aam@imm.dtu.dk
//
// $Id: AAMBuilder.h,v 1.3 2003/01/20 10:29:15 aam Exp $
//
// DISCLAIMER:
//
// No guarantees of performance accompany this software,
// nor is any responsibility assumed on the part of the author(s).
// The software has been tested extensively and every effort has been
// made to insure its reliability.
//
// This software is provided by IMM and the contributor(s) ``as is'' and
// any express or implied warranties, including, but not limited to, the
// implied warranties of merchantability and fitness for a particular purpose
// are disclaimed.  In no event shall IMM or the contributor(s) be liable
// for any direct, indirect, incidental, special, exemplary, or consequential
// damages (including, but not limited to, procurement of substitute goods
// or services; loss of use, data, or profits; or business interruption)
// however caused and on any theory of liability, whether in contract, strict
// liability, or tort (including negligence or otherwise) arising in any way
// out of the use of this software, even if advised of the possibility of
// such damage.
//
// This software is partly based on;
//
//		- The Microsoft Vision Software Developers Kit: VisSDK, Microsoft
//		- The DTU Image Viewer: DIVA, Rune Fisker et al., IMM, DTU
//
//****************************************************************************
#ifndef __C_AAMBUILDER__
#define __C_AAMBUILDER__

#include <stdio.h>

#include "AAMObject.h"
#include "AAMModel.h"
#include "AAMShapeCollection.h"

/**

  @author   Mikkel B. Stegmann
  @version  10-26-2000

  @memo     Factory object that produces CAAMModel objects.

  @doc      Factory object that produces CAAMModel objects.
			Main tasks are the estimation of parameter update matrices
			and verbose dumping of model information. Most other tasks are
			simple calls into CAAMModel, CAAMShape etc.

  @see      CAAMModel
  
*/
class CAAMBuilder : public CAAMObject {

protected:

    /// The raw (unaligned) shapes.
    CAAMShapeCollection m_Shapes;

    /// The aligned shapes.
    CAAMShapeCollection m_AlignedShapes;   	

    /// The texture samples
	std::vector<CDVector> m_vTexture;

    bool m_bVerbose;
	bool m_bWriteRegMovie;
	bool m_bWriteVarImage;
	bool m_bMakeDocumentation;
    int  m_iTSSubsampling;
    int m_iWarpMethod;

    /// Pointer to the model we're building
    CAAMModel *m_pModel;
	
    void BuildTextureVectors();


    // regression method
    void EstRegressionMatrices(   const std::vector<CDVector> &bVectors, 
                                    const int ts_subsampling );

    void DoPoseExperiments( const std::vector<CDVector> &vPoseDisps, 
                            const std::vector<CDVector> &cVectors, 
                            CDMatrix &X, CDMatrix &C,
                            const int ts_subsampling ) const;

    void DoCParamExperiments( const std::vector<CDVector> &vCDisps, 
                              const std::vector<CDVector> &cVectors, 
                              CDMatrix &X, CDMatrix &C,
                              const int ts_subsampling ) const;
    

    // gradient method
    void EstPredictionMatrices( const std::vector<CDVector> &bVectors, 
                                const int ts_subsampling);

    void EstPoseGradientMatrix( const std::vector<CDVector> &vPoseDisps, 
                                const std::vector<CDVector> &cVectors, 
                                CDMatrix &Gpose,
                                const int ts_subsampling ) const;

    void EstCParamGradientMatrix( const std::vector<CDVector> &vCDisps, 
                                  const std::vector<CDVector> &cVectors, 
                                  CDMatrix &Gparam,
                                  const int ts_subsampling) const;


    void DisplacementSets(  std::vector<CDVector> &vCDisps, 
                            std::vector<CDVector> &vPoseDisps,
                            const std::vector<CDVector> &cVectors ) const;

    std::vector<CDVector> CParamDispVectors( const CDVector &vStdDisp, 
                                             const std::vector<CDVector> &cVectors,
                                             const int pStart=0, const int pLen=0 ) const;

    std::vector<CDVector> PoseDispVectors( const CDVector &vXDisp, 
                                           const CDVector &vYDisp, 
                                           const CDVector &vScaleDisp, 
                                           const CDVector &vRotDisp  ) const;

    void CalcPixel2ShapeWeights();

    std::vector<CDVector> DoCombinedPCA();

    void ModelDisplacement(	const CDMultiBand<TAAMPixel> &image, 
							const CAAMShape &shape, 
							const CDVector &c0, 
		    				const CDVector &delta_c, 
							CDVector &pixel_diff ) const;

    void PoseDisplacement(	const CDMultiBand<TAAMPixel> &image, 
							const CAAMShape &shape, 
							const CDVector &c0, 
							const CDVector &t, 										
							CDVector &pixel_diff	) const;
    
    void DumpModelDoc( std::vector<CDVector> bVectors );

    void DoShapeAlignment( const bool fUseTangentSpace );

    void DumpPCA( const std::vector<CDVector> &bVectors );

    void NormalizeTextureVectors();

   	bool ReadACF( const CString &filename );

    void RecalcMeanTexture();

    void MapTextures();

    static bool LoadShapes( const std::vector<CString> &asfFiles,
                            CAAMShapeCollection &destination, 
                            int modelReduction = 1, 
                            bool addCompleteImage = false,
                            double addExtents = .0,
                            int excludeShape = -1 );

public:

	CAAMBuilder( );	// default constructor
	~CAAMBuilder();	// default destructor   

    void BuildFromFiles( CAAMModel &model, 
                         const CString &inDir, 
                         const CString &acf = "", 
                         const int modelReduction = 1,
                         const int excludeShape = -1 );

    void BuildFromFiles( CAAMModel &model, 
                         const std::vector<CString> &asfFiles,
                         const CString &acf = "", 
                         const int modelReduction = 1,
                         const int excludeShape = -1);                                                       

};

#endif /* __C_AAMBUILDER__ */
