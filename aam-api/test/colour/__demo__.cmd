@echo off
echo *********************************************************
echo.
echo   This small AAMC demo builds an AAM and produce the
echo   following files:
echo.
echo     model.txt          Model information
echo.
echo     model.amf          Binary model data
echo.
echo     modelvar.bmp       Image of the pixel variation
echo.
echo     registration.avi   Registration movie - i.e.
echo                        all training examples warped       
echo                        to the mean shape
echo.
echo     shape0i.avi        i-th shape PCA mode (+/- 3 std)
echo     texture0i.avi      i-th texture PCA mode (+/- 3 std)
echo     combined0i.avi     i-th combined PCA mode (+/- 3 std)
echo.
echo   AAM-API site: http://www.imm.dtu.dk/~aam/
echo.
echo *********************************************************
pause

aamcm b data model __conf__.acf
aamcm m model.amf all 2
aamcm e model.amf unseen both auto
aamcm e model.amf unseen both pseudo

echo *********************************************************
echo.
echo All tests done!
echo.
echo *********************************************************
pause