# Microsoft Developer Studio Project File - Name="aam_api" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=aam_api - Win32 Debug 3 band
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "aam_api.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "aam_api.mak" CFG="aam_api - Win32 Debug 3 band"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "aam_api - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "aam_api - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "aam_api - Win32 Release 3 band" (based on "Win32 (x86) Static Library")
!MESSAGE "aam_api - Win32 Debug 3 band" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "aam_api - Win32 Release"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /Ob2 /I "../aam-api/pbuffer/" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /YX /FD /c
# ADD BASE RSC /l 0x406 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x406 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"..\lib\aam-api.lib"

!ELSEIF  "$(CFG)" == "aam_api - Win32 Debug"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GR /GX /ZI /Od /I "../aam-api/pbuffer/" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /YX /FD /GZ /c
# ADD BASE RSC /l 0x406 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x406 /i "./" /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"..\lib\aam-apiDB.lib"

!ELSEIF  "$(CFG)" == "aam_api - Win32 Release 3 band"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "aam_api___Win32_Release_3_band"
# PROP BASE Intermediate_Dir "aam_api___Win32_Release_3_band"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "aam_api___Win32_Release_3_band"
# PROP Intermediate_Dir "aam_api___Win32_Release_3_band"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /Ob2 /I "../aam-api/pbuffer/" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "AAM_3BAND" /YX /FD /c
# ADD BASE RSC /l 0x406 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x406 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo /out:"..\lib\aam_api.lib"
# ADD LIB32 /nologo /out:"..\lib\aam-apim.lib"

!ELSEIF  "$(CFG)" == "aam_api - Win32 Debug 3 band"

# PROP BASE Use_MFC 2
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "aam_api___Win32_Debug_3_band"
# PROP BASE Intermediate_Dir "aam_api___Win32_Debug_3_band"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "aam_api___Win32_Debug_3_band"
# PROP Intermediate_Dir "aam_api___Win32_Debug_3_band"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GR /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GR /GX /ZI /Od /I "../aam-api/pbuffer/" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "AAM_3BAND" /YX /FD /GZ /c
# ADD BASE RSC /l 0x406 /i "./" /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x406 /i "./" /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo /out:"..\lib\aam_apiDB.lib"
# ADD LIB32 /nologo /out:"..\lib\aam-apimDB.lib"

!ENDIF 

# Begin Target

# Name "aam_api - Win32 Release"
# Name "aam_api - Win32 Debug"
# Name "aam_api - Win32 Release 3 band"
# Name "aam_api - Win32 Debug 3 band"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE="..\aam-api\AAMAnalyzeSynthesize.cpp"
# End Source File
# Begin Source File

SOURCE="..\aam-api\AAMAnalyzeSynthesizeOpenGL.cpp"
# End Source File
# Begin Source File

SOURCE="..\aam-api\AAMAnalyzeSynthesizeSoftware.cpp"
# End Source File
# Begin Source File

SOURCE="..\aam-api\AAMBuilder.cpp"
# End Source File
# Begin Source File

SOURCE="..\aam-api\AAMDeform.cpp"
# End Source File
# Begin Source File

SOURCE="..\aam-api\AAMDelaunay.cpp"
# End Source File
# Begin Source File

SOURCE="..\aam-api\AAMInitialize.cpp"
# End Source File
# Begin Source File

SOURCE="..\aam-api\AAMLinearReg.cpp"
# End Source File
# Begin Source File

SOURCE="..\aam-api\AAMLoadTransferFunction.cpp"
# End Source File
# Begin Source File

SOURCE="..\aam-api\AAMLog.cpp"
# End Source File
# Begin Source File

SOURCE="..\aam-api\AAMMathUtil.cpp"
# End Source File
# Begin Source File

SOURCE="..\aam-api\AAMMesh.cpp"
# End Source File
# Begin Source File

SOURCE="..\aam-api\AAMModel.cpp"
# End Source File
# Begin Source File

SOURCE="..\aam-api\AAMModelMS.cpp"
# End Source File
# Begin Source File

SOURCE="..\aam-api\AAMModelSeq.cpp"
# End Source File
# Begin Source File

SOURCE="..\aam-api\AAMMovie.cpp"
# End Source File
# Begin Source File

SOURCE="..\aam-api\AAMObject.cpp"
# End Source File
# Begin Source File

SOURCE="..\aam-api\AAMOptimize.cpp"
# End Source File
# Begin Source File

SOURCE="..\aam-api\AAMReferenceFrame.cpp"
# End Source File
# Begin Source File

SOURCE="..\aam-api\AAMShape.cpp"
# End Source File
# Begin Source File

SOURCE="..\aam-api\AAMShapeCollection.cpp"
# End Source File
# Begin Source File

SOURCE="..\aam-api\AAMTest.cpp"
# End Source File
# Begin Source File

SOURCE="..\aam-api\AAMTransferFunction.cpp"
# End Source File
# Begin Source File

SOURCE="..\aam-api\AAMTransferFunctions.cpp"
# End Source File
# Begin Source File

SOURCE="..\aam-api\AAMUtil.cpp"
# End Source File
# Begin Source File

SOURCE="..\aam-api\AAMVisualizer.cpp"
# End Source File
# Begin Source File

SOURCE="..\aam-api\AAMWarp.cpp"
# End Source File
# Begin Source File

SOURCE="..\aam-api\DMultiBand.cpp"
# End Source File
# Begin Source File

SOURCE="..\aam-api\htimer.cpp"
# End Source File
# Begin Source File

SOURCE="..\aam-api\pbuffer\pbuffer.cpp"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\inc\AAM.h
# End Source File
# Begin Source File

SOURCE=..\inc\AAMAnalyzeSynthesize.h
# End Source File
# Begin Source File

SOURCE=..\inc\AAMBuilder.h
# End Source File
# Begin Source File

SOURCE=..\inc\AAMdef.h
# End Source File
# Begin Source File

SOURCE=..\inc\AAMDeform.h
# End Source File
# Begin Source File

SOURCE=..\inc\AAMDelaunay.h
# End Source File
# Begin Source File

SOURCE=..\inc\AAMInitialize.h
# End Source File
# Begin Source File

SOURCE=..\inc\AAMLinearReg.h
# End Source File
# Begin Source File

SOURCE=..\inc\AAMLog.h
# End Source File
# Begin Source File

SOURCE=..\inc\AAMMathUtil.h
# End Source File
# Begin Source File

SOURCE=..\inc\AAMMesh.h
# End Source File
# Begin Source File

SOURCE=..\inc\AAMModel.h
# End Source File
# Begin Source File

SOURCE=..\inc\AAMModelMS.h
# End Source File
# Begin Source File

SOURCE=..\inc\AAMModelSeq.h
# End Source File
# Begin Source File

SOURCE=..\inc\AAMMovie.h
# End Source File
# Begin Source File

SOURCE=..\inc\AAMObject.h
# End Source File
# Begin Source File

SOURCE=..\inc\AAMOptimize.h
# End Source File
# Begin Source File

SOURCE=..\inc\AAMReferenceFrame.h
# End Source File
# Begin Source File

SOURCE=..\inc\AAMShape.h
# End Source File
# Begin Source File

SOURCE=..\inc\AAMShapeCollection.h
# End Source File
# Begin Source File

SOURCE=..\inc\AAMTest.h
# End Source File
# Begin Source File

SOURCE=..\inc\AAMTransferFunction.h
# End Source File
# Begin Source File

SOURCE=..\inc\AAMTransferFunctions.h
# End Source File
# Begin Source File

SOURCE=..\inc\AAMUtil.h
# End Source File
# Begin Source File

SOURCE=..\inc\AAMVisualizer.h
# End Source File
# Begin Source File

SOURCE=..\inc\AAMWarp.h
# End Source File
# Begin Source File

SOURCE=..\inc\DMultiBand.h
# End Source File
# Begin Source File

SOURCE=..\inc\DMultiBand.inl
# End Source File
# Begin Source File

SOURCE=..\inc\htimer.h
# End Source File
# Begin Source File

SOURCE="..\aam-api\pbuffer\pbuffer.h"
# End Source File
# End Group
# Begin Group "External Header Files"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\diva\inc\DMatrix.h
# End Source File
# Begin Source File

SOURCE="..\..\..\..\Program Files\VisSDK\VisMatrix\VisDMatrix.h"
# End Source File
# End Group
# End Target
# End Project
