#include "FaceRec.h"

String face_cascade_name = "data/haarcascade_frontalface_alt.xml";
String eyes_cascade_name = "data/haarcascade_eye_tree_eyeglasses.xml";
CascadeClassifier face_cascade;
CascadeClassifier eyes_cascade;
Mat frame;
Mat frame_gray;
vector<Rect> faces;
vector<Rect> eyes;
Rect faceTop;
RecData data;

FaceRec::FaceRec()
{
	face_cascade.load(face_cascade_name);
	eyes_cascade.load(eyes_cascade_name);
}

RecData FaceRec::recognize(Mat &frame_gray)
{	
	int maxarea = 0; // ������������ ������� ����
	int num_maxsize = 0; // ����� ���� � ������������ ��������
	face_cascade.detectMultiScale(frame_gray, data.faces, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, Size(30, 30));
	if (!data.faces.empty()){
		for (int i = 0; i < data.faces.size(); i++)
		{
			if ((data.faces[i].width * data.faces[i].height) >= maxarea) // ��������� ����������� �� ��� �� �������
			{
				maxarea = data.faces[i].width * data.faces[i].height;
				num_maxsize = i; // ����� ����������� �� ���
			}
		}
		data.maxnum = num_maxsize; // ������ � ������������ ��������� ����������� �� ���
		faceTop = data.faces[num_maxsize];
		faceTop.height = faceTop.height * 0.5;
		Mat topFaceROI = frame_gray(faceTop); // ����� ���� ������ � ������� ����� ����
		//-- In each face, detect eyes
		eyes_cascade.detectMultiScale(topFaceROI, data.eyes, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, Size(30, 30));
	}
	return data; // ���������� ���������, � ����� ������� ������ �� ������������ ��������
}

FaceRec::~FaceRec()
{
}