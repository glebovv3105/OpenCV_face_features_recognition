#include "FaceRec.h"

vector<Rect> faces_main;
vector<Rect> eyes_main;
Mat frame_main;
Mat frame_gray_main;
string window_name = "Capture - Face detection";
RNG rng(12345);

int main(int argc, const char** argv)
{
	VideoCapture cap(0); // open the default camera
	if (!cap.isOpened())  // check if we succeeded
		return -1;
		FaceRec facesAndEyes; // object creation
	for (;;)
	{
		cap >> frame_main; // get a new frame from camera
		if (!frame_main.empty())
		{
			cvtColor(frame_main, frame_gray_main, CV_BGR2GRAY);
			equalizeHist(frame_gray_main, frame_gray_main);
			RecData result_data;
			result_data = facesAndEyes.recognize(frame_gray_main); // return and eyes vectors of rectangles and faces rectangle to structure result_data
			if (!result_data.faces.empty()){ // check faces' presence in frame
				Point center(result_data.faces[result_data.maxnum].x + result_data.faces[result_data.maxnum].width*0.5, result_data.faces[result_data.maxnum].y + result_data.faces[result_data.maxnum].height*0.5);
				ellipse(frame_main, center, Size(result_data.faces[result_data.maxnum].width*0.5, result_data.faces[result_data.maxnum].height*0.5), 0, 0, 360, Scalar(255, 0, 255), 4, 8, 0);
				for (int j = 0; j < result_data.eyes.size(); j++)
				{
					Point center(result_data.faces[result_data.maxnum].x + result_data.eyes[j].x + result_data.eyes[j].width*0.5, result_data.faces[result_data.maxnum].y + result_data.eyes[j].y + result_data.eyes[j].height*0.5);
					int radius = cvRound((result_data.eyes[j].width + result_data.eyes[j].height)*0.25);
					circle(frame_main, center, radius, Scalar(255, 0, 0), 4, 8, 0);
					cout << result_data.eyes[j] << endl;
				}
			}
				imshow(window_name, frame_main);
		}
		else
		{
			printf(" --(!) No captured frame -- Break!"); break;
		}
		int c = waitKey(10);
		if ((char)c == 'c') { break; }
	}
	// the camera will be deinitialized automatically in VideoCapture destructor
	return 0;
}